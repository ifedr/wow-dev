-- MySQL dump 10.13  Distrib 5.5.8, for Win32 (x86)
--
-- Host: localhost    Database: world_lite
-- ------------------------------------------------------
-- Server version	5.5.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `access_requirement`
--

DROP TABLE IF EXISTS `access_requirement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `access_requirement` (
  `mapId` mediumint(8) unsigned NOT NULL,
  `difficulty` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `level_min` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `level_max` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `item` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `item2` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `quest_done_A` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `quest_done_H` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `completed_achievement` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `quest_failed_text` text,
  `comment` text,
  PRIMARY KEY (`mapId`,`difficulty`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='CTDB Access Requirements';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `access_requirement`
--

LOCK TABLES `access_requirement` WRITE;
/*!40000 ALTER TABLE `access_requirement` DISABLE KEYS */;
/*!40000 ALTER TABLE `access_requirement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `achievement_criteria_data`
--

DROP TABLE IF EXISTS `achievement_criteria_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `achievement_criteria_data` (
  `criteria_id` mediumint(8) NOT NULL,
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `value1` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `value2` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `ScriptName` char(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`criteria_id`,`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='CTDB Achievment system';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `achievement_criteria_data`
--

LOCK TABLES `achievement_criteria_data` WRITE;
/*!40000 ALTER TABLE `achievement_criteria_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `achievement_criteria_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `achievement_dbc`
--

DROP TABLE IF EXISTS `achievement_dbc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `achievement_dbc` (
  `ID` int(10) unsigned NOT NULL,
  `requiredFaction` int(11) NOT NULL DEFAULT '-1',
  `mapID` int(11) NOT NULL DEFAULT '-1',
  `points` int(10) unsigned NOT NULL DEFAULT '0',
  `flags` int(10) unsigned NOT NULL DEFAULT '0',
  `count` int(10) unsigned NOT NULL DEFAULT '0',
  `refAchievement` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `achievement_dbc`
--

LOCK TABLES `achievement_dbc` WRITE;
/*!40000 ALTER TABLE `achievement_dbc` DISABLE KEYS */;
INSERT INTO `achievement_dbc` VALUES (3696,-1,-1,0,2,1,0),(4788,-1,-1,0,2,1,0),(4789,-1,-1,0,2,1,0);
/*!40000 ALTER TABLE `achievement_dbc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `achievement_reward`
--

DROP TABLE IF EXISTS `achievement_reward`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `achievement_reward` (
  `entry` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `title_A` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `title_H` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `item` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `sender` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `subject` varchar(255) DEFAULT NULL,
  `text` text,
  PRIMARY KEY (`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='CTDB Loot System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `achievement_reward`
--

LOCK TABLES `achievement_reward` WRITE;
/*!40000 ALTER TABLE `achievement_reward` DISABLE KEYS */;
/*!40000 ALTER TABLE `achievement_reward` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `areatrigger_involvedrelation`
--

DROP TABLE IF EXISTS `areatrigger_involvedrelation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `areatrigger_involvedrelation` (
  `id` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'Identifier',
  `quest` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'Quest Identifier',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='CTDB Trigger System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `areatrigger_involvedrelation`
--

LOCK TABLES `areatrigger_involvedrelation` WRITE;
/*!40000 ALTER TABLE `areatrigger_involvedrelation` DISABLE KEYS */;
/*!40000 ALTER TABLE `areatrigger_involvedrelation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `areatrigger_scripts`
--

DROP TABLE IF EXISTS `areatrigger_scripts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `areatrigger_scripts` (
  `entry` mediumint(8) NOT NULL,
  `ScriptName` char(64) NOT NULL,
  PRIMARY KEY (`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Trigger System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `areatrigger_scripts`
--

LOCK TABLES `areatrigger_scripts` WRITE;
/*!40000 ALTER TABLE `areatrigger_scripts` DISABLE KEYS */;
/*!40000 ALTER TABLE `areatrigger_scripts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `areatrigger_tavern`
--

DROP TABLE IF EXISTS `areatrigger_tavern`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `areatrigger_tavern` (
  `id` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'Identifier',
  `name` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='CTDB Trigger System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `areatrigger_tavern`
--

LOCK TABLES `areatrigger_tavern` WRITE;
/*!40000 ALTER TABLE `areatrigger_tavern` DISABLE KEYS */;
/*!40000 ALTER TABLE `areatrigger_tavern` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `areatrigger_teleport`
--

DROP TABLE IF EXISTS `areatrigger_teleport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `areatrigger_teleport` (
  `id` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'Identifier',
  `name` text,
  `target_map` smallint(5) unsigned NOT NULL DEFAULT '0',
  `target_position_x` float NOT NULL DEFAULT '0',
  `target_position_y` float NOT NULL DEFAULT '0',
  `target_position_z` float NOT NULL DEFAULT '0',
  `target_orientation` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  FULLTEXT KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='CTDB Trigger System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `areatrigger_teleport`
--

LOCK TABLES `areatrigger_teleport` WRITE;
/*!40000 ALTER TABLE `areatrigger_teleport` DISABLE KEYS */;
/*!40000 ALTER TABLE `areatrigger_teleport` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `autobroadcast`
--

DROP TABLE IF EXISTS `autobroadcast`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `autobroadcast` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Define a broadcast message.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `autobroadcast`
--

LOCK TABLES `autobroadcast` WRITE;
/*!40000 ALTER TABLE `autobroadcast` DISABLE KEYS */;
/*!40000 ALTER TABLE `autobroadcast` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `battleground_template`
--

DROP TABLE IF EXISTS `battleground_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `battleground_template` (
  `id` mediumint(8) unsigned NOT NULL,
  `MinPlayersPerTeam` smallint(5) unsigned NOT NULL DEFAULT '0',
  `MaxPlayersPerTeam` smallint(5) unsigned NOT NULL DEFAULT '0',
  `MinLvl` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `MaxLvl` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `AllianceStartLoc` mediumint(8) unsigned NOT NULL,
  `AllianceStartO` float NOT NULL,
  `HordeStartLoc` mediumint(8) unsigned NOT NULL,
  `HordeStartO` float NOT NULL,
  `Weight` tinyint(2) unsigned NOT NULL DEFAULT '1',
  `ScriptName` char(64) NOT NULL DEFAULT '',
  `Comment` char(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB All battleground system.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `battleground_template`
--

LOCK TABLES `battleground_template` WRITE;
/*!40000 ALTER TABLE `battleground_template` DISABLE KEYS */;
INSERT INTO `battleground_template` VALUES (1,20,40,51,85,611,3.16312,610,0.715504,1,'','Alterac Valley'),(2,5,10,10,85,769,3.14159,770,0.151581,1,'','Warsong Gulch'),(3,8,15,20,85,890,3.91571,889,0.813671,1,'','Arathi Basin'),(7,8,15,61,85,1103,3.03123,1104,0.055761,1,'','Eye of The Storm'),(4,0,2,10,85,929,0,936,3.14159,1,'','Nagrand Arena'),(5,0,2,10,85,939,0,940,3.14159,1,'','Blades\'s Edge Arena'),(6,0,2,10,85,0,0,0,0,1,'','All Arena'),(8,0,2,10,85,1258,0,1259,3.14159,1,'','Ruins of Lordaeron'),(9,7,15,71,85,1367,0,1368,0,1,'','Strand of the Ancients'),(10,5,5,10,85,1362,0,1363,0,1,'','Dalaran Sewers'),(11,5,5,10,85,1364,0,1365,0,1,'','The Ring of Valor'),(30,20,40,71,85,1485,0,1486,3.16124,1,'','Isle of Conquest'),(32,10,10,0,85,0,0,0,0,1,'','Random battleground'),(108,10,10,1,85,1726,0,1727,0,1,'','Twin Peaks'),(120,5,10,1,85,1740,0,1739,0,1,'','The Battle for Gilneas');
/*!40000 ALTER TABLE `battleground_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `battlemaster_entry`
--

DROP TABLE IF EXISTS `battlemaster_entry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `battlemaster_entry` (
  `entry` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'Entry of a creature',
  `bg_template` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'Battleground template id',
  PRIMARY KEY (`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `battlemaster_entry`
--

LOCK TABLES `battlemaster_entry` WRITE;
/*!40000 ALTER TABLE `battlemaster_entry` DISABLE KEYS */;
/*!40000 ALTER TABLE `battlemaster_entry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `character_banned`
--

DROP TABLE IF EXISTS `character_banned`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `character_banned` (
  `guid` int(11) NOT NULL DEFAULT '0' COMMENT 'Account id',
  `bandate` bigint(40) NOT NULL DEFAULT '0',
  `unbandate` bigint(40) NOT NULL DEFAULT '0',
  `bannedby` varchar(50) NOT NULL,
  `banreason` varchar(255) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`guid`,`bandate`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='CTDB Ban List';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `character_banned`
--

LOCK TABLES `character_banned` WRITE;
/*!40000 ALTER TABLE `character_banned` DISABLE KEYS */;
/*!40000 ALTER TABLE `character_banned` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `command`
--

DROP TABLE IF EXISTS `command`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `command` (
  `name` varchar(50) NOT NULL DEFAULT '',
  `security` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `help` longtext,
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='Chat System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `command`
--

LOCK TABLES `command` WRITE;
/*!40000 ALTER TABLE `command` DISABLE KEYS */;
INSERT INTO `command` VALUES ('account create',4,'Syntax: .account create $account $password\r\n\r\nCreate account and set password to it.'),('account delete',4,'Syntax: .account delete $account\r\n\r\nDelete account with all characters.'),('account lock',1,'Syntax: .account lock [on|off]\r\n\r\nAllow login from account only from current used IP or remove this requirement.'),('account onlinelist',4,'Syntax: .account onlinelist\r\n\r\nShow list of online accounts.'),('account password',0,'Syntax: .account password $old_password $new_password $new_password\r\n\r\nChange your account password.'),('account set addon',3,'Syntax: .account set addon [$account] #addon\r\n\r\nSet user (possible targeted) expansion addon level allowed. Addon values: 0 - normal, 1 - tbc, 2 - wotlk.'),('account set gmlevel',4,'Syntax: .account set gmlevel [$account] #level [#realmid]\r\n\r\nSet the security level for targeted player (can\'t be used at self) or for account $name to a level of #level on the realm #realmID.\r\n\r\n#level may range from 0 to 3.\r\n\r\n#reamID may be -1 for all realms.'),('account set password',4,'Syntax: .account set password $account $password $password\r\n\r\nSet password for account.'),('account set',3,'Syntax: .account set $subcommand\nType .account set to see the list of possible subcommands or .help account set $subcommand to see info on subcommands'),('account',1,'Syntax: .account\r\n\r\nDisplay the access level of your account.'),('additem',3,'Syntax: .additem #itemid/[#itemname]/#shift-click-item-link #itemcount\r\n\r\nAdds the specified number of items of id #itemid (or exact (!) name $itemname in brackets, or link created by shift-click at item in inventory or recipe) to your or selected character inventory. If #itemcount is omitted, only one item will be added.\r\n.'),('additemset',3,'Syntax: .additemset #itemsetid\r\n\r\nAdd items from itemset of id #itemsetid to your or selected character inventory. Will add by one example each item from itemset.'),('announce',1,'Syntax: .announce $MessageToBroadcast\r\n\r\nSend a global message to all players online in chat log.'),('aura',3,'Syntax: .aura #spellid\r\n\r\nAdd the aura from spell #spellid to the selected Unit.'),('ban account',3,'Syntax: .ban account $Name $bantime $reason\r\nBan account kick player.\r\n$bantime: negative value leads to permban, otherwise use a timestring like \"4d20h3s\".'),('ban character',3,'Syntax: .ban character $Name $bantime $reason\nBan character and kick player.\n$bantime: negative value leads to permban, otherwise use a timestring like \"4d20h3s\".'),('ban ip',3,'Syntax: .ban ip $Ip $bantime $reason\r\nBan IP.\r\n$bantime: negative value leads to permban, otherwise use a timestring like \"4d20h3s\".'),('ban',3,'Syntax: .ban $subcommand\nType .ban to see the list of possible subcommands or .help ban $subcommand to see info on subcommands'),('baninfo account',3,'Syntax: .baninfo account $accountid\r\nWatch full information about a specific ban.'),('baninfo character',3,'Syntax: .baninfo character $charactername \r\nWatch full information about a specific ban.'),('baninfo ip',3,'Syntax: .baninfo ip $ip\r\nWatch full information about a specific ban.'),('baninfo',3,'Syntax: .baninfo $subcommand\nType .baninfo to see the list of possible subcommands or .help baninfo $subcommand to see info on subcommands'),('bank',3,'Syntax: .bank\r\n\r\nShow your bank inventory.'),('banlist account',3,'Syntax: .banlist account [$Name]\r\nSearches the banlist for a account name pattern or show full list account bans.'),('banlist character',3,'Syntax: .banlist character $Name\r\nSearches the banlist for a character name pattern. Pattern required.'),('banlist ip',3,'Syntax: .banlist ip [$Ip]\r\nSearches the banlist for a IP pattern or show full list of IP bans.'),('banlist',3,'Syntax: .banlist $subcommand\nType .banlist to see the list of possible subcommands or .help banlist $subcommand to see info on subcommands'),('bindsight',3,'Syntax: .bindsight\r\n\r\nBinds vision to the selected unit indefinitely. Cannot be used while currently possessing a target.'),('cast back',3,'Syntax: .cast back #spellid [triggered]\r\n  Selected target will cast #spellid to your character. If \'trigered\' or part provided then spell casted with triggered flag.'),('cast dist',3,'Syntax: .cast dist #spellid [#dist [triggered]]\r\n  You will cast spell to pint at distance #dist. If \'trigered\' or part provided then spell casted with triggered flag. Not all spells can be casted as area spells.'),('cast self',3,'Syntax: .cast self #spellid [triggered]\r\nCast #spellid by target at target itself. If \'trigered\' or part provided then spell casted with triggered flag.'),('cast target',3,'Syntax: .cast target #spellid [triggered]\r\n  Selected target will cast #spellid to his victim. If \'trigered\' or part provided then spell casted with triggered flag.'),('cast',3,'Syntax: .cast #spellid [triggered]\r\n  Cast #spellid to selected target. If no target selected cast to self. If \'trigered\' or part provided then spell casted with triggered flag.'),('character customize',2,'Syntax: .character customize [$name]\r\n\r\nMark selected in game or by $name in command character for customize at next login.'),('character erase',4,'Syntax: .character erase $name\r\n\r\nDelete character $name. Character finally deleted in case any deleting options.'),('character level',3,'Syntax: .character level [$playername] [#level]\r\n\r\nSet the level of character with $playername (or the selected if not name provided) by #numberoflevels Or +1 if no #numberoflevels provided). If #numberoflevels is omitted, the level will be increase by 1. If #numberoflevels is 0, the same level will be restarted. If no character is selected and name not provided, increase your level. Command can be used for offline character. All stats and dependent values recalculated. At level decrease talents can be reset if need. Also at level decrease equipped items with greater level requirement can be lost.'),('character rename',2,'Syntax: .character rename [$name]\r\n\r\nMark selected in game or by $name in command character for rename at next login.'),('character reputation',2,'Syntax: .character reputation [$player_name]\r\n\r\nShow reputation information for selected player or player find by $player_name.'),('combatstop',2,'Syntax: .combatstop [$playername]\r\nStop combat for selected character. If selected non-player then command applied to self. If $playername provided then attempt applied to online player $playername.'),('cometome',3,'SYntax: .cometome $parameter\nMake selected creature come to your current location (new position not saved to DB).'),('commands',0,'Syntax: .commands\r\n\r\nDisplay a list of available commands for your account level.'),('cooldown',3,'Syntax: .cooldown [#spell_id]\r\n\r\nRemove all (if spell_id not provided) or #spel_id spell cooldown from selected character or you (if no selection).'),('damage',3,'Syntax: .damage $damage_amount [$school [$spellid]]\r\n\r\nApply $damage to target. If not $school and $spellid provided then this flat clean melee damage without any modifiers. If $school provided then damage modified by armor reduction (if school physical), and target absorbing modifiers and result applied as melee damage to target. If spell provided then damage modified and applied as spell damage. $spellid can be shift-link.'),('debug arena',3,'Syntax: .debug arena\r\n\r\nToggle debug mode for arenas. In debug mode GM can start arena with single player.'),('debug bg',3,'Syntax: .debug bg\r\n\r\nToggle debug mode for battlegrounds. In debug mode GM can start battleground with single player.'),('debug Mod32Value',3,'Syntax: .debug Mod32Value #field #value\r\n\r\nAdd #value to field #field of your character.'),('debug play cinematic',1,'Syntax: .debug play cinematic #cinematicid\r\n\r\nPlay cinematic #cinematicid for you. You stay at place while your mind fly.\r\n'),('debug play movie',1,'Syntax: .debug play movie #movieid\r\n\r\nPlay movie #movieid for you.'),('debug play sound',1,'Syntax: .debug play sound #soundid\r\n\r\nPlay sound with #soundid.\r\nSound will be play only for you. Other players do not hear this.\r\nWarning: client may have more 5000 sounds...'),('debug',1,'Syntax: .debug $subcommand\nType .debug to see the list of possible subcommands or .help debug $subcommand to see info on subcommands'),('demorph',2,'Syntax: .demorph\r\n\r\nDemorph the selected player.'),('die',3,'Syntax: .die\r\n\r\nKill the selected player. If no player is selected, it will kill you.'),('dismount',1,'Syntax: .dismount\r\n\r\nDismount you, if you are mounted.'),('distance',3,'Syntax: .distance\r\n\r\nDisplay the distance from your character to the selected creature.'),('event activelist',2,'Syntax: .event activelist\r\nShow list of currently active events.'),('event start',2,'Syntax: .event start #event_id\r\nStart event #event_id. Set start time for event to current moment (change not saved in DB).'),('event stop',2,'Syntax: .event stop #event_id\r\nStop event #event_id. Set start time for event to time in past that make current moment is event stop time (change not saved in DB).'),('event',2,'Syntax: .event #event_id\r\nShow details about event with #event_id.'),('explorecheat',3,'Syntax: .explorecheat #flag\r\n\r\nReveal  or hide all maps for the selected player. If no player is selected, hide or reveal maps to you.\r\n\r\nUse a #flag of value 1 to reveal, use a #flag value of 0 to hide all maps.'),('flusharenapoints',3,'Syntax: .flusharenapoints\r\n\r\nUse it to distribute arena points based on arena team ratings, and start a new week.'),('freeze',1,'Syntax: .freeze (#player)\r\n\"Freezes\" #player and disables his chat. When using this without #name it will freeze your target.'),('gm chat',1,'Syntax: .gm chat [on/off]\r\n\r\nEnable or disable chat GM MODE (show gm badge in messages) or show current state of on/off not provided.'),('gm fly',3,'Syntax: .gm fly [on/off]\r\nEnable/disable gm fly mode.'),('gm ingame',1,'Syntax: .gm ingame\r\n\r\nDisplay a list of available in game Game Masters.'),('gm list',3,'Syntax: .gm list\r\n\r\nDisplay a list of all Game Masters accounts and security levels.'),('gm visible',1,'Syntax: .gm visible on/off\r\n\r\nOutput current visibility state or make GM visible(on) and invisible(off) for other players.'),('gm',1,'Syntax: .gm [on/off]\r\n\r\nEnable or Disable in game GM MODE or show current state of on/off not provided.'),('gmannounce',1,'Syntax: .gmannounce $announcement\r\nSend an announcement to online Gamemasters.'),('gmnameannounce',1,'Syntax: .gmnameannounce $announcement.\r\nSend an announcement to all online GM\'s, displaying the name of the sender.'),('gmnotify',1,'Syntax: .gmnotify $notification\r\nDisplays a notification on the screen of all online GM\'s.'),('go creature',1,'Syntax: .go creature #creature_guid\r\nTeleport your character to creature with guid #creature_guid.\r\n.gocreature #creature_name\r\nTeleport your character to creature with this name.\r\n.gocreature id #creature_id\r\nTeleport your character to a creature that was spawned from the template with this entry.\r\n*If* more than one creature is found, then you are teleported to the first that is found inside the database.'),('go graveyard',1,'Syntax: .go graveyard #graveyardId\r\n Teleport to graveyard with the graveyardId specified.'),('go grid',1,'Syntax: .go grid #gridX #gridY [#mapId]\r\n\r\nTeleport the gm to center of grid with provided indexes at map #mapId (or current map if it not provided).'),('go object',1,'Syntax: .go object #object_guid\r\nTeleport your character to gameobject with guid #object_guid'),('go taxinode',1,'Syntax: .go taxinode #taxinode\r\n\r\nTeleport player to taxinode coordinates. You can look up zone using .lookup taxinode $namepart'),('go ticket',1,'Syntax: .go ticket #ticketid\r\nTeleports the user to the location where $ticketid was created.'),('go trigger',1,'Syntax: .go trigger #trigger_id\r\n\r\nTeleport your character to areatrigger with id #trigger_id. Character will be teleported to trigger target if selected areatrigger is telporting trigger.'),('go xy',1,'Syntax: .go xy #x #y [#mapid]\r\n\r\nTeleport player to point with (#x,#y) coordinates at ground(water) level at map #mapid or same map if #mapid not provided.'),('go xyz',1,'Syntax: .go xyz #x #y #z [#mapid]\r\n\r\nTeleport player to point with (#x,#y,#z) coordinates at ground(water) level at map #mapid or same map if #mapid not provided.'),('go zonexy',1,'Syntax: .go zonexy #x #y [#zone]\r\n\r\nTeleport player to point with (#x,#y) client coordinates at ground(water) level in zone #zoneid or current zone if #zoneid not provided. You can look up zone using .lookup area $namepart'),('go',1,'Syntax: .go $subcommand\nType .go to see the list of possible subcommands or .help go $subcommand to see info on subcommands'),('gobject activate',2,'Syntax: .gobject activate #guid\r\n\r\nActivates an object like a door or a button.'),('gobject add',2,'Syntax: .gobject add #id <spawntimeSecs>\r\n\r\nAdd a game object from game object templates to the world at your current location using the #id.\r\nspawntimesecs sets the spawntime, it is optional.\r\n\r\nNote: this is a copy of .gameobject.'),('gobject delete',2,'Syntax: .gobject delete #go_guid\r\nDelete gameobject with guid #go_guid.'),('gobject move',2,'Syntax: .gobject move #goguid [#x #y #z]\r\n\r\nMove gameobject #goguid to character coordinates (or to (#x,#y,#z) coordinates if its provide).'),('gobject near',2,'Syntax: .gobject near  [#distance]\r\n\r\nOutput gameobjects at distance #distance from player. Output gameobject guids and coordinates sorted by distance from character. If #distance not provided use 10 as default value.'),('gobject add temp',2,'Adds a temporary gameobject that is not saved to DB.'),('gobject target',2,'Syntax: .gobject target [#go_id|#go_name_part]\r\n\r\nLocate and show position nearest gameobject. If #go_id or #go_name_part provide then locate and show position of nearest gameobject with gameobject template id #go_id or name included #go_name_part as part.'),('gobject set phase',2,'Syntax: .gobject set phase #guid #phasemask\r\n\r\nGameobject with DB guid #guid phasemask changed to #phasemask with related world vision update for players. Gameobject state saved to DB and persistent.'),('gobject turn',2,'Syntax: .gobject turn #goguid \r\n\r\nSet for gameobject #goguid orientation same as current character orientation.'),('gobject',2,'Syntax: .gobject $subcommand\nType .gobject to see the list of possible subcommands or .help gobject $subcommand to see info on subcommands'),('summon',1,'Syntax: .summon [$charactername]\r\n\r\nTeleport the given character to you. Character can be offline.'),('gps',1,'Syntax: .gps [$name|$shift-link]\r\n\r\nDisplay the position information for a selected character or creature (also if player name $name provided then for named player, or if creature/gameobject shift-link provided then pointed creature/gameobject if it loaded). Position information includes X, Y, Z, and orientation, map Id and zone Id'),('groupsummon',1,'Syntax: .groupsummon [$charactername]\r\n\r\nTeleport the given character and his group to you. Teleported only online characters but original selected group member can be offline.'),('guid',2,'Syntax: .guid\r\n\r\nDisplay the GUID for the selected character.'),('guild create',2,'Syntax: .guild create [$GuildLeaderName] \"$GuildName\"\r\n\r\nCreate a guild named $GuildName with the player $GuildLeaderName (or selected) as leader.  Guild name must in quotes.'),('guild delete',2,'Syntax: .guild delete \"$GuildName\"\r\n\r\nDelete guild $GuildName. Guild name must in quotes.'),('guild invite',2,'Syntax: .guild invite [$CharacterName] \"$GuildName\"\r\n\r\nAdd player $CharacterName (or selected) into a guild $GuildName. Guild name must in quotes.'),('guild rank',2,'Syntax: .guild rank [$CharacterName] #Rank\r\n\r\nSet for player $CharacterName (or selected) rank #Rank in a guild.'),('guild uninvite',2,'Syntax: .guild uninvite [$CharacterName]\r\n\r\nRemove player $CharacterName (or selected) from a guild.'),('guild',3,'Syntax: .guild $subcommand\nType .guild to see the list of possible subcommands or .help guild $subcommand to see info on subcommands'),('help',1,'Syntax: .help [$command]\r\n\r\nDisplay usage instructions for the given $command. If no $command provided show list available commands.'),('hidearea',3,'Syntax: .hidearea #areaid\r\n\r\nHide the area of #areaid to the selected character. If no character is selected, hide this area to you.'),('honor add',2,'Syntax: .honor add $amount\r\n\r\nAdd a certain amount of honor (gained today) to the selected player.'),('honor add kill',2,'Syntax: .honor add kill\r\n\r\nAdd the targeted unit as one of your pvp kills today (you only get honor if it\'s a racial leader or a player)'),('honor update',2,'Syntax: .honor update\r\n\r\nForce the yesterday\'s honor fields to be updated with today\'s data, which will get reset for the selected player.'),('honor',2,'Syntax: .honor $subcommand\nType .honor to see the list of possible subcommands or .help honor $subcommand to see info on subcommands'),('hover',3,'Syntax: .hover #flag\r\n\r\nEnable or disable hover mode for your character.\r\n\r\nUse a #flag of value 1 to enable, use a #flag value of 0 to disable hover.'),('instance listbinds',3,'Syntax: .instance listbinds\r\n  Lists the binds of the selected player.'),('instance savedata',3,'Syntax: .instance savedata\r\n  Save the InstanceData for the current player\'s map to the DB.'),('instance stats',3,'Syntax: .instance stats\r\n  Shows statistics about instances.'),('instance unbind',3,'Syntax: .instance unbind <mapid|all> [difficulty]\r\n  Clear all/some of player\'s binds'),('instance',3,'Syntax: .instance $subcommand\nType .instance to see the list of possible subcommands or .help instance $subcommand to see info on subcommands'),('itemmove',2,'Syntax: .itemmove #sourceslotid #destinationslotid\r\n\r\nMove an item from slots #sourceslotid to #destinationslotid in your inventory\r\n\r\nNot yet implemented'),('kick',2,'Syntax: .kick [$charactername] [$reason]\r\n\r\nKick the given character name from the world with or without reason. If no character name is provided then the selected player (except for yourself) will be kicked. If no reason is provided, default is \"No Reason\".'),('learn all',3,'Syntax: .learn all\r\n\r\nLearn all big set different spell maybe useful for Administaror.'),('learn all_crafts',2,'Syntax: .learn crafts\r\n\r\nLearn all professions and recipes.'),('learn all_default',1,'Syntax: .learn all_default [$playername]\r\n\r\nLearn for selected/$playername player all default spells for his race/class and spells rewarded by completed quests.'),('learn all_gm',2,'Syntax: .learn all_gm\r\n\r\nLearn all default spells for Game Masters.'),('learn all_lang',1,'Syntax: .learn all_lang\r\n\r\nLearn all languages'),('learn all_myclass',3,'Syntax: .learn all_myclass\r\n\r\nLearn all spells and talents available for his class.'),('learn all_mypettalents',3,'Syntax: .learn all_mypettalents\r\n\r\nLearn all talents for your pet available for his creature type (only for hunter pets).'),('learn all_myspells',3,'Syntax: .learn all_myspells\r\n\r\nLearn all spells (except talents and spells with first rank learned as talent) available for his class.'),('learn all_mytalents',3,'Syntax: .learn all_mytalents\r\n\r\nLearn all talents (and spells with first rank learned as talent) available for his class.'),('learn all_recipes',2,'Syntax: .learn all_recipes [$profession]\r\rLearns all recipes of specified profession and sets skill level to max.\rExample: .learn all_recipes enchanting'),('learn',3,'Syntax: .learn #spell [all]\r\n\r\nSelected character learn a spell of id #spell. If \'all\' provided then all ranks learned.'),('levelup',3,'Syntax: .levelup [$playername] [#numberoflevels]\r\n\r\nIncrease/decrease the level of character with $playername (or the selected if not name provided) by #numberoflevels Or +1 if no #numberoflevels provided). If #numberoflevels is omitted, the level will be increase by 1. If #numberoflevels is 0, the same level will be restarted. If no character is selected and name not provided, increase your level. Command can be used for offline character. All stats and dependent values recalculated. At level decrease talents can be reset if need. Also at level decrease equipped items with greater level requirement can be lost.'),('linkgrave',3,'Syntax: .linkgrave #graveyard_id [alliance|horde]\r\n\r\nLink current zone to graveyard for any (or alliance/horde faction ghosts). This let character ghost from zone teleport to graveyard after die if graveyard is nearest from linked to zone and accept ghost of this faction. Add only single graveyard at another map and only if no graveyards linked (or planned linked at same map).'),('list auras',3,'Syntax: .list auras\nList auras (passive and active) of selected creature or player. If no creature or player is selected, list your own auras.'),('list creature',3,'Syntax: .list creature #creature_id [#max_count]\r\n\r\nOutput creatures with creature id #creature_id found in world. Output creature guids and coordinates sorted by distance from character. Will be output maximum #max_count creatures. If #max_count not provided use 10 as default value.'),('list item',3,'Syntax: .list item #item_id [#max_count]\r\n\r\nOutput items with item id #item_id found in all character inventories, mails, auctions, and guild banks. Output item guids, item owner guid, owner account and owner name (guild name and guid in case guild bank). Will be output maximum #max_count items. If #max_count not provided use 10 as default value.'),('list object',3,'Syntax: .list object #gameobject_id [#max_count]\r\n\r\nOutput gameobjects with gameobject id #gameobject_id found in world. Output gameobject guids and coordinates sorted by distance from character. Will be output maximum #max_count gameobject. If #max_count not provided use 10 as default value.'),('list',3,'Syntax: .list $subcommand\nType .list to see the list of possible subcommands or .help list $subcommand to see info on subcommands'),('listfreeze',1,'Syntax: .listfreeze\r\n\r\nSearch and output all frozen players.'),('wp reload',3,'Syntax: .wp reload $pathid\nLoad path changes ingame - IMPORTANT: must be applied first for new paths before .wp load #pathid '),('reload spell_group',3,'Syntax: .reload spell_group\nReload spell_group table.'),('lookup area',1,'Syntax: .lookup area $namepart\r\n\r\nLooks up an area by $namepart, and returns all matches with their area ID\'s.'),('lookup creature',3,'Syntax: .lookup creature $namepart\r\n\r\nLooks up a creature by $namepart, and returns all matches with their creature ID\'s.'),('lookup event',2,'Syntax: .lookup event $name\r\nAttempts to find the ID of the event with the provided $name.'),('lookup faction',3,'Syntax: .lookup faction $name\r\nAttempts to find the ID of the faction with the provided $name.'),('lookup item',3,'Syntax: .lookup item $itemname\r\n\r\nLooks up an item by $itemname, and returns all matches with their Item ID\'s.'),('lookup itemset',3,'Syntax: .lookup itemset $itemname\r\n\r\nLooks up an item set by $itemname, and returns all matches with their Item set ID\'s.'),('lookup map',3,'Syntax: .lookup map $namepart\r\n\r\nLooks up a map by $namepart, and returns all matches with their map ID\'s.'),('lookup object',3,'Syntax: .lookup object $objname\r\n\r\nLooks up an gameobject by $objname, and returns all matches with their Gameobject ID\'s.'),('lookup player account',2,'Syntax: .lookup player account $account ($limit) \r\n\r\n Searchs players, which account username is $account with optional parametr $limit of results.'),('lookup player email',2,'Syntax: .lookup player email $email ($limit) \r\n\r\n Searchs players, which account email is $email with optional parametr $limit of results.'),('lookup player ip',2,'Syntax: .lookup player ip $ip ($limit) \r\n\r\n Searchs players, which account ast_ip is $ip with optional parametr $limit of results.'),('lookup quest',3,'Syntax: .lookup quest $namepart\r\n\r\nLooks up a quest by $namepart, and returns all matches with their quest ID\'s.'),('lookup skill',3,'Syntax: .lookup skill $$namepart\r\n\r\nLooks up a skill by $namepart, and returns all matches with their skill ID\'s.'),('lookup spell',3,'Syntax: .lookup spell $namepart\r\n\r\nLooks up a spell by $namepart, and returns all matches with their spell ID\'s.'),('lookup taxinode',3,'Syntax: .lookup taxinode $substring\r\n\r\nSearch and output all taxinodes with provide $substring in name.'),('lookup tele',1,'Syntax: .lookup tele $substring\r\n\r\nSearch and output all .tele command locations with provide $substring in name.'),('lookup',3,'Syntax: .lookup $subcommand\nType .lookup to see the list of possible subcommands or .help lookup $subcommand to see info on subcommands'),('maxskill',3,'Syntax: .maxskill\r\nSets all skills of the targeted player to their maximum values for its current level.'),('modify aspeed',1,'Syntax: .modify aspeed #rate\r\n\r\nModify all speeds -run,swim,run back,swim back- of the selected player to \"normalbase speed for this move type\"*rate. If no player is selected, modify your speed.\r\n\r\n #rate may range from 0.1 to 50.'),('modify bit',1,'Syntax: .modify bit #field #bit\r\n\r\nToggle the #bit bit of the #field field for the selected player. If no player is selected, modify your character.'),('modify bwalk',1,'Syntax: .modify bwalk #rate\r\n\r\nModify the speed of the selected player while running backwards to \"normal walk back speed\"*rate. If no player is selected, modify your speed.\r\n\r\n #rate may range from 0.1 to 50.'),('modify drunk',1,'Syntax: .modify drunk #value\r\n Set drunk level to #value (0..100). Value 0 remove drunk state, 100 is max drunked state.'),('modify energy',1,'Syntax: .modify energy #energy\r\n\r\nModify the energy of the selected player. If no player is selected, modify your energy.'),('modify faction',1,'Syntax: .modify faction #factionid #flagid #npcflagid #dynamicflagid\r\n\r\nModify the faction and flags of the selected creature. Without arguments, display the faction and flags of the selected creature.'),('modify fly',1,'.modify fly $parameter\nModify the flying speed of the selected player to \"normal flying speed\"*rate. If no player is selected, modify your speed.\n #rate may range from 0.1 to 50.'),('modify gender',2,'Syntax: .modify gender male/female\r\n\r\nChange gender of selected player.'),('modify hp',1,'Syntax: .modify hp #newhp\r\n\r\nModify the hp of the selected player. If no player is selected, modify your hp.'),('modify mana',1,'Syntax: .modify mana #newmana\r\n\r\nModify the mana of the selected player. If no player is selected, modify your mana.'),('modify money',1,'Syntax: .modify money #money\r\n.money #money\r\n\r\nAdd or remove money to the selected player. If no player is selected, modify your money.\r\n\r\n #gold can be negative to remove money.'),('modify morph',2,'Syntax: .modify morph #displayid\r\n\r\nChange your current model id to #displayid.'),('modify mount',1,'Syntax: .modify mount #id #speed\r\nDisplay selected player as mounted at #id creature and set speed to #speed value.'),('modify phase',3,'Syntax: .modify phase #phasemask\r\n\r\nSelected character phasemask changed to #phasemask with related world vision update. Change active until in game phase changed, or GM-mode enable/disable, or re-login. Character pts pasemask update to same value.'),('modify rage',1,'Syntax: .modify rage #newrage\r\n\r\nModify the rage of the selected player. If no player is selected, modify your rage.'),('modify rep',2,'Syntax: .modify rep #repId (#repvalue | $rankname [#delta])\r\nSets the selected players reputation with faction #repId to #repvalue or to $reprank.\r\nIf the reputation rank name is provided, the resulting reputation will be the lowest reputation for that rank plus the delta amount, if specified.\r\nYou can use \'.pinfo rep\' to list all known reputation ids, or use \'.lookup faction $name\' to locate a specific faction id.'),('modify runicpower',1,'Syntax: .modify runicpower #newrunicpower\r\n\r\nModify the runic power of the selected player. If no player is selected, modify your runic power.'),('modify scale',1,'.modify scale $parameter\nModify size of the selected player to \"normal scale\"*rate. If no player is selected, modify your size.\n#rate may range from 0.1 to 10.'),('modify speed',1,'Syntax: .modify speed #rate\r\n.speed #rate\r\n\r\nModify the running speed of the selected player to \"normal base run speed\"*rate. If no player is selected, modify your speed.\r\n\r\n #rate may range from 0.1 to 50.'),('modify spell',1,'TODO'),('modify standstate',2,'Syntax: .modify standstate #emoteid\r\n\r\nChange the emote of your character while standing to #emoteid.'),('modify swim',1,'Syntax: .modify swim #rate\r\n\r\nModify the swim speed of the selected player to \"normal swim speed\"*rate. If no player is selected, modify your speed.\r\n\r\n #rate may range from 0.1 to 50.'),('character titles',2,'Syntax: .character titles [$player_name]\r\n\r\nShow known titles list for selected player or player find by $player_name.'),('modify tp',1,'Syntax: .modify tp #amount\r\n\r\nSet free talent pointes for selected character or character\'s pet. It will be reset to default expected at next levelup/login/quest reward.'),('modify',1,'Syntax: .modify $subcommand\nType .modify to see the list of possible subcommands or .help modify $subcommand to see info on subcommands'),('movegens',3,'Syntax: .movegens\r\n  Show movement generators stack for selected creature or player.'),('mute',1,'Syntax: .mute [$playerName] $timeInMinutes [$reason]\r\n\r\nDisible chat messaging for any character from account of character $playerName (or currently selected) at $timeInMinutes minutes. Player can be offline.'),('nameannounce',1,'Syntax: .nameannounce $announcement.\nSend an announcement to all online players, displaying the name of the sender.'),('appear',1,'Syntax: .appear [$charactername]\r\n\r\nTeleport to the given character. Either specify the character name or click on the character\'s portrait,e.g. when you are in a group. Character can be offline.'),('neargrave',3,'Syntax: .neargrave [alliance|horde]\r\n\r\nFind nearest graveyard linked to zone (or only nearest from accepts alliance or horde faction ghosts).'),('notify',1,'Syntax: .notify $MessageToBroadcast\r\n\r\nSend a global message to all players online in screen.'),('npc add',2,'Syntax: .npc add #creatureid\r\n\r\nSpawn a creature by the given template id of #creatureid.'),('npc delete',2,'Syntax: .npc delete [#guid]\r\n\r\nDelete creature with guid #guid (or the selected if no guid is provided)'),('npc follow',2,'Syntax: .npc follow\r\n\r\nSelected creature start follow you until death/fight/etc.'),('npc info',3,'Syntax: .npc info\r\n\r\nDisplay a list of details for the selected creature.\r\n\r\nThe list includes:\r\n- GUID, Faction, NPC flags, Entry ID, Model ID,\r\n- Level,\r\n- Health (current/maximum),\r\n\r\n- Field flags, dynamic flags, faction template, \r\n- Position information,\r\n- and the creature type, e.g. if the creature is a vendor.'),('npc move',2,'Syntax: .npc move [#creature_guid]\r\n\r\nMove the targeted creature spawn point to your coordinates.'),('npc playemote',3,'Syntax: .npc playemote #emoteid\r\n\r\nMake the selected creature emote with an emote of id #emoteid.'),('npc say',1,'Syntax: .npc say $message\nMake selected creature say specified message.'),('npc textemote',1,'Syntax: .npc textemote #emoteid\r\n\r\nMake the selected creature to do textemote with an emote of id #emoteid.'),('npc whisper',1,'Syntax: .npc whisper #playerguid #text\r\nMake the selected npc whisper #text to  #playerguid.'),('npc yell',1,'Syntax: .npc yell $message\nMake selected creature yell specified message.'),('npc',1,'Syntax: .npc $subcommand\nType .npc to see the list of possible subcommands or .help npc $subcommand to see info on subcommands'),('pdump load',3,'Syntax: .pdump load $filename $account [$newname] [$newguid]\r\nLoad character dump from dump file into character list of $account with saved or $newname, with saved (or first free) or $newguid guid.'),('pdump write',3,'Syntax: .pdump write $filename $playerNameOrGUID\r\nWrite character dump with name/guid $playerNameOrGUID to file $filename.'),('pdump',3,'Syntax: .pdump $subcommand\nType .pdump to see the list of possible subcommands or .help pdump $subcommand to see info on subcommands'),('pet create',2,'Syntax: .pet create\r\n\r\nCreates a pet of the selected creature.'),('pet learn',2,'Syntax: .pet learn\r\n\r\nLearn #spellid to pet.'),('pet tp',2,'Syntax: .pet tp #\r\n\r\nChange pet\'s amount of training points.'),('pet unlearn',2,'Syntax: .pet unlean\r\n\r\nunLearn #spellid to pet.'),('pet',2,'Syntax: .pet $subcommand\nType .pet to see the list of possible subcommands or .help pet $subcommand to see info on subcommands'),('pinfo',2,'Syntax: .pinfo [$player_name]\r\n\r\nOutput account information for selected player or player find by $player_name.'),('playall',2,'Syntax: .playall #soundid\r\n\r\nPlayer a sound to whole server.'),('possess',3,'Syntax: .possess\r\n\r\nPossesses indefinitely the selected creature.'),('quest add',3,'Syntax: .quest add #quest_id\r\n\r\nAdd to character quest log quest #quest_id. Quest started from item can\'t be added by this command but correct .additem call provided in command output.'),('quest complete',3,'Syntax: .quest complete #questid\r\nMark all quest objectives as completed for target character active quest. After this target character can go and get quest reward.'),('quest remove',3,'Syntax: .quest remove #quest_id\r\n\r\nSet quest #quest_id state to not completed and not active (and remove from active quest list) for selected player.'),('quest',3,'Syntax: .quest $subcommand\nType .quest to see the list of possible subcommands or .help quest $subcommand to see info on subcommands'),('recall',1,'Syntax: .recall [$playername]\r\n\r\nTeleport $playername or selected player to the place where he has been before last use of a teleportation command. If no $playername is entered and no player is selected, it will teleport you.'),('reload areatrigger_involvedrelation',3,'Syntax: .reload areatrigger_involvedrelation\nReload areatrigger_involvedrelation table.'),('reload areatrigger_tavern',3,'Syntax: .reload areatrigger_tavern\nReload areatrigger_tavern table.'),('reload areatrigger_teleport',3,'Syntax: .reload areatrigger_teleport\nReload areatrigger_teleport table.'),('reload autobroadcast',3,'Syntax: .reload autobroadcast\nReload autobroadcast table.'),('reload command',3,'Syntax: .reload command\nReload command table.'),('reload config',3,'Syntax: .reload config\r\n\r\nReload config settings (by default stored in trinityd.conf). Not all settings can be change at reload: some new setting values will be ignored until restart, some values will applied with delay or only to new objects/maps, some values will explicitly rejected to change at reload.'),('reload creature_involvedrelation',3,'Syntax: .reload creature_involvedrelation\nReload creature_involvedrelation table.'),('reload creature_linked_respawn',2,'Syntax: .reload creature_linked_respawn\r\nReload creature_linked_respawn table.'),('reload creature_loot_template',3,'Syntax: .reload creature_loot_template\nReload creature_loot_template table.'),('reload creature_questrelation',3,'Syntax: .reload creature_questrelation\nReload creature_questrelation table.'),('reload disenchant_loot_template',3,'Syntax: .reload disenchant_loot_template\nReload disenchant_loot_template table.'),('reload event_scripts',3,'Syntax: .reload event_scripts\nReload event_scripts table.'),('reload fishing_loot_template',3,'Syntax: .reload fishing_loot_template\nReload fishing_loot_template table.'),('reload game_graveyard_zone',3,'Syntax: .reload game_graveyard_zone\nReload game_graveyard_zone table.'),('reload game_tele',3,'Syntax: .reload game_tele\nReload game_tele table.'),('reload gameobject_involvedrelation',3,'Syntax: .reload gameobject_involvedrelation\nReload gameobject_involvedrelation table.'),('reload gameobject_loot_template',3,'Syntax: .reload gameobject_loot_template\nReload gameobject_loot_template table.'),('reload gameobject_questrelation',3,'Syntax: .reload gameobject_questrelation\nReload gameobject_questrelation table.'),('reload gameobject_scripts',3,'Syntax: .reload gameobject_scripts\nReload gameobject_scripts table.'),('reload gm_tickets',3,'Syntax: .reload gm_tickets\nReload gm_tickets table.'),('reload item_enchantment_template',3,'Syntax: .reload item_enchantment_template\nReload item_enchantment_template table.'),('reload item_loot_template',3,'Syntax: .reload item_loot_template\nReload item_loot_template table.'),('reload locales_creature',3,'Syntax: .reload locales_creature\nReload locales_creature table.'),('reload locales_gameobject',3,'Syntax: .reload locales_gameobject\nReload locales_gameobject table.'),('reload locales_item',3,'Syntax: .reload locales_item\nReload locales_item table.'),('reload locales_npc_text',3,'Syntax: .reload locales_npc_text\nReload locales_npc_text table.'),('reload locales_page_text',3,'Syntax: .reload locales_page_text\nReload locales_page_text table.'),('reload locales_points_of_interest',3,'Syntax: .reload locales_points_of_interest\nReload locales_point_of_interest table.'),('reload locales_quest',3,'Syntax: .reload locales_quest\nReload locales_quest table.'),('reload milling_loot_template',3,'Syntax: .reload milling_loot_template\nReload milling_loot_template table.'),('reload npc_trainer',3,'Syntax: .reload npc_trainer\nReload npc_trainer table.'),('reload npc_vendor',3,'Syntax: .reload npc_vendor\nReload npc_vendor table.'),('reload page_text',3,'Syntax: .reload page_text\nReload page_text table.'),('reload pickpocketing_loot_template',3,'Syntax: .reload pickpocketing_loot_template\nReload pickpocketing_loot_template table.'),('reload points_of_interest',3,'Syntax: .reload points_of_interest\nReload points_of_interest table.'),('reload prospecting_loot_template',3,'Syntax: .reload prospecting_loot_template\nReload prospecting_loot_template table.'),('reload quest_end_scripts',3,'Syntax: .reload quest_end_scripts\nReload quest_end_scripts table.'),('reload mail_loot_template',3,'Syntax: .reload quest_mail_loot_template\nReload quest_mail_loot_template table.'),('reload quest_start_scripts',3,'Syntax: .reload quest_start_scripts\nReload quest_start_scripts table.'),('reload quest_template',3,'Syntax: .reload quest_template\nReload quest_template table.'),('reload reference_loot_template',3,'Syntax: .reload reference_loot_template\nReload reference_loot_template table.'),('reload reserved_name',3,'Syntax: .reload reserved_name\nReload reserved_name table.'),('reload skill_discovery_template',3,'Syntax: .reload skill_discovery_template\nReload skill_discovery_template table.'),('reload skill_extra_item_template',3,'Syntax: .reload skill_extra_item_template\nReload skill_extra_item_template table.'),('reload skill_fishing_base_level',3,'Syntax: .reload skill_fishing_base_level\nReload skill_fishing_base_level table.'),('reload skinning_loot_template',3,'Syntax: .reload skinning_loot_template\nReload skinning_loot_template table.'),('reload spell_area',3,'Syntax: .reload spell_area\nReload spell_area table.'),('reload spell_bonus_data',3,'Syntax: .reload spell_bonus_data\nReload spell_bonus_data table.'),('reload disables',3,'Syntax: .reload disables\r\nReload disables table.'),('reload spell_group_stack_rules',3,'Syntax: .reload spell_group\nReload spell_group_stack_rules table.'),('reload spell_learn_spell',3,'Syntax: .reload spell_learn_spell\nReload spell_learn_spell table.'),('reload spell_linked_spell',3,'Usage: .reload spell_linked_spell\r\nReloads the spell_linked_spell DB table.'),('reload spell_loot_template',3,'Syntax: .reload spell_loot_template\nReload spell_loot_template table.'),('reload spell_pet_auras',3,'Syntax: .reload spell_pet_auras\nReload spell_pet_auras table.'),('reload spell_proc_event',3,'Syntax: .reload spell_proc_event\nReload spell_proc_event table.'),('reload spell_required',3,'Syntax: .reload spell_required\nReload spell_required table.'),('reload item_set_names',3,'Syntax: .reload item_set_names\nReload item_set_names table.'),('reload spell_scripts',3,'Syntax: .reload spell_scripts\nReload spell_scripts table.'),('reload spell_target_position',3,'Syntax: .reload spell_target_position\nReload spell_target_position table.'),('reload spell_threats',3,'Syntax: .reload spell_threats\nReload spell_threats table.'),('reload waypoint_scripts',3,'Syntax: .reload waypoint_scripts\nReload waypoint_scripts table.'),('reload',3,'Syntax: .reload $subcommand\nType .reload to see the list of possible subcommands or .help reload $subcommand to see info on subcommands'),('repairitems',2,'Syntax: .repairitems\r\n\r\nRepair all selected player\'s items.'),('reset achievements',3,'Syntax: .reset achievements [$playername]\r\n\r\nReset achievements data for selected or named (online or offline) character. Achievements for persistance progress data like completed quests/etc re-filled at reset. Achievements for events like kills/casts/etc will lost.'),('reset all',3,'Syntax: .reset all spells\r\n\r\nSyntax: .reset all talents\r\n\r\nRequest reset spells or talents (including talents for all character\'s pets if any) at next login each existed character.'),('reset honor',3,'Syntax: .reset honor [Playername]\r\n  Reset all honor data for targeted character.'),('reset level',3,'Syntax: .reset level [Playername]\r\n  Reset level to 1 including reset stats and talents.  Equipped items with greater level requirement can be lost.'),('reset spells',3,'Syntax: .reset spells [Playername]\r\n  Removes all non-original spells from spellbook.\r\n. Playername can be name of offline character.'),('reset stats',3,'Syntax: .reset stats [Playername]\r\n  Resets(recalculate) all stats of the targeted player to their original VALUESat current level.'),('reset talents',3,'Syntax: .reset talents [Playername]\r\n  Removes all talents of the targeted player or pet or named player. Playername can be name of offline character. With player talents also will be reset talents for all character\'s pets if any.'),('reset',3,'Syntax: .reset $subcommand\nType .reset to see the list of possible subcommands or .help reset $subcommand to see info on subcommands'),('respawn',3,'Syntax: .respawn\r\n\r\nRespawn all nearest creatures and GO without waiting respawn time expiration.'),('revive',3,'Syntax: .revive\r\n\r\nRevive the selected player. If no player is selected, it will revive you.'),('save',0,'Syntax: .save\r\n\r\nSaves your character.'),('saveall',1,'Syntax: .saveall\r\n\r\nSave all characters in game.'),('send items',3,'Syntax: .send items #playername \"#subject\" \"#text\" itemid1[:count1] itemid2[:count2] ... itemidN[:countN]\r\n\r\nSend a mail to a player. Subject and mail text must be in \"\". If for itemid not provided related count values then expected 1, if count > max items in stack then items will be send in required amount stacks. All stacks amount in mail limited to 12.'),('send mail',1,'Syntax: .send mail #playername \"#subject\" \"#text\"\r\n\r\nSend a mail to a player. Subject and mail text must be in \"\".'),('send message',3,'Syntax: .send message $playername $message\r\n\r\nSend screen message to player from ADMINISTRATOR.'),('send money',3,'Syntax: .send money #playername \"#subject\" \"#text\" #money\r\n\r\nSend mail with money to a player. Subject and mail text must be in \"\".'),('server corpses',2,'Syntax: .server corpses\r\n\r\nTriggering corpses expire check in world.'),('server exit',4,'Syntax: .server exit\r\n\r\nTerminate trinity-core NOW. Exit code 0.'),('server idlerestart cancel',3,'Syntax: .server idlerestart cancel\r\n\r\nCancel the restart/shutdown timer if any.'),('server idlerestart',3,'Syntax: .server idlerestart #delay\r\n\r\nRestart the server after #delay seconds if no active connections are present (no players). Use #exist_code or 2 as program exist code.'),('server idleshutdown cancel',3,'Syntax: .server idleshutdown cancel\r\n\r\nCancel the restart/shutdown timer if any.'),('server idleshutdown',3,'Syntax: .server idleshutdown #delay [#exist_code]\r\n\r\nShut the server down after #delay seconds if no active connections are present (no players). Use #exist_code or 0 as program exist code.'),('server info',1,'Syntax: .server info\r\n\r\nDisplay server version and the number of connected players.'),('server motd',1,'Syntax: .server motd\r\n\r\nShow server Message of the day.'),('server restart cancel',3,'Syntax: .server restart cancel\r\n\r\nCancel the restart/shutdown timer if any.'),('server restart',3,'Syntax: .server restart #delay\r\n\r\nRestart the server after #delay seconds. Use #exist_code or 2 as program exist code.'),('server set closed',3,'Syntax: server set closed on/off\r\n\r\nSets whether the world accepts new client connectsions.'),('server set loglevel',4,'Syntax: .server set loglevel #level\r\n\r\nSet server log level (0 - errors only, 1 - basic, 2 - detail, 3 - debug).'),('server set motd',3,'Syntax: .server set motd $MOTD\r\n\r\nSet server Message of the day.'),('server shutdown cancel',3,'Syntax: .server shutdown cancel\r\n\r\nCancel the restart/shutdown timer if any.'),('server shutdown',3,'Syntax: .server shutdown #delay [#exit_code]\r\n\r\nShut the server down after #delay seconds. Use #exit_code or 0 as program exit code.'),('server',3,'Syntax: .server $subcommand\nType .server to see the list of possible subcommands or .help server $subcommand to see info on subcommands'),('setskill',3,'Syntax: .setskill #skill #level [#max]\r\n\r\nSet a skill of id #skill with a current skill value of #level and a maximum value of #max (or equal current maximum if not provide) for the selected character. If no character is selected, you learn the skill.'),('showarea',3,'Syntax: .showarea #areaid\r\n\r\nReveal the area of #areaid to the selected character. If no character is selected, reveal this area to you.'),('start',1,'Syntax: .start\r\n\r\nTeleport you to the starting area of your character.'),('taxicheat',1,'Syntax: .taxicheat on/off\r\n\r\nTemporary grant access or remove to all taxi routes for the selected character. If no character is selected, hide or reveal all routes to you.\r\n\r\nVisited taxi nodes sill accessible after removing access.'),('tele add',3,'Syntax: .tele add $name\r\n\r\nAdd current your position to .tele command target locations list with name $name.'),('tele del',3,'Syntax: .tele del $name\r\n\r\nRemove location with name $name for .tele command locations list.'),('tele group',1,'Syntax: .tele group#location\r\n\r\nTeleport a selected player and his group members to a given location.'),('tele name',1,'Syntax: .tele name [#playername] #location\r\n\r\nTeleport the given character to a given location. Character can be offline.'),('tele',1,'Syntax: .tele #location\r\n\r\nTeleport player to a given location.'),('ticket assign',1,'Usage: .ticket assign $ticketid $gmname.\r\nAssigns the specified ticket to the specified Game Master.'),('ticket close',1,'Usage: .ticket close $ticketid.\r\nCloses the specified ticket. Does not delete permanently.'),('ticket closedlist',1,'Displays a list of closed GM tickets.'),('ticket comment',1,'Usage: .ticket comment $ticketid $comment.\r\nAllows the adding or modifying of a comment to the specified ticket.'),('ticket delete',3,'Usage: .ticket delete $ticketid.\r\nDeletes the specified ticket permanently. Ticket must be closed first.'),('ticket list',1,'Displays a list of open GM tickets.'),('ticket onlinelist',1,'Displays a list of open GM tickets whose owner is online.'),('ticket unassign',1,'Usage: .ticket unassign $ticketid.\r\nUnassigns the specified ticket from the current assigned Game Master.'),('ticket viewid',1,'Usage: .ticket viewid $ticketid.\r\nReturns details about specified ticket. Ticket must be open and not deleted.'),('ticket viewname',1,'Usage: .ticket viewname $creatorname. \r\nReturns details about specified ticket. Ticket must be open and not deleted.'),('ticket',1,'Syntax: .ticket $subcommand\nType .ticket to see the list of possible subcommands or .help ticket $subcommand to see info on subcommands'),('unaura',3,'Syntax: .unaura #spellid\r\n\r\nRemove aura due to spell #spellid from the selected Unit.'),('unban account',3,'Syntax: .unban account $Name\r\nUnban accounts for account name pattern.'),('unban character',3,'Syntax: .unban character $Name\r\nUnban accounts for character name pattern.'),('unban ip',3,'Syntax : .unban ip $Ip\r\nUnban accounts for IP pattern.'),('unban',3,'Syntax: .unban $subcommand\nType .unban to see the list of possible subcommands or .help unban $subcommand to see info on subcommands'),('unbindsight',3,'Syntax: .unbindsight\r\n\r\nRemoves bound vision. Cannot be used while currently possessing a target.'),('unfreeze',1,'Syntax: .unfreeze (#player)\r\n\"Unfreezes\" #player and enables his chat again. When using this without #name it will unfreeze your target.'),('unlearn',3,'Syntax: .unlearn #spell [all]\r\n\r\nUnlearn for selected player a spell #spell.  If \'all\' provided then all ranks unlearned.'),('unmute',1,'Syntax: .unmute [$playerName]\r\n\r\nRestore chat messaging for any character from account of character $playerName (or selected). Character can be ofline.'),('unpossess',3,'Syntax: .unpossess\r\n\r\nIf you are possessed, unpossesses yourself; otherwise unpossesses current possessed target.'),('waterwalk',2,'Syntax: .waterwalk on/off\r\n\r\nSet on/off waterwalk state for selected player or self if no player selected.'),('wchange',3,'Syntax: .wchange #weathertype #status\r\n\r\nSet current weather to #weathertype with an intensity of #status.\r\n\r\n#weathertype can be 1 for rain, 2 for snow, and 3 for sand. #status can be 0 for disabled, and 1 for enabled.'),('whispers',1,'Syntax: .whispers on|off\r\nEnable/disable accepting whispers by GM from players. By default use trinityd.conf setting.'),('wp event',2,'Syntax: .wp event $subcommand\nType .path event to see the list of possible subcommands or .help path event $subcommand to see info on subcommands.'),('wp load',2,'Syntax: .wp load $pathid\nLoad pathid number for selected creature. Creature must have no waypoint data.'),('wp show',2,'Syntax: .wp show $option\nOptions:\non $pathid (or selected creature with loaded path) - Show path\noff - Hide path\ninfo $slected_waypoint - Show info for selected waypoint.'),('wp unload',2,'Syntax: .wp unload\nUnload path for selected creature.'),('account addon',1,'Syntax: .account addon #addon\nSet expansion addon level allowed. Addon values: 0 - normal, 1 - tbc, 2 - wotlk.'),('titles remove',2,'Syntax: .titles remove #title\r\nRemove title #title (id or shift-link) from known titles list for selected player.'),('titles current',2,'Syntax: .titles current #title\r\nSet title #title (id or shift-link) as current selected titl for selected player. If title not in known title list for player then it will be added to list.'),('titles add',2,'Syntax: .titles add #title\r\nAdd title #title (id or shift-link) to known titles list for selected player.'),('titles set mask',2,'Syntax: .titles set mask #mask\r\n\r\nAllows user to use all titles from #mask.\r\n\r\n #mask=0 disables the title-choose-field'),('lookup title',2,'Syntax: .lookup title $$namepart\r\n\r\nLooks up a title by $namepart, and returns all matches with their title ID\'s and index\'s.'),('gobject info',2,'Syntax: .gobject info [$object_entry]\r\n\r\nQuery Gameobject information for selected gameobject or given entry.'),('reload creature_template',3,'Syntax: .reload creature_template $entry\r\nReload the specified creature\'s template.'),('character deleted delete',4,'Syntax: .character deleted delete #guid|$name\r\n\r\nCompletely deletes the selected characters.\r\nIf $name is supplied, only characters with that string in their name will be deleted, if #guid is supplied, only the character with that GUID will be deleted.'),('reload creature_onkill_reputation',3,'Syntax: .reload creature_onkill_reputation\r\nReload creature_onkill_reputation table.'),('reload conditions',3,'Reload conditions table.'),('reload locales_item_set_name',3,'Syntax: .reload locales_item_set_name\nReload locales_item_set_name table.'),('character deleted list',3,'Syntax: .character deleted list [#guid|$name]\r\n\r\nShows a list with all deleted characters.\r\nIf $name is supplied, only characters with that string in their name will be selected, if #guid is supplied, only the character with that GUID will be selected.'),('character deleted old',4,'Syntax: .character deleted old [#keepDays]\r\n\r\nCompletely deletes all characters with deleted time longer #keepDays. If #keepDays not provided the  used value from mangosd.conf option \'CharDelete.KeepDays\'. If referenced config option disabled (use 0 value) then command can\'t be used without #keepDays.'),('character deleted restore',3,'Syntax: .character deleted restore #guid|$name [$newname] [#new account]\r\n\r\nRestores deleted characters.\r\nIf $name is supplied, only characters with that string in their name will be restored, if $guid is supplied, only the character with that GUID will be restored.\r\nIf $newname is set, the character will be restored with that name instead of the original one. If #newaccount is set, the character will be restored to specific account character list. This works only with one character!'),('reload gossip_menu',3,'Syntax: .reload gossip_menu\nReload gossip_menu table.'),('reload gossip_menu_option',3,'Syntax: .reload gossip_menu_option\nReload gossip_menu_option table.'),('reload gossip_scripts',3,'Syntax: .reload gossip_scripts\nReload gossip_scripts table.'),('reload locales_gossip_menu_option',3,'Syntax: .reload locales_gossip_menu_option\nReload locales_gossip_menu_option table.'),('reload quest_poi',3,'Syntax: .reload quest_poi\nReload quest_poi table.'),('reload lfg_dungeon_rewards',3,'Syntax: .reload lfg_dungeon_rewards\nReload lfg_dungeon_rewards table.'),('character changefaction',2,'Syntax: .character changefaction $name\r\n\r\nChange character faction.'),('character changerace',2,'Syntax: .character changerace $name\r\n\r\nChange character race.'),('ban playeraccount',3,'Syntax: .ban playeraccount $Name $bantime $reason\nBan account and kick player.\n$bantime: negative value leads to permban, otherwise use a timestring like \"4d20h3s\".'),('achievement add',4,'Syntax: .achievement add $achievement\nAdd an achievement to the targeted player.\n$achievement: can be either achievement id or achievement link'),('achievement',4,'Syntax: .achievement $subcommand\nType .achievement to see the list of possible subcommands or .help achievement $subcommand to see info on subcommands'),('lookup npc',3,'Syntax: .lookup npc $namepart\r\n\r\nLooks up a npc by $namepart, and returns all matches with their npc ID\'s.'),('opcode',4,'Syntax: .opcode is a developer function not intended for use only for opcode research.'),('reload creature_text',3,'Syntax: .reload creature_text\r\nReload creature_text table.'),('wp add',2,'Syntax: .wp add\r\n\r\nAdd a waypoint for the selected creature at your current position.'),('reload all gossips',3,'Syntax: .reload all gossips\nReload gossip_menu, gossip_menu_option, gossip_scripts, npc_gossip, points_of_interest tables.'),('reload all item',3,'Syntax: .reload all item\nReload page_text, item_enchantment_table tables.'),('reload all locales',3,'Syntax: .reload all locales\r\n\r\nReload all `locales_*` tables with reload support added and that can be _safe_ reloaded.'),('reload all loot',3,'Syntax: .reload all loot\r\n\r\nReload all `*_loot_template` tables. This can be slow operation with lags for server run.'),('reload all npc',3,'Syntax: .reload all npc\nReload npc_gossip, npc_option, npc_trainer, npc vendor, points of interest tables.'),('reload all quest',3,'Syntax: .reload all quest\r\n\r\nReload all quest related tables if reload support added for this table and this table can be _safe_ reloaded.'),('reload all scripts',3,'Syntax: .reload all scripts\nReload gameobject_scripts, event_scripts, quest_end_scripts, quest_start_scripts, spell_scripts, db_script_string, waypoint_scripts tables.'),('reload all',3,'Syntax: .reload all\r\n\r\nReload all tables with reload support added and that can be _safe_ reloaded.'),('reload all spell',3,'Syntax: .reload all spell\r\n\r\nReload all `spell_*` tables with reload support added and that can be _safe_ reloaded.'),('resurrect',0,'Resurrect yourself');
/*!40000 ALTER TABLE `command` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conditions`
--

DROP TABLE IF EXISTS `conditions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conditions` (
  `SourceTypeOrReferenceId` mediumint(8) NOT NULL DEFAULT '0',
  `SourceGroup` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `SourceEntry` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `ElseGroup` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `ConditionTypeOrReference` mediumint(8) NOT NULL DEFAULT '0',
  `ConditionValue1` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `ConditionValue2` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `ConditionValue3` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `ErrorTextId` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `ScriptName` char(64) NOT NULL DEFAULT '',
  `Comment` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SourceTypeOrReferenceId`,`SourceGroup`,`SourceEntry`,`ElseGroup`,`ConditionTypeOrReference`,`ConditionValue1`,`ConditionValue2`,`ConditionValue3`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Condition System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conditions`
--

LOCK TABLES `conditions` WRITE;
/*!40000 ALTER TABLE `conditions` DISABLE KEYS */;
/*!40000 ALTER TABLE `conditions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `creature`
--

DROP TABLE IF EXISTS `creature`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `creature` (
  `guid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Global Unique Identifier',
  `id` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'Creature Identifier',
  `map` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Map Identifier',
  `spawnMask` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `phaseMask` smallint(5) unsigned NOT NULL DEFAULT '1',
  `modelid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `equipment_id` mediumint(9) NOT NULL DEFAULT '0',
  `position_x` float NOT NULL DEFAULT '0',
  `position_y` float NOT NULL DEFAULT '0',
  `position_z` float NOT NULL DEFAULT '0',
  `orientation` float NOT NULL DEFAULT '0',
  `spawntimesecs` int(10) unsigned NOT NULL DEFAULT '120',
  `spawndist` float NOT NULL DEFAULT '0',
  `currentwaypoint` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `curhealth` int(10) unsigned NOT NULL DEFAULT '1',
  `curmana` int(10) unsigned NOT NULL DEFAULT '0',
  `DeathState` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `MovementType` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `npcflag` int(10) unsigned NOT NULL DEFAULT '0',
  `unit_flags` int(10) unsigned NOT NULL DEFAULT '0',
  `dynamicflags` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`guid`),
  KEY `idx_map` (`map`),
  KEY `idx_id` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15424914 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='Creature System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `creature`
--

LOCK TABLES `creature` WRITE;
/*!40000 ALTER TABLE `creature` DISABLE KEYS */;
INSERT INTO `creature` VALUES (1061047,46508,732,1,1,0,0,-264.586,1287.86,23.661,1.43719,600,0,0,42,0,0,2,0,0,0),(1060903,46508,732,1,1,0,0,-195.381,1501.86,24.9165,4.90081,600,0,0,42,0,0,2,0,0,0),(1060877,46508,732,1,1,0,0,-204.251,1558.8,25.0401,1.96734,600,0,0,42,0,0,2,0,0,0),(1057756,46508,732,1,1,0,0,-354.44,1247.5,25.4971,5.22982,600,10,0,2650,0,0,1,0,0,0),(1057675,46508,732,1,1,0,0,-306.314,1429.08,22.6127,4.87488,600,0,0,2650,0,0,2,0,0,0),(1057674,46508,732,1,1,0,0,-351.136,1396.79,24.1323,4.72173,600,0,0,2650,0,0,2,0,0,0),(1057671,46508,732,1,1,0,0,-331.991,1290.63,24.2121,0.794741,600,10,0,2650,0,0,1,0,0,0),(1057665,46508,732,1,1,0,0,-406.856,1313.59,24.0268,2.98993,600,0,0,2650,0,0,2,0,0,0),(1057663,46508,732,1,1,0,0,-305.21,1362.85,22.0916,0.307789,600,10,0,2650,0,0,1,0,0,0),(1057748,46508,732,1,1,0,0,-273.173,1245.18,24.1401,3.72579,600,0,0,2650,0,0,2,0,0,0),(1057658,46508,732,1,1,0,0,-183.474,1447.21,23.3868,3.51616,600,0,0,2650,0,0,2,0,0,0),(1057648,46508,732,1,1,0,0,-299.458,1473.16,23.5722,3.70857,600,0,0,2650,0,0,2,0,0,0),(1057647,46508,732,1,1,0,0,-239.88,1507.27,22.992,3.39049,600,0,0,2650,0,0,2,0,0,0),(1057645,46508,732,1,1,0,0,-169.588,1470.3,24.6696,2.39696,600,0,0,2650,0,0,2,0,0,0),(1057643,46508,732,1,1,0,0,-248.451,1306.78,24.7764,1.60764,600,0,0,2650,0,0,2,0,0,0),(1057642,46508,732,1,1,0,0,-207.133,1319.13,25.3439,4.1484,600,0,0,2650,0,0,2,0,0,0),(1057641,46508,732,1,1,0,0,-197.114,1334.97,25.2832,4.37616,600,10,0,2650,0,0,1,0,0,0),(1057630,46508,732,1,1,0,0,-303.647,1279.64,25.4411,5.6053,600,0,0,2650,0,0,2,0,0,0),(1057628,46508,732,1,1,0,0,-303.511,1328.7,19.3396,4.05806,600,10,0,42,0,0,1,0,0,0),(1057626,46508,732,1,1,0,0,-243.342,1342.26,19.6526,3.49258,600,0,0,2650,0,0,2,0,0,0),(1057615,46508,732,1,1,0,0,-225.593,1392.06,20.6155,3.60254,600,0,0,2650,0,0,2,0,0,0),(1057613,46508,732,1,1,0,0,-202.51,1416.7,22.3519,4.11304,600,0,0,2650,0,0,2,0,0,0),(1057609,46508,732,1,1,0,0,-207.199,1442.03,24.3488,5.58567,600,10,0,2650,0,0,1,0,0,0),(1057604,46508,732,1,1,0,0,-234.301,1468.47,23.9413,0.323497,600,0,0,2650,0,0,2,0,0,0),(1057603,46508,732,1,1,0,0,-254.872,1457.47,26.0148,0.409891,600,0,0,2650,0,0,2,0,0,0),(1057600,46508,732,1,1,0,0,-264.015,1431.87,25.8601,2.00818,600,10,0,2650,0,0,1,0,0,0),(1057599,46508,732,1,1,0,0,-259.898,1410.5,23.1018,0.888986,600,10,0,2650,0,0,1,0,0,0),(1057593,46508,732,1,1,0,0,-355.029,1449.22,25.936,2.35768,600,0,0,2650,0,0,2,0,0,0),(1056634,46508,732,1,1,0,0,-369.29,1442.59,25.937,4.02115,600,10,0,2650,0,0,1,0,0,0),(1056633,46508,732,1,1,0,0,-329.923,1452.81,24.7408,2.75274,600,10,0,2650,0,0,1,0,0,0),(1056632,46508,732,1,1,0,0,-397.503,1385.95,22.6349,1.017,600,0,0,2650,0,0,2,0,0,0),(1056631,46508,732,1,1,0,0,-318.64,1413.37,21.451,2.95301,600,10,0,2650,0,0,1,0,0,0),(1060482,46508,732,1,1,0,0,-373.476,1349.29,20.459,5.43881,600,10,0,42,0,0,1,0,0,0);
/*!40000 ALTER TABLE `creature` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `creature_addon`
--

DROP TABLE IF EXISTS `creature_addon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `creature_addon` (
  `guid` int(10) unsigned NOT NULL DEFAULT '0',
  `path_id` int(11) unsigned NOT NULL DEFAULT '0',
  `mount` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `bytes1` int(10) unsigned NOT NULL DEFAULT '0',
  `bytes2` int(10) unsigned NOT NULL DEFAULT '0',
  `emote` int(10) unsigned NOT NULL DEFAULT '0',
  `auras` text,
  PRIMARY KEY (`guid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `creature_addon`
--

LOCK TABLES `creature_addon` WRITE;
/*!40000 ALTER TABLE `creature_addon` DISABLE KEYS */;
/*!40000 ALTER TABLE `creature_addon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `creature_ai_scripts`
--

DROP TABLE IF EXISTS `creature_ai_scripts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `creature_ai_scripts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Identifier',
  `creature_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'Creature Template Identifier',
  `event_type` tinyint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Event Type',
  `event_inverse_phase_mask` int(11) NOT NULL DEFAULT '0' COMMENT 'Mask which phases this event will not trigger in',
  `event_chance` int(3) unsigned NOT NULL DEFAULT '100',
  `event_flags` int(3) unsigned NOT NULL DEFAULT '0',
  `event_param1` int(11) NOT NULL DEFAULT '0',
  `event_param2` int(11) NOT NULL DEFAULT '0',
  `event_param3` int(11) NOT NULL DEFAULT '0',
  `event_param4` int(11) NOT NULL DEFAULT '0',
  `action1_type` tinyint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Action Type',
  `action1_param1` int(11) NOT NULL DEFAULT '0',
  `action1_param2` int(11) NOT NULL DEFAULT '0',
  `action1_param3` int(11) NOT NULL DEFAULT '0',
  `action2_type` tinyint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Action Type',
  `action2_param1` int(11) NOT NULL DEFAULT '0',
  `action2_param2` int(11) NOT NULL DEFAULT '0',
  `action2_param3` int(11) NOT NULL DEFAULT '0',
  `action3_type` tinyint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Action Type',
  `action3_param1` int(11) NOT NULL DEFAULT '0',
  `action3_param2` int(11) NOT NULL DEFAULT '0',
  `action3_param3` int(11) NOT NULL DEFAULT '0',
  `comment` varchar(255) NOT NULL DEFAULT '' COMMENT 'Event Comment',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='CTDB EventAI Scripts';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `creature_ai_scripts`
--

LOCK TABLES `creature_ai_scripts` WRITE;
/*!40000 ALTER TABLE `creature_ai_scripts` DISABLE KEYS */;
/*!40000 ALTER TABLE `creature_ai_scripts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `creature_ai_summons`
--

DROP TABLE IF EXISTS `creature_ai_summons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `creature_ai_summons` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Location Identifier',
  `position_x` float NOT NULL DEFAULT '0',
  `position_y` float NOT NULL DEFAULT '0',
  `position_z` float NOT NULL DEFAULT '0',
  `orientation` float NOT NULL DEFAULT '0',
  `spawntimesecs` int(11) unsigned NOT NULL DEFAULT '120',
  `comment` varchar(255) NOT NULL DEFAULT '' COMMENT 'Summon Comment',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='CTDB EventAI Summoning Locations';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `creature_ai_summons`
--

LOCK TABLES `creature_ai_summons` WRITE;
/*!40000 ALTER TABLE `creature_ai_summons` DISABLE KEYS */;
/*!40000 ALTER TABLE `creature_ai_summons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `creature_ai_texts`
--

DROP TABLE IF EXISTS `creature_ai_texts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `creature_ai_texts` (
  `entry` mediumint(8) NOT NULL,
  `content_default` text NOT NULL,
  `content_loc1` text,
  `content_loc2` text,
  `content_loc3` text,
  `content_loc4` text,
  `content_loc5` text,
  `content_loc6` text,
  `content_loc7` text,
  `content_loc8` text,
  `sound` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `language` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `emote` smallint(5) unsigned NOT NULL DEFAULT '0',
  `comment` text,
  PRIMARY KEY (`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='CTDB Script Texts';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `creature_ai_texts`
--

LOCK TABLES `creature_ai_texts` WRITE;
/*!40000 ALTER TABLE `creature_ai_texts` DISABLE KEYS */;
/*!40000 ALTER TABLE `creature_ai_texts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `creature_classlevelstats`
--

DROP TABLE IF EXISTS `creature_classlevelstats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `creature_classlevelstats` (
  `level` tinyint(1) NOT NULL,
  `class` tinyint(1) NOT NULL,
  `basehp0` smallint(2) NOT NULL,
  `basehp1` smallint(2) NOT NULL,
  `basehp2` smallint(2) NOT NULL,
  `basehp3` int(10) unsigned NOT NULL DEFAULT '0',
  `basemana` smallint(2) NOT NULL,
  `basearmor` smallint(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Define the stats by class and level.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `creature_classlevelstats`
--

LOCK TABLES `creature_classlevelstats` WRITE;
/*!40000 ALTER TABLE `creature_classlevelstats` DISABLE KEYS */;
INSERT INTO `creature_classlevelstats` VALUES (1,1,42,1,1,1,0,8),(2,1,55,1,1,1,0,20),(3,1,71,1,1,1,0,33),(4,1,86,1,1,1,0,68),(5,1,102,1,1,1,0,111),(6,1,120,1,1,1,0,165),(7,1,137,1,1,1,0,230),(8,1,156,1,1,1,0,306),(9,1,176,1,1,1,0,387),(10,1,198,1,1,1,0,463),(11,1,222,1,1,1,0,528),(12,1,247,1,1,1,0,562),(13,1,273,1,1,1,0,596),(14,1,300,1,1,1,0,630),(15,1,328,1,1,1,0,665),(16,1,356,1,1,1,0,700),(17,1,386,1,1,1,0,734),(18,1,417,1,1,1,0,768),(19,1,449,1,1,1,0,802),(20,1,484,1,1,1,0,836),(21,1,521,1,1,1,0,872),(22,1,562,1,1,1,0,906),(23,1,605,1,1,1,0,940),(24,1,651,1,1,1,0,975),(25,1,699,1,1,1,0,1008),(26,1,750,1,1,1,0,1043),(27,1,800,1,1,1,0,1078),(28,1,853,1,1,1,0,1111),(29,1,905,1,1,1,0,1145),(30,1,955,1,1,1,0,1179),(31,1,1006,1,1,1,0,1213),(32,1,1057,1,1,1,0,1249),(33,1,1110,1,1,1,0,1281),(34,1,1163,1,1,1,0,1317),(35,1,1220,1,1,1,0,1349),(36,1,1277,1,1,1,0,1456),(37,1,1336,1,1,1,0,1568),(38,1,1395,1,1,1,0,1684),(39,1,1459,1,1,1,0,1808),(40,1,1524,1,1,1,0,1938),(41,1,1585,1,1,1,0,2074),(42,1,1651,1,1,1,0,2218),(43,1,1716,1,1,1,0,2369),(44,1,1782,1,1,1,0,2528),(45,1,1848,1,1,1,0,2695),(46,1,1919,1,1,1,0,2750),(47,1,1990,1,1,1,0,2804),(48,1,2062,1,1,1,0,2857),(49,1,2138,1,1,1,0,2912),(50,1,2215,1,1,1,0,2966),(51,1,2292,1,1,1,0,3018),(52,1,2371,1,1,1,0,3060),(53,1,2453,1,1,1,0,3128),(54,1,2533,1,1,1,0,3180),(55,1,2614,1,1,1,0,3234),(56,1,2699,1,1,1,0,3289),(57,1,2784,1,1,1,0,3342),(58,1,2871,3989,1,1,0,3396),(59,1,2961,4142,1,1,0,3449),(60,1,3052,4979,1,1,0,3750),(61,1,3144,5158,1,1,0,4047),(62,1,3237,5341,1,1,0,4344),(63,1,3331,5527,1,1,0,4641),(64,1,3427,5715,1,1,0,4937),(65,1,3524,5914,1,1,0,5234),(66,1,3624,6116,1,1,0,5531),(67,1,3728,6326,1,1,0,5829),(68,1,3834,6542,6986,1,0,6126),(69,1,3942,6761,7984,1,0,6423),(70,1,4050,6986,8982,1,0,6719),(71,1,4163,7181,9291,1,0,7018),(72,1,4278,7380,9610,1,0,7318),(73,1,4399,7588,9940,1,0,7618),(74,1,4524,7804,10282,1,0,7918),(75,1,4652,8025,10635,1,0,8219),(76,1,4781,8247,11001,1,0,8520),(77,1,4916,8480,11379,1,0,8822),(78,1,5052,8715,11770,1,0,9124),(79,1,5194,8960,12175,1,0,9426),(80,1,5342,9215,12600,30951,0,9729),(81,1,5492,9474,13033,31951,0,10033),(82,1,5647,9474,13481,44679,0,10356),(83,1,5808,1,13945,53681,0,10673),(85,1,5808,1,13945,58681,0,10673),(87,2,4646,1,13945,60681,4258,10573),(90,8,4090,1,11156,63681,9325,8505),(92,8,4090,1,11156,60681,9325,8505),(93,8,4090,1,11156,60681,9325,8505),(96,2,4646,1,13945,60681,4258,10573),(98,2,4646,1,13945,60681,4258,10573),(99,8,4090,1,11156,60681,9325,8505),(1,2,41,1,1,1,60,7),(2,2,54,1,1,1,69,19),(3,2,69,1,1,1,79,33),(4,2,83,1,1,1,104,66),(5,2,98,1,1,1,115,109),(6,2,115,1,1,1,126,163),(7,2,131,1,1,1,138,208),(8,2,148,1,1,1,165,303),(9,2,166,1,1,1,178,369),(10,2,186,1,1,1,191,460),(11,2,208,1,1,1,205,526),(12,2,230,1,1,1,249,560),(13,2,253,1,1,1,264,596),(14,2,276,1,1,1,295,630),(15,2,301,1,1,1,326,665),(16,2,325,1,1,1,357,700),(17,2,350,1,1,1,390,734),(18,2,377,1,1,1,408,768),(19,2,404,1,1,1,456,802),(20,2,433,1,1,1,490,836),(21,2,464,1,1,1,510,872),(22,2,498,1,1,1,545,906),(23,2,533,1,1,1,581,940),(24,2,571,1,1,1,618,975),(25,2,610,1,1,1,655,1008),(26,2,651,1,1,1,693,1042),(27,2,690,1,1,1,732,1078),(28,2,732,1,1,1,756,1110),(29,2,773,1,1,1,811,1145),(30,2,811,1,1,1,852,1178),(31,2,850,1,1,1,878,1213),(32,2,888,1,1,1,935,1248),(33,2,928,1,1,1,963,1281),(34,2,967,1,1,1,1007,1316),(35,2,1009,1,1,1,1067,1349),(36,2,1050,1,1,1,1097,1455),(37,2,1093,1,1,1,1142,1567),(38,2,1135,1,1,1,1189,1683),(39,2,1180,1,1,1,1236,1807),(40,2,1226,1,1,1,1283,1937),(41,2,1268,1,1,1,1332,2072),(42,2,1321,1,1,1,1381,2216),(43,2,1373,1,1,1,1432,2367),(44,2,1426,1,1,1,1483,2527),(45,2,1478,1,1,1,1534,2692),(46,2,1535,1,1,1,1587,2749),(47,2,1592,1,1,1,1640,2802),(48,2,1650,1,1,1,1695,2855),(49,2,1710,1,1,1,1750,2910),(50,2,1772,1,1,1,1807,2964),(51,2,1834,1,1,1,1864,3017),(52,2,1897,1,1,1,1923,3072),(53,2,1962,1,1,1,1982,3126),(54,2,2026,1,1,1,2041,3178),(55,2,2091,1,1,1,2117,3232),(56,2,2159,1,1,1,2163,3287),(57,2,2227,1,1,1,2241,3340),(58,2,2297,3191,1,1,2289,3394),(59,2,2369,3314,1,1,2369,3447),(60,2,2442,3984,1,1,2434,3748),(61,2,2515,4126,1,1,2486,4044),(62,2,2590,4274,1,1,2568,4340),(63,2,2665,4422,1,1,2620,4637),(64,2,2740,4572,1,1,2705,4933),(65,2,2819,4731,1,1,2790,5228),(66,2,2899,4892,6116,1,2846,5523),(67,2,2982,5060,1,1,2933,5821),(68,2,3067,5233,6986,1,2991,6116),(69,2,3153,5409,7984,1,3080,6412),(70,2,3240,5589,8982,1,3155,6708),(71,2,3330,5744,9291,1,3231,7007),(72,2,3422,5903,9610,1,3309,7305),(73,2,3519,6070,9940,1,3387,7604),(74,2,3619,6243,10282,1,3466,7903),(75,2,3722,6420,10635,1,3561,8204),(76,2,3825,6602,11001,1,3643,8503),(77,2,3933,6784,11379,1,3725,8803),(78,2,4042,6972,11770,1,3809,9104),(79,2,4155,7167,12175,1,3893,9405),(80,2,4274,7373,12600,30951,3994,9706),(81,2,4394,7581,13033,31951,4081,10007),(82,2,4518,7794,13481,44679,4169,10253),(84,1,5808,1,13945,56681,0,10673),(86,1,5808,1,13945,59681,0,10673),(87,1,5808,1,13945,60681,0,10673),(90,2,4646,1,13945,63681,4258,10573),(91,8,4090,1,11156,64681,9325,8505),(94,8,4090,1,11156,60681,9325,8505),(95,4,5808,1,13945,60681,0,9589),(98,4,5808,1,13945,60681,0,9589),(99,2,4646,1,13945,60681,4258,10573),(1,4,42,1,1,1,0,6),(2,4,55,1,1,1,0,18),(3,4,71,1,1,1,0,31),(4,4,86,1,1,1,0,63),(5,4,102,1,1,1,0,102),(6,4,120,1,1,1,0,152),(7,4,137,1,1,1,0,212),(8,4,156,1,1,1,0,286),(9,4,176,1,1,1,0,363),(10,4,198,1,1,1,0,443),(11,4,222,1,1,1,0,488),(12,4,247,1,1,1,0,519),(13,4,273,1,1,1,0,553),(14,4,300,1,1,1,0,577),(15,4,328,1,1,1,0,612),(16,4,356,1,1,1,0,645),(17,4,386,1,1,1,0,676),(18,4,417,1,1,1,0,706),(19,4,449,1,1,1,0,738),(20,4,484,1,1,1,0,769),(21,4,521,1,1,1,0,801),(22,4,562,1,1,1,0,833),(23,4,605,1,1,1,0,863),(24,4,651,1,1,1,0,895),(25,4,699,1,1,1,0,926),(26,4,750,1,1,1,0,957),(27,4,800,1,1,1,0,989),(28,4,853,1,1,1,0,1020),(29,4,905,1,1,1,0,1051),(30,4,955,1,1,1,0,1082),(31,4,1006,1,1,1,0,1113),(32,4,1057,1,1,1,0,1146),(33,4,1110,1,1,1,0,1173),(34,4,1163,1,1,1,0,1208),(35,4,1220,1,1,1,0,1237),(36,4,1277,1,1,1,0,1349),(37,4,1336,1,1,1,0,1434),(38,4,1395,1,1,1,0,1538),(39,4,1459,1,1,1,0,1649),(40,4,1524,1,1,1,0,1764),(41,4,1585,1,1,1,0,1886),(42,4,1651,1,1,1,0,2015),(43,4,1716,1,1,1,0,2148),(44,4,1782,1,1,1,0,2303),(45,4,1848,1,1,1,0,2436),(46,4,1919,1,1,1,0,2485),(47,4,1990,1,1,1,0,2535),(48,4,2062,1,1,1,0,2582),(49,4,2138,1,1,1,0,2631),(50,4,2215,1,1,1,0,2680),(51,4,2292,1,1,1,0,2728),(52,4,2371,1,1,1,0,2778),(53,4,2453,1,1,1,0,2826),(54,4,2533,1,1,1,0,2874),(55,4,2614,1,1,1,0,2922),(56,4,2699,1,1,1,0,2972),(57,4,2784,1,1,1,0,3020),(58,4,2871,3989,1,1,0,3068),(59,4,2961,4142,1,1,0,3117),(60,4,3052,4979,1,1,0,3388),(61,4,3144,5158,1,1,0,3655),(62,4,3237,5341,1,1,0,3922),(63,4,3331,5527,1,1,0,4189),(64,4,3427,5715,1,1,0,4457),(65,4,3524,5914,1,1,0,4724),(66,4,3624,6116,1,1,0,5104),(67,4,3728,6326,1,1,0,5326),(68,4,3834,6542,6986,1,0,5527),(69,4,3942,6761,7984,1,0,5795),(70,4,4050,6986,8982,1,0,6062),(71,4,4163,7181,9291,1,0,6332),(72,4,4278,7380,9610,1,0,6602),(73,4,4399,7580,9940,1,0,6872),(74,4,4524,7580,10282,1,0,7143),(75,4,4652,5617,10635,1,0,7415),(76,4,4781,8025,11001,1,0,7686),(77,4,4916,8480,11379,1,0,7958),(78,4,5052,8715,11770,1,0,8230),(79,4,5194,8960,12175,1,0,8503),(80,4,5342,8960,12600,30951,0,8776),(81,4,5496,7581,13033,31951,0,9068),(82,4,5647,9474,13481,44679,0,9348),(84,2,4646,1,13945,56681,4258,10573),(86,2,4646,1,13945,59681,4258,10573),(88,2,4646,1,13945,61681,4258,10573),(89,8,4090,1,11156,62681,9325,8505),(91,1,5808,1,13945,64681,0,10673),(94,4,5808,1,13945,60681,0,9589),(95,2,4646,1,13945,60681,4258,10573),(98,1,5808,1,13945,60681,0,10673),(100,4,5808,1,13945,60681,0,9589),(1,8,40,1,1,1,120,5),(2,8,52,1,1,1,147,16),(3,8,67,1,1,1,174,28),(4,8,81,1,1,1,202,57),(5,8,95,1,1,1,230,93),(6,8,111,1,1,1,259,139),(7,8,126,1,1,1,289,194),(8,8,143,1,1,1,319,265),(9,8,160,1,1,1,350,339),(10,8,178,1,1,1,382,423),(11,8,199,1,1,1,459,447),(12,8,219,1,1,1,537,475),(13,8,241,1,1,1,601,509),(14,8,263,1,1,1,710,523),(15,8,285,1,1,1,790,559),(16,8,307,1,1,1,856,589),(17,8,330,1,1,1,938,617),(18,8,354,1,1,1,1020,643),(19,8,379,1,1,1,1118,674),(20,8,405,1,1,1,1202,701),(21,8,432,1,1,1,1272,729),(22,8,463,1,1,1,1357,759),(23,8,494,1,1,1,1443,786),(24,8,528,1,1,1,1545,815),(25,8,562,1,1,1,1633,843),(26,8,598,1,1,1,1707,871),(27,8,633,1,1,1,1812,900),(28,8,669,1,1,1,1977,928),(29,8,704,1,1,1,2068,957),(30,8,737,1,1,1,2175,984),(31,8,770,1,1,1,2253,1012),(32,8,802,1,1,1,2362,1042),(33,8,835,1,1,1,2457,1065),(34,8,867,1,1,1,2553,1098),(35,8,902,1,1,1,2680,1124),(36,8,935,1,1,1,2763,1241),(37,8,970,1,1,1,2861,1300),(38,8,1004,1,1,1,2975,1391),(39,8,1040,1,1,1,3075,1489),(40,8,1077,1,1,1,3191,1590),(41,8,1110,1,1,1,3293,1697),(42,8,1156,1,1,1,3471,1811),(43,8,1201,1,1,1,3575,1926),(44,8,1247,1,1,1,3680,2078),(45,8,1294,1,1,1,3801,2177),(46,8,1343,1,1,1,3923,2220),(47,8,1393,1,1,1,4031,2265),(48,8,1443,1,1,1,4140,2307),(49,8,1497,1,1,1,4281,2349),(50,8,1551,1,1,1,4393,2393),(51,8,1604,1,1,1,4506,2437),(52,8,1660,1,1,1,4650,2481),(53,8,1717,1,1,1,4765,2524),(54,8,1773,1,1,1,4896,2567),(55,8,1830,1,1,1,5013,2609),(56,8,1889,1,1,1,5206,2654),(57,8,1949,1,1,1,5340,2698),(58,8,2010,2793,1,1,5461,2740),(59,8,2073,2899,1,1,5598,2784),(60,8,2136,3484,1,1,5751,3025),(61,8,2201,3611,1,1,5875,3263),(62,8,2266,3739,1,1,6015,3500),(63,8,2332,3870,1,1,6156,3736),(64,8,2399,4000,1,1,6229,3977),(65,8,2467,4140,4731,1,6443,4214),(66,8,2552,4281,4892,1,6588,4460),(67,8,2610,4429,1,1,6749,4710),(68,8,2684,4580,5588,1,6882,4928),(69,8,2759,4733,6387,1,7031,5167),(70,8,2835,4890,7185,1,7196,5404),(71,8,2914,5027,7432,1,7332,5645),(72,8,2995,5166,7688,1,7500,5886),(73,8,3098,5311,7952,1,7654,6126),(74,8,3186,7580,8225,1,7809,6368),(75,8,3256,5617,8508,1,7981,6610),(76,8,3367,8025,8800,1,8139,6851),(77,8,3462,8480,9103,1,8313,7094),(78,8,3558,6972,9416,1,8459,7335),(79,8,3658,6972,9740,1,8636,7579),(80,8,3739,8960,10080,30951,8814,7822),(81,8,3870,7581,10486,31951,8979,8102),(82,8,3977,9474,10784,44679,9160,8340),(83,2,4646,1,13945,53681,4258,10573),(86,4,5808,1,13945,59681,0,9589),(87,8,4090,1,11156,60681,9325,8505),(90,1,5808,1,13945,63681,0,10673),(91,4,5808,1,13945,64681,0,9589),(93,4,5808,1,13945,60681,0,9589),(95,8,4090,1,11156,60681,9325,8505),(97,8,4090,1,11156,60681,9325,8505),(100,1,5808,1,13945,60681,0,10673),(83,8,4090,1,11156,53681,9325,8505),(84,4,5808,1,13945,56681,0,9589),(84,8,4090,1,11156,56681,9325,8505),(83,4,5808,1,13945,53681,0,9589),(85,4,5808,1,13945,58681,0,9589),(85,2,4646,1,13945,58681,4258,10573),(85,8,4090,1,11156,58681,9325,8505),(86,8,4090,1,11156,59681,9325,8505),(88,8,4090,1,11156,61681,9325,8505),(88,1,5808,1,13945,61681,0,10673),(88,4,5808,1,13945,61681,0,9589),(87,4,5808,1,13945,60681,0,9589),(90,4,5808,1,13945,63681,0,9589),(89,4,5808,1,13945,62681,0,9589),(89,2,4646,1,13945,62681,4258,10573),(89,1,5808,1,13945,62681,0,10673),(92,2,4646,1,13945,60681,4258,10573),(92,1,5808,1,13945,60681,0,10673),(92,4,5808,1,13945,60681,0,9589),(91,2,4646,1,13945,64681,4258,10573),(94,2,4646,1,13945,60681,4258,10573),(93,1,5808,1,13945,60681,0,10673),(94,1,5808,1,13945,60681,0,10673),(93,2,4646,1,13945,60681,4258,10573),(96,8,4090,1,11156,60681,9325,8505),(96,4,5808,1,13945,60681,0,9589),(96,1,5808,1,13945,60681,0,10673),(95,1,5808,1,13945,60681,0,10673),(98,8,4090,1,11156,60681,9325,8505),(97,2,4646,1,13945,60681,4258,10573),(97,1,5808,1,13945,60681,0,10673),(97,4,5808,1,13945,60681,0,9589),(99,4,5808,1,13945,60681,0,9589),(99,1,5808,1,13945,60681,0,10673),(100,8,4090,1,11156,60681,9325,8505),(100,2,4646,1,13945,60681,4258,10573),(70,1,6986,8982,8982,1,0,6719),(71,1,9291,9291,9291,1,0,7018),(72,1,9610,9610,9610,1,0,7318),(73,1,9940,9940,9940,1,0,7618),(74,1,10282,10282,10282,1,0,7918),(75,1,10635,10635,10635,1,0,8219),(76,1,11001,11001,11001,1,0,8520),(77,1,11379,11379,11379,1,0,8822),(78,1,11770,11770,11770,1,0,9124),(79,1,12175,12175,12175,1,0,9426),(80,1,10313,10313,10313,30951,0,9729),(81,1,12395,12395,12395,37187,0,10033),(82,1,14893,14893,14893,44679,0,10356),(83,1,17893,17893,17893,53681,0,10673),(84,1,21498,21498,21498,64496,0,11033),(85,1,25830,25830,25830,77490,0,12673),(86,1,1,1,1,59681,0,1),(87,1,1,1,1,60681,0,1),(88,1,1,1,1,60681,0,1),(89,1,1,1,1,60681,0,1),(90,1,1,1,1,60681,0,1),(91,1,1,1,1,60681,0,1),(92,1,1,1,1,60681,0,1),(93,1,1,1,1,60681,0,1),(94,1,1,1,1,60681,0,1),(95,1,1,1,1,60681,0,1),(96,1,1,1,1,60681,0,1),(97,1,1,1,1,60681,0,1),(98,1,1,1,1,60681,0,1),(99,1,1,1,1,60681,0,1),(100,1,1,1,1,60681,0,1),(70,2,8982,8982,8982,1,3155,6708),(71,2,9291,9291,9291,1,3231,7007),(72,2,9610,9610,9610,1,3309,7305),(73,2,9940,9940,9940,1,3387,7604),(74,2,10282,10282,10282,1,3466,7903),(75,2,10635,10635,10635,1,3561,8204),(76,2,11001,11001,11001,1,3643,8503),(77,2,11379,11379,11379,1,3725,8803),(78,2,11770,11770,11770,1,3809,9104),(79,2,12175,12175,12175,1,3893,9405),(80,2,10313,10313,10313,30951,3994,9706),(81,2,12395,12395,12395,37187,4081,10007),(82,2,14893,14893,14893,44679,4169,10253),(83,2,17893,17893,17893,53681,4258,10573),(84,2,21498,21498,21498,64496,4369,11033),(85,2,25830,25830,25830,77490,4558,12673),(86,2,1,1,1,59681,1,1),(87,2,1,1,1,60681,1,1),(88,2,1,1,1,60681,4258,1),(89,2,1,1,1,60681,4258,1),(90,2,1,1,1,60681,4258,1),(91,2,1,1,1,60681,4258,1),(92,2,1,1,1,60681,1,1),(93,2,1,1,1,60681,1,1),(94,2,1,1,1,60681,1,1),(95,2,1,1,1,60681,1,1),(96,2,1,1,1,60681,1,1),(97,2,1,1,1,60681,1,1),(98,2,1,1,1,60681,1,1),(99,2,1,1,1,60681,1,1),(100,2,1,1,1,60681,1,1),(74,4,4524,1,10282,1,0,7143),(75,4,4652,1,10635,1,0,7415),(76,4,4781,1,11001,1,0,7686),(77,4,4916,1,11379,1,0,7958),(78,4,5052,1,11770,1,0,8230),(79,4,5194,1,12175,1,0,8503),(80,4,5342,1,12600,1,0,8776),(81,4,5496,1,13033,1,0,9068),(82,4,5647,1,13481,44679,0,9348),(83,4,5808,1,13945,53681,0,9589),(84,4,1,1,13945,53681,0,1),(85,4,1,1,1,58681,0,1),(86,4,1,1,1,59681,0,1),(87,4,1,1,1,60681,0,1),(88,4,1,1,1,60681,0,1),(89,4,1,1,1,60681,0,1),(90,4,1,1,1,60681,0,1),(91,4,1,1,1,60681,0,1),(92,4,1,1,1,60681,0,1),(93,4,1,1,1,60681,0,1),(94,4,1,1,1,60681,0,1),(95,4,1,1,1,60681,0,1),(96,4,1,1,1,60681,0,1),(97,4,1,1,1,60681,0,1),(98,4,1,1,1,60681,0,1),(99,4,1,1,1,60681,0,1),(100,4,1,1,1,60681,0,1),(74,8,3186,1,8225,1,7809,6368),(76,8,3367,1,8800,1,8139,6851),(77,8,3462,1,9103,1,8313,7094),(78,8,3558,1,9416,1,8459,7335),(79,8,3658,1,9740,1,8636,7579),(80,8,3739,1,10080,1,8814,7822),(81,8,3870,1,10486,1,8979,8102),(82,8,3977,1,10784,1,9160,8340),(83,8,4090,1,11156,44679,9325,8505),(84,8,1,1,13945,53681,1,1),(85,8,1,1,1,58681,1,1),(86,8,1,1,1,59681,1,1),(87,8,1,1,1,60681,9325,1),(88,8,1,1,1,60681,1,1),(89,8,1,1,1,60681,9325,1),(90,8,1,1,1,60681,9325,1),(91,8,1,1,1,60681,9325,1),(92,8,1,1,1,60681,9325,1),(93,8,1,1,1,60681,1,1),(94,8,1,1,1,60681,1,1),(95,8,1,1,1,60681,1,1),(96,8,1,1,1,60681,1,1),(97,8,1,1,1,60681,1,1),(98,8,1,1,1,60681,1,1),(99,8,1,1,1,60681,1,1),(100,8,1,1,1,60681,1,1),(81,1,5492,9474,13033,37187,0,10033),(82,1,5647,1,13481,44679,0,10356),(84,1,1,1,1,64496,0,10930),(85,1,1,1,1,91623,0,11230),(86,1,1,1,1,107596,0,11530),(87,1,1,1,1,60681,0,11830),(88,1,1,1,1,60681,0,1),(89,1,1,1,1,60681,0,1),(90,1,1,1,1,60681,0,1),(91,1,1,1,1,1,0,1),(92,1,1,1,1,1,0,1),(93,1,1,1,1,1,0,1),(94,1,1,1,1,1,0,1),(95,1,1,1,1,1,0,1),(96,1,1,1,1,1,0,1),(97,1,1,1,1,1,0,1),(98,1,1,1,1,1,0,1),(99,1,1,1,1,1,0,1),(100,1,1,1,1,1,0,1),(81,2,4394,7581,13033,37187,4081,10007),(84,2,1,1,1,64496,4322,10879),(85,2,1,1,1,91623,4412,11176),(86,2,1,1,1,107596,4502,11474),(87,2,1,1,1,60681,4594,11771),(88,2,1,1,1,60681,1,1),(89,2,1,1,1,60681,1,1),(90,2,1,1,1,60681,1,1),(91,2,1,1,1,1,1,1),(92,2,1,1,1,1,1,1),(93,2,1,1,1,1,1,1),(94,2,1,1,1,1,1,1),(95,2,1,1,1,1,1,1),(96,2,1,1,1,1,1,1),(97,2,1,1,1,1,1,1),(98,2,1,1,1,1,1,1),(99,2,1,1,1,1,1,1),(100,2,1,1,1,1,1,1),(80,4,5342,1,12600,30951,0,8776),(81,4,5496,1,13033,37187,0,9068),(84,4,1,1,1,64496,0,9853),(85,4,1,1,1,91623,0,10122),(86,4,1,1,1,107596,0,10392),(87,4,1,1,1,60681,0,10661),(88,4,1,1,1,60681,0,1),(89,4,1,1,1,60681,0,1),(90,4,1,1,1,60681,0,1),(91,4,1,1,1,1,0,1),(92,4,1,1,1,1,0,1),(93,4,1,1,1,1,0,1),(94,4,1,1,1,1,0,1),(95,4,1,1,1,1,0,1),(96,4,1,1,1,1,0,1),(97,4,1,1,1,1,0,1),(98,4,1,1,1,1,0,1),(99,4,1,1,1,1,0,1),(100,4,1,1,1,1,0,1),(80,8,3739,1,10080,24761,8814,7822),(81,8,3870,1,10486,29750,8979,8102),(82,8,3977,1,10784,35743,9160,8340),(83,8,4090,1,11156,42945,9325,8505),(84,8,1,1,1,51597,9517,8780),(85,8,1,1,1,73299,9694,9020),(86,8,1,1,1,86077,9873,9261),(87,8,1,1,1,60681,10052,9501),(88,8,1,1,1,60681,1,1),(89,8,1,1,1,60681,1,1),(90,8,1,1,1,60681,1,1),(91,8,1,1,1,1,1,1),(92,8,1,1,1,1,1,1),(93,8,1,1,1,1,1,1),(94,8,1,1,1,1,1,1),(95,8,1,1,1,1,1,1),(96,8,1,1,1,1,1,1),(97,8,1,1,1,1,1,1),(98,8,1,1,1,1,1,1),(99,8,1,1,1,1,1,1),(100,8,1,1,1,1,1,1),(84,1,1,1,1,64496,0,10930),(85,1,1,1,1,91623,0,11230),(86,1,1,1,1,107596,0,11530),(87,1,1,1,1,1,0,11830),(88,1,1,1,1,1,0,1),(89,1,1,1,1,1,0,1),(90,1,1,1,1,1,0,1),(91,1,1,1,1,1,0,1),(92,1,1,1,1,1,0,1),(93,1,1,1,1,1,0,1),(94,1,1,1,1,1,0,1),(95,1,1,1,1,1,0,1),(96,1,1,1,1,1,0,1),(97,1,1,1,1,1,0,1),(98,1,1,1,1,1,0,1),(99,1,1,1,1,1,0,1),(100,1,1,1,1,1,0,1),(84,2,1,1,1,64496,4322,10879),(85,2,1,1,1,91623,4412,11176),(86,2,1,1,1,107596,4502,11474),(87,2,1,1,1,1,4594,11771),(88,2,1,1,1,1,1,1),(89,2,1,1,1,1,1,1),(90,2,1,1,1,1,1,1),(91,2,1,1,1,1,1,1),(92,2,1,1,1,1,1,1),(93,2,1,1,1,1,1,1),(94,2,1,1,1,1,1,1),(95,2,1,1,1,1,1,1),(96,2,1,1,1,1,1,1),(97,2,1,1,1,1,1,1),(98,2,1,1,1,1,1,1),(99,2,1,1,1,1,1,1),(100,2,1,1,1,1,1,1),(84,4,1,1,1,64496,0,9853),(85,4,1,1,1,91623,0,10122),(86,4,1,1,1,107596,0,10392),(87,4,1,1,1,1,0,10661),(88,4,1,1,1,1,0,1),(89,4,1,1,1,1,0,1),(90,4,1,1,1,1,0,1),(91,4,1,1,1,1,0,1),(92,4,1,1,1,1,0,1),(93,4,1,1,1,1,0,1),(94,4,1,1,1,1,0,1),(95,4,1,1,1,1,0,1),(96,4,1,1,1,1,0,1),(97,4,1,1,1,1,0,1),(98,4,1,1,1,1,0,1),(99,4,1,1,1,1,0,1),(100,4,1,1,1,1,0,1),(84,8,1,1,1,51597,9517,8780),(85,8,1,1,1,73299,9694,9020),(86,8,1,1,1,86077,9873,9261),(87,8,1,1,1,1,10052,9501),(88,8,1,1,1,1,1,1),(89,8,1,1,1,1,1,1),(90,8,1,1,1,1,1,1),(91,8,1,1,1,1,1,1),(92,8,1,1,1,1,1,1),(93,8,1,1,1,1,1,1),(94,8,1,1,1,1,1,1),(95,8,1,1,1,1,1,1),(96,8,1,1,1,1,1,1),(97,8,1,1,1,1,1,1),(98,8,1,1,1,1,1,1),(99,8,1,1,1,1,1,1),(100,8,1,1,1,1,1,1),(83,1,5808,1,13945,53681,0,10673),(85,1,5808,1,13945,58681,0,10673),(87,2,4646,1,13945,60681,4258,10573),(90,8,4090,1,11156,63681,9325,8505),(92,8,4090,1,11156,53681,9325,8505),(93,8,4090,1,11156,53681,9325,8505),(96,2,4646,1,13945,53681,4258,10573),(98,2,4646,1,13945,53681,4258,10573),(99,8,4090,1,11156,53681,9325,8505),(84,1,5808,1,13945,56681,0,10673),(86,1,5808,1,13945,59681,0,10673),(87,1,5808,1,13945,60681,0,10673),(90,2,4646,1,13945,63681,4258,10573),(91,8,4090,1,11156,64681,9325,8505),(94,8,4090,1,11156,53681,9325,8505),(95,4,5808,1,13945,53681,0,9589),(98,4,5808,1,13945,53681,0,9589),(99,2,4646,1,13945,53681,4258,10573),(84,2,4646,1,13945,56681,4258,10573),(86,2,4646,1,13945,59681,4258,10573),(88,2,4646,1,13945,61681,4258,10573),(89,8,4090,1,11156,62681,9325,8505),(91,1,5808,1,13945,64681,0,10673),(94,4,5808,1,13945,53681,0,9589),(95,2,4646,1,13945,53681,4258,10573),(98,1,5808,1,13945,53681,0,10673),(100,4,5808,1,13945,53681,0,9589),(86,4,5808,1,13945,59681,0,9589),(87,8,4090,1,11156,60681,9325,8505),(90,1,5808,1,13945,63681,0,10673),(91,4,5808,1,13945,64681,0,9589),(93,4,5808,1,13945,53681,0,9589),(95,8,4090,1,11156,53681,9325,8505),(97,8,4090,1,11156,53681,9325,8505),(100,1,5808,1,13945,53681,0,10673),(83,8,4090,1,11156,53681,9325,8505),(84,4,5808,1,13945,56681,0,9589),(84,8,4090,1,11156,56681,9325,8505),(83,4,5808,1,13945,53681,0,9589),(85,4,5808,1,13945,58681,0,9589),(85,2,4646,1,13945,58681,4258,10573),(85,8,4090,1,11156,58681,9325,8505),(86,8,4090,1,11156,59681,9325,8505),(88,8,4090,1,11156,61681,9325,8505),(88,1,5808,1,13945,61681,0,10673),(88,4,5808,1,13945,61681,0,9589),(87,4,5808,1,13945,60681,0,9589),(90,4,5808,1,13945,63681,0,9589),(89,4,5808,1,13945,62681,0,9589),(89,2,4646,1,13945,62681,4258,10573),(89,1,5808,1,13945,62681,0,10673),(92,2,4646,1,13945,53681,4258,10573),(92,1,5808,1,13945,53681,0,10673),(92,4,5808,1,13945,53681,0,9589),(91,2,4646,1,13945,64681,4258,10573),(94,2,4646,1,13945,53681,4258,10573),(93,1,5808,1,13945,53681,0,10673),(94,1,5808,1,13945,53681,0,10673),(93,2,4646,1,13945,53681,4258,10573),(96,8,4090,1,11156,53681,9325,8505),(96,4,5808,1,13945,53681,0,9589),(96,1,5808,1,13945,53681,0,10673),(95,1,5808,1,13945,53681,0,10673),(98,8,4090,1,11156,53681,9325,8505),(97,2,4646,1,13945,53681,4258,10573),(97,1,5808,1,13945,53681,0,10673),(97,4,5808,1,13945,53681,0,9589),(99,4,5808,1,13945,53681,0,9589),(99,1,5808,1,13945,53681,0,10673),(100,8,4090,1,11156,53681,9325,8505),(100,2,4646,1,13945,53681,4258,10573),(83,1,5808,1,13945,53681,0,10673),(85,1,5808,1,13945,58681,0,10673),(87,2,4646,1,13945,60681,4258,10573),(90,8,4090,1,11156,63681,9325,8505),(92,8,4090,1,11156,53681,9325,8505),(93,8,4090,1,11156,53681,9325,8505),(96,2,4646,1,13945,53681,4258,10573),(98,2,4646,1,13945,53681,4258,10573),(99,8,4090,1,11156,53681,9325,8505),(84,1,5808,1,13945,56681,0,10673),(86,1,5808,1,13945,59681,0,10673),(87,1,5808,1,13945,60681,0,10673),(90,2,4646,1,13945,63681,4258,10573),(91,8,4090,1,11156,64681,9325,8505),(94,8,4090,1,11156,53681,9325,8505),(95,4,5808,1,13945,53681,0,9589),(98,4,5808,1,13945,53681,0,9589),(99,2,4646,1,13945,53681,4258,10573),(84,2,4646,1,13945,56681,4258,10573),(86,2,4646,1,13945,59681,4258,10573),(88,2,4646,1,13945,61681,4258,10573),(89,8,4090,1,11156,62681,9325,8505),(91,1,5808,1,13945,64681,0,10673),(94,4,5808,1,13945,53681,0,9589),(95,2,4646,1,13945,53681,4258,10573),(98,1,5808,1,13945,53681,0,10673),(100,4,5808,1,13945,53681,0,9589),(86,4,5808,1,13945,59681,0,9589),(87,8,4090,1,11156,60681,9325,8505),(90,1,5808,1,13945,63681,0,10673),(91,4,5808,1,13945,64681,0,9589),(93,4,5808,1,13945,53681,0,9589),(95,8,4090,1,11156,53681,9325,8505),(97,8,4090,1,11156,53681,9325,8505),(100,1,5808,1,13945,53681,0,10673),(83,8,4090,1,11156,53681,9325,8505),(84,4,5808,1,13945,56681,0,9589),(84,8,4090,1,11156,56681,9325,8505),(83,4,5808,1,13945,53681,0,9589),(85,4,5808,1,13945,58681,0,9589),(85,2,4646,1,13945,58681,4258,10573),(85,8,4090,1,11156,58681,9325,8505),(86,8,4090,1,11156,59681,9325,8505),(88,8,4090,1,11156,61681,9325,8505),(88,1,5808,1,13945,61681,0,10673),(88,4,5808,1,13945,61681,0,9589),(87,4,5808,1,13945,60681,0,9589),(90,4,5808,1,13945,63681,0,9589),(89,4,5808,1,13945,62681,0,9589),(89,2,4646,1,13945,62681,4258,10573),(89,1,5808,1,13945,62681,0,10673),(92,2,4646,1,13945,53681,4258,10573),(92,1,5808,1,13945,53681,0,10673),(92,4,5808,1,13945,53681,0,9589),(91,2,4646,1,13945,64681,4258,10573),(94,2,4646,1,13945,53681,4258,10573),(93,1,5808,1,13945,53681,0,10673),(94,1,5808,1,13945,53681,0,10673),(93,2,4646,1,13945,53681,4258,10573),(96,8,4090,1,11156,53681,9325,8505),(96,4,5808,1,13945,53681,0,9589),(96,1,5808,1,13945,53681,0,10673),(95,1,5808,1,13945,53681,0,10673),(98,8,4090,1,11156,53681,9325,8505),(97,2,4646,1,13945,53681,4258,10573),(97,1,5808,1,13945,53681,0,10673),(97,4,5808,1,13945,53681,0,9589),(99,4,5808,1,13945,53681,0,9589),(99,1,5808,1,13945,53681,0,10673),(100,8,4090,1,11156,53681,9325,8505),(100,2,4646,1,13945,53681,4258,10573),(85,1,5808,1,13945,58681,0,10673),(87,2,4646,1,13945,60681,4258,10573),(90,8,4090,1,11156,63681,9325,8505),(92,8,4090,1,11156,53681,9325,8505),(93,8,4090,1,11156,53681,9325,8505),(96,2,4646,1,13945,53681,4258,10573),(98,2,4646,1,13945,53681,4258,10573),(99,8,4090,1,11156,53681,9325,8505),(84,1,5808,1,13945,56681,0,10673),(86,1,5808,1,13945,59681,0,10673),(87,1,5808,1,13945,60681,0,10673),(90,2,4646,1,13945,63681,4258,10573),(91,8,4090,1,11156,64681,9325,8505),(94,8,4090,1,11156,53681,9325,8505),(95,4,5808,1,13945,53681,0,9589),(98,4,5808,1,13945,53681,0,9589),(99,2,4646,1,13945,53681,4258,10573),(84,2,4646,1,13945,56681,4258,10573),(86,2,4646,1,13945,59681,4258,10573),(88,2,4646,1,13945,61681,4258,10573),(89,8,4090,1,11156,62681,9325,8505),(91,1,5808,1,13945,64681,0,10673),(94,4,5808,1,13945,53681,0,9589),(95,2,4646,1,13945,53681,4258,10573),(98,1,5808,1,13945,53681,0,10673),(100,4,5808,1,13945,53681,0,9589),(86,4,5808,1,13945,59681,0,9589),(87,8,4090,1,11156,60681,9325,8505),(90,1,5808,1,13945,63681,0,10673),(91,4,5808,1,13945,64681,0,9589),(93,4,5808,1,13945,53681,0,9589),(95,8,4090,1,11156,53681,9325,8505),(97,8,4090,1,11156,53681,9325,8505),(100,1,5808,1,13945,53681,0,10673),(83,8,4090,1,11156,53681,9325,8505),(84,4,5808,1,13945,56681,0,9589),(84,8,4090,1,11156,56681,9325,8505),(83,4,5808,1,13945,53681,0,9589),(85,4,5808,1,13945,58681,0,9589),(85,2,4646,1,13945,58681,4258,10573),(85,8,4090,1,11156,58681,9325,8505),(86,8,4090,1,11156,59681,9325,8505),(88,8,4090,1,11156,61681,9325,8505),(88,1,5808,1,13945,61681,0,10673),(88,4,5808,1,13945,61681,0,9589),(87,4,5808,1,13945,60681,0,9589),(90,4,5808,1,13945,63681,0,9589),(89,4,5808,1,13945,62681,0,9589),(89,2,4646,1,13945,62681,4258,10573),(89,1,5808,1,13945,62681,0,10673),(92,2,4646,1,13945,53681,4258,10573),(92,1,5808,1,13945,53681,0,10673),(92,4,5808,1,13945,53681,0,9589),(91,2,4646,1,13945,64681,4258,10573),(94,2,4646,1,13945,53681,4258,10573),(93,1,5808,1,13945,53681,0,10673),(94,1,5808,1,13945,53681,0,10673),(93,2,4646,1,13945,53681,4258,10573),(96,8,4090,1,11156,53681,9325,8505),(96,4,5808,1,13945,53681,0,9589),(96,1,5808,1,13945,53681,0,10673),(95,1,5808,1,13945,53681,0,10673),(98,8,4090,1,11156,53681,9325,8505),(97,2,4646,1,13945,53681,4258,10573),(97,1,5808,1,13945,53681,0,10673),(97,4,5808,1,13945,53681,0,9589),(99,4,5808,1,13945,53681,0,9589),(99,1,5808,1,13945,53681,0,10673),(100,8,4090,1,11156,53681,9325,8505),(100,2,4646,1,13945,53681,4258,10573);
/*!40000 ALTER TABLE `creature_classlevelstats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `creature_equip_template`
--

DROP TABLE IF EXISTS `creature_equip_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `creature_equip_template` (
  `entry` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'Unique entry',
  `equipentry1` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `equipentry2` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `equipentry3` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Creature System (Equipment)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `creature_equip_template`
--

LOCK TABLES `creature_equip_template` WRITE;
/*!40000 ALTER TABLE `creature_equip_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `creature_equip_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `creature_formations`
--

DROP TABLE IF EXISTS `creature_formations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `creature_formations` (
  `leaderGUID` int(11) unsigned NOT NULL,
  `memberGUID` int(11) unsigned NOT NULL,
  `dist` float unsigned NOT NULL,
  `angle` float unsigned NOT NULL,
  `groupAI` int(11) unsigned NOT NULL,
  PRIMARY KEY (`memberGUID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `creature_formations`
--

LOCK TABLES `creature_formations` WRITE;
/*!40000 ALTER TABLE `creature_formations` DISABLE KEYS */;
/*!40000 ALTER TABLE `creature_formations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `creature_involvedrelation`
--

DROP TABLE IF EXISTS `creature_involvedrelation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `creature_involvedrelation` (
  `id` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'Identifier',
  `quest` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'Quest Identifier',
  PRIMARY KEY (`id`,`quest`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='CTDB Creature System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `creature_involvedrelation`
--

LOCK TABLES `creature_involvedrelation` WRITE;
/*!40000 ALTER TABLE `creature_involvedrelation` DISABLE KEYS */;
/*!40000 ALTER TABLE `creature_involvedrelation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `creature_linked_respawn`
--

DROP TABLE IF EXISTS `creature_linked_respawn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `creature_linked_respawn` (
  `guid` int(10) unsigned NOT NULL COMMENT 'dependent creature',
  `linkedGuid` int(10) unsigned NOT NULL COMMENT 'master creature',
  PRIMARY KEY (`guid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='Creature Respawn Link System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `creature_linked_respawn`
--

LOCK TABLES `creature_linked_respawn` WRITE;
/*!40000 ALTER TABLE `creature_linked_respawn` DISABLE KEYS */;
/*!40000 ALTER TABLE `creature_linked_respawn` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `creature_loot_template`
--

DROP TABLE IF EXISTS `creature_loot_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `creature_loot_template` (
  `entry` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `item` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `ChanceOrQuestChance` float NOT NULL DEFAULT '100',
  `lootmode` smallint(5) unsigned NOT NULL DEFAULT '1',
  `groupid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `mincountOrRef` mediumint(9) NOT NULL DEFAULT '1',
  `maxcount` tinyint(3) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`entry`,`item`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Loot System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `creature_loot_template`
--

LOCK TABLES `creature_loot_template` WRITE;
/*!40000 ALTER TABLE `creature_loot_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `creature_loot_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `creature_model_info`
--

DROP TABLE IF EXISTS `creature_model_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `creature_model_info` (
  `modelid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `bounding_radius` float NOT NULL DEFAULT '0',
  `combat_reach` float NOT NULL DEFAULT '0',
  `gender` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `modelid_other_gender` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`modelid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Creature System (Model related info)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `creature_model_info`
--

LOCK TABLES `creature_model_info` WRITE;
/*!40000 ALTER TABLE `creature_model_info` DISABLE KEYS */;
INSERT INTO `creature_model_info` VALUES (26775,0.31,1,2,0);
/*!40000 ALTER TABLE `creature_model_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `creature_onkill_reputation`
--

DROP TABLE IF EXISTS `creature_onkill_reputation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `creature_onkill_reputation` (
  `creature_id` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'Creature Identifier',
  `RewOnKillRepFaction1` smallint(6) NOT NULL DEFAULT '0',
  `RewOnKillRepFaction2` smallint(6) NOT NULL DEFAULT '0',
  `MaxStanding1` tinyint(4) NOT NULL DEFAULT '0',
  `IsTeamAward1` tinyint(4) NOT NULL DEFAULT '0',
  `RewOnKillRepValue1` mediumint(9) NOT NULL DEFAULT '0',
  `MaxStanding2` tinyint(4) NOT NULL DEFAULT '0',
  `IsTeamAward2` tinyint(4) NOT NULL DEFAULT '0',
  `RewOnKillRepValue2` mediumint(9) NOT NULL DEFAULT '0',
  `TeamDependent` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`creature_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='CTDB Creature OnKill Reputation gain';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `creature_onkill_reputation`
--

LOCK TABLES `creature_onkill_reputation` WRITE;
/*!40000 ALTER TABLE `creature_onkill_reputation` DISABLE KEYS */;
/*!40000 ALTER TABLE `creature_onkill_reputation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `creature_questrelation`
--

DROP TABLE IF EXISTS `creature_questrelation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `creature_questrelation` (
  `id` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'Identifier',
  `quest` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'Quest Identifier',
  PRIMARY KEY (`id`,`quest`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='CTDB Creature System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `creature_questrelation`
--

LOCK TABLES `creature_questrelation` WRITE;
/*!40000 ALTER TABLE `creature_questrelation` DISABLE KEYS */;
/*!40000 ALTER TABLE `creature_questrelation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `creature_respawn`
--

DROP TABLE IF EXISTS `creature_respawn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `creature_respawn` (
  `guid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Global Unique Identifier',
  `respawntime` bigint(20) unsigned NOT NULL DEFAULT '0',
  `instance` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`guid`,`instance`),
  KEY `instance` (`instance`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='Grid Loading System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `creature_respawn`
--

LOCK TABLES `creature_respawn` WRITE;
/*!40000 ALTER TABLE `creature_respawn` DISABLE KEYS */;
INSERT INTO `creature_respawn` VALUES (1057647,1308556818,0);
/*!40000 ALTER TABLE `creature_respawn` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `creature_template`
--

DROP TABLE IF EXISTS `creature_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `creature_template` (
  `entry` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `difficulty_entry_1` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `difficulty_entry_2` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `difficulty_entry_3` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `KillCredit1` int(11) unsigned NOT NULL DEFAULT '0',
  `KillCredit2` int(11) unsigned NOT NULL DEFAULT '0',
  `modelid1` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `modelid2` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `modelid3` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `modelid4` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `name` char(100) NOT NULL DEFAULT '0',
  `subname` char(100) DEFAULT NULL,
  `IconName` char(100) DEFAULT NULL,
  `gossip_menu_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `minlevel` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `maxlevel` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `exp` smallint(2) NOT NULL DEFAULT '0',
  `faction_A` smallint(5) unsigned NOT NULL DEFAULT '0',
  `faction_H` smallint(5) unsigned NOT NULL DEFAULT '0',
  `npcflag` int(10) unsigned NOT NULL DEFAULT '0',
  `speed_walk` float NOT NULL DEFAULT '1' COMMENT 'Result of 2.5/2.5, most common value',
  `speed_run` float NOT NULL DEFAULT '1.14286' COMMENT 'Result of 8.0/7.0, most common value',
  `scale` float NOT NULL DEFAULT '1',
  `rank` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `mindmg` float NOT NULL DEFAULT '0',
  `maxdmg` float NOT NULL DEFAULT '0',
  `dmgschool` tinyint(4) NOT NULL DEFAULT '0',
  `attackpower` int(10) unsigned NOT NULL DEFAULT '0',
  `dmg_multiplier` float NOT NULL DEFAULT '1',
  `baseattacktime` int(10) unsigned NOT NULL DEFAULT '0',
  `rangeattacktime` int(10) unsigned NOT NULL DEFAULT '0',
  `unit_class` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `unit_flags` int(10) unsigned NOT NULL DEFAULT '0',
  `dynamicflags` int(10) unsigned NOT NULL DEFAULT '0',
  `family` tinyint(4) NOT NULL DEFAULT '0',
  `trainer_type` tinyint(4) NOT NULL DEFAULT '0',
  `trainer_spell` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `trainer_class` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `trainer_race` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `minrangedmg` float NOT NULL DEFAULT '0',
  `maxrangedmg` float NOT NULL DEFAULT '0',
  `rangedattackpower` smallint(5) unsigned NOT NULL DEFAULT '0',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `type_flags` int(10) unsigned NOT NULL DEFAULT '0',
  `lootid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `pickpocketloot` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `skinloot` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `resistance1` smallint(5) NOT NULL DEFAULT '0',
  `resistance2` smallint(5) NOT NULL DEFAULT '0',
  `resistance3` smallint(5) NOT NULL DEFAULT '0',
  `resistance4` smallint(5) NOT NULL DEFAULT '0',
  `resistance5` smallint(5) NOT NULL DEFAULT '0',
  `resistance6` smallint(5) NOT NULL DEFAULT '0',
  `spell1` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `spell2` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `spell3` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `spell4` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `spell5` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `spell6` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `spell7` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `spell8` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `PetSpellDataId` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `VehicleId` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `mingold` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `maxgold` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `AIName` char(64) NOT NULL DEFAULT '',
  `MovementType` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `InhabitType` tinyint(3) unsigned NOT NULL DEFAULT '3',
  `Health_mod` float NOT NULL DEFAULT '1',
  `Mana_mod` float NOT NULL DEFAULT '1',
  `Armor_mod` float NOT NULL DEFAULT '1',
  `RacialLeader` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `questItem1` int(11) unsigned NOT NULL DEFAULT '0',
  `questItem2` int(11) unsigned NOT NULL DEFAULT '0',
  `questItem3` int(11) unsigned NOT NULL DEFAULT '0',
  `questItem4` int(11) unsigned NOT NULL DEFAULT '0',
  `questItem5` int(11) unsigned NOT NULL DEFAULT '0',
  `questItem6` int(11) unsigned NOT NULL DEFAULT '0',
  `movementId` int(11) unsigned NOT NULL DEFAULT '0',
  `RegenHealth` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `equipment_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `mechanic_immune_mask` int(10) unsigned NOT NULL DEFAULT '0',
  `flags_extra` int(10) unsigned NOT NULL DEFAULT '0',
  `ScriptName` char(64) NOT NULL DEFAULT '',
  `WDBVerified` smallint(5) DEFAULT '1',
  PRIMARY KEY (`entry`),
  KEY `idx_name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='CTDB Creature System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `creature_template`
--

LOCK TABLES `creature_template` WRITE;
/*!40000 ALTER TABLE `creature_template` DISABLE KEYS */;
INSERT INTO `creature_template` VALUES (46508,0,0,0,0,0,26775,0,0,0,'Darkwood Lurker','','',0,85,85,3,16,16,0,1,1.14286,1,0,530,713,0,827,1,2000,0,1,0,0,3,0,0,0,0,399,559,169,1,1,46508,0,46508,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,12,13,'EventAI',0,1,1,1,1,0,0,0,0,0,0,0,0,1,0,0,0,'',1);
/*!40000 ALTER TABLE `creature_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `creature_template_addon`
--

DROP TABLE IF EXISTS `creature_template_addon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `creature_template_addon` (
  `entry` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `path_id` int(11) unsigned NOT NULL DEFAULT '0',
  `mount` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `bytes1` int(10) unsigned NOT NULL DEFAULT '0',
  `bytes2` int(10) unsigned NOT NULL DEFAULT '0',
  `emote` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `auras` text,
  PRIMARY KEY (`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Creature addon define by entrys.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `creature_template_addon`
--

LOCK TABLES `creature_template_addon` WRITE;
/*!40000 ALTER TABLE `creature_template_addon` DISABLE KEYS */;
/*!40000 ALTER TABLE `creature_template_addon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `creature_text`
--

DROP TABLE IF EXISTS `creature_text`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `creature_text` (
  `entry` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `groupid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `text` longtext,
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `language` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `probability` float NOT NULL DEFAULT '0',
  `emote` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `duration` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `sound` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `comment` varchar(255) DEFAULT '',
  PRIMARY KEY (`entry`,`groupid`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Creature texts';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `creature_text`
--

LOCK TABLES `creature_text` WRITE;
/*!40000 ALTER TABLE `creature_text` DISABLE KEYS */;
/*!40000 ALTER TABLE `creature_text` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `creature_transport`
--

DROP TABLE IF EXISTS `creature_transport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `creature_transport` (
  `guid` int(16) NOT NULL AUTO_INCREMENT COMMENT 'GUID of NPC on transport - not the same as creature.guid',
  `transport_entry` int(8) NOT NULL COMMENT 'Transport entry',
  `npc_entry` int(8) NOT NULL COMMENT 'NPC entry',
  `TransOffsetX` float NOT NULL DEFAULT '0',
  `TransOffsetY` float NOT NULL DEFAULT '0',
  `TransOffsetZ` float NOT NULL DEFAULT '0',
  `TransOffsetO` float NOT NULL DEFAULT '0',
  `emote` int(16) NOT NULL,
  PRIMARY KEY (`transport_entry`,`guid`),
  UNIQUE KEY `entry` (`transport_entry`,`guid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `creature_transport`
--

LOCK TABLES `creature_transport` WRITE;
/*!40000 ALTER TABLE `creature_transport` DISABLE KEYS */;
/*!40000 ALTER TABLE `creature_transport` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `custom_texts`
--

DROP TABLE IF EXISTS `custom_texts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `custom_texts` (
  `entry` mediumint(8) NOT NULL,
  `content_default` text NOT NULL,
  `content_loc1` text,
  `content_loc2` text,
  `content_loc3` text,
  `content_loc4` text,
  `content_loc5` text,
  `content_loc6` text,
  `content_loc7` text,
  `content_loc8` text,
  `sound` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `language` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `emote` smallint(5) unsigned NOT NULL DEFAULT '0',
  `comment` text,
  PRIMARY KEY (`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='CTDB Custom Texts';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `custom_texts`
--

LOCK TABLES `custom_texts` WRITE;
/*!40000 ALTER TABLE `custom_texts` DISABLE KEYS */;
/*!40000 ALTER TABLE `custom_texts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `db_script_string`
--

DROP TABLE IF EXISTS `db_script_string`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `db_script_string` (
  `entry` int(11) unsigned NOT NULL DEFAULT '0',
  `content_default` text NOT NULL,
  `content_loc1` text,
  `content_loc2` text,
  `content_loc3` text,
  `content_loc4` text,
  `content_loc5` text,
  `content_loc6` text,
  `content_loc7` text,
  `content_loc8` text,
  PRIMARY KEY (`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Scripts';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `db_script_string`
--

LOCK TABLES `db_script_string` WRITE;
/*!40000 ALTER TABLE `db_script_string` DISABLE KEYS */;
/*!40000 ALTER TABLE `db_script_string` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `disables`
--

DROP TABLE IF EXISTS `disables`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `disables` (
  `sourceType` int(10) unsigned NOT NULL,
  `entry` int(10) unsigned NOT NULL,
  `flags` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `params_0` varchar(255) NOT NULL DEFAULT '',
  `params_1` varchar(255) NOT NULL DEFAULT '',
  `comment` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  PRIMARY KEY (`sourceType`,`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='CTDB Disable Actions';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `disables`
--

LOCK TABLES `disables` WRITE;
/*!40000 ALTER TABLE `disables` DISABLE KEYS */;
/*!40000 ALTER TABLE `disables` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `disenchant_loot_template`
--

DROP TABLE IF EXISTS `disenchant_loot_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `disenchant_loot_template` (
  `entry` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'Recommended id selection: item_level*100 + item_quality',
  `item` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `ChanceOrQuestChance` float NOT NULL DEFAULT '100',
  `lootmode` smallint(5) unsigned NOT NULL DEFAULT '1',
  `groupid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `mincountOrRef` mediumint(9) NOT NULL DEFAULT '1',
  `maxcount` tinyint(3) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`entry`,`item`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='CTDB Loot System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `disenchant_loot_template`
--

LOCK TABLES `disenchant_loot_template` WRITE;
/*!40000 ALTER TABLE `disenchant_loot_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `disenchant_loot_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_scripts`
--

DROP TABLE IF EXISTS `event_scripts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_scripts` (
  `id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `delay` int(10) unsigned NOT NULL DEFAULT '0',
  `command` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `datalong` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `datalong2` int(10) unsigned NOT NULL DEFAULT '0',
  `dataint` int(11) NOT NULL DEFAULT '0',
  `x` float NOT NULL DEFAULT '0',
  `y` float NOT NULL DEFAULT '0',
  `z` float NOT NULL DEFAULT '0',
  `o` float NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Scripts Events';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_scripts`
--

LOCK TABLES `event_scripts` WRITE;
/*!40000 ALTER TABLE `event_scripts` DISABLE KEYS */;
/*!40000 ALTER TABLE `event_scripts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exploration_basexp`
--

DROP TABLE IF EXISTS `exploration_basexp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exploration_basexp` (
  `level` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `basexp` mediumint(9) NOT NULL DEFAULT '0',
  PRIMARY KEY (`level`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='CTDB Exploration System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exploration_basexp`
--

LOCK TABLES `exploration_basexp` WRITE;
/*!40000 ALTER TABLE `exploration_basexp` DISABLE KEYS */;
INSERT INTO `exploration_basexp` VALUES (0,0),(1,5),(2,15),(3,25),(4,35),(5,45),(6,55),(7,65),(8,70),(9,80),(10,85),(11,90),(12,90),(13,90),(14,100),(15,105),(16,115),(17,125),(18,135),(19,145),(20,155),(21,165),(22,175),(23,185),(24,195),(25,200),(26,210),(27,220),(28,230),(29,240),(30,245),(31,250),(32,255),(33,265),(34,270),(35,275),(36,280),(37,285),(38,285),(39,300),(40,315),(41,330),(42,345),(43,360),(44,375),(45,390),(46,405),(47,420),(48,440),(49,455),(50,470),(51,490),(52,510),(53,530),(54,540),(55,560),(56,580),(57,600),(58,620),(59,640),(60,660),(61,970),(62,1000),(63,1050),(64,1080),(65,1100),(66,1130),(67,1160),(68,1200),(69,1230),(70,1300),(71,1330),(72,1370),(73,1410),(74,1440),(75,1470),(76,1510),(77,1530),(78,1600),(79,1630),(84,1800),(82,1750),(81,1700),(80,1670),(83,1780);
/*!40000 ALTER TABLE `exploration_basexp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fishing_loot_template`
--

DROP TABLE IF EXISTS `fishing_loot_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fishing_loot_template` (
  `entry` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `item` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `ChanceOrQuestChance` float NOT NULL DEFAULT '100',
  `lootmode` smallint(5) unsigned NOT NULL DEFAULT '1',
  `groupid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `mincountOrRef` mediumint(9) NOT NULL DEFAULT '1',
  `maxcount` tinyint(3) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`entry`,`item`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Loot System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fishing_loot_template`
--

LOCK TABLES `fishing_loot_template` WRITE;
/*!40000 ALTER TABLE `fishing_loot_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `fishing_loot_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `game_event`
--

DROP TABLE IF EXISTS `game_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `game_event` (
  `entry` mediumint(8) unsigned NOT NULL COMMENT 'Entry of the game event',
  `start_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Absolute start date, the event will never start before',
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Absolute end date, the event will never start afler',
  `occurence` bigint(20) unsigned NOT NULL DEFAULT '5184000' COMMENT 'Delay in minutes between occurences of the event',
  `length` bigint(20) unsigned NOT NULL DEFAULT '2592000' COMMENT 'Length in minutes of the event',
  `holiday` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'Client side holiday id',
  `description` varchar(255) DEFAULT NULL COMMENT 'Description of the event displayed in console',
  `world_event` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0 if normal event, 1 if world event',
  PRIMARY KEY (`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Game events';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game_event`
--

LOCK TABLES `game_event` WRITE;
/*!40000 ALTER TABLE `game_event` DISABLE KEYS */;
/*!40000 ALTER TABLE `game_event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `game_event_battleground_holiday`
--

DROP TABLE IF EXISTS `game_event_battleground_holiday`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `game_event_battleground_holiday` (
  `event` int(10) unsigned NOT NULL,
  `bgflag` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`event`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Game event battlegrounds';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game_event_battleground_holiday`
--

LOCK TABLES `game_event_battleground_holiday` WRITE;
/*!40000 ALTER TABLE `game_event_battleground_holiday` DISABLE KEYS */;
/*!40000 ALTER TABLE `game_event_battleground_holiday` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `game_event_condition`
--

DROP TABLE IF EXISTS `game_event_condition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `game_event_condition` (
  `event_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `condition_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `req_num` float DEFAULT '0',
  `max_world_state_field` smallint(5) unsigned NOT NULL DEFAULT '0',
  `done_world_state_field` smallint(5) unsigned NOT NULL DEFAULT '0',
  `description` varchar(25) NOT NULL DEFAULT '',
  PRIMARY KEY (`event_id`,`condition_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Game event conditions';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game_event_condition`
--

LOCK TABLES `game_event_condition` WRITE;
/*!40000 ALTER TABLE `game_event_condition` DISABLE KEYS */;
/*!40000 ALTER TABLE `game_event_condition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `game_event_creature`
--

DROP TABLE IF EXISTS `game_event_creature`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `game_event_creature` (
  `guid` int(10) unsigned NOT NULL,
  `event` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Put negatives values to remove during event',
  PRIMARY KEY (`guid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Game event creatures';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game_event_creature`
--

LOCK TABLES `game_event_creature` WRITE;
/*!40000 ALTER TABLE `game_event_creature` DISABLE KEYS */;
/*!40000 ALTER TABLE `game_event_creature` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `game_event_creature_quest`
--

DROP TABLE IF EXISTS `game_event_creature_quest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `game_event_creature_quest` (
  `id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `quest` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `event` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`quest`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Creatures with quests';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game_event_creature_quest`
--

LOCK TABLES `game_event_creature_quest` WRITE;
/*!40000 ALTER TABLE `game_event_creature_quest` DISABLE KEYS */;
/*!40000 ALTER TABLE `game_event_creature_quest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `game_event_gameobject`
--

DROP TABLE IF EXISTS `game_event_gameobject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `game_event_gameobject` (
  `guid` int(10) unsigned NOT NULL,
  `event` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Put negatives values to remove during event',
  PRIMARY KEY (`guid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Game event gameobject';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game_event_gameobject`
--

LOCK TABLES `game_event_gameobject` WRITE;
/*!40000 ALTER TABLE `game_event_gameobject` DISABLE KEYS */;
/*!40000 ALTER TABLE `game_event_gameobject` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `game_event_gameobject_quest`
--

DROP TABLE IF EXISTS `game_event_gameobject_quest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `game_event_gameobject_quest` (
  `id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `quest` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `event` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`quest`,`event`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Game event gameobject quest';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game_event_gameobject_quest`
--

LOCK TABLES `game_event_gameobject_quest` WRITE;
/*!40000 ALTER TABLE `game_event_gameobject_quest` DISABLE KEYS */;
/*!40000 ALTER TABLE `game_event_gameobject_quest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `game_event_model_equip`
--

DROP TABLE IF EXISTS `game_event_model_equip`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `game_event_model_equip` (
  `guid` int(10) unsigned NOT NULL DEFAULT '0',
  `modelid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `equipment_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `event` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`guid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Game event model equip';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game_event_model_equip`
--

LOCK TABLES `game_event_model_equip` WRITE;
/*!40000 ALTER TABLE `game_event_model_equip` DISABLE KEYS */;
/*!40000 ALTER TABLE `game_event_model_equip` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `game_event_npc_gossip`
--

DROP TABLE IF EXISTS `game_event_npc_gossip`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `game_event_npc_gossip` (
  `guid` int(10) unsigned NOT NULL,
  `event_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `textid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`guid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Game event npc gossip';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game_event_npc_gossip`
--

LOCK TABLES `game_event_npc_gossip` WRITE;
/*!40000 ALTER TABLE `game_event_npc_gossip` DISABLE KEYS */;
/*!40000 ALTER TABLE `game_event_npc_gossip` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `game_event_npc_vendor`
--

DROP TABLE IF EXISTS `game_event_npc_vendor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `game_event_npc_vendor` (
  `event` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `guid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `slot` smallint(6) NOT NULL DEFAULT '0',
  `item` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `maxcount` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `incrtime` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `ExtendedCost` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`guid`,`item`),
  KEY `slot` (`slot`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Game event npc vendor';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game_event_npc_vendor`
--

LOCK TABLES `game_event_npc_vendor` WRITE;
/*!40000 ALTER TABLE `game_event_npc_vendor` DISABLE KEYS */;
/*!40000 ALTER TABLE `game_event_npc_vendor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `game_event_npcflag`
--

DROP TABLE IF EXISTS `game_event_npcflag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `game_event_npcflag` (
  `guid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `event_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `npcflag` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`guid`,`event_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Game event npcflag';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game_event_npcflag`
--

LOCK TABLES `game_event_npcflag` WRITE;
/*!40000 ALTER TABLE `game_event_npcflag` DISABLE KEYS */;
/*!40000 ALTER TABLE `game_event_npcflag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `game_event_pool`
--

DROP TABLE IF EXISTS `game_event_pool`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `game_event_pool` (
  `pool_entry` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'Id of the pool',
  `event` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Put negatives values to remove during event',
  PRIMARY KEY (`pool_entry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Game event pool';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game_event_pool`
--

LOCK TABLES `game_event_pool` WRITE;
/*!40000 ALTER TABLE `game_event_pool` DISABLE KEYS */;
/*!40000 ALTER TABLE `game_event_pool` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `game_event_prerequisite`
--

DROP TABLE IF EXISTS `game_event_prerequisite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `game_event_prerequisite` (
  `event_id` mediumint(8) unsigned NOT NULL,
  `prerequisite_event` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`event_id`,`prerequisite_event`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Game Event Prerequisite';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game_event_prerequisite`
--

LOCK TABLES `game_event_prerequisite` WRITE;
/*!40000 ALTER TABLE `game_event_prerequisite` DISABLE KEYS */;
/*!40000 ALTER TABLE `game_event_prerequisite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `game_event_quest_condition`
--

DROP TABLE IF EXISTS `game_event_quest_condition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `game_event_quest_condition` (
  `quest` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `event_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `condition_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `num` float DEFAULT '0',
  PRIMARY KEY (`quest`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Game event quest conditions';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game_event_quest_condition`
--

LOCK TABLES `game_event_quest_condition` WRITE;
/*!40000 ALTER TABLE `game_event_quest_condition` DISABLE KEYS */;
/*!40000 ALTER TABLE `game_event_quest_condition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `game_graveyard_zone`
--

DROP TABLE IF EXISTS `game_graveyard_zone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `game_graveyard_zone` (
  `id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `ghost_zone` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `faction` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`ghost_zone`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Trigger System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game_graveyard_zone`
--

LOCK TABLES `game_graveyard_zone` WRITE;
/*!40000 ALTER TABLE `game_graveyard_zone` DISABLE KEYS */;
/*!40000 ALTER TABLE `game_graveyard_zone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `game_tele`
--

DROP TABLE IF EXISTS `game_tele`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `game_tele` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `position_x` float NOT NULL DEFAULT '0',
  `position_y` float NOT NULL DEFAULT '0',
  `position_z` float NOT NULL DEFAULT '0',
  `orientation` float NOT NULL DEFAULT '0',
  `map` smallint(5) unsigned NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10007 DEFAULT CHARSET=utf8 COMMENT='CTDB Tele Command';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game_tele`
--

LOCK TABLES `game_tele` WRITE;
/*!40000 ALTER TABLE `game_tele` DISABLE KEYS */;
INSERT INTO `game_tele` VALUES (314,-758.534,4401.98,79.563,2.88658,530,'DustquillRavine'),(313,-6440.73,-1987.77,244.718,0.464476,0,'DustfireValley'),(312,-10898.3,-364.784,39.2681,3.04614,0,'Duskwood'),(311,9317.38,-7856.4,63.2953,5.35693,530,'DuskwitherSpire'),(310,9496.84,-7862.65,16.1435,5.20221,530,'DuskwitherGrounds'),(309,1340.98,-4638.58,53.5445,5.0328,1,'DurotarZeppelin'),(308,1007.78,-4446.22,11.2022,0.20797,1,'Durotar'),(307,-483.455,-1426.23,89.1916,1.93697,0,'DurnholdeKeep'),(306,-5451.55,-656.992,392.675,0.66789,0,'DunMorogh'),(305,-2600.48,-2350.81,82.9572,0.914501,0,'DunModr'),(304,5719.41,-4504.33,760.87,4.73111,1,'DunMandarr'),(303,-1256.99,-1189.47,38.9804,3.82979,0,'DunGarok'),(302,-8491.62,-3036.55,9.25275,3.88535,1,'DunemaulCompound'),(301,-4086.41,-2604.38,44.7943,1.58996,0,'DunAlgaz'),(300,-1003.14,-3841.03,148.832,5.54348,0,'DrywhiskerGorge'),(299,916.611,-4810.57,14.8334,6.28024,1,'DrygulchRavine'),(298,-2944.13,1891.22,33.9487,0.049356,1,'DreamBough'),(297,-3141.22,-4220.36,0.570819,5.34856,1,'DreadmurkShore'),(296,327.143,-2253.88,242.753,3.93818,1,'DreadmistPeak'),(295,317.4,-2209.99,213.239,4.9592,1,'DreadmistDen'),(294,-7734.77,-2609.01,165.137,4.22183,0,'DreadmaulRock'),(293,-11517.4,-2851.64,6.78542,4.15663,0,'DreadmaulPost'),(1425,-10874.4,-2682.6,9.46563,0.821115,0,'DreadmaulHold'),(291,2672.5,5852.8,-15.4945,1.64421,530,'DragonsEnd'),(290,-3453.27,-3655.72,58.0513,4.29267,0,'DragonmawGates'),(289,-4193.21,380.221,118.023,3.5046,530,'DragonmawFortress'),(288,-5105.71,600.108,85.5544,5.2386,530,'DragonmawBaseCamp'),(287,1413.82,6539.05,3.65071,3.14981,530,'Draenethystmine'),(286,-8187.12,-1019.58,145.084,3.11439,0,'Dracodar'),(285,9848.37,966.953,1306.38,3.77457,1,'Dolanaar'),(284,-67.3581,163.341,-3.46468,2.13943,429,'Dmwest'),(283,255.249,-16.0561,-2.58737,4.7,429,'Dmnorth'),(282,-201.11,-328.66,-2.72386,5.22313,429,'Dmeast'),(281,-3828.01,1250.22,160.226,3.20835,1,'DireMaulWest'),(280,-3521.29,1085.2,161.097,4.7281,1,'DireMaulNorth'),(279,-3980.8,789.005,161.007,4.71945,1,'DireMaulEast'),(278,-2838.55,-2875.5,32.5915,0.253382,0,'DireforgeHill'),(277,-6123.1,-2955.85,204.97,2.93259,0,'DigOne'),(276,-606.395,2211.75,92.9818,0.809746,1,'Desolace'),(275,-3754.58,4708.83,-17.8536,0.692921,530,'DerelictCaravan'),(274,-1089.35,4197.33,16.769,3.0523,530,'DenOfHaalEsh'),(273,-11117.5,1797.77,36.5497,2.64007,0,'DemontsPlace'),(272,1561.25,-2840.63,182.535,2.51311,1,'DemonFallRidge'),(271,1795.06,-3179.68,92.2109,3.17443,1,'DemonFallCanyon'),(270,23.0998,9.00932,-4.29664,3.18737,369,'DeeprunTram'),(269,368.992,1100.97,106.238,5.00081,0,'DeepElemMine'),(268,2033.17,5415.61,144.698,0.117995,530,'DeathsDoor'),(267,6545.51,-6406.76,32.4218,2.39547,530,'Deatholme'),(266,1841.39,1591.43,93.4301,2.39092,0,'Deathnell'),(265,1843.5,1590,93.2971,3.08757,0,'Deathknell'),(264,-3472.5,2006.64,96.8434,6.22837,530,'DeathforgeTower'),(263,3560.7,-1123.08,220.487,3.14152,1,'DeadwoodVillage'),(262,-10811.7,-1859.29,114.868,4.26659,0,'DeadwindRavine'),(261,-10438.8,-1932.75,104.617,4.77402,0,'DeadwindPass'),(260,-16.4,-383.07,61.78,2.52637,36,'Deadmines'),(259,-10461.4,-1744.82,88.1416,5.05985,0,'DeadmansCrossing'),(258,831.592,-5068.66,4.14613,4.95999,1,'DeadeyeShore'),(257,7828.77,-7846.07,168.986,4.5841,530,'DawnstarSpire'),(256,9994.48,-6487.35,1.20902,2.43127,530,'DawningLane'),(255,1469.48,-3686.9,77.7679,3.44337,0,'DarrowShire'),(254,1547.05,-2355.31,61.0999,2.91792,0,'DarrowmereLake'),(253,-318.143,-456.173,59.0946,0.914737,0,'DarrowHill'),(252,9949.56,2284.21,1341.4,1.59587,1,'Darnassus'),(251,4918.16,-4553.79,857.268,3.62056,1,'DarkwhisperGorge'),(250,-1111.52,-4776.58,4.40189,4.11253,1,'DarkspearStrand'),(249,5756.25,298.505,20.6049,5.96504,1,'Darkshore'),(248,-10573,-1182.51,28.0148,0.309022,0,'Darkshire'),(247,-2819.49,-2770.63,35.1771,1.85512,1,'DarkmistCavern'),(246,-589.891,5879.56,21.8492,5.17685,530,'DarkcrestShore'),(245,-389.634,6364.47,21.7062,5.20512,530,'DarkcrestEnclave'),(244,-4779.79,-1882.54,90.1741,3.77625,1,'DarkcloudPinnacle'),(243,1180.74,-330.016,43.949,0.365731,0,'DandredsFold'),(242,1845.91,-1563.5,59.1322,5.86316,0,'DalsonsTears '),(241,335.479,204.771,42.1124,3.42294,0,'DalaranCrater'),(240,3549.35,5684.65,-10.3382,5.88067,530,'DaggermawCanyon'),(239,1175.45,8135.31,18.8313,0.846961,530,'DaggerfenVillage'),(238,-1092.24,-2852.42,42.3323,4.65476,0,'DabyriesFarmstead'),(237,2687.72,5597.35,-11.2866,4.70098,530,'CursedHollow'),(236,-13304.6,-428.921,14.3756,4.78735,0,'CrystalveinMine'),(235,4004.78,4989.05,267.544,0.940518,530,'CrystalSpine'),(234,-9528.41,-359.942,61.0692,5.9536,0,'CrystalLake'),(233,-11052.9,-1568.93,27.233,4.64509,0,'Crypt'),(232,684.935,-583.82,163.055,3.81442,0,'CrushridgeHold'),(231,1797.5,689.077,44.7761,1.51755,0,'CrusadersOutpost'),(230,3039.71,-559.263,121.856,0.032519,0,'CrusadersOutpost'),(229,1864.38,-3675.68,153.909,4.56648,0,'CrownGuardTower'),(228,-4606.39,1359.61,139.903,2.40976,530,'CrimsonWatch'),(227,-4042.22,-13779.5,74.8069,2.37976,530,'CrashSite'),(226,1598.52,106.656,22.1495,4.00152,1,'CragpoolLake'),(225,10070,2293.83,1329.44,5.9784,1,'CraftmensTerrace'),(224,9888.38,-7178.39,31.0292,0.854246,530,'CourtOfTheSun'),(223,2988.21,1806.9,139.071,3.8591,530,'Cosmowrench'),(222,34.5932,-549.443,144.423,2.68188,0,'CorrahnsDagger'),(221,2012.28,-4470.7,73.6229,5.15472,0,'CorinsCrossing'),(220,-6231.77,332.993,383.171,0.480178,0,'ColdridgeValley'),(219,2109.16,603.695,35.2618,0.727451,0,'ColdHearthManor'),(218,-3010.81,1715.21,74.0269,6.16004,530,'CoilskarPoint'),(217,-2926.87,1333.17,8.17311,4.24367,530,'CoilskarCistern'),(216,735.066,6883.45,-66.2913,5.89172,530,'CoilfangReservoir'),(215,7344.08,-398.198,0.822346,3.29942,1,'CliffspringRiver'),(214,6892.58,-659.319,85.6375,3.11156,1,'CliffspringFalls'),(213,1836.86,-4397.85,3.58252,2.15931,1,'CleftOfShadow'),(212,-867.807,-1777.93,40.2461,5.60058,0,'CircleOfWestBinding'),(211,-1354.53,-2737.5,58.9458,5.9114,0,'CircleOfOuterBiding'),(210,-1539.11,-2168.63,17.3715,0.578985,0,'CircleOfInnerBindindg'),(209,-842.406,-3271.57,78.3724,0.432116,0,'CircleOfEastBinding'),(208,2839.44,5930.17,11.1002,3.16284,530,'CircleOfBloodArena'),(207,262.381,-1437.45,50.1333,4.13091,0,'ChillwindPoint'),(206,967.964,-1443.99,65.0399,2.05398,0,'ChillwindCamp'),(205,-5568.39,94.4882,483.947,2.57091,0,'ChillBreezeValley'),(204,2416.41,2171.83,90.9205,5.58696,530,'ChapelYard'),(203,-1816.67,4691.8,10.6813,0.383229,530,'CenarionThicket'),(202,-223.541,5487.99,23.2281,0.886755,530,'CenarionRefuge'),(201,-318.842,4718.99,18.4132,6.21599,530,'CenarionPost'),(200,-6818.09,733.814,41.5661,2.3082,1,'CenarionHold'),(199,10122.8,2540.84,1321.73,0.326671,1,'CenarionEnclave'),(198,-6834,763.39,43.0314,4.97156,1,'CenarioHold'),(197,4035.83,1435.5,-123.376,5.73225,530,'CelestialRidge'),(196,-8204.88,-4495.25,9.0091,4.72574,1,'CavernsOfTime'),(195,-8623.51,778.174,96.6514,0.703912,0,'CathedralSquare'),(194,-8581.98,807.885,106.518,0.688204,0,'CathedralOfLight'),(193,-2866.74,4738.97,-4.36527,0.870223,530,'CarrionHill'),(192,1650.58,200.023,-62.1773,1.41639,0,'Canals'),(191,-6838.66,-2833.16,241.666,1.16111,0,'CampWurg'),(190,-2363.11,-1913.78,95.7829,0.165556,1,'CampTaurajo'),(189,3216.07,3543.61,124.642,5.94353,530,'CampOfBoom'),(188,-2919.35,-264.535,53.6197,0.409027,1,'CampNarache'),(187,-4396.7,224.841,25.4136,4.93684,1,'CampMojache'),(186,-6264.36,-3703.95,244.144,4.65455,0,'CampKosh'),(185,-4619.13,-1234.54,-53.5973,3.48882,1,'CampEThok'),(184,-7166.43,-2390.08,240.945,5.77925,0,'CampCagg'),(183,-7022.26,-3609.21,241.879,4.08514,0,'CampBoff'),(182,-44.6129,-505.122,-46.1274,1.84172,1,'CampAparaje'),(181,1248.8,-2604.13,90.961,0.255412,0,'CaerDarrow'),(180,-7664.76,-1100.87,399.679,0.561981,469,'BWL'),(179,-8118.54,-1633.83,132.996,0.089067,0,'BurningSteppes'),(178,-2560.08,6248.7,17.9276,1.14689,530,'BurningBladeRuins'),(177,-171.297,-4362.51,68.0577,0.011192,1,'BurningBladeCoven'),(176,96.4462,1002.35,-86.9984,6.26671,564,'BT'),(175,2465.15,-5116.14,76.4285,5.12016,0,'BrowmanMill'),(174,-8008.65,1107.88,-0.668291,3.56178,1,'BronzebeardEncampment'),(173,3507.86,4615.89,216.47,0.378958,530,'BrokenWilds'),(172,-7997.8,-3891.14,11.7253,1.31317,1,'BrokenPillar'),(171,-467.983,3471.16,38.2596,4.92625,530,'BrokenHill'),(170,-3867.39,1131.48,154.788,6.16863,1,'BrokenCommons'),(169,-4505.81,-11623.1,11.3414,3.04968,530,'BristlelimbVillage'),(168,-2534.78,-12296.2,13.9722,6.17556,530,'BristlelimbEnclave'),(167,2259.25,290.43,34.1137,0.987414,0,'Brill'),(166,-10443.9,-830.975,49.7064,1.95754,0,'BrightwoodGrove'),(165,2539.96,-53.7843,28.1068,1.0229,0,'BrightwaterLake'),(164,-5385.04,310.278,394.151,5.19649,0,'BrewnallVillage'),(163,458.32,26.52,-70.67,4.95,230,'BRD'),(162,-2077.71,-2700.8,93.9102,5.72965,1,'Bramblescar'),(161,-3029.73,-1058.27,50.5731,4.53254,1,'BramblebladeRavine'),(160,-9757.58,-897.584,39.4214,5.20747,0,'BrackwellPumpkinPatch'),(159,-3130.67,-2908.43,34.0976,1.42798,1,'BrackenwallVillage'),(158,-69.2305,63.8333,48.9635,2.54067,1,'BoulderslideRavine'),(157,-100.856,251.61,100.892,0.777452,1,'BoulderslideCavern'),(156,3588.57,7143.33,140.447,1.56718,530,'BoulderMok'),(155,1329.13,-3598.07,91.7029,3.94133,1,'BoulderLodeMine'),(154,-1194.77,-2202.83,53.6903,1.52416,0,'BoulderGor'),(153,-1210.81,-2082.89,49.878,0.221184,0,'BoulderfistOutpost'),(152,-1197.66,-2130.17,61.2405,1.26105,0,'BoulderfistOuspost'),(151,-1968.97,-2773.94,79.3106,4.10542,0,'BoulderfistHall'),(150,3155.52,-3526.2,130.635,5.5267,1,'BoughShadow'),(149,40.0395,-28.613,-1.1189,2.35856,553,'Bota'),(148,-14297.2,530.993,8.77916,3.98863,0,'BootyBay'),(147,-2975.06,3481.44,-0.79203,5.2333,530,'BonechewerRuins'),(146,-2270.12,2505.58,74.147,3.50113,1,'BolgansHole'),(145,-349.384,7253.7,47.4862,3.07277,530,'BohaMuRuins'),(144,-235.037,-2566.93,118.73,1.11813,0,'BogensLedge'),(143,-3012.77,-1066.97,8.82117,0.006195,0,'BluegillMarsh'),(142,-2825.56,-3112.78,36.0223,0.327524,1,'Bluefen'),(141,-1944.5,-11873.7,49.3983,6.04835,530,'BloodWatch'),(140,5277.69,-512.571,322.307,3.04806,1,'BloodvenomRiver'),(139,5128.91,-343.506,355.035,3.39176,1,'BloodvenomPost'),(138,5282.47,-716.836,346.13,0.216695,1,'BloodvenomFalls'),(137,1641.62,-1448.83,147.085,2.65133,1,'BloodtoothCamp'),(136,539.523,6226.4,21.2966,0.731476,530,'BloodscaleGrounds'),(135,360.89,8136.18,23.3585,1.1674,530,'BloodscaleEnclave'),(134,-13470,675.488,10.4762,4.9884,0,'BloodsailCompound'),(133,-1993.62,-11475.8,63.9657,5.29437,530,'BloodmystIsle'),(132,1809.28,6411.22,-10.338,3.25427,530,'BloodmaulRavine'),(131,1623.51,6344.2,-2.54653,3.86891,530,'BloodmaulOutpost'),(130,3526.96,5794.43,1.72332,2.48461,530,'BloodmaulCamp'),(129,-2240.91,-399.174,-9.42446,2.53353,1,'BloodhoofVillage'),(128,-4324.33,-2671.96,32.864,1.86217,1,'BloodfenBurrow'),(127,-1933.1,-12871.3,85.896,5.32971,530,'BloodcurseIsle'),(126,7171.91,-6397.79,49.1227,5.62946,530,'BleedingZiggurat'),(125,-3273.56,5988.13,-2.58558,1.14452,530,'BleedingHollowRuins'),(124,-11182.5,-3016.67,7.42235,4.0004,0,'BlastedLands'),(123,2211.66,5712.69,272.876,0.564311,530,'BladspireOutpost'),(122,-1772.53,-11581.4,35.9727,2.16531,530,'Bladewood'),(121,1059.17,7087.83,118.027,0.112612,530,'BladeToothCanyon'),(120,933.847,5948.09,121.211,0.641137,530,'BladesRun'),(119,2639.37,6526.93,0.818903,6.17248,530,'BladespireHold'),(118,2209.1,6376.61,-10.338,3.27783,530,'BladespireGrounds'),(117,3037.67,5962.86,130.774,1.27253,530,'BladesEdgeMountains'),(1622,1427.99,-4891.11,11.1838,4.72046,1,'BladefistBay'),(115,2899.4,5081.33,264.302,1.15416,530,'BladedGulch'),(114,2493.53,-4222.79,74.027,1.21046,0,'BlackwoodLake'),(113,4621.7,33.6288,69.6487,4.35643,1,'BlackwoodDen'),(112,1398.08,179.4,12.3398,1.81183,1,'BlackwolfRiver'),(111,164.789,-475.305,116.842,0.022714,229,'BlackwingLair'),(110,3202.45,7117.62,173.86,3.63041,530,'BlackwingCoven'),(109,-3636.25,3191.57,312.961,4.99533,530,'BlackwindValley'),(108,-3663.45,3577.09,259.226,3.19599,530,'BlackwindLake'),(107,-3945.67,-1776.91,91.6706,0.595491,1,'BlackthornRidge'),(106,-3649.92,317.469,35.2827,2.94285,530,'BlackTemple'),(105,-2348.01,-11059.3,0.530463,2.53917,530,'BlacksiltShore'),(104,-7733.43,-1510.24,132.792,1.01584,0,'BlackrockStronghold'),(103,-7527.05,-1226.77,285.732,5.29626,0,'BlackrockSpire'),(102,-8520.94,-2559.99,133.334,3.27225,0,'BlackrockPass'),(101,-7494.94,-1123.49,265.547,3.3092,0,'BlackrockMountain'),(100,-7179.34,-921.212,165.821,5.09599,0,'BlackrockDepths'),(99,-2462.03,-3157.79,35.5825,4.91817,1,'BlackhoofVillage'),(98,4249.99,740.102,-25.671,1.34062,1,'BlackfathomDeeps'),(97,-7270.01,-804.3,295.929,2.43188,0,'BlackcharCave'),(96,-3528.34,-1339.17,10.2396,3.07711,0,'BlackChannelMarsh'),(95,4869.55,-7058.97,105.579,4.8442,1,'BitterReaches'),(94,-151.89,106.96,-39.87,4.53,48,'Bfd'),(93,-3.9967,14.6363,-44.8009,1.21574,542,'Bf'),(92,-2626.6,-12430.8,2.59312,3.08502,530,'BerylCoast'),(91,-370.444,897.319,132.97,1.03222,0,'BerensPeril'),(90,-10361.1,-1529.87,91.4594,5.96075,0,'BeggarsHaunt'),(89,-3974.75,-3814.87,37.7727,3.30494,1,'BeezilsWreck'),(88,3872.82,-4755.78,268.5,3.94022,1,'BearsHead'),(87,3665.57,-6014.97,5.71029,5.4152,1,'BayOfStorms'),(86,-13210.4,294.518,21.8575,4.92246,0,'BattleRing'),(85,3825.87,-120.149,-0.200964,5.18504,1,'BathransHaunt'),(84,3815.16,6110.93,265.862,1.15472,530,'BashirLanding'),(83,6712.31,-21.5758,42.8535,1.15594,1,'BashalAran'),(82,48.9976,-2715.55,91.6677,0.158612,1,'Barrens'),(81,-2537.88,-1692.24,4.30329,0.08867,0,'BaradinBay'),(80,9952.24,1538.42,1312.13,3.28133,1,'BanEthilHollow'),(79,9863.43,1558.24,1329.33,2.35456,1,'BanEthilBarrowDen'),(78,1996.05,-374.731,35.4522,5.19636,0,'BalnirFarmstead'),(77,-12019.5,363.339,2.1088,5.11095,0,'BallalRuins'),(76,-12551.3,-714.58,38.8789,5.67092,0,'BaliaMahRuins'),(75,-4134.29,-2192.52,50.7921,3.99548,1,'BaelModan'),(74,-4091.59,-2329.66,126.129,5.15315,1,'BaelDunKeep'),(73,-1942.74,413.534,134.205,2.53057,1,'BaelDunDigsite'),(72,-6779.2,-3423.64,241.667,0.647481,0,'Badlands'),(71,-4190.85,-12516.5,44.5276,1.34225,530,'AzureWatch'),(70,-4216.87,-12336.9,4.34247,6.02008,530,'AzuremystIsle'),(69,-902.924,244.914,8.80433,5.61973,0,'AzurelodeMine'),(68,9620.79,-8043.05,0.475485,2.05119,530,'AzurebreezeCoast'),(1730,2818.91,-4124.19,95.228,5.63091,1,'Azshara'),(65,-1512.97,-11378.7,67.4006,0.326728,530,'Axxarien'),(64,-215.738,-309.394,6.66761,3.07325,30,'AV'),(63,-3324.49,4943.45,-101.239,4.63901,530,'Auchindoun'),(62,-3637.1,5188.61,-19.1186,3.39151,530,'AuchenaiGrounds'),(61,-3362.04,5209.85,-101.05,1.60924,530,'AuchenaiCrypts'),(1529,6506.93,471.258,6.4263,5.19849,1,'Auberdine'),(59,-3336.2,363.236,120.466,0.150953,530,'AtaMalTerrace'),(58,2676.19,-422.905,107.123,0.648691,1,'Astranaar'),(57,1928.34,-2165.95,93.7896,0.205731,1,'Ashenvale'),(56,2779.37,3210.53,149.846,3.37686,530,'ArklonRuins'),(55,2865.62,3196.4,177.857,0.572978,530,'ArklonisRidge'),(54,-10456.7,-2139.85,90.7913,6.26152,0,'AridensCamp'),(53,3043.33,3681.33,143.065,5.07464,530,'Area52'),(52,-1.23165,0.014346,-0.204293,0.00984,552,'Arca'),(51,-1508.51,-2732.06,32.4986,3.35708,0,'ArathiHighlands'),(50,686.053,683.165,-12.9149,0.816022,529,'ArathiBasinHorde'),(49,1308.68,1306.03,-9.0107,3.91285,529,'ArathiBasinAlliance'),(48,1017.16,1040.59,-44.9017,0.325149,529,'ArathiBasin'),(47,-8213.48,2016,129.072,1.28414,531,'AQ40'),(46,-8437.74,1516.91,31.9074,2.73319,509,'AQ20'),(45,-6925.52,-2506.75,240.744,0.795898,0,'ApocryphansRest'),(44,7427.38,-7297.13,93.3437,0.18602,530,'AnTelas'),(43,6828.47,-7186.99,24.7617,4.91465,530,'AnOwyn'),(42,1593.28,8607.12,-32.6589,6.1963,530,'AngoRoshStronghold'),(41,1046.25,8503.63,20.2232,2.63767,530,'AngoRoshGrounds'),(40,-6398.46,-3166.67,299.812,0.769213,0,'AngorFortress'),(39,-3376.27,-2270.04,53.5307,4.14502,0,'AngerfangEncampment'),(38,7012.04,-6814.81,42.0817,5.87793,530,'AndilienEstate'),(37,7939.87,-6484.24,47.8099,3.93927,530,'AnDaroth'),(36,-2145.99,8844.79,19.8449,3.42056,530,'AncestralGrounds'),(35,-4021.4,-13582.1,54.7153,2.06953,530,'AmmenVale'),(34,-4260.24,-13021,1.0163,5.35482,530,'AmmenFord'),(33,-4155.37,-13498.5,48.356,5.92816,530,'AmmenFields'),(32,5674.5,128.459,32.3476,6.11808,1,'AmethAran'),(31,-1566.43,-10800.1,51.7294,0.885114,530,'AmberwebPass'),(30,-5537.72,-1336.54,398.664,0.237343,0,'AmberstillRanch'),(29,-129.094,835.621,63.598,4.83351,0,'Ambermill'),(28,6849.17,-7747.52,122.016,4.56069,530,'AmaniPass'),(27,7639.28,-7482.36,158.73,1.63194,530,'AmaniCatacombs'),(26,-9194.3,-2748.93,88.8711,1.84838,0,'AlthersMill'),(25,-818.155,-623.043,54.0884,2.15276,30,'AlteracValleyHorde'),(24,883.187,-489.375,96.7618,3.06932,30,'AlteracValleyAlliance'),(23,-215.738,-309.394,6.66761,3.07325,30,'AlteracValley'),(22,370.763,-491.355,175.361,5.37858,0,'AlteracMountains'),(21,-7616.9,-744.571,190.288,5.01897,0,'AltarOfStorms'),(20,-3053.96,828.896,-10.4689,5.53466,530,'AltarOfShatar'),(19,-2949.27,3958.32,0.285465,1.37205,530,'AllerianStronghold'),(18,-2593.73,3326.38,0.607573,4.93171,530,'AllerianPost'),(17,-4817.04,-2656.26,327.12,4.68835,0,'AlgazStation'),(16,-10656.1,1655,41.8732,2.86155,0,'AlexstonFarmstead'),(15,10405.3,883.631,1320.33,0.102033,1,'Aldrassil'),(14,-1746.58,5780.03,146.44,1.30629,530,'AldorRise'),(13,-2681.4,-4787.21,16.0751,4.69276,1,'AlcazIsland'),(12,451.572,-3342.23,119.689,0.772541,0,'AgolWatha'),(11,-7040.08,-3342.15,241.667,0.82338,0,'AgmondsEnd'),(10,2799.46,847.549,111.842,0.509892,0,'AgamandMills'),(9,-2020.72,-2107.58,91.9626,5.49717,1,'AgamaGor'),(8,-2059.85,8550.89,24.0587,1.944,530,'AerisLanding'),(7,260.366,-2125.21,119.565,3.18494,0,'AeriePeak'),(6,-10964,240.019,28.5578,2.99821,0,'AddlesStead'),(5,3742.43,2083.64,148.765,6.032,530,'AccessShaftZeon'),(4,-21.8975,0.16,-0.1206,6.28318,558,'AC'),(3,-7583.58,-3408.81,43.2019,2.47556,1,'AbyssalSands'),(2,-2068.19,7463.77,-18.9908,5.21755,530,'AbandonedArmory'),(1,1054.94,1055.23,-48.2358,0.313367,529,'AB'),(315,-3646.88,-3847.61,4.72729,6.08682,1,'DustwallowBay'),(316,-4043.65,-2991.32,36.3984,3.37443,1,'DustwallowMarsh'),(317,790.409,-4756.59,38.4724,0.283734,1,'DustwindCave'),(318,-8448.88,591.945,93.7917,0.293236,0,'DwarvenDistrict'),(319,2300.97,-4613.36,73.6231,0.367722,0,'EasternPlaguelands'),(320,-1011.45,-765.635,5.14928,2.23574,0,'EasternStrand'),(321,-8794.45,-3396.89,11.7786,3.88771,1,'EastmoonRuins'),(322,8748.09,-7067.4,37.1027,4.29194,530,'EastSanctum'),(323,-9450.82,-1299.92,42.7818,5.20197,0,'EastvaleLoggingCamp'),(324,3194.88,-4038.96,107.991,6.27156,0,'EastwallGate'),(325,2521.67,-4756.54,99.988,5.5003,0,'EastwallTower'),(1618,-1281.01,-5555.94,20.9241,2.5847,1,'DarkspearHold'),(327,-148.859,387.324,-38.5684,2.99526,70,'EchonokCaver'),(328,-8689.6,-114.525,88.8215,5.95046,0,'EchoRidgeMine'),(329,-4472.37,1671.36,162.888,3.88687,530,'EclipsePoint'),(330,4949.53,3041.26,98.7052,5.42963,530,'EcoDomeFarfield'),(331,3421.37,2800.02,150.087,0.705719,530,'EcoDomeMidrealm'),(332,4469.96,2840.21,140.442,1.97331,530,'EcoDomeSutheron'),(333,-1048.56,-274.104,159.03,5.4703,1,'ElderRise'),(334,-4186.72,1261.28,161.214,4.84131,1,'EldrethRow'),(335,-748.972,6563.11,171.754,2.21497,530,'ElementalPlateau'),(336,8624.53,-7693.87,158.81,2.13052,530,'ElrendarFalls'),(337,8039.45,-6241.13,16.2526,4.89432,530,'ElrendarRiver'),(338,-9617.06,-288.949,57.3053,4.72687,0,'ElwynnForest'),(339,-4998.67,-3837.87,44.2691,3.7377,1,'EmberstrifesDen'),(340,3986.71,-1293.58,250.144,5.74591,1,'EmeraldSanctuary'),(341,2063.11,1547.38,1162.1,1.87623,566,'EOTS'),(342,-443.408,2468.83,105.837,0.930524,1,'EthelRethor'),(343,4042.33,2326.74,113.447,3.1331,530,'EthereumStagingGrounds'),(344,2976.85,5511.01,144.637,3.86042,530,'Evergrove'),(345,6725.69,-4619.44,720.909,4.66802,1,'Everlook'),(346,9079.92,-7193.23,55.6013,5.94597,530,'EversongWoods'),(347,-6091.69,-3076.67,242.055,1.80555,0,'ExcavationCenter'),(348,-1384.5,2724.56,-28.568,3.37207,530,'ExpeditionArmory'),(349,-671.631,1865.18,66.8961,5.49722,530,'ExpeditionPoint'),(350,-10.8021,-1.15045,-2.42833,6.22821,550,'Eye'),(351,2167.44,1564.11,1159.35,0.254382,566,'EyeOfTheStorm'),(352,2487.72,1609.12,1224.64,3.35671,566,'EyeOfTheStormAlliance'),(353,1843.73,1529.77,1224.43,0.297579,566,'EyeOfTheStormHorde'),(354,8714.14,-6650.33,72.7517,2.39205,530,'FairbreezeVillage'),(355,-2906.3,-12222.4,17.3124,4.94957,530,'FairbridgeStrand'),(356,-600.782,4100.1,90.5571,0.207697,530,'FalconWatch'),(357,9514.33,-6822.1,16.4907,1.60979,530,'FalconwingSquare'),(358,-2074.65,-2113.64,15.3642,1.55343,0,'FaldirsCove'),(359,2666.18,-2545.89,173.4,1.26906,1,'FalfarrenRiver'),(360,1486.96,-2181.85,90.2204,0.965078,1,'FallenSkyLake'),(361,126.53,4847.85,74.3962,3.65942,530,'FallenSkyRidge'),(362,-9985.94,-3594.22,21.6782,5.47218,0,'FallowSanctuary'),(363,10158.8,-6001.84,110.154,3.59136,530,'FalthrienAcademy'),(364,-9870.52,212.959,14.0087,5.95753,0,'FargodeepMine'),(365,7544.57,-7667.85,151.258,6.22794,530,'farstriderEnclave'),(366,-5657.29,-4278.25,407.823,2.08396,0,'FarstriderLodge'),(367,9041.51,-7456.05,83.385,3.11721,530,'FarstriderRetrear'),(368,9850.54,-7396.79,13.6387,0.894016,530,'FarstridersSquare'),(369,276.636,-3689.5,27.8892,1.64403,1,'FarWatchPost'),(370,-4985.61,1590.89,63.7734,1.179,1,'FearScarVale'),(371,82.3052,-2086.59,112.764,3.16252,0,'FeatherbeardsHovel'),(1587,-4437.57,3276.7,11.8283,3.62933,1,'RuinsofFeathermoon'),(373,2038.72,-2981.96,105.815,4.69104,1,'FelfireHill'),(374,6851.06,-1912.89,550.904,3.00564,1,'FelpawVillage'),(375,10041.9,1031.59,1329.46,0.24968,1,'FelRock'),(376,1776.93,-1179.19,59.4284,3.30669,0,'FelstoneField'),(377,4102.25,-1006.79,272.717,0.790048,1,'Felwood'),(378,998.173,736.541,59.2738,6.16398,0,'FenrisIsle'),(379,-4841.19,1309.44,81.3937,1.48501,1,'Feralas'),(380,-110.357,7115.07,18.8779,3.49689,530,'FeralfenVillage'),(381,9889.14,-6647.39,10.5165,5.79591,530,'FethsWay'),(382,-3125.88,-2224.59,91.9413,3.27842,1,'FieldOfGiants'),(383,-7236.79,-1417.51,-231.997,0.628312,1,'FirePlumeRidge'),(384,2221.45,197.106,131.234,1.51093,1,'FireScarShrine'),(385,-6652.97,-832.567,244.531,1.11634,0,'FirewachtRidge'),(386,-2345.07,3253.53,-3.37535,4.84822,530,'FirewingPoint'),(387,-7501.51,-2183.08,165.926,6.07144,0,'FlameCrest'),(388,-9700.27,812.057,27.2225,5.46273,0,'ForestsEdge'),(389,3011.16,-3359.08,147.96,2.42202,1,'ForestSong'),(390,4742.7,3356.29,200.155,4.89556,530,'ForgeBaseGehenna'),(391,4444.94,3415.77,165.925,5.6074,530,'ForgeBaseOblivion'),(392,2940.65,4802.15,283.677,1.47618,530,'ForgeCampAnger'),(393,-1840.08,9236.04,71.6117,5.15551,530,'ForgeCampFear'),(394,-1275.81,8895.8,57.4377,2.96739,530,'ForgeCampHate'),(395,392.203,2195.63,118.621,5.07233,530,'ForgeCampMageddon'),(396,353.575,2497.05,158.244,6.02265,530,'ForgeCampRage'),(397,1540.72,7281.82,364.469,3.39479,530,'ForgeCampTerror'),(398,2910.37,6984.92,365.438,2.81124,530,'ForgeCampWrath'),(399,2750.63,-4597.28,183.965,1.62173,1,'ForlornRidge'),(400,-5661.05,1667.52,87.9256,5.3848,1,'FrayfeatherHighlands'),(401,-5431.78,-2449.38,89.2848,2.32854,1,'FreewindPost'),(402,6812.06,-2425.9,552.871,4.37848,1,'FrostfireHotSprings'),(403,-5542.53,501.507,387.248,2.21198,0,'FrostmaneHold'),(404,8091.41,-3882.58,696.668,2.18722,1,'FrostsaberRock'),(405,5259.73,-4781.05,690.81,4.5269,1,'FrostwhisperGorge'),(406,-6376.41,-1810.71,-263.646,4.96999,1,'FungalRock'),(407,-1125.17,5712.53,58.7895,2.46015,530,'FunggorCavern'),(408,-9925.14,1239.35,42.3457,0.815598,0,'FurlbrowsPumpkinFarm'),(409,3875.48,5223.77,270.997,0.390741,530,'FurywingsPerch'),(410,-7177.15,-3785.34,8.36981,6.10237,1,'Gadgetzan'),(411,1723.05,-2292.37,58.9754,2.73335,0,'GahrronsWithering'),(412,450.999,-627.906,167.189,1.86114,0,'GallowsCorner'),(413,-1321.34,7239.12,32.7371,4.04169,530,'Garadar'),(414,2789.48,366.372,24.4196,5.13839,0,'GarrensHaunt'),(415,-10902.7,-3179.32,49.0836,0.042708,0,'GarrisonArmory'),(416,-8255.03,1484.42,4.87199,3.44005,1,'GateOfAhnQiraj'),(417,-5040.5,-814.817,495.128,5.49652,0,'GateOfIronforge'),(418,-8216.06,1536.36,1.30797,3.0826,1,'GatesOfAhnQiraj'),(419,-55.4571,-103.292,132.406,5.47787,0,'GavinsNaze'),(420,-4615.2,-12917.5,9.74236,3.88613,530,'GeezlesCamp'),(421,-2182.62,2534.41,71.9615,2.22408,1,'GelkisVillage'),(422,7360.86,-6803.3,44.2942,5.83679,530,'Ghostlands'),(423,-1224.46,1728.53,90.0592,0.831707,1,'GhostWalkerPost'),(424,16226.2,16257,13.2022,1.65007,1,'GMIsland'),(425,9254.93,1636.45,1302.05,2.40247,1,'GnarlpineHold'),(426,-332.22,-2.28,-150.86,2.77,90,'Gnome'),(427,-5139.42,890.669,287.392,2.26618,0,'Gnomeregan'),(428,-7258.24,-584.973,-271.642,0.734342,1,'GolakkaHotSprings'),(429,-5783.67,-1620.21,358.878,4.148,0,'GolBolarQuarry'),(430,-10419.6,1907.61,7.48861,2.50341,0,'GoldCoastQuarry'),(431,8418.56,-6138.39,58.0933,4.69877,530,'GoldenboughPass'),(432,7895.49,-6188.39,19.5624,2.39911,530,'GoldenmistVillage'),(433,8652.87,-5683.02,1.22002,4.18198,530,'GoldenStrand'),(434,616.536,-2489.5,94.631,4.21857,1,'GoldRoad'),(435,-9448.55,68.236,56.3225,2.1115,0,'Goldshire'),(436,-3862.64,137.7,123.544,6.18747,1,'GordunniOutpost'),(437,-1117.72,3230.8,67.8104,3.69003,530,'GorGazOutpost'),(438,-1518.71,-3067.96,14.751,0.005637,0,'GoShekFarm'),(439,-2458.16,4935.8,33.7396,0.969919,530,'GrangolvarVillage'),(440,87.2504,-223.637,9.64243,4.2607,1,'GreatwoodVale'),(441,2185.38,-1580.47,85.9111,0.829216,1,'GreenpawVillage'),(1444,-4062.23,-3450.43,280.571,0.390264,0,'Grimbatol'),(443,-7011.77,-1729.52,234.099,4.78807,0,'GrimesiltDigSite'),(444,-4188.75,629.705,70.4892,4.89549,1,'GrimtotemCompound'),(445,108.029,-322.863,3.36205,4.62591,1,'GrimtotemPost'),(446,3616.06,6635.92,130.007,4.95354,530,'Grishnath'),(447,-5728.8,-3113.4,315.857,0.804162,0,'GrizzlepawRidge'),(448,262.459,-3043.31,96.5546,0.64736,1,'GroldomFarm'),(449,-12388.9,172.578,2.83358,1.91753,0,'GromgolBaseCamp'),(450,1924.94,-4205.16,42.0388,1.65038,1,'GrommashHold'),(451,-12365.7,175.632,2.77714,4.9248,0,'GrongolbaseCamp'),(452,-11108.1,-2326.63,118.375,4.28623,0,'GroshgokCompound'),(453,4986.02,92.278,52.7174,5.66255,1,'GroveOfTheAncients'),(454,216.885,-273.27,145.045,0.145041,0,'GrowlessCave'),(455,3707.86,2150.23,36.76,3.22,571,'AzjolNerub'),(456,62.7842,35.462,-3.9835,1.41844,565,'Gruul'),(457,3530.06,5104.08,3.50861,5.51117,530,'GruulsLair'),(458,-13277.4,127.372,26.1418,1.11878,0,'GurubashiArena'),(459,3373.87,4296.43,120.293,0.2091,530,'GyroPlankBridge'),(460,-1241.9,4096.71,70.6772,3.55888,530,'HaalEshiGorge'),(461,-1563.05,7945.67,-22.5568,1.13572,530,'Halaa'),(462,-1349.94,7925.93,-98.2172,2.31159,530,'HalaaniBasin'),(463,3280.67,-4322.2,132.301,3.1839,1,'HaldarrEncampment'),(464,78.5083,-225.044,49.839,4.67589,229,'HallOfBlackhand'),(465,-4675.79,-1256.32,501.993,1.974,0,'HallOfExplorers'),(466,221.322,74.4933,25.7195,0.484836,450,'HallOfLegends'),(467,1966.24,-4794.55,56.7642,6.241,1,'HallOfTheBrave'),(468,17.3566,231.68,-31.392,0.39445,70,'HallOfTheCrafters'),(469,-226.8,49.09,-46.03,1.39,70,'HallOfTheKeepers'),(470,-941.007,-3526.66,70.935,3.48668,0,'Hammerfall'),(471,-6427.04,-3381.19,229.711,4.254,0,'HammertoesDigsite'),(472,7065.06,-7687.02,113.903,5.96904,530,'HatchetHills'),(473,2793.09,-1621.4,129.333,1.98722,0,'Hearthglen'),(474,-390.863,3130.64,4.51327,0.218692,530,'HellfireCitadel'),(475,-211.237,4278.54,86.5678,4.59776,530,'HellfirePeninsula'),(476,-360.671,3071.9,-15.0977,1.89389,530,'HellfireRamparts'),(477,-5661.83,-1993.18,396.111,0.091415,0,'HelmsBedLake'),(478,-9127.98,-1052.89,70.604,2.11379,0,'HeroesVigil'),(479,3554.3,-6232.61,44.262,2.12438,1,'HetaerasClutch'),(480,1001.29,7586.72,22.1567,1.60094,530,'HewnBog'),(481,-807.114,-4111.26,55.5967,4.7919,1,'HiddenPath'),(1551,-5020.04,-941.02,0.304255,2.32642,1,'Highperch'),(483,-4610.67,1700.18,114.705,3.05218,1,'HighWilderness'),(484,-510.344,9.53799,50.345,1.33728,0,'HillsbradFields'),(485,-436.657,-581.254,53.5944,1.25917,0,'HillsbradFoothills'),(486,-17.942,-2805.92,122.271,2.75175,0,'HiriWatha'),(487,-6661.66,988.222,-14.6814,1.34146,1,'HiveAshi'),(488,-7678.21,611.804,-52.124,2.91382,1,'HiveRegal'),(489,-7152.81,1387.39,2.14026,2.78729,1,'HiveZora'),(490,-748.211,2681.52,100.35,5.7479,530,'HonorHold'),(491,-353.057,-1296.11,90.9438,4.37878,1,'HonorsStand'),(492,7225.84,-6628.42,55.2716,2.89705,530,'HowlingZiggurat'),(493,-1396.43,-115.322,158.935,2.92089,1,'HunterRise'),(494,9706.81,-6712.6,0.548797,4.38141,530,'HuntressOfTheSun'),(495,-8177.89,-4181.23,-167.552,0.913338,1,'HyjalSummit'),(496,-5195.11,-3.63924,386.111,1.74231,0,'IceflowLake'),(497,6153.06,-5050.74,787.233,5.86444,1,'IceThistleHills'),(498,-3794.35,2556.02,91.0218,1.65971,530,'IllidariPoint'),(499,4140.7,5429.55,274.427,2.81212,530,'InsidionsPerch'),(500,555.59,2804.49,217.804,6.20094,530,'InvasionPointAnnihilator'),(501,-2670.83,2691.26,102.277,4.91601,530,'InvasionPointCataclysm'),(502,4775.3,3487.7,116.091,5.1893,530,'InvasionPointOverlord'),(503,2758.5,-928.177,204.091,2.86339,1,'IrisLake'),(504,-5854.97,-1996.35,401.478,3.88489,0,'IronbandsCompound'),(505,-5650.98,-3826.83,321.919,4.87213,0,'IronbandsExcavationSite'),(506,-2887.82,-2224.53,23.6055,0.386887,0,'IronbeardsTomb'),(507,-4918.88,-940.406,501.564,5.42347,0,'Ironforge'),(508,-5843.28,-3417.03,-50.9329,2.95077,1,'IronstoneCamp'),(509,-6969.88,-2409.95,-216.546,5.0496,1,'IronstonePlateau'),(510,6463.17,-1577.12,438.992,4.84426,1,'IrontreeCavern'),(511,6223.18,-1151.23,382.803,3.19416,1,'IrontreeWoods'),(512,-5403.55,3055.7,1.70606,1.67518,1,'IsleOfDread'),(513,12947.4,-6893.31,5.68398,3.09154,530,'IsleOfQuelDanas'),(514,8042.94,-7548.78,150.119,0.139542,530,'IsleOfTribulations'),(515,-10661.8,-2527.18,26.8436,3.30055,0,'IthariussCave'),(516,3822.04,-727.42,324.477,1.00681,1,'JadefireGlen'),(517,6293.06,-609.351,468.671,2.9224,1,'JadefireRun'),(518,-3057.92,1837.42,29.4535,0.06035,1,'JademirLake'),(519,4785.13,-477.744,331.858,1.88488,1,'Jaedenar'),(520,4983.03,-6041.19,95.5841,1.88091,1,'JaggedReef'),(521,1793.51,6009.75,137.638,4.55488,530,'JaggedRidge'),(522,1253.77,-4525.58,20.9508,2.77973,1,'JaggedswineFarm'),(523,-14601.1,-234.16,24.7223,5.58923,0,'JagueroIsle'),(524,-14180.5,729.773,21.633,4.17552,0,'JaneirosPoint'),(525,-10004.7,1462.85,40.887,6.10526,0,'JangolodeMine'),(526,-9208.19,-618.418,60.4529,0.385121,0,'JasperlodeMine'),(527,-233.765,-4121.89,117.635,3.39306,0,'JinthaAlor'),(528,-9971.72,-115.655,26.3099,5.38026,0,'JoredsLanding'),(529,-12135,17.9116,-4.73151,4.10564,0,'KalaiRuins'),(530,-11091.2,-1992.38,49.816,0.95178,532,'Kara'),(531,-11118.9,-2010.33,47.0819,0.649895,0,'Karazhan'),(1439,-6744.02,-2545.36,241.881,5.31354,0,'NewKargath'),(533,2440.86,-3506.6,99.3423,4.69889,1,'KargathiaKeep'),(534,-2676.89,-12141.3,17.2281,5.52763,530,'KesselsCrossing'),(535,-5597.31,-483.398,396.981,3.17566,0,'Kharanos'),(536,-2850.34,6405.81,77.0322,3.64548,530,'KilsorrowFortress'),(537,2280.68,2273.98,94.6903,4.34107,530,'KirinVarVillage'),(538,-1273.07,1970.71,50.7823,3.981,1,'KodoGraveyard'),(539,-3030.09,-704.29,44.1878,5.32579,1,'KodoRock'),(540,-946.186,-4492.28,28.8956,0.54997,1,'KolgarCrag'),(541,-990.368,935.934,91.9163,3.14846,1,'KolgarVillage'),(542,-713.262,1455.17,90.526,3.72573,1,'KormeksHut'),(543,-11605.1,-645.993,29.5029,5.88457,0,'KurzensCompound'),(544,9356.42,1191.51,1252.81,6.20615,1,'LakeAlAmeth'),(545,8453.08,-7748.48,144.45,2.73684,530,'LakeElrendar'),(546,7794.49,-2635.94,460.244,3.286,1,'LakeEluneAra'),(547,-3161.76,3814.37,58.1972,4.55865,530,'LakeEreNoru'),(548,-9355.14,-2280.93,71.6411,0.104803,0,'LakeEverstill'),(549,3044.74,526.567,4.85566,3.02598,1,'LakeFalathim'),(550,-2471.75,4657.29,159.739,4.97727,530,'LakeJorune'),(551,6517.05,-4006.25,658.802,4.53634,1,'LakeKelTheril'),(552,2680.46,-5363.06,84.0912,2.11964,1,'LakeMennar'),(553,1716.21,-4585.15,76.217,4.78011,0,'LakeMereldar'),(554,-11825.1,-421.353,12.0978,2.58982,0,'LakeNazferiti'),(555,-9656.42,-2519.56,56.8919,0.022329,0,'LakeridgeHighway'),(556,-9266.59,-2188.77,64.0892,2.10205,0,'Lakeshire'),(557,-1708.2,8418.44,-21.4027,3.67976,530,'LakeSunspring'),(558,-6347.89,-1218.88,-270.355,2.6083,1,'Lakkari TarPits'),(559,-6430.22,-1052.96,-272.085,5.61795,1,'LakkariTarPits'),(560,-10219.4,-3922.28,0.821068,5.62501,1,'LandsEndBeach'),(561,-4171.38,145.112,57.0216,5.74763,1,'LarissPavilion'),(562,-832.056,7741.02,34.3061,4.44079,530,'LaughingSkullRuins'),(563,78.2437,-239.341,55.3424,4.77799,229,'LBRS'),(564,4525.1,-6359.32,126.976,4.83794,1,'LegashEncampment'),(565,-3291.28,2888,131.919,2.25661,530,'LegionHold'),(566,-6742.69,-4092.03,264.17,6.25284,0,'LethlorRavine'),(567,2279.65,-5310.01,87.0759,5.07618,0,'LightsHopeChapel'),(568,-5202.94,-2855.18,336.822,0.37651,0,'LochModan'),(569,-10091.5,1955.59,7.16286,0.077321,0,'Longshore'),(570,1214.77,731.732,32.9321,2.30374,0,'LordamareLake'),(571,-62.356,189.834,53.4352,2.43918,0,'LordamereInternmentCamp'),(572,-3920.81,-2842.47,45.0363,4.48854,1,'LostPoint'),(573,-8021.77,-5292.28,1.33809,4.35817,1,'LostRiggerCove'),(574,-7512.38,-1043.39,181.55,0.909809,0,'LothosRiftwaker'),(575,-2055.96,5305.2,-37.9105,4.02848,530,'LowerCity'),(576,-3904.46,3147.39,327.077,4.42121,530,'LowerVeilShilAk'),(577,-4847.14,434.406,16.2021,2.33115,1,'LowerWilds'),(578,-960.748,-2043.74,81.2893,5.9535,1,'LushwaterOasis'),(579,3194.97,163.192,5.48506,3.36212,1,'MaestrasPost'),(580,-8976.21,876.055,106.542,3.3507,0,'MageQuarter'),(581,395.308,3715.99,179.279,0.814673,530,'MagHarGrounds'),(582,-1283.99,8498.33,16.4938,1.76808,530,'MagHariProcession'),(583,512.08,3879.32,191.494,0.027839,530,'MagharPost'),(584,1736.52,96.2668,-62.2857,5.5036,0,'MagicQuarter'),(585,12884.6,-7317.69,65.5023,4.799,530,'MagistersTerrace'),(586,-3440.16,2547.11,58.5443,0.37951,530,'MagmaFields'),(587,-1733.94,897.726,90.7702,2.15654,1,'MagramVillage'),(588,190.035,3.38458,67.9651,3.15819,544,'magtheridon'),(589,-312.7,3087.26,-116.52,5.19026,530,'MagtheridonsLair'),(590,-198.834,-336.707,11.0481,5.48042,1,'MalakaJin'),(591,1404.44,1071.55,52.4562,1.01804,0,'MaldensOrchard'),(592,3902.61,4036.58,188.558,0.149414,530,'ManaforgeAra'),(593,3007.58,4216.86,160.794,2.8844,530,'ManaforgeBnaar'),(594,2463.21,2862.53,132.065,3.9824,530,'ManaforgeCoruu'),(595,3079.11,2211.89,160.351,3.59991,530,'ManaforgeDuro'),(596,3850.68,2061.16,254.219,5.65108,530,'ManaforgeUltris'),(597,-3104.18,4945.52,-101.507,6.22344,530,'ManaTombs'),(598,-2041.95,1915.12,70.474,0.545691,1,'MannorocCoven'),(599,-10342.5,-1250.62,35.3013,3.56761,0,'ManorMistmantle'),(600,-228.302,236.591,-50.6459,1.64438,70,'MapChamber'),(601,1019.69,-458.31,-43.43,0.31,349,'Maraorange'),(602,751.254,-606.421,-33.2458,0.089788,349,'Marapurple'),(603,-1419.13,2908.14,137.464,1.57366,1,'Maraudon'),(604,-1464.14,2615.21,76.7172,3.21357,1,'MaraudonOrange'),(605,-1188.37,2879.61,85.7888,5.07366,1,'MaraudonPurple'),(606,2927.64,-1420.34,150.782,1.13192,0,'MardenholdeKeep'),(607,-6152.25,-1087.6,-201.435,0.707637,1,'MarshalsRefuge'),(608,583.302,8485.48,21.6066,4.03254,530,'MarshlightLake'),(609,3452.17,-4992.52,196.839,5.44846,0,'MazraAlor'),(610,6163.22,-4432.47,660.866,1.60523,1,'Mazthoril'),(611,1082.04,-474.596,-107.762,5.02623,409,'MC'),(612,-28.906,0.680314,-1.81282,6.26827,554,'Mech'),(613,-3236.59,-12713.5,27.0563,3.45338,530,'MenagerieWreckage'),(614,-3977.14,-898.953,8.83729,0.524569,0,'MenethilBay'),(1499,-3749.15,-849.84,0.375742,3.65398,0,'MenethilHarbor'),(616,15.2235,-0.221107,-2.79687,0.007752,585,'Mgt'),(617,-2390.97,-11785.5,16.2604,2.27998,530,'Middenvale'),(618,3371.2,2883.39,143.893,1.73852,530,'MidrealmPost'),(619,-6967.2,-2857.38,241.814,2.5709,0,'MirageFlats'),(620,-6221.35,-3927.64,-58.7495,0.757735,1,'MirageRaceway'),(621,1587.03,874.207,183.222,3.18787,1,'MirkfallonLake'),(622,-9360.13,441.323,49.1944,2.7374,0,'MirrorLake'),(623,-9497.38,476.237,51.0552,6.1853,0,'MirrorLakeOrchard'),(624,6960.9,278.856,2.62734,3.54128,1,'MistsEdge'),(625,-13968.3,121.306,14.8902,5.42744,0,'MistvaleValley'),(626,-5355.18,-1046.42,394.441,0.123457,0,'MistyPineRefuge'),(627,-10855.8,-4114.24,22.2215,1.37632,0,'MistyReedPost'),(628,-9813.85,-4115.01,4.58721,5.54286,0,'MistyReedStrand'),(629,590.273,132.249,42.0482,0.487462,0,'MistyShore'),(630,-10170.6,-2528.78,27.3798,1.09358,0,'MistyValley'),(631,-12424.8,-148.732,15.055,3.21029,0,'MizjahRuins'),(632,-4915.65,-3958.31,298.303,0.553624,0,'MogroshStronghold'),(633,2210.93,4763.72,157.42,4.60576,530,'MokNathalVillage'),(634,1126.64,-459.94,-102.535,3.46095,230,'MoltenCore'),(635,-10986.7,1542.75,44.7858,2.62438,0,'Moonbrook'),(636,7654.3,-2232.87,462.107,5.96786,1,'Moonglade'),(637,7486.41,-2124.22,490.738,3.11294,1,'MoongladeHordeFlypoint'),(638,-3623.63,-12755.4,5.5687,0.798743,530,'MoongrazeWoods'),(639,-5074.81,-11092.5,26.8435,1.10189,530,'MoonwingDen'),(640,-8372.77,-2754.46,186.622,3.43486,0,'MorgansVigil'),(641,3790.49,-1681.81,251.595,3.87194,1,'MorlosAran'),(642,1035.62,-2106,122.946,1.60767,1,'MorshanBaseCamp'),(643,-12345.2,-966.453,12.2932,4.29021,0,'MoshoggOgreMound'),(644,-3750.76,-2902.37,12.3196,4.19214,0,'MosshideFen'),(645,-11.1808,0.991746,-0.9543,3.13767,557,'MT'),(646,-4573.79,-3173.15,34.0877,3.1231,1,'Mudsprocket'),(647,9755.35,-7304.14,24.4126,0.426204,530,'MuerderRow'),(648,-2192.62,-736.317,-13.3274,0.487569,1,'Mulgore'),(649,9755.35,-7304.14,24.4126,0.426204,530,'MurderEow'),(650,1902.31,-1363.98,99.987,0.929737,1,'MystralLake'),(651,-2593.97,-11700.4,11.4544,1.18828,530,'Mystwood'),(652,-1145.95,8182.35,3.60249,6.13478,530,'Nagrand'),(653,-4141.59,-4054.2,2.2126,5.28022,1,'NatsLanding'),(654,3021.64,-3402.99,298.22,2.97352,533,'Nax'),(655,3668.72,-1262.46,243.622,4.785,571,'Naxxramas'),(656,-2474.89,-11380.1,37.3419,1.59514,530,'Nazzivian'),(657,-13835.5,420.427,90.6791,5.33319,0,'NekManiWellspring'),(658,-1446.49,6346.65,37.5461,2.77876,530,'NesingwarySafari'),(659,-11609.3,-52.9532,10.9376,3.48911,0,'NesingwarysExpedition'),(660,-4452,-13871.9,101.029,3.69763,530,'NestlewoodHills'),(661,-4395.01,-13772.3,52.3834,5.03674,530,'NestlewoodThicket'),(662,-907.58,-979.134,30.3489,2.36926,0,'NethanderStead'),(663,-10999.8,-3380.08,62.2525,4.63501,0,'NethergardeKeep'),(664,4836.34,2892.29,144.631,3.419,530,'Netherstone'),(665,3830.23,3426.5,88.6145,5.16677,530,'Netherstorm'),(666,-3814.67,4264.16,5.66786,3.73006,530,'NetherwebRidge'),(667,-4122.58,800.66,6.54928,1.1217,530,'NetherwingFields'),(668,-5098.7,505.358,84.97,3.41328,530,'NetherwingLedge'),(669,-5145.07,711.023,43.4061,5.2221,530,'NetherwingMines'),(670,-4331.55,819.57,15.3479,5.87712,530,'NetherwingPass'),(671,7966.85,-2491.04,487.734,3.20562,1,'Nighthaven'),(672,2076.72,1158.54,39.13,3.26114,0,'NightmareVale'),(673,2529.43,-2143.92,197.679,3.09116,1,'NightRun'),(674,3396.9,-1526.19,165.357,3.32216,1,'NightsongWoods'),(675,2048.96,1813.93,105.345,1.84271,0,'NightWebsHollow'),(676,176.426,1309.76,190.18,0.556817,1,'NijelsPoint'),(677,-7017.57,-4339.67,11.159,6.02322,1,'NoonshadeRuins'),(678,3029.82,-4901.18,100.63,4.63322,0,'Northdale'),(679,-815.306,-2033.47,34.2293,3.32686,0,'NorthfoldManor'),(680,-5191.62,-2297.24,400.342,3.9438,0,'NorthGateOutpost'),(681,-4802.07,-2532.42,353.848,1.92022,0,'NorthGatePass'),(682,3162.59,-4323.12,132.34,4.87705,0,'NorthpassTower'),(683,-2861.79,-3423.24,39.0049,3.29711,1,'NorthPointTower'),(684,2365.94,-1511,101.787,0.282905,0,'NorthridgeLumberCamp'),(685,9287.65,-6701.97,22.026,1.3137,530,'NorthSanctum'),(686,-8921.09,-119.135,82.195,5.82878,0,'NorthshireValley'),(687,-9076.49,-344.655,73.4527,0.882288,0,'NorthshireVineyards'),(688,851.635,1581.33,32.3011,0.628498,0,'NorthTidesHollow'),(689,878.157,1883.93,1.85641,2.38779,0,'NorthTidesRun'),(690,-1998.12,-3680.54,21.1346,2.45298,1,'NorthwatchHold'),(691,-1155.9,8035.88,-86.9813,3.21951,530,'NorthwindCleft'),(692,2444.66,6990.76,367.981,2.20806,530,'ObsidiasPerch'),(693,-4677.05,-12439.1,16.2655,2.09541,530,'OdesyuLanding'),(694,2297.68,7293.12,365.617,1.28888,530,'Ogrila'),(695,-8404.3,-4070.62,-208.586,0.237038,1,'OldHillsbradFoothills'),(696,-8722.38,412.837,98.9042,5.44545,0,'OldTown'),(697,141.756,1465.7,114.441,1.55058,0,'OlsensFarthing'),(698,-3220.5,1892.94,49.3026,0.094122,1,'Oneiros'),(699,29.1607,-71.3372,-8.18032,4.43584,249,'onyxia'),(700,-4708.27,-3727.64,54.5589,3.72786,1,'OnyxiasLair'),(701,-7663.74,-1217.4,287.789,5.33945,0,'OrbOfCommand'),(702,958.66,7374.02,27.9079,6.12064,530,'OreborHarborage'),(1419,3585.69,217.532,-120.055,5.30438,571,'RubySanctumRaid'),(704,-2790.42,1265.35,74.8801,3.51465,530,'OronoksFarm'),(705,-7583.14,223.826,12.1392,5.11522,1,'OrtellsHideout'),(706,-2417.03,8314.75,-37.5225,3.13613,530,'Oshugun'),(707,5503.37,-4948.18,850.096,3.91821,1,'OwlWingThicket'),(708,-2400.94,295.281,65.7074,1.27785,1,'PalemaneRock'),(709,2308.92,-5075.61,69.9643,3.30119,0,'PestilentScar '),(710,350.535,-2519.39,194.666,0.207063,0,'PlaguemistRavine'),(711,3065.36,-3704,120.931,1.21752,0,'Plaguewood'),(712,3032.21,-3058.25,118.245,2.69521,0,'PlaguewoodTower '),(713,-4468.03,-11990.3,31.3148,0.665984,530,'PodCluster'),(714,-4430.11,-12641.6,18.6332,3.64265,530,'PodWreckage'),(715,-10615.7,-3842.2,19.601,0.237497,0,'PoolOfTears'),(716,282.878,3479.82,63.4083,0.305086,530,'PoolsOfAggonar'),(717,9543.02,1665.64,1298.2,5.67365,1,'PoolsOfArlithrien'),(718,571.187,8674.03,21.2322,4.86742,530,'PortalClearing'),(719,4270.15,2154.62,136.167,1.80184,530,'ProtectorateWatchPost'),(720,-1242.7,462.432,6.25075,1.75667,0,'PurgationIsle'),(721,-388.146,1543.67,18.1592,3.10171,0,'PyrewoodVillage'),(722,-186.082,8076.92,19.0829,5.03314,530,'QuaggRidge'),(723,226.318,-2777.59,123.356,0.59469,0,'QuelDanilLodge'),(724,3385.31,-4220.6,155.092,3.57879,0,'QuelLithienLodge'),(725,-1428.84,-11951.2,16.4308,6.12844,530,'RagefeatherRidge'),(726,1811.78,-4410.5,-18.4704,5.20165,1,'RagefireChasm'),(727,-3839.07,1760.37,142.462,4.58606,1,'RageScarHold'),(728,-1346.58,1653.44,68.8313,0.486942,543,'Ramp'),(729,170.849,2914.41,19.6981,0.530757,1,'RanazjarIsle'),(730,-2023.09,-3220.48,90.5983,1.07774,1,'RaptorGrounds'),(731,-3134.17,-3238.84,63.364,5.4904,0,'RaptorRidge'),(732,-956.664,-3754.71,5.33239,0.996637,1,'Ratchet'),(733,-1920.81,-732.852,2.29586,1.70032,1,'RavagedCaravan'),(734,-6160.61,1746.37,24.2906,0.89614,1,'RavagedTwilightCamp'),(735,-6191.56,1743.28,19.6888,0.630589,1,'RavagedTwilightCapm'),(736,2580.25,-6830.65,117.466,5.1403,1,'RavencrestMonument'),(737,-10742.2,330.574,38.2503,0.551712,0,'RavenHill'),(738,-10526.7,295.889,31.0954,6.27724,0,'RavenHillCemetery'),(739,-9.50862,-1569.46,194.606,5.78733,0,'RavenholdtManor'),(740,3565.61,6890.04,141.477,4.50586,530,'RavensWood'),(741,2667.31,-1855.58,188.309,5.72617,1,'RaynewoodRetreat'),(742,2830.32,5252.84,264.143,5.26235,530,'RazaansLanding'),(743,-4657.3,-2519.35,81.0529,4.54808,1,'RazorfenDowns'),(744,-4470.28,-1677.77,81.3925,1.16302,1,'RazorfenKraul'),(745,326.81,-4706.65,15.3665,4.16414,1,'RazorHill'),(746,406.553,-4227.69,25.8136,6.14752,1,'RazormaneGrounds'),(747,2432.62,5201.37,264.661,3.00927,530,'RazorRidge'),(748,-1248.26,3837.32,264.797,3.21797,530,'RazorthornRise'),(749,-1708.1,4028.77,60.0601,2.08778,530,'RazorthornShelf'),(750,-1550.52,3547.46,32.4329,2.05248,530,'RazorthornTrail'),(751,950.321,-4596.04,19.5531,3.94604,1,'RazorwindCanyon'),(752,-32.7738,2123.59,110.62,0.264264,530,'ReaversFall'),(753,-11322.4,-202.492,75.6362,0.432339,0,'RebelCamp'),(754,-3162.62,-490.493,30.0476,1.64305,1,'RedCloudMesa'),(755,-8955.78,-2303.5,132.459,0.783378,0,'RedridgeCanyons'),(756,-9551.81,-2204.73,93.473,5.47141,0,'RedridgeMountains'),(757,-975.866,-1089.96,44.4156,5.60771,1,'RedRocks'),(758,-2807.36,5077.65,-12.2418,3.23972,530,'RefugeeCaravan'),(759,-1246.61,-2529.32,20.6098,0.741709,0,'RefugePointe'),(760,4633.51,627.215,7.05298,5.68533,1,'RemtravelsExcavation'),(761,-9654.98,-3097.37,49.9266,5.39445,0,'RendersValley'),(762,-8977.54,-2013.25,136.611,1.06612,0,'RethbanCaverns'),(763,-557.226,-4581.27,9.5884,1.01724,0,'RevantuskVillage'),(764,3.81,-14.82,-17.84,4.23745,389,'RFC'),(765,2592.55,1107.5,51.29,4.74,129,'RFD'),(766,1943,1544.63,82,1.38,47,'RFK'),(767,-9759.42,-1370.87,57.8966,4.14011,0,'RidgepointTower'),(768,-3314.47,4942.72,-101.176,3.47559,530,'RingOfObservance'),(769,-11240.9,-2842.69,157.925,1.47842,0,'RiseOfTheDefiler'),(770,2081.75,7384.85,372.481,4.04039,530,'RivendarksPerch'),(771,-5485.38,-1628.67,27.7119,2.85653,1,'RoguefeatherDen'),(772,1458.68,106.137,-61.3152,3.93673,0,'RoguesQuarter'),(773,1303.13,361.856,-67.2945,4.43312,0,'RoyalQuarter'),(774,-13688.9,-309.139,6.95504,6.11545,0,'RuinsOfAboraz'),(775,629.684,-348.068,151.105,2.85588,0,'RuinsOfAlterac'),(776,1400.61,-1493.87,54.7844,4.08661,0,'RuinsOfAndorhal'),(777,-3258.09,1165.62,60.1821,3.18338,530,'RuinsOfBaaRi'),(778,4567.97,-492.526,300.884,5.2189,1,'RuinsOfConstellas'),(779,4004.29,-5411.78,114.943,4.03446,1,'RuinsOfEldarath'),(780,3443.76,3680.31,149.409,3.34755,530,'RuinsOfEnkaat'),(781,4639.58,2593.03,210.931,2.71213,530,'RuinsOfFarahlon'),(782,-5628.76,1443.64,54.1252,5.72722,1,'RuinsOfIsildien'),(783,-13353.7,0.40638,21.6159,3.28725,0,'RuinsOfJubuwal'),(784,-3732.7,456.872,104.143,4.0858,530,'RuinsOfKarabor'),(785,1923.69,234.217,49.0664,3.16311,0,'RuinsOfLordaeron'),(786,-1832.47,-12078.2,31.9203,5.82134,530,'RuinsOfLorethAran'),(787,7344.01,-914.641,32.6207,2.18634,1,'RuinsOfMathystra'),(788,-2877.75,2346,39.7117,0.306187,1,'RuinsOfRavenwind'),(789,-588.564,4803.37,35.1991,2.89915,530,'RuinsOfShaNaar'),(790,9738.47,-6678.72,0.626843,1.08358,530,'RuinsOfSilvermoon'),(791,-4934.06,3590.1,10.8445,0.100452,1,'RuinsOfSolarsal'),(792,-7842.54,-2159.07,133.439,6.05649,0,'RuinsOfThaurissan'),(793,-11651.4,647.492,50.5754,1.50754,0,'RuinsOfZulKunda'),(794,-12952.2,-800.204,63.3924,2.07461,0,'RuinsOfZulMamwe'),(795,-12901.5,-653.396,51.3323,4.29414,0,'RuinsOfZulManwe'),(796,8222.45,-6684.93,86.2016,1.2383,530,'RunestoneFalithas'),(797,8270.72,-7208.74,141.199,5.58077,530,'RunestoneShandor'),(798,8704.53,944.683,13.5177,2.99385,1,'RutTheranVillage'),(799,2964.84,5663.43,146.879,5.89082,530,'RuuanWeald'),(800,-10165.8,1099.65,36.7546,5.35127,0,'SaldeansFarm'),(801,8664.16,-6357.99,55.1721,2.0504,530,'SaltherilsHaven'),(802,-2723.47,-1626.34,12.0927,4.1374,0,'SaltsprayGlen'),(803,7534.77,-6413.43,13.0591,2.45016,530,'SanctumOfTheMoon'),(804,-4115.51,1120.54,44.5242,2.6318,530,'SanctumOfTheStars'),(805,7169.99,-7082.62,56.1741,5.68364,530,'SanctumOfTheSun'),(806,-7244.39,-2950.49,8.88702,5.65329,1,'SandsorrowWatch'),(807,-4677.53,3178.37,4.88728,4.77121,1,'SardorIsle'),(808,-52.4961,1009.64,93.8872,4.40432,1,'Sargeron'),(809,-1886.8,2888.14,49.9161,1.48894,1,'SarTherisStrand'),(810,2745.04,-2957.47,141.707,3.80433,1,'Satyrnaar'),(811,3704.1,-6063.61,1.4207,4.81044,1,'ScalebeardsCave'),(812,1994.43,5139.17,265.058,3.07523,530,'ScalewingShelf'),(813,1654.25,-4791.25,83.2546,4.22248,0,'ScarletBaseCamp'),(814,2872.6,-764.398,160.332,5.05735,0,'ScarletMonastery'),(815,2837.12,-511.285,105.317,5.81304,0,'ScarletWatchPost'),(816,1269.64,-2556.21,93.6088,0.620623,0,'Scholomance'),(817,-0.310414,0.107129,-100.538,2.94612,13,'ScottTest'),(818,-1414.71,1469.51,59.498,1.05066,1,'ScrabblescrewsCamp'),(819,-2185.93,5538.97,64.0729,2.82446,530,'ScryersTier'),(820,175.996,-5142.55,0.778698,4.49423,1,'ScuttleCoast'),(821,-7012.47,-1065.13,241.786,5.63162,0,'SearingGorge'),(822,-3955.23,-11727.3,-138.878,0.981744,530,'SeatOfTheNaaru'),(823,-813.097,-4880.08,18.995,4.42647,1,'SenjinVillage'),(824,-10624.5,1096.66,33.7641,1.31041,0,'SentinelHill'),(825,-10511.6,1053.2,59.0454,5.07245,0,'SentinelTower'),(826,-3461.11,-4125.85,17.099,2.25408,1,'SentryPoint'),(827,799.721,-3995.68,122.007,3.77399,0,'Seradane'),(828,-11311.5,-3407.92,7.4681,5.30724,0,'SerpentsCoil'),(829,820.025,6864.93,-66.7556,6.28127,530,'SerpentshrineCavern'),(830,-4.6811,-0.09308,0.0062,0.035342,556,'Sethekk'),(831,-3362.2,4664.12,-101.049,4.6605,530,'SethekkHalls'),(832,1609.4,728.543,68.0957,4.05141,0,'Sewers'),(833,-229.135,2109.18,76.8898,1.267,33,'SFK'),(834,-40.8716,-19.7538,-13.8065,1.11133,540,'SH'),(835,-1866.38,655.098,107.241,3.87731,1,'ShadowbreakRavine'),(836,-234.675,1561.63,76.8921,1.24031,0,'ShadowFangKeep'),(837,10334,833.902,1326.11,3.62142,1,'Shadowglen'),(838,1664.29,1679.34,120.53,3.2093,0,'ShadowGrave'),(839,4895.81,-392.013,351.471,5.9611,1,'ShadowHold'),(840,-3627.9,4941.98,-101.049,3.16039,530,'ShadowLabyrinth'),(841,-2998.66,2568.9,76.6306,0.551303,530,'ShadowmoonVillage'),(842,-1664.79,3091.67,30.5552,6.07818,1,'ShadowpreyVillage'),(843,2890.66,-4019.71,138.135,3.34097,1,'ShadowsongShrine'),(844,10746.2,926.841,1336.47,6.24541,1,'ShadowthreadCave'),(845,-2896.28,5394.74,-8.88803,1.60241,530,'ShadowTomb'),(846,-324.286,-2900.4,77.795,2.67714,0,'ShadraAlor'),(847,-3707.79,-2530.37,68.2635,3.31945,1,'ShadyRestInn'),(848,7702.12,-5708.67,3.11034,3.51184,530,'ShalandisIsle'),(849,-5486.8,3630.93,2.10077,3.84603,1,'ShalzarusLair'),(850,-385.521,4797.07,19.9065,3.15127,530,'ShaNaariWastes'),(851,98.8545,-4379.69,121.322,0.049959,0,'ShaolWatha'),(852,2693.44,7110.67,365.1,0.37965,530,'ShartuulsTransporter'),(853,-3747.08,5393.6,-3.99659,1.34873,530,'ShaTariBaseCamp'),(854,-2607.52,1401.71,79.9574,5.04617,530,'ShatteredPlains'),(855,12947.4,-6893.31,5.68398,3.09154,530,'ShatteredSunStaging'),(856,286.357,1469.6,-11.4535,2.41453,530,'ShatterPoint'),(857,5654.65,-883.661,377.91,2.25167,1,'ShatterScarVale'),(858,7367.77,-1560.74,163.446,2.55011,1,'ShatterspearVillage'),(859,-1838.16,5301.79,-12.428,5.9517,530,'Shattrath'),(860,-5316.24,-215.783,440.907,3.74507,0,'ShimmerRidge'),(861,227.928,-2604.95,159.771,3.53715,0,'ShindiggersCamp'),(862,-14140.4,458.677,3.71587,0.997797,0,'ShouthernSavageCoast'),(863,10400.1,-5956.19,40.7994,0.977561,530,'ShrineOfDathremar'),(864,7841.73,-2209.34,471.118,5.23693,1,'ShrineOfRemulos'),(865,4844.91,-602.987,308.754,2.21868,1,'ShrineOfTheDeceiver'),(866,-240.214,-4013.38,187.304,2.54097,1,'ShrineOfTheDormantFlame'),(867,-7426.87,1005.31,1.13359,2.96086,1,'Silithus'),(868,-3126.44,-11957.6,4.28641,1.8119,530,'SiltingShore'),(869,9487.69,-7279.2,14.2866,6.16478,530,'SilvermoonCity'),(870,-5094.34,-11129.6,25.058,2.78659,530,'SilvermystIsle'),(871,878.74,1359.33,50.355,5.89929,0,'SilverpineForest'),(872,-4813.81,-2970.08,320.757,3.14151,0,'SilverStreamMine'),(873,2137.3,-1189.05,98.308,3.23488,1,'SilverwindRefuge'),(874,1486.44,-1866,113.941,2.75186,1,'SilverwingGrove'),(875,1810.07,-2001.61,103.117,3.68491,1,'SilverwingOutpost'),(876,1617.43,5337.21,265.469,1.44789,530,'SingingRidge'),(877,428.398,467.608,97.4092,1.555,1,'SishirCanyon'),(878,3604.04,4963.59,267.724,0.788938,530,'Skald'),(879,-4089.38,1787.12,100.382,5.96761,530,'SkethLonBaseCamp'),(880,-3055.5,2210.29,66.5523,5.54036,530,'SkethLonWreckage'),(881,-3674.85,3912.42,298.476,5.27651,530,'SkethylMountains'),(882,-3770.72,3684.42,277.236,4.34424,530,'Skettis'),(883,9519.69,-6531.37,21.9874,3.06352,530,'SkulkingRow'),(884,369.856,-3802.84,170.093,3.58942,0,'SkulkRock'),(885,1446.31,-4879.79,11.3317,0.477731,1,'SkullRock'),(886,2509.9,7322.28,374.229,1.60016,530,'SkyguardOutpost'),(887,-5.10147,0.126865,-1.12788,3.08034,555,'SL'),(888,-3345.84,2112.94,102.374,4.71491,530,'SlagWatch'),(889,846.782,-566.79,142.518,0.268342,0,'SlaughterHollow'),(890,-10639.3,-2092.15,101.664,2.48375,0,'SleepingGorge'),(891,-7660.35,-3011.37,132.992,5.02368,0,'SlitherRock'),(892,1610.83,-323.433,18.6738,6.28022,189,'SMArmory'),(893,855.683,1321.5,18.6709,0.001747,189,'SMCath'),(894,1702.01,1053.5,18.4922,1.46594,189,'SMGY'),(895,255.346,-209.09,18.6773,6.26656,189,'SMLib'),(896,4810.01,3843.66,223.376,6.06345,530,'SocretharsSeat'),(897,2312.13,1405.46,33.3337,1.15155,0,'SollidenFarmstead'),(898,1257.8,-1705.34,60.6843,3.72296,0,'SorrowHill'),(899,-10498.9,-4189.17,23.3262,4.97345,0,'Sorrowmurk'),(900,-4357.47,4612.63,-39.8305,1.7972,530,'SorrowWingPoint'),(901,3549.42,5593.04,-3.47566,3.88262,530,'SoulgrindersBarrow'),(902,-8665.15,-4725.35,0.656339,4.02044,1,'SouthbreakShore'),(903,-2333.57,-2295.55,92.6909,4.76754,1,'SouthernBarrens'),(904,-1696.57,-2553.96,91.2953,3.2423,1,'SouthernGoldRoad'),(905,-823.97,3345,97.486,5.93392,530,'SouthernRampart'),(906,-13668.6,471.263,37.4754,1.95598,0,'SouthernSavageCoast'),(907,2815.68,-3790.07,88.207,2.8713,1,'SouthfuryRiver'),(908,-5438.02,-2441.87,400.492,2.49866,0,'SouthGateOutpost'),(909,-5645.3,-2540,373.388,0.840292,0,'SouthGatePass'),(910,-9317.12,-3016.24,9.98814,0.142927,1,'SouthmoonRuins'),(911,-694.861,406.638,74.9142,6.13966,0,'SouthpointTower'),(912,3166.38,-5784.64,6.85091,5.34844,1,'SouthridgeBeach'),(913,-853.221,-533.529,9.98556,0.242866,0,'Southshore'),(914,-2033.15,7551.71,-92.349,4.13842,530,'SouthwindCleft'),(915,-7108.06,320.415,18.0411,2.87455,1,'SouthwindVillage'),(916,130.446,-127.482,-1.59053,1.86731,547,'SP'),(917,-1326.36,2371.99,88.9503,6.19165,530,'SpinebreakerPost'),(918,-13772.4,-3.7118,41.9401,5.65521,0,'SpiritDen'),(919,-2423.72,8040.33,-43.698,1.98013,530,'SpiritFields'),(920,-1013.67,224.643,134.661,1.12784,1,'SpiritRise'),(921,-866.113,-4284.06,76.167,3.62166,1,'SpiritRock'),(922,-10385.2,-2600.12,21.7007,4.60431,0,'SplinterspearJunction'),(923,2270.94,-2538.19,93.9198,0.060429,1,'SplintertreePost'),(924,-4998.14,-2219.52,-53.2078,2.3798,1,'SplithoofCrag'),(925,-5037.83,-2381.29,-54.2846,5.10748,1,'SplithoofHold'),(926,209.095,8547.4,23.1083,5.33054,530,'Sporeggar'),(927,9.83957,2.74496,822.096,0.106614,548,'SSC'),(928,-319.24,99.9,-131.85,3.19,109,'ST'),(929,-10786.7,-3743.04,24.4991,3.0021,0,'StagalbogCave'),(930,-6446.24,211.361,4.05948,4.18068,1,'StaghelmPoint'),(931,9838.87,385.538,1307.78,0.769623,1,'StarbreezeVillage'),(932,7165.69,-4054.54,724.251,1.31543,1,'StarfallVillage'),(933,-6908.08,-4801.39,8.15214,5.07916,1,'SteamwheedlePort'),(934,-5476.73,-666.917,392.674,3.48888,0,'SteelgrillsDepot'),(935,-10798.9,1381.56,31.0576,0.622382,0,'StendelsPond'),(936,-3378.24,-12347.1,22.5696,0.068326,530,'StillpineHold'),(937,2296.74,825.657,33.764,4.40704,0,'StillwaterPond'),(938,9266.84,-7240.15,16.4273,2.81695,530,'StillwhisperPond'),(939,-10446.9,-3261.91,20.1795,5.02142,0,'Stonard'),(940,-2640.08,4404.38,35.1,4.14529,530,'StonebreakerHold'),(941,-1951.33,-537.594,-13.1229,3.48631,1,'StonebullLake'),(942,-8982.69,-1206.38,72.206,3.0602,0,'StoneCairnLake'),(943,-4339.1,-3318.75,34.2536,2.30041,1,'StonemaulRuins'),(944,-5882.25,-2921.18,366.988,2.04588,0,'StonesplinterValley'),(945,1570.92,1031.52,137.959,3.33006,1,'StonetalonMountains'),(946,2678.38,1497.46,233.869,6.26038,1,'StonetalonPeak'),(947,346.86,3069.39,23.6589,3.0193,530,'StonewallCanyon'),(948,-9323.5,-3030.84,132.559,2.94713,0,'Stonewatch'),(949,-9569.7,-3272.71,48.6139,0.622368,0,'StonewatchFalls'),(950,-9386.59,-3039.03,139.437,4.8054,0,'StonewatchKeep'),(951,-9298.19,-2972.84,124.909,1.3418,0,'StonewatchTower'),(952,-4750.07,-3328.02,310.257,4.61609,0,'StonewroughtDam'),(953,7612.5,-2906.39,463.678,3.8829,1,'StormrageBarrowDens'),(954,-8833.38,628.628,94.0066,1.06535,0,'Stormwind'),(955,659.762,-959.316,164.404,0.433716,0,'Strahnbrad'),(956,-12644.3,-377.411,10.1021,6.09978,0,'StranglethornVale'),(957,3387.95,-3380.34,142.76,0.005913,329,'Strat'),(958,3352.92,-3379.03,144.782,6.25978,0,'Stratholme'),(959,-1551.2,-1808.1,67.5219,3.119,0,'StromgardeKeep'),(960,7960.27,-7257.01,136.908,0.234555,530,'SuncrownVillage'),(961,7154.17,-7070.49,54.9794,5.56207,530,'SunctumOfTheSun'),(962,-2873.95,-1496.04,9.47361,1.04607,0,'SundownMarsh'),(963,2498.92,2368.74,134.379,4.71414,530,'SunfuryHold'),(964,9982.85,-7062.83,45.3625,0.873881,530,'SunfurySpire'),(965,7631.29,-7338.48,272.726,3.43678,530,'SungrazePeak'),(966,966.147,926.499,104.649,1.27231,1,'SunRockRetreat'),(967,8715.52,-6101.45,20.1472,0.173306,530,'SunsailAnchorage'),(968,-1520.44,8552.38,7.26028,1.69728,530,'SunspringPost'),(969,10331.1,-6235.42,26.7759,1.94594,530,'SunstriderIsle'),(970,12574.1,-6774.81,15.0904,3.13788,530,'SunwellPlateau'),(971,1.60675,8.07684,-4.12796,4.467,545,'SV'),(972,-2959.77,-3881.9,32.9334,5.43104,1,'SwamplightManor'),(973,-10345.4,-2773.42,21.99,5.08426,0,'SwampOfSorrows'),(974,104.534,5199.31,21.1033,4.22156,530,'SwampratPost'),(975,1784.23,924.563,15.581,3.5177,580,'SWP'),(976,2018.91,6854.47,171.409,0.087216,530,'Sylvanaar'),(977,-6547.88,-3855.96,-58.7492,5.31404,1,'TahondaRuins'),(978,6209.51,-1927.01,569.393,3.82137,1,'TalonbranchGlade'),(979,-1213.24,-12436.8,94.9062,4.32594,530,'TalonStand'),(980,2735.06,-3867.44,98.6548,3.56139,1,'TalrendisPoint'),(981,-7931.2,-3414.28,80.7365,0.66522,1,'Tanaris'),(982,-7214.21,-1732.2,244.448,3.61783,0,'TannerCamp'),(983,-34.1467,-923.366,54.5576,0.15019,0,'TarrenMill'),(984,-2560.76,7300.72,13.9485,2.18422,530,'Telaar'),(985,-1953.56,7012.48,-88.5123,4.2248,530,'TelaariBasin'),(986,-1664.4,-10916.8,58.8456,5.88417,530,'TelathionsCamp'),(987,10111.3,1557.73,1324.33,4.04239,1,'Teldrassil'),(988,278.582,6001.27,144.73,1.53156,530,'Telredor'),(989,3099.36,1518.73,190.3,4.72592,530,'TempestKeep'),(990,3967.58,-7219.97,17.0967,2.6396,1,'TempleOfArkkoran'),(991,-10450.3,-3825.44,18.0679,6.03616,0,'TempleOfAtalHakkar'),(992,78.9769,4333.58,101.553,0.011347,530,'TempleOfTelhamat'),(993,9671.4,2524.02,1333.11,3.23265,1,'TempleOfTheMoon'),(994,3547.67,-5279.93,107.188,4.80808,1,'TempleOfZinMalor'),(995,-2000.47,4451.54,8.37917,4.40447,530,'TerokkarForest'),(996,-3781.87,3531.08,285.715,4.45498,530,'TerokksRest'),(997,-1874.51,5429.28,-10.4637,0.084994,530,'TerraceOfLight'),(998,2844.08,-691.716,139.331,5.27247,0,'TerraceOfRepose'),(999,2957.87,-2794.79,110.464,1.19003,0,'Terrordale'),(1000,-8243.79,-1081.22,-201.915,2.1135,1,'TerrorRun'),(1001,2739.7,-2465.84,73.5058,5.39901,0,'TerrorwebTunnel'),(1002,-7585.46,-2720.35,134.451,5.65201,0,'TerrorWingPath'),(1003,43.7346,1785.5,110.983,2.83589,1,'TethrisAran'),(1004,-4525.63,-791.364,-42.3639,1.09938,1,'Thalanaar'),(1005,4337.93,-6105.19,122.737,4.99736,1,'ThalassianBaseCamp'),(1006,6480.81,-6851.1,100.15,3.10746,530,'ThalassianPass'),(1007,-2363.54,-2503.22,88.3441,1.25443,0,'ThandolSpan'),(1008,-3597.86,1879.9,47.2408,4.94268,530,'TheAltarOfDamnation'),(1009,-4559.69,1014.4,10.9432,0.447663,530,'TheAltarOfShadows'),(1010,-271.689,-3438.52,187.18,3.93027,0,'TheAltarOfZul'),(1011,1514.38,323.128,-62.1661,2.31096,0,'TheApothecarium'),(1012,3308.92,1340.72,505.56,4.94686,530,'TheArcatraz'),(1013,884.54,-3548.45,91.8532,2.95957,1,'TheBarrens'),(1014,-1366.97,5905.97,191.618,5.17124,530,'TheBarrierHills'),(1015,9687.22,-7175.47,13.9257,1.37183,530,'TheBazaar'),(1016,-8734.3,-4230.11,-209.5,2.16212,1,'TheBlackMorass'),(1017,-1218.3,-12837.2,0.001255,3.77931,530,'TheBloodcursedReff'),(1018,-291.324,3149.1,31.5541,2.27147,530,'TheBloodFurnace'),(1019,-3537.02,4572.27,-20.9226,3.55964,530,'TheBoneWastes'),(1020,3407.11,1488.48,182.838,5.59559,530,'TheBotanica'),(1021,1711.99,-719.761,54.3351,4.66387,0,'TheBulwark'),(1022,-14087,290.316,15.7614,3.9234,0,'TheCapeOfStranglethorn'),(1023,-6939.52,-1263.21,179.709,0.200595,0,'TheCauldron'),(1024,721.568,1494.15,-18.1516,1.85895,1,'TheCharredValed'),(1025,10303.8,1198.57,1457.46,3.0779,1,'TheCleft'),(1026,-9498.58,-1921.71,78.7092,0.662431,0,'TheCorners'),(1027,120.332,-3456.52,108.157,4.37088,0,'TheCreepingRuin'),(1028,-2510.36,-12488.4,2.03858,4.73043,530,'TheCrimsonReach'),(1029,-452.84,-2650.76,95.5209,0.241081,1,'TheCrossroads'),(1030,-2054.45,-11389.9,61.4746,2.05143,530,'TheCryocore'),(1031,-3821.99,-11489.4,-138.564,1.09563,530,'TheCrystalHall'),(1032,-14081.3,-144.113,3.68835,4.61063,0,'TheCrystalShore'),(1033,-6248.2,1695.54,6.3349,3.55227,1,'TheCrystalVale'),(1034,-11276.9,1428.07,89.7866,1.29233,0,'TheDaggerHills'),(1035,-9966.63,-614.729,34.8066,5.94737,0,'TheDarkenedBank'),(1036,-11840.1,-3196.63,-29.6059,3.3391,0,'TheDarkPortal'),(1037,-247.29,910.638,84.3798,1.49341,530,'TheDarkPortalOutland'),(1038,1173.43,372.651,33.9918,4.07796,0,'TheDawningIsles'),(1039,-10821.8,869.442,33.0338,0.631022,0,'TheDeadAcre'),(1040,1064.64,1551.05,28.3425,3.01297,0,'TheDeadField'),(1041,-11208.7,1673.52,24.6361,1.51067,0,'TheDeadmines'),(1042,664.515,5349.82,-23.1373,0.875759,530,'TheDeadMire'),(1043,9013.29,-6930.75,17.8158,2.99131,530,'TheDeadScar'),(1044,-3435.61,2044.34,79.0861,5.47439,530,'TheDeathforge'),(1045,658.574,1011.34,44.6068,5.4367,0,'TheDecrepitFerry'),(1046,-603.88,-4191.68,41.0998,1.55607,1,'TheDen'),(1047,-4303.61,-3041.87,29.9607,1.55428,1,'TheDenOfFlame'),(1048,1775.99,-2663.07,109.366,4.96353,1,'TheDorDanilBarrowDen'),(1049,1913.54,-4490.07,23.16,4.13223,1,'TheDrag'),(1050,-4558.57,-2934.04,35.306,4.21758,1,'TheDragonmurk'),(1051,-2273.25,-1723.87,-56.5399,1.61234,0,'TheDrownedReef'),(1052,748.603,-1348.11,92.6076,0.122718,1,'TheDryHills'),(1053,-6614.72,-2629.53,265.833,0.843023,0,'TheDustbowl'),(1054,-11110.5,600.244,36.5205,4.15746,0,'TheDustPlains'),(1055,-3965.7,-11653.6,-138.844,0.852154,530,'TheExodar'),(1056,3088.49,1381.57,184.863,4.61973,530,'TheEye'),(1057,-3536.55,1631.94,45.0453,4.57511,530,'TheFelPits'),(1058,-3452.24,2747.76,127.219,1.28272,530,'TheFetidPool'),(1059,-4347.46,2415.11,8.00515,1.52603,1,'TheForgottenCoast'),(1060,141.278,-1948.08,94.318,3.13864,1,'TheForgottenPools'),(1061,-4624.25,-1097.17,501.273,3.00102,0,'TheForlornCavern'),(1062,2448.89,-3708.71,177.867,5.66288,0,'TheFungalVale'),(1063,-8877.71,-3953.51,9.90049,4.17595,1,'TheGapingChasm'),(1064,-1059.43,-678.757,-53.0876,3.4738,1,'TheGoldenPlains'),(1065,2874.85,-765.215,160.333,5.09183,0,'TheGrandVestibule'),(1066,-13226.2,228.625,33.1421,1.37088,0,'TheGreatArena'),(1067,-630.013,3967.65,28.9957,5.30639,530,'TheGreatFissure'),(1068,-4792.95,-1119.12,498.806,2.17085,0,'TheGreatForge'),(1069,-4608.03,-1851.45,86.1002,3.08268,1,'TheGreatLift'),(1070,-3372.12,-3082.42,22.0718,2.24279,0,'TheGreenBelt'),(1071,-715.077,1503.82,11.4471,2.58024,0,'TheGreynameWall'),(1072,-7390.69,-941.553,169.43,3.90454,0,'TheGrindingQuarry'),(1073,-5679.22,-294.234,369.284,0.35122,0,'TheGrizzledDen'),(1074,-3674.68,1344.99,275.705,3.38664,530,'TheHandOfGulDan'),(1075,-10126,-2834.73,22.2157,0.674244,0,'TheHarborage'),(1076,-16.6821,-333.396,131.146,0.8126,0,'TheHeadland'),(1077,2662.78,3766.07,143.16,2.69827,530,'TheHeap'),(1078,7645.61,-4931,697.11,0.777426,1,'TheHiddenCove'),(1079,119.387,-3190.37,117.331,2.34064,0,'TheHinterlands'),(1080,3156.34,-1176.21,216.451,4.79547,1,'TheHowlingVale'),(1081,-10322.3,635.42,26.6725,4.82819,0,'TheHushedBank'),(1082,1918.81,-4104.45,67.9526,5.43984,0,'TheInfectisScar'),(1083,1258.38,1207.68,52.6055,0.010372,0,'TheIvarPatch'),(1084,-9824.42,991.59,29.1302,1.81697,0,'TheJansenStead'),(1085,-212.967,6697.84,21.9545,4.46685,530,'TheLagoon'),(1086,-469.406,1769.22,48.8923,5.22312,530,'TheLegionFront'),(1087,-3911.04,-2581.19,44.066,1.00735,0,'ThelgenRock'),(1088,1835.86,6820.31,139.508,4.81166,530,'TheLivingGrove'),(1089,8702.12,-7557.77,89.7481,4.17962,530,'TheLivingWood'),(1090,-5289.82,-3482.56,297.605,6.2238,0,'TheLoch'),(1091,5937.02,523.751,1.58805,3.17606,1,'TheLongWash'),(1092,-2795.41,-1020.59,6.67844,2.57838,0,'TheLostFleet'),(1093,-2739.5,6042.77,35.8899,0.681148,530,'TheLowPath'),(1094,-5352.54,-2948.53,323.78,5.34258,0,'Thelsamar'),(1095,-9935.13,86.7215,32.9021,4.73231,0,'TheMaclureVineyards'),(1096,-6049.42,-3305.28,257.85,2.26108,0,'TheMakersTerrace'),(1097,1869.13,-3213.89,124.624,1.9126,0,'TheMarrisStead'),(1098,-7775.56,-1923.59,-272.806,2.71513,1,'TheMarshlands'),(1099,4572.2,430.013,33.6399,3.80203,1,'TheMastersGlaive'),(1100,-3739.86,1093.8,131.969,3.09301,1,'TheMaul'),(1101,2867.12,1549.42,252.159,3.82218,530,'TheMechanar'),(1102,-855.206,-4072.39,1.35044,1.88279,1,'TheMerchantCoast'),(1103,-4968.86,-1225.49,501.669,1.30248,0,'TheMilitaryWard'),(1104,-4662.07,-952.721,500.378,3.64898,0,'TheMistyWard'),(1105,-10247.5,1440.7,40.7233,3.92498,0,'TheMolsenFarm'),(1106,-7538.51,-1063.45,180.981,0.03409,0,'TheMoltenSpan'),(1107,1242.82,-2236.11,91.9476,3.83099,1,'TheMorShanRampart'),(1108,3021.37,-236.555,7.38714,0.881534,0,'TheNorthCoast '),(1109,2677.02,-5303.72,153.226,5.25368,0,'TheNoxiousGlade '),(1110,-7833.72,-2713.72,10.0944,1.12939,1,'TheNoxiousLair'),(1111,10658.2,1930.28,1332.89,0.27247,1,'TheOracleGlade'),(1112,-157.561,3437.57,60.4541,2.3525,530,'TheOverlook'),(1113,-0.750207,-4657.33,11.0567,6.12818,0,'TheOverlookCliffs'),(1114,-8778.04,1066.56,90.781,1.2262,0,'ThePark'),(1115,-4311.8,1381.19,144.269,3.19908,530,'ThePathOfConquest'),(1116,-199.545,1657.92,43.6863,4.76602,530,'ThePathOfGlory'),(1117,-8075.47,-1686.41,131.836,1.28519,0,'ThePillarOfAsh'),(1118,-1026.6,221.11,108.998,1.07758,1,'ThePoolsOfVision'),(1119,3073.95,3398.27,105.141,0.249392,530,'TheProvingGrounds'),(1120,-3743.96,-2933.6,35.9061,4.8082,1,'TheQuagmire'),(1121,-3641.3,-4358.93,8.35467,3.81559,1,'Theramore'),(1122,-3711.95,-4404.3,21.3729,4.03694,1,'TheramoreIsle'),(1123,-717.31,7930.72,58.5098,4.77929,530,'TheRingOfBlood'),(1124,-1999.94,6581.71,11.32,2.36528,530,'TheRingOfTrials'),(1125,2136.95,-4739.25,50.4957,0.299454,1,'TheRingOfValor'),(1126,-2590.98,-1056.52,-3.74557,5.09009,1,'TheRollingPlains'),(1127,-11028.6,-883.618,61.4453,4.14486,0,'TheRottingOrchard'),(1128,9619.83,-7449.09,13.5847,6.04494,530,'TheRoyalExhange'),(1129,2374.69,-5904.33,11.4691,2.12828,1,'TheRuinedReaches'),(1130,-8409.82,1499.06,27.7179,2.51868,1,'TheRuinsOfAhnQiraj'),(1131,6440.9,-4289.75,666.743,0.780563,1,'TheRuinsOfKelTheril'),(1132,3508.82,-109.892,0.824844,3.53256,1,'TheRuinsOfOrdilAran'),(1133,2104.12,-226.389,97.064,5.19053,1,'TheRuinsOfStardust'),(1134,-6453.01,-3497.6,-58.8657,3.69926,1,'TheRustmaulDigSite'),(1135,9788.65,-7293.19,13.3877,3.41072,530,'TheSanctum'),(1136,-12619.3,-40.9729,28.0657,1.30488,0,'TheSavageCoast'),(1137,-8066.33,1634.51,24.3409,1.55029,1,'TheScarabDais'),(1138,-8098.67,1525.15,2.77194,3.01977,1,'TheScarabWall'),(1139,-8097.76,1502.46,2.60945,4.17432,1,'TheScaradWalp'),(1140,1616.01,-5519.79,109.33,4.16042,0,'TheScarletBasilica'),(1141,8158.19,-6340.32,66.7171,0.029571,530,'TheScorchedGrove'),(1142,3223.19,2677.41,144.211,0.984536,530,'TheScrapField'),(1143,-5394.77,-1662.67,-54.1244,3.08822,1,'TheScreechingCanyon'),(1144,-7048.17,-1493.56,241.598,0.319151,0,'TheSeaOfCinders'),(1145,504.534,1539.08,129.502,1.35812,0,'TheSepulcher'),(1146,-3106.88,4946.71,-22.5348,3.21013,530,'TheShadowStair'),(1147,2312.29,-1893.78,68.3491,5.94215,1,'TheShadyNook'),(1148,-305.79,3061.63,-2.53847,1.88888,530,'TheShatteredHalls'),(1149,3858.59,-5663.59,13.7294,4.55518,1,'TheShatteredStrand'),(1150,-10197.5,-3222.68,20.3199,2.60154,0,'TheShiftingMire'),(1151,-5588.82,-3752.19,-58.7494,4.1972,1,'TheShimmeringFlats'),(1152,1338.49,712.097,33.2729,3.49676,0,'TheShiningStrand'),(1153,2622.09,448.504,73.5894,5.07272,1,'TheShrineOfAessina'),(1154,1247.04,1944.64,12.595,0.200456,0,'TheSkitteringDark'),(1155,-6767.57,-1421.64,210.383,0.609748,0,'TheSlagPit'),(1156,717.282,6979.87,-73.0281,1.50287,530,'TheSlavePens'),(1157,-7884.39,-1338.18,-279.648,2.89812,1,'TheSlitheringScar'),(1158,1058.98,-3169.91,103.805,1.80111,1,'TheSludgeFen'),(1159,-100.415,8775,18.7677,0.508193,530,'TheSpawningGlen'),(1160,-296.918,3661.74,70.3319,1.55139,530,'TheStadium'),(1161,-1332.03,-3009.5,90.8255,0.266427,1,'TheStagnantOasis'),(1162,-248,956,84.3628,1.58766,530,'TheStairOfDestiny'),(1163,794.537,6927.81,-80.4757,0.159089,530,'TheSteamvault'),(1164,-11511,-703.088,35.6861,5.22091,0,'TheStockpile'),(1165,-9917.83,384.428,35.0559,5.35278,0,'THeStonefieldFarm'),(1166,4150.19,3015.92,339.188,4.0499,530,'TheStormspire'),(1167,-10177.9,-3994.9,-111.239,6.01885,0,'TheSunkenTemple'),(1168,10353.4,-6395.35,38.5292,1.45664,530,'TheSunspire'),(1169,-7136.04,586.608,9.01752,1.21736,1,'TheSwarmingPillar'),(1170,-11892.7,-2647.08,-4.68415,3.69096,0,'TheTaintedScar'),(1171,1945.42,-742.68,112.78,2.76364,1,'TheTalondeepPath'),(1172,2417.77,1790.01,393.666,2.00589,1,'TheTalonDen'),(1173,9826.5,2533.55,1321.09,3.1706,1,'TheTempleGardens'),(1174,-8240.09,1991.32,129.072,0.941603,1,'TheTempleOfAhnQiraj'),(1175,-10449.5,-3827.47,18.0675,6.04945,0,'TheTempleOfAtalHakkar'),(1176,-10447.1,-3824.83,18.0678,6.07301,0,'TheTempleOfTears'),(1177,-5812.99,-1180.16,378.179,5.84509,0,'TheTundridHills'),(1178,-1376.82,9616.71,201.301,2.64604,530,'TheTwilightRidge'),(1179,-3183.13,2402.97,254.1,4.02663,1,'TheTwinColossals'),(1180,763.307,6767.81,-67.7695,5.99726,530,'TheUnderbog'),(1181,1655.1,-3258.48,84.1786,3.27451,0,'TheUndercroft'),(1182,1093.26,-684.829,82.0192,5.18414,0,'TheUplands'),(1183,-4073.9,-11426.1,-141.433,2.05381,530,'TheVaultOfLights'),(1184,-1897.51,-10710.7,110.708,1.17888,530,'TheVectorCoil'),(1185,-4263.27,-11338,5.59938,1.67446,530,'TheVeiledSea'),(1186,-1896.77,-1104.8,88.4923,5.78989,1,'TheVentureCoMine'),(1187,-10879.6,-2206.99,122.514,3.74515,0,'TheVice'),(1188,-12163.2,551.904,12.1557,4.11665,0,'TheVileReef'),(1189,2242.67,2240.95,101.525,4.06407,530,'TheVioletTower'),(1190,3142.74,1895.54,144.578,1.53825,530,'TheVortexFields'),(1191,-805.049,-2032.03,95.8796,6.18912,1,'TheWailingCaverns'),(1192,-1408.9,3321.58,34.6189,4.03089,530,'TheWarpFields'),(1193,-1230.03,-11808.4,6.76926,6.23447,530,'TheWarpPiston'),(1194,-5209.65,-2795.71,-7.20066,5.88581,1,'TheWeatheredNook'),(1195,2244.95,-2401.97,60.6771,1.16648,0,'TheWeepingCave'),(1196,-5154.02,214.92,52.8355,3.86268,1,'TheWrithingDeep'),(1197,1453.28,-1864.44,58.7015,5.81604,0,'TheWrithingHaunt'),(1198,-11049.3,-488.388,30.2968,3.20631,0,'TheYorgenFarmstead'),(1199,3579.08,904.851,3.41642,1.04992,1,'TheZoramStrand'),(1200,-8970.3,-2192.9,8.89688,5.57003,1,'ThistleshrubValley'),(1201,1925.08,-2621.31,62.2875,4.65288,0,'ThondrorilRiver'),(1202,-820.219,-1562.88,54.1671,4.06022,0,'ThoradinsWall'),(1203,-6506.47,-1149.95,307.708,4.18256,0,'ThoriumPoint'),(1204,-247.65,5170.55,83.1052,4.97103,530,'ThornfangHill'),(1205,-449.553,-3476.24,94.5329,0.871187,1,'ThornHill'),(1206,-4969.02,-1726.89,-62.1269,3.7933,1,'ThousandNeedles'),(1207,156.251,2673.45,85.1587,0.382074,530,'Thrallmar'),(1208,385.93,2881.01,53.2777,5.09746,530,'ThrallmarMine'),(1209,809.937,2332.9,281.37,5.77383,530,'ThroneOfKiljaeden'),(1210,-781.295,6943.52,33.3343,2.62259,530,'ThroneOfTheElements'),(1211,-309.085,1666.33,116.017,2.09605,1,'ThunderAxeFortress'),(1212,-1277.37,124.804,131.287,5.22274,1,'ThunderBluff'),(1213,-9305.71,671.617,133.138,0.165219,0,'ThunderFalls'),(1214,-1813.19,-251.665,-9.42466,2.30507,1,'ThunderhornWaterWell'),(1215,2314.75,6041.96,142.417,6.24317,530,'ThunderlordStronghold'),(1216,808.307,-4084.94,-12.8768,1.03222,1,'ThunderRidge'),(1217,9259.11,-7482.08,35.529,5.02965,530,'ThuronsLivery'),(1218,-4437.22,-4134.71,2.21481,0.98565,1,'TidefuryCove'),(1219,6808.73,-2091.08,624.962,5.93802,1,'TimbermawHold'),(1220,6474.08,-3142.89,570.406,2.14401,1,'TimbermawPost'),(1221,-4831.62,-1279.2,501.869,1.44385,0,'TinkerTown'),(1222,-202.191,-5025.83,21.631,5.30633,1,'TiragardeKeep'),(1223,2036.02,161.331,33.8674,0.143896,0,'TirisfalGlades'),(1224,2063.35,273.607,94.1076,5.30632,0,'TirisfalGladesZeppelin'),(1225,-11834.4,64.8268,14.1722,2.65266,0,'TkashiRuins'),(1226,-2998.41,4545.29,-21.8414,0.066171,530,'TombOfLights'),(1227,726.529,-4244.99,17.3731,0.66858,1,'TorKrenFarm'),(1228,8561.26,-7920.25,154.912,3.21279,530,'TorWatha'),(1229,1910.63,5556.25,263.017,6.25981,530,'ToshleysStation'),(1230,7165.92,-772.39,54.9534,0.847262,1,'TowerOfAlthalaxx'),(1231,4231.66,-7812.84,6.72805,3.74308,1,'TowerOfEldara'),(1232,-9284.76,-3346.89,109.759,1.52871,0,'TowerOfIlgalar'),(1233,-9281.56,-3341.22,111.243,1.50594,0,'TowerOfLlgalar'),(1234,2265.88,2270.97,97.4046,4.06565,530,'TownSquare'),(1235,-8835.7,620.485,93.2388,0.772328,0,'TradeDistrict'),(1236,1563.81,238.463,-43.4301,0.104772,0,'TradeQuarter'),(1237,-4109.67,-11683.9,-142.705,3.22799,530,'TradersTier'),(1238,-5073.41,-11252.9,0.820152,4.43198,530,'TraitorsCove'),(1239,-10965.2,-1271.56,52.4084,4.05062,0,'TranquilGardensCemetery'),(1240,7564.25,-6872.23,96.0413,4.3579,530,'Tranquillien'),(1241,9010.64,-5825.39,0.507373,3.72724,530,'TranquilShore'),(1242,3799.23,4014.57,121.147,6.17891,530,'TrelleumMine'),(1243,1718.35,4964.6,171.722,1.68743,530,'TrogmasClaim'),(1244,4053,3551.25,120.964,2.73676,530,'TulumansLanding'),(1245,-2162.03,4218.48,6.33299,5.20557,530,'Tuurem'),(1246,-6989.36,1156.02,10.2789,1.12618,1,'TwilightBaseCamp'),(1247,-10384.3,-421.588,63.6179,3.23856,0,'TwilightGrove'),(1248,-7895.48,1862.03,3.00047,0.438955,1,'TwilightOutpost'),(1249,-6685.63,1563.55,7.60951,4.43192,1,'TwilightPost'),(1250,5712.85,571.447,1.39422,3.39362,1,'TwilightShore'),(1251,-6307.77,138.391,15.5158,5.19777,1,'TwilightsRun'),(1252,4768.02,220.04,49.0354,6.12288,1,'TwilightVale'),(1253,223.846,7085.71,35.2267,6.21829,530,'TwinSpireRuins'),(1254,1684.77,-5320.44,73.6126,4.52641,0,'TyrsHand'),(1255,1736.53,-5388.97,82.5741,5.26784,0,'TyrsHandAbbey'),(1256,138.726,-318.182,70.9562,0.116645,229,'UBRS'),(1257,-226.8,49.09,-46.03,1.39,70,'Ulda'),(1258,-6071.37,-2955.16,209.782,0.015708,0,'Uldaman'),(1731,-9570.95,-2782.15,14.1582,3.16513,1,'RiunsofUldum'),(1260,-427.677,5856.72,20.6041,5.04097,530,'UmbrafenLake'),(1261,-804.516,5258.08,19.2276,5.67557,530,'UmbrafenVillage'),(1262,9.71391,-16.2008,-2.75334,5.62187,546,'underbog'),(1263,1584.07,241.987,-52.1534,0.049647,0,'Undercity'),(1264,7185.68,-6205.57,18.986,3.43112,530,'UnderlightMines'),(1265,-7943.22,-2119.09,-218.343,6.0727,1,'UnGoroCrater'),(1266,-3910,3021.99,356.692,4.7927,530,'UpperVeilShilAk'),(1267,4163.63,-5910.48,100.681,3.04329,1,'Ursolan'),(1268,1025.53,-1807.11,76.9136,3.4245,0,'UthersTomb'),(1269,928.515,1242.67,46.8493,5.52386,0,'ValgansField'),(1270,-934.43,2474.4,3.88924,5.62132,530,'ValleyOfBones'),(1271,-6773.84,-3155.91,240.745,0.59012,0,'ValleyOfFangs'),(1272,1936.47,-4673.71,26.3522,5.2789,1,'ValleyOfHonor'),(1273,-5805.89,-2610.93,316.609,2.43858,0,'ValleyOfKings'),(1274,-1346.42,2828.24,113.273,2.37881,1,'ValleyOfSpears'),(1275,1529.69,-4210.23,41.3515,3.31305,1,'ValleyOfSpirits'),(1276,1548.05,-4417.19,9.50517,0.139761,1,'ValleyOfStrength'),(1277,-9565.22,-2792.77,7.83946,3.06854,1,'ValleyOfTheWatchers'),(1278,-601.294,-4296.76,37.8115,1.65401,1,'ValleyOfTrials'),(1279,1931.31,-4284.49,28.3693,1.66609,1,'ValleyOfWisdom'),(1280,3608.59,-4414.43,113.047,1.62303,1,'Valormok'),(1281,-6382.67,-291.916,-3.07818,4.47432,1,'ValorsRest'),(1282,-106.365,-3151.74,119.322,0.604897,0,'ValorwindLake'),(1283,-3683.96,3349.31,284.921,5.27572,530,'VeilAlaRak'),(1284,-3702.96,3754.15,276.354,3.76069,530,'VeilHarrIk'),(1285,1669.12,6935.57,167.396,2.60233,530,'VeilLashh'),(1286,-3590.61,5802.91,-4.29788,0.673285,530,'VeilLithic'),(1287,-2999.94,5605.95,-3.92787,1.66524,530,'VeilRhaze'),(1288,3253.49,5333.48,145.558,5.27728,530,'VeilRuuan'),(1289,-3544.89,4043.25,68.1842,3.42925,530,'VeilShalas'),(1290,-1902.4,3919.57,-1.69338,0.786375,530,'VeilShienor'),(1291,-2504.8,5429.98,0.047476,4.09213,530,'VeilSkith'),(1292,1729.69,4637.52,147.393,4.36564,530,'VeilVekh'),(1293,1713.82,4920.47,169.989,5.92915,530,'VekhaarStand'),(1294,2528,-871.109,53.9863,4.87192,0,'VenomwebVale'),(1295,-11988,-582.287,10.0503,1.64342,0,'VentureCoBaseCamp'),(1296,-4737.16,1077.75,93.5801,1.83556,1,'VerdantisRiver'),(1297,-1067.77,-12505.2,20.8405,0.379317,530,'VeridianPoint'),(1298,3325.01,4608.41,217.521,2.62912,530,'VimGolsCircle'),(1299,-1742.09,-11075.5,76.3923,2.56979,530,'VindicatorsRest'),(1300,4291.45,2295.64,122.382,3.85881,530,'VoidwindPlateau'),(1301,2606.02,7247.71,366.1,3.50082,530,'VortexPinnacle'),(1302,-11078.1,-81.2907,16.645,3.36735,0,'VulgolOgreMound'),(1303,-731.607,-2218.39,17.0281,2.78486,1,'WailingCaverns'),(1304,9517.59,-7344.19,14.3582,3.75943,530,'WalkOfElders'),(1305,-3750.85,1059.38,70.921,3.49973,530,'WardensCage'),(1306,-735.921,8790.11,184.206,3.10641,530,'WarmaulHill'),(1307,1724.8,368.623,-60.4843,0.709539,0,'WarQuarter'),(1308,9921.22,2346.78,1330.78,6.12055,1,'WarriorsTerrace'),(1309,1235.54,1427.1,309.715,0.557629,489,'WarsongGulch'),(1310,1525.95,1481.66,352.001,3.20756,489,'WarsongGulchAlliance'),(1311,930.851,1431.57,345.537,0.015704,489,'WarsongGulchHorde'),(1312,1572.22,-2460.86,97.991,5.06328,1,'WarsongLaborCamp'),(1313,2288.94,-3277.64,101.471,6.11261,1,'WarsongLumberCamp'),(1314,-7376.95,-4579.41,9.88491,3.184,1,'WaterspringField'),(1315,-7221.12,-4892.68,0.553891,4.28199,1,'WavestriderBeach'),(1316,-163.49,132.9,-73.66,5.83,43,'WC'),(1317,-5790.12,-3912.32,-91.3618,2.06955,1,'WeazelsCrater'),(1318,817.389,469.891,66.6528,1.40814,1,'WebwinderPath'),(1319,10410.6,1628.39,1288.2,0.009365,1,'WellspringLake'),(1320,10639.6,1625.53,1285.09,1.27385,1,'WellspringRiver'),(1321,-9663.01,686.769,37.414,5.88684,0,'WestbrookGarrison'),(1322,1728.65,-1602.25,63.429,1.6558,0,'WesternPlaguelands'),(1323,-1073.67,276.526,0.822079,3.35856,0,'WesternStrand'),(1324,-10235.2,1222.47,43.6252,6.2427,0,'Westfall'),(1325,-11405,1970.07,8.90656,4.10641,0,'WestfallLighthouse'),(1326,9144.45,-6273.75,22.6069,5.69229,530,'WestSanctum'),(1327,-3242.81,-2469.04,15.9226,6.03924,0,'Wetlands'),(1328,-3446.82,-1818.75,24.1032,3.54812,0,'WhelgarsExcavationSite'),(1329,2812.03,-704.537,136.047,0.37551,0,'WhisperingGardens'),(1330,2508.78,1663.26,3.20579,5.76976,0,'WhisperingShore'),(1331,-4905.75,-1378.99,-52.6113,2.47564,1,'WhitereachPost'),(1332,5110.28,142.992,42.9436,3.71084,1,'WildbendRiver'),(1333,291.808,-2124.52,121.243,0.276169,0,'WildhammerKeep'),(1334,-3989.47,2168.39,105.35,3.08422,530,'WildhammerStronghold'),(1335,-752.026,-132.783,-28.7924,4.70689,1,'WildmaneWaterWell'),(1336,-14647.1,316.009,3.49004,0.472363,0,'WildShore'),(1337,-5308.52,-2838.5,-55.5527,4.00714,1,'WindbreakCanyon'),(1338,-556.474,-554.987,16.7506,5.90461,1,'WindfuryRidge'),(1339,7016.75,-5707.58,106.608,4.71628,530,'WindrunnerSpire'),(1340,7251.69,-5943.03,17.0084,3.32884,530,'WindrunnerVillage'),(1341,1260.37,393.698,93.3468,4.15782,1,'WindshearCrag'),(1342,982.879,-358.391,14.8563,3.71878,1,'WindshearMine'),(1343,6837.43,-5084.04,691.045,3.19095,1,'WinterfallVillage'),(1344,6759.18,-4419.63,763.214,4.43476,1,'Winterspring'),(1345,-2863.26,-4052.18,37.0607,3.9647,1,'WitchHill'),(1346,-1675.81,-3461.71,52.4582,3.67737,0,'WitherbarkVillage'),(1347,2251.27,2403.09,117.366,2.99201,530,'WizardRow'),(1348,-4952.22,471.918,17.6569,3.27362,1,'WoodpawHills'),(1349,-1755.97,5154.3,-37.2047,4.85864,530,'WorldsEndTavern'),(1350,-2236.79,-12274.6,49.2693,4.68644,530,'WrathscaleLair'),(1351,-4942.25,-11750.1,22.2637,3.90184,530,'WrathscalePoint'),(1352,-3425.33,4455.75,-8.58434,0.530853,530,'WrithingMound'),(1353,1304.63,1455.89,317.694,0.08639,489,'WSG'),(1354,-4864.84,-3375.2,30.4545,4.34245,1,'Wyrmbog'),(1355,-1205.72,-12428.7,94.8675,6.23292,530,'WyrmscarIsland'),(1356,3013.53,5909.98,130.699,1.26484,530,'WyrmskullBridge'),(1357,3117.35,6186.83,138.132,1.31978,530,'WyrmskullTunnel'),(1358,2937.28,-2828.3,212.398,0.433398,1,'Xavian'),(1359,-11774.3,1266.94,2.99361,3.226,0,'YojambaIsle'),(1360,120.7,1776,43.46,4.7713,568,'ZA'),(1361,260.28,7860.4,23.3231,3.77545,530,'Zabrajin'),(1362,-7351.38,-4878.43,3.31332,1.46633,1,'ZalashjisDen'),(1363,-54.8621,5813.44,20.9764,0.081722,530,'Zangarmarsh'),(1364,-575.676,8441.21,63.0364,3.12603,530,'ZangarRidge'),(1365,7112.27,-7491.7,48.8403,4.39733,530,'ZebNowa'),(1366,8027.92,-7831.6,174.185,5.34437,530,'ZebSora'),(1367,7435.65,-7837.06,151.305,4.63123,530,'ZebTela'),(1368,8442.72,-7611.23,155.586,1.86819,530,'ZebWatha'),(1369,-1088.38,2998.27,8.18949,2.73983,530,'ZeppelinCrash'),(1370,-1005.37,2030.91,67.873,3.12703,530,'ZethGor'),(1371,-11916.9,-1248.36,92.5334,4.72417,309,'ZG'),(1372,-12659.8,-458.71,29.312,3.65797,0,'ZiatajaiRuins'),(1422,3360.26,1018.01,3.52035,5.76859,1,'zoramgaroutpost'),(1374,6851.78,-7972.57,179.242,4.64691,530,'ZulAman'),(1375,-6801.19,-2893.02,9.00388,0.158639,1,'ZulFarrak'),(1376,-11916.7,-1215.72,92.289,4.72454,0,'ZulGurub'),(1377,3381.52,-4922.54,160.168,5.33458,0,'Zulmashar'),(1378,-14.5469,-2479.52,120.64,2.32214,0,'ZunWatha'),(1379,-11701.7,963.365,3.89563,5.6466,0,'ZuuldaiaRuins'),(1380,-8419.34,1212.14,5.17277,1.55232,0,'StormwindHarbor'),(1381,564.401,-4944.94,18.5962,5.36544,571,'Valgarde'),(1382,424.405,-4548.76,245.652,5.37325,571,'NewAgamand'),(1383,774.043,-2940.65,7.36477,1.41719,571,'Kamagua'),(1384,1391.04,-3284.63,163.929,1.59391,571,'WestguardKeep'),(1385,1942.86,-6167.11,23.724,2.64258,571,'VengeanceLanding'),(1386,2469.09,-5086.4,282.921,2.07711,571,'FortWildervar'),(1387,2649.82,-4362.69,276.885,5.15979,571,'CampWinterhoof'),(1388,1259.33,-4852.02,215.763,3.48293,571,'UtgardePinnacle'),(1389,1219.72,-4865.28,41.2479,0.313228,571,'UtgardeKeep'),(1390,2213.95,5273.15,11.2565,5.89294,571,'ValianceKeep'),(1391,2741.29,6097.16,76.9055,0.731543,571,'WarsongHold'),(1392,4147.98,5278.79,24.7334,0.150308,571,'FizzcrankAirstrip'),(1393,3781.81,6953.65,104.82,0.467432,571,'TheNexus'),(1394,4488.76,5736.18,80.0769,5.45078,571,'Bor\'gorokOutpost'),(1395,3071.85,4791.19,1.13476,5.76654,571,'Kaskala'),(1396,2925.02,4065.63,1.46737,3.56739,571,'Unu\'pe'),(1397,3464.17,4087.16,17.0561,2.02607,571,'Taunka\'leVillage'),(1398,5804.15,624.771,647.767,1.64,571,'Dalaran'),(1399,4103.36,264.478,50.5019,3.09349,571,'Dragonblight'),(1400,3256.57,5278.23,40.8046,0.246367,571,'BoreanTundra'),(1401,1902.15,-4883.91,171.363,3.11537,571,'HowlingFjord'),(1402,4391.73,-3587.92,238.531,3.57526,571,'GrizzlyHills'),(1403,5560.23,-3211.66,371.709,5.55055,571,'Zul\'Drak'),(1404,6898.72,-4584.94,451.12,2.34455,571,'Gundrak'),(1405,7527.14,-1260.89,919.049,2.0696,571,'StormPeaks'),(1406,9049.37,-1282.35,1060.19,5.8395,571,'Ulduar'),(1407,7253.64,1644.78,433.68,4.83412,571,'Icecrown'),(1420,-8492.63,399.867,115.767,5.341,0,'StormwindKeep'),(1409,-9003.05,874.064,148.619,3.47324,0,'WizardsSanctum'),(1410,5453.72,2840.79,421.28,0,571,'VaultOfArchavon'),(1411,4774.6,-2032.92,229.15,1.59,571,'DrakTharonKeep'),(1412,4760.7,2143.7,423,1.13,571,'Wintergrasp'),(1413,5323,4942,-133.5,2.17,571,'SholazarBasin'),(1414,8515.89,629.25,547.396,1.5747,571,'ArgentTournament'),(1415,5696.73,507.4,652.97,4.03,571,'TheVioletHold'),(1416,3412.88,-2791.17,201.521,2.2458,571,'AmberpineLodge'),(1417,3682.71,-722.635,212.729,5.7991,571,'WintergardeKeep'),(1418,5873.82,2110.98,636.011,3.5523,571,'IcecrownCitadelRaid'),(1421,849.887,1047.82,-7.21361,5.17182,730,'Maelstrom'),(1423,3028.46,-535.834,205.637,1.67599,1,'HellscreamsWatch'),(1424,2083.1,-1190.57,100.995,3.16275,1,'SilverwingRefuge'),(1426,-12225.9,-2464.37,38.9107,3.17497,0,'Maldraz'),(1427,-12444.9,-2417.35,2.89744,5.79035,0,'TaintedForest'),(1428,-12718.8,-2859.98,1.39573,3.93602,0,'Surwich'),(1429,-12839.7,-432.057,13.1104,3.79464,0,'FortLivingston'),(1430,-12109.3,-848.865,46.8417,5.94663,0,'Bambala'),(1431,-13284.6,735.409,3.14799,5.50052,0,'HardwrenchHideaway'),(1432,-13622.2,-68.3346,35.2397,3.04144,0,'ExplorersLeagueDigSite'),(1433,-7933.5,-1904.26,132.553,5.83975,0,'Chiselgrip'),(1434,-10143,-3791.27,17.3551,0.276046,0,'MarshtideWatch'),(1435,-9787.68,-3893.98,21.7214,2.18456,0,'Bogpaddle'),(1436,-9144.61,-3046.24,108.723,5.92301,0,'KeeshansPost'),(1437,-9616.57,-3478.58,122.42,5.62009,0,'ShalewindCanyon'),(1438,-7108.97,-1195.1,323.928,4.68522,0,'IronSummit'),(1440,-6903.45,-3505.93,242.085,1.26637,0,'BloodwatcherPoint'),(1441,-6614.67,-3416.83,279.172,0.775497,0,'DustwindDig'),(1442,-6612.49,-3894.86,379.13,4.38126,0,'Fuselight'),(1443,-4600.92,-4118.29,331.717,3.46824,0,'TwilightCitadel'),(1445,-3083.78,-3989.05,266.138,0.286578,0,'VErmillionRedoubt'),(1446,-3145.56,-4445.8,183.465,1.4686,0,'GlopgutsHollow'),(1447,-3202.07,-5021.2,119.823,4.85837,0,'Thundermar'),(1448,-2775.37,-5340.91,173.805,0.91174,0,'Kirthhaven'),(1449,-2676.42,-5042.87,129.829,1.22983,0,'MawofMadness'),(1450,-2708.5,-5661.92,379.065,2.45033,0,'ThunderstrikeMountain'),(1451,-2766.82,-6439.01,247.915,3.88289,0,'TheKrazzworks'),(1452,-4021.72,-6365.27,37.8157,2.10868,0,'DragonmawPort'),(1453,-3873.27,-5701.33,43.3167,1.04839,0,'GorshakWarCamp'),(1454,-3676.87,-5312.67,35.7687,4.44131,0,'Bloodgulch'),(1455,-4187.99,-5154.8,-7.56141,1.92804,0,'CrucibleofCarnage'),(1456,-4192.53,-4737.7,126.786,1.71991,0,'VictorsPoint'),(1457,-4378.07,-4581.8,180.926,4.94789,0,'TwilightGate'),(1458,-4858.27,-4848.96,196.575,1.03269,0,'Crushblow'),(1459,-4184.4,-5605.28,25.5473,3.84049,0,'FirebrandsPatrol'),(1460,-4791.29,-6685.08,5.51092,1.64216,0,'Highbank'),(1461,-27.4946,-2805.86,122.257,2.51499,0,'HiriwathaResearchStation'),(1462,327.169,-4127.62,117.192,6.19694,0,'StormfeatherOutpost'),(1463,-570.189,-1066.48,60.52,2.71922,0,'EastpointTower'),(1464,-962.552,-1587.32,51.3152,0.63242,0,'GalensFall'),(1465,-574.979,19.3976,47.6175,5.02595,0,'TheSludgeFields'),(1466,-213.007,103.418,63.388,6.03126,0,'SludgeguardTower'),(1467,-587.561,428.44,79.6859,0.151762,0,'SouthpointGate'),(1468,-155.578,1257.23,51.7183,1.04319,0,'TheForsakenFront'),(1469,1070.74,1571.1,28.0402,6.16045,0,'ForsakenRearGuard'),(1470,1384.07,1037.13,54.1925,3.53565,0,'ForsakenHighCommand'),(1471,2231.18,1023.88,36.3924,4.08502,0,'CalstonEstate'),(1472,1857.7,-1756.25,60.1443,4.94268,0,'TheMendersStead'),(1473,1918.48,-2646.76,59.6194,2.80796,0,'FionasCaravan'),(1474,2251.5,-4419.66,111.731,5.05262,0,'LightsShieldTower'),(1475,-869.649,1559,29.5733,3.03494,0,'RuinsofGilneas'),(1476,-4440.64,3815.32,-82.8223,3.48655,0,'TheBrinyCutter'),(1477,-4583.47,3663.16,-115.575,3.43079,0,'SeafarersTomb'),(1478,-4444.46,4017.24,-51.1982,4.09131,0,'LegionsFate'),(1479,-4789.13,3739.03,-128.541,3.63971,0,'KelptharForest'),(1480,-5071.76,3585.73,-144.859,2.78755,0,'GurbogglesLEdge'),(1481,-5451.66,3473.97,-131.52,2.69723,0,'TheClutch'),(1482,-5131.16,3280.08,-118.244,2.46318,0,'DeepmistGrotto'),(1483,-5119.55,3394.5,-119.945,5.20422,0,'GnawsBoneyard'),(1484,-4956.78,3361.26,-78.4193,0.240503,0,'TheSkeletalReef'),(1485,-4701.86,3464.68,-82.8205,1.12015,0,'GorroksLament'),(1486,-4564.67,3467.91,-101.524,2.50637,0,'SMugglersScar'),(1487,-5358.04,3896.8,2.94341,0.745509,0,'ShimmeringExpanse'),(1488,-5767.16,3958.75,-211.124,5.87337,0,'ShimmeringGrotto'),(1489,-6193.27,3498.74,-468.877,3.13312,0,'RuinsofThelseraiTemple'),(1490,-6378.57,3828.17,-172.181,3.14097,0,'Nespirah'),(1491,-7413.64,4045.64,-100.391,3.08162,0,'BielaranRidge'),(1492,-7294.58,4873.67,-280.846,1.39301,0,'QuelDormirTerrace'),(1493,-6952.26,5079.22,-287.541,6.27583,0,'NarsholaTerrace'),(1494,-6767.98,5077.1,-345.671,0.004424,0,'RuinsofVashjir'),(1495,-4122.11,-2761.88,17.9337,4.21947,0,'SlabchiselsSurvey'),(1496,-3259.08,-2747.8,11.8654,5.3316,0,'GreenwardensGrove'),(1497,-3237.52,-1996.57,50.5491,0.740159,0,'WhelgarsRetreat'),(1498,-2869.55,-1489.14,9.751,1.25381,0,'SwiftgearStation'),(1500,-5317.71,-217.358,440.936,0.835165,0,'ShimerrRidgr'),(1501,-5172.99,473.902,388.722,5.47687,0,'NewTinkertown'),(1502,-5421.37,491.22,385.024,3.56207,0,'ToxicAirfield'),(1503,-4637.48,-1695.75,503.324,3.95084,0,'IronforgeAirport'),(1504,4746.74,-4919.45,884.328,0.789974,1,'DoomsVigil'),(1505,4576.4,-4694.69,883.016,4.14943,1,'ForgeofSupplication'),(1506,4788.75,-4228.18,894.168,1.17513,1,'SeatoftheChosen'),(1507,4105.75,-3991,970.667,3.15041,1,'GatesofSothann'),(1508,3924.72,-3426.34,1011.92,3.93345,1,'AscendantsRise'),(1509,3996.55,-2965.05,1002.55,1.89298,1,'SulfuronSpire'),(1510,3428.95,-2435.37,968.778,3.90753,1,'SehtriasRoost'),(1511,4534.98,-2578.82,1123.8,5.58828,1,'TheRegrowth'),(1512,4563.34,-2580.15,829.437,1.25837,1,'FirelandsHatchery'),(1513,4424,-2082.46,1210.66,2.39564,1,'SanctuaryofMalorne'),(1514,5117.84,-1764.43,1333.67,0.359885,1,'GroveofAessina'),(1515,4922.2,-1862.36,1333.43,4.82958,1,'BlackhornsPenance'),(1516,5036.41,-2045.99,1368.39,5.2961,1,'LightningLedge'),(1517,5030.96,-2032.44,1148.98,4.42431,1,'FirelandsForgeworks'),(1518,5357.79,-2185.19,1285.77,5.84588,1,'ShrineofGoldrinn'),(1519,5164.29,-2283.39,1279.77,3.98842,1,'MawofLycanthoth'),(1520,4918.17,-2725.36,1437.62,4.34499,1,'ShrineofAviana'),(1521,5446.69,-2793.97,1516.2,3.23364,1,'CircleofCinders'),(1522,5374.3,-2933.95,1540.95,4.89869,1,'TheVerdantThicket'),(1523,5497.07,-3572.97,1569.26,2.07911,1,'Nordrassil'),(1524,4653.99,-3688.44,955.265,2.38149,1,'TheScorchedPlain'),(1525,4679.1,-3675.53,696.452,1.3644,1,'TheCrucibleofFlame'),(1526,6888.13,-1601.87,500.034,2.67768,1,'IrontreeClearing'),(1527,6103.26,-876.087,411.86,2.71789,1,'WhisperwindGrove'),(1528,4736.27,-871.435,343.468,0.987956,1,'WildheartPoint'),(1530,7372.23,-250.557,7.65159,4.05495,1,'Lordanel'),(1531,7361.15,-1089.86,42.5336,4.41858,1,'ShatterspearPass'),(1532,7861.19,-1028.76,31.4317,2.89806,1,'shatterspearwarcamp'),(1533,7486.08,-1536.77,159.275,5.56448,1,'ShatterspearVale'),(1534,6278.28,265.272,18.2156,2.95226,1,'AuberdineRefugeeCamp'),(1535,5915.04,192.702,64.1486,0.509667,1,'EyeoftheVortex'),(1536,5543.19,474.356,28.5956,0.613335,1,'TheBlazingStrand'),(1537,4611.46,942.484,53.8799,4.39503,1,'Nazjvel'),(1538,3884.63,644.215,10.4563,1.013,1,'BlackfathomCamp'),(1539,1923.8,-332.584,118.252,1.80087,1,'StardustSpire'),(1540,1150.16,-3394.3,91.6915,0.316715,1,'NozzlepotsOutpost'),(1541,-1904.13,-2811.99,90.7991,1.21283,1,'TeegansExpedition'),(1542,-1970.76,-2571.7,96.1465,1.51207,1,'TheOvergrowth'),(1543,-1710.24,-2207.87,100.124,2.6988,1,'NightmareScar'),(1544,-1463.28,-1921.17,94.9702,3.06872,1,'CampUnafe'),(1545,-800.314,-1564.79,140.827,5.84982,1,'HuntersHill'),(1546,-2113.27,-1727.12,97.2723,2.69566,1,'VendettaPoint'),(1547,-2233.09,-2304.05,93.7337,4.27431,1,'ForwardCommand'),(1548,-3288.59,-1682.52,122.621,4.8858,1,'DesolationHold'),(1549,-3149.45,-2285.36,92.9994,4.68945,1,'FortTriumph'),(1550,-3140.21,-2002.59,89.572,0.754604,1,'Battlescar'),(1552,-4301.46,-914.621,80.9191,4.50747,1,'WestreachSummit'),(1553,-5383.03,-1292.82,86.6787,2.99401,1,'TwilightAerie'),(1554,-5635.75,-1667.01,92.0193,2.89584,1,'TwilightBulwark'),(1555,-5806.69,-2838.27,106.036,4.25065,1,'TwilightWithering'),(1556,-6113.1,-3894.24,6.18473,5.34078,1,'FizzlePozziksSpeedbarge'),(1557,-5420.41,-4295.4,85.089,3.5383,1,'SplithoofHeights'),(1558,-6222.69,-4494.36,88.2471,2.855,1,'SouthseaHoldfast'),(1559,-9920.26,-2167.54,27.2207,0.142144,1,'TombsofthePrecursors'),(1560,-10488.7,-1890.17,90.3234,2.15434,1,'SunstoneTerrace'),(1561,-10182.8,-2005.49,54.8532,1.69018,1,'HallsofOrigination'),(1562,-10183.5,-2307.59,38.8009,1.57236,1,'TheStepsofFate'),(1563,-10181.6,-2867.62,4.99583,1.84646,1,'TheCursedLanding'),(1564,-10448.2,-2428.69,46.7722,1.55979,1,'TrailofDevastation'),(1565,-10645.7,-2431.91,100.357,1.60691,1,'TomboftheSunKing'),(1566,-11099.3,-1948.61,2.76428,5.42002,1,'VirnaalRiverDelta'),(1567,-10962.6,-1479.8,6.67713,6.17793,1,'LostCityoftheTolVir'),(1568,-11484.4,-2302.23,605.18,0.424881,1,'TheVortexPinnacle'),(1569,-11429.4,-754.107,136.566,1.52051,1,'NefersetCity'),(1570,-11380.5,119.991,852.155,5.48598,1,'ThroneoftheFourWinds'),(1571,-10944.7,-343.55,21.3991,4.59063,1,'Cradleofthencients'),(1572,-10411.4,-410.325,339.841,0.192397,1,'ObeliskoftheSun'),(1573,-10454.7,-381.74,226.145,6.22033,1,'ChamberoftheSun'),(1574,-9861.54,-859.273,106.939,5.48598,1,'VirnaalDam'),(1575,-9442.08,-958.952,111.013,3.1392,1,'Ramkahen'),(1576,-9290.74,-1550.98,68.1297,3.18319,1,'ObeliskoftheStars'),(1577,-9262.93,-1456.33,-170.202,4.66839,1,'ChamberoftheStars'),(1578,-8986.96,-1552.44,94.4541,0.014112,1,'KhartutsTomb'),(1579,-9077.22,-747.827,146.149,1.83231,1,'SahketWastes'),(1580,-8967.31,-24.6161,143.444,2.59022,1,'ObeliskoftheMoon'),(1581,-8799.07,306.84,140.359,1.00057,1,'ChamberoftheMoon'),(1582,-9299.12,346.449,274.638,1.54485,1,'TempleofUldum'),(1583,-9787.76,-70.1306,67.7241,4.17987,1,'Orsis'),(1584,-10692.7,424.547,20.7643,2.88004,1,'RuinsofAmmon'),(1585,-8681.98,208.699,339.877,2.3868,1,'RuinsofKhintaset'),(1586,-8342.43,783.254,152.323,1.25976,1,'OasisofVirsar'),(1588,-4531.53,2242.21,8.75675,6.1315,1,'FeathermoonStronghold'),(1589,-4598.28,1836.45,88.6069,6.16448,1,'StonemaulHold'),(1590,-4916.26,1483.72,84.7777,3.2161,1,'TowerofEstulan'),(1591,-4991.49,81.6896,99.5467,6.27044,1,'Shadebough'),(1592,-3194,1896.75,49.3339,3.66535,1,'DreamersRest'),(1593,-3088.83,2564.99,50.18,5.6689,1,'CampAtaya'),(1594,-616.52,191.948,53.0694,1.21972,1,'StonetalonPass'),(1595,-2391.86,-1610.42,124.877,1.46225,1,'TheGreatGate'),(1596,-1692.67,2590.66,136.836,0.354191,1,'ThargadsCamp'),(1597,-979.241,1658.22,66.87,4.73435,1,'KarnumsGlade'),(1598,-441.973,2223.46,89.495,6.03183,1,'FuriensPost'),(1599,2842.46,-4485.88,95.1263,4.22741,1,'MountainfootStripMine'),(1600,3281.8,-4521.36,266.995,5.46441,1,'GallywixPleasurePalace'),(1601,2955.29,-4998.92,165.651,2.90479,1,'OrgrimmarRocketwayExchange'),(1602,2574.92,-5694.63,109.526,4.59025,1,'TheSecretLab'),(1603,2658.75,-6171.88,138.071,4.70256,1,'SouthernRocketwayTerminus'),(1604,3517.69,-6773.93,70.5824,1.50206,1,'BilgewaterHarbor'),(1605,4934.05,-6456.48,160.631,5.05755,1,'ArcanePinnacle'),(1606,1363.95,1045.29,165.177,2.12054,1,'MirkfallonPost'),(1607,1027.52,2003.36,126.734,1.87314,1,'FarwatchersGlen'),(1608,1573.54,1604.99,114.222,5.83861,1,'ThalDarahGrove'),(1609,2144.42,1591.63,341.255,4.0534,1,'ThaldarahOverlook'),(1610,2127.89,1259.97,451.935,4.46259,1,'CliffwalkerPost'),(1611,1751.89,799.525,130.922,4.21047,1,'TheSLudgewerks'),(1612,1229.52,436.619,77.9145,2.7025,1,'WindshearHold'),(1613,896.416,-1.37233,92.9033,0.621201,1,'KromgarFortress'),(1614,1535.47,-477.697,55.8894,4.42253,1,'TheFold'),(1615,347.118,-645.768,21.3829,4.64245,1,'UnearthedGrounds'),(1616,259.17,-298.3,92.7365,0.605501,1,'NorthwatchExpeditionBaseCamp'),(1617,-191.201,-333.188,9.79661,5.53309,1,'Makajin'),(1619,-1107.88,-5355.91,14.4998,4.31651,1,'DarkspearIsle'),(1620,-1098.7,-5149.14,2.0433,4.9495,1,'EchoIsles'),(1621,-988.807,-4553.99,25.6564,1.65475,1,'NorthwatchFoothold'),(1623,851.24,1045.7,-6.83044,4.76182,730,'TheMaelstrom'),(1624,775.093,-95.4299,-61.0889,0.501807,646,'DeathwingsFall'),(1625,772.704,252.616,-59.1849,0.916497,646,'TheBloodTrail'),(1626,981.22,517.28,-49.3337,0.237123,646,'TempleofEarth'),(1627,1027.29,639.037,156.673,4.8325,646,'TheStonecore'),(1628,923.214,750.789,-69.0947,1.2668,646,'QuartzineBasin'),(1629,692.297,969.928,181.039,1.60452,646,'TwilightTerrace'),(1630,527.946,1048.06,75.1102,2.15901,646,'MastersGate'),(1648,107.45,1414.7,220.506,5.40033,646,'TheQuakingFields'),(1632,498.844,1707.99,344.629,3.54524,646,'Stonehearth'),(1633,746.724,1860.28,326.773,0.235568,646,'FracturedFront'),(1634,1212.12,1896.71,310.465,3.10227,646,'NeedlerockSlag'),(1635,1411.82,1759.14,302.518,5.96504,646,'NeedlerockChasm'),(1636,2132.07,1107.12,126.056,5.88257,646,'ThePaleRoost'),(1637,2362.94,179.012,181.618,6.26349,646,'TherazanesThrone'),(1638,1901.2,45.0345,-165.576,4.46257,646,'CrumblingDepths'),(1639,1740.45,-182.396,27.8365,0.347084,646,'TwilightPrecipice'),(1640,1869.01,36.9268,-78.3393,0.661243,646,'ScouredReach'),(1641,1932.79,-400.147,172.066,5.19692,646,'VerlokStand'),(1642,1285.28,-487.408,272.121,2.49436,646,'CrimsonExpanse'),(1643,728.174,-639.363,146.382,3.37401,646,'UpperSilvermarsh'),(1644,488.772,-342.393,47.0634,4.44608,646,'LowerSilvermarsh'),(1645,226.463,-516.672,175.642,3.05514,646,'AbyssionsLair'),(1646,244.239,212.317,38.6467,4.40837,646,'StormsFuryWreckage'),(1647,-49.556,428.105,161.304,4.32983,646,'AlabasterShelf'),(1649,1387.86,841.479,-53.7031,3.95912,646,'JaggedWastes'),(1650,-1254.22,1050.03,106.995,3.15094,732,'BaradinHold'),(1651,-1090.33,1151.46,120.737,6.2729,732,'Dblock'),(1652,-882.322,983.345,121.442,1.45056,732,'IroncladGarrison'),(1653,-1233.3,789.026,119.422,4.6864,732,'CursedDepths'),(1654,-1044.63,578.582,147.929,5.26367,732,'EastSpire'),(1655,-1419.54,701.538,123.422,3.96776,732,'Slagworks'),(1656,-1570.19,990.24,168.438,3.79105,732,'SouthSpire'),(1657,-1485.42,1126.5,124.3,3.06692,732,'TheHole'),(1658,-1456.88,1271.65,133.584,2.32471,732,'WardensVigil'),(1659,-1004.61,1419.69,176.253,0.667525,732,'WestSpire'),(1660,-731.964,1187.12,104.467,6.13782,732,'BlackstoneSpan'),(1661,-591.114,1179.05,95.6243,2.99623,732,'TolBarad'),(1662,-369.122,1060.32,21.7401,6.16531,732,'BaradinBaseCamp'),(1663,-249.233,980.65,49.3517,0.573274,732,'LargosOverlook'),(1664,-21.3906,1154.89,15.2998,2.55248,732,'RustbergVillage'),(1665,262.217,1535.66,2.5819,2.75668,732,'CapeofLostHope'),(1666,-78.5566,1624.89,25.3212,0.856015,732,'FarsonHold'),(1667,-101.674,1880.94,13.4682,2.47001,732,'WellsonShipyard'),(1668,-605.364,1734.09,70.1185,4.14762,732,'ForgottenHill'),(1669,-348.559,1646.64,18.7217,6.03257,732,'TheRestlessFront'),(1670,-595.784,1411.83,21.2468,4.19396,732,'HellscreamsGrasp'),(1671,-288.238,1350.02,22.766,0.372992,732,'Darkwood'),(1672,-1438.16,1403.35,35.5561,3.14788,654,'MerchantSquare'),(1673,-1729.72,1391.04,20.856,3.85474,654,'MilitaryDistrict'),(1674,-1538.37,1576.55,28.7498,3.78405,654,'LightsDawnCathedral'),(1675,-1443.88,1668.02,20.4857,3.72907,654,'CathedralQuarter'),(1676,-1709.86,1633.74,20.4886,5.7554,654,'GreymaneCourt'),(1677,-1208.11,1822.38,21.4234,0.819172,654,'NorthernHeadlands'),(1678,-1146.02,1153.21,24.2837,4.62758,654,'EmberstoneVillage'),(1679,-1374,1207.3,35.5597,0.669172,654,'LiveryOutpost'),(1680,-1905.84,970.246,76.2242,2.51486,654,'TempestsReach'),(1681,-2070.09,1281,-85.6562,4.86712,654,'Taldoren'),(1682,-2091.79,1449.26,-64.6396,1.82213,654,'TheBlackWald'),(1683,-2474.32,1551.71,16.5174,0.871802,654,'StormglenVillage'),(1684,-2230.67,1809.83,11.9092,1.02102,654,'StagecoachCrashSite'),(1685,-2372.76,1984.3,71.6796,2.6468,654,'KorothsDen'),(1686,-2343.91,2281.62,0.39278,3.86024,654,'HaywardFishery'),(1687,-1884.45,2277.78,42.2508,5.58733,654,'Duskhaven'),(1688,-1949.66,2578.64,1.39337,5.94076,654,'HammondFarmstead'),(1689,-2182.91,2518.9,3.53003,2.5125,654,'DuskmistShore'),(1690,-2141.43,2414.54,10.7156,1.43257,654,'WahlCottage'),(1691,-1614.56,2528.8,127.592,0.635389,654,'GreymaneManor'),(1692,-1216.96,2483.5,65.6051,5.73655,654,'TheHeadlands'),(1693,-1316.68,2118.58,5.62727,5.77738,654,'KeelHarbor'),(1694,-1605.52,1929.93,16.7376,4.29298,654,'AdericsRepose'),(1695,-680.828,2243.91,93.7377,1.33203,654,'GalewatchLighthouse'),(1696,-7881.46,1837.55,4.17201,0.022936,648,'GallywixYacht'),(1697,-8102.41,1892.69,54.5099,3.15903,648,'GallywixsVilla'),(1698,-8082.08,1504.16,8.83278,0.241278,648,'BilgewaterPort'),(1699,-8277.47,1505.2,43.4199,4.45887,648,'KajaroField'),(1700,-8424.1,1332.31,102.256,1.47828,648,'KTCHeadquarters'),(1701,-8399.77,1330.13,102.014,0.221641,648,'KEzan'),(1702,-8459.5,1197.79,41.9041,4.16434,648,'KAjamine'),(1703,-8367.1,1564.2,49.4087,1.83563,648,'Drudgetown'),(1704,-8312.6,1726.05,50.4851,3.16923,648,'BankofKezan'),(1705,-8158.86,1642.63,19.9116,3.20065,648,'SwindleStreet'),(1706,2373.33,2471.92,12.0965,5.42333,648,'GallywixDocks'),(1707,2108.23,2561.84,1.71463,3.33181,648,'SlavePits'),(1708,1593.54,2725,82.8661,3.02943,648,'WarchiefsLookout'),(1709,919.439,2897.2,100.502,2.19691,648,'SavageGlen'),(1710,855.826,2779.31,113.027,4.83192,648,'WildOverlook'),(1711,576.72,3106.31,2.43299,5.40447,648,'ShipwreckShore'),(1712,552.376,2701.19,105.896,3.99467,648,'HordeBaseCamp'),(1713,506.071,2984.17,7.16684,5.53483,648,'KajamiteCavern'),(1714,931.96,3316.82,5.84954,0.504359,648,'AllianceBeachhead'),(1715,173.675,1940.15,4.38497,3.15899,648,'RuinsofVashjelan'),(1716,738.977,1674.86,118.462,6.07989,648,'OomlotVillage'),(1717,1267.84,1166.23,117.798,3.53519,648,'VolcanothsLaie'),(1718,1267.84,1166.23,117.798,3.53519,648,'VolconathsLair'),(1719,1540.4,1312.38,108.357,0.11557,648,'LostCaldera'),(1720,1880.97,1511.68,315.577,3.29643,648,'LostPeak'),(1721,1666.41,1672.48,293.275,2.74665,648,'Oostan'),(1722,915.683,2284.91,12.964,1.50337,648,'LAndingSite'),(1723,2387.03,2094.18,1.88705,6.0579,648,'KTCOilPlatform'),(1724,2338.04,1945.63,23.2704,5.805,648,'BilgewaterLumberYard'),(1725,10273.9,2433.42,1330.48,0.471968,1,'TheHowlingOak'),(1631,909.795,502.76,-49.219,0.21656,646,'Deepholm'),(1726,5516.19,-3599.34,1570.05,2.21601,1,'newhyjal'),(1727,-606.401,808.955,244.81,6.27254,643,'ThroneoftheTides'),(1728,-7571.42,-1320.84,245.538,4.84243,0,'BlackrockCaverns'),(1729,-1522.88,1593.77,25.1195,3.93815,0,'GilneasCity'),(1732,5545.29,-3644.18,1566.06,5.09291,1,'MountOfHyjal'),(1733,2886.75,-811.963,160.333,4.74983,0,'ScarletMonasteryInstances'),(1734,-11209.7,1670.96,24.7374,1.31825,0,'MinesOfDeath'),(1735,-1467.26,2617.1,76.7576,3.15699,1,'MaraudonInstance'),(1736,-2519.57,-2477.87,81.3563,3.24715,0,'Westlands'),(1737,2361.59,2323.79,1.17093,3.74258,648,'LostIsles'),(1738,-355.88,-2.78,632.781,4.05107,657,'Skywall'),(1742,1755.18,2077.8,243.411,0.850567,659,'Lost Isle(post-eruption)'),(1743,960.567,2355.44,5.88083,3.95682,648,'LostIsle(town in a box closed)'),(1744,947.249,2317.51,5.02816,2.90438,661,'LostIsle(town in a box opened)'),(1745,-1161.69,-761.832,835.026,4.61151,671,'GrimBatol(interior/raid)'),(1746,-318.66,-222.44,193.43,4.61151,669,'BlackwingDescent'),(1747,214.4,718.86,105.497,4.71361,645,'BlackrockCaverns'),(1748,3546.8,-5287.96,109.935,1.61522,1,'AszharaNagaCity '),(1749,2026.1,1602.82,330.094,3.92585,731,'GiantCrater'),(1750,-589.89,809.07,244.809,1.55939,643,'ThroneofTheTides'),(1751,-3620.12,-4833.18,84.1853,4.37028,0,'TwilightHighlands'),(66,3341.36,-4603.79,92.5027,5.28142,1,'Azshara'),(67,128.205,135.136,236.11,4.59132,37,'AzsharaCrater'),(1259,-9636.42,-2788.76,7.83902,3.1455,1,'Uldum'),(1752,1360.06,-4375.95,26.0961,0.243312,1,'OrgrimmarCity'),(1753,-8915.96,-123.503,81.6986,1.74358,0,'HumanStart'),(1754,5873.82,2110.98,636.011,3.61905,571,'ICC'),(1755,-1611.38,1505.12,29.7052,3.93422,638,'LastStand'),(1756,-1820.33,2298.73,42.2901,3.67487,654,'WorgenPhase2'),(1757,-1451.53,1403.35,35.5561,0.212058,638,'Worgenstart'),(1758,1973.32,-4801.01,37.1737,4.51123,1,'tempadd'),(1759,2355,-5663.33,426.028,1.93208,609,'DKStart'),(1760,1832.57,2984.52,1.83145,3.5517,648,'Tempor'),(1761,-4887.84,-4246.13,827.763,2.051,0,'Bastionoftwilight'),(1762,-1835.22,2291.92,42.1481,3.47067,654,'Gilneas654'),(1763,1402.01,-5827.06,137.188,5.33941,609,'OrbazBlood'),(1764,5791.6,-3017.77,286.315,2.07516,571,'Amphitheater'),(9999,16220.7,16398.3,-64.3786,0.825313,1,'GMroom'),(10000,-9468.53,-2833.06,65.278,0.624365,0,'CampEverstill'),(10001,-105.888,225.736,53.2764,2.99677,0,'PlantsVsZombies'),(10002,-8394.34,1130.25,18.062,4.64851,723,'OtherStormwind'),(10003,-9459.13,57.868,55.9497,2.55282,723,'OtherStormwindforest'),(10004,-9744.09,680.935,28.0113,0.274378,723,'OtherStormwindBastion'),(10005,2965.58,-3040.28,98.5621,5.57,169,'EmeraldDream'),(10006,5540.11,5751.58,-78.7211,6.16755,571,'NesinwaryBaseCamp');
/*!40000 ALTER TABLE `game_tele` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `game_weather`
--

DROP TABLE IF EXISTS `game_weather`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `game_weather` (
  `zone` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `spring_rain_chance` tinyint(3) unsigned NOT NULL DEFAULT '25',
  `spring_snow_chance` tinyint(3) unsigned NOT NULL DEFAULT '25',
  `spring_storm_chance` tinyint(3) unsigned NOT NULL DEFAULT '25',
  `summer_rain_chance` tinyint(3) unsigned NOT NULL DEFAULT '25',
  `summer_snow_chance` tinyint(3) unsigned NOT NULL DEFAULT '25',
  `summer_storm_chance` tinyint(3) unsigned NOT NULL DEFAULT '25',
  `fall_rain_chance` tinyint(3) unsigned NOT NULL DEFAULT '25',
  `fall_snow_chance` tinyint(3) unsigned NOT NULL DEFAULT '25',
  `fall_storm_chance` tinyint(3) unsigned NOT NULL DEFAULT '25',
  `winter_rain_chance` tinyint(3) unsigned NOT NULL DEFAULT '25',
  `winter_snow_chance` tinyint(3) unsigned NOT NULL DEFAULT '25',
  `winter_storm_chance` tinyint(3) unsigned NOT NULL DEFAULT '25',
  `ScriptName` char(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`zone`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='CTDB Weather System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game_weather`
--

LOCK TABLES `game_weather` WRITE;
/*!40000 ALTER TABLE `game_weather` DISABLE KEYS */;
/*!40000 ALTER TABLE `game_weather` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gameobject`
--

DROP TABLE IF EXISTS `gameobject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gameobject` (
  `guid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Global Unique Identifier',
  `id` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'Gameobject Identifier',
  `map` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Map Identifier',
  `spawnMask` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `phaseMask` smallint(5) unsigned NOT NULL DEFAULT '1',
  `position_x` float NOT NULL DEFAULT '0',
  `position_y` float NOT NULL DEFAULT '0',
  `position_z` float NOT NULL DEFAULT '0',
  `orientation` float NOT NULL DEFAULT '0',
  `rotation0` float NOT NULL DEFAULT '0',
  `rotation1` float NOT NULL DEFAULT '0',
  `rotation2` float NOT NULL DEFAULT '0',
  `rotation3` float NOT NULL DEFAULT '0',
  `spawntimesecs` int(11) NOT NULL DEFAULT '0',
  `animprogress` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `state` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`guid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='Gameobject System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gameobject`
--

LOCK TABLES `gameobject` WRITE;
/*!40000 ALTER TABLE `gameobject` DISABLE KEYS */;
/*!40000 ALTER TABLE `gameobject` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gameobject_involvedrelation`
--

DROP TABLE IF EXISTS `gameobject_involvedrelation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gameobject_involvedrelation` (
  `id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `quest` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'Quest Identifier',
  PRIMARY KEY (`id`,`quest`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Gameobject involed in events';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gameobject_involvedrelation`
--

LOCK TABLES `gameobject_involvedrelation` WRITE;
/*!40000 ALTER TABLE `gameobject_involvedrelation` DISABLE KEYS */;
/*!40000 ALTER TABLE `gameobject_involvedrelation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gameobject_loot_template`
--

DROP TABLE IF EXISTS `gameobject_loot_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gameobject_loot_template` (
  `entry` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `item` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `ChanceOrQuestChance` float NOT NULL DEFAULT '100',
  `lootmode` smallint(5) unsigned NOT NULL DEFAULT '1',
  `groupid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `mincountOrRef` mediumint(9) NOT NULL DEFAULT '1',
  `maxcount` tinyint(3) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`entry`,`item`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Loot System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gameobject_loot_template`
--

LOCK TABLES `gameobject_loot_template` WRITE;
/*!40000 ALTER TABLE `gameobject_loot_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `gameobject_loot_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gameobject_questrelation`
--

DROP TABLE IF EXISTS `gameobject_questrelation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gameobject_questrelation` (
  `id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `quest` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'Quest Identifier',
  PRIMARY KEY (`id`,`quest`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Gameobjects related by quests';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gameobject_questrelation`
--

LOCK TABLES `gameobject_questrelation` WRITE;
/*!40000 ALTER TABLE `gameobject_questrelation` DISABLE KEYS */;
/*!40000 ALTER TABLE `gameobject_questrelation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gameobject_respawn`
--

DROP TABLE IF EXISTS `gameobject_respawn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gameobject_respawn` (
  `guid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Global Unique Identifier',
  `respawntime` bigint(20) unsigned NOT NULL DEFAULT '0',
  `instance` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`guid`,`instance`),
  KEY `instance` (`instance`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='CTDB Grid Loading System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gameobject_respawn`
--

LOCK TABLES `gameobject_respawn` WRITE;
/*!40000 ALTER TABLE `gameobject_respawn` DISABLE KEYS */;
/*!40000 ALTER TABLE `gameobject_respawn` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gameobject_scripts`
--

DROP TABLE IF EXISTS `gameobject_scripts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gameobject_scripts` (
  `id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `delay` int(10) unsigned NOT NULL DEFAULT '0',
  `command` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `datalong` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `datalong2` int(10) unsigned NOT NULL DEFAULT '0',
  `dataint` int(11) NOT NULL DEFAULT '0',
  `x` float NOT NULL DEFAULT '0',
  `y` float NOT NULL DEFAULT '0',
  `z` float NOT NULL DEFAULT '0',
  `o` float NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Gameobjects scripts';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gameobject_scripts`
--

LOCK TABLES `gameobject_scripts` WRITE;
/*!40000 ALTER TABLE `gameobject_scripts` DISABLE KEYS */;
/*!40000 ALTER TABLE `gameobject_scripts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gameobject_template`
--

DROP TABLE IF EXISTS `gameobject_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gameobject_template` (
  `entry` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `displayId` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL DEFAULT '',
  `IconName` varchar(100) NOT NULL DEFAULT '',
  `castBarCaption` varchar(100) NOT NULL DEFAULT '',
  `unk1` varchar(100) NOT NULL DEFAULT '',
  `faction` smallint(5) unsigned NOT NULL DEFAULT '0',
  `flags` int(10) unsigned NOT NULL DEFAULT '0',
  `size` float NOT NULL DEFAULT '1',
  `questItem1` int(11) unsigned NOT NULL DEFAULT '0',
  `questItem2` int(11) unsigned NOT NULL DEFAULT '0',
  `questItem3` int(11) unsigned NOT NULL DEFAULT '0',
  `questItem4` int(11) unsigned NOT NULL DEFAULT '0',
  `questItem5` int(11) unsigned NOT NULL DEFAULT '0',
  `questItem6` int(11) unsigned NOT NULL DEFAULT '0',
  `data0` int(15) unsigned NOT NULL DEFAULT '0',
  `data1` int(15) NOT NULL DEFAULT '-1',
  `data2` int(15) unsigned NOT NULL DEFAULT '0',
  `data3` int(15) unsigned NOT NULL DEFAULT '0',
  `data4` int(15) unsigned NOT NULL DEFAULT '0',
  `data5` int(15) unsigned NOT NULL DEFAULT '0',
  `data6` int(11) NOT NULL DEFAULT '-1',
  `data7` int(10) unsigned NOT NULL DEFAULT '0',
  `data8` int(10) unsigned NOT NULL DEFAULT '0',
  `data9` int(10) unsigned NOT NULL DEFAULT '0',
  `data10` int(10) unsigned NOT NULL DEFAULT '0',
  `data11` int(10) unsigned NOT NULL DEFAULT '0',
  `data12` int(10) unsigned NOT NULL DEFAULT '0',
  `data13` int(10) unsigned NOT NULL DEFAULT '0',
  `data14` int(10) unsigned NOT NULL DEFAULT '0',
  `data15` int(10) unsigned NOT NULL DEFAULT '0',
  `data16` int(10) unsigned NOT NULL DEFAULT '0',
  `data17` int(10) unsigned NOT NULL DEFAULT '0',
  `data18` int(10) unsigned NOT NULL DEFAULT '0',
  `data19` int(10) unsigned NOT NULL DEFAULT '0',
  `data20` int(10) unsigned NOT NULL DEFAULT '0',
  `data21` int(10) unsigned NOT NULL DEFAULT '0',
  `data22` int(10) unsigned NOT NULL DEFAULT '0',
  `data23` int(10) unsigned NOT NULL DEFAULT '0',
  `data24` int(10) DEFAULT NULL,
  `data25` int(10) DEFAULT NULL,
  `data26` int(10) DEFAULT NULL,
  `data27` int(10) DEFAULT NULL,
  `data28` int(10) DEFAULT NULL,
  `data29` int(10) DEFAULT NULL,
  `data30` int(10) DEFAULT NULL,
  `data31` int(10) DEFAULT NULL,
  `AIName` char(64) NOT NULL DEFAULT '',
  `ScriptName` varchar(64) NOT NULL DEFAULT '',
  `WDBVerified` smallint(5) DEFAULT '1',
  PRIMARY KEY (`entry`),
  KEY `idx_name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='CTDB Gameobject System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gameobject_template`
--

LOCK TABLES `gameobject_template` WRITE;
/*!40000 ALTER TABLE `gameobject_template` DISABLE KEYS */;
INSERT INTO `gameobject_template` VALUES (21680,16,787,'Duel Flag','','','',5,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','',13329),(191126,16,787,'Duel Flag','','','',2084,6553600,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','',12340),(194306,16,787,'Duel Flag','','','',0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','',11723),(191884,16,787,'Challenge Flag','','','',0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','',11723),(204328,16,787,'Gurubashi Challenge Flag','','','',0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','',13329);
/*!40000 ALTER TABLE `gameobject_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gossip_menu`
--

DROP TABLE IF EXISTS `gossip_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gossip_menu` (
  `entry` smallint(6) unsigned NOT NULL DEFAULT '0',
  `text_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`entry`,`text_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Gossip Menus';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gossip_menu`
--

LOCK TABLES `gossip_menu` WRITE;
/*!40000 ALTER TABLE `gossip_menu` DISABLE KEYS */;
/*!40000 ALTER TABLE `gossip_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gossip_menu_option`
--

DROP TABLE IF EXISTS `gossip_menu_option`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gossip_menu_option` (
  `menu_id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `option_icon` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `option_text` text,
  `option_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `npc_option_npcflag` int(10) unsigned NOT NULL DEFAULT '0',
  `action_menu_id` int(11) unsigned NOT NULL DEFAULT '0',
  `action_poi_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `action_script_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `box_coded` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `box_money` int(11) unsigned NOT NULL DEFAULT '0',
  `box_text` text,
  PRIMARY KEY (`menu_id`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Gossip menu options';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gossip_menu_option`
--

LOCK TABLES `gossip_menu_option` WRITE;
/*!40000 ALTER TABLE `gossip_menu_option` DISABLE KEYS */;
/*!40000 ALTER TABLE `gossip_menu_option` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gossip_scripts`
--

DROP TABLE IF EXISTS `gossip_scripts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gossip_scripts` (
  `id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `delay` int(10) unsigned NOT NULL DEFAULT '0',
  `command` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `datalong` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `datalong2` int(10) unsigned NOT NULL DEFAULT '0',
  `dataint` int(11) NOT NULL DEFAULT '0',
  `x` float NOT NULL DEFAULT '0',
  `y` float NOT NULL DEFAULT '0',
  `z` float NOT NULL DEFAULT '0',
  `o` float NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Gossip scripts';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gossip_scripts`
--

LOCK TABLES `gossip_scripts` WRITE;
/*!40000 ALTER TABLE `gossip_scripts` DISABLE KEYS */;
/*!40000 ALTER TABLE `gossip_scripts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `guild_rewards`
--

DROP TABLE IF EXISTS `guild_rewards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `guild_rewards` (
  `item_entry` int(11) NOT NULL,
  `price` int(22) DEFAULT NULL,
  `achievement` int(11) NOT NULL,
  `standing` int(1) NOT NULL,
  PRIMARY KEY (`item_entry`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guild_rewards`
--

LOCK TABLES `guild_rewards` WRITE;
/*!40000 ALTER TABLE `guild_rewards` DISABLE KEYS */;
INSERT INTO `guild_rewards` VALUES (67107,15000000,5492,4),(63398,3000000,5144,4),(62287,200000000,4943,2),(63353,1500000,4989,2),(62038,12000000,4944,2),(62039,12000000,4944,2),(62040,12000000,4944,2),(63207,3000000,4945,2),(61931,15000000,4946,2),(62286,100000000,4943,1),(65364,5000000,5201,2),(64402,3000000,5422,1),(65362,3000000,5179,1),(63125,30000000,4988,0),(64401,2000000,5143,1),(62800,1500000,5036,2),(62799,1500000,5467,2),(65498,1500000,5024,2),(65274,5000000,5035,2),(65435,1500000,5465,2),(64400,1500000,4860,1),(63138,3000000,5125,2),(61935,15000000,4946,2),(61936,15000000,4946,2),(61937,15000000,4946,2),(61942,15000000,4946,2),(61958,15000000,4946,2),(62298,15000000,4912,4),(63206,3000000,4945,2),(63352,1500000,4989,2),(63359,1500000,4860,1),(64398,2000000,5143,1),(64399,3000000,5422,1),(65360,5000000,5035,2),(65361,3000000,5031,1),(65363,5000000,5201,2);
/*!40000 ALTER TABLE `guild_rewards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `guild_xp_for_level`
--

DROP TABLE IF EXISTS `guild_xp_for_level`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `guild_xp_for_level` (
  `lvl` int(11) NOT NULL,
  `xp_for_next_level` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='CTDB XP for Guilds';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guild_xp_for_level`
--

LOCK TABLES `guild_xp_for_level` WRITE;
/*!40000 ALTER TABLE `guild_xp_for_level` DISABLE KEYS */;
/*!40000 ALTER TABLE `guild_xp_for_level` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `instance`
--

DROP TABLE IF EXISTS `instance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `instance` (
  `id` int(11) unsigned NOT NULL DEFAULT '0',
  `map` int(11) unsigned NOT NULL DEFAULT '0',
  `resettime` bigint(40) NOT NULL DEFAULT '0',
  `difficulty` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `data` longtext,
  `newid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `map` (`map`),
  KEY `resettime` (`resettime`),
  KEY `difficulty` (`difficulty`),
  KEY `newid` (`newid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Instances';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `instance`
--

LOCK TABLES `instance` WRITE;
/*!40000 ALTER TABLE `instance` DISABLE KEYS */;
/*!40000 ALTER TABLE `instance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `instance_encounters`
--

DROP TABLE IF EXISTS `instance_encounters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `instance_encounters` (
  `entry` int(10) unsigned NOT NULL COMMENT 'Unique entry from DungeonEncounter.dbc',
  `creditType` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `creditEntry` int(10) unsigned NOT NULL DEFAULT '0',
  `lastEncounterDungeon` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'If not 0, LfgDungeon.dbc entry for the instance it is last encounter in',
  `comment` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Encounters';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `instance_encounters`
--

LOCK TABLES `instance_encounters` WRITE;
/*!40000 ALTER TABLE `instance_encounters` DISABLE KEYS */;
/*!40000 ALTER TABLE `instance_encounters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `instance_template`
--

DROP TABLE IF EXISTS `instance_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `instance_template` (
  `map` smallint(5) unsigned NOT NULL,
  `parent` int(10) unsigned NOT NULL,
  `startLocX` float DEFAULT NULL,
  `startLocY` float DEFAULT NULL,
  `startLocZ` float DEFAULT NULL,
  `startLocO` float DEFAULT NULL,
  `script` varchar(128) NOT NULL DEFAULT '',
  `allowMount` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`map`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Instance Template';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `instance_template`
--

LOCK TABLES `instance_template` WRITE;
/*!40000 ALTER TABLE `instance_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `instance_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_enchantment_template`
--

DROP TABLE IF EXISTS `item_enchantment_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_enchantment_template` (
  `entry` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `ench` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `chance` float unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`entry`,`ench`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='Item Random Enchantment System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_enchantment_template`
--

LOCK TABLES `item_enchantment_template` WRITE;
/*!40000 ALTER TABLE `item_enchantment_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_enchantment_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_loot_template`
--

DROP TABLE IF EXISTS `item_loot_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_loot_template` (
  `entry` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `item` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `ChanceOrQuestChance` float NOT NULL DEFAULT '100',
  `lootmode` smallint(5) unsigned NOT NULL DEFAULT '1',
  `groupid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `mincountOrRef` mediumint(9) NOT NULL DEFAULT '1',
  `maxcount` smallint(5) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`entry`,`item`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='CTDB Loot System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_loot_template`
--

LOCK TABLES `item_loot_template` WRITE;
/*!40000 ALTER TABLE `item_loot_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_loot_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_set_names`
--

DROP TABLE IF EXISTS `item_set_names`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_set_names` (
  `entry` mediumint(8) unsigned NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `InventoryType` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `WDBVerified` smallint(5) NOT NULL DEFAULT '1',
  PRIMARY KEY (`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='CTDB Item set names';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_set_names`
--

LOCK TABLES `item_set_names` WRITE;
/*!40000 ALTER TABLE `item_set_names` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_set_names` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_template`
--

DROP TABLE IF EXISTS `item_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_template` (
  `entry` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `class` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `subclass` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `unk0` int(11) NOT NULL DEFAULT '-1',
  `name` varchar(255) NOT NULL DEFAULT '',
  `displayid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `Quality` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `Flags` bigint(20) NOT NULL DEFAULT '0',
  `FlagsExtra` int(10) unsigned NOT NULL DEFAULT '0',
  `BuyCount` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `BuyPrice` bigint(20) NOT NULL DEFAULT '0',
  `SellPrice` int(10) unsigned NOT NULL DEFAULT '0',
  `InventoryType` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `AllowableClass` int(11) NOT NULL DEFAULT '-1',
  `AllowableRace` int(11) NOT NULL DEFAULT '-1',
  `ItemLevel` smallint(5) unsigned NOT NULL DEFAULT '0',
  `RequiredLevel` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `RequiredSkill` smallint(5) unsigned NOT NULL DEFAULT '0',
  `RequiredSkillRank` smallint(5) unsigned NOT NULL DEFAULT '0',
  `requiredspell` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `requiredhonorrank` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `RequiredCityRank` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `RequiredReputationFaction` smallint(5) unsigned NOT NULL DEFAULT '0',
  `RequiredReputationRank` smallint(5) unsigned NOT NULL DEFAULT '0',
  `maxcount` int(11) NOT NULL DEFAULT '0',
  `stackable` int(11) DEFAULT '1',
  `ContainerSlots` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `stat_type1` tinyint(3) NOT NULL DEFAULT '0',
  `stat_value1` smallint(6) NOT NULL DEFAULT '0',
  `stat_type2` tinyint(3) NOT NULL DEFAULT '0',
  `stat_value2` smallint(6) NOT NULL DEFAULT '0',
  `stat_type3` tinyint(3) NOT NULL DEFAULT '0',
  `stat_value3` smallint(6) NOT NULL DEFAULT '0',
  `stat_type4` tinyint(3) NOT NULL DEFAULT '0',
  `stat_value4` smallint(6) NOT NULL DEFAULT '0',
  `stat_type5` tinyint(3) NOT NULL DEFAULT '0',
  `stat_value5` smallint(6) NOT NULL DEFAULT '0',
  `stat_type6` tinyint(3) NOT NULL DEFAULT '0',
  `stat_value6` smallint(6) NOT NULL DEFAULT '0',
  `stat_type7` tinyint(3) NOT NULL DEFAULT '0',
  `stat_value7` smallint(6) NOT NULL DEFAULT '0',
  `stat_type8` tinyint(3) NOT NULL DEFAULT '0',
  `stat_value8` smallint(6) NOT NULL DEFAULT '0',
  `stat_type9` tinyint(3) NOT NULL DEFAULT '0',
  `stat_value9` smallint(6) NOT NULL DEFAULT '0',
  `stat_type10` tinyint(3) NOT NULL DEFAULT '0',
  `stat_value10` smallint(6) NOT NULL DEFAULT '0',
  `ScalingStatDistribution` smallint(6) NOT NULL DEFAULT '0',
  `ScalingStatValue` int(6) unsigned NOT NULL DEFAULT '0',
  `damageType` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `delay` smallint(5) unsigned NOT NULL DEFAULT '1000',
  `RangedModRange` float NOT NULL DEFAULT '0',
  `spellid_1` mediumint(8) NOT NULL DEFAULT '0',
  `spelltrigger_1` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `spellcharges_1` smallint(4) DEFAULT NULL,
  `spellppmRate_1` float NOT NULL DEFAULT '0',
  `spellcooldown_1` int(11) NOT NULL DEFAULT '-1',
  `spellcategory_1` smallint(5) unsigned NOT NULL DEFAULT '0',
  `spellcategorycooldown_1` int(11) NOT NULL DEFAULT '-1',
  `spellid_2` mediumint(8) NOT NULL DEFAULT '0',
  `spelltrigger_2` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `spellcharges_2` smallint(4) DEFAULT NULL,
  `spellppmRate_2` float NOT NULL DEFAULT '0',
  `spellcooldown_2` int(11) NOT NULL DEFAULT '-1',
  `spellcategory_2` smallint(5) unsigned NOT NULL DEFAULT '0',
  `spellcategorycooldown_2` int(11) NOT NULL DEFAULT '-1',
  `spellid_3` mediumint(8) NOT NULL DEFAULT '0',
  `spelltrigger_3` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `spellcharges_3` smallint(4) DEFAULT NULL,
  `spellppmRate_3` float NOT NULL DEFAULT '0',
  `spellcooldown_3` int(11) NOT NULL DEFAULT '-1',
  `spellcategory_3` smallint(5) unsigned NOT NULL DEFAULT '0',
  `spellcategorycooldown_3` int(11) NOT NULL DEFAULT '-1',
  `spellid_4` mediumint(8) NOT NULL DEFAULT '0',
  `spelltrigger_4` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `spellcharges_4` smallint(4) DEFAULT NULL,
  `spellppmRate_4` float NOT NULL DEFAULT '0',
  `spellcooldown_4` int(11) NOT NULL DEFAULT '-1',
  `spellcategory_4` smallint(5) unsigned NOT NULL DEFAULT '0',
  `spellcategorycooldown_4` int(11) NOT NULL DEFAULT '-1',
  `spellid_5` mediumint(8) NOT NULL DEFAULT '0',
  `spelltrigger_5` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `spellcharges_5` smallint(4) DEFAULT NULL,
  `spellppmRate_5` float NOT NULL DEFAULT '0',
  `spellcooldown_5` int(11) NOT NULL DEFAULT '-1',
  `spellcategory_5` smallint(5) unsigned NOT NULL DEFAULT '0',
  `spellcategorycooldown_5` int(11) NOT NULL DEFAULT '-1',
  `bonding` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `description` varchar(255) NOT NULL DEFAULT '',
  `PageText` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `LanguageID` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `PageMaterial` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `startquest` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `lockid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `Material` tinyint(4) NOT NULL DEFAULT '0',
  `sheath` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `RandomProperty` mediumint(8) NOT NULL DEFAULT '0',
  `RandomSuffix` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `block` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `itemset` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `MaxDurability` smallint(5) unsigned NOT NULL DEFAULT '0',
  `area` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `Map` smallint(6) NOT NULL DEFAULT '0',
  `BagFamily` mediumint(9) NOT NULL DEFAULT '0',
  `TotemCategory` mediumint(9) NOT NULL DEFAULT '0',
  `socketColor_1` tinyint(4) NOT NULL DEFAULT '0',
  `socketContent_1` mediumint(9) NOT NULL DEFAULT '0',
  `socketColor_2` tinyint(4) NOT NULL DEFAULT '0',
  `socketContent_2` mediumint(9) NOT NULL DEFAULT '0',
  `socketColor_3` tinyint(4) NOT NULL DEFAULT '0',
  `socketContent_3` mediumint(9) NOT NULL DEFAULT '0',
  `socketBonus` mediumint(9) NOT NULL DEFAULT '0',
  `GemProperties` mediumint(9) NOT NULL DEFAULT '0',
  `RequiredDisenchantSkill` smallint(6) NOT NULL DEFAULT '-1',
  `ArmorDamageModifier` float NOT NULL DEFAULT '0',
  `Duration` int(11) NOT NULL DEFAULT '0' COMMENT 'Duration in seconds. Negative value means realtime, postive value ingame time',
  `ItemLimitCategory` smallint(6) NOT NULL DEFAULT '0',
  `HolidayId` int(11) unsigned NOT NULL DEFAULT '0',
  `ScriptName` varchar(64) NOT NULL DEFAULT '',
  `DisenchantID` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `FoodType` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `minMoneyLoot` int(10) unsigned NOT NULL DEFAULT '0',
  `maxMoneyLoot` int(10) unsigned NOT NULL DEFAULT '0',
  `WDBVerified` smallint(5) DEFAULT '1',
  PRIMARY KEY (`entry`),
  KEY `idx_name` (`name`),
  KEY `items_index` (`class`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='CTDB Item System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_template`
--

LOCK TABLES `item_template` WRITE;
/*!40000 ALTER TABLE `item_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lfg_dungeon_encounters`
--

DROP TABLE IF EXISTS `lfg_dungeon_encounters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lfg_dungeon_encounters` (
  `achievementId` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Achievement marking final boss completion',
  `dungeonId` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Dungeon entry from dbc',
  PRIMARY KEY (`achievementId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB LFG';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lfg_dungeon_encounters`
--

LOCK TABLES `lfg_dungeon_encounters` WRITE;
/*!40000 ALTER TABLE `lfg_dungeon_encounters` DISABLE KEYS */;
/*!40000 ALTER TABLE `lfg_dungeon_encounters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lfg_dungeon_rewards`
--

DROP TABLE IF EXISTS `lfg_dungeon_rewards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lfg_dungeon_rewards` (
  `dungeonId` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Dungeon entry from dbc',
  `maxLevel` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Max level at which this reward is rewarded',
  `firstQuestId` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Quest id with rewards for first dungeon this day',
  `firstMoneyVar` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Money multiplier for completing the dungeon first time in a day with less players than max',
  `firstXPVar` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Experience multiplier for completing the dungeon first time in a day with less players than max',
  `otherQuestId` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Quest id with rewards for Nth dungeon this day',
  `otherMoneyVar` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Money multiplier for completing the dungeon Nth time in a day with less players than max',
  `otherXPVar` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Experience multiplier for completing the dungeon Nth time in a day with less players than max',
  PRIMARY KEY (`dungeonId`,`maxLevel`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lfg_dungeon_rewards`
--

LOCK TABLES `lfg_dungeon_rewards` WRITE;
/*!40000 ALTER TABLE `lfg_dungeon_rewards` DISABLE KEYS */;
/*!40000 ALTER TABLE `lfg_dungeon_rewards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `linked_respawn`
--

DROP TABLE IF EXISTS `linked_respawn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `linked_respawn` (
  `guid` int(10) unsigned NOT NULL COMMENT 'dependent creature',
  `linkedGuid` int(10) unsigned NOT NULL COMMENT 'master creature',
  `linkType` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`guid`,`linkType`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='Creature Respawn Link System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `linked_respawn`
--

LOCK TABLES `linked_respawn` WRITE;
/*!40000 ALTER TABLE `linked_respawn` DISABLE KEYS */;
/*!40000 ALTER TABLE `linked_respawn` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `locales_achievement_reward`
--

DROP TABLE IF EXISTS `locales_achievement_reward`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `locales_achievement_reward` (
  `entry` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `subject_loc1` varchar(100) NOT NULL DEFAULT '',
  `subject_loc2` varchar(100) NOT NULL DEFAULT '',
  `subject_loc3` varchar(100) NOT NULL DEFAULT '',
  `subject_loc4` varchar(100) NOT NULL DEFAULT '',
  `subject_loc5` varchar(100) NOT NULL DEFAULT '',
  `subject_loc6` varchar(100) NOT NULL DEFAULT '',
  `subject_loc7` varchar(100) NOT NULL DEFAULT '',
  `subject_loc8` varchar(100) NOT NULL DEFAULT '',
  `text_loc1` text,
  `text_loc2` text,
  `text_loc3` text,
  `text_loc4` text,
  `text_loc5` text,
  `text_loc6` text,
  `text_loc7` text,
  `text_loc8` text,
  PRIMARY KEY (`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Achievement Reward Locales';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `locales_achievement_reward`
--

LOCK TABLES `locales_achievement_reward` WRITE;
/*!40000 ALTER TABLE `locales_achievement_reward` DISABLE KEYS */;
/*!40000 ALTER TABLE `locales_achievement_reward` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `locales_creature`
--

DROP TABLE IF EXISTS `locales_creature`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `locales_creature` (
  `entry` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `name_loc1` varchar(100) NOT NULL DEFAULT '',
  `name_loc2` varchar(100) NOT NULL DEFAULT '',
  `name_loc3` varchar(100) NOT NULL DEFAULT '',
  `name_loc4` varchar(100) NOT NULL DEFAULT '',
  `name_loc5` varchar(100) NOT NULL DEFAULT '',
  `name_loc6` varchar(100) NOT NULL DEFAULT '',
  `name_loc7` varchar(100) NOT NULL DEFAULT '',
  `name_loc8` varchar(100) NOT NULL DEFAULT '',
  `subname_loc1` varchar(100) DEFAULT NULL,
  `subname_loc2` varchar(100) DEFAULT NULL,
  `subname_loc3` varchar(100) DEFAULT NULL,
  `subname_loc4` varchar(100) DEFAULT NULL,
  `subname_loc5` varchar(100) DEFAULT NULL,
  `subname_loc6` varchar(100) DEFAULT NULL,
  `subname_loc7` varchar(100) DEFAULT NULL,
  `subname_loc8` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Creature locales';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `locales_creature`
--

LOCK TABLES `locales_creature` WRITE;
/*!40000 ALTER TABLE `locales_creature` DISABLE KEYS */;
/*!40000 ALTER TABLE `locales_creature` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `locales_gameobject`
--

DROP TABLE IF EXISTS `locales_gameobject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `locales_gameobject` (
  `entry` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `name_loc1` varchar(100) NOT NULL DEFAULT '',
  `name_loc2` varchar(100) NOT NULL DEFAULT '',
  `name_loc3` varchar(100) NOT NULL DEFAULT '',
  `name_loc4` varchar(100) NOT NULL DEFAULT '',
  `name_loc5` varchar(100) NOT NULL DEFAULT '',
  `name_loc6` varchar(100) NOT NULL DEFAULT '',
  `name_loc7` varchar(100) NOT NULL DEFAULT '',
  `name_loc8` varchar(100) NOT NULL DEFAULT '',
  `castbarcaption_loc1` varchar(100) NOT NULL DEFAULT '',
  `castbarcaption_loc2` varchar(100) NOT NULL DEFAULT '',
  `castbarcaption_loc3` varchar(100) NOT NULL DEFAULT '',
  `castbarcaption_loc4` varchar(100) NOT NULL DEFAULT '',
  `castbarcaption_loc5` varchar(100) NOT NULL DEFAULT '',
  `castbarcaption_loc6` varchar(100) NOT NULL DEFAULT '',
  `castbarcaption_loc7` varchar(100) NOT NULL DEFAULT '',
  `castbarcaption_loc8` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Gameobjects locales';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `locales_gameobject`
--

LOCK TABLES `locales_gameobject` WRITE;
/*!40000 ALTER TABLE `locales_gameobject` DISABLE KEYS */;
/*!40000 ALTER TABLE `locales_gameobject` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `locales_gossip_menu_option`
--

DROP TABLE IF EXISTS `locales_gossip_menu_option`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `locales_gossip_menu_option` (
  `menu_id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `option_text_loc1` text,
  `option_text_loc2` text,
  `option_text_loc3` text,
  `option_text_loc4` text,
  `option_text_loc5` text,
  `option_text_loc6` text,
  `option_text_loc7` text,
  `option_text_loc8` text,
  `box_text_loc1` text,
  `box_text_loc2` text,
  `box_text_loc3` text,
  `box_text_loc4` text,
  `box_text_loc5` text,
  `box_text_loc6` text,
  `box_text_loc7` text,
  `box_text_loc8` text,
  PRIMARY KEY (`menu_id`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Gossips locales';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `locales_gossip_menu_option`
--

LOCK TABLES `locales_gossip_menu_option` WRITE;
/*!40000 ALTER TABLE `locales_gossip_menu_option` DISABLE KEYS */;
/*!40000 ALTER TABLE `locales_gossip_menu_option` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `locales_item`
--

DROP TABLE IF EXISTS `locales_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `locales_item` (
  `entry` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `name_loc1` varchar(100) NOT NULL DEFAULT '',
  `name_loc2` varchar(100) NOT NULL DEFAULT '',
  `name_loc3` varchar(100) NOT NULL DEFAULT '',
  `name_loc4` varchar(100) NOT NULL DEFAULT '',
  `name_loc5` varchar(100) NOT NULL DEFAULT '',
  `name_loc6` varchar(100) NOT NULL DEFAULT '',
  `name_loc7` varchar(100) NOT NULL DEFAULT '',
  `name_loc8` varchar(100) NOT NULL DEFAULT '',
  `description_loc1` varchar(255) DEFAULT NULL,
  `description_loc2` varchar(255) DEFAULT NULL,
  `description_loc3` varchar(255) DEFAULT NULL,
  `description_loc4` varchar(255) DEFAULT NULL,
  `description_loc5` varchar(255) DEFAULT NULL,
  `description_loc6` varchar(255) DEFAULT NULL,
  `description_loc7` varchar(255) DEFAULT NULL,
  `description_loc8` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Items locales';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `locales_item`
--

LOCK TABLES `locales_item` WRITE;
/*!40000 ALTER TABLE `locales_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `locales_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `locales_item_set_names`
--

DROP TABLE IF EXISTS `locales_item_set_names`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `locales_item_set_names` (
  `entry` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `name_loc1` varchar(100) NOT NULL DEFAULT '',
  `name_loc2` varchar(100) NOT NULL DEFAULT '',
  `name_loc3` varchar(100) NOT NULL DEFAULT '',
  `name_loc4` varchar(100) NOT NULL DEFAULT '',
  `name_loc5` varchar(100) NOT NULL DEFAULT '',
  `name_loc6` varchar(100) NOT NULL DEFAULT '',
  `name_loc7` varchar(100) NOT NULL DEFAULT '',
  `name_loc8` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Item set names locales';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `locales_item_set_names`
--

LOCK TABLES `locales_item_set_names` WRITE;
/*!40000 ALTER TABLE `locales_item_set_names` DISABLE KEYS */;
/*!40000 ALTER TABLE `locales_item_set_names` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `locales_npc_text`
--

DROP TABLE IF EXISTS `locales_npc_text`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `locales_npc_text` (
  `entry` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `Text0_0_loc1` longtext,
  `Text0_0_loc2` longtext,
  `Text0_0_loc3` longtext,
  `Text0_0_loc4` longtext,
  `Text0_0_loc5` longtext,
  `Text0_0_loc6` longtext,
  `Text0_0_loc7` longtext,
  `Text0_0_loc8` longtext,
  `Text0_1_loc1` longtext,
  `Text0_1_loc2` longtext,
  `Text0_1_loc3` longtext,
  `Text0_1_loc4` longtext,
  `Text0_1_loc5` longtext,
  `Text0_1_loc6` longtext,
  `Text0_1_loc7` longtext,
  `Text0_1_loc8` longtext,
  `Text1_0_loc1` longtext,
  `Text1_0_loc2` longtext,
  `Text1_0_loc3` longtext,
  `Text1_0_loc4` longtext,
  `Text1_0_loc5` longtext,
  `Text1_0_loc6` longtext,
  `Text1_0_loc7` longtext,
  `Text1_0_loc8` longtext,
  `Text1_1_loc1` longtext,
  `Text1_1_loc2` longtext,
  `Text1_1_loc3` longtext,
  `Text1_1_loc4` longtext,
  `Text1_1_loc5` longtext,
  `Text1_1_loc6` longtext,
  `Text1_1_loc7` longtext,
  `Text1_1_loc8` longtext,
  `Text2_0_loc1` longtext,
  `Text2_0_loc2` longtext,
  `Text2_0_loc3` longtext,
  `Text2_0_loc4` longtext,
  `Text2_0_loc5` longtext,
  `Text2_0_loc6` longtext,
  `Text2_0_loc7` longtext,
  `Text2_0_loc8` longtext,
  `Text2_1_loc1` longtext,
  `Text2_1_loc2` longtext,
  `Text2_1_loc3` longtext,
  `Text2_1_loc4` longtext,
  `Text2_1_loc5` longtext,
  `Text2_1_loc6` longtext,
  `Text2_1_loc7` longtext,
  `Text2_1_loc8` longtext,
  `Text3_0_loc1` longtext,
  `Text3_0_loc2` longtext,
  `Text3_0_loc3` longtext,
  `Text3_0_loc4` longtext,
  `Text3_0_loc5` longtext,
  `Text3_0_loc6` longtext,
  `Text3_0_loc7` longtext,
  `Text3_0_loc8` longtext,
  `Text3_1_loc1` longtext,
  `Text3_1_loc2` longtext,
  `Text3_1_loc3` longtext,
  `Text3_1_loc4` longtext,
  `Text3_1_loc5` longtext,
  `Text3_1_loc6` longtext,
  `Text3_1_loc7` longtext,
  `Text3_1_loc8` longtext,
  `Text4_0_loc1` longtext,
  `Text4_0_loc2` longtext,
  `Text4_0_loc3` longtext,
  `Text4_0_loc4` longtext,
  `Text4_0_loc5` longtext,
  `Text4_0_loc6` longtext,
  `Text4_0_loc7` longtext,
  `Text4_0_loc8` longtext,
  `Text4_1_loc1` longtext,
  `Text4_1_loc2` longtext,
  `Text4_1_loc3` longtext,
  `Text4_1_loc4` longtext,
  `Text4_1_loc5` longtext,
  `Text4_1_loc6` longtext,
  `Text4_1_loc7` longtext,
  `Text4_1_loc8` longtext,
  `Text5_0_loc1` longtext,
  `Text5_0_loc2` longtext,
  `Text5_0_loc3` longtext,
  `Text5_0_loc4` longtext,
  `Text5_0_loc5` longtext,
  `Text5_0_loc6` longtext,
  `Text5_0_loc7` longtext,
  `Text5_0_loc8` longtext,
  `Text5_1_loc1` longtext,
  `Text5_1_loc2` longtext,
  `Text5_1_loc3` longtext,
  `Text5_1_loc4` longtext,
  `Text5_1_loc5` longtext,
  `Text5_1_loc6` longtext,
  `Text5_1_loc7` longtext,
  `Text5_1_loc8` longtext,
  `Text6_0_loc1` longtext,
  `Text6_0_loc2` longtext,
  `Text6_0_loc3` longtext,
  `Text6_0_loc4` longtext,
  `Text6_0_loc5` longtext,
  `Text6_0_loc6` longtext,
  `Text6_0_loc7` longtext,
  `Text6_0_loc8` longtext,
  `Text6_1_loc1` longtext,
  `Text6_1_loc2` longtext,
  `Text6_1_loc3` longtext,
  `Text6_1_loc4` longtext,
  `Text6_1_loc5` longtext,
  `Text6_1_loc6` longtext,
  `Text6_1_loc7` longtext,
  `Text6_1_loc8` longtext,
  `Text7_0_loc1` longtext,
  `Text7_0_loc2` longtext,
  `Text7_0_loc3` longtext,
  `Text7_0_loc4` longtext,
  `Text7_0_loc5` longtext,
  `Text7_0_loc6` longtext,
  `Text7_0_loc7` longtext,
  `Text7_0_loc8` longtext,
  `Text7_1_loc1` longtext,
  `Text7_1_loc2` longtext,
  `Text7_1_loc3` longtext,
  `Text7_1_loc4` longtext,
  `Text7_1_loc5` longtext,
  `Text7_1_loc6` longtext,
  `Text7_1_loc7` longtext,
  `Text7_1_loc8` longtext,
  PRIMARY KEY (`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Npc text locales';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `locales_npc_text`
--

LOCK TABLES `locales_npc_text` WRITE;
/*!40000 ALTER TABLE `locales_npc_text` DISABLE KEYS */;
/*!40000 ALTER TABLE `locales_npc_text` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `locales_page_text`
--

DROP TABLE IF EXISTS `locales_page_text`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `locales_page_text` (
  `entry` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `Text_loc1` longtext,
  `Text_loc2` longtext,
  `Text_loc3` longtext,
  `Text_loc4` longtext,
  `Text_loc5` longtext,
  `Text_loc6` longtext,
  `Text_loc7` longtext,
  `Text_loc8` longtext,
  PRIMARY KEY (`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Page Text locales';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `locales_page_text`
--

LOCK TABLES `locales_page_text` WRITE;
/*!40000 ALTER TABLE `locales_page_text` DISABLE KEYS */;
/*!40000 ALTER TABLE `locales_page_text` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `locales_points_of_interest`
--

DROP TABLE IF EXISTS `locales_points_of_interest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `locales_points_of_interest` (
  `entry` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `icon_name_loc1` text,
  `icon_name_loc2` text,
  `icon_name_loc3` text,
  `icon_name_loc4` text,
  `icon_name_loc5` text,
  `icon_name_loc6` text,
  `icon_name_loc7` text,
  `icon_name_loc8` text,
  PRIMARY KEY (`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Points of interest locales';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `locales_points_of_interest`
--

LOCK TABLES `locales_points_of_interest` WRITE;
/*!40000 ALTER TABLE `locales_points_of_interest` DISABLE KEYS */;
/*!40000 ALTER TABLE `locales_points_of_interest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `locales_quest`
--

DROP TABLE IF EXISTS `locales_quest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `locales_quest` (
  `entry` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `Title_loc1` text,
  `Title_loc2` text,
  `Title_loc3` text,
  `Title_loc4` text,
  `Title_loc5` text,
  `Title_loc6` text,
  `Title_loc7` text,
  `Title_loc8` text,
  `Details_loc1` text,
  `Details_loc2` text,
  `Details_loc3` text,
  `Details_loc4` text,
  `Details_loc5` text,
  `Details_loc6` text,
  `Details_loc7` text,
  `Details_loc8` text,
  `Objectives_loc1` text,
  `Objectives_loc2` text,
  `Objectives_loc3` text,
  `Objectives_loc4` text,
  `Objectives_loc5` text,
  `Objectives_loc6` text,
  `Objectives_loc7` text,
  `Objectives_loc8` text,
  `OfferRewardText_loc1` text,
  `OfferRewardText_loc2` text,
  `OfferRewardText_loc3` text,
  `OfferRewardText_loc4` text,
  `OfferRewardText_loc5` text,
  `OfferRewardText_loc6` text,
  `OfferRewardText_loc7` text,
  `OfferRewardText_loc8` text,
  `RequestItemsText_loc1` text,
  `RequestItemsText_loc2` text,
  `RequestItemsText_loc3` text,
  `RequestItemsText_loc4` text,
  `RequestItemsText_loc5` text,
  `RequestItemsText_loc6` text,
  `RequestItemsText_loc7` text,
  `RequestItemsText_loc8` text,
  `EndText_loc1` text,
  `EndText_loc2` text,
  `EndText_loc3` text,
  `EndText_loc4` text,
  `EndText_loc5` text,
  `EndText_loc6` text,
  `EndText_loc7` text,
  `EndText_loc8` text,
  `CompletedText_loc1` text,
  `CompletedText_loc2` text,
  `CompletedText_loc3` text,
  `CompletedText_loc4` text,
  `CompletedText_loc5` text,
  `CompletedText_loc6` text,
  `CompletedText_loc7` text,
  `CompletedText_loc8` text,
  `ObjectiveText1_loc1` text,
  `ObjectiveText1_loc2` text,
  `ObjectiveText1_loc3` text,
  `ObjectiveText1_loc4` text,
  `ObjectiveText1_loc5` text,
  `ObjectiveText1_loc6` text,
  `ObjectiveText1_loc7` text,
  `ObjectiveText1_loc8` text,
  `ObjectiveText2_loc1` text,
  `ObjectiveText2_loc2` text,
  `ObjectiveText2_loc3` text,
  `ObjectiveText2_loc4` text,
  `ObjectiveText2_loc5` text,
  `ObjectiveText2_loc6` text,
  `ObjectiveText2_loc7` text,
  `ObjectiveText2_loc8` text,
  `ObjectiveText3_loc1` text,
  `ObjectiveText3_loc2` text,
  `ObjectiveText3_loc3` text,
  `ObjectiveText3_loc4` text,
  `ObjectiveText3_loc5` text,
  `ObjectiveText3_loc6` text,
  `ObjectiveText3_loc7` text,
  `ObjectiveText3_loc8` text,
  `ObjectiveText4_loc1` text,
  `ObjectiveText4_loc2` text,
  `ObjectiveText4_loc3` text,
  `ObjectiveText4_loc4` text,
  `ObjectiveText4_loc5` text,
  `ObjectiveText4_loc6` text,
  `ObjectiveText4_loc7` text,
  `ObjectiveText4_loc8` text,
  PRIMARY KEY (`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Quest locales';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `locales_quest`
--

LOCK TABLES `locales_quest` WRITE;
/*!40000 ALTER TABLE `locales_quest` DISABLE KEYS */;
/*!40000 ALTER TABLE `locales_quest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mail_level_reward`
--

DROP TABLE IF EXISTS `mail_level_reward`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mail_level_reward` (
  `level` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `raceMask` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `mailTemplateId` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `senderEntry` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`level`,`raceMask`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='Mail System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mail_level_reward`
--

LOCK TABLES `mail_level_reward` WRITE;
/*!40000 ALTER TABLE `mail_level_reward` DISABLE KEYS */;
/*!40000 ALTER TABLE `mail_level_reward` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mail_loot_template`
--

DROP TABLE IF EXISTS `mail_loot_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mail_loot_template` (
  `entry` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `item` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `ChanceOrQuestChance` float NOT NULL DEFAULT '100',
  `lootmode` smallint(5) unsigned NOT NULL DEFAULT '1',
  `groupid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `mincountOrRef` mediumint(9) NOT NULL DEFAULT '1',
  `maxcount` tinyint(3) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`entry`,`item`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='Loot System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mail_loot_template`
--

LOCK TABLES `mail_loot_template` WRITE;
/*!40000 ALTER TABLE `mail_loot_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `mail_loot_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `milling_loot_template`
--

DROP TABLE IF EXISTS `milling_loot_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `milling_loot_template` (
  `entry` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `item` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `ChanceOrQuestChance` float NOT NULL DEFAULT '100',
  `lootmode` smallint(5) unsigned NOT NULL DEFAULT '1',
  `groupid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `mincountOrRef` mediumint(9) NOT NULL DEFAULT '1',
  `maxcount` tinyint(3) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`entry`,`item`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='Loot System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `milling_loot_template`
--

LOCK TABLES `milling_loot_template` WRITE;
/*!40000 ALTER TABLE `milling_loot_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `milling_loot_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `npc_spellclick_spells`
--

DROP TABLE IF EXISTS `npc_spellclick_spells`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `npc_spellclick_spells` (
  `npc_entry` int(10) unsigned NOT NULL COMMENT 'reference to creature_template',
  `spell_id` int(10) unsigned NOT NULL COMMENT 'spell which should be casted ',
  `quest_start` mediumint(8) unsigned NOT NULL COMMENT 'reference to quest_template',
  `quest_start_active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `quest_end` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `cast_flags` tinyint(3) unsigned NOT NULL COMMENT 'first bit defines caster: 1=player, 0=creature; second bit defines target, same mapping as caster bit',
  `aura_required` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'player without aura cant click',
  `aura_forbidden` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'player with aura cant click',
  `user_type` smallint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'relation with summoner: 0-no 1-friendly 2-raid 3-party player can click'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Npc Spells on click';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `npc_spellclick_spells`
--

LOCK TABLES `npc_spellclick_spells` WRITE;
/*!40000 ALTER TABLE `npc_spellclick_spells` DISABLE KEYS */;
/*!40000 ALTER TABLE `npc_spellclick_spells` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `npc_text`
--

DROP TABLE IF EXISTS `npc_text`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `npc_text` (
  `ID` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `text0_0` longtext,
  `text0_1` longtext,
  `lang0` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `prob0` float NOT NULL DEFAULT '0',
  `em0_0` smallint(5) unsigned NOT NULL DEFAULT '0',
  `em0_1` smallint(5) unsigned NOT NULL DEFAULT '0',
  `em0_2` smallint(5) unsigned NOT NULL DEFAULT '0',
  `em0_3` smallint(5) unsigned NOT NULL DEFAULT '0',
  `em0_4` smallint(5) unsigned NOT NULL DEFAULT '0',
  `em0_5` smallint(5) unsigned NOT NULL DEFAULT '0',
  `text1_0` longtext,
  `text1_1` longtext,
  `lang1` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `prob1` float NOT NULL DEFAULT '0',
  `em1_0` smallint(5) unsigned NOT NULL DEFAULT '0',
  `em1_1` smallint(5) unsigned NOT NULL DEFAULT '0',
  `em1_2` smallint(5) unsigned NOT NULL DEFAULT '0',
  `em1_3` smallint(5) unsigned NOT NULL DEFAULT '0',
  `em1_4` smallint(5) unsigned NOT NULL DEFAULT '0',
  `em1_5` smallint(5) unsigned NOT NULL DEFAULT '0',
  `text2_0` longtext,
  `text2_1` longtext,
  `lang2` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `prob2` float NOT NULL DEFAULT '0',
  `em2_0` smallint(5) unsigned NOT NULL DEFAULT '0',
  `em2_1` smallint(5) unsigned NOT NULL DEFAULT '0',
  `em2_2` smallint(5) unsigned NOT NULL DEFAULT '0',
  `em2_3` smallint(5) unsigned NOT NULL DEFAULT '0',
  `em2_4` smallint(5) unsigned NOT NULL DEFAULT '0',
  `em2_5` smallint(5) unsigned NOT NULL DEFAULT '0',
  `text3_0` longtext,
  `text3_1` longtext,
  `lang3` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `prob3` float NOT NULL DEFAULT '0',
  `em3_0` smallint(5) unsigned NOT NULL DEFAULT '0',
  `em3_1` smallint(5) unsigned NOT NULL DEFAULT '0',
  `em3_2` smallint(5) unsigned NOT NULL DEFAULT '0',
  `em3_3` smallint(5) unsigned NOT NULL DEFAULT '0',
  `em3_4` smallint(5) unsigned NOT NULL DEFAULT '0',
  `em3_5` smallint(5) unsigned NOT NULL DEFAULT '0',
  `text4_0` longtext,
  `text4_1` longtext,
  `lang4` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `prob4` float NOT NULL DEFAULT '0',
  `em4_0` smallint(5) unsigned NOT NULL DEFAULT '0',
  `em4_1` smallint(5) unsigned NOT NULL DEFAULT '0',
  `em4_2` smallint(5) unsigned NOT NULL DEFAULT '0',
  `em4_3` smallint(5) unsigned NOT NULL DEFAULT '0',
  `em4_4` smallint(5) unsigned NOT NULL DEFAULT '0',
  `em4_5` smallint(5) unsigned NOT NULL DEFAULT '0',
  `text5_0` longtext,
  `text5_1` longtext,
  `lang5` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `prob5` float NOT NULL DEFAULT '0',
  `em5_0` smallint(5) unsigned NOT NULL DEFAULT '0',
  `em5_1` smallint(5) unsigned NOT NULL DEFAULT '0',
  `em5_2` smallint(5) unsigned NOT NULL DEFAULT '0',
  `em5_3` smallint(5) unsigned NOT NULL DEFAULT '0',
  `em5_4` smallint(5) unsigned NOT NULL DEFAULT '0',
  `em5_5` smallint(5) unsigned NOT NULL DEFAULT '0',
  `text6_0` longtext,
  `text6_1` longtext,
  `lang6` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `prob6` float NOT NULL DEFAULT '0',
  `em6_0` smallint(5) unsigned NOT NULL DEFAULT '0',
  `em6_1` smallint(5) unsigned NOT NULL DEFAULT '0',
  `em6_2` smallint(5) unsigned NOT NULL DEFAULT '0',
  `em6_3` smallint(5) unsigned NOT NULL DEFAULT '0',
  `em6_4` smallint(5) unsigned NOT NULL DEFAULT '0',
  `em6_5` smallint(5) unsigned NOT NULL DEFAULT '0',
  `text7_0` longtext,
  `text7_1` longtext,
  `lang7` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `prob7` float NOT NULL DEFAULT '0',
  `em7_0` smallint(5) unsigned NOT NULL DEFAULT '0',
  `em7_1` smallint(5) unsigned NOT NULL DEFAULT '0',
  `em7_2` smallint(5) unsigned NOT NULL DEFAULT '0',
  `em7_3` smallint(5) unsigned NOT NULL DEFAULT '0',
  `em7_4` smallint(5) unsigned NOT NULL DEFAULT '0',
  `em7_5` smallint(5) unsigned NOT NULL DEFAULT '0',
  `WDBVerified` smallint(5) DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Npc System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `npc_text`
--

LOCK TABLES `npc_text` WRITE;
/*!40000 ALTER TABLE `npc_text` DISABLE KEYS */;
/*!40000 ALTER TABLE `npc_text` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `npc_trainer`
--

DROP TABLE IF EXISTS `npc_trainer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `npc_trainer` (
  `entry` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `spell` mediumint(8) NOT NULL DEFAULT '0',
  `spellcost` int(10) unsigned NOT NULL DEFAULT '0',
  `reqskill` smallint(5) unsigned NOT NULL DEFAULT '0',
  `reqskillvalue` smallint(5) unsigned NOT NULL DEFAULT '0',
  `reqlevel` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`entry`,`spell`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Npc System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `npc_trainer`
--

LOCK TABLES `npc_trainer` WRITE;
/*!40000 ALTER TABLE `npc_trainer` DISABLE KEYS */;
/*!40000 ALTER TABLE `npc_trainer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `npc_vendor`
--

DROP TABLE IF EXISTS `npc_vendor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `npc_vendor` (
  `entry` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `slot` smallint(6) NOT NULL DEFAULT '0',
  `item` mediumint(8) NOT NULL DEFAULT '0',
  `maxcount` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `incrtime` int(10) unsigned NOT NULL DEFAULT '0',
  `ExtendedCost` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`entry`,`item`,`ExtendedCost`),
  KEY `slot` (`slot`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='Npc System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `npc_vendor`
--

LOCK TABLES `npc_vendor` WRITE;
/*!40000 ALTER TABLE `npc_vendor` DISABLE KEYS */;
/*!40000 ALTER TABLE `npc_vendor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `outdoorpvp_template`
--

DROP TABLE IF EXISTS `outdoorpvp_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `outdoorpvp_template` (
  `TypeId` tinyint(2) unsigned NOT NULL,
  `ScriptName` char(64) NOT NULL DEFAULT '',
  `comment` text,
  PRIMARY KEY (`TypeId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='OutdoorPvP Templates';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `outdoorpvp_template`
--

LOCK TABLES `outdoorpvp_template` WRITE;
/*!40000 ALTER TABLE `outdoorpvp_template` DISABLE KEYS */;
INSERT INTO `outdoorpvp_template` VALUES (1,'outdoorpvp_hp','Hellfire Peninsula'),(2,'outdoorpvp_na','Nagrand'),(3,'outdoorpvp_tf','Terokkar Forest'),(4,'outdoorpvp_zm','Zangarmarsh'),(5,'outdoorpvp_si','Silithus'),(6,'outdoorpvp_ep','Eastern Plaguelands'),(7,'outdoorpvp_gh','Grizzly Hills');
/*!40000 ALTER TABLE `outdoorpvp_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_text`
--

DROP TABLE IF EXISTS `page_text`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_text` (
  `entry` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `text` longtext NOT NULL,
  `next_page` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `WDBVerified` smallint(5) DEFAULT '1',
  PRIMARY KEY (`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Item System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_text`
--

LOCK TABLES `page_text` WRITE;
/*!40000 ALTER TABLE `page_text` DISABLE KEYS */;
/*!40000 ALTER TABLE `page_text` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pet_levelstats`
--

DROP TABLE IF EXISTS `pet_levelstats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pet_levelstats` (
  `creature_entry` mediumint(8) unsigned NOT NULL,
  `level` tinyint(3) unsigned NOT NULL,
  `hp` smallint(5) unsigned NOT NULL,
  `mana` smallint(5) unsigned NOT NULL,
  `armor` int(10) unsigned NOT NULL DEFAULT '0',
  `str` smallint(5) unsigned NOT NULL,
  `agi` smallint(5) unsigned NOT NULL,
  `sta` smallint(5) unsigned NOT NULL,
  `inte` smallint(5) unsigned NOT NULL,
  `spi` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`creature_entry`,`level`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0 COMMENT='Stores pet levels stats.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pet_levelstats`
--

LOCK TABLES `pet_levelstats` WRITE;
/*!40000 ALTER TABLE `pet_levelstats` DISABLE KEYS */;
INSERT INTO `pet_levelstats` VALUES (1,1,42,1,20,22,20,22,20,20),(1,2,55,1,21,23,21,23,20,20),(1,3,71,1,46,24,21,24,20,21),(1,4,86,1,82,25,22,25,21,21),(1,5,102,1,126,26,23,25,21,21),(1,6,120,1,180,27,24,26,21,22),(1,7,137,1,245,28,24,27,21,22),(1,8,156,1,322,29,25,28,22,22),(1,9,176,1,412,30,26,29,22,23),(1,10,198,1,518,31,26,30,22,23),(1,11,222,1,545,32,27,34,22,24),(1,12,247,1,580,33,28,38,23,25),(1,13,273,1,615,34,30,42,23,25),(1,14,300,1,650,35,31,46,23,27),(1,15,328,1,685,37,32,51,24,28),(1,16,356,1,721,38,33,55,25,29),(1,17,386,1,756,40,34,59,25,30),(1,18,417,1,791,42,36,63,25,30),(1,19,449,1,826,44,37,67,26,32),(1,20,484,1,861,45,39,72,27,33),(1,21,521,1,897,47,39,76,27,34),(1,22,562,1,932,49,40,80,27,35),(1,23,605,1,967,50,42,84,28,36),(1,24,651,1,1002,52,43,88,28,37),(1,25,699,1,1037,53,45,94,29,39),(1,26,750,1,1072,55,46,98,30,39),(1,27,800,1,1108,56,47,102,30,40),(1,28,853,1,1142,58,49,106,30,41),(1,29,905,1,1177,60,50,110,31,42),(1,30,955,1,1212,61,52,115,32,44),(1,31,1006,1,1247,63,53,120,32,44),(1,32,1057,1,1283,64,54,124,33,46),(1,33,1110,1,1317,66,56,128,33,46),(1,34,1163,1,1353,67,57,132,33,48),(1,35,1220,1,1387,69,59,138,35,49),(1,36,1277,1,1494,70,60,142,35,50),(1,37,1336,1,1607,72,61,146,35,52),(1,38,1395,1,1724,74,63,151,36,52),(1,39,1459,1,1849,76,64,155,36,54),(1,40,1524,1,1980,78,66,160,37,55),(1,41,1585,1,2117,81,68,165,38,56),(1,42,1651,1,2262,86,69,169,38,57),(1,43,1716,1,2414,91,71,174,39,58),(1,44,1782,1,2574,97,72,178,39,59),(1,45,1848,1,2742,102,74,184,40,61),(1,46,1919,1,2798,104,75,188,41,62),(1,47,1990,1,2853,106,77,193,41,63),(1,48,2062,1,2907,108,79,197,42,64),(1,49,2138,1,2963,110,80,202,42,66),(1,50,2215,1,3018,113,82,207,43,67),(1,51,2292,1,3072,115,84,212,44,68),(1,52,2371,1,3128,117,85,216,44,69),(1,53,2453,1,3183,119,87,221,45,70),(1,54,2533,1,3237,122,89,226,45,72),(1,55,2614,1,3292,124,91,231,47,73),(1,56,2699,1,3348,127,92,236,47,74),(1,57,2784,1,3402,129,94,241,48,76),(1,58,2871,1,3457,131,96,245,48,77),(1,59,2961,1,3512,134,98,250,49,78),(1,60,3052,1,3814,136,100,256,50,80),(1,61,3144,1,4113,139,102,161,51,81),(1,62,3237,1,4410,141,105,266,52,83),(1,63,3331,1,4708,144,107,271,53,85),(1,64,3425,1,5006,146,110,276,54,87),(1,65,3524,1,5303,149,113,281,55,89),(1,66,3624,1,5601,151,116,287,56,91),(1,67,3728,1,5900,154,119,292,57,93),(1,68,3834,1,6197,156,122,297,58,95),(1,69,3941,1,6495,159,125,302,59,97),(1,70,4051,1,6794,162,128,307,60,99),(1,71,4162,1,7093,165,131,311,61,97),(1,72,4273,1,7392,168,134,316,62,99),(1,73,4384,1,7691,171,137,321,63,101),(1,74,4495,1,7990,174,140,326,64,103),(1,75,4606,1,8289,177,143,331,65,105),(1,76,4717,1,8588,180,146,336,66,107),(1,77,4828,1,8887,183,149,341,67,109),(1,78,4939,1,9186,186,152,346,68,111),(1,79,5050,1,9485,189,155,351,69,113),(1,80,5161,1,9784,192,158,356,70,115),(1,85,7140,554,16907,258,222,435,127,175),(1,84,6705,432,15340,244,209,419,115,163),(1,83,6290,316,13848,231,196,403,103,151),(1,82,5895,206,12427,218,183,387,92,139),(1,81,5519,101,11073,205,170,371,81,127);
/*!40000 ALTER TABLE `pet_levelstats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pet_name_generation`
--

DROP TABLE IF EXISTS `pet_name_generation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pet_name_generation` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `word` tinytext NOT NULL,
  `entry` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `half` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=314 DEFAULT CHARSET=utf8 COMMENT='CTDB Automatic pet name generation';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pet_name_generation`
--

LOCK TABLES `pet_name_generation` WRITE;
/*!40000 ALTER TABLE `pet_name_generation` DISABLE KEYS */;
INSERT INTO `pet_name_generation` VALUES (1,'Aba',416,0),(2,'Az',416,0),(3,'Bel',416,0),(4,'Biz',416,0),(5,'Cho',416,0),(6,'Dag',416,0),(7,'Gak',416,0),(8,'Gar',416,0),(9,'Gel',416,0),(10,'Gho',416,0),(11,'Gob',416,0),(12,'Gra',416,0),(13,'Jak',416,0),(14,'Jub',416,0),(15,'Kar',416,0),(16,'Kup',416,0),(17,'Laz',416,0),(18,'Nal',416,0),(19,'Nok',416,0),(20,'Pag',416,0),(21,'Pig',416,0),(22,'Pip',416,0),(23,'Piz',416,0),(24,'Quz',416,0),(25,'Rui',416,0),(26,'Rul',416,0),(27,'Rup',416,0),(28,'Tar',416,0),(29,'Vol',416,0),(30,'Yaz',416,0),(31,'Zep',416,0),(32,'Zig',416,0),(33,'Zil',416,0),(34,'Zor',416,0),(35,'bis',416,1),(36,'fip',416,1),(37,'gup',416,1),(38,'ham',416,1),(39,'jub',416,1),(40,'kin',416,1),(41,'kol',416,1),(42,'lop',416,1),(43,'loz',416,1),(44,'mat',416,1),(45,'mir',416,1),(46,'nam',416,1),(47,'nar',416,1),(48,'nik',416,1),(49,'nip',416,1),(50,'pad',416,1),(51,'pep',416,1),(52,'pit',416,1),(53,'qua',416,1),(54,'rai',416,1),(55,'rin',416,1),(56,'rot',416,1),(57,'tai',416,1),(58,'tal',416,1),(59,'tik',416,1),(60,'tip',416,1),(61,'tog',416,1),(62,'tuk',416,1),(63,'uri',416,1),(64,'yal',416,1),(65,'yap',416,1),(66,'Bhee',417,0),(67,'Bruu',417,0),(68,'Czaa',417,0),(69,'Droo',417,0),(70,'Flaa',417,0),(71,'Fzuu',417,0),(72,'Ghaa',417,0),(73,'Gree',417,0),(74,'Gzaa',417,0),(75,'Haa',417,0),(76,'Haad',417,0),(77,'Haag',417,0),(78,'Haap',417,0),(79,'Jhaa',417,0),(80,'Jhuu',417,0),(81,'Khaa',417,0),(82,'Khii',417,0),(83,'Khuu',417,0),(84,'Kree',417,0),(85,'Luu',417,0),(86,'Maa',417,0),(87,'Nhee',417,0),(88,'Phuu',417,0),(89,'Pryy',417,0),(90,'Rhuu',417,0),(91,'Shaa',417,0),(92,'Sloo',417,0),(93,'Sruu',417,0),(94,'Thoo',417,0),(95,'Traa',417,0),(96,'Wraa',417,0),(97,'Zhaa',417,0),(98,'dhon',417,1),(99,'dhum',417,1),(100,'dhun',417,1),(101,'dom',417,1),(102,'don',417,1),(103,'drom',417,1),(104,'dym',417,1),(105,'fenn',417,1),(106,'fum',417,1),(107,'fun',417,1),(108,'ghon',417,1),(109,'ghun',417,1),(110,'grom',417,1),(111,'grym',417,1),(112,'hom',417,1),(113,'hon',417,1),(114,'hun',417,1),(115,'jhom',417,1),(116,'kun',417,1),(117,'lum',417,1),(118,'mmon',417,1),(119,'mon',417,1),(120,'myn',417,1),(121,'nam',417,1),(122,'nem',417,1),(123,'nhym',417,1),(124,'nom',417,1),(125,'num',417,1),(126,'phom',417,1),(127,'roon',417,1),(128,'rym',417,1),(129,'shon',417,1),(130,'thun',417,1),(131,'tom',417,1),(132,'zhem',417,1),(133,'zhum',417,1),(134,'zun',417,1),(135,'Bar',1860,0),(136,'Bel',1860,0),(137,'Char',1860,0),(138,'Grak\'',1860,0),(139,'Graz\'',1860,0),(140,'Grim',1860,0),(141,'Hath',1860,0),(142,'Hel',1860,0),(143,'Hok',1860,0),(144,'Huk',1860,0),(145,'Jhaz',1860,0),(146,'Jhom',1860,0),(147,'Juk\'',1860,0),(148,'Kal\'',1860,0),(149,'Klath',1860,0),(150,'Kon',1860,0),(151,'Krag',1860,0),(152,'Krak',1860,0),(153,'Mak',1860,0),(154,'Mezz',1860,0),(155,'Orm',1860,0),(156,'Phan',1860,0),(157,'Sar',1860,0),(158,'Tang',1860,0),(159,'Than',1860,0),(160,'Thog',1860,0),(161,'Thok',1860,0),(162,'Thul',1860,0),(163,'Zag\'',1860,0),(164,'Zang',1860,0),(165,'Zhar\'',1860,0),(166,'kath',1860,1),(167,'doc',1860,1),(168,'dok',1860,1),(169,'gak',1860,1),(170,'garth',1860,1),(171,'gore',1860,1),(172,'gorg',1860,1),(173,'grave',1860,1),(174,'gron',1860,1),(175,'juk',1860,1),(176,'krast',1860,1),(177,'kresh',1860,1),(178,'krit',1860,1),(179,'los',1860,1),(180,'mon',1860,1),(181,'mos',1860,1),(182,'moth',1860,1),(183,'nagma',1860,1),(184,'nak',1860,1),(185,'nar',1860,1),(186,'nos',1860,1),(187,'nuz',1860,1),(188,'phog',1860,1),(189,'rath',1860,1),(190,'tast',1860,1),(191,'taz',1860,1),(192,'thak',1860,1),(193,'thang',1860,1),(194,'thyk',1860,1),(195,'vhug',1860,1),(196,'zazt',1860,1),(197,'Ael',1863,0),(198,'Aez',1863,0),(199,'Ang',1863,0),(200,'Ban',1863,0),(201,'Bet',1863,0),(202,'Bro',1863,0),(203,'Bry',1863,0),(204,'Cat',1863,0),(205,'Dir',1863,0),(206,'Dis',1863,0),(207,'Dom',1863,0),(208,'Drus',1863,0),(209,'Fie',1863,0),(210,'Fier',1863,0),(211,'Gly',1863,0),(212,'Hel',1863,0),(213,'Hes',1863,0),(214,'Kal',1863,0),(215,'Lyn',1863,0),(216,'Mir',1863,0),(217,'Nim',1863,0),(218,'Sar',1863,0),(219,'Sel',1863,0),(220,'Vil',1863,0),(221,'Zah',1863,0),(222,'aith',1863,1),(223,'anda',1863,1),(224,'antia',1863,1),(225,'evere',1863,1),(226,'lia',1863,1),(227,'lissa',1863,1),(228,'neri',1863,1),(229,'neth',1863,1),(230,'nia',1863,1),(231,'nlissa',1863,1),(232,'nora',1863,1),(233,'nva',1863,1),(234,'nys',1863,1),(235,'ola',1863,1),(236,'ona',1863,1),(237,'ora',1863,1),(238,'rah',1863,1),(239,'riana',1863,1),(240,'riel',1863,1),(241,'rona',1863,1),(242,'tai',1863,1),(243,'tevere',1863,1),(244,'thea',1863,1),(245,'vina',1863,1),(246,'wena',1863,1),(247,'wyn',1863,1),(248,'xia',1863,1),(249,'yla',1863,1),(250,'yssa',1863,1),(251,'Flaa',17252,0),(252,'Haa',17252,0),(253,'Jhuu',17252,0),(254,'Shaa',17252,0),(255,'Thoo',17252,0),(256,'dhun',17252,1),(257,'ghun',17252,1),(258,'roon',17252,1),(259,'thun',17252,1),(260,'tom',17252,1),(261,'Stone',26125,0),(262,'Stone',26125,0),(263,'Eye',26125,0),(264,'Dirt',26125,0),(265,'Blight',26125,0),(266,'Bat',26125,0),(267,'Rat',26125,0),(268,'Corpse',26125,0),(269,'Grave',26125,0),(270,'Carrion',26125,0),(271,'Skull',26125,0),(272,'Bone',26125,0),(273,'Crypt',26125,0),(274,'Rib',26125,0),(275,'Brain',26125,0),(276,'Tomb',26125,0),(277,'Rot',26125,0),(278,'Gravel',26125,0),(279,'Plague',26125,0),(280,'Casket',26125,0),(281,'Limb',26125,0),(282,'Worm',26125,0),(283,'Earth',26125,0),(284,'Spine',26125,0),(285,'Pebble',26125,0),(286,'Root',26125,0),(287,'Marrow',26125,0),(288,'Hammer',26125,0),(289,'ravager',26125,1),(290,'muncher',26125,1),(291,'cruncher',26125,1),(292,'masher',26125,1),(293,'leaper',26125,1),(294,'grinder',26125,1),(295,'stalker',26125,1),(296,'gobbler',26125,1),(297,'feeder',26125,1),(298,'basher',26125,1),(299,'chewer',26125,1),(300,'ripper',26125,1),(301,'slicer',26125,1),(302,'gnaw',26125,1),(303,'flayer',26125,1),(304,'rumbler',26125,1),(305,'chomp',26125,1),(306,'breaker',26125,1),(307,'keeper',26125,1),(308,'rawler',26125,1),(309,'stealer',26125,1),(310,'thief',26125,1),(311,'catcher',26125,1),(312,'drinker',26125,1),(313,'slicer',26125,1);
/*!40000 ALTER TABLE `pet_name_generation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pickpocketing_loot_template`
--

DROP TABLE IF EXISTS `pickpocketing_loot_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pickpocketing_loot_template` (
  `entry` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `item` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `ChanceOrQuestChance` float NOT NULL DEFAULT '100',
  `lootmode` smallint(5) unsigned NOT NULL DEFAULT '1',
  `groupid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `mincountOrRef` mediumint(9) NOT NULL DEFAULT '1',
  `maxcount` tinyint(3) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`entry`,`item`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='Loot System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pickpocketing_loot_template`
--

LOCK TABLES `pickpocketing_loot_template` WRITE;
/*!40000 ALTER TABLE `pickpocketing_loot_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `pickpocketing_loot_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `player_classlevelstats`
--

DROP TABLE IF EXISTS `player_classlevelstats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `player_classlevelstats` (
  `class` tinyint(3) unsigned NOT NULL,
  `level` tinyint(3) unsigned NOT NULL,
  `basehp` smallint(5) unsigned NOT NULL,
  `basemana` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`class`,`level`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0 COMMENT='Stores levels stats.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `player_classlevelstats`
--

LOCK TABLES `player_classlevelstats` WRITE;
/*!40000 ALTER TABLE `player_classlevelstats` DISABLE KEYS */;
INSERT INTO `player_classlevelstats` VALUES (1,1,20,0),(1,2,29,0),(1,3,38,0),(1,4,47,0),(1,5,56,0),(1,6,65,0),(1,7,74,0),(1,8,83,0),(1,9,92,0),(1,10,101,0),(1,11,101,0),(1,12,109,0),(1,13,118,0),(1,14,128,0),(1,15,139,0),(1,16,151,0),(1,17,154,0),(1,18,168,0),(1,19,183,0),(1,20,199,0),(1,21,206,0),(1,22,224,0),(1,23,243,0),(1,24,253,0),(1,25,274,0),(1,26,296,0),(1,27,309,0),(1,28,333,0),(1,29,348,0),(1,30,374,0),(1,31,401,0),(1,32,419,0),(1,33,448,0),(1,34,468,0),(1,35,499,0),(1,36,521,0),(1,37,545,0),(1,38,581,0),(1,39,609,0),(1,40,649,0),(1,41,681,0),(1,42,715,0),(1,43,761,0),(1,44,799,0),(1,45,839,0),(1,46,881,0),(1,47,935,0),(1,48,981,0),(1,49,1029,0),(1,50,1079,0),(1,51,1131,0),(1,52,1185,0),(1,53,1241,0),(1,54,1299,0),(1,55,1359,0),(1,56,1421,0),(1,57,1485,0),(1,58,1551,0),(1,59,1619,0),(1,60,1689,0),(1,61,1902,0),(1,62,2129,0),(1,63,2357,0),(1,64,2612,0),(1,65,2883,0),(1,66,3169,0),(1,67,3455,0),(1,68,3774,0),(1,69,4109,0),(1,70,4444,0),(2,1,28,60),(2,2,36,78),(2,3,44,98),(2,4,52,104),(2,5,60,111),(2,6,68,134),(2,7,76,143),(2,8,84,153),(2,9,92,179),(2,10,101,192),(2,11,101,205),(2,12,109,219),(2,13,118,249),(2,14,128,265),(2,15,139,282),(2,16,151,315),(2,17,154,334),(2,18,168,354),(2,19,183,390),(2,20,199,412),(2,21,206,435),(2,22,224,459),(2,23,243,499),(2,24,253,525),(2,25,274,552),(2,26,296,579),(2,27,309,621),(2,28,333,648),(2,29,348,675),(2,30,374,702),(2,31,401,729),(2,32,419,756),(2,33,448,798),(2,34,468,825),(2,35,499,852),(2,36,521,879),(2,37,545,906),(2,38,581,933),(2,39,609,960),(2,40,649,987),(2,41,681,1014),(2,42,715,1041),(2,43,761,1068),(2,44,799,1110),(2,45,839,1137),(2,46,881,1164),(2,47,935,1176),(2,48,981,1203),(2,49,1029,1230),(2,50,1079,1257),(2,51,1131,1284),(2,52,1185,1311),(2,53,1241,1338),(2,54,1299,1365),(2,55,1359,1392),(2,56,1421,1419),(2,57,1485,1446),(2,58,1551,1458),(2,59,1619,1485),(2,60,1689,1512),(2,61,1902,1656),(2,62,2129,1800),(2,63,2357,1944),(2,64,2612,2088),(2,65,2883,2232),(2,66,3169,2377),(2,67,3455,2521),(2,68,3774,2665),(2,69,4109,2809),(2,70,4444,2953),(3,1,46,65),(3,2,53,70),(3,3,60,76),(3,4,67,98),(3,5,74,106),(3,6,81,130),(3,7,88,140),(3,8,95,166),(3,9,102,193),(3,10,109,0),(3,11,116,0),(3,12,123,0),(3,13,130,0),(3,14,138,0),(3,15,147,0),(3,16,157,0),(3,17,168,0),(3,18,180,0),(3,19,193,0),(3,20,207,0),(3,21,222,0),(3,22,238,0),(3,23,255,0),(3,24,273,0),(3,25,292,0),(3,26,312,0),(3,27,333,0),(3,28,355,0),(3,29,378,0),(3,30,402,0),(3,31,417,0),(3,32,443,0),(3,33,470,0),(3,34,498,0),(3,35,527,0),(3,36,547,0),(3,37,578,0),(3,38,610,0),(3,39,643,0),(3,40,667,0),(3,41,702,0),(3,42,738,0),(3,43,775,0),(3,44,803,0),(3,45,842,0),(3,46,872,0),(3,47,913,0),(3,48,955,0),(3,49,988,0),(3,50,1032,0),(3,51,1067,0),(3,52,1113,0),(3,53,1150,0),(3,54,1198,0),(3,55,1237,0),(3,56,1287,0),(3,57,1328,0),(3,58,1370,0),(3,59,1423,0),(3,60,1467,0),(3,61,1633,0),(3,62,1819,0),(3,63,2003,0),(3,64,2195,0),(3,65,2397,0),(3,66,2623,0),(3,67,2844,0),(3,68,3075,0),(3,69,3316,0),(3,70,3568,0),(4,1,25,0),(4,2,32,0),(4,3,49,0),(4,4,56,0),(4,5,63,0),(4,6,70,0),(4,7,87,0),(4,8,94,0),(4,9,101,0),(4,10,118,0),(4,11,125,0),(4,12,142,0),(4,13,149,0),(4,14,156,0),(4,15,173,0),(4,16,181,0),(4,17,190,0),(4,18,200,0),(4,19,221,0),(4,20,233,0),(4,21,246,0),(4,22,260,0),(4,23,275,0),(4,24,301,0),(4,25,318,0),(4,26,336,0),(4,27,355,0),(4,28,375,0),(4,29,396,0),(4,30,428,0),(4,31,451,0),(4,32,475,0),(4,33,500,0),(4,34,526,0),(4,35,553,0),(4,36,581,0),(4,37,610,0),(4,38,640,0),(4,39,671,0),(4,40,703,0),(4,41,736,0),(4,42,770,0),(4,43,805,0),(4,44,841,0),(4,45,878,0),(4,46,916,0),(4,47,955,0),(4,48,995,0),(4,49,1026,0),(4,50,1068,0),(4,51,1111,0),(4,52,1155,0),(4,53,1200,0),(4,54,1246,0),(4,55,1283,0),(4,56,1331,0),(4,57,1380,0),(4,58,1430,0),(4,59,1471,0),(4,60,1523,0),(4,61,1702,0),(4,62,1879,0),(4,63,2077,0),(4,64,2285,0),(4,65,2489,0),(4,66,2717,0),(4,67,2941,0),(4,68,3190,0),(4,69,3450,0),(4,70,3704,0),(5,1,52,73),(5,2,57,76),(5,3,72,95),(5,4,77,114),(5,5,92,133),(5,6,97,152),(5,7,112,171),(5,8,117,190),(5,9,132,209),(5,10,137,212),(5,11,142,215),(5,12,157,234),(5,13,172,254),(5,14,177,260),(5,15,192,282),(5,16,197,305),(5,17,212,329),(5,18,227,339),(5,19,232,365),(5,20,247,377),(5,21,252,405),(5,22,268,434),(5,23,275,449),(5,24,293,480),(5,25,302,497),(5,26,322,530),(5,27,343,549),(5,28,355,584),(5,29,378,605),(5,30,392,627),(5,31,417,665),(5,32,433,689),(5,33,460,728),(5,34,478,752),(5,35,507,776),(5,36,527,800),(5,37,548,839),(5,38,580,863),(5,39,603,887),(5,40,637,911),(5,41,662,950),(5,42,698,974),(5,43,725,998),(5,44,763,1022),(5,45,792,1046),(5,46,822,1070),(5,47,863,1094),(5,48,895,1118),(5,49,928,1142),(5,50,972,1166),(5,51,1007,1190),(5,52,1053,1214),(5,53,1090,1238),(5,54,1128,1262),(5,55,1177,1271),(5,56,1217,1295),(5,57,1258,1319),(5,58,1300,1343),(5,59,1353,1352),(5,60,1397,1376),(5,61,1557,1500),(5,62,1738,1625),(5,63,1916,1749),(5,64,2101,1873),(5,65,2295,1998),(5,66,2495,2122),(5,67,2719,2247),(5,68,2936,2371),(5,69,3160,2495),(5,70,3391,2620),(7,80,6960,4396),(7,79,6477,4252),(7,78,6027,4108),(7,77,5609,3965),(7,76,5219,3821),(7,75,4858,3677),(7,74,4521,3533),(7,73,4207,3389),(7,72,3915,3246),(7,71,3644,3102),(7,70,3390,2958),(7,69,3146,2814),(7,68,2912,2670),(7,67,2687,2527),(7,66,2473,2383),(7,65,2269,2239),(7,64,2074,2095),(7,63,1889,1951),(7,62,1699,1808),(7,61,1533,1664),(7,60,1374,1520),(7,59,1330,1501),(7,58,1287,1467),(7,57,1234,1448),(7,56,1193,1414),(7,55,1154,1395),(7,54,1104,1376),(7,53,1067,1342),(7,52,1030,1323),(7,51,984,1289),(7,50,950,1255),(7,49,906,1236),(7,48,874,1202),(7,47,832,1183),(7,46,802,1149),(7,45,762,1115),(7,44,734,1096),(7,43,697,1062),(7,42,671,1028),(7,41,635,1009),(7,40,612,975),(7,39,578,941),(7,38,546,922),(7,37,526,888),(7,36,496,854),(7,35,467,820),(7,34,450,786),(7,33,423,767),(7,32,397,733),(7,31,372,699),(7,30,360,665),(7,29,337,631),(7,28,316,598),(7,27,295,566),(7,26,276,535),(7,25,258,505),(7,24,251,476),(7,23,235,448),(7,22,220,421),(7,21,206,395),(7,20,193,370),(7,19,181,346),(7,18,171,323),(7,17,161,301),(7,16,152,280),(7,15,145,260),(7,14,137,241),(7,13,130,223),(7,12,122,206),(7,11,115,190),(8,1,32,100),(8,2,47,110),(8,3,52,106),(8,4,67,118),(8,5,82,131),(8,6,97,130),(8,7,102,145),(8,8,117,146),(8,9,132,163),(8,10,137,196),(8,11,152,215),(8,12,167,220),(8,13,172,241),(8,14,187,263),(8,15,202,271),(8,16,207,295),(8,17,222,305),(8,18,237,331),(8,19,242,343),(8,20,257,371),(8,21,272,385),(8,22,277,415),(8,23,292,431),(8,24,298,463),(8,25,315,481),(8,26,333,515),(8,27,342,535),(8,28,362,556),(8,29,373,592),(8,30,395,613),(8,31,418,634),(8,32,432,670),(8,33,457,691),(8,34,473,712),(8,35,500,733),(8,36,518,754),(8,37,547,790),(8,38,577,811),(8,39,598,832),(8,40,630,853),(8,41,653,874),(8,42,687,895),(8,43,712,916),(8,44,748,937),(8,45,775,958),(8,46,813,979),(8,47,842,1000),(8,48,882,1021),(8,49,913,1042),(8,50,955,1048),(8,51,988,1069),(8,52,1032,1090),(8,53,1067,1111),(8,54,1103,1117),(8,55,1150,1138),(8,56,1188,1159),(8,57,1237,1165),(8,58,1277,1186),(8,59,1328,1192),(8,60,1370,1213),(8,61,1526,1316),(8,62,1702,1419),(8,63,1875,1521),(8,64,2070,1624),(8,65,2261,1727),(8,66,2461,1830),(8,67,2686,1932),(8,68,2906,2035),(8,69,3136,2138),(8,70,3393,2241),(9,1,23,90),(9,2,28,98),(9,3,43,107),(9,4,48,102),(9,5,63,113),(9,6,68,126),(9,7,83,144),(9,8,88,162),(9,9,93,180),(9,10,108,198),(9,11,123,200),(9,12,128,218),(9,13,143,237),(9,14,148,257),(9,15,153,278),(9,16,168,300),(9,17,173,308),(9,18,189,332),(9,19,196,357),(9,20,204,383),(9,21,223,395),(9,22,233,423),(9,23,244,452),(9,24,266,467),(9,25,279,498),(9,26,293,530),(9,27,318,548),(9,28,334,582),(9,29,351,602),(9,30,379,638),(9,31,398,674),(9,32,418,695),(9,33,439,731),(9,34,471,752),(9,35,494,788),(9,36,518,809),(9,37,543,830),(9,38,569,866),(9,39,606,887),(9,40,634,923),(9,41,663,944),(9,42,693,965),(9,43,724,1001),(9,44,756,1022),(9,45,799,1043),(9,46,833,1064),(9,47,868,1100),(9,48,904,1121),(9,49,941,1142),(9,50,979,1163),(9,51,1018,1184),(9,52,1058,1205),(9,53,1099,1226),(9,54,1141,1247),(9,55,1184,1268),(9,56,1228,1289),(9,57,1273,1310),(9,58,1319,1331),(9,59,1366,1352),(9,60,1414,1373),(9,61,1580,1497),(9,62,1755,1621),(9,63,1939,1745),(9,64,2133,1870),(9,65,2323,1994),(9,66,2535,2118),(9,67,2758,2242),(9,68,2991,2366),(9,69,3235,2490),(9,70,3490,2615),(11,1,44,60),(11,2,51,66),(11,3,58,73),(11,4,75,81),(11,5,82,90),(11,6,89,100),(11,7,106,111),(11,8,113,123),(11,9,120,136),(11,10,137,149),(11,11,144,165),(11,12,151,182),(11,13,168,200),(11,14,175,219),(11,15,182,239),(11,16,199,260),(11,17,206,282),(11,18,214,305),(11,19,233,329),(11,20,243,354),(11,21,254,380),(11,22,266,392),(11,23,289,420),(11,24,303,449),(11,25,318,479),(11,26,334,509),(11,27,361,524),(11,28,379,554),(11,29,398,584),(11,30,418,614),(11,31,439,629),(11,32,461,659),(11,33,494,689),(11,34,518,704),(11,35,543,734),(11,36,569,749),(11,37,596,779),(11,38,624,809),(11,39,653,824),(11,40,683,854),(11,41,714,869),(11,42,746,899),(11,43,779,914),(11,44,823,944),(11,45,858,959),(11,46,894,989),(11,47,921,1004),(11,48,959,1019),(11,49,998,1049),(11,50,1038,1064),(11,51,1079,1079),(11,52,1121,1109),(11,53,1164,1124),(11,54,1208,1139),(11,55,1253,1154),(11,56,1299,1169),(11,57,1346,1199),(11,58,1384,1214),(11,59,1433,1229),(11,60,1483,1244),(11,61,1657,1357),(11,62,1840,1469),(11,63,2020,1582),(11,64,2222,1694),(11,65,2433,1807),(11,66,2640,1919),(11,67,2872,2032),(11,68,3114,2145),(11,69,3351,2257),(11,70,3614,2370),(1,71,4720,0),(1,72,5013,0),(1,73,5325,0),(1,74,5656,0),(1,75,6008,0),(1,76,6381,0),(1,77,6778,0),(1,78,7199,0),(1,79,7646,0),(1,80,8121,0),(2,71,4720,3097),(2,72,5013,3241),(2,73,5325,3385),(2,74,5656,3529),(2,75,6008,3673),(2,76,6381,3817),(2,77,6778,3962),(2,78,7199,4106),(2,79,7646,4250),(2,80,8121,4394),(3,71,3834,0),(3,72,4120,0),(3,73,4427,0),(3,74,4757,0),(3,75,5112,0),(3,76,5493,0),(3,77,5903,0),(3,78,6343,0),(3,79,6816,0),(3,80,7324,0),(4,71,3980,0),(4,72,4277,0),(4,73,4596,0),(4,74,4939,0),(4,75,5307,0),(4,76,5703,0),(4,77,6128,0),(4,78,6585,0),(4,79,7076,0),(4,80,7604,0),(5,71,3644,2744),(5,72,3916,2868),(5,73,4208,2993),(5,74,4522,3117),(5,75,4859,3242),(5,76,5221,3366),(5,77,5610,3490),(5,78,6028,3615),(5,79,6477,3739),(5,80,6960,3863),(6,1,22,0),(6,2,27,0),(6,3,32,0),(6,4,37,0),(6,5,42,0),(6,6,47,0),(6,7,52,0),(6,8,58,0),(6,9,64,0),(6,10,70,0),(6,11,77,0),(6,12,84,0),(6,13,92,0),(6,14,100,0),(6,15,117,0),(6,16,127,0),(6,17,138,0),(6,18,150,0),(6,19,163,0),(6,20,177,0),(6,21,192,0),(6,22,208,0),(6,23,225,0),(6,24,239,0),(6,25,258,0),(6,26,278,0),(6,27,299,0),(6,28,321,0),(6,29,344,0),(6,30,368,0),(6,31,393,0),(6,32,419,0),(6,33,446,0),(6,34,474,0),(6,35,503,0),(6,36,533,0),(6,37,564,0),(6,38,596,0),(6,39,629,0),(6,40,698,0),(6,41,698,0),(6,42,734,0),(6,43,771,0),(6,44,809,0),(6,45,849,0),(6,46,891,0),(6,47,935,0),(6,48,981,0),(6,49,1029,0),(6,50,1079,0),(6,51,1131,0),(6,52,1185,0),(6,53,1241,0),(6,54,1299,0),(6,55,1359,0),(6,56,1421,0),(6,57,1485,0),(6,58,1551,0),(6,59,1619,0),(6,60,1689,0),(6,61,1902,0),(6,62,2129,0),(6,63,2357,0),(6,64,2612,0),(6,65,2883,0),(6,66,3169,0),(6,67,3455,0),(6,68,3774,0),(6,69,4109,0),(6,70,4444,0),(6,71,4720,0),(6,72,5013,0),(6,73,5325,0),(6,74,5656,0),(6,75,6008,0),(6,76,6381,0),(6,77,6778,0),(6,78,7199,0),(6,79,7646,0),(6,80,8121,0),(7,10,107,175),(7,9,100,161),(7,8,92,148),(7,7,85,136),(7,6,77,125),(7,5,70,115),(7,4,62,106),(7,3,55,98),(7,2,47,91),(7,1,40,85),(8,71,3646,2343),(8,72,3918,2446),(8,73,4210,2549),(8,74,4524,2652),(8,75,4861,2754),(8,76,5223,2857),(8,77,5612,2960),(8,78,6030,3063),(8,79,6480,3165),(8,80,6963,3268),(9,71,3750,2739),(9,72,4030,2863),(9,73,4330,2987),(9,74,4653,3111),(9,75,5000,3235),(9,76,5373,3360),(9,77,5774,3484),(9,78,6204,3608),(9,79,6667,3732),(9,80,7164,3856),(11,71,3883,2482),(11,72,4172,2595),(11,73,4483,2708),(11,74,4817,2820),(11,75,5176,2933),(11,76,5562,3045),(11,77,5977,3158),(11,78,6423,3270),(11,79,6902,3383),(11,80,7417,3496),(11,85,39533,21751),(11,84,38865,13335),(11,83,26487,9542),(11,82,18477,6828),(11,81,12301,4886),(9,85,38184,20553),(9,84,39580,14707),(9,83,26948,10524),(9,82,18408,7531),(9,81,12080,5389),(8,85,37113,17418),(8,84,37638,12464),(8,83,26172,8919),(8,82,17508,6382),(8,81,11659,4567),(6,85,43285,0),(6,84,42087,0),(6,83,29493,0),(6,82,20013,0),(6,81,13441,0),(5,85,43285,20590),(5,84,40612,14734),(5,83,28079,10543),(5,82,18588,7544),(5,81,11883,5398),(4,85,40529,0),(4,84,39984,0),(4,83,28085,0),(4,82,18731,0),(4,81,12482,0),(3,85,39037,0),(3,84,39079,0),(3,83,27383,0),(3,82,18274,0),(3,81,12023,0),(2,85,43285,23422),(2,84,42350,16760),(2,83,29668,11993),(2,82,19988,8582),(2,81,13345,6141),(1,85,43285,0),(1,84,43246,0),(1,83,30192,0),(1,82,20452,0),(1,81,13277,0),(7,85,37097,23430),(7,84,37192,16766),(7,83,25662,11997),(7,82,17655,8585),(7,81,11595,6143);
/*!40000 ALTER TABLE `player_classlevelstats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `player_factionchange_achievement`
--

DROP TABLE IF EXISTS `player_factionchange_achievement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `player_factionchange_achievement` (
  `alliance_id` int(8) NOT NULL,
  `horde_id` int(8) NOT NULL,
  PRIMARY KEY (`alliance_id`,`horde_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Achievements when change faction';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `player_factionchange_achievement`
--

LOCK TABLES `player_factionchange_achievement` WRITE;
/*!40000 ALTER TABLE `player_factionchange_achievement` DISABLE KEYS */;
/*!40000 ALTER TABLE `player_factionchange_achievement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `player_factionchange_items`
--

DROP TABLE IF EXISTS `player_factionchange_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `player_factionchange_items` (
  `race_A` int(8) NOT NULL,
  `alliance_id` int(8) NOT NULL,
  `commentA` text,
  `race_H` int(8) NOT NULL,
  `horde_id` int(8) NOT NULL,
  `commentH` text,
  PRIMARY KEY (`alliance_id`,`horde_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `player_factionchange_items`
--

LOCK TABLES `player_factionchange_items` WRITE;
/*!40000 ALTER TABLE `player_factionchange_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `player_factionchange_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `player_factionchange_reputations`
--

DROP TABLE IF EXISTS `player_factionchange_reputations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `player_factionchange_reputations` (
  `alliance_id` int(8) NOT NULL,
  `horde_id` int(8) NOT NULL,
  PRIMARY KEY (`alliance_id`,`horde_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `player_factionchange_reputations`
--

LOCK TABLES `player_factionchange_reputations` WRITE;
/*!40000 ALTER TABLE `player_factionchange_reputations` DISABLE KEYS */;
/*!40000 ALTER TABLE `player_factionchange_reputations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `player_factionchange_spells`
--

DROP TABLE IF EXISTS `player_factionchange_spells`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `player_factionchange_spells` (
  `alliance_id` int(8) NOT NULL,
  `horde_id` int(8) NOT NULL,
  PRIMARY KEY (`alliance_id`,`horde_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `player_factionchange_spells`
--

LOCK TABLES `player_factionchange_spells` WRITE;
/*!40000 ALTER TABLE `player_factionchange_spells` DISABLE KEYS */;
/*!40000 ALTER TABLE `player_factionchange_spells` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `player_levelstats`
--

DROP TABLE IF EXISTS `player_levelstats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `player_levelstats` (
  `race` tinyint(3) unsigned NOT NULL,
  `class` tinyint(3) unsigned NOT NULL,
  `level` tinyint(3) unsigned NOT NULL,
  `str` tinyint(3) unsigned NOT NULL,
  `agi` tinyint(3) unsigned NOT NULL,
  `sta` tinyint(3) unsigned NOT NULL,
  `inte` tinyint(3) unsigned NOT NULL,
  `spi` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`race`,`class`,`level`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0 COMMENT='Stores levels stats.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `player_levelstats`
--

LOCK TABLES `player_levelstats` WRITE;
/*!40000 ALTER TABLE `player_levelstats` DISABLE KEYS */;
INSERT INTO `player_levelstats` VALUES (1,1,1,23,20,22,20,20),(1,1,2,24,21,23,20,20),(1,1,3,26,22,24,20,21),(1,1,4,27,22,26,20,21),(1,1,5,28,23,27,20,21),(1,1,6,30,24,28,20,21),(1,1,7,31,25,29,21,22),(1,1,8,32,26,30,21,22),(1,1,9,34,26,32,21,22),(1,1,10,33,26,31,21,23),(1,1,11,35,27,33,21,23),(1,1,12,36,28,34,21,23),(1,1,13,37,29,35,21,24),(1,1,14,39,30,36,22,24),(1,1,15,40,30,37,22,24),(1,1,16,41,31,38,22,25),(1,1,17,42,32,40,22,25),(1,1,18,44,33,41,22,25),(1,1,19,45,34,42,22,26),(1,1,20,47,35,43,22,26),(1,1,21,48,35,45,23,26),(1,1,22,49,36,46,23,27),(1,1,23,51,37,47,23,27),(1,1,24,52,38,49,23,28),(1,1,25,54,39,50,23,28),(1,1,26,55,40,51,23,28),(1,1,27,57,41,53,23,29),(1,1,28,58,42,54,24,29),(1,1,29,60,43,56,24,30),(1,1,30,62,44,57,24,30),(1,1,31,63,45,58,24,30),(1,1,32,65,46,60,24,31),(1,1,33,66,47,61,24,31),(1,1,34,68,48,63,25,32),(1,1,35,70,49,64,25,32),(1,1,36,72,50,66,25,33),(1,1,37,73,51,68,25,33),(1,1,38,75,52,69,25,33),(1,1,39,77,53,71,26,34),(1,1,40,79,54,72,26,34),(1,1,41,80,56,74,26,35),(1,1,42,82,57,76,26,35),(1,1,43,84,58,77,26,36),(1,1,44,86,59,79,26,36),(1,1,45,88,60,81,27,37),(1,1,46,90,61,83,27,37),(1,1,47,92,63,84,27,38),(1,1,48,94,64,86,27,38),(1,1,49,96,65,88,28,39),(1,1,50,98,66,90,28,39),(1,1,51,100,68,92,28,40),(1,1,52,102,69,94,28,40),(1,1,53,104,70,96,28,41),(1,1,54,106,72,98,29,42),(1,1,55,109,73,100,29,42),(1,1,56,111,74,102,29,43),(1,1,57,113,76,104,29,43),(1,1,58,115,77,106,30,44),(1,1,59,118,79,108,30,44),(1,1,60,120,80,110,30,45),(1,1,61,122,81,112,30,46),(1,1,62,125,83,114,30,46),(1,1,63,127,84,117,31,47),(1,1,64,130,86,119,31,47),(1,1,65,132,88,121,31,48),(1,1,66,135,89,123,32,49),(1,1,67,137,91,126,32,49),(1,1,68,140,92,128,32,50),(1,1,69,142,94,130,32,51),(1,1,70,145,96,133,33,51),(1,1,71,148,97,135,33,52),(1,1,72,150,99,138,33,53),(1,1,73,153,101,140,33,54),(1,1,74,156,102,143,34,54),(1,1,75,159,104,145,34,55),(1,1,76,162,106,148,34,56),(1,1,77,165,108,151,35,57),(1,1,78,168,109,153,35,57),(1,1,79,171,111,156,35,58),(1,1,80,174,113,159,36,59),(1,2,1,22,20,22,20,21),(1,2,2,23,21,23,21,22),(1,2,3,24,21,24,21,22),(1,2,4,25,22,25,22,23),(1,2,5,26,22,26,23,24),(1,2,6,28,23,27,23,25),(1,2,7,29,24,28,24,25),(1,2,8,30,24,29,25,26),(1,2,9,31,25,30,25,27),(1,2,10,31,25,30,25,27),(1,2,11,32,25,31,26,28),(1,2,12,33,26,32,27,28),(1,2,13,34,27,33,27,29),(1,2,14,35,27,34,28,30),(1,2,15,36,28,36,29,30),(1,2,16,38,28,37,29,31),(1,2,17,39,29,38,30,32),(1,2,18,40,30,39,31,33),(1,2,19,41,30,40,31,33),(1,2,20,42,31,41,32,34),(1,2,21,43,32,42,33,35),(1,2,22,45,32,43,34,36),(1,2,23,46,33,44,34,37),(1,2,24,47,34,46,35,37),(1,2,25,48,34,47,36,38),(1,2,26,50,35,48,37,39),(1,2,27,51,36,49,37,40),(1,2,28,52,36,50,38,41),(1,2,29,54,37,52,39,42),(1,2,30,55,38,53,40,42),(1,2,31,56,39,54,41,43),(1,2,32,58,39,56,42,44),(1,2,33,59,40,57,42,45),(1,2,34,61,41,58,43,46),(1,2,35,62,42,60,44,47),(1,2,36,64,43,61,45,48),(1,2,37,65,43,62,46,49),(1,2,38,67,44,64,47,50),(1,2,39,68,45,65,48,51),(1,2,40,70,46,67,49,52),(1,2,41,71,47,68,50,53),(1,2,42,73,47,70,51,54),(1,2,43,74,48,71,52,55),(1,2,44,76,49,73,52,56),(1,2,45,78,50,74,53,57),(1,2,46,79,51,76,54,58),(1,2,47,81,52,77,56,59),(1,2,48,83,53,79,57,60),(1,2,49,84,54,81,58,62),(1,2,50,86,55,82,59,63),(1,2,51,88,56,84,60,64),(1,2,52,90,57,86,61,65),(1,2,53,92,58,87,62,66),(1,2,54,93,59,89,63,67),(1,2,55,95,60,91,64,69),(1,2,56,97,61,93,65,70),(1,2,57,99,62,94,66,71),(1,2,58,101,63,96,68,72),(1,2,59,108,64,98,69,74),(1,2,60,108,65,100,70,75),(1,2,61,108,66,102,71,76),(1,2,62,114,67,104,72,78),(1,2,63,116,68,106,74,79),(1,2,64,116,69,108,75,80),(1,2,65,116,71,110,76,82),(1,2,66,118,72,112,78,83),(1,2,67,120,73,114,79,85),(1,2,68,122,74,116,80,86),(1,2,69,130,75,118,82,87),(1,2,70,130,77,120,83,89),(1,2,71,130,78,122,84,90),(1,2,72,137,79,125,86,92),(1,2,73,139,80,127,87,94),(1,2,74,139,82,129,89,95),(1,2,75,144,83,131,90,97),(1,2,76,144,84,134,92,98),(1,2,77,150,86,136,93,100),(1,2,78,153,87,138,95,102),(1,2,79,155,88,141,96,103),(1,2,80,155,90,143,98,105),(1,4,1,21,23,21,20,20),(1,4,2,22,24,22,20,20),(1,4,3,23,26,22,20,21),(1,4,4,23,27,23,20,21),(1,4,5,24,29,24,21,21),(1,4,6,25,30,25,21,22),(1,4,7,26,32,25,21,22),(1,4,8,26,33,26,21,23),(1,4,9,27,35,27,21,23),(1,4,10,27,35,27,22,23),(1,4,11,28,36,28,22,24),(1,4,12,29,37,28,22,24),(1,4,13,30,39,29,22,24),(1,4,14,30,40,30,22,25),(1,4,15,31,42,30,23,25),(1,4,16,32,43,31,23,26),(1,4,17,33,44,32,23,26),(1,4,18,34,46,33,23,26),(1,4,19,35,48,33,23,27),(1,4,20,35,49,34,24,27),(1,4,21,36,51,35,24,28),(1,4,22,37,52,36,24,28),(1,4,23,38,54,37,24,29),(1,4,24,39,55,37,25,29),(1,4,25,40,57,38,25,30),(1,4,26,41,59,39,25,30),(1,4,27,42,60,40,25,30),(1,4,28,43,62,41,25,31),(1,4,29,43,64,42,26,31),(1,4,30,44,66,42,26,32),(1,4,31,45,67,43,26,32),(1,4,32,46,69,44,26,33),(1,4,33,47,71,45,27,33),(1,4,34,48,73,46,27,34),(1,4,35,49,75,47,27,34),(1,4,36,51,77,48,28,35),(1,4,37,52,78,49,28,36),(1,4,38,53,80,50,28,36),(1,4,39,54,82,51,28,37),(1,4,40,55,84,52,29,37),(1,4,41,56,86,53,29,38),(1,4,42,57,88,54,29,38),(1,4,43,58,90,55,29,39),(1,4,44,59,93,56,30,39),(1,4,45,61,95,57,30,40),(1,4,46,62,97,58,30,41),(1,4,47,63,99,59,31,41),(1,4,48,64,101,60,31,42),(1,4,49,65,103,62,31,43),(1,4,50,67,106,63,32,43),(1,4,51,68,113,64,32,44),(1,4,52,69,113,65,32,44),(1,4,53,70,118,66,33,45),(1,4,54,72,120,67,33,46),(1,4,55,73,122,69,33,46),(1,4,56,74,126,70,34,47),(1,4,57,76,128,71,34,48),(1,4,58,77,131,72,34,49),(1,4,59,79,133,74,35,49),(1,4,60,80,136,75,35,50),(1,4,61,81,139,76,35,51),(1,4,62,83,141,78,36,51),(1,4,63,84,141,79,36,52),(1,4,64,86,148,80,36,53),(1,4,65,87,150,82,37,54),(1,4,66,89,153,83,37,55),(1,4,67,90,156,85,38,55),(1,4,68,92,159,86,38,56),(1,4,69,94,162,87,38,57),(1,4,70,95,162,89,39,58),(1,4,71,97,169,90,39,59),(1,4,72,99,172,92,40,59),(1,4,73,100,175,94,40,60),(1,4,74,102,178,95,41,61),(1,4,75,104,181,97,41,62),(1,4,76,105,184,98,41,63),(1,4,77,107,188,100,42,64),(1,4,78,109,192,102,42,65),(1,4,79,111,195,103,43,66),(1,4,80,113,198,105,43,67),(1,5,1,20,20,20,22,23),(1,5,2,20,20,20,23,24),(1,5,3,20,20,21,25,26),(1,5,4,20,21,21,26,27),(1,5,5,21,21,21,27,28),(1,5,6,21,21,22,29,30),(1,5,7,21,21,22,30,31),(1,5,8,21,22,23,31,33),(1,5,9,21,22,23,33,34),(1,5,10,22,22,23,33,34),(1,5,11,22,22,24,34,35),(1,5,12,22,23,24,35,37),(1,5,13,22,23,24,36,38),(1,5,14,22,23,25,38,39),(1,5,15,23,23,25,39,41),(1,5,16,23,24,26,40,42),(1,5,17,23,24,26,42,43),(1,5,18,23,24,26,43,45),(1,5,19,23,25,27,44,46),(1,5,20,24,25,27,46,48),(1,5,21,24,25,28,47,49),(1,5,22,24,25,28,49,51),(1,5,23,24,26,29,50,52),(1,5,24,25,26,29,52,54),(1,5,25,25,26,30,53,55),(1,5,26,25,27,30,55,57),(1,5,27,25,27,30,56,59),(1,5,28,25,27,31,58,60),(1,5,29,26,28,31,59,62),(1,5,30,26,28,32,61,64),(1,5,31,26,28,32,63,65),(1,5,32,26,29,33,64,67),(1,5,33,27,29,33,66,69),(1,5,34,27,29,34,68,70),(1,5,35,27,30,34,69,72),(1,5,36,28,30,35,71,74),(1,5,37,28,30,36,73,76),(1,5,38,28,31,36,75,78),(1,5,39,28,31,37,76,80),(1,5,40,29,31,37,78,81),(1,5,41,29,32,38,80,83),(1,5,42,29,32,38,82,85),(1,5,43,29,33,39,84,87),(1,5,44,30,33,39,86,89),(1,5,45,30,33,40,88,91),(1,5,46,30,34,41,90,93),(1,5,47,31,34,41,92,95),(1,5,48,31,35,42,94,98),(1,5,49,31,35,43,96,100),(1,5,50,32,35,43,102,102),(1,5,51,32,36,44,105,104),(1,5,52,32,36,44,107,106),(1,5,53,33,37,45,109,108),(1,5,54,33,37,46,111,111),(1,5,55,33,38,46,114,113),(1,5,56,34,38,47,116,115),(1,5,57,34,39,48,118,118),(1,5,58,34,39,49,120,120),(1,5,59,35,40,49,123,123),(1,5,60,35,40,50,126,125),(1,5,61,35,40,51,128,127),(1,5,62,36,41,51,131,130),(1,5,63,36,41,52,133,133),(1,5,64,36,42,53,136,135),(1,5,65,37,43,54,138,138),(1,5,66,37,43,55,141,140),(1,5,67,38,44,55,143,143),(1,5,68,38,44,56,147,146),(1,5,69,38,45,57,150,149),(1,5,70,39,45,58,152,151),(1,5,71,39,46,59,155,154),(1,5,72,40,46,59,158,157),(1,5,73,40,47,60,161,160),(1,5,74,41,47,61,163,163),(1,5,75,41,48,62,166,166),(1,5,76,41,49,63,170,169),(1,5,77,42,49,64,173,172),(1,5,78,42,50,65,176,175),(1,5,79,43,50,66,179,178),(1,5,80,43,51,67,182,181),(1,6,1,23,20,22,20,20),(1,6,2,24,21,23,20,20),(1,6,3,26,22,24,20,21),(1,6,4,27,22,26,20,21),(1,6,5,28,23,27,20,21),(1,6,6,30,24,28,20,21),(1,6,7,31,25,29,21,22),(1,6,8,32,26,30,21,22),(1,6,9,34,26,32,21,22),(1,6,10,35,27,33,21,23),(1,6,11,36,28,34,21,23),(1,6,12,38,29,35,21,23),(1,6,13,39,30,37,21,24),(1,6,14,41,31,38,21,24),(1,6,15,42,32,39,21,24),(1,6,16,44,33,41,21,25),(1,6,17,45,34,42,22,25),(1,6,18,47,34,43,22,25),(1,6,19,48,35,45,22,26),(1,6,20,50,36,46,22,26),(1,6,21,51,37,48,22,26),(1,6,22,53,38,49,22,27),(1,6,23,54,39,51,22,27),(1,6,24,56,40,52,23,28),(1,6,25,58,41,53,23,28),(1,6,26,59,42,55,23,28),(1,6,27,61,43,56,23,29),(1,6,28,63,44,58,23,29),(1,6,29,64,45,59,23,30),(1,6,30,66,46,61,24,30),(1,6,31,68,47,62,24,30),(1,6,32,69,48,64,24,31),(1,6,33,71,50,66,24,31),(1,6,34,73,51,67,24,32),(1,6,35,74,52,69,24,32),(1,6,36,76,53,70,25,33),(1,6,37,78,54,72,25,33),(1,6,38,80,55,74,25,34),(1,6,39,82,56,75,25,34),(1,6,40,83,57,77,25,35),(1,6,41,85,58,79,26,35),(1,6,42,87,60,80,26,35),(1,6,43,89,61,82,26,36),(1,6,44,91,62,84,26,36),(1,6,45,93,63,85,26,37),(1,6,46,95,64,87,27,37),(1,6,47,97,66,89,27,38),(1,6,48,99,67,91,27,38),(1,6,49,101,68,93,27,39),(1,6,50,103,69,94,28,40),(1,6,51,105,71,96,28,40),(1,6,52,106,72,97,28,41),(1,6,53,107,72,98,28,41),(1,6,54,107,73,98,29,42),(1,6,55,108,73,99,29,42),(1,6,56,111,75,102,29,43),(1,6,57,113,76,104,29,43),(1,6,58,115,77,106,30,44),(1,6,59,118,79,108,30,44),(1,6,60,120,80,110,30,45),(1,6,61,122,81,112,30,46),(1,6,62,125,83,114,30,46),(1,6,63,127,84,117,31,47),(1,6,64,130,86,119,31,47),(1,6,65,133,87,121,31,48),(1,6,66,135,89,124,31,49),(1,6,67,138,90,126,32,49),(1,6,68,140,92,128,32,50),(1,6,69,143,93,131,32,51),(1,6,70,146,95,133,32,51),(1,6,71,149,97,136,33,52),(1,6,72,151,98,138,33,53),(1,6,73,154,100,141,33,54),(1,6,74,157,102,143,33,54),(1,6,75,160,103,146,34,55),(1,6,76,163,105,149,34,56),(1,6,77,166,107,151,34,57),(1,6,78,169,108,154,34,57),(1,6,79,172,110,157,35,58),(1,6,80,175,112,160,35,59),(1,8,1,20,20,20,23,22),(1,8,2,20,20,20,24,23),(1,8,3,20,20,21,26,25),(1,8,4,20,20,21,27,26),(1,8,5,20,21,21,28,27),(1,8,6,20,21,21,30,29),(1,8,7,21,21,22,31,30),(1,8,8,21,21,22,33,31),(1,8,9,21,21,22,34,33),(1,8,10,21,22,23,34,33),(1,8,11,21,22,23,35,34),(1,8,12,21,22,23,37,35),(1,8,13,21,22,24,38,36),(1,8,14,22,22,24,39,38),(1,8,15,22,23,24,41,39),(1,8,16,22,23,25,42,40),(1,8,17,22,23,25,43,42),(1,8,18,22,23,25,45,43),(1,8,19,22,23,26,46,44),(1,8,20,22,24,26,48,46),(1,8,21,23,24,26,49,47),(1,8,22,23,24,27,51,49),(1,8,23,23,24,27,52,50),(1,8,24,23,25,28,54,52),(1,8,25,23,25,28,55,53),(1,8,26,23,25,28,57,55),(1,8,27,23,25,29,59,56),(1,8,28,24,25,29,60,58),(1,8,29,24,26,30,62,59),(1,8,30,24,26,30,64,61),(1,8,31,24,26,30,65,63),(1,8,32,24,26,31,67,64),(1,8,33,24,27,31,69,66),(1,8,34,25,27,32,70,68),(1,8,35,25,27,32,72,69),(1,8,36,25,28,33,74,71),(1,8,37,25,28,33,76,73),(1,8,38,25,28,33,78,75),(1,8,39,26,28,34,80,76),(1,8,40,26,29,34,81,78),(1,8,41,26,29,35,83,80),(1,8,42,26,29,35,85,82),(1,8,43,26,29,36,87,84),(1,8,44,26,30,36,89,86),(1,8,45,27,30,37,91,88),(1,8,46,27,30,37,93,90),(1,8,47,27,31,38,95,92),(1,8,48,27,31,38,98,94),(1,8,49,28,31,39,100,96),(1,8,50,28,32,39,107,98),(1,8,51,28,32,40,109,100),(1,8,52,28,32,40,111,102),(1,8,53,28,33,41,113,104),(1,8,54,29,33,42,116,106),(1,8,55,29,33,42,118,109),(1,8,56,29,34,43,120,111),(1,8,57,29,34,43,123,113),(1,8,58,30,34,44,126,115),(1,8,59,30,35,44,129,118),(1,8,60,30,35,45,131,120),(1,8,61,30,35,46,133,122),(1,8,62,30,36,46,136,125),(1,8,63,31,36,47,139,127),(1,8,64,31,36,47,141,130),(1,8,65,31,37,48,144,132),(1,8,66,32,37,49,147,135),(1,8,67,32,38,49,150,137),(1,8,68,32,38,50,153,140),(1,8,69,32,38,51,156,143),(1,8,70,33,39,51,158,145),(1,8,71,33,39,52,161,148),(1,8,72,33,40,53,164,151),(1,8,73,33,40,54,168,154),(1,8,74,34,41,54,171,156),(1,8,75,34,41,55,174,159),(1,8,76,34,41,56,177,162),(1,8,77,35,42,57,180,165),(1,8,78,35,42,57,183,168),(1,8,79,35,43,58,186,171),(1,8,80,36,43,59,190,174),(1,9,1,20,20,21,22,22),(1,9,2,20,20,22,23,23),(1,9,3,21,21,22,24,24),(1,9,4,21,21,23,26,25),(1,9,5,21,21,23,27,27),(1,9,6,21,22,24,28,28),(1,9,7,22,22,24,29,29),(1,9,8,22,23,25,30,30),(1,9,9,22,23,26,32,31),(1,9,10,23,23,26,31,32),(1,9,11,23,24,26,33,33),(1,9,12,23,24,27,34,34),(1,9,13,24,24,27,35,36),(1,9,14,24,25,28,36,37),(1,9,15,24,25,29,37,38),(1,9,16,25,26,29,38,39),(1,9,17,25,26,30,40,41),(1,9,18,25,26,30,41,42),(1,9,19,26,27,31,42,43),(1,9,20,26,27,32,43,45),(1,9,21,26,28,32,45,46),(1,9,22,27,28,33,46,47),(1,9,23,27,29,34,47,49),(1,9,24,28,29,34,49,50),(1,9,25,28,30,35,50,52),(1,9,26,28,30,36,51,53),(1,9,27,29,30,36,53,54),(1,9,28,29,31,37,54,56),(1,9,29,30,31,38,56,57),(1,9,30,30,32,38,57,59),(1,9,31,30,32,39,58,61),(1,9,32,31,33,40,60,62),(1,9,33,31,33,41,61,64),(1,9,34,32,34,41,63,65),(1,9,35,32,34,42,64,67),(1,9,36,33,35,43,66,69),(1,9,37,33,36,44,68,70),(1,9,38,33,36,45,69,72),(1,9,39,34,37,45,71,74),(1,9,40,34,37,46,72,75),(1,9,41,35,38,47,74,77),(1,9,42,35,38,48,76,79),(1,9,43,36,39,49,77,81),(1,9,44,36,39,50,79,82),(1,9,45,37,40,50,81,84),(1,9,46,37,41,51,83,86),(1,9,47,38,41,52,84,88),(1,9,48,38,42,53,86,90),(1,9,49,39,43,54,88,92),(1,9,50,39,43,55,94,94),(1,9,51,40,44,56,96,96),(1,9,52,40,44,57,98,98),(1,9,53,41,45,58,100,100),(1,9,54,42,46,59,102,102),(1,9,55,42,46,60,105,104),(1,9,56,43,47,61,107,106),(1,9,57,43,48,62,109,108),(1,9,58,44,49,63,111,111),(1,9,59,44,49,64,113,113),(1,9,60,45,50,65,115,115),(1,9,61,46,51,66,117,117),(1,9,62,46,51,67,119,120),(1,9,63,47,52,68,122,122),(1,9,64,47,53,69,124,124),(1,9,65,48,54,71,127,127),(1,9,66,49,55,72,129,129),(1,9,67,49,55,73,132,132),(1,9,68,50,56,74,134,134),(1,9,69,51,57,75,136,136),(1,9,70,51,58,76,139,139),(1,9,71,52,59,78,141,142),(1,9,72,53,59,79,144,144),(1,9,73,54,60,80,147,147),(1,9,74,54,61,81,150,150),(1,9,75,55,62,83,152,152),(1,9,76,56,63,84,155,155),(1,9,77,57,64,85,158,158),(1,9,78,57,65,87,160,161),(1,9,79,58,66,88,163,164),(1,9,80,59,67,89,166,166),(2,1,1,26,17,24,17,23),(2,1,2,27,18,25,17,23),(2,1,3,29,19,26,17,24),(2,1,4,30,19,27,17,24),(2,1,5,31,20,29,17,24),(2,1,6,32,21,30,17,24),(2,1,7,34,22,31,18,25),(2,1,8,35,23,32,18,25),(2,1,9,37,24,34,18,25),(2,1,10,36,23,32,18,25),(2,1,11,38,24,34,18,25),(2,1,12,39,25,35,18,25),(2,1,13,40,26,36,18,26),(2,1,14,42,27,37,19,26),(2,1,15,43,27,38,19,26),(2,1,16,44,28,39,19,27),(2,1,17,45,29,41,19,27),(2,1,18,47,30,42,19,27),(2,1,19,48,31,43,19,28),(2,1,20,50,32,44,19,28),(2,1,21,51,32,46,20,28),(2,1,22,52,33,47,20,29),(2,1,23,54,34,48,20,29),(2,1,24,55,35,50,20,30),(2,1,25,57,36,51,20,30),(2,1,26,58,37,52,20,30),(2,1,27,60,38,54,20,31),(2,1,28,61,39,55,21,31),(2,1,29,63,40,57,21,32),(2,1,30,65,41,58,21,32),(2,1,31,66,42,59,21,32),(2,1,32,68,43,61,21,33),(2,1,33,69,44,62,21,33),(2,1,34,71,45,64,22,34),(2,1,35,73,46,65,22,34),(2,1,36,75,47,67,22,35),(2,1,37,76,48,69,22,35),(2,1,38,78,49,70,22,35),(2,1,39,80,50,72,23,36),(2,1,40,82,51,73,23,36),(2,1,41,83,53,75,23,37),(2,1,42,85,54,77,23,37),(2,1,43,87,55,78,23,38),(2,1,44,89,56,80,23,38),(2,1,45,91,57,82,24,39),(2,1,46,93,58,84,24,39),(2,1,47,95,60,85,24,40),(2,1,48,97,61,87,24,40),(2,1,49,99,62,89,25,41),(2,1,50,101,63,91,25,41),(2,1,51,103,65,93,25,42),(2,1,52,105,66,95,25,42),(2,1,53,107,67,97,25,43),(2,1,54,109,69,99,26,44),(2,1,55,112,70,101,26,44),(2,1,56,114,71,103,26,45),(2,1,57,116,73,105,26,45),(2,1,58,118,74,107,27,46),(2,1,59,121,76,109,27,46),(2,1,60,123,77,111,27,47),(2,1,61,125,78,113,27,48),(2,1,62,128,80,115,27,48),(2,1,63,130,81,118,28,49),(2,1,64,133,83,120,28,49),(2,1,65,135,85,122,28,50),(2,1,66,138,86,124,29,51),(2,1,67,140,88,127,29,51),(2,1,68,143,89,129,29,52),(2,1,69,145,91,131,29,53),(2,1,70,148,93,134,30,53),(2,1,71,151,94,136,30,54),(2,1,72,153,96,139,30,55),(2,1,73,156,98,141,30,56),(2,1,74,159,99,144,31,56),(2,1,75,162,101,146,31,57),(2,1,76,165,103,149,31,58),(2,1,77,168,105,152,32,59),(2,1,78,171,106,154,32,59),(2,1,79,174,108,157,32,60),(2,1,80,177,110,160,33,61),(2,3,1,23,20,23,17,24),(2,3,2,23,21,24,18,25),(2,3,3,24,23,25,18,25),(2,3,4,24,24,26,19,26),(2,3,5,25,25,27,19,26),(2,3,6,25,27,28,20,27),(2,3,7,26,28,28,21,28),(2,3,8,26,30,29,21,28),(2,3,9,26,31,30,22,29),(2,3,10,27,31,29,22,28),(2,3,11,27,32,30,22,29),(2,3,12,28,34,31,23,30),(2,3,13,28,35,32,24,30),(2,3,14,29,36,33,24,31),(2,3,15,29,38,34,25,31),(2,3,16,30,39,35,25,32),(2,3,17,30,40,36,26,33),(2,3,18,31,42,37,27,34),(2,3,19,31,43,38,27,34),(2,3,20,32,45,39,28,35),(2,3,21,32,46,40,29,36),(2,3,22,33,48,41,29,36),(2,3,23,33,49,42,30,37),(2,3,24,34,51,43,31,38),(2,3,25,34,52,44,31,39),(2,3,26,35,54,45,32,39),(2,3,27,35,56,46,33,40),(2,3,28,36,57,47,33,41),(2,3,29,36,59,48,34,42),(2,3,30,37,61,49,35,42),(2,3,31,37,62,51,36,43),(2,3,32,38,64,52,36,44),(2,3,33,39,66,53,37,45),(2,3,34,39,67,54,38,46),(2,3,35,40,69,55,39,47),(2,3,36,41,71,57,40,48),(2,3,37,41,73,58,40,48),(2,3,38,42,75,59,41,49),(2,3,39,42,77,60,42,50),(2,3,40,43,78,62,43,51),(2,3,41,44,80,63,44,52),(2,3,42,44,82,64,44,53),(2,3,43,45,84,65,45,54),(2,3,44,46,86,67,46,55),(2,3,45,46,88,68,47,56),(2,3,46,47,90,70,48,57),(2,3,47,48,92,71,49,58),(2,3,48,49,95,72,50,59),(2,3,49,49,97,74,51,60),(2,3,50,50,99,75,52,61),(2,3,51,51,101,77,53,62),(2,3,52,52,103,78,54,63),(2,3,53,52,105,80,55,64),(2,3,54,53,108,81,56,65),(2,3,55,54,110,83,57,66),(2,3,56,55,112,84,58,67),(2,3,57,56,115,86,59,69),(2,3,58,56,117,88,60,70),(2,3,59,57,120,89,61,71),(2,3,60,58,122,91,62,72),(2,3,61,59,124,93,63,73),(2,3,62,60,127,94,64,74),(2,3,63,61,130,96,65,76),(2,3,64,61,132,98,66,77),(2,3,65,62,135,100,68,78),(2,3,66,63,137,101,69,79),(2,3,67,64,140,103,70,81),(2,3,68,65,143,105,71,82),(2,3,69,66,146,107,72,83),(2,3,70,67,148,109,74,85),(2,3,71,68,151,111,75,86),(2,3,72,69,154,113,76,87),(2,3,73,70,157,115,77,89),(2,3,74,71,160,117,79,90),(2,3,75,72,163,119,80,92),(2,3,76,73,174,121,81,93),(2,3,77,74,177,123,83,95),(2,3,78,75,180,125,84,96),(2,3,79,76,183,127,85,98),(2,3,80,77,187,129,87,99),(2,4,1,24,20,23,17,23),(2,4,2,25,21,24,17,23),(2,4,3,25,23,24,17,24),(2,4,4,26,24,25,17,24),(2,4,5,27,26,26,18,24),(2,4,6,28,27,26,18,25),(2,4,7,29,29,27,18,25),(2,4,8,29,30,28,18,26),(2,4,9,30,32,29,18,26),(2,4,10,30,32,28,19,25),(2,4,11,31,33,29,19,26),(2,4,12,32,34,29,19,26),(2,4,13,33,36,30,19,26),(2,4,14,33,37,31,19,27),(2,4,15,34,39,31,20,27),(2,4,16,35,40,32,20,28),(2,4,17,36,41,33,20,28),(2,4,18,37,43,34,20,28),(2,4,19,38,45,34,20,29),(2,4,20,38,46,35,21,29),(2,4,21,39,48,36,21,30),(2,4,22,40,49,37,21,30),(2,4,23,41,51,38,21,31),(2,4,24,42,52,38,22,31),(2,4,25,43,54,39,22,32),(2,4,26,44,56,40,22,32),(2,4,27,45,57,41,22,32),(2,4,28,46,59,42,22,33),(2,4,29,46,61,43,23,33),(2,4,30,47,63,43,23,34),(2,4,31,48,64,44,23,34),(2,4,32,49,66,45,23,35),(2,4,33,50,68,46,24,35),(2,4,34,51,70,47,24,36),(2,4,35,52,72,48,24,36),(2,4,36,54,74,49,25,37),(2,4,37,55,75,50,25,38),(2,4,38,56,77,51,25,38),(2,4,39,57,79,52,25,39),(2,4,40,58,81,53,26,39),(2,4,41,59,83,54,26,40),(2,4,42,60,85,55,26,40),(2,4,43,61,87,56,26,41),(2,4,44,62,90,57,27,41),(2,4,45,64,92,58,27,42),(2,4,46,65,94,59,27,43),(2,4,47,66,96,60,28,43),(2,4,48,67,98,61,28,44),(2,4,49,68,100,63,28,45),(2,4,50,70,103,64,29,45),(2,4,51,71,110,65,29,46),(2,4,52,72,110,66,29,46),(2,4,53,73,115,67,30,47),(2,4,54,75,117,68,30,48),(2,4,55,76,119,70,30,48),(2,4,56,77,123,71,31,49),(2,4,57,79,125,72,31,50),(2,4,58,80,128,73,31,51),(2,4,59,82,130,75,32,51),(2,4,60,83,133,76,32,52),(2,4,61,84,136,77,32,53),(2,4,62,86,138,79,33,53),(2,4,63,87,138,80,33,54),(2,4,64,89,145,81,33,55),(2,4,65,90,147,83,34,56),(2,4,66,92,150,84,34,57),(2,4,67,93,153,86,35,57),(2,4,68,95,156,87,35,58),(2,4,69,97,159,88,35,59),(2,4,70,98,159,90,36,60),(2,4,71,100,166,91,36,61),(2,4,72,102,169,93,37,61),(2,4,73,103,172,95,37,62),(2,4,74,105,175,96,38,63),(2,4,75,107,178,98,38,64),(2,4,76,108,181,99,38,65),(2,4,77,110,185,101,39,66),(2,4,78,112,189,103,39,67),(2,4,79,114,192,104,40,68),(2,4,80,116,195,106,40,69),(2,6,1,26,17,24,17,23),(2,6,2,27,18,25,17,23),(2,6,3,29,19,26,17,24),(2,6,4,30,19,27,17,24),(2,6,5,31,20,29,17,24),(2,6,6,32,21,30,17,24),(2,6,7,34,22,31,18,25),(2,6,8,35,23,32,18,25),(2,6,9,37,24,34,18,25),(2,6,10,38,24,35,18,26),(2,6,11,39,25,36,18,26),(2,6,12,41,26,37,18,26),(2,6,13,42,27,39,18,27),(2,6,14,44,28,40,18,27),(2,6,15,45,29,41,18,27),(2,6,16,47,30,43,19,28),(2,6,17,48,31,44,19,28),(2,6,18,50,32,45,19,28),(2,6,19,51,33,47,19,29),(2,6,20,53,34,48,19,29),(2,6,21,54,34,50,19,29),(2,6,22,56,35,51,19,30),(2,6,23,57,36,52,20,30),(2,6,24,59,37,54,20,30),(2,6,25,60,38,55,20,31),(2,6,26,62,39,57,20,31),(2,6,27,64,40,58,20,32),(2,6,28,65,41,60,20,32),(2,6,29,67,43,61,21,32),(2,6,30,69,44,63,21,33),(2,6,31,70,45,64,21,33),(2,6,32,72,46,66,21,34),(2,6,33,74,47,67,21,34),(2,6,34,76,48,69,21,35),(2,6,35,77,49,71,22,35),(2,6,36,79,50,72,22,36),(2,6,37,81,51,74,22,36),(2,6,38,83,52,76,22,36),(2,6,39,84,53,77,22,37),(2,6,40,86,55,79,23,37),(2,6,41,88,56,81,23,38),(2,6,42,90,57,82,23,38),(2,6,43,92,58,84,23,39),(2,6,44,94,59,86,23,39),(2,6,45,96,60,87,24,40),(2,6,46,98,62,89,24,40),(2,6,47,100,63,91,24,41),(2,6,48,101,64,93,24,41),(2,6,49,103,65,94,25,42),(2,6,50,105,66,96,25,42),(2,6,51,107,68,98,25,43),(2,6,52,109,69,100,25,43),(2,6,53,110,69,100,25,44),(2,6,54,111,70,101,26,45),(2,6,55,111,70,100,26,44),(2,6,56,114,72,103,26,45),(2,6,57,116,73,105,26,45),(2,6,58,118,74,107,27,46),(2,6,59,121,76,109,27,46),(2,6,60,123,77,111,27,47),(2,6,61,125,78,113,27,48),(2,6,62,128,80,115,27,48),(2,6,63,130,81,118,28,49),(2,6,64,133,83,120,28,49),(2,6,65,136,84,122,28,50),(2,6,66,138,86,125,28,51),(2,6,67,141,87,127,29,51),(2,6,68,143,89,129,29,52),(2,6,69,146,90,132,29,53),(2,6,70,149,92,134,29,53),(2,6,71,152,94,137,30,54),(2,6,72,154,95,139,30,55),(2,6,73,157,97,142,30,56),(2,6,74,160,99,144,30,56),(2,6,75,163,100,147,31,57),(2,6,76,166,102,150,31,58),(2,6,77,169,104,152,31,59),(2,6,78,172,105,155,31,59),(2,6,79,175,107,158,32,60),(2,6,80,178,109,161,32,61),(2,7,1,24,17,23,18,25),(2,7,2,25,17,24,19,26),(2,7,3,26,18,25,20,27),(2,7,4,26,18,26,21,28),(2,7,5,27,19,27,22,29),(2,7,6,28,19,28,23,30),(2,7,7,29,20,29,24,31),(2,7,8,30,20,30,25,32),(2,7,9,31,21,31,26,33),(2,7,10,31,21,30,25,32),(2,7,11,32,21,31,26,33),(2,7,12,33,22,32,27,34),(2,7,13,33,22,33,28,35),(2,7,14,34,23,34,29,36),(2,7,15,35,23,35,30,38),(2,7,16,36,24,36,31,39),(2,7,17,37,24,37,32,40),(2,7,18,38,25,38,33,41),(2,7,19,39,25,39,34,42),(2,7,20,40,26,40,35,43),(2,7,21,41,26,41,36,44),(2,7,22,41,27,42,37,45),(2,7,23,42,27,43,38,46),(2,7,24,43,28,44,39,48),(2,7,25,44,28,46,40,49),(2,7,26,45,29,47,41,50),(2,7,27,46,29,48,42,51),(2,7,28,47,30,49,43,52),(2,7,29,48,30,50,44,54),(2,7,30,49,31,51,45,55),(2,7,31,51,31,53,47,56),(2,7,32,52,32,54,48,58),(2,7,33,53,33,55,49,59),(2,7,34,54,33,56,50,60),(2,7,35,55,34,58,51,62),(2,7,36,56,35,59,53,63),(2,7,37,57,35,60,54,64),(2,7,38,58,36,62,55,66),(2,7,39,59,36,63,56,67),(2,7,40,61,37,64,58,69),(2,7,41,62,38,66,59,70),(2,7,42,63,38,67,60,72),(2,7,43,64,39,69,61,73),(2,7,44,66,40,70,63,75),(2,7,45,67,40,72,64,76),(2,7,46,68,41,73,66,78),(2,7,47,69,42,75,67,79),(2,7,48,71,43,76,68,81),(2,7,49,72,43,78,70,83),(2,7,50,73,44,79,71,84),(2,7,51,75,45,81,73,86),(2,7,52,76,46,82,74,88),(2,7,53,78,46,84,76,89),(2,7,54,79,47,86,77,91),(2,7,55,80,48,87,79,93),(2,7,56,82,49,89,80,95),(2,7,57,83,50,91,82,96),(2,7,58,85,50,92,84,98),(2,7,59,86,51,94,85,100),(2,7,60,88,52,96,87,102),(2,7,61,90,53,98,89,104),(2,7,62,91,54,100,90,106),(2,7,63,93,55,101,92,108),(2,7,64,94,55,103,94,110),(2,7,65,96,56,105,96,112),(2,7,66,98,57,107,97,114),(2,7,67,99,58,109,99,116),(2,7,68,101,59,111,101,118),(2,7,69,103,60,113,103,120),(2,7,70,105,61,115,105,122),(2,7,71,106,62,117,107,124),(2,7,72,108,63,119,109,127),(2,7,73,110,64,121,111,129),(2,7,74,112,65,124,113,131),(2,7,75,114,66,126,115,133),(2,7,76,116,67,128,117,136),(2,7,77,117,68,130,119,138),(2,7,78,119,69,132,121,140),(2,7,79,121,70,135,123,143),(2,7,80,123,71,137,125,145),(2,9,1,23,17,23,19,25),(2,9,2,23,17,24,20,26),(2,9,3,24,18,24,21,27),(2,9,4,24,18,25,23,28),(2,9,5,24,18,25,24,30),(2,9,6,24,19,26,25,31),(2,9,7,25,19,26,26,32),(2,9,8,25,20,27,27,33),(2,9,9,25,20,27,29,34),(2,9,10,26,20,27,28,34),(2,9,11,26,21,27,30,35),(2,9,12,26,21,28,31,36),(2,9,13,27,21,28,32,38),(2,9,14,27,22,29,33,39),(2,9,15,27,22,30,34,40),(2,9,16,28,23,30,35,41),(2,9,17,28,23,31,37,43),(2,9,18,28,23,31,38,44),(2,9,19,29,24,32,39,45),(2,9,20,29,24,33,40,47),(2,9,21,29,25,33,42,48),(2,9,22,30,25,34,43,49),(2,9,23,30,26,35,44,51),(2,9,24,31,26,35,46,52),(2,9,25,31,27,36,47,54),(2,9,26,31,27,37,48,55),(2,9,27,32,27,37,50,56),(2,9,28,32,28,38,51,58),(2,9,29,33,28,39,53,59),(2,9,30,33,29,39,54,61),(2,9,31,33,29,40,55,63),(2,9,32,34,30,41,57,64),(2,9,33,34,30,42,58,66),(2,9,34,35,31,42,60,67),(2,9,35,35,31,43,61,69),(2,9,36,36,32,44,63,71),(2,9,37,36,33,45,65,72),(2,9,38,36,33,46,66,74),(2,9,39,37,34,46,68,76),(2,9,40,37,34,47,69,77),(2,9,41,38,35,48,71,79),(2,9,42,38,35,49,73,81),(2,9,43,39,36,50,74,83),(2,9,44,39,36,51,76,84),(2,9,45,40,37,51,78,86),(2,9,46,40,38,52,80,88),(2,9,47,41,38,53,81,90),(2,9,48,41,39,54,83,92),(2,9,49,42,40,55,85,94),(2,9,50,42,40,56,91,96),(2,9,51,43,41,57,93,98),(2,9,52,43,41,58,95,100),(2,9,53,44,42,59,97,102),(2,9,54,45,43,60,99,104),(2,9,55,45,43,61,102,106),(2,9,56,46,44,62,104,108),(2,9,57,46,45,63,106,110),(2,9,58,47,46,64,108,113),(2,9,59,47,46,65,110,115),(2,9,60,48,47,66,112,117),(2,9,61,49,48,67,114,119),(2,9,62,49,48,68,116,122),(2,9,63,50,49,69,119,124),(2,9,64,50,50,70,121,126),(2,9,65,51,51,72,124,129),(2,9,66,52,52,73,126,131),(2,9,67,52,52,74,129,134),(2,9,68,53,53,75,131,136),(2,9,69,54,54,76,133,138),(2,9,70,54,55,77,136,141),(2,9,71,55,56,79,138,144),(2,9,72,56,56,80,141,146),(2,9,73,57,57,81,144,149),(2,9,74,57,58,82,147,152),(2,9,75,58,59,84,149,154),(2,9,76,59,60,85,152,157),(2,9,77,60,61,86,155,160),(2,9,78,60,62,88,157,163),(2,9,79,61,63,89,160,166),(2,9,80,62,64,90,163,168),(3,1,1,25,16,25,19,19),(3,1,2,26,17,26,19,19),(3,1,3,28,18,27,19,20),(3,1,4,29,18,28,19,20),(3,1,5,30,19,30,19,20),(3,1,6,31,20,31,19,20),(3,1,7,33,21,32,20,21),(3,1,8,34,22,33,20,21),(3,1,9,36,23,35,20,21),(3,1,10,38,22,32,20,22),(3,1,11,40,23,34,20,22),(3,1,12,41,24,35,20,22),(3,1,13,42,25,36,20,23),(3,1,14,44,26,37,21,23),(3,1,15,45,26,38,21,23),(3,1,16,46,27,39,21,24),(3,1,17,47,28,41,21,24),(3,1,18,49,29,42,21,24),(3,1,19,50,30,43,21,25),(3,1,20,52,31,44,21,25),(3,1,21,53,31,46,22,25),(3,1,22,54,32,47,22,26),(3,1,23,56,33,48,22,26),(3,1,24,57,34,50,22,27),(3,1,25,59,35,51,22,27),(3,1,26,60,36,52,22,27),(3,1,27,62,37,54,22,28),(3,1,28,63,38,55,23,28),(3,1,29,65,39,57,23,29),(3,1,30,67,40,58,23,29),(3,1,31,68,41,59,23,29),(3,1,32,70,42,61,23,30),(3,1,33,71,43,62,23,30),(3,1,34,73,44,64,24,31),(3,1,35,75,45,65,24,31),(3,1,36,77,46,67,24,32),(3,1,37,78,47,69,24,32),(3,1,38,80,48,70,24,32),(3,1,39,82,49,72,25,33),(3,1,40,84,50,73,25,33),(3,1,41,85,52,75,25,34),(3,1,42,87,53,77,25,34),(3,1,43,89,54,78,25,35),(3,1,44,91,55,80,25,35),(3,1,45,93,56,82,26,36),(3,1,46,95,57,84,26,36),(3,1,47,97,59,85,26,37),(3,1,48,99,60,87,26,37),(3,1,49,101,61,89,27,38),(3,1,50,103,62,91,27,38),(3,1,51,105,64,93,27,39),(3,1,52,107,65,95,27,39),(3,1,53,109,66,97,27,40),(3,1,54,111,68,99,28,41),(3,1,55,114,69,101,28,41),(3,1,56,116,70,103,28,42),(3,1,57,118,72,105,28,42),(3,1,58,120,73,107,29,43),(3,1,59,123,75,109,29,43),(3,1,60,125,76,111,29,44),(3,1,61,127,77,113,29,45),(3,1,62,130,79,115,29,45),(3,1,63,132,80,118,30,46),(3,1,64,135,82,120,30,46),(3,1,65,137,84,122,30,47),(3,1,66,140,85,124,31,48),(3,1,67,142,87,127,31,48),(3,1,68,145,88,129,31,49),(3,1,69,147,90,131,31,50),(3,1,70,150,92,134,32,50),(3,1,71,153,93,136,32,51),(3,1,72,155,95,139,32,52),(3,1,73,158,97,141,32,53),(3,1,74,161,98,144,33,53),(3,1,75,164,100,146,33,54),(3,1,76,167,102,149,33,55),(3,1,77,170,104,152,34,56),(3,1,78,173,105,154,34,56),(3,1,79,176,107,157,34,57),(3,1,80,179,109,160,35,58),(3,2,1,24,16,25,19,20),(3,2,2,25,17,26,20,21),(3,2,3,26,17,27,20,21),(3,2,4,27,18,28,21,22),(3,2,5,28,18,29,22,23),(3,2,6,29,19,30,22,24),(3,2,7,31,20,31,23,24),(3,2,8,32,20,32,24,25),(3,2,9,33,21,33,24,26),(3,2,10,36,21,31,24,26),(3,2,11,37,21,32,25,27),(3,2,12,38,22,33,26,27),(3,2,13,39,23,34,26,28),(3,2,14,40,23,35,27,29),(3,2,15,41,24,37,28,29),(3,2,16,43,24,38,28,30),(3,2,17,44,25,39,29,31),(3,2,18,45,26,40,30,32),(3,2,19,46,26,41,30,32),(3,2,20,47,27,42,31,33),(3,2,21,48,28,43,32,34),(3,2,22,50,28,44,33,35),(3,2,23,51,29,45,33,36),(3,2,24,52,30,47,34,36),(3,2,25,53,30,48,35,37),(3,2,26,55,31,49,36,38),(3,2,27,56,32,50,36,39),(3,2,28,57,32,51,37,40),(3,2,29,59,33,53,38,41),(3,2,30,60,34,54,39,41),(3,2,31,61,35,55,40,42),(3,2,32,63,35,57,41,43),(3,2,33,64,36,58,41,44),(3,2,34,66,37,59,42,45),(3,2,35,67,38,61,43,46),(3,2,36,69,39,62,44,47),(3,2,37,70,39,63,45,48),(3,2,38,72,40,65,46,49),(3,2,39,73,41,66,47,50),(3,2,40,75,42,68,48,51),(3,2,41,76,43,69,49,52),(3,2,42,78,43,71,50,53),(3,2,43,79,44,72,51,54),(3,2,44,81,45,74,51,55),(3,2,45,83,46,75,52,56),(3,2,46,84,47,77,53,57),(3,2,47,86,48,78,55,58),(3,2,48,88,49,80,56,59),(3,2,49,89,50,82,57,61),(3,2,50,91,51,83,58,62),(3,2,51,93,52,85,59,63),(3,2,52,95,53,87,60,64),(3,2,53,97,54,88,61,65),(3,2,54,98,55,90,62,66),(3,2,55,100,56,92,63,68),(3,2,56,102,57,94,64,69),(3,2,57,104,58,95,65,70),(3,2,58,106,59,97,67,71),(3,2,59,113,60,99,68,73),(3,2,60,113,61,101,69,74),(3,2,61,113,62,103,70,75),(3,2,62,119,63,105,71,77),(3,2,63,121,64,107,73,78),(3,2,64,121,65,109,74,79),(3,2,65,121,67,111,75,81),(3,2,66,123,68,113,77,82),(3,2,67,125,69,115,78,84),(3,2,68,127,70,117,79,85),(3,2,69,135,71,119,81,86),(3,2,70,135,73,121,82,88),(3,2,71,135,74,123,83,89),(3,2,72,142,75,126,85,91),(3,2,73,144,76,128,86,93),(3,2,74,144,78,130,88,94),(3,2,75,149,79,132,89,96),(3,2,76,149,80,135,91,97),(3,2,77,155,82,137,92,99),(3,2,78,158,83,139,94,101),(3,2,79,160,84,142,95,102),(3,2,80,160,86,144,97,104),(3,3,1,22,19,24,19,20),(3,3,2,22,20,25,20,21),(3,3,3,23,22,26,20,21),(3,3,4,23,23,27,21,22),(3,3,5,24,25,28,21,23),(3,3,6,24,26,29,22,23),(3,3,7,25,27,29,23,24),(3,3,8,25,29,30,23,25),(3,3,9,25,30,31,24,25),(3,3,10,29,30,29,24,25),(3,3,11,29,31,30,24,26),(3,3,12,30,33,31,25,27),(3,3,13,30,34,32,26,27),(3,3,14,31,35,33,26,28),(3,3,15,31,37,34,27,28),(3,3,16,32,38,35,27,29),(3,3,17,32,39,36,28,30),(3,3,18,33,41,37,29,31),(3,3,19,33,42,38,29,31),(3,3,20,34,44,39,30,32),(3,3,21,34,45,40,31,33),(3,3,22,35,47,41,31,33),(3,3,23,35,48,42,32,34),(3,3,24,36,50,43,33,35),(3,3,25,36,51,44,33,36),(3,3,26,37,53,45,34,36),(3,3,27,37,55,46,35,37),(3,3,28,38,56,47,35,38),(3,3,29,38,58,48,36,39),(3,3,30,39,60,49,37,39),(3,3,31,39,61,51,38,40),(3,3,32,40,63,52,38,41),(3,3,33,41,65,53,39,42),(3,3,34,41,66,54,40,43),(3,3,35,42,68,55,41,44),(3,3,36,43,70,57,42,45),(3,3,37,43,72,58,42,45),(3,3,38,44,74,59,43,46),(3,3,39,44,76,60,44,47),(3,3,40,45,77,62,45,48),(3,3,41,46,79,63,46,49),(3,3,42,46,81,64,46,50),(3,3,43,47,83,65,47,51),(3,3,44,48,85,67,48,52),(3,3,45,48,87,68,49,53),(3,3,46,49,89,70,50,54),(3,3,47,50,91,71,51,55),(3,3,48,51,94,72,52,56),(3,3,49,51,96,74,53,57),(3,3,50,52,98,75,54,58),(3,3,51,53,100,77,55,59),(3,3,52,54,102,78,56,60),(3,3,53,54,104,80,57,61),(3,3,54,55,107,81,58,62),(3,3,55,56,109,83,59,63),(3,3,56,57,111,84,60,64),(3,3,57,58,114,86,61,66),(3,3,58,58,116,88,62,67),(3,3,59,59,119,89,63,68),(3,3,60,60,121,91,64,69),(3,3,61,61,123,93,65,70),(3,3,62,62,126,94,66,71),(3,3,63,63,129,96,67,73),(3,3,64,63,131,98,68,74),(3,3,65,64,134,100,70,75),(3,3,66,65,136,101,71,76),(3,3,67,66,139,103,72,78),(3,3,68,67,142,105,73,79),(3,3,69,68,145,107,74,80),(3,3,70,69,147,109,76,82),(3,3,71,70,150,111,77,83),(3,3,72,71,153,113,78,84),(3,3,73,72,156,115,79,86),(3,3,74,73,159,117,81,87),(3,3,75,74,162,119,82,89),(3,3,76,75,173,121,83,90),(3,3,77,76,176,123,85,92),(3,3,78,77,179,125,86,93),(3,3,79,78,182,127,87,95),(3,3,80,79,186,129,89,96),(3,4,1,23,19,24,19,19),(3,4,2,24,20,25,19,19),(3,4,3,24,22,25,19,20),(3,4,4,25,23,26,19,20),(3,4,5,26,25,27,20,20),(3,4,6,27,26,27,20,21),(3,4,7,28,28,28,20,21),(3,4,8,28,29,29,20,22),(3,4,9,29,31,30,20,22),(3,4,10,32,31,28,21,22),(3,4,11,33,32,29,21,23),(3,4,12,34,33,29,21,23),(3,4,13,35,35,30,21,23),(3,4,14,35,36,31,21,24),(3,4,15,36,38,31,22,24),(3,4,16,37,39,32,22,25),(3,4,17,38,40,33,22,25),(3,4,18,39,42,34,22,25),(3,4,19,40,44,34,22,26),(3,4,20,40,45,35,23,26),(3,4,21,41,47,36,23,27),(3,4,22,42,48,37,23,27),(3,4,23,43,50,38,23,28),(3,4,24,44,51,38,24,28),(3,4,25,45,53,39,24,29),(3,4,26,46,55,40,24,29),(3,4,27,47,56,41,24,29),(3,4,28,48,58,42,24,30),(3,4,29,48,60,43,25,30),(3,4,30,49,62,43,25,31),(3,4,31,50,63,44,25,31),(3,4,32,51,65,45,25,32),(3,4,33,52,67,46,26,32),(3,4,34,53,69,47,26,33),(3,4,35,54,71,48,26,33),(3,4,36,56,73,49,27,34),(3,4,37,57,74,50,27,35),(3,4,38,58,76,51,27,35),(3,4,39,59,78,52,27,36),(3,4,40,60,80,53,28,36),(3,4,41,61,82,54,28,37),(3,4,42,62,84,55,28,37),(3,4,43,63,86,56,28,38),(3,4,44,64,89,57,29,38),(3,4,45,66,91,58,29,39),(3,4,46,67,93,59,29,40),(3,4,47,68,95,60,30,40),(3,4,48,69,97,61,30,41),(3,4,49,70,99,63,30,42),(3,4,50,72,102,64,31,42),(3,4,51,73,109,65,31,43),(3,4,52,74,109,66,31,43),(3,4,53,75,114,67,32,44),(3,4,54,77,116,68,32,45),(3,4,55,78,118,70,32,45),(3,4,56,79,122,71,33,46),(3,4,57,81,124,72,33,47),(3,4,58,82,127,73,33,48),(3,4,59,84,129,75,34,48),(3,4,60,85,132,76,34,49),(3,4,61,86,135,77,34,50),(3,4,62,88,137,79,35,50),(3,4,63,89,137,80,35,51),(3,4,64,91,144,81,35,52),(3,4,65,92,146,83,36,53),(3,4,66,94,149,84,36,54),(3,4,67,95,152,86,37,54),(3,4,68,97,155,87,37,55),(3,4,69,99,158,88,37,56),(3,4,70,100,158,90,38,57),(3,4,71,102,165,91,38,58),(3,4,72,104,168,93,39,58),(3,4,73,105,171,95,39,59),(3,4,74,107,174,96,40,60),(3,4,75,109,177,98,40,61),(3,4,76,110,180,99,40,62),(3,4,77,112,184,101,41,63),(3,4,78,114,188,103,41,64),(3,4,79,116,191,104,42,65),(3,4,80,118,194,106,42,66),(3,5,1,22,16,23,21,22),(3,5,2,22,16,23,22,23),(3,5,3,22,16,24,24,25),(3,5,4,22,17,24,25,26),(3,5,5,23,17,24,26,27),(3,5,6,23,17,25,28,29),(3,5,7,23,17,25,29,30),(3,5,8,23,18,26,30,32),(3,5,9,23,18,26,32,33),(3,5,10,27,18,24,32,33),(3,5,11,27,18,25,33,34),(3,5,12,27,19,25,34,36),(3,5,13,27,19,25,35,37),(3,5,14,27,19,26,37,38),(3,5,15,28,19,26,38,40),(3,5,16,28,20,27,39,41),(3,5,17,28,20,27,41,42),(3,5,18,28,20,27,42,44),(3,5,19,28,21,28,43,45),(3,5,20,29,21,28,45,47),(3,5,21,29,21,29,46,48),(3,5,22,29,21,29,48,50),(3,5,23,29,22,30,49,51),(3,5,24,30,22,30,51,53),(3,5,25,30,22,31,52,54),(3,5,26,30,23,31,54,56),(3,5,27,30,23,31,55,58),(3,5,28,30,23,32,57,59),(3,5,29,31,24,32,58,61),(3,5,30,31,24,33,60,63),(3,5,31,31,24,33,62,64),(3,5,32,31,25,34,63,66),(3,5,33,32,25,34,65,68),(3,5,34,32,25,35,67,69),(3,5,35,32,26,35,68,71),(3,5,36,33,26,36,70,73),(3,5,37,33,26,37,72,75),(3,5,38,33,27,37,74,77),(3,5,39,33,27,38,75,79),(3,5,40,34,27,38,77,80),(3,5,41,34,28,39,79,82),(3,5,42,34,28,39,81,84),(3,5,43,34,29,40,83,86),(3,5,44,35,29,40,85,88),(3,5,45,35,29,41,87,90),(3,5,46,35,30,42,89,92),(3,5,47,36,30,42,91,94),(3,5,48,36,31,43,93,97),(3,5,49,36,31,44,95,99),(3,5,50,37,31,44,101,101),(3,5,51,37,32,45,104,103),(3,5,52,37,32,45,106,105),(3,5,53,38,33,46,108,107),(3,5,54,38,33,47,110,110),(3,5,55,38,34,47,113,112),(3,5,56,39,34,48,115,114),(3,5,57,39,35,49,117,117),(3,5,58,39,35,50,119,119),(3,5,59,40,36,50,122,122),(3,5,60,40,36,51,125,124),(3,5,61,40,36,52,127,126),(3,5,62,41,37,52,130,129),(3,5,63,41,37,53,132,132),(3,5,64,41,38,54,135,134),(3,5,65,42,39,55,137,137),(3,5,66,42,39,56,140,139),(3,5,67,43,40,56,142,142),(3,5,68,43,40,57,146,145),(3,5,69,43,41,58,149,148),(3,5,70,44,41,59,151,150),(3,5,71,44,42,60,154,153),(3,5,72,45,42,60,157,156),(3,5,73,45,43,61,160,159),(3,5,74,46,43,62,162,162),(3,5,75,46,44,63,165,165),(3,5,76,46,45,64,169,168),(3,5,77,47,45,65,172,171),(3,5,78,47,46,66,175,174),(3,5,79,48,46,67,178,177),(3,5,80,48,47,68,181,180),(3,6,1,25,16,25,19,19),(3,6,2,26,17,26,19,19),(3,6,3,28,18,27,19,20),(3,6,4,29,18,28,19,20),(3,6,5,30,19,30,19,20),(3,6,6,31,20,31,19,20),(3,6,7,33,21,32,20,21),(3,6,8,34,22,33,20,21),(3,6,9,36,23,35,20,21),(3,6,10,37,23,36,20,22),(3,6,11,38,24,37,20,22),(3,6,12,40,25,38,20,22),(3,6,13,41,26,40,20,23),(3,6,14,43,27,41,20,23),(3,6,15,44,28,42,20,23),(3,6,16,46,29,44,21,24),(3,6,17,47,30,45,21,24),(3,6,18,49,31,46,21,24),(3,6,19,50,32,48,21,25),(3,6,20,52,33,49,21,25),(3,6,21,53,34,51,21,26),(3,6,22,55,34,52,21,26),(3,6,23,56,35,53,21,26),(3,6,24,58,36,55,22,27),(3,6,25,59,37,56,22,27),(3,6,26,61,38,58,22,27),(3,6,27,63,39,59,22,28),(3,6,28,64,41,61,22,28),(3,6,29,66,42,62,22,29),(3,6,30,68,43,64,23,29),(3,6,31,69,44,65,23,30),(3,6,32,71,45,67,23,30),(3,6,33,73,46,68,23,30),(3,6,34,75,47,70,23,31),(3,6,35,76,48,72,24,31),(3,6,36,78,49,73,24,32),(3,6,37,80,50,75,24,32),(3,6,38,82,51,76,24,33),(3,6,39,84,52,78,24,33),(3,6,40,85,54,80,24,34),(3,6,41,87,55,81,25,34),(3,6,42,89,56,83,25,35),(3,6,43,91,57,85,25,35),(3,6,44,93,58,87,25,36),(3,6,45,95,59,88,26,36),(3,6,46,97,61,90,26,37),(3,6,47,99,62,92,26,37),(3,6,48,101,63,94,26,38),(3,6,49,102,64,95,26,38),(3,6,50,104,65,97,27,39),(3,6,51,106,67,99,27,39),(3,6,52,108,68,99,27,40),(3,6,53,109,68,101,27,40),(3,6,54,110,69,101,28,41),(3,6,55,113,69,100,28,41),(3,6,56,116,71,103,28,42),(3,6,57,118,72,105,28,42),(3,6,58,120,73,107,29,43),(3,6,59,123,75,109,29,43),(3,6,60,125,76,111,29,44),(3,6,61,127,77,113,29,45),(3,6,62,130,79,115,29,45),(3,6,63,132,80,118,30,46),(3,6,64,135,82,120,30,46),(3,6,65,138,83,122,30,47),(3,6,66,140,85,125,30,48),(3,6,67,143,86,127,31,48),(3,6,68,145,88,129,31,49),(3,6,69,148,89,132,31,50),(3,6,70,151,91,134,31,50),(3,6,71,154,93,137,32,51),(3,6,72,156,94,139,32,52),(3,6,73,159,96,142,32,53),(3,6,74,162,98,144,32,53),(3,6,75,165,99,147,33,54),(3,6,76,168,101,150,33,55),(3,6,77,171,103,152,33,56),(3,6,78,174,104,155,33,56),(3,6,79,177,106,158,34,57),(3,6,80,180,108,161,34,58),(4,1,1,20,25,21,20,20),(4,1,2,21,26,22,20,20),(4,1,3,23,27,23,20,21),(4,1,4,24,27,25,20,21),(4,1,5,25,28,26,20,21),(4,1,6,27,29,27,20,21),(4,1,7,28,30,28,21,22),(4,1,8,29,31,29,21,22),(4,1,9,31,31,31,21,22),(4,1,10,29,30,31,21,23),(4,1,11,31,31,33,21,23),(4,1,12,32,32,34,21,23),(4,1,13,33,33,35,21,24),(4,1,14,35,34,36,22,24),(4,1,15,36,34,37,22,24),(4,1,16,37,35,38,22,25),(4,1,17,38,36,40,22,25),(4,1,18,40,37,41,22,25),(4,1,19,41,38,42,22,26),(4,1,20,43,39,43,22,26),(4,1,21,44,39,45,23,26),(4,1,22,45,40,46,23,27),(4,1,23,47,41,47,23,27),(4,1,24,48,42,49,23,28),(4,1,25,50,43,50,23,28),(4,1,26,51,44,51,23,28),(4,1,27,53,45,53,23,29),(4,1,28,54,46,54,24,29),(4,1,29,56,47,56,24,30),(4,1,30,58,48,57,24,30),(4,1,31,59,49,58,24,30),(4,1,32,61,50,60,24,31),(4,1,33,62,51,61,24,31),(4,1,34,64,52,63,25,32),(4,1,35,66,53,64,25,32),(4,1,36,68,54,66,25,33),(4,1,37,69,55,68,25,33),(4,1,38,71,56,69,25,33),(4,1,39,73,57,71,26,34),(4,1,40,75,58,72,26,34),(4,1,41,76,60,74,26,35),(4,1,42,78,61,76,26,35),(4,1,43,80,62,77,26,36),(4,1,44,82,63,79,26,36),(4,1,45,84,64,81,27,37),(4,1,46,86,65,83,27,37),(4,1,47,88,67,84,27,38),(4,1,48,90,68,86,27,38),(4,1,49,92,69,88,28,39),(4,1,50,94,70,90,28,39),(4,1,51,96,72,92,28,40),(4,1,52,98,73,94,28,40),(4,1,53,100,74,96,28,41),(4,1,54,102,76,98,29,42),(4,1,55,105,77,100,29,42),(4,1,56,107,78,102,29,43),(4,1,57,109,80,104,29,43),(4,1,58,111,81,106,30,44),(4,1,59,114,83,108,30,44),(4,1,60,116,84,110,30,45),(4,1,61,118,85,112,30,46),(4,1,62,121,87,114,30,46),(4,1,63,123,88,117,31,47),(4,1,64,126,90,119,31,47),(4,1,65,128,92,121,31,48),(4,1,66,131,93,123,32,49),(4,1,67,133,95,126,32,49),(4,1,68,136,96,128,32,50),(4,1,69,138,98,130,32,51),(4,1,70,141,100,133,33,51),(4,1,71,144,101,135,33,52),(4,1,72,146,103,138,33,53),(4,1,73,149,105,140,33,54),(4,1,74,152,106,143,34,54),(4,1,75,155,108,145,34,55),(4,1,76,158,110,148,34,56),(4,1,77,161,112,151,35,57),(4,1,78,164,113,153,35,57),(4,1,79,167,115,156,35,58),(4,1,80,170,117,159,36,59),(4,3,1,17,28,20,20,21),(4,3,2,17,29,21,21,22),(4,3,3,18,31,22,21,22),(4,3,4,18,32,23,22,23),(4,3,5,19,33,24,22,24),(4,3,6,19,35,25,23,24),(4,3,7,20,36,26,24,25),(4,3,8,20,38,27,24,25),(4,3,9,21,39,27,25,26),(4,3,10,20,38,28,25,26),(4,3,11,20,39,29,25,27),(4,3,12,21,41,30,26,28),(4,3,13,21,42,31,27,28),(4,3,14,22,43,32,27,29),(4,3,15,22,45,33,28,29),(4,3,16,23,46,34,28,30),(4,3,17,23,47,35,29,31),(4,3,18,24,49,36,30,32),(4,3,19,24,50,37,30,32),(4,3,20,25,52,38,31,33),(4,3,21,25,53,39,32,34),(4,3,22,26,55,40,32,34),(4,3,23,26,56,41,33,35),(4,3,24,27,58,42,34,36),(4,3,25,27,59,43,34,37),(4,3,26,28,61,44,35,37),(4,3,27,28,63,45,36,38),(4,3,28,29,64,46,36,39),(4,3,29,29,66,47,37,40),(4,3,30,30,68,48,38,40),(4,3,31,30,69,50,39,41),(4,3,32,31,71,51,39,42),(4,3,33,32,73,52,40,43),(4,3,34,32,74,53,41,44),(4,3,35,33,76,54,42,45),(4,3,36,34,78,56,43,46),(4,3,37,34,80,57,43,46),(4,3,38,35,82,58,44,47),(4,3,39,35,84,59,45,48),(4,3,40,36,85,61,46,49),(4,3,41,37,87,62,47,50),(4,3,42,37,89,63,47,51),(4,3,43,38,91,64,48,52),(4,3,44,39,93,66,49,53),(4,3,45,39,95,67,50,54),(4,3,46,40,97,69,51,55),(4,3,47,41,99,70,52,56),(4,3,48,42,102,71,53,57),(4,3,49,42,104,73,54,58),(4,3,50,43,106,74,55,59),(4,3,51,44,108,76,56,60),(4,3,52,45,110,77,57,61),(4,3,53,45,112,79,58,62),(4,3,54,46,115,80,59,63),(4,3,55,47,117,82,60,64),(4,3,56,48,119,83,61,65),(4,3,57,49,122,85,62,67),(4,3,58,49,124,87,63,68),(4,3,59,50,127,88,64,69),(4,3,60,51,129,90,65,70),(4,3,61,52,131,92,66,71),(4,3,62,53,134,93,67,72),(4,3,63,54,137,95,68,74),(4,3,64,54,139,97,69,75),(4,3,65,55,142,99,71,76),(4,3,66,56,144,100,72,77),(4,3,67,57,147,102,73,79),(4,3,68,58,150,104,74,80),(4,3,69,59,153,106,75,81),(4,3,70,60,155,108,77,83),(4,3,71,61,158,110,78,84),(4,3,72,62,161,112,79,85),(4,3,73,63,164,114,80,87),(4,3,74,64,167,116,82,88),(4,3,75,65,170,118,83,90),(4,3,76,66,181,120,84,91),(4,3,77,67,184,122,86,93),(4,3,78,68,187,124,87,94),(4,3,79,69,190,126,88,96),(4,3,80,70,194,128,90,97),(4,4,1,18,28,20,20,20),(4,4,2,19,29,21,20,20),(4,4,3,20,31,21,20,21),(4,4,4,20,32,22,20,21),(4,4,5,21,34,23,21,21),(4,4,6,22,35,24,21,22),(4,4,7,23,37,24,21,22),(4,4,8,24,38,25,21,23),(4,4,9,24,40,26,21,23),(4,4,10,23,39,27,22,23),(4,4,11,24,40,28,22,24),(4,4,12,25,41,28,22,24),(4,4,13,26,43,29,22,24),(4,4,14,26,44,30,22,25),(4,4,15,27,46,30,23,25),(4,4,16,28,47,31,23,26),(4,4,17,29,48,32,23,26),(4,4,18,30,50,33,23,26),(4,4,19,31,52,33,23,27),(4,4,20,31,53,34,24,27),(4,4,21,32,55,35,24,28),(4,4,22,33,56,36,24,28),(4,4,23,34,58,37,24,29),(4,4,24,35,59,37,25,29),(4,4,25,36,61,38,25,30),(4,4,26,37,63,39,25,30),(4,4,27,38,64,40,25,30),(4,4,28,39,66,41,25,31),(4,4,29,39,68,42,26,31),(4,4,30,40,70,42,26,32),(4,4,31,41,71,43,26,32),(4,4,32,42,73,44,26,33),(4,4,33,43,75,45,27,33),(4,4,34,44,77,46,27,34),(4,4,35,45,79,47,27,34),(4,4,36,47,81,48,28,35),(4,4,37,48,82,49,28,36),(4,4,38,49,84,50,28,36),(4,4,39,50,86,51,28,37),(4,4,40,51,88,52,29,37),(4,4,41,52,90,53,29,38),(4,4,42,53,92,54,29,38),(4,4,43,54,94,55,29,39),(4,4,44,55,97,56,30,39),(4,4,45,57,99,57,30,40),(4,4,46,58,101,58,30,41),(4,4,47,59,103,59,31,41),(4,4,48,60,105,60,31,42),(4,4,49,61,107,62,31,43),(4,4,50,63,110,63,32,43),(4,4,51,64,117,64,32,44),(4,4,52,65,117,65,32,44),(4,4,53,66,122,66,33,45),(4,4,54,68,124,67,33,46),(4,4,55,69,126,69,33,46),(4,4,56,70,130,70,34,47),(4,4,57,72,132,71,34,48),(4,4,58,73,135,72,34,49),(4,4,59,75,137,74,35,49),(4,4,60,76,140,75,35,50),(4,4,61,77,143,76,35,51),(4,4,62,79,145,78,36,51),(4,4,63,80,145,79,36,52),(4,4,64,82,152,80,36,53),(4,4,65,83,154,82,37,54),(4,4,66,85,157,83,37,55),(4,4,67,86,160,85,38,55),(4,4,68,88,163,86,38,56),(4,4,69,90,166,87,38,57),(4,4,70,91,166,89,39,58),(4,4,71,93,173,90,39,59),(4,4,72,95,176,92,40,59),(4,4,73,96,179,94,40,60),(4,4,74,98,182,95,41,61),(4,4,75,100,185,97,41,62),(4,4,76,101,188,98,41,63),(4,4,77,103,192,100,42,64),(4,4,78,105,196,102,42,65),(4,4,79,107,199,103,43,66),(4,4,80,109,202,105,43,67),(4,5,1,17,25,19,22,23),(4,5,2,17,25,19,23,24),(4,5,3,17,25,20,25,26),(4,5,4,17,26,20,26,27),(4,5,5,18,26,20,27,28),(4,5,6,18,26,21,29,30),(4,5,7,18,26,21,30,31),(4,5,8,18,26,22,31,33),(4,5,9,18,27,22,33,34),(4,5,10,18,26,23,33,34),(4,5,11,18,26,24,34,35),(4,5,12,18,27,24,35,37),(4,5,13,18,27,24,36,38),(4,5,14,18,27,25,38,39),(4,5,15,19,27,25,39,41),(4,5,16,19,28,26,40,42),(4,5,17,19,28,26,42,43),(4,5,18,19,28,26,43,45),(4,5,19,19,29,27,44,46),(4,5,20,20,29,27,46,48),(4,5,21,20,29,28,47,49),(4,5,22,20,29,28,49,51),(4,5,23,20,30,29,50,52),(4,5,24,21,30,29,52,54),(4,5,25,21,30,30,53,55),(4,5,26,21,31,30,55,57),(4,5,27,21,31,30,56,59),(4,5,28,21,31,31,58,60),(4,5,29,22,32,31,59,62),(4,5,30,22,32,32,61,64),(4,5,31,22,32,32,63,65),(4,5,32,22,33,33,64,67),(4,5,33,23,33,33,66,69),(4,5,34,23,33,34,68,70),(4,5,35,23,34,34,69,72),(4,5,36,24,34,35,71,74),(4,5,37,24,34,36,73,76),(4,5,38,24,35,36,75,78),(4,5,39,24,35,37,76,80),(4,5,40,25,35,37,78,81),(4,5,41,25,36,38,80,83),(4,5,42,25,36,38,82,85),(4,5,43,25,37,39,84,87),(4,5,44,26,37,39,86,89),(4,5,45,26,37,40,88,91),(4,5,46,26,38,41,90,93),(4,5,47,27,38,41,92,95),(4,5,48,27,39,42,94,98),(4,5,49,27,39,43,96,100),(4,5,50,28,39,43,102,102),(4,5,51,28,40,44,105,104),(4,5,52,28,40,44,107,106),(4,5,53,29,41,45,109,108),(4,5,54,29,41,46,111,111),(4,5,55,29,42,46,114,113),(4,5,56,30,42,47,116,115),(4,5,57,30,43,48,118,118),(4,5,58,30,43,49,120,120),(4,5,59,31,44,49,123,123),(4,5,60,31,44,50,126,125),(4,5,61,31,44,51,128,127),(4,5,62,32,45,51,131,130),(4,5,63,32,45,52,133,133),(4,5,64,32,46,53,136,135),(4,5,65,33,47,54,138,138),(4,5,66,33,47,55,141,140),(4,5,67,34,48,55,143,143),(4,5,68,34,48,56,147,146),(4,5,69,34,49,57,150,149),(4,5,70,35,49,58,152,151),(4,5,71,35,50,59,155,154),(4,5,72,36,50,59,158,157),(4,5,73,36,51,60,161,160),(4,5,74,37,51,61,163,163),(4,5,75,37,52,62,166,166),(4,5,76,37,53,63,170,169),(4,5,77,38,53,64,173,172),(4,5,78,38,54,65,176,175),(4,5,79,39,54,66,179,178),(4,5,80,39,55,67,182,181),(4,6,1,20,25,21,20,20),(4,6,2,21,26,22,20,20),(4,6,3,23,27,23,20,21),(4,6,4,24,27,25,20,21),(4,6,5,25,28,26,20,21),(4,6,6,27,29,27,20,21),(4,6,7,28,30,28,21,22),(4,6,8,29,31,29,21,22),(4,6,9,31,31,31,21,22),(4,6,10,32,32,32,21,23),(4,6,11,33,33,33,21,23),(4,6,12,35,34,34,21,23),(4,6,13,36,35,36,21,24),(4,6,14,38,36,37,21,24),(4,6,15,39,37,38,21,24),(4,6,16,41,37,40,21,25),(4,6,17,42,38,41,22,25),(4,6,18,44,39,43,22,25),(4,6,19,45,40,44,22,26),(4,6,20,47,41,45,22,26),(4,6,21,48,42,47,22,26),(4,6,22,50,43,48,22,27),(4,6,23,52,44,50,22,27),(4,6,24,53,45,51,23,28),(4,6,25,55,46,52,23,28),(4,6,26,56,47,54,23,28),(4,6,27,58,48,55,23,29),(4,6,28,60,49,57,23,29),(4,6,29,61,50,58,23,30),(4,6,30,63,51,60,24,30),(4,6,31,65,52,62,24,30),(4,6,32,66,53,63,24,31),(4,6,33,68,54,65,24,31),(4,6,34,70,55,66,24,32),(4,6,35,72,56,68,24,32),(4,6,36,73,58,69,25,33),(4,6,37,75,59,71,25,33),(4,6,38,77,60,73,25,34),(4,6,39,79,61,74,25,34),(4,6,40,81,62,76,25,35),(4,6,41,82,63,78,26,35),(4,6,42,84,64,79,26,35),(4,6,43,86,66,81,26,36),(4,6,44,88,67,83,26,36),(4,6,45,90,68,85,26,37),(4,6,46,92,69,86,27,37),(4,6,47,94,70,88,27,38),(4,6,48,96,72,90,27,38),(4,6,49,98,73,92,27,39),(4,6,50,100,74,93,28,40),(4,6,51,101,75,94,28,40),(4,6,52,102,76,95,28,41),(4,6,53,103,77,96,28,41),(4,6,54,104,77,97,29,42),(4,6,55,104,77,99,29,42),(4,6,56,107,79,102,29,43),(4,6,57,109,80,104,29,43),(4,6,58,111,81,106,30,44),(4,6,59,114,83,108,30,44),(4,6,60,116,84,110,30,45),(4,6,61,118,85,112,30,46),(4,6,62,121,87,114,30,46),(4,6,63,123,88,117,31,47),(4,6,64,126,90,119,31,47),(4,6,65,129,91,121,31,48),(4,6,66,131,93,124,31,49),(4,6,67,134,94,126,32,49),(4,6,68,136,96,128,32,50),(4,6,69,139,97,131,32,51),(4,6,70,142,99,133,32,51),(4,6,71,145,101,136,33,52),(4,6,72,147,102,138,33,53),(4,6,73,150,104,141,33,54),(4,6,74,153,106,143,33,54),(4,6,75,156,107,146,34,55),(4,6,76,159,109,149,34,56),(4,6,77,162,111,151,34,57),(4,6,78,165,112,154,34,57),(4,6,79,168,114,157,35,58),(4,6,80,171,116,160,35,59),(4,11,1,18,25,19,22,22),(4,11,2,19,25,20,23,23),(4,11,3,19,26,20,24,24),(4,11,4,20,26,21,25,26),(4,11,5,20,27,22,26,27),(4,11,6,21,27,22,27,28),(4,11,7,21,28,23,28,29),(4,11,8,22,28,24,29,30),(4,11,9,23,29,24,30,32),(4,11,10,22,28,25,30,31),(4,11,11,22,29,26,31,33),(4,11,12,23,29,27,32,34),(4,11,13,23,30,27,33,35),(4,11,14,24,30,28,34,36),(4,11,15,25,31,29,36,37),(4,11,16,25,31,29,37,38),(4,11,17,26,32,30,38,40),(4,11,18,26,33,31,39,41),(4,11,19,27,33,31,40,42),(4,11,20,28,34,32,41,43),(4,11,21,28,34,33,42,45),(4,11,22,29,35,34,43,46),(4,11,23,30,36,34,44,47),(4,11,24,30,36,35,46,49),(4,11,25,31,37,36,47,50),(4,11,26,32,37,37,48,51),(4,11,27,32,38,37,49,53),(4,11,28,33,39,38,50,54),(4,11,29,34,39,39,52,56),(4,11,30,34,40,40,53,57),(4,11,31,35,41,41,54,58),(4,11,32,36,41,42,56,60),(4,11,33,37,42,42,57,61),(4,11,34,37,43,43,58,63),(4,11,35,38,43,44,60,64),(4,11,36,39,44,45,61,66),(4,11,37,40,45,46,62,68),(4,11,38,41,45,47,64,69),(4,11,39,41,46,48,65,71),(4,11,40,42,47,49,67,72),(4,11,41,43,48,50,68,74),(4,11,42,44,48,51,70,76),(4,11,43,45,49,52,71,77),(4,11,44,46,50,52,73,79),(4,11,45,46,51,53,74,81),(4,11,46,47,52,54,76,83),(4,11,47,48,52,56,77,84),(4,11,48,49,53,57,79,86),(4,11,49,50,54,58,81,88),(4,11,50,51,55,59,82,90),(4,11,51,52,56,60,84,92),(4,11,52,53,57,61,86,94),(4,11,53,54,58,62,87,96),(4,11,54,55,58,63,89,98),(4,11,55,56,59,64,91,100),(4,11,56,57,60,65,93,102),(4,11,57,58,61,66,94,104),(4,11,58,59,62,68,96,106),(4,11,59,60,63,69,98,108),(4,11,60,61,64,70,100,110),(4,11,61,62,65,71,102,112),(4,11,62,63,66,72,104,114),(4,11,63,64,67,74,106,117),(4,11,64,65,68,75,108,119),(4,11,65,67,69,76,110,121),(4,11,66,68,70,78,112,123),(4,11,67,69,71,79,114,126),(4,11,68,70,72,80,116,128),(4,11,69,71,73,82,118,130),(4,11,70,72,74,83,120,133),(4,11,71,74,75,84,122,135),(4,11,72,75,77,86,125,138),(4,11,73,76,78,87,127,140),(4,11,74,77,79,89,129,143),(4,11,75,79,80,90,131,145),(4,11,76,80,81,92,134,148),(4,11,77,81,82,93,136,151),(4,11,78,83,84,95,138,153),(4,11,79,84,85,96,141,156),(4,11,80,85,86,98,143,159),(5,1,1,22,18,23,18,25),(5,1,2,23,19,24,18,25),(5,1,3,25,20,25,18,26),(5,1,4,26,20,26,18,26),(5,1,5,27,21,28,18,26),(5,1,6,29,22,29,18,26),(5,1,7,30,23,30,19,27),(5,1,8,31,24,31,19,27),(5,1,9,33,25,33,19,27),(5,1,10,32,24,31,19,28),(5,1,11,34,25,33,19,28),(5,1,12,35,26,34,19,28),(5,1,13,36,27,35,19,29),(5,1,14,38,28,36,20,29),(5,1,15,39,28,37,20,29),(5,1,16,40,29,38,20,30),(5,1,17,41,30,40,20,30),(5,1,18,43,31,41,20,30),(5,1,19,44,32,42,20,31),(5,1,20,46,33,43,20,31),(5,1,21,47,33,45,21,31),(5,1,22,48,34,46,21,32),(5,1,23,50,35,47,21,32),(5,1,24,51,36,49,21,33),(5,1,25,53,37,50,21,33),(5,1,26,54,38,51,21,33),(5,1,27,56,39,53,21,34),(5,1,28,57,40,54,22,34),(5,1,29,59,41,56,22,35),(5,1,30,61,42,57,22,35),(5,1,31,62,43,58,22,35),(5,1,32,64,44,60,22,36),(5,1,33,65,45,61,22,36),(5,1,34,67,46,63,23,37),(5,1,35,69,47,64,23,37),(5,1,36,71,48,66,23,38),(5,1,37,72,49,68,23,38),(5,1,38,74,50,69,23,38),(5,1,39,76,51,71,24,39),(5,1,40,78,52,72,24,39),(5,1,41,79,54,74,24,40),(5,1,42,81,55,76,24,40),(5,1,43,83,56,77,24,41),(5,1,44,85,57,79,24,41),(5,1,45,87,58,81,25,42),(5,1,46,89,59,83,25,42),(5,1,47,91,61,84,25,43),(5,1,48,93,62,86,25,43),(5,1,49,95,63,88,26,44),(5,1,50,97,64,90,26,44),(5,1,51,99,66,92,26,45),(5,1,52,101,67,94,26,45),(5,1,53,103,68,96,26,46),(5,1,54,105,70,98,27,47),(5,1,55,108,71,100,27,47),(5,1,56,110,72,102,27,48),(5,1,57,112,74,104,27,48),(5,1,58,114,75,106,28,49),(5,1,59,117,77,108,28,49),(5,1,60,119,78,110,28,50),(5,1,61,121,79,112,28,51),(5,1,62,124,81,114,28,51),(5,1,63,126,82,117,29,52),(5,1,64,129,84,119,29,52),(5,1,65,131,86,121,29,53),(5,1,66,134,87,123,30,54),(5,1,67,136,89,126,30,54),(5,1,68,139,90,128,30,55),(5,1,69,141,92,130,30,56),(5,1,70,144,94,133,31,56),(5,1,71,147,95,135,31,57),(5,1,72,149,97,138,31,58),(5,1,73,152,99,140,31,59),(5,1,74,155,100,143,32,59),(5,1,75,158,102,145,32,60),(5,1,76,161,104,148,32,61),(5,1,77,164,106,151,33,62),(5,1,78,167,107,153,33,62),(5,1,79,170,109,156,33,63),(5,1,80,173,111,159,34,64),(5,4,1,20,21,22,18,25),(5,4,2,21,22,23,18,25),(5,4,3,22,24,23,18,26),(5,4,4,22,25,24,18,26),(5,4,5,23,27,25,19,26),(5,4,6,24,28,25,19,27),(5,4,7,25,30,26,19,27),(5,4,8,25,31,27,19,27),(5,4,9,26,33,28,19,28),(5,4,10,26,33,27,20,28),(5,4,11,27,34,28,20,29),(5,4,12,28,35,28,20,29),(5,4,13,29,37,29,20,29),(5,4,14,29,38,30,20,30),(5,4,15,30,40,30,21,30),(5,4,16,31,41,31,21,31),(5,4,17,32,42,32,21,31),(5,4,18,33,44,33,21,31),(5,4,19,34,46,33,21,32),(5,4,20,34,47,34,22,32),(5,4,21,35,49,35,22,33),(5,4,22,36,50,36,22,33),(5,4,23,37,52,37,22,34),(5,4,24,38,53,37,23,34),(5,4,25,39,55,38,23,35),(5,4,26,40,57,39,23,35),(5,4,27,41,58,40,23,35),(5,4,28,42,60,41,23,36),(5,4,29,42,62,42,24,36),(5,4,30,43,64,42,24,37),(5,4,31,44,65,43,24,37),(5,4,32,45,67,44,24,38),(5,4,33,46,69,45,25,38),(5,4,34,47,71,46,25,39),(5,4,35,48,73,47,25,39),(5,4,36,50,75,48,26,40),(5,4,37,51,76,49,26,41),(5,4,38,52,78,50,26,41),(5,4,39,53,80,51,26,42),(5,4,40,54,82,52,27,42),(5,4,41,55,84,53,27,43),(5,4,42,56,86,54,27,43),(5,4,43,57,88,55,27,44),(5,4,44,58,91,56,28,44),(5,4,45,60,93,57,28,45),(5,4,46,61,95,58,28,46),(5,4,47,62,97,59,29,46),(5,4,48,63,99,60,29,47),(5,4,49,64,101,62,29,48),(5,4,50,66,104,63,30,48),(5,4,51,67,111,64,30,49),(5,4,52,68,111,65,30,49),(5,4,53,69,116,66,31,50),(5,4,54,71,118,67,31,51),(5,4,55,72,120,69,31,51),(5,4,56,73,124,70,32,52),(5,4,57,75,126,71,32,53),(5,4,58,76,129,72,32,54),(5,4,59,78,131,74,33,54),(5,4,60,79,134,75,33,55),(5,4,61,80,137,76,33,56),(5,4,62,82,139,78,34,56),(5,4,63,83,139,79,34,57),(5,4,64,85,146,80,34,58),(5,4,65,86,148,82,35,59),(5,4,66,88,151,83,35,60),(5,4,67,89,154,85,36,60),(5,4,68,91,157,86,36,61),(5,4,69,93,160,87,36,62),(5,4,70,94,160,89,37,63),(5,4,71,96,167,90,37,64),(5,4,72,98,170,92,38,64),(5,4,73,99,173,94,38,65),(5,4,74,101,176,95,39,66),(5,4,75,103,179,97,39,67),(5,4,76,104,182,98,39,68),(5,4,77,106,186,100,40,69),(5,4,78,108,190,102,40,70),(5,4,79,110,193,103,41,71),(5,4,80,112,196,105,41,72),(5,5,1,19,18,21,20,28),(5,5,2,19,18,21,21,29),(5,5,3,19,18,22,23,31),(5,5,4,19,19,22,24,32),(5,5,5,20,19,22,25,33),(5,5,6,20,19,23,27,35),(5,5,7,20,19,23,28,36),(5,5,8,20,20,24,29,38),(5,5,9,20,20,24,31,39),(5,5,10,21,20,23,31,39),(5,5,11,21,20,24,32,40),(5,5,12,21,21,24,33,42),(5,5,13,21,21,24,34,43),(5,5,14,21,21,25,36,44),(5,5,15,22,21,25,37,46),(5,5,16,22,22,26,38,47),(5,5,17,22,22,26,40,48),(5,5,18,22,22,26,41,50),(5,5,19,22,23,27,42,51),(5,5,20,23,23,27,44,53),(5,5,21,23,23,28,45,54),(5,5,22,23,23,28,47,56),(5,5,23,23,24,29,48,57),(5,5,24,24,24,29,50,59),(5,5,25,24,24,30,51,60),(5,5,26,24,25,30,53,62),(5,5,27,24,25,30,54,64),(5,5,28,24,25,31,56,65),(5,5,29,25,26,31,57,67),(5,5,30,25,26,32,59,69),(5,5,31,25,26,32,61,70),(5,5,32,25,27,33,62,72),(5,5,33,26,27,33,64,74),(5,5,34,26,27,34,66,75),(5,5,35,26,28,34,67,77),(5,5,36,27,28,35,69,79),(5,5,37,27,28,36,71,81),(5,5,38,27,29,36,73,83),(5,5,39,27,29,37,74,85),(5,5,40,28,29,37,76,86),(5,5,41,28,30,38,78,88),(5,5,42,28,30,38,80,90),(5,5,43,28,31,39,82,92),(5,5,44,29,31,39,84,94),(5,5,45,29,31,40,86,96),(5,5,46,29,32,41,88,98),(5,5,47,30,32,41,90,100),(5,5,48,30,33,42,92,103),(5,5,49,30,33,43,94,105),(5,5,50,31,33,43,100,107),(5,5,51,31,34,44,103,109),(5,5,52,31,34,44,105,111),(5,5,53,32,35,45,107,113),(5,5,54,32,35,46,109,116),(5,5,55,32,36,46,112,118),(5,5,56,33,36,47,114,120),(5,5,57,33,37,48,116,123),(5,5,58,33,37,49,118,125),(5,5,59,34,38,49,121,128),(5,5,60,34,38,50,124,130),(5,5,61,34,38,51,126,132),(5,5,62,35,39,51,129,135),(5,5,63,35,39,52,131,138),(5,5,64,35,40,53,134,140),(5,5,65,36,41,54,136,143),(5,5,66,36,41,55,139,145),(5,5,67,37,42,55,141,148),(5,5,68,37,42,56,145,151),(5,5,69,37,43,57,148,154),(5,5,70,38,43,58,150,156),(5,5,71,38,44,59,153,159),(5,5,72,39,44,59,156,162),(5,5,73,39,45,60,159,165),(5,5,74,40,45,61,161,168),(5,5,75,40,46,62,164,171),(5,5,76,40,47,63,168,174),(5,5,77,41,47,64,171,177),(5,5,78,41,48,65,174,180),(5,5,79,42,48,66,177,183),(5,5,80,42,49,67,180,186),(5,6,1,22,18,23,18,25),(5,6,2,23,19,24,18,25),(5,6,3,25,20,25,18,26),(5,6,4,26,20,26,18,26),(5,6,5,27,21,28,18,26),(5,6,6,29,22,29,18,26),(5,6,7,30,23,30,19,27),(5,6,8,31,24,31,19,27),(5,6,9,33,25,33,19,27),(5,6,10,34,25,34,19,28),(5,6,11,35,26,35,19,28),(5,6,12,37,27,36,19,28),(5,6,13,38,28,38,19,28),(5,6,14,40,29,39,19,29),(5,6,15,41,30,40,19,29),(5,6,16,43,31,42,20,29),(5,6,17,44,32,43,20,30),(5,6,18,46,33,44,20,30),(5,6,19,47,34,46,20,31),(5,6,20,49,34,47,20,31),(5,6,21,50,35,49,20,31),(5,6,22,52,36,50,20,32),(5,6,23,53,37,51,21,32),(5,6,24,55,38,53,21,32),(5,6,25,57,39,54,21,33),(5,6,26,58,40,56,21,33),(5,6,27,60,41,57,21,34),(5,6,28,62,42,59,21,34),(5,6,29,63,43,60,21,34),(5,6,30,65,44,62,22,35),(5,6,31,67,46,63,22,35),(5,6,32,68,47,65,22,36),(5,6,33,70,48,67,22,36),(5,6,34,72,49,68,22,36),(5,6,35,74,50,70,23,37),(5,6,36,75,51,71,23,37),(5,6,37,77,52,73,23,38),(5,6,38,79,53,75,23,38),(5,6,39,81,54,76,23,39),(5,6,40,83,55,78,24,39),(5,6,41,84,57,80,24,40),(5,6,42,86,58,81,24,40),(5,6,43,88,59,83,24,41),(5,6,44,90,60,85,24,41),(5,6,45,92,61,86,25,42),(5,6,46,94,62,88,25,42),(5,6,47,96,64,90,25,43),(5,6,48,98,65,92,25,43),(5,6,49,100,66,93,25,44),(5,6,50,102,67,95,26,44),(5,6,51,103,67,96,26,45),(5,6,52,104,68,97,26,45),(5,6,53,105,69,98,26,46),(5,6,54,106,70,99,27,46),(5,6,55,107,71,99,27,47),(5,6,56,110,73,102,27,48),(5,6,57,112,74,104,27,48),(5,6,58,114,75,106,28,49),(5,6,59,117,77,108,28,49),(5,6,60,119,78,110,28,50),(5,6,61,121,79,112,28,51),(5,6,62,124,81,114,28,51),(5,6,63,126,82,117,29,52),(5,6,64,129,84,119,29,52),(5,6,65,132,85,121,29,53),(5,6,66,134,87,124,29,54),(5,6,67,137,88,126,30,54),(5,6,68,139,90,128,30,55),(5,6,69,142,91,131,30,56),(5,6,70,145,93,133,30,56),(5,6,71,148,95,136,31,57),(5,6,72,150,96,138,31,58),(5,6,73,153,98,141,31,59),(5,6,74,156,100,143,31,59),(5,6,75,159,101,146,32,60),(5,6,76,162,103,149,32,61),(5,6,77,165,105,151,32,62),(5,6,78,168,106,154,32,62),(5,6,79,171,108,157,33,63),(5,6,80,174,110,160,33,64),(5,8,1,19,18,21,21,27),(5,8,2,19,18,21,22,28),(5,8,3,19,18,22,24,30),(5,8,4,19,18,22,25,31),(5,8,5,19,19,22,26,32),(5,8,6,19,19,22,28,34),(5,8,7,20,19,23,29,35),(5,8,8,20,19,23,31,36),(5,8,9,20,19,23,32,38),(5,8,10,20,20,23,32,38),(5,8,11,20,20,23,33,39),(5,8,12,20,20,23,35,40),(5,8,13,20,20,24,36,41),(5,8,14,21,20,24,37,43),(5,8,15,21,21,24,39,44),(5,8,16,21,21,25,40,45),(5,8,17,21,21,25,41,47),(5,8,18,21,21,25,43,48),(5,8,19,21,21,26,44,49),(5,8,20,21,22,26,46,51),(5,8,21,22,22,26,47,52),(5,8,22,22,22,27,49,54),(5,8,23,22,22,27,50,55),(5,8,24,22,23,28,52,57),(5,8,25,22,23,28,53,58),(5,8,26,22,23,28,55,60),(5,8,27,22,23,29,57,61),(5,8,28,23,23,29,58,63),(5,8,29,23,24,30,60,64),(5,8,30,23,24,30,62,66),(5,8,31,23,24,30,63,68),(5,8,32,23,24,31,65,69),(5,8,33,23,25,31,67,71),(5,8,34,24,25,32,68,73),(5,8,35,24,25,32,70,74),(5,8,36,24,26,33,72,76),(5,8,37,24,26,33,74,78),(5,8,38,24,26,33,76,80),(5,8,39,25,26,34,78,81),(5,8,40,25,27,34,79,83),(5,8,41,25,27,35,81,85),(5,8,42,25,27,35,83,87),(5,8,43,25,27,36,85,89),(5,8,44,25,28,36,87,91),(5,8,45,26,28,37,89,93),(5,8,46,26,28,37,91,95),(5,8,47,26,29,38,93,97),(5,8,48,26,29,38,96,99),(5,8,49,27,29,39,98,101),(5,8,50,27,30,39,105,103),(5,8,51,27,30,40,107,105),(5,8,52,27,30,40,109,107),(5,8,53,27,31,41,111,109),(5,8,54,28,31,42,114,111),(5,8,55,28,31,42,116,114),(5,8,56,28,32,43,118,116),(5,8,57,28,32,43,121,118),(5,8,58,29,32,44,124,120),(5,8,59,29,33,44,127,123),(5,8,60,29,33,45,129,125),(5,8,61,29,33,46,131,127),(5,8,62,29,34,46,134,130),(5,8,63,30,34,47,137,132),(5,8,64,30,34,47,139,135),(5,8,65,30,35,48,142,137),(5,8,66,31,35,49,145,140),(5,8,67,31,36,49,148,142),(5,8,68,31,36,50,151,145),(5,8,69,31,36,51,154,148),(5,8,70,32,37,51,156,150),(5,8,71,32,37,52,159,153),(5,8,72,32,38,53,162,156),(5,8,73,32,38,54,166,159),(5,8,74,33,39,54,169,161),(5,8,75,33,39,55,172,164),(5,8,76,33,39,56,175,167),(5,8,77,34,40,57,178,170),(5,8,78,34,40,57,181,173),(5,8,79,34,41,58,184,176),(5,8,80,35,41,59,188,179),(5,9,1,19,18,22,20,27),(5,9,2,19,18,23,21,28),(5,9,3,20,19,23,22,29),(5,9,4,20,19,24,24,30),(5,9,5,20,19,24,25,32),(5,9,6,20,20,25,26,33),(5,9,7,21,20,25,27,34),(5,9,8,21,21,26,28,35),(5,9,9,21,21,26,30,36),(5,9,10,22,21,26,29,37),(5,9,11,22,22,26,31,38),(5,9,12,22,22,27,32,39),(5,9,13,23,22,27,33,41),(5,9,14,23,23,28,34,42),(5,9,15,23,23,29,35,43),(5,9,16,24,24,29,36,44),(5,9,17,24,24,30,38,46),(5,9,18,24,24,30,39,47),(5,9,19,25,25,31,40,48),(5,9,20,25,25,32,41,50),(5,9,21,25,26,32,43,51),(5,9,22,26,26,33,44,52),(5,9,23,26,27,34,45,54),(5,9,24,27,27,34,47,55),(5,9,25,27,28,35,48,57),(5,9,26,27,28,36,49,58),(5,9,27,28,28,36,51,59),(5,9,28,28,29,37,52,61),(5,9,29,29,29,38,54,62),(5,9,30,29,30,38,55,64),(5,9,31,29,30,39,56,66),(5,9,32,30,31,40,58,67),(5,9,33,30,31,41,59,69),(5,9,34,31,32,41,61,70),(5,9,35,31,32,42,62,72),(5,9,36,32,33,43,64,74),(5,9,37,32,34,44,66,75),(5,9,38,32,34,45,67,77),(5,9,39,33,35,45,69,79),(5,9,40,33,35,46,70,80),(5,9,41,34,36,47,72,82),(5,9,42,34,36,48,74,84),(5,9,43,35,37,49,75,86),(5,9,44,35,37,50,77,87),(5,9,45,36,38,50,79,89),(5,9,46,36,39,51,81,91),(5,9,47,37,39,52,82,93),(5,9,48,37,40,53,84,95),(5,9,49,38,41,54,86,97),(5,9,50,38,41,55,92,99),(5,9,51,39,42,56,94,101),(5,9,52,39,42,57,96,103),(5,9,53,40,43,58,98,105),(5,9,54,41,44,59,100,107),(5,9,55,41,44,60,103,109),(5,9,56,42,45,61,105,111),(5,9,57,42,46,62,107,113),(5,9,58,43,47,63,109,116),(5,9,59,43,47,64,111,118),(5,9,60,44,48,65,113,120),(5,9,61,45,49,66,115,122),(5,9,62,45,49,67,117,125),(5,9,63,46,50,68,120,127),(5,9,64,46,51,69,122,129),(5,9,65,47,52,71,125,132),(5,9,66,48,53,72,127,134),(5,9,67,48,53,73,130,137),(5,9,68,49,54,74,132,139),(5,9,69,50,55,75,134,141),(5,9,70,50,56,76,137,144),(5,9,71,51,57,78,139,147),(5,9,72,52,57,79,142,149),(5,9,73,53,58,80,145,152),(5,9,74,53,59,81,148,155),(5,9,75,54,60,83,150,157),(5,9,76,55,61,84,153,160),(5,9,77,56,62,85,156,163),(5,9,78,56,63,87,158,166),(5,9,79,57,64,88,161,169),(5,9,80,58,65,89,164,171),(6,1,1,28,15,24,15,22),(6,1,2,29,16,25,15,22),(6,1,3,31,17,26,15,23),(6,1,4,32,17,27,15,23),(6,1,5,33,18,29,15,23),(6,1,6,34,19,30,15,23),(6,1,7,36,20,31,16,24),(6,1,8,37,21,32,16,24),(6,1,9,38,22,34,16,24),(6,1,10,38,22,32,17,25),(6,1,11,40,23,34,17,25),(6,1,12,41,24,35,17,25),(6,1,13,42,25,36,17,26),(6,1,14,44,26,37,18,26),(6,1,15,45,26,38,18,26),(6,1,16,46,27,39,18,27),(6,1,17,47,28,41,18,27),(6,1,18,49,29,42,18,27),(6,1,19,50,30,43,18,28),(6,1,20,52,31,44,18,28),(6,1,21,53,31,46,19,28),(6,1,22,54,32,47,19,29),(6,1,23,56,33,48,19,29),(6,1,24,57,34,50,19,30),(6,1,25,59,35,51,19,30),(6,1,26,60,36,52,19,30),(6,1,27,62,37,54,19,31),(6,1,28,63,38,55,20,31),(6,1,29,65,39,57,20,32),(6,1,30,67,40,58,20,32),(6,1,31,68,41,59,20,32),(6,1,32,70,42,61,20,33),(6,1,33,71,43,62,20,33),(6,1,34,73,44,64,21,34),(6,1,35,75,45,65,21,34),(6,1,36,77,46,67,21,35),(6,1,37,78,47,69,21,35),(6,1,38,80,48,70,21,35),(6,1,39,82,49,72,22,36),(6,1,40,84,50,73,22,36),(6,1,41,85,52,75,22,37),(6,1,42,87,53,77,22,37),(6,1,43,89,54,78,22,38),(6,1,44,91,55,80,22,38),(6,1,45,93,56,82,23,39),(6,1,46,95,57,84,23,39),(6,1,47,97,59,85,23,40),(6,1,48,99,60,87,23,40),(6,1,49,101,61,89,24,41),(6,1,50,103,62,91,24,41),(6,1,51,105,64,93,24,42),(6,1,52,107,65,95,24,42),(6,1,53,109,66,97,24,43),(6,1,54,111,68,99,25,44),(6,1,55,114,69,101,25,44),(6,1,56,116,70,103,25,45),(6,1,57,118,72,105,25,45),(6,1,58,120,73,107,26,46),(6,1,59,123,75,109,26,46),(6,1,60,125,76,111,26,47),(6,1,61,127,77,113,26,48),(6,1,62,130,79,115,26,48),(6,1,63,132,80,118,27,49),(6,1,64,135,82,120,27,49),(6,1,65,137,84,122,27,50),(6,1,66,140,85,124,28,51),(6,1,67,142,87,127,28,51),(6,1,68,145,88,129,28,52),(6,1,69,147,90,131,28,53),(6,1,70,150,92,134,29,53),(6,1,71,153,93,136,29,54),(6,1,72,155,95,139,29,55),(6,1,73,158,97,141,29,56),(6,1,74,161,98,144,30,56),(6,1,75,164,100,146,30,57),(6,1,76,167,102,149,30,58),(6,1,77,170,104,152,31,59),(6,1,78,173,105,154,31,59),(6,1,79,176,107,157,31,60),(6,1,80,179,109,160,32,61),(6,3,1,25,18,23,15,23),(6,3,2,25,19,24,16,24),(6,3,3,26,21,25,16,24),(6,3,4,26,22,26,17,25),(6,3,5,27,24,27,17,25),(6,3,6,27,25,28,18,26),(6,3,7,28,26,28,19,27),(6,3,8,28,28,29,19,27),(6,3,9,28,29,30,20,28),(6,3,10,29,30,29,21,28),(6,3,11,29,31,30,21,29),(6,3,12,30,33,31,22,30),(6,3,13,30,34,32,23,30),(6,3,14,31,35,33,23,31),(6,3,15,31,37,34,24,31),(6,3,16,32,38,35,24,32),(6,3,17,32,39,36,25,33),(6,3,18,33,41,37,26,34),(6,3,19,33,42,38,26,34),(6,3,20,34,44,39,27,35),(6,3,21,34,45,40,28,36),(6,3,22,35,47,41,28,36),(6,3,23,35,48,42,29,37),(6,3,24,36,50,43,30,38),(6,3,25,36,51,44,30,39),(6,3,26,37,53,45,31,39),(6,3,27,37,55,46,32,40),(6,3,28,38,56,47,32,41),(6,3,29,38,58,48,33,42),(6,3,30,39,60,49,34,42),(6,3,31,39,61,51,35,43),(6,3,32,40,63,52,35,44),(6,3,33,41,65,53,36,45),(6,3,34,41,66,54,37,46),(6,3,35,42,68,55,38,47),(6,3,36,43,70,57,39,48),(6,3,37,43,72,58,39,48),(6,3,38,44,74,59,40,49),(6,3,39,44,76,60,41,50),(6,3,40,45,77,62,42,51),(6,3,41,46,79,63,43,52),(6,3,42,46,81,64,43,53),(6,3,43,47,83,65,44,54),(6,3,44,48,85,67,45,55),(6,3,45,48,87,68,46,56),(6,3,46,49,89,70,47,57),(6,3,47,50,91,71,48,58),(6,3,48,51,94,72,49,59),(6,3,49,51,96,74,50,60),(6,3,50,52,98,75,51,61),(6,3,51,53,100,77,52,62),(6,3,52,54,102,78,53,63),(6,3,53,54,104,80,54,64),(6,3,54,55,107,81,55,65),(6,3,55,56,109,83,56,66),(6,3,56,57,111,84,57,67),(6,3,57,58,114,86,58,69),(6,3,58,58,116,88,59,70),(6,3,59,59,119,89,60,71),(6,3,60,60,121,91,61,72),(6,3,61,61,123,93,62,73),(6,3,62,62,126,94,63,74),(6,3,63,63,129,96,64,76),(6,3,64,63,131,98,65,77),(6,3,65,64,134,100,67,78),(6,3,66,65,136,101,68,79),(6,3,67,66,139,103,69,81),(6,3,68,67,142,105,70,82),(6,3,69,68,145,107,71,83),(6,3,70,69,147,109,73,85),(6,3,71,70,150,111,74,86),(6,3,72,71,153,113,75,87),(6,3,73,72,156,115,76,89),(6,3,74,73,159,117,78,90),(6,3,75,74,162,119,79,92),(6,3,76,75,173,121,80,93),(6,3,77,76,176,123,82,95),(6,3,78,77,179,125,83,96),(6,3,79,78,182,127,84,98),(6,3,80,79,186,129,86,99),(6,6,1,28,15,24,15,22),(6,6,2,29,16,25,15,22),(6,6,3,31,17,26,15,23),(6,6,4,32,17,27,15,23),(6,6,5,33,18,29,15,23),(6,6,6,34,19,30,15,23),(6,6,7,36,20,31,16,24),(6,6,8,37,21,32,16,24),(6,6,9,38,22,34,16,24),(6,6,10,40,22,35,16,25),(6,6,11,41,23,36,16,25),(6,6,12,43,24,37,16,25),(6,6,13,44,25,39,16,26),(6,6,14,46,26,40,16,26),(6,6,15,47,27,41,17,26),(6,6,16,48,28,43,17,27),(6,6,17,50,29,44,17,27),(6,6,18,51,30,45,17,27),(6,6,19,53,31,47,17,28),(6,6,20,54,32,48,17,28),(6,6,21,56,33,50,17,28),(6,6,22,58,34,51,18,29),(6,6,23,59,35,52,18,29),(6,6,24,61,36,54,18,30),(6,6,25,62,37,55,18,30),(6,6,26,64,38,57,18,30),(6,6,27,66,39,58,18,31),(6,6,28,67,40,60,18,31),(6,6,29,69,41,61,19,32),(6,6,30,71,42,63,19,32),(6,6,31,72,43,64,19,32),(6,6,32,74,44,66,19,33),(6,6,33,76,45,67,19,33),(6,6,34,77,46,69,20,34),(6,6,35,79,47,71,20,34),(6,6,36,81,48,72,20,35),(6,6,37,83,49,74,20,35),(6,6,38,85,50,76,20,35),(6,6,39,86,51,77,21,36),(6,6,40,88,53,79,21,36),(6,6,41,90,54,81,21,37),(6,6,42,92,55,82,21,37),(6,6,43,94,56,84,21,38),(6,6,44,96,57,86,22,38),(6,6,45,98,58,87,22,39),(6,6,46,99,60,89,22,39),(6,6,47,101,60,91,22,40),(6,6,48,103,61,93,22,40),(6,6,49,105,62,94,23,41),(6,6,50,107,63,96,23,41),(6,6,51,109,64,97,23,42),(6,6,52,110,65,98,23,42),(6,6,53,111,66,99,24,43),(6,6,54,112,67,100,24,44),(6,6,55,113,69,100,25,44),(6,6,56,116,71,103,25,45),(6,6,57,118,72,105,25,45),(6,6,58,120,73,107,26,46),(6,6,59,123,75,109,26,46),(6,6,60,125,76,111,26,47),(6,6,61,127,77,113,26,48),(6,6,62,130,79,115,26,48),(6,6,63,132,80,118,27,49),(6,6,64,135,82,120,27,49),(6,6,65,138,83,122,27,50),(6,6,66,140,85,125,27,51),(6,6,67,143,86,127,28,51),(6,6,68,145,88,129,28,52),(6,6,69,148,89,132,28,53),(6,6,70,151,91,134,28,53),(6,6,71,154,93,137,29,54),(6,6,72,156,94,139,29,55),(6,6,73,159,96,142,29,56),(6,6,74,162,98,144,29,56),(6,6,75,165,99,147,30,57),(6,6,76,168,101,150,30,58),(6,6,77,171,103,152,30,59),(6,6,78,174,104,155,30,59),(6,6,79,177,106,158,31,60),(6,6,80,180,108,161,31,61),(6,7,1,26,15,23,16,24),(6,7,2,27,15,24,17,25),(6,7,3,28,16,25,18,26),(6,7,4,28,16,26,19,27),(6,7,5,29,17,27,20,28),(6,7,6,30,17,28,21,29),(6,7,7,31,18,29,22,30),(6,7,8,32,18,30,23,31),(6,7,9,33,19,31,24,32),(6,7,10,33,20,30,24,32),(6,7,11,34,20,31,25,33),(6,7,12,35,21,32,26,34),(6,7,13,35,21,33,27,35),(6,7,14,36,22,34,28,36),(6,7,15,37,22,35,29,38),(6,7,16,38,23,36,30,39),(6,7,17,39,23,37,31,40),(6,7,18,40,24,38,32,41),(6,7,19,41,24,39,33,42),(6,7,20,42,25,40,34,43),(6,7,21,43,25,41,35,44),(6,7,22,43,26,42,36,45),(6,7,23,44,26,43,37,46),(6,7,24,45,27,44,38,48),(6,7,25,46,27,46,39,49),(6,7,26,47,28,47,40,50),(6,7,27,48,28,48,41,51),(6,7,28,49,29,49,42,52),(6,7,29,50,29,50,43,54),(6,7,30,51,30,51,44,55),(6,7,31,53,30,53,46,56),(6,7,32,54,31,54,47,58),(6,7,33,55,32,55,48,59),(6,7,34,56,32,56,49,60),(6,7,35,57,33,58,50,62),(6,7,36,58,34,59,52,63),(6,7,37,59,34,60,53,64),(6,7,38,60,35,62,54,66),(6,7,39,61,35,63,55,67),(6,7,40,63,36,64,57,69),(6,7,41,64,37,66,58,70),(6,7,42,65,37,67,59,72),(6,7,43,66,38,69,60,73),(6,7,44,68,39,70,62,75),(6,7,45,69,39,72,63,76),(6,7,46,70,40,73,65,78),(6,7,47,71,41,75,66,79),(6,7,48,73,42,76,67,81),(6,7,49,74,42,78,69,83),(6,7,50,75,43,79,70,84),(6,7,51,77,44,81,72,86),(6,7,52,78,45,82,73,88),(6,7,53,80,45,84,75,89),(6,7,54,81,46,86,76,91),(6,7,55,82,47,87,78,93),(6,7,56,84,48,89,79,95),(6,7,57,85,49,91,81,96),(6,7,58,87,49,92,83,98),(6,7,59,88,50,94,84,100),(6,7,60,90,51,96,86,102),(6,7,61,92,52,98,88,104),(6,7,62,93,53,100,89,106),(6,7,63,95,54,101,91,108),(6,7,64,96,54,103,93,110),(6,7,65,98,55,105,95,112),(6,7,66,100,56,107,96,114),(6,7,67,101,57,109,98,116),(6,7,68,103,58,111,100,118),(6,7,69,105,59,113,102,120),(6,7,70,107,60,115,104,122),(6,7,71,108,61,117,106,124),(6,7,72,110,62,119,108,127),(6,7,73,112,63,121,110,129),(6,7,74,114,64,124,112,131),(6,7,75,116,65,126,114,133),(6,7,76,118,66,128,116,136),(6,7,77,119,67,130,118,138),(6,7,78,121,68,132,120,140),(6,7,79,123,69,135,122,143),(6,7,80,125,70,137,124,145),(6,11,1,26,15,22,17,24),(6,11,2,27,16,23,18,25),(6,11,3,27,16,23,19,26),(6,11,4,28,17,24,20,27),(6,11,5,28,17,25,21,29),(6,11,6,29,18,25,22,30),(6,11,7,29,18,26,23,31),(6,11,8,30,19,27,24,32),(6,11,9,30,19,27,26,34),(6,11,10,31,20,26,26,33),(6,11,11,31,21,27,27,35),(6,11,12,32,21,28,28,36),(6,11,13,32,22,28,29,37),(6,11,14,33,22,29,30,38),(6,11,15,34,23,30,32,39),(6,11,16,34,23,30,33,40),(6,11,17,35,24,31,34,42),(6,11,18,35,25,32,35,43),(6,11,19,36,25,32,36,44),(6,11,20,37,26,33,37,45),(6,11,21,37,26,34,38,47),(6,11,22,38,27,35,39,48),(6,11,23,39,28,35,40,49),(6,11,24,39,28,36,42,51),(6,11,25,40,29,37,43,52),(6,11,26,41,29,38,44,53),(6,11,27,41,30,38,45,55),(6,11,28,42,31,39,46,56),(6,11,29,43,31,40,48,58),(6,11,30,43,32,41,49,59),(6,11,31,44,33,42,50,60),(6,11,32,45,33,43,52,62),(6,11,33,46,34,43,53,63),(6,11,34,46,35,44,54,65),(6,11,35,47,35,45,56,66),(6,11,36,48,36,46,57,68),(6,11,37,49,37,47,58,70),(6,11,38,50,37,48,60,71),(6,11,39,50,38,49,61,73),(6,11,40,51,39,50,63,74),(6,11,41,52,40,51,64,76),(6,11,42,53,40,52,66,78),(6,11,43,54,41,53,67,79),(6,11,44,55,42,53,69,81),(6,11,45,55,43,54,70,83),(6,11,46,56,44,55,72,85),(6,11,47,57,44,57,73,86),(6,11,48,58,45,58,75,88),(6,11,49,59,46,59,77,90),(6,11,50,60,47,60,78,92),(6,11,51,61,48,61,80,94),(6,11,52,62,49,62,82,96),(6,11,53,63,50,63,83,98),(6,11,54,64,50,64,85,100),(6,11,55,65,51,65,87,102),(6,11,56,66,52,66,89,104),(6,11,57,67,53,67,90,106),(6,11,58,68,54,69,92,108),(6,11,59,69,55,70,94,110),(6,11,60,70,56,71,96,112),(6,11,61,71,57,72,98,114),(6,11,62,72,58,73,100,116),(6,11,63,73,59,75,102,119),(6,11,64,74,60,76,104,121),(6,11,65,76,61,77,106,123),(6,11,66,77,62,79,108,125),(6,11,67,78,63,80,110,128),(6,11,68,79,64,81,112,130),(6,11,69,80,65,83,114,132),(6,11,70,81,66,84,116,135),(6,11,71,83,67,85,118,137),(6,11,72,84,69,87,121,140),(6,11,73,85,70,88,123,142),(6,11,74,86,71,90,125,145),(6,11,75,88,72,91,127,147),(6,11,76,89,73,93,130,150),(6,11,77,90,74,94,132,153),(6,11,78,92,76,96,134,155),(6,11,79,93,77,97,137,158),(6,11,80,94,78,99,139,161),(7,1,1,18,23,21,24,20),(7,1,2,19,24,22,24,20),(7,1,3,21,25,23,24,21),(7,1,4,22,25,25,24,21),(7,1,5,23,26,26,24,21),(7,1,6,25,27,27,24,21),(7,1,7,26,28,28,24,22),(7,1,8,27,29,29,25,22),(7,1,9,29,29,31,25,22),(7,1,10,28,28,31,24,23),(7,1,11,30,29,33,24,23),(7,1,12,31,30,34,24,23),(7,1,13,32,31,35,24,24),(7,1,14,34,32,36,25,24),(7,1,15,35,32,37,25,24),(7,1,16,36,33,38,25,25),(7,1,17,37,34,40,25,25),(7,1,18,39,35,41,25,25),(7,1,19,40,36,42,25,26),(7,1,20,42,37,43,25,26),(7,1,21,43,37,45,26,26),(7,1,22,44,38,46,26,27),(7,1,23,46,39,47,26,27),(7,1,24,47,40,49,26,28),(7,1,25,49,41,50,26,28),(7,1,26,50,42,51,26,28),(7,1,27,52,43,53,26,29),(7,1,28,53,44,54,27,29),(7,1,29,55,45,56,27,30),(7,1,30,57,46,57,27,30),(7,1,31,58,47,58,27,30),(7,1,32,60,48,60,27,31),(7,1,33,61,49,61,27,31),(7,1,34,63,50,63,28,32),(7,1,35,65,51,64,28,32),(7,1,36,67,52,66,28,33),(7,1,37,68,53,68,28,33),(7,1,38,70,54,69,28,33),(7,1,39,72,55,71,29,34),(7,1,40,74,56,72,29,34),(7,1,41,75,58,74,29,35),(7,1,42,77,59,76,29,35),(7,1,43,79,60,77,29,36),(7,1,44,81,61,79,29,36),(7,1,45,83,62,81,30,37),(7,1,46,85,63,83,30,37),(7,1,47,87,65,84,30,38),(7,1,48,89,66,86,30,38),(7,1,49,91,67,88,31,39),(7,1,50,93,68,90,31,39),(7,1,51,95,70,92,31,40),(7,1,52,97,71,94,31,40),(7,1,53,99,72,96,31,41),(7,1,54,101,74,98,32,42),(7,1,55,104,75,100,32,42),(7,1,56,106,76,102,32,43),(7,1,57,108,78,104,32,43),(7,1,58,110,79,106,33,44),(7,1,59,113,81,108,33,44),(7,1,60,115,82,110,33,45),(7,1,61,117,83,112,33,46),(7,1,62,120,85,114,33,46),(7,1,63,122,86,117,34,47),(7,1,64,125,88,119,34,47),(7,1,65,127,90,121,34,48),(7,1,66,130,91,123,35,49),(7,1,67,132,93,126,35,49),(7,1,68,135,94,128,35,50),(7,1,69,137,96,130,35,51),(7,1,70,140,98,133,36,51),(7,1,71,143,99,135,36,52),(7,1,72,145,101,138,36,53),(7,1,73,148,103,140,36,54),(7,1,74,151,104,143,37,54),(7,1,75,154,106,145,37,55),(7,1,76,157,108,148,37,56),(7,1,77,160,110,151,38,57),(7,1,78,163,111,153,38,57),(7,1,79,166,113,156,38,58),(7,1,80,169,115,159,39,59),(7,4,1,16,26,20,24,20),(7,4,2,17,27,21,24,20),(7,4,3,18,29,21,24,21),(7,4,4,18,30,22,24,21),(7,4,5,19,32,23,25,21),(7,4,6,20,33,24,25,22),(7,4,7,21,35,24,25,22),(7,4,8,22,36,25,25,23),(7,4,9,22,38,26,25,23),(7,4,10,22,37,27,25,23),(7,4,11,23,38,28,25,24),(7,4,12,24,39,28,25,24),(7,4,13,25,41,29,25,24),(7,4,14,25,42,30,25,25),(7,4,15,26,44,30,26,25),(7,4,16,27,45,31,26,26),(7,4,17,28,46,32,26,26),(7,4,18,29,48,33,26,26),(7,4,19,30,50,33,26,27),(7,4,20,30,51,34,27,27),(7,4,21,31,53,35,27,28),(7,4,22,32,54,36,27,28),(7,4,23,33,56,37,27,29),(7,4,24,34,57,37,28,29),(7,4,25,35,59,38,28,30),(7,4,26,36,61,39,28,30),(7,4,27,37,62,40,28,30),(7,4,28,38,64,41,28,31),(7,4,29,38,66,42,29,31),(7,4,30,39,68,42,29,32),(7,4,31,40,69,43,29,32),(7,4,32,41,71,44,29,33),(7,4,33,42,73,45,30,33),(7,4,34,43,75,46,30,34),(7,4,35,44,77,47,30,34),(7,4,36,46,79,48,31,35),(7,4,37,47,80,49,31,36),(7,4,38,48,82,50,31,36),(7,4,39,49,84,51,31,37),(7,4,40,50,86,52,32,37),(7,4,41,51,88,53,32,38),(7,4,42,52,90,54,32,38),(7,4,43,53,92,55,32,39),(7,4,44,54,95,56,33,39),(7,4,45,56,97,57,33,40),(7,4,46,57,99,58,33,41),(7,4,47,58,101,59,34,41),(7,4,48,59,103,60,34,42),(7,4,49,60,105,62,34,43),(7,4,50,62,108,63,35,43),(7,4,51,63,115,64,35,44),(7,4,52,64,115,65,35,44),(7,4,53,65,120,66,36,45),(7,4,54,67,122,67,36,46),(7,4,55,68,124,69,36,46),(7,4,56,69,128,70,37,47),(7,4,57,71,130,71,37,48),(7,4,58,72,133,72,37,49),(7,4,59,74,135,74,38,49),(7,4,60,75,138,75,38,50),(7,4,61,76,141,76,38,51),(7,4,62,78,143,78,39,51),(7,4,63,79,143,79,39,52),(7,4,64,81,150,80,39,53),(7,4,65,82,152,82,40,54),(7,4,66,84,155,83,40,55),(7,4,67,85,158,85,41,55),(7,4,68,87,161,86,41,56),(7,4,69,89,164,87,41,57),(7,4,70,90,164,89,42,58),(7,4,71,92,171,90,42,59),(7,4,72,94,174,92,43,59),(7,4,73,95,177,94,43,60),(7,4,74,97,180,95,44,61),(7,4,75,99,183,97,44,62),(7,4,76,100,186,98,44,63),(7,4,77,102,190,100,45,64),(7,4,78,104,194,102,45,65),(7,4,79,106,197,103,46,66),(7,4,80,108,200,105,46,67),(7,6,1,18,23,21,24,20),(7,6,2,19,24,22,24,20),(7,6,3,21,25,23,24,21),(7,6,4,22,25,25,24,21),(7,6,5,23,26,26,24,21),(7,6,6,25,27,27,24,21),(7,6,7,26,28,28,24,22),(7,6,8,27,29,29,25,22),(7,6,9,29,29,31,25,22),(7,6,10,30,30,32,25,23),(7,6,11,32,31,33,25,23),(7,6,12,33,32,34,25,23),(7,6,13,34,33,36,25,24),(7,6,14,36,34,37,25,24),(7,6,15,37,35,38,25,24),(7,6,16,39,36,40,25,25),(7,6,17,40,36,41,25,25),(7,6,18,42,37,43,26,25),(7,6,19,43,38,44,26,26),(7,6,20,45,39,45,26,26),(7,6,21,47,40,47,26,26),(7,6,22,48,41,48,26,27),(7,6,23,50,42,50,26,27),(7,6,24,51,43,51,26,28),(7,6,25,53,44,52,27,28),(7,6,26,55,45,54,27,28),(7,6,27,56,46,55,27,29),(7,6,28,58,47,57,27,29),(7,6,29,59,48,58,27,30),(7,6,30,61,49,60,27,30),(7,6,31,63,50,62,27,30),(7,6,32,65,51,63,28,31),(7,6,33,66,52,65,28,31),(7,6,34,68,53,66,28,32),(7,6,35,70,55,68,28,32),(7,6,36,72,56,69,28,33),(7,6,37,73,57,71,29,33),(7,6,38,75,58,73,29,34),(7,6,39,77,59,74,29,34),(7,6,40,79,60,76,29,35),(7,6,41,81,61,78,29,35),(7,6,42,82,62,79,30,35),(7,6,43,84,64,81,30,36),(7,6,44,86,65,83,30,36),(7,6,45,88,66,85,30,37),(7,6,46,90,67,86,30,37),(7,6,47,92,68,88,31,38),(7,6,48,94,70,90,31,38),(7,6,49,96,71,92,31,39),(7,6,50,98,72,93,31,40),(7,6,51,99,72,93,32,40),(7,6,52,100,73,95,32,41),(7,6,53,101,74,96,32,41),(7,6,54,102,75,97,32,42),(7,6,55,103,75,99,32,42),(7,6,56,106,77,102,32,43),(7,6,57,108,78,104,32,43),(7,6,58,110,79,106,33,44),(7,6,59,113,81,108,33,44),(7,6,60,115,82,110,33,45),(7,6,61,117,83,112,33,46),(7,6,62,120,85,114,33,46),(7,6,63,122,86,117,34,47),(7,6,64,125,88,119,34,47),(7,6,65,128,89,121,34,48),(7,6,66,130,91,124,34,49),(7,6,67,133,92,126,35,49),(7,6,68,135,94,128,35,50),(7,6,69,138,95,131,35,51),(7,6,70,141,97,133,35,51),(7,6,71,144,99,136,36,52),(7,6,72,146,100,138,36,53),(7,6,73,149,102,141,36,54),(7,6,74,152,104,143,36,54),(7,6,75,155,105,146,37,55),(7,6,76,158,107,149,37,56),(7,6,77,161,109,151,37,57),(7,6,78,164,110,154,37,57),(7,6,79,167,112,157,38,58),(7,6,80,170,114,160,38,59),(7,8,1,15,23,19,27,22),(7,8,2,15,23,19,28,23),(7,8,3,15,23,20,30,25),(7,8,4,15,23,20,31,26),(7,8,5,15,24,20,32,27),(7,8,6,15,24,20,34,29),(7,8,7,16,24,21,35,30),(7,8,8,16,24,21,37,31),(7,8,9,16,24,21,38,33),(7,8,10,16,24,23,37,33),(7,8,11,16,24,23,38,34),(7,8,12,16,24,23,40,35),(7,8,13,16,24,24,41,36),(7,8,14,17,24,24,42,38),(7,8,15,17,25,24,44,39),(7,8,16,17,25,25,45,40),(7,8,17,17,25,25,46,42),(7,8,18,17,25,25,48,43),(7,8,19,17,25,26,49,44),(7,8,20,17,26,26,51,46),(7,8,21,18,26,26,52,47),(7,8,22,18,26,27,54,49),(7,8,23,18,26,27,55,50),(7,8,24,18,27,28,57,52),(7,8,25,18,27,28,58,53),(7,8,26,18,27,28,60,55),(7,8,27,18,27,29,62,56),(7,8,28,19,27,29,63,58),(7,8,29,19,28,30,65,59),(7,8,30,19,28,30,67,61),(7,8,31,19,28,30,68,63),(7,8,32,19,28,31,70,64),(7,8,33,19,29,31,72,66),(7,8,34,20,29,32,73,68),(7,8,35,20,29,32,75,69),(7,8,36,20,30,33,77,71),(7,8,37,20,30,33,79,73),(7,8,38,20,30,33,81,75),(7,8,39,21,30,34,83,76),(7,8,40,21,31,34,84,78),(7,8,41,21,31,35,86,80),(7,8,42,21,31,35,88,82),(7,8,43,21,31,36,90,84),(7,8,44,21,32,36,92,86),(7,8,45,22,32,37,94,88),(7,8,46,22,32,37,96,90),(7,8,47,22,33,38,98,92),(7,8,48,22,33,38,101,94),(7,8,49,23,33,39,103,96),(7,8,50,23,34,39,110,98),(7,8,51,23,34,40,112,100),(7,8,52,23,34,40,114,102),(7,8,53,23,35,41,116,104),(7,8,54,24,35,42,119,106),(7,8,55,24,35,42,121,109),(7,8,56,24,36,43,123,111),(7,8,57,24,36,43,126,113),(7,8,58,25,36,44,129,115),(7,8,59,25,37,44,132,118),(7,8,60,25,37,45,134,120),(7,8,61,25,37,46,136,122),(7,8,62,25,38,46,139,125),(7,8,63,26,38,47,142,127),(7,8,64,26,38,47,144,130),(7,8,65,26,39,48,147,132),(7,8,66,27,39,49,150,135),(7,8,67,27,40,49,153,137),(7,8,68,27,40,50,156,140),(7,8,69,27,40,51,159,143),(7,8,70,28,41,51,161,145),(7,8,71,28,41,52,164,148),(7,8,72,28,42,53,167,151),(7,8,73,28,42,54,171,154),(7,8,74,29,43,54,174,156),(7,8,75,29,43,55,177,159),(7,8,76,29,43,56,180,162),(7,8,77,30,44,57,183,165),(7,8,78,30,44,57,186,168),(7,8,79,30,45,58,189,171),(7,8,80,31,45,59,193,174),(7,9,1,15,23,20,26,22),(7,9,2,15,23,21,27,23),(7,9,3,16,24,21,28,24),(7,9,4,16,24,22,29,25),(7,9,5,16,24,22,31,27),(7,9,6,17,25,23,32,28),(7,9,7,17,25,23,33,29),(7,9,8,17,26,24,34,30),(7,9,9,17,26,25,36,31),(7,9,10,18,25,26,34,32),(7,9,11,18,26,26,36,33),(7,9,12,18,26,27,37,34),(7,9,13,19,26,27,38,36),(7,9,14,19,27,28,39,37),(7,9,15,19,27,29,40,38),(7,9,16,20,28,29,41,39),(7,9,17,20,28,30,43,41),(7,9,18,20,28,30,44,42),(7,9,19,21,29,31,45,43),(7,9,20,21,29,32,46,45),(7,9,21,21,30,32,48,46),(7,9,22,22,30,33,49,47),(7,9,23,22,31,34,50,49),(7,9,24,23,31,34,52,50),(7,9,25,23,32,35,53,52),(7,9,26,23,32,36,54,53),(7,9,27,24,32,36,56,54),(7,9,28,24,33,37,57,56),(7,9,29,25,33,38,59,57),(7,9,30,25,34,38,60,59),(7,9,31,25,34,39,61,61),(7,9,32,26,35,40,63,62),(7,9,33,26,35,41,64,64),(7,9,34,27,36,41,66,65),(7,9,35,27,36,42,67,67),(7,9,36,28,37,43,69,69),(7,9,37,28,38,44,71,70),(7,9,38,28,38,45,72,72),(7,9,39,29,39,45,74,74),(7,9,40,29,39,46,75,75),(7,9,41,30,40,47,77,77),(7,9,42,30,40,48,79,79),(7,9,43,31,41,49,80,81),(7,9,44,31,41,50,82,82),(7,9,45,32,42,50,84,84),(7,9,46,32,43,51,86,86),(7,9,47,33,43,52,87,88),(7,9,48,33,44,53,89,90),(7,9,49,34,45,54,91,92),(7,9,50,34,45,55,97,94),(7,9,51,35,46,56,99,96),(7,9,52,35,46,57,101,98),(7,9,53,36,47,58,103,100),(7,9,54,37,48,59,105,102),(7,9,55,37,48,60,108,104),(7,9,56,38,49,61,110,106),(7,9,57,38,50,62,112,108),(7,9,58,39,51,63,114,111),(7,9,59,39,51,64,116,113),(7,9,60,40,52,65,118,115),(7,9,61,41,53,66,120,117),(7,9,62,41,53,67,122,120),(7,9,63,42,54,68,125,122),(7,9,64,42,55,69,127,124),(7,9,65,43,56,71,130,127),(7,9,66,44,57,72,132,129),(7,9,67,44,57,73,135,132),(7,9,68,45,58,74,137,134),(7,9,69,46,59,75,139,136),(7,9,70,46,60,76,142,139),(7,9,71,47,61,78,144,142),(7,9,72,48,61,79,147,144),(7,9,73,49,62,80,150,147),(7,9,74,49,63,81,153,150),(7,9,75,50,64,83,155,152),(7,9,76,51,65,84,158,155),(7,9,77,52,66,85,161,158),(7,9,78,52,67,87,163,161),(7,9,79,53,68,88,166,164),(7,9,80,54,69,89,169,166),(8,1,1,24,22,23,16,21),(8,1,2,25,23,24,16,21),(8,1,3,27,24,25,16,22),(8,1,4,28,24,26,16,22),(8,1,5,29,25,28,16,22),(8,1,6,31,26,29,16,22),(8,1,7,32,27,30,17,23),(8,1,8,33,28,31,17,23),(8,1,9,35,28,33,17,23),(8,1,10,34,28,31,17,24),(8,1,11,36,29,33,17,24),(8,1,12,37,30,34,17,24),(8,1,13,38,31,35,17,25),(8,1,14,40,32,36,18,25),(8,1,15,41,32,37,18,25),(8,1,16,42,33,38,18,26),(8,1,17,43,34,40,18,26),(8,1,18,45,35,41,18,26),(8,1,19,46,36,42,18,27),(8,1,20,48,37,43,18,27),(8,1,21,49,37,45,19,27),(8,1,22,50,38,46,19,28),(8,1,23,52,39,47,19,28),(8,1,24,53,40,49,19,29),(8,1,25,55,41,50,19,29),(8,1,26,56,42,51,19,29),(8,1,27,58,43,53,19,30),(8,1,28,59,44,54,20,30),(8,1,29,61,45,56,20,31),(8,1,30,63,46,57,20,31),(8,1,31,64,47,58,20,31),(8,1,32,66,48,60,20,32),(8,1,33,67,49,61,20,32),(8,1,34,69,50,63,21,33),(8,1,35,71,51,64,21,33),(8,1,36,73,52,66,21,34),(8,1,37,74,53,68,21,34),(8,1,38,76,54,69,21,34),(8,1,39,78,55,71,22,35),(8,1,40,80,56,72,22,35),(8,1,41,81,58,74,22,36),(8,1,42,83,59,76,22,36),(8,1,43,85,60,77,22,37),(8,1,44,87,61,79,22,37),(8,1,45,89,62,81,23,38),(8,1,46,91,63,83,23,38),(8,1,47,93,65,84,23,39),(8,1,48,95,66,86,23,39),(8,1,49,97,67,88,24,40),(8,1,50,99,68,90,24,40),(8,1,51,101,70,92,24,41),(8,1,52,103,71,94,24,41),(8,1,53,105,72,96,24,42),(8,1,54,107,74,98,25,43),(8,1,55,110,75,100,25,43),(8,1,56,112,76,102,25,44),(8,1,57,114,78,104,25,44),(8,1,58,116,79,106,26,45),(8,1,59,119,81,108,26,45),(8,1,60,121,82,110,26,46),(8,1,61,123,83,112,26,47),(8,1,62,126,85,114,26,47),(8,1,63,128,86,117,27,48),(8,1,64,131,88,119,27,48),(8,1,65,133,90,121,27,49),(8,1,66,136,91,123,28,50),(8,1,67,138,93,126,28,50),(8,1,68,141,94,128,28,51),(8,1,69,143,96,130,28,52),(8,1,70,146,98,133,29,52),(8,1,71,149,99,135,29,53),(8,1,72,151,101,138,29,54),(8,1,73,154,103,140,29,55),(8,1,74,157,104,143,30,55),(8,1,75,160,106,145,30,56),(8,1,76,163,108,148,30,57),(8,1,77,166,110,151,31,58),(8,1,78,169,111,153,31,58),(8,1,79,172,113,156,31,59),(8,1,80,175,115,159,32,60),(8,3,1,21,25,22,16,22),(8,3,2,21,26,23,17,23),(8,3,3,22,28,24,17,23),(8,3,4,22,29,25,18,24),(8,3,5,23,30,26,18,25),(8,3,6,23,32,27,19,25),(8,3,7,24,33,28,20,26),(8,3,8,24,35,28,20,26),(8,3,9,25,36,29,21,27),(8,3,10,25,36,28,21,27),(8,3,11,25,37,29,21,28),(8,3,12,26,39,30,22,29),(8,3,13,26,40,31,23,29),(8,3,14,27,41,32,23,30),(8,3,15,27,43,33,24,30),(8,3,16,28,44,34,24,31),(8,3,17,28,45,35,25,32),(8,3,18,29,47,36,26,33),(8,3,19,29,48,37,26,33),(8,3,20,30,50,38,27,34),(8,3,21,30,51,39,28,35),(8,3,22,31,53,40,28,35),(8,3,23,31,54,41,29,36),(8,3,24,32,56,42,30,37),(8,3,25,32,57,43,30,38),(8,3,26,33,59,44,31,38),(8,3,27,33,61,45,32,39),(8,3,28,34,62,46,32,40),(8,3,29,34,64,47,33,41),(8,3,30,35,66,48,34,41),(8,3,31,35,67,50,35,42),(8,3,32,36,69,51,35,43),(8,3,33,37,71,52,36,44),(8,3,34,37,72,53,37,45),(8,3,35,38,74,54,38,46),(8,3,36,39,76,56,39,47),(8,3,37,39,78,57,39,47),(8,3,38,40,80,58,40,48),(8,3,39,40,82,59,41,49),(8,3,40,41,83,61,42,50),(8,3,41,42,85,62,43,51),(8,3,42,42,87,63,43,52),(8,3,43,43,89,64,44,53),(8,3,44,44,91,66,45,54),(8,3,45,44,93,67,46,55),(8,3,46,45,95,69,47,56),(8,3,47,46,97,70,48,57),(8,3,48,47,100,71,49,58),(8,3,49,47,102,73,50,59),(8,3,50,48,104,74,51,60),(8,3,51,49,106,76,52,61),(8,3,52,50,108,77,53,62),(8,3,53,50,110,79,54,63),(8,3,54,51,113,80,55,64),(8,3,55,52,115,82,56,65),(8,3,56,53,117,83,57,66),(8,3,57,54,120,85,58,68),(8,3,58,54,122,87,59,69),(8,3,59,55,125,88,60,70),(8,3,60,56,127,90,61,71),(8,3,61,57,129,92,62,72),(8,3,62,58,132,93,63,73),(8,3,63,59,135,95,64,75),(8,3,64,59,137,97,65,76),(8,3,65,60,140,99,67,77),(8,3,66,61,142,100,68,78),(8,3,67,62,145,102,69,80),(8,3,68,63,148,104,70,81),(8,3,69,64,151,106,71,82),(8,3,70,65,153,108,73,84),(8,3,71,66,156,110,74,85),(8,3,72,67,159,112,75,86),(8,3,73,68,162,114,76,88),(8,3,74,69,165,116,78,89),(8,3,75,70,168,118,79,91),(8,3,76,71,179,120,80,92),(8,3,77,72,182,122,82,94),(8,3,78,73,185,124,83,95),(8,3,79,74,188,126,84,97),(8,3,80,75,192,128,86,98),(8,4,1,22,25,22,16,21),(8,4,2,23,26,23,16,21),(8,4,3,24,28,23,16,22),(8,4,4,24,29,24,16,22),(8,4,5,25,31,25,17,22),(8,4,6,26,32,25,17,23),(8,4,7,27,34,26,17,23),(8,4,8,27,35,27,17,24),(8,4,9,28,37,28,17,24),(8,4,10,28,37,27,18,24),(8,4,11,29,38,28,18,25),(8,4,12,30,39,28,18,25),(8,4,13,31,41,29,18,25),(8,4,14,31,42,30,18,26),(8,4,15,32,44,30,19,26),(8,4,16,33,45,31,19,27),(8,4,17,34,46,32,19,27),(8,4,18,35,48,33,19,27),(8,4,19,36,50,33,19,28),(8,4,20,36,51,34,20,28),(8,4,21,37,53,35,20,29),(8,4,22,38,54,36,20,29),(8,4,23,39,56,37,20,30),(8,4,24,40,57,37,21,30),(8,4,25,41,59,38,21,31),(8,4,26,42,61,39,21,31),(8,4,27,43,62,40,21,31),(8,4,28,44,64,41,21,32),(8,4,29,44,66,42,22,32),(8,4,30,45,68,42,22,33),(8,4,31,46,69,43,22,33),(8,4,32,47,71,44,22,34),(8,4,33,48,73,45,23,34),(8,4,34,49,75,46,23,35),(8,4,35,50,77,47,23,35),(8,4,36,52,79,48,24,36),(8,4,37,53,80,49,24,37),(8,4,38,54,82,50,24,37),(8,4,39,55,84,51,24,38),(8,4,40,56,86,52,25,38),(8,4,41,57,88,53,25,39),(8,4,42,58,90,54,25,39),(8,4,43,59,92,55,25,40),(8,4,44,60,95,56,26,40),(8,4,45,62,97,57,26,41),(8,4,46,63,99,58,26,42),(8,4,47,64,101,59,27,42),(8,4,48,65,103,60,27,43),(8,4,49,66,105,62,27,44),(8,4,50,68,108,63,28,44),(8,4,51,69,115,64,28,45),(8,4,52,70,115,65,28,45),(8,4,53,71,120,66,29,46),(8,4,54,73,122,67,29,47),(8,4,55,74,124,69,29,47),(8,4,56,75,128,70,30,48),(8,4,57,77,130,71,30,49),(8,4,58,78,133,72,30,50),(8,4,59,80,135,74,31,50),(8,4,60,81,138,75,31,51),(8,4,61,82,141,76,31,52),(8,4,62,84,143,78,32,52),(8,4,63,85,143,79,32,53),(8,4,64,87,150,80,32,54),(8,4,65,88,152,82,33,55),(8,4,66,90,155,83,33,56),(8,4,67,91,158,85,34,56),(8,4,68,93,161,86,34,57),(8,4,69,95,164,87,34,58),(8,4,70,96,164,89,35,59),(8,4,71,98,171,90,35,60),(8,4,72,100,174,92,36,60),(8,4,73,101,177,94,36,61),(8,4,74,103,180,95,37,62),(8,4,75,105,183,97,37,63),(8,4,76,106,186,98,37,64),(8,4,77,108,190,100,38,65),(8,4,78,110,194,102,38,66),(8,4,79,112,197,103,39,67),(8,4,80,114,200,105,39,68),(8,5,1,21,22,21,18,24),(8,5,2,21,22,21,19,25),(8,5,3,21,22,22,21,27),(8,5,4,21,23,22,22,28),(8,5,5,22,23,22,23,29),(8,5,6,22,23,23,25,31),(8,5,7,22,23,23,26,32),(8,5,8,22,24,24,27,34),(8,5,9,22,24,24,29,35),(8,5,10,23,24,23,29,35),(8,5,11,23,24,24,30,36),(8,5,12,23,25,24,31,38),(8,5,13,23,25,24,32,39),(8,5,14,23,25,25,34,40),(8,5,15,24,25,25,35,42),(8,5,16,24,26,26,36,43),(8,5,17,24,26,26,38,44),(8,5,18,24,26,26,39,46),(8,5,19,24,27,27,40,47),(8,5,20,25,27,27,42,49),(8,5,21,25,27,28,43,50),(8,5,22,25,27,28,45,52),(8,5,23,25,28,29,46,53),(8,5,24,26,28,29,48,55),(8,5,25,26,28,30,49,56),(8,5,26,26,29,30,51,58),(8,5,27,26,29,30,52,60),(8,5,28,26,29,31,54,61),(8,5,29,27,30,31,55,63),(8,5,30,27,30,32,57,65),(8,5,31,27,30,32,59,66),(8,5,32,27,31,33,60,68),(8,5,33,28,31,33,62,70),(8,5,34,28,31,34,64,71),(8,5,35,28,32,34,65,73),(8,5,36,29,32,35,67,75),(8,5,37,29,32,36,69,77),(8,5,38,29,33,36,71,79),(8,5,39,29,33,37,72,81),(8,5,40,30,33,37,74,82),(8,5,41,30,34,38,76,84),(8,5,42,30,34,38,78,86),(8,5,43,30,35,39,80,88),(8,5,44,31,35,39,82,90),(8,5,45,31,35,40,84,92),(8,5,46,31,36,41,86,94),(8,5,47,32,36,41,88,96),(8,5,48,32,37,42,90,99),(8,5,49,32,37,43,92,101),(8,5,50,33,37,43,98,103),(8,5,51,33,38,44,101,105),(8,5,52,33,38,44,103,107),(8,5,53,34,39,45,105,109),(8,5,54,34,39,46,107,112),(8,5,55,34,40,46,110,114),(8,5,56,35,40,47,112,116),(8,5,57,35,41,48,114,119),(8,5,58,35,41,49,116,121),(8,5,59,36,42,49,119,124),(8,5,60,36,42,50,122,126),(8,5,61,36,42,51,124,128),(8,5,62,37,43,51,127,131),(8,5,63,37,43,52,129,134),(8,5,64,37,44,53,132,136),(8,5,65,38,45,54,134,139),(8,5,66,38,45,55,137,141),(8,5,67,39,46,55,139,144),(8,5,68,39,46,56,143,147),(8,5,69,39,47,57,146,150),(8,5,70,40,47,58,148,152),(8,5,71,40,48,59,151,155),(8,5,72,41,48,59,154,158),(8,5,73,41,49,60,157,161),(8,5,74,42,49,61,159,164),(8,5,75,42,50,62,162,167),(8,5,76,42,51,63,166,170),(8,5,77,43,51,64,169,173),(8,5,78,43,52,65,172,176),(8,5,79,44,52,66,175,179),(8,5,80,44,53,67,178,182),(8,6,1,24,22,23,16,21),(8,6,2,25,23,24,16,21),(8,6,3,27,24,25,16,22),(8,6,4,28,24,26,16,22),(8,6,5,29,25,28,16,22),(8,6,6,31,26,29,16,22),(8,6,7,32,27,30,17,23),(8,6,8,33,28,31,17,23),(8,6,9,35,28,33,17,23),(8,6,10,36,29,34,17,24),(8,6,11,37,30,35,17,24),(8,6,12,39,31,36,17,24),(8,6,13,40,32,38,17,25),(8,6,14,42,33,39,17,25),(8,6,15,43,34,40,18,25),(8,6,16,45,35,42,18,26),(8,6,17,46,35,43,18,26),(8,6,18,48,36,44,18,26),(8,6,19,49,37,46,18,27),(8,6,20,51,38,47,18,27),(8,6,21,52,39,49,18,27),(8,6,22,54,40,50,18,28),(8,6,23,55,41,51,19,28),(8,6,24,57,42,53,19,29),(8,6,25,59,43,54,19,29),(8,6,26,60,44,56,19,29),(8,6,27,62,45,57,19,30),(8,6,28,63,46,59,19,30),(8,6,29,65,47,60,20,31),(8,6,30,67,48,62,20,31),(8,6,31,69,49,63,20,31),(8,6,32,70,50,65,20,32),(8,6,33,72,51,67,20,32),(8,6,34,74,53,68,20,33),(8,6,35,75,54,70,21,33),(8,6,36,77,55,71,21,34),(8,6,37,79,56,73,21,34),(8,6,38,81,57,75,21,35),(8,6,39,83,58,76,21,35),(8,6,40,84,59,78,22,35),(8,6,41,86,60,80,22,36),(8,6,42,88,62,81,22,36),(8,6,43,90,63,83,22,37),(8,6,44,92,64,85,22,37),(8,6,45,94,65,86,23,38),(8,6,46,96,66,88,23,38),(8,6,47,98,67,90,23,39),(8,6,48,100,69,92,23,39),(8,6,49,102,70,93,24,40),(8,6,50,103,71,95,24,40),(8,6,51,105,72,96,24,41),(8,6,52,106,74,97,24,42),(8,6,53,107,73,98,25,42),(8,6,54,108,74,99,25,43),(8,6,55,109,75,99,25,43),(8,6,56,112,77,102,25,44),(8,6,57,114,78,104,25,44),(8,6,58,116,79,106,26,45),(8,6,59,119,81,108,26,45),(8,6,60,121,82,110,26,46),(8,6,61,123,83,112,26,47),(8,6,62,126,85,114,26,47),(8,6,63,128,86,117,27,48),(8,6,64,131,88,119,27,48),(8,6,65,134,89,121,27,49),(8,6,66,136,91,124,27,50),(8,6,67,139,92,126,28,50),(8,6,68,141,94,128,28,51),(8,6,69,144,95,131,28,52),(8,6,70,147,97,133,28,52),(8,6,71,150,99,136,29,53),(8,6,72,152,100,138,29,54),(8,6,73,155,102,141,29,55),(8,6,74,158,104,143,29,55),(8,6,75,161,105,146,30,56),(8,6,76,164,107,149,30,57),(8,6,77,167,109,151,30,58),(8,6,78,170,110,154,30,58),(8,6,79,173,112,157,31,59),(8,6,80,176,114,160,31,60),(8,7,1,22,22,22,17,23),(8,7,2,23,22,23,18,24),(8,7,3,24,23,24,19,25),(8,7,4,25,23,25,20,26),(8,7,5,25,24,26,21,27),(8,7,6,26,24,27,22,28),(8,7,7,27,25,28,23,29),(8,7,8,28,25,29,24,30),(8,7,9,29,25,30,25,31),(8,7,10,29,26,29,24,31),(8,7,11,30,26,30,25,32),(8,7,12,31,27,31,26,33),(8,7,13,31,27,32,27,34),(8,7,14,32,28,33,28,35),(8,7,15,33,28,34,29,37),(8,7,16,34,29,35,30,38),(8,7,17,35,29,36,31,39),(8,7,18,36,30,37,32,40),(8,7,19,37,30,38,33,41),(8,7,20,38,31,39,34,42),(8,7,21,39,31,40,35,43),(8,7,22,39,32,41,36,44),(8,7,23,40,32,42,37,45),(8,7,24,41,33,43,38,47),(8,7,25,42,33,45,39,48),(8,7,26,43,34,46,40,49),(8,7,27,44,34,47,41,50),(8,7,28,45,35,48,42,51),(8,7,29,46,35,49,43,53),(8,7,30,47,36,50,44,54),(8,7,31,49,36,52,46,55),(8,7,32,50,37,53,47,57),(8,7,33,51,38,54,48,58),(8,7,34,52,38,55,49,59),(8,7,35,53,39,57,50,61),(8,7,36,54,40,58,52,62),(8,7,37,55,40,59,53,63),(8,7,38,56,41,61,54,65),(8,7,39,57,41,62,55,66),(8,7,40,59,42,63,57,68),(8,7,41,60,43,65,58,69),(8,7,42,61,43,66,59,71),(8,7,43,62,44,68,60,72),(8,7,44,64,45,69,62,74),(8,7,45,65,45,71,63,75),(8,7,46,66,46,72,65,77),(8,7,47,67,47,74,66,78),(8,7,48,69,48,75,67,80),(8,7,49,70,48,77,69,82),(8,7,50,71,49,78,70,83),(8,7,51,73,50,80,72,85),(8,7,52,74,51,81,73,87),(8,7,53,76,51,83,75,88),(8,7,54,77,52,85,76,90),(8,7,55,78,53,86,78,92),(8,7,56,80,54,88,79,94),(8,7,57,81,55,90,81,95),(8,7,58,83,55,91,83,97),(8,7,59,84,56,93,84,99),(8,7,60,86,57,95,86,101),(8,7,61,88,58,97,88,103),(8,7,62,89,59,99,89,105),(8,7,63,91,60,100,91,107),(8,7,64,92,60,102,93,109),(8,7,65,94,61,104,95,111),(8,7,66,96,62,106,96,113),(8,7,67,97,63,108,98,115),(8,7,68,99,64,110,100,117),(8,7,69,101,65,112,102,119),(8,7,70,103,66,114,104,121),(8,7,71,104,67,116,106,123),(8,7,72,106,68,118,108,126),(8,7,73,108,69,120,110,128),(8,7,74,110,70,123,112,130),(8,7,75,112,71,125,114,132),(8,7,76,114,72,127,116,135),(8,7,77,115,73,129,118,137),(8,7,78,117,74,131,120,139),(8,7,79,119,75,134,122,142),(8,7,80,121,76,136,124,144),(8,8,1,21,22,21,19,23),(8,8,2,21,22,21,20,24),(8,8,3,21,22,22,22,26),(8,8,4,21,22,22,23,27),(8,8,5,21,23,22,25,28),(8,8,6,21,23,22,26,30),(8,8,7,21,23,23,27,31),(8,8,8,22,23,23,29,32),(8,8,9,22,23,23,30,34),(8,8,10,22,24,23,30,34),(8,8,11,22,24,23,31,35),(8,8,12,22,24,23,33,36),(8,8,13,22,24,24,34,37),(8,8,14,23,24,24,35,39),(8,8,15,23,25,24,37,40),(8,8,16,23,25,25,38,41),(8,8,17,23,25,25,39,43),(8,8,18,23,25,25,41,44),(8,8,19,23,25,26,42,45),(8,8,20,23,26,26,44,47),(8,8,21,24,26,26,45,48),(8,8,22,24,26,27,47,50),(8,8,23,24,26,27,48,51),(8,8,24,24,27,28,50,53),(8,8,25,24,27,28,51,54),(8,8,26,24,27,28,53,56),(8,8,27,24,27,29,55,57),(8,8,28,25,27,29,56,59),(8,8,29,25,28,30,58,60),(8,8,30,25,28,30,60,62),(8,8,31,25,28,30,61,64),(8,8,32,25,28,31,63,65),(8,8,33,25,29,31,65,67),(8,8,34,26,29,32,66,69),(8,8,35,26,29,32,68,70),(8,8,36,26,30,33,70,72),(8,8,37,26,30,33,72,74),(8,8,38,26,30,33,74,76),(8,8,39,27,30,34,76,77),(8,8,40,27,31,34,77,79),(8,8,41,27,31,35,79,81),(8,8,42,27,31,35,81,83),(8,8,43,27,31,36,83,85),(8,8,44,27,32,36,85,87),(8,8,45,28,32,37,87,89),(8,8,46,28,32,37,89,91),(8,8,47,28,33,38,91,93),(8,8,48,28,33,38,94,95),(8,8,49,29,33,39,96,97),(8,8,50,29,34,39,103,99),(8,8,51,29,34,40,105,101),(8,8,52,29,34,40,107,103),(8,8,53,29,35,41,109,105),(8,8,54,30,35,42,112,107),(8,8,55,30,35,42,114,110),(8,8,56,30,36,43,116,112),(8,8,57,30,36,43,119,114),(8,8,58,31,36,44,122,116),(8,8,59,31,37,44,125,119),(8,8,60,31,37,45,127,121),(8,8,61,31,37,46,129,123),(8,8,62,31,38,46,132,126),(8,8,63,32,38,47,135,128),(8,8,64,32,38,47,137,131),(8,8,65,32,39,48,140,133),(8,8,66,33,39,49,143,136),(8,8,67,33,40,49,146,138),(8,8,68,33,40,50,149,141),(8,8,69,33,40,51,152,144),(8,8,70,34,41,51,154,146),(8,8,71,34,41,52,157,149),(8,8,72,34,42,53,160,152),(8,8,73,34,42,54,164,155),(8,8,74,35,43,54,167,157),(8,8,75,35,43,55,170,160),(8,8,76,35,43,56,173,163),(8,8,77,36,44,57,176,166),(8,8,78,36,44,57,179,169),(8,8,79,36,45,58,182,172),(8,8,80,37,45,59,186,175),(10,2,1,19,22,21,24,20),(10,2,2,20,23,22,25,21),(10,2,3,21,23,23,25,21),(10,2,4,22,24,24,26,22),(10,2,5,23,24,25,27,23),(10,2,6,25,25,26,27,24),(10,2,7,26,25,27,28,24),(10,2,8,27,26,28,29,25),(10,2,9,28,27,29,29,26),(10,2,10,28,27,30,28,25),(10,2,11,29,27,31,29,26),(10,2,12,30,28,32,30,26),(10,2,13,31,29,33,30,27),(10,2,14,32,29,34,31,28),(10,2,15,33,30,36,32,28),(10,2,16,35,30,37,32,29),(10,2,17,36,31,38,33,30),(10,2,18,37,32,39,34,31),(10,2,19,38,32,40,34,31),(10,2,20,39,33,41,35,32),(10,2,21,40,34,42,36,33),(10,2,22,42,34,43,37,34),(10,2,23,43,35,44,37,35),(10,2,24,44,36,46,38,35),(10,2,25,45,36,47,39,36),(10,2,26,47,37,48,40,37),(10,2,27,48,38,49,40,38),(10,2,28,49,38,50,41,39),(10,2,29,51,39,52,42,40),(10,2,30,52,40,53,43,40),(10,2,31,53,41,54,44,41),(10,2,32,55,41,56,45,42),(10,2,33,56,42,57,45,43),(10,2,34,58,43,58,46,44),(10,2,35,59,44,60,47,45),(10,2,36,61,45,61,48,46),(10,2,37,62,45,62,49,47),(10,2,38,64,46,64,50,48),(10,2,39,65,47,65,51,49),(10,2,40,67,48,67,52,50),(10,2,41,68,49,68,53,51),(10,2,42,70,49,70,54,52),(10,2,43,71,50,71,55,53),(10,2,44,73,51,73,55,54),(10,2,45,75,52,74,56,55),(10,2,46,76,53,76,57,56),(10,2,47,78,54,77,59,57),(10,2,48,80,55,79,60,58),(10,2,49,81,56,81,61,60),(10,2,50,83,57,82,62,61),(10,2,51,85,58,84,63,62),(10,2,52,87,59,86,64,63),(10,2,53,89,60,87,65,64),(10,2,54,90,61,89,66,65),(10,2,55,92,62,91,67,67),(10,2,56,94,63,93,68,68),(10,2,57,96,64,94,69,69),(10,2,58,98,65,96,71,70),(10,2,59,105,66,98,72,72),(10,2,60,105,67,100,73,73),(10,2,61,105,68,102,74,74),(10,2,62,111,69,104,75,76),(10,2,63,113,70,106,77,77),(10,2,64,113,71,108,78,78),(10,2,65,113,73,110,79,80),(10,2,66,115,74,112,81,81),(10,2,67,117,75,114,82,83),(10,2,68,119,76,116,83,84),(10,2,69,127,77,118,85,85),(10,2,70,127,79,120,86,87),(10,2,71,127,80,122,87,88),(10,2,72,134,81,125,89,90),(10,2,73,136,82,127,90,92),(10,2,74,136,84,129,92,93),(10,2,75,141,85,131,93,95),(10,2,76,141,86,134,95,96),(10,2,77,147,88,136,96,98),(10,2,78,150,89,138,98,100),(10,2,79,152,90,141,99,101),(10,2,80,152,92,143,101,103),(10,3,1,17,25,20,24,20),(10,3,2,17,26,21,25,21),(10,3,3,18,28,22,25,21),(10,3,4,18,29,23,26,22),(10,3,5,19,30,24,26,23),(10,3,6,19,32,25,27,23),(10,3,7,20,33,26,27,24),(10,3,8,20,35,27,28,25),(10,3,9,21,36,27,29,25),(10,3,10,21,36,28,28,24),(10,3,11,21,37,29,28,25),(10,3,12,22,39,30,29,26),(10,3,13,22,40,31,30,26),(10,3,14,23,41,32,30,27),(10,3,15,23,43,33,31,27),(10,3,16,24,44,34,31,28),(10,3,17,24,45,35,32,29),(10,3,18,25,47,36,33,30),(10,3,19,25,48,37,33,30),(10,3,20,26,50,38,34,31),(10,3,21,26,51,39,35,32),(10,3,22,27,53,40,35,32),(10,3,23,27,54,41,36,33),(10,3,24,28,56,42,37,34),(10,3,25,28,57,43,37,35),(10,3,26,29,59,44,38,35),(10,3,27,29,61,45,39,36),(10,3,28,30,62,46,39,37),(10,3,29,30,64,47,40,38),(10,3,30,31,66,48,41,38),(10,3,31,31,67,50,42,39),(10,3,32,32,69,51,42,40),(10,3,33,33,71,52,43,41),(10,3,34,33,72,53,44,42),(10,3,35,34,74,54,45,43),(10,3,36,35,76,56,46,44),(10,3,37,35,78,57,46,44),(10,3,38,36,80,58,47,45),(10,3,39,36,82,59,48,46),(10,3,40,37,83,61,49,47),(10,3,41,38,85,62,50,48),(10,3,42,38,87,63,50,49),(10,3,43,39,89,64,51,50),(10,3,44,40,91,66,52,51),(10,3,45,40,93,67,53,52),(10,3,46,41,95,69,54,53),(10,3,47,42,97,70,55,54),(10,3,48,43,100,71,56,55),(10,3,49,43,102,73,57,56),(10,3,50,44,104,74,58,57),(10,3,51,45,106,76,59,58),(10,3,52,46,108,77,60,59),(10,3,53,46,110,79,61,60),(10,3,54,47,113,80,62,61),(10,3,55,48,115,82,63,62),(10,3,56,49,117,83,64,63),(10,3,57,50,120,85,65,65),(10,3,58,50,122,87,66,66),(10,3,59,51,125,88,67,67),(10,3,60,52,127,90,68,68),(10,3,61,53,129,92,69,69),(10,3,62,54,132,93,70,70),(10,3,63,55,135,95,71,72),(10,3,64,55,137,97,72,73),(10,3,65,56,140,99,74,74),(10,3,66,57,142,100,75,75),(10,3,67,58,145,102,76,77),(10,3,68,59,148,104,77,78),(10,3,69,60,151,106,78,79),(10,3,70,61,153,108,80,81),(10,3,71,62,156,110,81,82),(10,3,72,63,159,112,82,83),(10,3,73,64,162,114,83,85),(10,3,74,65,165,116,85,86),(10,3,75,66,168,118,86,88),(10,3,76,67,179,120,87,89),(10,3,77,68,182,122,89,91),(10,3,78,69,185,124,90,92),(10,3,79,70,188,126,91,94),(10,3,80,71,192,128,93,95),(10,4,1,18,25,20,24,19),(10,4,2,19,26,21,24,19),(10,4,3,20,28,21,24,20),(10,4,4,20,29,22,24,20),(10,4,5,21,31,23,25,20),(10,4,6,22,32,24,25,21),(10,4,7,23,34,24,25,21),(10,4,8,24,35,25,25,22),(10,4,9,24,37,26,25,22),(10,4,10,24,37,27,25,21),(10,4,11,25,38,28,25,22),(10,4,12,26,39,28,25,22),(10,4,13,27,41,29,25,22),(10,4,14,27,42,30,25,23),(10,4,15,28,44,30,26,23),(10,4,16,29,45,31,26,24),(10,4,17,30,46,32,26,24),(10,4,18,31,48,33,26,24),(10,4,19,32,50,33,26,25),(10,4,20,32,51,34,27,25),(10,4,21,33,53,35,27,26),(10,4,22,34,54,36,27,26),(10,4,23,35,56,37,27,27),(10,4,24,36,57,37,28,27),(10,4,25,37,59,38,28,28),(10,4,26,38,61,39,28,28),(10,4,27,39,62,40,28,28),(10,4,28,40,64,41,28,29),(10,4,29,40,66,42,29,29),(10,4,30,41,68,42,29,30),(10,4,31,42,69,43,29,30),(10,4,32,43,71,44,29,31),(10,4,33,44,73,45,30,31),(10,4,34,45,75,46,30,32),(10,4,35,46,77,47,30,32),(10,4,36,48,79,48,31,33),(10,4,37,49,80,49,31,34),(10,4,38,50,82,50,31,34),(10,4,39,51,84,51,31,35),(10,4,40,52,86,52,32,35),(10,4,41,53,88,53,32,36),(10,4,42,54,90,54,32,36),(10,4,43,55,92,55,32,37),(10,4,44,56,95,56,33,37),(10,4,45,58,97,57,33,38),(10,4,46,59,99,58,33,39),(10,4,47,60,101,59,34,39),(10,4,48,61,103,60,34,40),(10,4,49,62,105,62,34,41),(10,4,50,64,108,63,35,41),(10,4,51,65,115,64,35,42),(10,4,52,66,115,65,35,42),(10,4,53,67,120,66,36,43),(10,4,54,69,122,67,36,44),(10,4,55,70,124,69,36,44),(10,4,56,71,128,70,37,45),(10,4,57,73,130,71,37,46),(10,4,58,74,133,72,37,47),(10,4,59,76,135,74,38,47),(10,4,60,77,138,75,38,48),(10,4,61,78,141,76,38,49),(10,4,62,80,143,78,39,49),(10,4,63,81,143,79,39,50),(10,4,64,83,150,80,39,51),(10,4,65,84,152,82,40,52),(10,4,66,86,155,83,40,53),(10,4,67,87,158,85,41,53),(10,4,68,89,161,86,41,54),(10,4,69,91,164,87,41,55),(10,4,70,92,164,89,42,56),(10,4,71,94,171,90,42,57),(10,4,72,96,174,92,43,57),(10,4,73,97,177,94,43,58),(10,4,74,99,180,95,44,59),(10,4,75,101,183,97,44,60),(10,4,76,102,186,98,44,61),(10,4,77,104,190,100,45,62),(10,4,78,106,194,102,45,63),(10,4,79,108,197,103,46,64),(10,4,80,110,200,105,46,65),(10,5,1,17,22,19,26,22),(10,5,2,17,22,19,27,23),(10,5,3,17,22,20,29,25),(10,5,4,17,23,20,30,26),(10,5,5,18,23,20,31,27),(10,5,6,18,23,21,33,29),(10,5,7,18,23,21,34,30),(10,5,8,18,24,22,35,32),(10,5,9,18,24,22,37,33),(10,5,10,19,24,23,36,32),(10,5,11,19,24,24,37,33),(10,5,12,19,25,24,38,35),(10,5,13,19,25,24,39,36),(10,5,14,19,25,25,41,37),(10,5,15,20,25,25,42,39),(10,5,16,20,26,26,43,40),(10,5,17,20,26,26,45,41),(10,5,18,20,26,26,46,43),(10,5,19,20,27,27,47,44),(10,5,20,21,27,27,49,46),(10,5,21,21,27,28,50,47),(10,5,22,21,27,28,52,49),(10,5,23,21,28,29,53,50),(10,5,24,22,28,29,55,52),(10,5,25,22,28,30,56,53),(10,5,26,22,29,30,58,55),(10,5,27,22,29,30,59,57),(10,5,28,22,29,31,61,58),(10,5,29,23,30,31,62,60),(10,5,30,23,30,32,64,62),(10,5,31,23,30,32,66,63),(10,5,32,23,31,33,67,65),(10,5,33,24,31,33,69,67),(10,5,34,24,31,34,71,68),(10,5,35,24,32,34,72,70),(10,5,36,25,32,35,74,72),(10,5,37,25,32,36,76,74),(10,5,38,25,33,36,78,76),(10,5,39,25,33,37,79,78),(10,5,40,26,33,37,81,79),(10,5,41,26,34,38,83,81),(10,5,42,26,34,38,85,83),(10,5,43,26,35,39,87,85),(10,5,44,27,35,39,89,87),(10,5,45,27,35,40,91,89),(10,5,46,27,36,41,93,91),(10,5,47,28,36,41,95,93),(10,5,48,28,37,42,97,96),(10,5,49,28,37,43,99,98),(10,5,50,29,37,43,105,100),(10,5,51,29,38,44,108,102),(10,5,52,29,38,44,110,104),(10,5,53,30,39,45,112,106),(10,5,54,30,39,46,114,109),(10,5,55,30,40,46,117,111),(10,5,56,31,40,47,119,113),(10,5,57,31,41,48,121,116),(10,5,58,31,41,49,123,118),(10,5,59,32,42,49,126,121),(10,5,60,32,42,50,129,123),(10,5,61,32,42,51,131,125),(10,5,62,33,43,51,134,128),(10,5,63,33,43,52,136,131),(10,5,64,33,44,53,139,133),(10,5,65,34,45,54,141,136),(10,5,66,34,45,55,144,138),(10,5,67,35,46,55,146,141),(10,5,68,35,46,56,150,144),(10,5,69,35,47,57,153,147),(10,5,70,36,47,58,155,149),(10,5,71,36,48,59,158,152),(10,5,72,37,48,59,161,155),(10,5,73,37,49,60,164,158),(10,5,74,38,49,61,166,161),(10,5,75,38,50,62,169,164),(10,5,76,38,51,63,173,167),(10,5,77,39,51,64,176,170),(10,5,78,39,52,65,179,173),(10,5,79,40,52,66,182,176),(10,5,80,40,53,67,185,179),(10,6,1,18,23,21,24,20),(10,6,2,19,24,22,24,20),(10,6,3,21,25,23,24,21),(10,6,4,22,25,25,24,21),(10,6,5,23,26,26,24,21),(10,6,6,25,27,27,24,21),(10,6,7,26,28,28,24,22),(10,6,8,27,29,29,25,22),(10,6,9,29,29,31,25,22),(10,6,10,30,30,32,25,23),(10,6,11,32,31,33,25,23),(10,6,12,33,32,34,25,23),(10,6,13,34,33,36,25,24),(10,6,14,36,34,37,25,24),(10,6,15,37,35,38,25,24),(10,6,16,39,36,40,25,25),(10,6,17,40,36,41,25,25),(10,6,18,42,37,43,26,25),(10,6,19,43,38,44,26,26),(10,6,20,45,39,45,26,26),(10,6,21,47,40,47,26,26),(10,6,22,48,41,48,26,27),(10,6,23,50,42,50,26,27),(10,6,24,51,43,51,26,28),(10,6,25,53,44,52,27,28),(10,6,26,55,45,54,27,28),(10,6,27,56,46,55,27,29),(10,6,28,58,47,57,27,29),(10,6,29,59,48,58,27,30),(10,6,30,61,49,60,27,30),(10,6,31,63,50,62,27,30),(10,6,32,65,51,63,28,31),(10,6,33,66,52,65,28,31),(10,6,34,68,53,66,28,32),(10,6,35,70,55,68,28,32),(10,6,36,72,56,69,28,33),(10,6,37,73,57,71,29,33),(10,6,38,75,58,73,29,34),(10,6,39,77,59,74,29,34),(10,6,40,79,60,76,29,35),(10,6,41,81,61,78,29,35),(10,6,42,82,62,79,30,35),(10,6,43,84,64,81,30,36),(10,6,44,86,65,83,30,36),(10,6,45,88,66,85,30,37),(10,6,46,90,67,86,30,37),(10,6,47,92,68,88,31,38),(10,6,48,94,69,90,31,38),(10,6,49,96,69,92,31,39),(10,6,50,98,70,93,31,40),(10,6,51,100,71,95,32,40),(10,6,52,102,72,96,32,41),(10,6,53,103,73,97,32,41),(10,6,54,104,74,98,32,41),(10,6,55,105,75,99,32,40),(10,6,56,108,77,102,32,41),(10,6,57,110,78,104,32,41),(10,6,58,112,79,106,33,42),(10,6,59,115,81,108,33,42),(10,6,60,117,82,110,33,43),(10,6,61,119,83,112,33,44),(10,6,62,122,85,114,33,44),(10,6,63,124,86,117,34,45),(10,6,64,127,88,119,34,45),(10,6,65,130,89,121,34,46),(10,6,66,132,91,124,34,47),(10,6,67,135,92,126,35,47),(10,6,68,137,94,128,35,48),(10,6,69,140,95,131,35,49),(10,6,70,143,97,133,35,49),(10,6,71,146,99,136,36,50),(10,6,72,148,100,138,36,51),(10,6,73,151,102,141,36,52),(10,6,74,154,104,143,36,52),(10,6,75,157,105,146,37,53),(10,6,76,160,107,149,37,54),(10,6,77,163,109,151,37,55),(10,6,78,166,110,154,37,55),(10,6,79,169,112,157,38,56),(10,6,80,172,114,160,38,57),(10,8,1,17,22,19,27,21),(10,8,2,17,22,19,28,22),(10,8,3,17,22,20,30,24),(10,8,4,17,22,20,31,25),(10,8,5,17,23,20,32,26),(10,8,6,17,23,20,34,28),(10,8,7,18,23,21,35,29),(10,8,8,18,23,21,37,30),(10,8,9,18,23,21,38,32),(10,8,10,18,24,23,37,31),(10,8,11,18,24,23,38,32),(10,8,12,18,24,23,40,33),(10,8,13,18,24,24,41,34),(10,8,14,19,24,24,42,36),(10,8,15,19,25,24,44,37),(10,8,16,19,25,25,45,38),(10,8,17,19,25,25,46,40),(10,8,18,19,25,25,48,41),(10,8,19,19,25,26,49,42),(10,8,20,19,26,26,51,44),(10,8,21,20,26,26,52,45),(10,8,22,20,26,27,54,47),(10,8,23,20,26,27,55,48),(10,8,24,20,27,28,57,50),(10,8,25,20,27,28,58,51),(10,8,26,20,27,28,60,53),(10,8,27,20,27,29,62,54),(10,8,28,21,27,29,63,56),(10,8,29,21,28,30,65,57),(10,8,30,21,28,30,67,59),(10,8,31,21,28,30,68,61),(10,8,32,21,28,31,70,62),(10,8,33,21,29,31,72,64),(10,8,34,22,29,32,73,66),(10,8,35,22,29,32,75,67),(10,8,36,22,30,33,77,69),(10,8,37,22,30,33,79,71),(10,8,38,22,30,33,81,73),(10,8,39,23,30,34,83,74),(10,8,40,23,31,34,84,76),(10,8,41,23,31,35,86,78),(10,8,42,23,31,35,88,80),(10,8,43,23,31,36,90,82),(10,8,44,23,32,36,92,84),(10,8,45,24,32,37,94,86),(10,8,46,24,32,37,96,88),(10,8,47,24,33,38,98,90),(10,8,48,24,33,38,101,92),(10,8,49,25,33,39,103,94),(10,8,50,25,34,39,110,96),(10,8,51,25,34,40,112,98),(10,8,52,25,34,40,114,100),(10,8,53,25,35,41,116,102),(10,8,54,26,35,42,119,104),(10,8,55,26,35,42,121,107),(10,8,56,26,36,43,123,109),(10,8,57,26,36,43,126,111),(10,8,58,27,36,44,129,113),(10,8,59,27,37,44,132,116),(10,8,60,27,37,45,134,118),(10,8,61,27,37,46,136,120),(10,8,62,27,38,46,139,123),(10,8,63,28,38,47,142,125),(10,8,64,28,38,47,144,128),(10,8,65,28,39,48,147,130),(10,8,66,29,39,49,150,133),(10,8,67,29,40,49,153,135),(10,8,68,29,40,50,156,138),(10,8,69,29,40,51,159,141),(10,8,70,30,41,51,161,143),(10,8,71,30,41,52,164,146),(10,8,72,30,42,53,167,149),(10,8,73,30,42,54,171,152),(10,8,74,31,43,54,174,154),(10,8,75,31,43,55,177,157),(10,8,76,31,43,56,180,160),(10,8,77,32,44,57,183,163),(10,8,78,32,44,57,186,166),(10,8,79,32,45,58,189,169),(10,8,80,33,45,59,193,172),(10,9,1,17,22,20,26,21),(10,9,2,17,22,21,27,22),(10,9,3,18,23,21,28,23),(10,9,4,18,23,22,29,24),(10,9,5,18,23,22,31,26),(10,9,6,18,24,23,32,27),(10,9,7,19,24,23,33,28),(10,9,8,19,25,24,34,29),(10,9,9,19,25,25,36,30),(10,9,10,20,25,26,34,30),(10,9,11,20,26,26,36,31),(10,9,12,20,26,27,37,32),(10,9,13,21,26,27,38,34),(10,9,14,21,27,28,39,35),(10,9,15,21,27,29,40,36),(10,9,16,22,28,29,41,37),(10,9,17,22,28,30,43,39),(10,9,18,22,28,30,44,40),(10,9,19,23,29,31,45,41),(10,9,20,23,29,32,46,43),(10,9,21,23,30,32,48,44),(10,9,22,24,30,33,49,45),(10,9,23,24,31,34,50,47),(10,9,24,25,31,34,52,48),(10,9,25,25,32,35,53,50),(10,9,26,25,32,36,54,51),(10,9,27,26,32,36,56,52),(10,9,28,26,33,37,57,54),(10,9,29,27,33,38,59,55),(10,9,30,27,34,38,60,57),(10,9,31,27,34,39,61,59),(10,9,32,28,35,40,63,60),(10,9,33,28,35,41,64,62),(10,9,34,29,36,41,66,63),(10,9,35,29,36,42,67,65),(10,9,36,30,37,43,69,67),(10,9,37,30,38,44,71,68),(10,9,38,30,38,45,72,70),(10,9,39,31,39,45,74,72),(10,9,40,31,39,46,75,73),(10,9,41,32,40,47,77,75),(10,9,42,32,40,48,79,77),(10,9,43,33,41,49,80,79),(10,9,44,33,41,50,82,80),(10,9,45,34,42,50,84,82),(10,9,46,34,43,51,86,84),(10,9,47,35,43,52,87,86),(10,9,48,35,44,53,89,88),(10,9,49,36,45,54,91,90),(10,9,50,36,45,55,97,92),(10,9,51,37,46,56,99,94),(10,9,52,37,46,57,101,96),(10,9,53,38,47,58,103,98),(10,9,54,39,48,59,105,100),(10,9,55,39,48,60,108,102),(10,9,56,40,49,61,110,104),(10,9,57,40,50,62,112,106),(10,9,58,41,51,63,114,109),(10,9,59,41,51,64,116,111),(10,9,60,42,52,65,118,113),(10,9,61,43,53,66,120,115),(10,9,62,43,53,67,122,118),(10,9,63,44,54,68,125,120),(10,9,64,44,55,69,127,122),(10,9,65,45,56,71,130,125),(10,9,66,46,57,72,132,127),(10,9,67,46,57,73,135,130),(10,9,68,47,58,74,137,132),(10,9,69,48,59,75,139,134),(10,9,70,48,60,76,142,137),(10,9,71,49,61,78,144,140),(10,9,72,50,61,79,147,142),(10,9,73,51,62,80,150,145),(10,9,74,51,63,81,153,148),(10,9,75,52,64,83,155,150),(10,9,76,53,65,84,158,153),(10,9,77,54,66,85,161,156),(10,9,78,54,67,87,163,159),(10,9,79,55,68,88,166,162),(10,9,80,56,69,89,169,164),(11,1,1,24,17,21,21,22),(11,1,2,25,18,22,21,22),(11,1,3,27,19,23,21,23),(11,1,4,28,19,25,21,23),(11,1,5,29,20,26,21,23),(11,1,6,31,21,27,21,23),(11,1,7,32,22,28,21,24),(11,1,8,33,23,29,22,24),(11,1,9,35,24,31,22,24),(11,1,10,34,23,31,21,25),(11,1,11,36,24,33,21,25),(11,1,12,37,25,34,21,25),(11,1,13,38,26,35,21,26),(11,1,14,40,27,36,22,26),(11,1,15,41,27,37,22,26),(11,1,16,42,28,38,22,27),(11,1,17,43,29,40,22,27),(11,1,18,45,30,41,22,27),(11,1,19,46,31,42,22,28),(11,1,20,48,32,43,22,28),(11,1,21,49,32,45,23,28),(11,1,22,50,33,46,23,29),(11,1,23,52,34,47,23,29),(11,1,24,53,35,49,23,30),(11,1,25,55,36,50,23,30),(11,1,26,56,37,51,23,30),(11,1,27,58,38,53,23,31),(11,1,28,59,39,54,24,31),(11,1,29,61,40,56,24,32),(11,1,30,63,41,57,24,32),(11,1,31,64,42,58,24,32),(11,1,32,66,43,60,24,33),(11,1,33,67,44,61,24,33),(11,1,34,69,45,63,25,34),(11,1,35,71,46,64,25,34),(11,1,36,73,47,66,25,35),(11,1,37,74,48,68,25,35),(11,1,38,76,49,69,25,35),(11,1,39,78,50,71,26,36),(11,1,40,80,51,72,26,36),(11,1,41,81,53,74,26,37),(11,1,42,83,54,76,26,37),(11,1,43,85,55,77,26,38),(11,1,44,87,56,79,26,38),(11,1,45,89,57,81,27,39),(11,1,46,91,58,83,27,39),(11,1,47,93,60,84,27,40),(11,1,48,95,61,86,27,40),(11,1,49,97,62,88,28,41),(11,1,50,99,63,90,28,41),(11,1,51,101,65,92,28,42),(11,1,52,103,66,94,28,42),(11,1,53,105,67,96,28,43),(11,1,54,107,69,98,29,44),(11,1,55,110,70,100,29,44),(11,1,56,112,71,102,29,45),(11,1,57,114,73,104,29,45),(11,1,58,116,74,106,30,46),(11,1,59,119,76,108,30,46),(11,1,60,121,77,110,30,47),(11,1,61,123,78,112,30,48),(11,1,62,126,80,114,30,48),(11,1,63,128,81,117,31,49),(11,1,64,131,83,119,31,49),(11,1,65,133,85,121,31,50),(11,1,66,136,86,123,32,51),(11,1,67,138,88,126,32,51),(11,1,68,141,89,128,32,52),(11,1,69,143,91,130,32,53),(11,1,70,146,93,133,33,53),(11,1,71,149,94,135,33,54),(11,1,72,151,96,138,33,55),(11,1,73,154,98,140,33,56),(11,1,74,157,99,143,34,56),(11,1,75,160,101,145,34,57),(11,1,76,163,103,148,34,58),(11,1,77,166,105,151,35,59),(11,1,78,169,106,153,35,59),(11,1,79,172,108,156,35,60),(11,1,80,175,110,159,36,61),(11,2,1,23,17,21,21,23),(11,2,2,24,18,22,22,24),(11,2,3,25,18,23,22,24),(11,2,4,26,19,24,23,25),(11,2,5,27,19,25,24,26),(11,2,6,29,20,26,24,26),(11,2,7,30,21,27,25,27),(11,2,8,31,21,28,26,28),(11,2,9,32,22,29,26,29),(11,2,10,32,22,30,25,29),(11,2,11,33,22,31,26,30),(11,2,12,34,23,32,27,30),(11,2,13,35,24,33,27,31),(11,2,14,36,24,34,28,32),(11,2,15,37,25,36,29,32),(11,2,16,39,25,37,29,33),(11,2,17,40,26,38,30,34),(11,2,18,41,27,39,31,35),(11,2,19,42,27,40,31,35),(11,2,20,43,28,41,32,36),(11,2,21,44,29,42,33,37),(11,2,22,46,29,43,34,38),(11,2,23,47,30,44,34,39),(11,2,24,48,31,46,35,39),(11,2,25,49,31,47,36,40),(11,2,26,51,32,48,37,41),(11,2,27,52,33,49,37,42),(11,2,28,53,33,50,38,43),(11,2,29,55,34,52,39,44),(11,2,30,56,35,53,40,44),(11,2,31,57,36,54,41,45),(11,2,32,59,36,56,42,46),(11,2,33,60,37,57,42,47),(11,2,34,62,38,58,43,48),(11,2,35,63,39,60,44,49),(11,2,36,65,40,61,45,50),(11,2,37,66,40,62,46,51),(11,2,38,68,41,64,47,52),(11,2,39,69,42,65,48,53),(11,2,40,71,43,67,49,54),(11,2,41,72,44,68,50,55),(11,2,42,74,44,70,51,56),(11,2,43,75,45,71,52,57),(11,2,44,77,46,73,52,58),(11,2,45,79,47,74,53,59),(11,2,46,80,48,76,54,60),(11,2,47,82,49,77,56,61),(11,2,48,84,50,79,57,62),(11,2,49,85,51,81,58,64),(11,2,50,87,52,82,59,65),(11,2,51,89,53,84,60,66),(11,2,52,91,54,86,61,67),(11,2,53,93,55,87,62,68),(11,2,54,94,56,89,63,69),(11,2,55,96,57,91,64,71),(11,2,56,98,58,93,65,72),(11,2,57,100,59,94,66,73),(11,2,58,102,60,96,68,74),(11,2,59,109,61,98,69,76),(11,2,60,109,62,100,70,77),(11,2,61,109,63,102,71,78),(11,2,62,115,64,104,72,80),(11,2,63,117,65,106,74,81),(11,2,64,117,66,108,75,82),(11,2,65,117,68,110,76,84),(11,2,66,119,69,112,78,85),(11,2,67,121,70,114,79,87),(11,2,68,123,71,116,80,88),(11,2,69,131,72,118,82,89),(11,2,70,131,74,120,83,91),(11,2,71,131,75,122,84,92),(11,2,72,138,76,125,86,94),(11,2,73,140,77,127,87,96),(11,2,74,140,79,129,89,97),(11,2,75,145,80,131,90,99),(11,2,76,145,81,134,92,100),(11,2,77,151,83,136,93,102),(11,2,78,154,84,138,95,104),(11,2,79,156,85,141,96,105),(11,2,80,156,87,143,98,107),(11,3,1,21,20,20,21,23),(11,3,2,21,21,21,22,24),(11,3,3,22,23,22,22,24),(11,3,4,22,24,23,23,25),(11,3,5,23,25,24,23,25),(11,3,6,23,27,25,24,26),(11,3,7,24,28,26,24,27),(11,3,8,24,30,27,25,27),(11,3,9,25,31,27,26,28),(11,3,10,25,31,28,25,28),(11,3,11,25,32,29,25,29),(11,3,12,26,34,30,26,30),(11,3,13,26,35,31,27,30),(11,3,14,27,36,32,27,31),(11,3,15,27,38,33,28,31),(11,3,16,28,39,34,28,32),(11,3,17,28,40,35,29,33),(11,3,18,29,42,36,30,34),(11,3,19,29,43,37,30,34),(11,3,20,30,45,38,31,35),(11,3,21,30,46,39,32,36),(11,3,22,31,48,40,32,36),(11,3,23,31,49,41,33,37),(11,3,24,32,51,42,34,38),(11,3,25,32,52,43,34,39),(11,3,26,33,54,44,35,39),(11,3,27,33,56,45,36,40),(11,3,28,34,57,46,36,41),(11,3,29,34,59,47,37,42),(11,3,30,35,61,48,38,42),(11,3,31,35,62,50,39,43),(11,3,32,36,64,51,39,44),(11,3,33,37,66,52,40,45),(11,3,34,37,67,53,41,46),(11,3,35,38,69,54,42,47),(11,3,36,39,71,56,43,48),(11,3,37,39,73,57,43,48),(11,3,38,40,75,58,44,49),(11,3,39,40,77,59,45,50),(11,3,40,41,78,61,46,51),(11,3,41,42,80,62,47,52),(11,3,42,42,82,63,47,53),(11,3,43,43,84,64,48,54),(11,3,44,44,86,66,49,55),(11,3,45,44,88,67,50,56),(11,3,46,45,90,69,51,57),(11,3,47,46,92,70,52,58),(11,3,48,47,95,71,53,59),(11,3,49,47,97,73,54,60),(11,3,50,48,99,74,55,61),(11,3,51,49,101,76,56,62),(11,3,52,50,103,77,57,63),(11,3,53,50,105,79,58,64),(11,3,54,51,108,80,59,65),(11,3,55,52,110,82,60,66),(11,3,56,53,112,83,61,67),(11,3,57,54,115,85,62,69),(11,3,58,54,117,87,63,70),(11,3,59,55,120,88,64,71),(11,3,60,56,122,90,65,72),(11,3,61,57,124,92,66,73),(11,3,62,58,127,93,67,74),(11,3,63,59,130,95,68,76),(11,3,64,59,132,97,69,77),(11,3,65,60,135,99,71,78),(11,3,66,61,137,100,72,79),(11,3,67,62,140,102,73,81),(11,3,68,63,143,104,74,82),(11,3,69,64,146,106,75,83),(11,3,70,65,148,108,77,85),(11,3,71,66,151,110,78,86),(11,3,72,67,154,112,79,87),(11,3,73,68,157,114,80,89),(11,3,74,69,160,116,82,90),(11,3,75,70,163,118,83,92),(11,3,76,71,174,120,84,93),(11,3,77,72,177,122,86,95),(11,3,78,73,180,124,87,96),(11,3,79,74,183,126,88,98),(11,3,80,75,187,128,90,99),(11,5,1,21,17,19,23,25),(11,5,2,21,17,19,24,26),(11,5,3,21,17,20,26,28),(11,5,4,21,18,20,27,29),(11,5,5,22,18,20,28,30),(11,5,6,22,18,21,30,32),(11,5,7,22,18,21,31,33),(11,5,8,22,19,22,32,35),(11,5,9,22,19,22,34,36),(11,5,10,23,19,23,33,36),(11,5,11,23,19,24,34,37),(11,5,12,23,20,24,35,39),(11,5,13,23,20,24,36,40),(11,5,14,23,20,25,38,41),(11,5,15,24,20,25,39,43),(11,5,16,24,21,26,40,44),(11,5,17,24,21,26,42,45),(11,5,18,24,21,26,43,47),(11,5,19,24,22,27,44,48),(11,5,20,25,22,27,46,50),(11,5,21,25,22,28,47,51),(11,5,22,25,22,28,49,53),(11,5,23,25,23,29,50,54),(11,5,24,26,23,29,52,56),(11,5,25,26,23,30,53,57),(11,5,26,26,24,30,55,59),(11,5,27,26,24,30,56,61),(11,5,28,26,24,31,58,62),(11,5,29,27,25,31,59,64),(11,5,30,27,25,32,61,66),(11,5,31,27,25,32,63,67),(11,5,32,27,26,33,64,69),(11,5,33,28,26,33,66,71),(11,5,34,28,26,34,68,72),(11,5,35,28,27,34,69,74),(11,5,36,29,27,35,71,76),(11,5,37,29,27,36,73,78),(11,5,38,29,28,36,75,80),(11,5,39,29,28,37,76,82),(11,5,40,30,28,37,78,83),(11,5,41,30,29,38,80,85),(11,5,42,30,29,38,82,87),(11,5,43,30,30,39,84,89),(11,5,44,31,30,39,86,91),(11,5,45,31,30,40,88,93),(11,5,46,31,31,41,90,95),(11,5,47,32,31,41,92,97),(11,5,48,32,32,42,94,100),(11,5,49,32,32,43,96,102),(11,5,50,33,32,43,102,104),(11,5,51,33,33,44,105,106),(11,5,52,33,33,44,107,108),(11,5,53,34,34,45,109,110),(11,5,54,34,34,46,111,113),(11,5,55,34,35,46,114,115),(11,5,56,35,35,47,116,117),(11,5,57,35,36,48,118,120),(11,5,58,35,36,49,120,122),(11,5,59,36,37,49,123,125),(11,5,60,36,37,50,126,127),(11,5,61,36,37,51,128,129),(11,5,62,37,38,51,131,132),(11,5,63,37,38,52,133,135),(11,5,64,37,39,53,136,137),(11,5,65,38,40,54,138,140),(11,5,66,38,40,55,141,142),(11,5,67,39,41,55,143,145),(11,5,68,39,41,56,147,148),(11,5,69,39,42,57,150,151),(11,5,70,40,42,58,152,153),(11,5,71,40,43,59,155,156),(11,5,72,41,43,59,158,159),(11,5,73,41,44,60,161,162),(11,5,74,42,44,61,163,165),(11,5,75,42,45,62,166,168),(11,5,76,42,46,63,170,171),(11,5,77,43,46,64,173,174),(11,5,78,43,47,65,176,177),(11,5,79,44,47,66,179,180),(11,5,80,44,48,67,182,183),(11,6,1,24,17,21,21,22),(11,6,2,25,18,22,21,22),(11,6,3,27,19,23,21,23),(11,6,4,28,19,25,21,23),(11,6,5,29,20,26,21,23),(11,6,6,31,21,27,21,23),(11,6,7,32,22,28,21,24),(11,6,8,33,23,29,22,24),(11,6,9,35,24,31,22,24),(11,6,10,36,24,32,22,25),(11,6,11,37,25,33,22,25),(11,6,12,39,26,34,22,25),(11,6,13,40,27,36,22,26),(11,6,14,42,28,37,22,26),(11,6,15,43,29,38,22,26),(11,6,16,45,30,40,22,27),(11,6,17,46,31,41,23,27),(11,6,18,48,32,43,23,27),(11,6,19,49,33,44,23,28),(11,6,20,51,34,45,23,28),(11,6,21,52,34,47,23,28),(11,6,22,54,35,48,23,29),(11,6,23,55,36,50,23,29),(11,6,24,57,37,51,24,30),(11,6,25,59,38,52,24,30),(11,6,26,60,39,54,24,30),(11,6,27,62,40,55,24,31),(11,6,28,63,41,57,24,31),(11,6,29,65,43,58,24,32),(11,6,30,67,44,60,24,32),(11,6,31,69,45,62,25,32),(11,6,32,70,46,63,25,33),(11,6,33,72,47,65,25,33),(11,6,34,74,48,66,25,34),(11,6,35,75,49,68,25,34),(11,6,36,77,50,69,26,35),(11,6,37,79,51,71,26,35),(11,6,38,81,52,73,26,35),(11,6,39,83,53,74,26,36),(11,6,40,84,55,76,26,36),(11,6,41,86,56,78,27,37),(11,6,42,88,57,79,27,37),(11,6,43,90,58,81,27,38),(11,6,44,92,59,83,27,38),(11,6,45,94,60,85,27,39),(11,6,46,96,62,86,28,39),(11,6,47,98,63,88,28,40),(11,6,48,100,64,90,28,40),(11,6,49,102,65,92,28,41),(11,6,50,103,65,93,29,41),(11,6,51,105,66,94,29,42),(11,6,52,106,67,95,29,42),(11,6,53,107,68,96,29,43),(11,6,54,108,69,97,30,44),(11,6,55,109,70,99,29,44),(11,6,56,112,72,102,29,45),(11,6,57,114,73,104,29,45),(11,6,58,116,74,106,30,46),(11,6,59,119,76,108,30,46),(11,6,60,121,77,110,30,47),(11,6,61,123,78,112,30,48),(11,6,62,126,80,114,30,48),(11,6,63,128,81,117,31,49),(11,6,64,131,83,119,31,49),(11,6,65,134,84,121,31,50),(11,6,66,136,86,124,31,51),(11,6,67,139,87,126,32,51),(11,6,68,141,89,128,32,52),(11,6,69,144,90,131,32,53),(11,6,70,147,92,133,32,53),(11,6,71,150,94,136,33,54),(11,6,72,152,95,138,33,55),(11,6,73,155,97,141,33,56),(11,6,74,158,99,143,33,56),(11,6,75,161,100,146,34,57),(11,6,76,164,102,149,34,58),(11,6,77,167,104,151,34,59),(11,6,78,170,105,154,34,59),(11,6,79,173,107,157,35,60),(11,6,80,176,109,160,35,61),(11,7,1,22,17,20,22,24),(11,7,2,23,17,21,23,25),(11,7,3,24,18,22,24,26),(11,7,4,25,18,23,25,27),(11,7,5,25,19,24,26,28),(11,7,6,26,19,25,27,29),(11,7,7,27,20,26,28,30),(11,7,8,28,20,27,28,31),(11,7,9,29,21,28,29,32),(11,7,10,29,21,29,28,32),(11,7,11,30,21,30,29,33),(11,7,12,31,22,31,30,34),(11,7,13,31,22,32,31,35),(11,7,14,32,23,33,32,36),(11,7,15,33,23,34,33,38),(11,7,16,34,24,35,34,39),(11,7,17,35,24,36,35,40),(11,7,18,36,25,37,36,41),(11,7,19,37,25,38,37,42),(11,7,20,38,26,39,38,43),(11,7,21,39,26,40,39,44),(11,7,22,39,27,41,40,45),(11,7,23,40,27,42,41,46),(11,7,24,41,28,43,42,48),(11,7,25,42,28,45,43,49),(11,7,26,43,29,46,44,50),(11,7,27,44,29,47,45,51),(11,7,28,45,30,48,46,52),(11,7,29,46,30,49,47,54),(11,7,30,47,31,50,48,55),(11,7,31,49,31,52,50,56),(11,7,32,50,32,53,51,58),(11,7,33,51,33,54,52,59),(11,7,34,52,33,55,53,60),(11,7,35,53,34,57,54,62),(11,7,36,54,35,58,56,63),(11,7,37,55,35,59,57,64),(11,7,38,56,36,61,58,66),(11,7,39,57,36,62,59,67),(11,7,40,59,37,63,61,69),(11,7,41,60,38,65,62,70),(11,7,42,61,38,66,63,72),(11,7,43,62,39,68,64,73),(11,7,44,64,40,69,66,75),(11,7,45,65,40,71,67,76),(11,7,46,66,41,72,69,78),(11,7,47,67,42,74,70,79),(11,7,48,69,43,75,71,81),(11,7,49,70,43,77,73,83),(11,7,50,71,44,78,74,84),(11,7,51,73,45,80,76,86),(11,7,52,74,46,81,77,88),(11,7,53,76,46,83,79,89),(11,7,54,77,47,85,80,91),(11,7,55,78,48,86,82,93),(11,7,56,80,49,88,83,95),(11,7,57,81,50,90,85,96),(11,7,58,83,50,91,87,98),(11,7,59,84,51,93,88,100),(11,7,60,86,52,95,90,102),(11,7,61,88,53,97,92,104),(11,7,62,89,54,99,93,106),(11,7,63,91,55,100,95,108),(11,7,64,92,55,102,97,110),(11,7,65,94,56,104,99,112),(11,7,66,96,57,106,100,114),(11,7,67,97,58,108,102,116),(11,7,68,99,59,110,104,118),(11,7,69,101,60,112,106,120),(11,7,70,103,61,114,108,122),(11,7,71,104,62,116,110,124),(11,7,72,106,63,118,112,127),(11,7,73,108,64,120,114,129),(11,7,74,110,65,123,116,131),(11,7,75,112,66,125,118,133),(11,7,76,114,67,127,120,136),(11,7,77,115,68,129,122,138),(11,7,78,117,69,131,124,140),(11,7,79,119,70,134,126,143),(11,7,80,121,71,136,128,145),(11,8,1,21,17,19,24,24),(11,8,2,21,17,19,25,25),(11,8,3,21,17,20,27,27),(11,8,4,21,17,20,28,28),(11,8,5,21,18,20,29,29),(11,8,6,21,18,20,31,31),(11,8,7,21,18,21,32,32),(11,8,8,22,18,21,34,33),(11,8,9,22,18,21,35,35),(11,8,10,22,19,23,34,35),(11,8,11,22,19,23,35,36),(11,8,12,22,19,23,37,37),(11,8,13,22,19,24,38,38),(11,8,14,23,19,24,39,40),(11,8,15,23,20,24,41,41),(11,8,16,23,20,25,42,42),(11,8,17,23,20,25,43,44),(11,8,18,23,20,25,45,45),(11,8,19,23,20,26,46,46),(11,8,20,23,21,26,48,48),(11,8,21,24,21,26,49,49),(11,8,22,24,21,27,51,51),(11,8,23,24,21,27,52,52),(11,8,24,24,22,28,54,54),(11,8,25,24,22,28,55,55),(11,8,26,24,22,28,57,57),(11,8,27,24,22,29,59,58),(11,8,28,25,22,29,60,60),(11,8,29,25,23,30,62,61),(11,8,30,25,23,30,64,63),(11,8,31,25,23,30,65,65),(11,8,32,25,23,31,67,66),(11,8,33,25,24,31,69,68),(11,8,34,26,24,32,70,70),(11,8,35,26,24,32,72,71),(11,8,36,26,25,33,74,73),(11,8,37,26,25,33,76,75),(11,8,38,26,25,33,78,77),(11,8,39,27,25,34,80,78),(11,8,40,27,26,34,81,80),(11,8,41,27,26,35,83,82),(11,8,42,27,26,35,85,84),(11,8,43,27,26,36,87,86),(11,8,44,27,27,36,89,88),(11,8,45,28,27,37,91,90),(11,8,46,28,27,37,93,92),(11,8,47,28,28,38,95,94),(11,8,48,28,28,38,98,96),(11,8,49,29,28,39,100,98),(11,8,50,29,29,39,107,100),(11,8,51,29,29,40,109,102),(11,8,52,29,29,40,111,104),(11,8,53,29,30,41,113,106),(11,8,54,30,30,42,116,108),(11,8,55,30,30,42,118,111),(11,8,56,30,31,43,120,113),(11,8,57,30,31,43,123,115),(11,8,58,31,31,44,126,117),(11,8,59,31,32,44,129,120),(11,8,60,31,32,45,131,122),(11,8,61,31,32,46,133,124),(11,8,62,31,33,46,136,127),(11,8,63,32,33,47,139,129),(11,8,64,32,33,47,141,132),(11,8,65,32,34,48,144,134),(11,8,66,33,34,49,147,137),(11,8,67,33,35,49,150,139),(11,8,68,33,35,50,153,142),(11,8,69,33,35,51,156,145),(11,8,70,34,36,51,158,147),(11,8,71,34,36,52,161,150),(11,8,72,34,37,53,164,153),(11,8,73,34,37,54,168,156),(11,8,74,35,38,54,171,158),(11,8,75,35,38,55,174,161),(11,8,76,35,38,56,177,164),(11,8,77,36,39,57,180,167),(11,8,78,36,39,57,183,170),(11,8,79,36,40,58,186,173),(11,8,80,37,40,59,190,176),(9,1,1,23,20,22,20,20),(9,1,2,24,21,23,20,20),(9,1,3,26,22,24,20,21),(9,1,4,27,22,26,20,21),(9,1,5,28,23,27,20,21),(9,1,6,30,24,28,20,21),(9,1,7,31,25,29,21,22),(9,1,8,32,26,30,21,22),(9,1,9,34,26,32,21,22),(9,1,10,35,27,33,21,23),(9,1,11,36,28,34,21,23),(9,1,12,38,29,35,21,23),(9,1,13,39,30,37,21,24),(9,1,14,41,31,38,21,24),(9,1,15,42,32,39,21,24),(9,1,16,44,33,41,21,25),(9,1,17,45,34,42,22,25),(9,1,18,47,34,43,22,25),(9,1,19,48,35,45,22,26),(9,1,20,50,36,46,22,26),(9,1,21,51,37,48,22,26),(9,1,22,53,38,49,22,27),(9,1,23,54,39,51,22,27),(9,1,24,56,40,52,23,28),(9,1,25,58,41,53,23,28),(9,1,26,59,42,55,23,28),(9,1,27,61,43,56,23,29),(9,1,28,63,44,58,23,29),(9,1,29,64,45,59,23,30),(9,1,30,66,46,61,24,30),(9,1,31,68,47,62,24,30),(9,1,32,69,48,64,24,31),(9,1,33,71,50,66,24,31),(9,1,34,73,51,67,24,32),(9,1,35,74,52,69,24,32),(9,1,36,76,53,70,25,33),(9,1,37,78,54,72,25,33),(9,1,38,80,55,74,25,34),(9,1,39,82,56,75,25,34),(9,1,40,83,57,77,25,35),(9,1,41,85,58,79,26,35),(9,1,42,87,60,80,26,35),(9,1,43,89,61,82,26,36),(9,1,44,91,62,84,26,36),(9,1,45,93,63,85,26,37),(9,1,46,95,64,87,27,37),(9,1,47,97,66,89,27,38),(9,1,48,99,67,91,27,38),(9,1,49,101,68,93,27,39),(9,1,50,103,69,94,28,40),(9,1,51,105,71,96,28,40),(9,1,52,107,72,98,28,41),(9,1,53,109,73,100,28,41),(9,1,54,111,74,102,29,42),(9,1,55,113,76,103,29,42),(9,1,56,115,77,105,29,43),(9,1,57,117,78,107,29,43),(9,1,58,119,79,109,30,44),(9,1,59,121,81,111,30,44),(9,1,60,123,82,113,30,45),(9,1,61,125,83,115,30,46),(9,1,62,127,85,117,31,46),(9,1,63,129,86,119,31,47),(9,1,64,132,88,121,31,47),(9,1,65,134,89,123,32,48),(9,1,66,136,90,125,32,49),(9,1,67,138,92,127,32,49),(9,1,68,140,93,129,32,50),(9,1,69,143,95,131,33,50),(9,1,70,145,96,133,33,51),(9,1,71,148,97,140,33,53),(9,1,72,156,99,143,33,54),(9,1,73,162,101,148,33,55),(9,1,74,162,102,148,34,55),(9,1,75,165,104,150,34,56),(9,1,76,171,106,156,34,57),(9,1,77,171,108,157,35,58),(9,1,78,174,109,159,35,58),(9,1,79,181,111,165,35,59),(9,1,80,184,113,168,36,60),(9,3,1,22,20,22,20,21),(9,3,2,23,21,23,21,22),(9,3,3,24,21,24,21,22),(9,3,4,25,22,25,22,23),(9,3,5,26,22,26,23,24),(9,3,6,28,23,27,23,25),(9,3,7,29,24,28,24,25),(9,3,8,30,24,29,25,26),(9,3,9,31,25,30,25,27),(9,3,10,32,25,32,26,27),(9,3,11,33,26,33,27,28),(9,3,12,35,27,34,27,29),(9,3,13,36,27,35,28,30),(9,3,14,37,28,36,29,31),(9,3,15,38,29,37,30,31),(9,3,16,40,29,38,30,32),(9,3,17,41,30,40,31,33),(9,3,18,42,31,41,32,34),(9,3,19,43,31,42,33,35),(9,3,20,45,32,43,33,35),(9,3,21,46,33,45,34,36),(9,3,22,47,33,46,35,37),(9,3,23,49,34,47,36,38),(9,3,24,50,35,48,37,39),(9,3,25,51,36,50,37,40),(9,3,26,53,36,51,38,41),(9,3,27,54,37,52,39,42),(9,3,28,56,38,54,40,43),(9,3,29,57,39,55,41,43),(9,3,30,58,39,56,42,44),(9,3,31,60,40,58,43,45),(9,3,32,61,41,59,43,46),(9,3,33,63,42,60,44,47),(9,3,34,64,43,62,45,48),(9,3,35,66,44,63,46,49),(9,3,36,67,44,65,47,50),(9,3,37,69,45,66,48,51),(9,3,38,70,46,67,49,52),(9,3,39,72,47,69,50,53),(9,3,40,73,48,70,51,54),(9,3,41,75,49,72,52,55),(9,3,42,77,49,73,53,56),(9,3,43,78,50,75,54,57),(9,3,44,80,51,76,55,58),(9,3,45,81,52,78,56,59),(9,3,46,83,53,79,57,61),(9,3,47,85,54,81,58,62),(9,3,48,86,55,83,59,63),(9,3,49,88,56,84,60,64),(9,3,50,90,57,86,61,65),(9,3,51,91,58,87,62,66),(9,3,52,93,59,89,63,67),(9,3,53,95,60,91,64,68),(9,3,54,97,61,92,65,69),(9,3,55,98,61,94,66,71),(9,3,56,100,62,95,67,72),(9,3,57,102,63,97,68,73),(9,3,58,104,64,99,69,74),(9,3,59,105,65,101,70,75),(9,3,60,107,66,102,71,77),(9,3,61,109,67,104,73,78),(9,3,62,111,69,106,74,79),(9,3,63,113,70,107,75,80),(9,3,64,115,71,109,76,81),(9,3,65,116,72,111,77,83),(9,3,66,118,73,113,78,84),(9,3,67,120,74,115,79,85),(9,3,68,122,75,116,81,86),(9,3,69,124,76,118,82,88),(9,3,70,126,77,120,83,89),(9,3,71,148,78,122,84,92),(9,3,72,150,79,125,86,94),(9,3,73,152,80,127,87,96),(9,3,74,156,82,129,89,97),(9,3,75,158,83,131,90,99),(9,3,76,162,84,134,92,100),(9,3,77,164,86,136,93,103),(9,3,78,167,87,138,95,105),(9,3,79,170,88,153,96,106),(9,3,80,173,90,160,98,108),(9,4,1,21,23,21,20,20),(9,4,2,22,24,22,20,20),(9,4,3,23,26,22,20,21),(9,4,4,23,27,23,20,21),(9,4,5,24,29,24,21,21),(9,4,6,25,30,25,21,22),(9,4,7,26,32,25,21,22),(9,4,8,26,33,26,21,23),(9,4,9,27,35,27,21,23),(9,4,10,28,36,27,21,23),(9,4,11,29,38,28,22,24),(9,4,12,30,39,29,22,24),(9,4,13,31,41,30,22,25),(9,4,14,31,43,31,22,25),(9,4,15,32,44,31,22,25),(9,4,16,33,46,32,23,26),(9,4,17,34,48,33,23,26),(9,4,18,35,49,34,23,27),(9,4,19,36,51,35,23,27),(9,4,20,37,53,35,23,28),(9,4,21,38,54,36,24,28),(9,4,22,39,56,37,24,29),(9,4,23,40,58,38,24,29),(9,4,24,41,60,39,24,30),(9,4,25,42,61,40,25,30),(9,4,26,43,63,41,25,31),(9,4,27,44,65,42,25,31),(9,4,28,45,67,43,25,32),(9,4,29,46,69,43,25,32),(9,4,30,47,71,44,26,33),(9,4,31,48,72,45,26,33),(9,4,32,49,74,46,26,34),(9,4,33,50,76,47,27,34),(9,4,34,51,78,48,27,35),(9,4,35,52,80,49,27,35),(9,4,36,53,82,50,27,36),(9,4,37,54,84,51,28,36),(9,4,38,55,86,52,28,37),(9,4,39,56,88,53,28,38),(9,4,40,57,90,54,28,38),(9,4,41,58,92,55,29,39),(9,4,42,60,94,56,29,39),(9,4,43,61,96,57,29,40),(9,4,44,62,98,58,30,40),(9,4,45,63,100,59,30,41),(9,4,46,64,103,61,30,42),(9,4,47,65,105,62,31,42),(9,4,48,66,107,63,31,43),(9,4,49,68,109,64,31,44),(9,4,50,69,111,65,32,44),(9,4,51,70,113,66,32,45),(9,4,52,71,116,67,32,45),(9,4,53,73,118,68,33,46),(9,4,54,74,120,69,33,47),(9,4,55,75,122,71,33,47),(9,4,56,76,125,72,34,48),(9,4,57,78,127,73,34,49),(9,4,58,79,129,74,34,49),(9,4,59,80,131,75,35,50),(9,4,60,81,134,77,35,51),(9,4,61,83,136,78,35,51),(9,4,62,84,138,79,36,52),(9,4,63,85,141,80,36,53),(9,4,64,87,143,81,37,54),(9,4,65,88,146,83,37,54),(9,4,66,89,148,84,37,55),(9,4,67,91,151,85,38,56),(9,4,68,92,153,86,38,57),(9,4,69,94,156,88,39,57),(9,4,70,95,158,89,39,58),(9,4,71,97,161,90,39,60),(9,4,72,99,164,92,40,60),(9,4,73,100,167,94,40,61),(9,4,74,102,170,95,41,62),(9,4,75,104,173,97,41,63),(9,4,76,105,176,98,41,64),(9,4,77,107,179,100,42,65),(9,4,78,109,183,106,42,66),(9,4,79,111,186,107,43,67),(9,4,80,113,189,109,43,69),(9,5,1,20,20,20,22,23),(9,5,2,20,20,20,23,24),(9,5,3,20,20,21,25,26),(9,5,4,20,21,21,26,27),(9,5,5,21,21,21,27,28),(9,5,6,21,21,22,29,30),(9,5,7,21,21,22,30,31),(9,5,8,21,22,23,31,33),(9,5,9,21,22,23,33,34),(9,5,10,21,22,23,34,36),(9,5,11,22,22,24,36,37),(9,5,12,22,23,24,37,39),(9,5,13,22,23,25,38,40),(9,5,14,22,23,25,40,42),(9,5,15,22,23,25,41,43),(9,5,16,23,24,26,43,45),(9,5,17,23,24,26,44,46),(9,5,18,23,24,27,46,48),(9,5,19,23,24,27,47,49),(9,5,20,23,25,28,49,51),(9,5,21,24,25,28,51,53),(9,5,22,24,25,29,52,54),(9,5,23,24,26,29,54,56),(9,5,24,24,26,30,55,58),(9,5,25,25,26,30,57,59),(9,5,26,25,27,31,59,61),(9,5,27,25,27,31,60,63),(9,5,28,25,27,32,62,65),(9,5,29,25,28,32,64,66),(9,5,30,26,28,33,65,68),(9,5,31,26,28,33,67,70),(9,5,32,26,29,34,69,72),(9,5,33,27,29,34,70,73),(9,5,34,27,29,35,72,75),(9,5,35,27,30,35,74,77),(9,5,36,27,30,36,76,79),(9,5,37,28,30,36,78,81),(9,5,38,28,31,37,79,83),(9,5,39,28,31,38,81,85),(9,5,40,28,31,38,83,87),(9,5,41,29,32,39,85,88),(9,5,42,29,32,39,87,90),(9,5,43,29,33,40,89,92),(9,5,44,30,33,40,91,94),(9,5,45,30,33,41,92,96),(9,5,46,30,34,42,94,98),(9,5,47,31,34,42,96,100),(9,5,48,31,35,43,98,102),(9,5,49,31,35,44,100,104),(9,5,50,32,36,44,102,106),(9,5,51,32,36,45,104,109),(9,5,52,32,36,45,106,111),(9,5,53,33,37,46,108,113),(9,5,54,33,37,47,110,115),(9,5,55,33,38,47,112,117),(9,5,56,34,38,48,114,119),(9,5,57,34,39,49,117,121),(9,5,58,34,39,49,119,124),(9,5,59,35,40,50,121,126),(9,5,60,35,40,51,123,128),(9,5,61,35,41,51,125,130),(9,5,62,36,41,52,127,132),(9,5,63,36,41,53,129,135),(9,5,64,37,42,54,132,137),(9,5,65,37,42,54,134,139),(9,5,66,37,43,55,136,142),(9,5,67,38,43,56,138,144),(9,5,68,38,44,57,140,146),(9,5,69,39,44,57,143,149),(9,5,70,39,45,58,145,151),(9,5,71,39,46,59,148,158),(9,5,72,40,46,59,151,161),(9,5,73,40,47,60,154,164),(9,5,74,41,47,61,156,167),(9,5,75,41,48,62,159,170),(9,5,76,41,49,63,162,174),(9,5,77,42,49,64,165,177),(9,5,78,42,50,65,168,180),(9,5,79,43,50,66,171,183),(9,5,80,43,51,67,174,186),(9,6,1,23,20,22,20,20),(9,6,2,24,21,23,20,20),(9,6,3,26,22,24,20,21),(9,6,4,27,22,26,20,21),(9,6,5,28,23,27,20,21),(9,6,6,30,24,28,20,21),(9,6,7,31,25,29,21,22),(9,6,8,32,26,30,21,22),(9,6,9,34,26,32,21,22),(9,6,10,35,27,33,21,23),(9,6,11,36,28,34,21,23),(9,6,12,38,29,35,21,23),(9,6,13,39,30,37,21,24),(9,6,14,41,31,38,21,24),(9,6,15,42,32,39,21,24),(9,6,16,44,33,41,21,25),(9,6,17,45,34,42,22,25),(9,6,18,47,34,43,22,25),(9,6,19,48,35,45,22,26),(9,6,20,50,36,46,22,26),(9,6,21,51,37,48,22,26),(9,6,22,53,38,49,22,27),(9,6,23,54,39,51,22,27),(9,6,24,56,40,52,23,28),(9,6,25,58,41,53,23,28),(9,6,26,59,42,55,23,28),(9,6,27,61,43,56,23,29),(9,6,28,63,44,58,23,29),(9,6,29,64,45,59,23,30),(9,6,30,66,46,61,24,30),(9,6,31,68,47,62,24,30),(9,6,32,69,48,64,24,31),(9,6,33,71,50,66,24,31),(9,6,34,73,51,67,24,32),(9,6,35,74,52,69,24,32),(9,6,36,76,53,70,25,33),(9,6,37,78,54,72,25,33),(9,6,38,80,55,74,25,34),(9,6,39,82,56,75,25,34),(9,6,40,83,57,77,25,35),(9,6,41,85,58,79,26,35),(9,6,42,87,60,80,26,35),(9,6,43,89,61,82,26,36),(9,6,44,91,62,84,26,36),(9,6,45,93,63,85,26,37),(9,6,46,95,64,87,27,37),(9,6,47,97,66,89,27,38),(9,6,48,99,67,91,27,38),(9,6,49,101,68,93,27,39),(9,6,50,103,69,94,28,40),(9,6,51,105,71,96,28,40),(9,6,52,106,72,97,28,41),(9,6,53,107,72,98,28,41),(9,6,54,107,73,98,29,42),(9,6,55,108,73,99,29,43),(9,6,56,111,75,102,29,44),(9,6,57,113,76,104,29,44),(9,6,58,118,77,106,30,45),(9,6,59,118,79,108,30,45),(9,6,60,123,80,110,30,46),(9,6,61,125,81,112,30,47),(9,6,62,128,83,114,30,47),(9,6,63,130,84,117,31,48),(9,6,64,130,86,119,31,48),(9,6,65,140,87,128,31,49),(9,6,66,143,89,131,31,50),(9,6,67,146,90,133,32,50),(9,6,68,148,92,135,32,51),(9,6,69,151,93,138,32,52),(9,6,70,154,95,140,32,52),(9,6,71,162,97,144,33,53),(9,6,72,164,98,146,33,54),(9,6,73,165,100,148,33,55),(9,6,74,166,102,151,33,55),(9,6,75,169,103,154,34,56),(9,6,76,172,105,157,34,57),(9,6,77,175,107,157,34,58),(9,6,78,176,108,157,34,58),(9,6,79,177,110,157,35,59),(9,6,80,180,112,160,35,60),(9,7,1,24,17,23,18,25),(9,7,2,25,17,24,19,26),(9,7,3,26,18,25,20,27),(9,7,4,26,18,26,21,28),(9,7,5,27,19,27,22,29),(9,7,6,28,19,28,23,30),(9,7,7,29,20,29,24,31),(9,7,8,30,20,30,25,32),(9,7,9,31,21,31,26,33),(9,7,10,32,21,32,27,34),(9,7,11,33,22,33,28,36),(9,7,12,34,22,34,29,37),(9,7,13,34,23,35,30,38),(9,7,14,35,23,36,31,39),(9,7,15,36,24,37,32,40),(9,7,16,37,24,39,33,41),(9,7,17,38,25,40,34,43),(9,7,18,39,25,41,35,44),(9,7,19,40,26,42,36,45),(9,7,20,41,26,43,37,46),(9,7,21,42,27,44,38,47),(9,7,22,43,27,45,39,49),(9,7,23,44,28,47,40,50),(9,7,24,45,28,48,41,51),(9,7,25,47,29,49,43,52),(9,7,26,48,30,50,44,54),(9,7,27,49,30,52,45,55),(9,7,28,50,31,53,46,56),(9,7,29,51,31,54,47,58),(9,7,30,52,32,55,48,59),(9,7,31,53,33,57,50,60),(9,7,32,54,33,58,51,62),(9,7,33,55,34,59,52,63),(9,7,34,57,34,61,53,65),(9,7,35,58,35,62,55,66),(9,7,36,59,36,63,56,67),(9,7,37,60,36,65,57,69),(9,7,38,61,37,66,58,70),(9,7,39,62,38,67,60,72),(9,7,40,64,38,69,61,73),(9,7,41,65,39,70,62,75),(9,7,42,66,40,72,64,76),(9,7,43,67,40,73,65,78),(9,7,44,69,41,74,66,79),(9,7,45,70,42,76,68,81),(9,7,46,71,42,77,69,82),(9,7,47,72,43,79,70,84),(9,7,48,74,44,80,72,85),(9,7,49,75,45,82,73,87),(9,7,50,76,45,83,75,89),(9,7,51,78,46,85,76,90),(9,7,52,79,47,86,77,92),(9,7,53,80,47,88,79,93),(9,7,54,82,48,90,80,95),(9,7,55,83,49,91,82,97),(9,7,56,85,50,93,83,98),(9,7,57,86,50,94,85,100),(9,7,58,87,51,96,86,102),(9,7,59,89,52,97,88,103),(9,7,60,90,53,99,89,105),(9,7,61,92,54,101,91,107),(9,7,62,93,54,102,92,109),(9,7,63,95,55,104,94,110),(9,7,64,96,56,106,95,112),(9,7,65,97,57,107,97,114),(9,7,66,99,58,109,99,116),(9,7,67,100,58,111,100,118),(9,7,68,102,59,113,102,119),(9,7,69,103,60,114,103,121),(9,7,70,105,61,116,105,123),(9,7,71,106,62,118,117,125),(9,7,72,108,63,120,119,128),(9,7,73,110,64,122,122,130),(9,7,74,112,65,125,124,132),(9,7,75,114,66,127,126,134),(9,7,76,116,67,129,128,137),(9,7,77,117,68,131,128,139),(9,7,78,119,69,133,133,141),(9,7,79,121,70,136,135,144),(9,7,80,123,71,138,137,146),(9,8,1,20,20,20,23,22),(9,8,2,20,20,20,24,23),(9,8,3,20,20,21,26,25),(9,8,4,20,20,21,27,26),(9,8,5,20,21,21,28,27),(9,8,6,20,21,21,30,29),(9,8,7,21,21,22,31,30),(9,8,8,21,21,22,33,31),(9,8,9,21,21,22,34,33),(9,8,10,21,21,23,36,34),(9,8,11,21,22,23,37,36),(9,8,12,21,22,23,39,37),(9,8,13,21,22,24,40,38),(9,8,14,21,22,24,42,40),(9,8,15,21,22,24,43,41),(9,8,16,21,23,25,45,43),(9,8,17,22,23,25,46,44),(9,8,18,22,23,25,48,46),(9,8,19,22,23,26,49,47),(9,8,20,22,23,26,51,49),(9,8,21,22,24,26,53,51),(9,8,22,22,24,27,54,52),(9,8,23,22,24,27,56,54),(9,8,24,23,24,28,58,55),(9,8,25,23,25,28,59,57),(9,8,26,23,25,28,61,59),(9,8,27,23,25,29,63,60),(9,8,28,23,25,29,65,62),(9,8,29,23,25,30,66,64),(9,8,30,24,26,30,68,65),(9,8,31,24,26,30,70,67),(9,8,32,24,26,31,72,69),(9,8,33,24,27,31,73,70),(9,8,34,24,27,32,75,72),(9,8,35,24,27,32,77,74),(9,8,36,25,27,33,79,76),(9,8,37,25,28,33,81,78),(9,8,38,25,28,34,83,79),(9,8,39,25,28,34,85,81),(9,8,40,25,28,35,87,83),(9,8,41,26,29,35,88,85),(9,8,42,26,29,35,90,87),(9,8,43,26,29,36,92,89),(9,8,44,26,30,36,94,91),(9,8,45,26,30,37,96,92),(9,8,46,27,30,37,98,94),(9,8,47,27,31,38,100,96),(9,8,48,27,31,38,102,98),(9,8,49,27,31,39,104,100),(9,8,50,28,32,40,106,102),(9,8,51,28,32,40,109,104),(9,8,52,28,32,41,111,106),(9,8,53,28,33,41,113,108),(9,8,54,29,33,42,115,110),(9,8,55,29,33,42,117,112),(9,8,56,29,34,43,119,114),(9,8,57,29,34,43,121,117),(9,8,58,30,34,44,124,119),(9,8,59,30,35,44,126,121),(9,8,60,30,35,45,128,123),(9,8,61,30,35,46,130,125),(9,8,62,31,36,46,132,127),(9,8,63,31,36,47,135,129),(9,8,64,31,37,47,137,132),(9,8,65,32,37,48,139,134),(9,8,66,32,37,49,142,136),(9,8,67,32,38,49,144,138),(9,8,68,32,38,50,146,140),(9,8,69,33,39,50,149,143),(9,8,70,33,39,51,151,145),(9,8,71,33,39,52,154,152),(9,8,72,33,40,53,160,155),(9,8,73,33,40,54,160,158),(9,8,74,34,41,54,163,160),(9,8,75,34,41,55,166,163),(9,8,76,34,41,56,169,166),(9,8,77,35,42,57,172,169),(9,8,78,35,42,57,175,173),(9,8,79,35,43,58,178,176),(9,8,80,36,43,59,181,179),(9,9,1,20,20,21,22,22),(9,9,2,20,20,22,23,23),(9,9,3,21,21,22,24,24),(9,9,4,21,21,23,26,25),(9,9,5,21,21,23,27,27),(9,9,6,21,22,24,28,28),(9,9,7,22,22,24,29,29),(9,9,8,22,23,25,30,30),(9,9,9,22,23,26,32,31),(9,9,10,23,23,26,33,33),(9,9,11,23,24,27,34,34),(9,9,12,23,24,27,35,35),(9,9,13,24,25,28,37,36),(9,9,14,24,25,29,38,38),(9,9,15,24,25,29,39,39),(9,9,16,25,26,30,41,40),(9,9,17,25,26,31,42,42),(9,9,18,25,27,31,43,43),(9,9,19,26,27,32,45,44),(9,9,20,26,28,33,46,46),(9,9,21,26,28,33,48,47),(9,9,22,27,29,34,49,49),(9,9,23,27,29,35,51,50),(9,9,24,28,30,35,52,51),(9,9,25,28,30,36,53,53),(9,9,26,28,31,37,55,54),(9,9,27,29,31,37,56,56),(9,9,28,29,32,38,58,57),(9,9,29,30,32,39,59,59),(9,9,30,30,33,40,61,60),(9,9,31,30,33,40,62,62),(9,9,32,31,34,41,64,63),(9,9,33,31,34,42,66,65),(9,9,34,32,35,43,67,66),(9,9,35,32,35,44,69,68),(9,9,36,33,36,44,70,69),(9,9,37,33,36,45,72,71),(9,9,38,34,37,46,74,73),(9,9,39,34,38,47,75,74),(9,9,40,35,38,48,77,76),(9,9,41,35,39,48,79,78),(9,9,42,35,39,49,80,79),(9,9,43,36,40,50,82,81),(9,9,44,36,40,51,84,83),(9,9,45,37,41,52,85,84),(9,9,46,37,42,53,87,86),(9,9,47,38,42,54,89,88),(9,9,48,38,43,55,91,89),(9,9,49,39,44,55,93,91),(9,9,50,40,44,56,94,93),(9,9,51,40,45,57,96,95),(9,9,52,41,45,58,98,97),(9,9,53,41,46,59,100,98),(9,9,54,42,47,60,102,100),(9,9,55,42,47,61,103,102),(9,9,56,43,48,62,105,104),(9,9,57,43,49,63,107,106),(9,9,58,44,49,64,109,108),(9,9,59,44,50,65,111,109),(9,9,60,45,51,66,113,111),(9,9,61,46,51,67,115,113),(9,9,62,46,52,68,117,115),(9,9,63,47,53,69,119,117),(9,9,64,47,54,70,121,119),(9,9,65,48,54,71,123,121),(9,9,66,49,55,72,125,123),(9,9,67,49,56,73,127,125),(9,9,68,50,57,74,129,127),(9,9,69,50,57,75,131,129),(9,9,70,51,58,76,133,131),(9,9,71,52,59,78,135,146),(9,9,72,53,59,79,138,148),(9,9,73,54,60,80,140,151),(9,9,74,54,61,89,143,154),(9,9,75,55,62,91,145,156),(9,9,76,56,63,92,148,159),(9,9,77,57,64,93,151,162),(9,9,78,57,65,95,153,165),(9,9,79,58,66,96,156,168),(9,9,80,59,67,97,159,170),(22,1,1,23,20,22,20,20),(22,1,2,24,21,23,20,20),(22,1,3,26,22,24,20,21),(22,1,4,27,22,26,20,21),(22,1,5,28,23,27,20,21),(22,1,6,30,24,28,20,21),(22,1,7,31,25,29,21,22),(22,1,8,32,26,30,21,22),(22,1,9,34,26,32,21,22),(22,1,10,35,27,33,21,23),(22,1,11,36,28,34,21,23),(22,1,12,38,29,35,21,23),(22,1,13,39,30,37,21,24),(22,1,14,41,31,38,21,24),(22,1,15,42,32,39,21,24),(22,1,16,44,33,41,21,25),(22,1,17,45,34,42,22,25),(22,1,18,47,34,43,22,25),(22,1,19,48,35,45,22,26),(22,1,20,50,36,46,22,26),(22,1,21,51,37,48,22,26),(22,1,22,53,38,49,22,27),(22,1,23,54,39,51,22,27),(22,1,24,56,40,52,23,28),(22,1,25,58,41,53,23,28),(22,1,26,59,42,55,23,28),(22,1,27,61,43,56,23,29),(22,1,28,63,44,58,23,29),(22,1,29,64,45,59,23,30),(22,1,30,66,46,61,24,30),(22,1,31,68,47,62,24,30),(22,1,32,69,48,64,24,31),(22,1,33,71,50,66,24,31),(22,1,34,73,51,67,24,32),(22,1,35,74,52,69,24,32),(22,1,36,76,53,70,25,33),(22,1,37,78,54,72,25,33),(22,1,38,80,55,74,25,34),(22,1,39,82,56,75,25,34),(22,1,40,83,57,77,25,35),(22,1,41,85,58,79,26,35),(22,1,42,87,60,80,26,35),(22,1,43,89,61,82,26,36),(22,1,44,91,62,84,26,36),(22,1,45,93,63,85,26,37),(22,1,46,95,64,87,27,37),(22,1,47,97,66,89,27,38),(22,1,48,99,67,91,27,38),(22,1,49,101,68,93,27,39),(22,1,50,103,69,94,28,40),(22,1,51,105,71,96,28,40),(22,1,52,107,72,98,28,41),(22,1,53,109,73,100,28,41),(22,1,54,111,74,102,29,42),(22,1,55,113,76,103,29,42),(22,1,56,115,77,105,29,43),(22,1,57,117,78,107,29,43),(22,1,58,119,79,109,30,44),(22,1,59,121,81,111,30,44),(22,1,60,123,82,113,30,45),(22,1,61,125,83,115,30,46),(22,1,62,127,85,117,31,46),(22,1,63,129,86,119,31,47),(22,1,64,132,88,121,31,47),(22,1,65,134,89,123,32,48),(22,1,66,136,90,125,32,49),(22,1,67,138,92,127,32,49),(22,1,68,140,93,129,32,50),(22,1,69,143,95,131,33,50),(22,1,70,145,96,133,33,51),(22,1,71,148,97,140,33,53),(22,1,72,156,99,143,33,54),(22,1,73,162,101,148,33,55),(22,1,74,162,102,148,34,55),(22,1,75,165,104,150,34,56),(22,1,76,171,106,156,34,57),(22,1,77,171,108,157,35,58),(22,1,78,174,109,159,35,58),(22,1,79,181,111,165,35,59),(22,1,80,184,113,168,36,60),(22,3,1,22,20,22,20,21),(22,3,2,23,21,23,21,22),(22,3,3,24,21,24,21,22),(22,3,4,25,22,25,22,23),(22,3,5,26,22,26,23,24),(22,3,6,28,23,27,23,25),(22,3,7,29,24,28,24,25),(22,3,8,30,24,29,25,26),(22,3,9,31,25,30,25,27),(22,3,10,32,25,32,26,27),(22,3,11,33,26,33,27,28),(22,3,12,35,27,34,27,29),(22,3,13,36,27,35,28,30),(22,3,14,37,28,36,29,31),(22,3,15,38,29,37,30,31),(22,3,16,40,29,38,30,32),(22,3,17,41,30,40,31,33),(22,3,18,42,31,41,32,34),(22,3,19,43,31,42,33,35),(22,3,20,45,32,43,33,35),(22,3,21,46,33,45,34,36),(22,3,22,47,33,46,35,37),(22,3,23,49,34,47,36,38),(22,3,24,50,35,48,37,39),(22,3,25,51,36,50,37,40),(22,3,26,53,36,51,38,41),(22,3,27,54,37,52,39,42),(22,3,28,56,38,54,40,43),(22,3,29,57,39,55,41,43),(22,3,30,58,39,56,42,44),(22,3,31,60,40,58,43,45),(22,3,32,61,41,59,43,46),(22,3,33,63,42,60,44,47),(22,3,34,64,43,62,45,48),(22,3,35,66,44,63,46,49),(22,3,36,67,44,65,47,50),(22,3,37,69,45,66,48,51),(22,3,38,70,46,67,49,52),(22,3,39,72,47,69,50,53),(22,3,40,73,48,70,51,54),(22,3,41,75,49,72,52,55),(22,3,42,77,49,73,53,56),(22,3,43,78,50,75,54,57),(22,3,44,80,51,76,55,58),(22,3,45,81,52,78,56,59),(22,3,46,83,53,79,57,61),(22,3,47,85,54,81,58,62),(22,3,48,86,55,83,59,63),(22,3,49,88,56,84,60,64),(22,3,50,90,57,86,61,65),(22,3,51,91,58,87,62,66),(22,3,52,93,59,89,63,67),(22,3,53,95,60,91,64,68),(22,3,54,97,61,92,65,69),(22,3,55,98,61,94,66,71),(22,3,56,100,62,95,67,72),(22,3,57,102,63,97,68,73),(22,3,58,104,64,99,69,74),(22,3,59,105,65,101,70,75),(22,3,60,107,66,102,71,77),(22,3,61,109,67,104,73,78),(22,3,62,111,69,106,74,79),(22,3,63,113,70,107,75,80),(22,3,64,115,71,109,76,81),(22,3,65,116,72,111,77,83),(22,3,66,118,73,113,78,84),(22,3,67,120,74,115,79,85),(22,3,68,122,75,116,81,86),(22,3,69,124,76,118,82,88),(22,3,70,126,77,120,83,89),(22,3,71,148,78,122,84,92),(22,3,72,150,79,125,86,94),(22,3,73,152,80,127,87,96),(22,3,74,156,82,129,89,97),(22,3,75,158,83,131,90,99),(22,3,76,162,84,134,92,100),(22,3,77,164,86,136,93,103),(22,3,78,167,87,138,95,105),(22,3,79,170,88,153,96,106),(22,3,80,173,90,160,98,108),(22,4,1,21,23,21,20,20),(22,4,2,22,24,22,20,20),(22,4,3,23,26,22,20,21),(22,4,4,23,27,23,20,21),(22,4,5,24,29,24,21,21),(22,4,6,25,30,25,21,22),(22,4,7,26,32,25,21,22),(22,4,8,26,33,26,21,23),(22,4,9,27,35,27,21,23),(22,4,10,28,36,27,21,23),(22,4,11,29,38,28,22,24),(22,4,12,30,39,29,22,24),(22,4,13,31,41,30,22,25),(22,4,14,31,43,31,22,25),(22,4,15,32,44,31,22,25),(22,4,16,33,46,32,23,26),(22,4,17,34,48,33,23,26),(22,4,18,35,49,34,23,27),(22,4,19,36,51,35,23,27),(22,4,20,37,53,35,23,28),(22,4,21,38,54,36,24,28),(22,4,22,39,56,37,24,29),(22,4,23,40,58,38,24,29),(22,4,24,41,60,39,24,30),(22,4,25,42,61,40,25,30),(22,4,26,43,63,41,25,31),(22,4,27,44,65,42,25,31),(22,4,28,45,67,43,25,32),(22,4,29,46,69,43,25,32),(22,4,30,47,71,44,26,33),(22,4,31,48,72,45,26,33),(22,4,32,49,74,46,26,34),(22,4,33,50,76,47,27,34),(22,4,34,51,78,48,27,35),(22,4,35,52,80,49,27,35),(22,4,36,53,82,50,27,36),(22,4,37,54,84,51,28,36),(22,4,38,55,86,52,28,37),(22,4,39,56,88,53,28,38),(22,4,40,57,90,54,28,38),(22,4,41,58,92,55,29,39),(22,4,42,60,94,56,29,39),(22,4,43,61,96,57,29,40),(22,4,44,62,98,58,30,40),(22,4,45,63,100,59,30,41),(22,4,46,64,103,61,30,42),(22,4,47,65,105,62,31,42),(22,4,48,66,107,63,31,43),(22,4,49,68,109,64,31,44),(22,4,50,69,111,65,32,44),(22,4,51,70,113,66,32,45),(22,4,52,71,116,67,32,45),(22,4,53,73,118,68,33,46),(22,4,54,74,120,69,33,47),(22,4,55,75,122,71,33,47),(22,4,56,76,125,72,34,48),(22,4,57,78,127,73,34,49),(22,4,58,79,129,74,34,49),(22,4,59,80,131,75,35,50),(22,4,60,81,134,77,35,51),(22,4,61,83,136,78,35,51),(22,4,62,84,138,79,36,52),(22,4,63,85,141,80,36,53),(22,4,64,87,143,81,37,54),(22,4,65,88,146,83,37,54),(22,4,66,89,148,84,37,55),(22,4,67,91,151,85,38,56),(22,4,68,92,153,86,38,57),(22,4,69,94,156,88,39,57),(22,4,70,95,158,89,39,58),(22,4,71,97,161,90,39,60),(22,4,72,99,164,92,40,60),(22,4,73,100,167,94,40,61),(22,4,74,102,170,95,41,62),(22,4,75,104,173,97,41,63),(22,4,76,105,176,98,41,64),(22,4,77,107,179,100,42,65),(22,4,78,109,183,106,42,66),(22,4,79,111,186,107,43,67),(22,4,80,113,189,109,43,69),(22,5,1,20,20,20,22,23),(22,5,2,20,20,20,23,24),(22,5,3,20,20,21,25,26),(22,5,4,20,21,21,26,27),(22,5,5,21,21,21,27,28),(22,5,6,21,21,22,29,30),(22,5,7,21,21,22,30,31),(22,5,8,21,22,23,31,33),(22,5,9,21,22,23,33,34),(22,5,10,21,22,23,34,36),(22,5,11,22,22,24,36,37),(22,5,12,22,23,24,37,39),(22,5,13,22,23,25,38,40),(22,5,14,22,23,25,40,42),(22,5,15,22,23,25,41,43),(22,5,16,23,24,26,43,45),(22,5,17,23,24,26,44,46),(22,5,18,23,24,27,46,48),(22,5,19,23,24,27,47,49),(22,5,20,23,25,28,49,51),(22,5,21,24,25,28,51,53),(22,5,22,24,25,29,52,54),(22,5,23,24,26,29,54,56),(22,5,24,24,26,30,55,58),(22,5,25,25,26,30,57,59),(22,5,26,25,27,31,59,61),(22,5,27,25,27,31,60,63),(22,5,28,25,27,32,62,65),(22,5,29,25,28,32,64,66),(22,5,30,26,28,33,65,68),(22,5,31,26,28,33,67,70),(22,5,32,26,29,34,69,72),(22,5,33,27,29,34,70,73),(22,5,34,27,29,35,72,75),(22,5,35,27,30,35,74,77),(22,5,36,27,30,36,76,79),(22,5,37,28,30,36,78,81),(22,5,38,28,31,37,79,83),(22,5,39,28,31,38,81,85),(22,5,40,28,31,38,83,87),(22,5,41,29,32,39,85,88),(22,5,42,29,32,39,87,90),(22,5,43,29,33,40,89,92),(22,5,44,30,33,40,91,94),(22,5,45,30,33,41,92,96),(22,5,46,30,34,42,94,98),(22,5,47,31,34,42,96,100),(22,5,48,31,35,43,98,102),(22,5,49,31,35,44,100,104),(22,5,50,32,36,44,102,106),(22,5,51,32,36,45,104,109),(22,5,52,32,36,45,106,111),(22,5,53,33,37,46,108,113),(22,5,54,33,37,47,110,115),(22,5,55,33,38,47,112,117),(22,5,56,34,38,48,114,119),(22,5,57,34,39,49,117,121),(22,5,58,34,39,49,119,124),(22,5,59,35,40,50,121,126),(22,5,60,35,40,51,123,128),(22,5,61,35,41,51,125,130),(22,5,62,36,41,52,127,132),(22,5,63,36,41,53,129,135),(22,5,64,37,42,54,132,137),(22,5,65,37,42,54,134,139),(22,5,66,37,43,55,136,142),(22,5,67,38,43,56,138,144),(22,5,68,38,44,57,140,146),(22,5,69,39,44,57,143,149),(22,5,70,39,45,58,145,151),(22,5,71,39,46,59,148,158),(22,5,72,40,46,59,151,161),(22,5,73,40,47,60,154,164),(22,5,74,41,47,61,156,167),(22,5,75,41,48,62,159,170),(22,5,76,41,49,63,162,174),(22,5,77,42,49,64,165,177),(22,5,78,42,50,65,168,180),(22,5,79,43,50,66,171,183),(22,5,80,43,51,67,174,186),(22,6,1,23,20,22,20,20),(22,6,2,24,21,23,20,20),(22,6,3,26,22,24,20,21),(22,6,4,27,22,26,20,21),(22,6,5,28,23,27,20,21),(22,6,6,30,24,28,20,21),(22,6,7,31,25,29,21,22),(22,6,8,32,26,30,21,22),(22,6,9,34,26,32,21,22),(22,6,10,35,27,33,21,23),(22,6,11,36,28,34,21,23),(22,6,12,38,29,35,21,23),(22,6,13,39,30,37,21,24),(22,6,14,41,31,38,21,24),(22,6,15,42,32,39,21,24),(22,6,16,44,33,41,21,25),(22,6,17,45,34,42,22,25),(22,6,18,47,34,43,22,25),(22,6,19,48,35,45,22,26),(22,6,20,50,36,46,22,26),(22,6,21,51,37,48,22,26),(22,6,22,53,38,49,22,27),(22,6,23,54,39,51,22,27),(22,6,24,56,40,52,23,28),(22,6,25,58,41,53,23,28),(22,6,26,59,42,55,23,28),(22,6,27,61,43,56,23,29),(22,6,28,63,44,58,23,29),(22,6,29,64,45,59,23,30),(22,6,30,66,46,61,24,30),(22,6,31,68,47,62,24,30),(22,6,32,69,48,64,24,31),(22,6,33,71,50,66,24,31),(22,6,34,73,51,67,24,32),(22,6,35,74,52,69,24,32),(22,6,36,76,53,70,25,33),(22,6,37,78,54,72,25,33),(22,6,38,80,55,74,25,34),(22,6,39,82,56,75,25,34),(22,6,40,83,57,77,25,35),(22,6,41,85,58,79,26,35),(22,6,42,87,60,80,26,35),(22,6,43,89,61,82,26,36),(22,6,44,91,62,84,26,36),(22,6,45,93,63,85,26,37),(22,6,46,95,64,87,27,37),(22,6,47,97,66,89,27,38),(22,6,48,99,67,91,27,38),(22,6,49,101,68,93,27,39),(22,6,50,103,69,94,28,40),(22,6,51,105,71,96,28,40),(22,6,52,106,72,97,28,41),(22,6,53,107,72,98,28,41),(22,6,54,107,73,98,29,42),(22,6,55,108,73,99,29,43),(22,6,56,111,75,102,29,44),(22,6,57,113,76,104,29,44),(22,6,58,118,77,106,30,45),(22,6,59,118,79,108,30,45),(22,6,60,123,80,110,30,46),(22,6,61,125,81,112,30,47),(22,6,62,128,83,114,30,47),(22,6,63,130,84,117,31,48),(22,6,64,130,86,119,31,48),(22,6,65,140,87,128,31,49),(22,6,66,143,89,131,31,50),(22,6,67,146,90,133,32,50),(22,6,68,148,92,135,32,51),(22,6,69,151,93,138,32,52),(22,6,70,154,95,140,32,52),(22,6,71,162,97,144,33,53),(22,6,72,164,98,146,33,54),(22,6,73,165,100,148,33,55),(22,6,74,166,102,151,33,55),(22,6,75,169,103,154,34,56),(22,6,76,172,105,157,34,57),(22,6,77,175,107,157,34,58),(22,6,78,176,108,157,34,58),(22,6,79,177,110,157,35,59),(22,6,80,180,112,160,35,60),(22,8,1,20,20,20,23,22),(22,8,2,20,20,20,24,23),(22,8,3,20,20,21,26,25),(22,8,4,20,20,21,27,26),(22,8,5,20,21,21,28,27),(22,8,6,20,21,21,30,29),(22,8,7,21,21,22,31,30),(22,8,8,21,21,22,33,31),(22,8,9,21,21,22,34,33),(22,8,10,21,21,23,36,34),(22,8,11,21,22,23,37,36),(22,8,12,21,22,23,39,37),(22,8,13,21,22,24,40,38),(22,8,14,21,22,24,42,40),(22,8,15,21,22,24,43,41),(22,8,16,21,23,25,45,43),(22,8,17,22,23,25,46,44),(22,8,18,22,23,25,48,46),(22,8,19,22,23,26,49,47),(22,8,20,22,23,26,51,49),(22,8,21,22,24,26,53,51),(22,8,22,22,24,27,54,52),(22,8,23,22,24,27,56,54),(22,8,24,23,24,28,58,55),(22,8,25,23,25,28,59,57),(22,8,26,23,25,28,61,59),(22,8,27,23,25,29,63,60),(22,8,28,23,25,29,65,62),(22,8,29,23,25,30,66,64),(22,8,30,24,26,30,68,65),(22,8,31,24,26,30,70,67),(22,8,32,24,26,31,72,69),(22,8,33,24,27,31,73,70),(22,8,34,24,27,32,75,72),(22,8,35,24,27,32,77,74),(22,8,36,25,27,33,79,76),(22,8,37,25,28,33,81,78),(22,8,38,25,28,34,83,79),(22,8,39,25,28,34,85,81),(22,8,40,25,28,35,87,83),(22,8,41,26,29,35,88,85),(22,8,42,26,29,35,90,87),(22,8,43,26,29,36,92,89),(22,8,44,26,30,36,94,91),(22,8,45,26,30,37,96,92),(22,8,46,27,30,37,98,94),(22,8,47,27,31,38,100,96),(22,8,48,27,31,38,102,98),(22,8,49,27,31,39,104,100),(22,8,50,28,32,40,106,102),(22,8,51,28,32,40,109,104),(22,8,52,28,32,41,111,106),(22,8,53,28,33,41,113,108),(22,8,54,29,33,42,115,110),(22,8,55,29,33,42,117,112),(22,8,56,29,34,43,119,114),(22,8,57,29,34,43,121,117),(22,8,58,30,34,44,124,119),(22,8,59,30,35,44,126,121),(22,8,60,30,35,45,128,123),(22,8,61,30,35,46,130,125),(22,8,62,31,36,46,132,127),(22,8,63,31,36,47,135,129),(22,8,64,31,37,47,137,132),(22,8,65,32,37,48,139,134),(22,8,66,32,37,49,142,136),(22,8,67,32,38,49,144,138),(22,8,68,32,38,50,146,140),(22,8,69,33,39,50,149,143),(22,8,70,33,39,51,151,145),(22,8,71,33,39,52,154,152),(22,8,72,33,40,53,160,155),(22,8,73,33,40,54,160,158),(22,8,74,34,41,54,163,160),(22,8,75,34,41,55,166,163),(22,8,76,34,41,56,169,166),(22,8,77,35,42,57,172,169),(22,8,78,35,42,57,175,173),(22,8,79,35,43,58,178,176),(22,8,80,36,43,59,181,179),(22,9,1,20,20,21,22,22),(22,9,2,20,20,22,23,23),(22,9,3,21,21,22,24,24),(22,9,4,21,21,23,26,25),(22,9,5,21,21,23,27,27),(22,9,6,21,22,24,28,28),(22,9,7,22,22,24,29,29),(22,9,8,22,23,25,30,30),(22,9,9,22,23,26,32,31),(22,9,10,23,23,26,33,33),(22,9,11,23,24,27,34,34),(22,9,12,23,24,27,35,35),(22,9,13,24,25,28,37,36),(22,9,14,24,25,29,38,38),(22,9,15,24,25,29,39,39),(22,9,16,25,26,30,41,40),(22,9,17,25,26,31,42,42),(22,9,18,25,27,31,43,43),(22,9,19,26,27,32,45,44),(22,9,20,26,28,33,46,46),(22,9,21,26,28,33,48,47),(22,9,22,27,29,34,49,49),(22,9,23,27,29,35,51,50),(22,9,24,28,30,35,52,51),(22,9,25,28,30,36,53,53),(22,9,26,28,31,37,55,54),(22,9,27,29,31,37,56,56),(22,9,28,29,32,38,58,57),(22,9,29,30,32,39,59,59),(22,9,30,30,33,40,61,60),(22,9,31,30,33,40,62,62),(22,9,32,31,34,41,64,63),(22,9,33,31,34,42,66,65),(22,9,34,32,35,43,67,66),(22,9,35,32,35,44,69,68),(22,9,36,33,36,44,70,69),(22,9,37,33,36,45,72,71),(22,9,38,34,37,46,74,73),(22,9,39,34,38,47,75,74),(22,9,40,35,38,48,77,76),(22,9,41,35,39,48,79,78),(22,9,42,35,39,49,80,79),(22,9,43,36,40,50,82,81),(22,9,44,36,40,51,84,83),(22,9,45,37,41,52,85,84),(22,9,46,37,42,53,87,86),(22,9,47,38,42,54,89,88),(22,9,48,38,43,55,91,89),(22,9,49,39,44,55,93,91),(22,9,50,40,44,56,94,93),(22,9,51,40,45,57,96,95),(22,9,52,41,45,58,98,97),(22,9,53,41,46,59,100,98),(22,9,54,42,47,60,102,100),(22,9,55,42,47,61,103,102),(22,9,56,43,48,62,105,104),(22,9,57,43,49,63,107,106),(22,9,58,44,49,64,109,108),(22,9,59,44,50,65,111,109),(22,9,60,45,51,66,113,111),(22,9,61,46,51,67,115,113),(22,9,62,46,52,68,117,115),(22,9,63,47,53,69,119,117),(22,9,64,47,54,70,121,119),(22,9,65,48,54,71,123,121),(22,9,66,49,55,72,125,123),(22,9,67,49,56,73,127,125),(22,9,68,50,57,74,129,127),(22,9,69,50,57,75,131,129),(22,9,70,51,58,76,133,131),(22,9,71,52,59,78,135,146),(22,9,72,53,59,79,138,148),(22,9,73,54,60,80,140,151),(22,9,74,54,61,89,143,154),(22,9,75,55,62,91,145,156),(22,9,76,56,63,92,148,159),(22,9,77,57,64,93,151,162),(22,9,78,57,65,95,153,165),(22,9,79,58,66,96,156,168),(22,9,80,59,67,97,159,170),(22,11,1,26,15,22,17,24),(22,11,2,27,16,23,18,25),(22,11,3,27,16,23,19,26),(22,11,4,28,17,24,20,27),(22,11,5,28,17,25,21,29),(22,11,6,29,18,25,22,30),(22,11,7,29,18,26,23,31),(22,11,8,30,19,27,24,32),(22,11,9,30,19,27,26,34),(22,11,10,31,20,28,27,35),(22,11,11,32,20,29,28,36),(22,11,12,32,21,29,29,37),(22,11,13,33,21,30,30,39),(22,11,14,33,22,31,31,40),(22,11,15,34,23,32,32,41),(22,11,16,35,23,32,34,43),(22,11,17,35,24,33,35,44),(22,11,18,36,24,34,36,45),(22,11,19,37,25,35,37,47),(22,11,20,37,26,35,39,48),(22,11,21,38,26,36,40,50),(22,11,22,39,27,37,41,51),(22,11,23,39,28,38,42,52),(22,11,24,40,28,39,44,54),(22,11,25,41,29,39,45,55),(22,11,26,41,30,40,46,57),(22,11,27,42,30,41,47,58),(22,11,28,43,31,42,49,60),(22,11,29,44,32,43,50,61),(22,11,30,44,32,44,52,63),(22,11,31,45,33,44,53,64),(22,11,32,46,34,45,54,66),(22,11,33,47,34,46,56,67),(22,11,34,47,35,47,57,69),(22,11,35,48,36,48,58,71),(22,11,36,49,36,49,60,72),(22,11,37,50,37,50,61,74),(22,11,38,51,38,51,63,76),(22,11,39,52,39,52,64,77),(22,11,40,52,39,53,66,79),(22,11,41,53,40,54,67,81),(22,11,42,54,41,55,69,82),(22,11,43,55,42,56,70,84),(22,11,44,56,43,57,72,86),(22,11,45,57,43,57,73,87),(22,11,46,57,44,58,75,89),(22,11,47,58,45,60,76,91),(22,11,48,59,46,61,78,93),(22,11,49,60,47,62,79,94),(22,11,50,61,47,63,81,96),(22,11,51,62,48,64,83,98),(22,11,52,63,49,65,84,100),(22,11,53,64,50,66,86,102),(22,11,54,65,51,67,87,104),(22,11,55,66,51,68,89,105),(22,11,56,67,52,69,91,107),(22,11,57,68,53,70,92,109),(22,11,58,69,54,71,94,111),(22,11,59,70,55,72,96,113),(22,11,60,71,56,73,97,115),(22,11,61,72,57,74,99,117),(22,11,62,73,58,76,101,119),(22,11,63,74,59,77,103,121),(22,11,64,75,59,78,104,123),(22,11,65,76,60,79,106,125),(22,11,66,77,61,80,108,127),(22,11,67,78,62,81,110,129),(22,11,68,79,63,83,111,131),(22,11,69,80,64,84,113,133),(22,11,70,81,65,85,115,135),(22,11,71,83,66,86,117,137),(22,11,72,84,68,88,120,140),(22,11,73,85,69,89,122,142),(22,11,74,86,70,91,124,145),(22,11,75,88,71,92,126,147),(22,11,76,89,72,94,128,150),(22,11,77,90,73,95,131,153),(22,11,78,92,75,97,133,155),(22,11,79,93,76,98,136,158),(22,11,80,94,77,100,138,185),(10,1,1,23,20,22,20,20),(10,1,2,24,21,23,20,20),(10,1,3,26,22,24,20,21),(10,1,4,27,22,26,20,21),(10,1,5,28,23,27,20,21),(10,1,6,30,24,28,20,21),(10,1,7,31,25,29,21,22),(10,1,8,32,26,30,21,22),(10,1,9,34,26,32,21,22),(10,1,10,30,28,31,24,21),(10,1,11,32,29,33,24,21),(10,1,12,33,30,34,24,21),(10,1,13,34,31,35,24,22),(10,1,14,36,32,36,25,22),(10,1,15,37,32,37,25,22),(10,1,16,38,33,38,25,23),(10,1,17,39,34,40,25,23),(10,1,18,41,35,41,25,23),(10,1,19,42,36,42,25,24),(10,1,20,44,37,43,25,24),(10,1,21,45,37,45,26,24),(10,1,22,46,38,46,26,25),(10,1,23,48,39,47,26,25),(10,1,24,49,40,49,26,26),(10,1,25,51,41,50,26,26),(10,1,26,52,42,51,26,26),(10,1,27,54,43,53,26,27),(10,1,28,55,44,54,27,27),(10,1,29,57,45,56,27,28),(10,1,30,59,46,57,27,28),(10,1,31,60,47,58,27,28),(10,1,32,62,48,60,27,29),(10,1,33,63,49,61,27,29),(10,1,34,65,50,63,28,30),(10,1,35,67,51,64,28,30),(10,1,36,69,52,66,28,31),(10,1,37,70,53,68,28,31),(10,1,38,72,54,69,28,31),(10,1,39,74,55,71,29,32),(10,1,40,76,56,72,29,32),(10,1,41,77,58,74,29,33),(10,1,42,79,59,76,29,33),(10,1,43,81,60,77,29,34),(10,1,44,83,61,79,29,34),(10,1,45,85,62,81,30,35),(10,1,46,87,63,83,30,35),(10,1,47,89,65,84,30,36),(10,1,48,91,66,86,30,36),(10,1,49,93,67,88,31,37),(10,1,50,95,68,90,31,37),(10,1,51,97,70,92,31,38),(10,1,52,99,71,94,31,38),(10,1,53,101,72,96,31,39),(10,1,54,103,74,98,32,40),(10,1,55,106,75,100,32,40),(10,1,56,108,76,102,32,41),(10,1,57,110,78,104,32,41),(10,1,58,112,79,106,33,42),(10,1,59,115,81,108,33,42),(10,1,60,117,82,110,33,43),(10,1,61,119,83,112,33,44),(10,1,62,122,85,114,33,44),(10,1,63,124,86,117,34,45),(10,1,64,127,88,119,34,45),(10,1,65,129,90,121,34,46),(10,1,66,132,91,123,35,47),(10,1,67,134,93,126,35,47),(10,1,68,137,94,128,35,48),(10,1,69,139,96,130,35,49),(10,1,70,142,98,133,36,49),(10,1,71,145,99,135,36,50),(10,1,72,147,101,138,36,51),(10,1,73,150,103,140,36,52),(10,1,74,153,104,143,37,52),(10,1,75,156,106,145,37,53),(10,1,76,159,108,148,37,54),(10,1,77,162,110,151,38,55),(10,1,78,165,111,153,38,55),(10,1,79,168,113,156,38,56),(10,1,80,171,115,159,39,57),(8,9,85,64,73,96,177,182),(8,9,84,63,72,95,174,179),(8,9,83,62,71,94,171,176),(8,9,82,61,70,92,168,173),(8,9,81,61,70,91,165,170),(8,11,1,26,15,22,17,24),(8,11,2,27,16,23,18,25),(8,11,3,27,16,23,19,26),(8,11,4,28,17,24,20,27),(8,11,5,28,17,25,21,29),(8,11,6,29,18,25,22,30),(8,11,7,29,18,26,23,31),(8,11,8,30,19,27,24,32),(8,11,9,30,19,27,26,34),(8,11,10,27,26,25,26,32),(8,11,11,27,27,26,27,34),(8,11,12,28,27,27,28,35),(8,11,13,28,28,27,29,36),(8,11,14,29,28,28,30,37),(8,11,15,30,29,29,32,38),(8,11,16,30,29,29,33,39),(8,11,17,31,30,30,34,41),(8,11,18,31,31,31,35,42),(8,11,19,32,31,31,36,43),(8,11,20,33,32,32,37,44),(8,11,21,33,32,33,38,46),(8,11,22,34,33,34,39,47),(8,11,23,35,34,34,40,48),(8,11,24,35,34,35,42,50),(8,11,25,36,35,36,43,51),(8,11,26,37,35,37,44,52),(8,11,27,37,36,37,45,54),(8,11,28,38,37,38,46,55),(8,11,29,39,37,39,48,57),(8,11,30,39,38,40,49,58),(8,11,31,40,39,41,50,59),(8,11,32,41,39,42,52,61),(8,11,33,42,40,42,53,62),(8,11,34,42,41,43,54,64),(8,11,35,43,41,44,56,65),(8,11,36,44,42,45,57,67),(8,11,37,45,43,46,58,69),(8,11,38,46,43,47,60,70),(8,11,39,46,44,48,61,72),(8,11,40,47,45,49,63,73),(8,11,41,48,46,50,64,75),(8,11,42,49,46,51,66,77),(8,11,43,50,47,52,67,78),(8,11,44,51,48,52,69,80),(8,11,45,51,49,53,70,82),(8,11,46,52,50,54,72,84),(8,11,47,53,50,56,73,85),(8,11,48,54,51,57,75,87),(8,11,49,55,52,58,77,89),(8,11,50,56,53,59,78,91),(8,11,51,57,54,60,80,93),(8,11,52,58,55,61,82,95),(8,11,53,59,56,62,83,97),(8,11,54,60,56,63,85,99),(8,11,55,61,57,64,87,101),(8,11,56,62,58,65,89,103),(8,11,57,63,59,66,90,105),(8,11,58,64,60,68,92,107),(8,11,59,65,61,69,94,109),(8,11,60,66,62,70,96,111),(8,11,61,67,63,71,98,113),(8,11,62,68,64,72,100,115),(8,11,63,69,65,74,102,118),(8,11,64,70,66,75,104,120),(8,11,65,72,67,76,106,122),(8,11,66,73,68,78,108,124),(8,11,67,74,69,79,110,127),(8,11,68,75,70,80,112,129),(8,11,69,76,71,82,114,131),(8,11,70,77,72,83,116,134),(8,11,71,79,73,84,118,136),(8,11,72,80,75,86,121,139),(8,11,73,81,76,87,123,141),(8,11,74,82,77,89,125,144),(8,11,75,84,78,90,127,146),(8,11,76,85,79,92,130,149),(8,11,77,86,80,93,132,152),(8,11,78,88,82,95,134,154),(8,11,79,89,83,96,137,157),(8,11,80,90,84,98,139,160),(3,9,85,68,67,97,180,180),(3,9,84,67,66,96,177,177),(3,9,83,66,65,95,174,174),(3,9,82,65,64,93,171,171),(3,9,81,65,64,92,168,168),(6,2,1,19,22,21,24,20),(6,2,2,20,23,22,25,21),(6,2,3,21,23,23,25,21),(6,2,4,22,24,24,26,22),(6,2,5,23,24,25,27,23),(6,2,6,25,25,26,27,24),(6,2,7,26,25,27,28,24),(6,2,8,27,26,28,29,25),(6,2,9,28,27,29,29,26),(6,2,10,36,21,31,21,29),(6,2,11,37,21,32,22,30),(6,2,12,38,22,33,23,30),(6,2,13,39,23,34,23,31),(6,2,14,40,23,35,24,32),(6,2,15,41,24,37,25,32),(6,2,16,43,24,38,25,33),(6,2,17,44,25,39,26,34),(6,2,18,45,26,40,27,35),(6,2,19,46,26,41,27,35),(6,2,20,47,27,42,28,36),(6,2,21,48,28,43,29,37),(6,2,22,50,28,44,30,38),(6,2,23,51,29,45,30,39),(6,2,24,52,30,47,31,39),(6,2,25,53,30,48,32,40),(6,2,26,55,31,49,33,41),(6,2,27,56,32,50,33,42),(6,2,28,57,32,51,34,43),(6,2,29,59,33,53,35,44),(6,2,30,60,34,54,36,44),(6,2,31,61,35,55,37,45),(6,2,32,63,35,57,38,46),(6,2,33,64,36,58,38,47),(6,2,34,66,37,59,39,48),(6,2,35,67,38,61,40,49),(6,2,36,69,39,62,41,50),(6,2,37,70,39,63,42,51),(6,2,38,72,40,65,43,52),(6,2,39,73,41,66,44,53),(6,2,40,75,42,68,45,54),(6,2,41,76,43,69,46,55),(6,2,42,78,43,71,47,56),(6,2,43,79,44,72,48,57),(6,2,44,81,45,74,48,58),(6,2,45,83,46,75,49,59),(6,2,46,84,47,77,50,60),(6,2,47,86,48,78,52,61),(6,2,48,88,49,80,53,62),(6,2,49,89,50,82,54,64),(6,2,50,91,51,83,55,65),(6,2,51,93,52,85,56,66),(6,2,52,95,53,87,57,67),(6,2,53,97,54,88,58,68),(6,2,54,98,55,90,59,69),(6,2,55,100,56,92,60,71),(6,2,56,102,57,94,61,72),(6,2,57,104,58,95,62,73),(6,2,58,106,59,97,64,74),(6,2,59,113,60,99,65,76),(6,2,60,113,61,101,66,77),(6,2,61,113,62,103,67,78),(6,2,62,119,63,105,68,80),(6,2,63,121,64,107,70,81),(6,2,64,121,65,109,71,82),(6,2,65,121,67,111,72,84),(6,2,66,123,68,113,74,85),(6,2,67,125,69,115,75,87),(6,2,68,127,70,117,76,88),(6,2,69,135,71,119,78,89),(6,2,70,135,73,121,79,91),(6,2,71,135,74,123,80,92),(6,2,72,142,75,126,82,94),(6,2,73,144,76,128,83,96),(6,2,74,144,78,130,85,97),(6,2,75,149,79,132,86,99),(6,2,76,149,80,135,88,100),(6,2,77,155,82,137,89,102),(6,2,78,158,83,139,91,104),(6,2,79,160,84,142,92,105),(6,2,80,160,86,144,94,107),(1,3,85,80,208,139,97,105),(1,3,84,79,204,137,96,103),(1,3,83,78,200,135,94,102),(1,3,82,77,197,133,93,100),(1,3,81,75,194,130,91,99),(6,5,1,17,22,19,26,22),(6,5,2,17,22,19,27,23),(6,5,3,17,22,20,29,25),(6,5,4,17,23,20,30,26),(6,5,5,18,23,20,31,27),(6,5,6,18,23,21,33,29),(6,5,7,18,23,21,34,30),(6,5,8,18,24,22,35,32),(6,5,9,18,24,22,37,33),(6,5,10,27,18,24,29,36),(6,5,11,27,18,25,30,37),(6,5,12,27,19,25,31,39),(6,5,13,27,19,25,32,40),(6,5,14,27,19,26,34,41),(6,5,15,28,19,26,35,43),(6,5,16,28,20,27,36,44),(6,5,17,28,20,27,38,45),(6,5,18,28,20,27,39,47),(6,5,19,28,21,28,40,48),(6,5,20,29,21,28,42,50),(6,5,21,29,21,29,43,51),(6,5,22,29,21,29,45,53),(6,5,23,29,22,30,46,54),(6,5,24,30,22,30,48,56),(6,5,25,30,22,31,49,57),(6,5,26,30,23,31,51,59),(6,5,27,30,23,31,52,61),(6,5,28,30,23,32,54,62),(6,5,29,31,24,32,55,64),(6,5,30,31,24,33,57,66),(6,5,31,31,24,33,59,67),(6,5,32,31,25,34,60,69),(6,5,33,32,25,34,62,71),(6,5,34,32,25,35,64,72),(6,5,35,32,26,35,65,74),(6,5,36,33,26,36,67,76),(6,5,37,33,26,37,69,78),(6,5,38,33,27,37,71,80),(6,5,39,33,27,38,72,82),(6,5,40,34,27,38,74,83),(6,5,41,34,28,39,76,85),(6,5,42,34,28,39,78,87),(6,5,43,34,29,40,80,89),(6,5,44,35,29,40,82,91),(6,5,45,35,29,41,84,93),(6,5,46,35,30,42,86,95),(6,5,47,36,30,42,88,97),(6,5,48,36,31,43,90,100),(6,5,49,36,31,44,92,102),(6,5,50,37,31,44,98,104),(6,5,51,37,32,45,101,106),(6,5,52,37,32,45,103,108),(6,5,53,38,33,46,105,110),(6,5,54,38,33,47,107,113),(6,5,55,38,34,47,110,115),(6,5,56,39,34,48,112,117),(6,5,57,39,35,49,114,120),(6,5,58,39,35,50,116,122),(6,5,59,40,36,50,119,125),(6,5,60,40,36,51,122,127),(6,5,61,40,36,52,124,129),(6,5,62,41,37,52,127,132),(6,5,63,41,37,53,129,135),(6,5,64,41,38,54,132,137),(6,5,65,42,39,55,134,140),(6,5,66,42,39,56,137,142),(6,5,67,43,40,56,139,145),(6,5,68,43,40,57,143,148),(6,5,69,43,41,58,146,151),(6,5,70,44,41,59,148,153),(6,5,71,44,42,60,151,156),(6,5,72,45,42,60,154,159),(6,5,73,45,43,61,157,162),(6,5,74,46,43,62,159,165),(6,5,75,46,44,63,162,168),(6,5,76,46,45,64,166,171),(6,5,77,47,45,65,169,174),(6,5,78,47,46,66,172,177),(6,5,79,48,46,67,175,180),(6,5,80,48,47,68,178,183),(3,7,85,136,76,149,144,155),(3,7,84,134,75,146,138,152),(3,7,83,132,74,144,138,150),(3,7,82,129,73,142,138,147),(3,7,81,127,71,139,135,145),(5,3,1,17,25,20,24,20),(5,3,2,17,26,21,25,21),(5,3,3,18,28,22,25,21),(5,3,4,18,29,23,26,22),(5,3,5,19,30,24,26,23),(5,3,6,19,32,25,27,23),(5,3,7,20,33,26,27,24),(5,3,8,20,35,27,28,25),(5,3,9,21,36,27,29,25),(5,3,10,23,32,28,23,31),(5,3,11,23,33,29,23,32),(5,3,12,24,35,30,24,33),(5,3,13,24,36,31,25,33),(5,3,14,25,37,32,25,34),(5,3,15,25,39,33,26,34),(5,3,16,26,40,34,26,35),(5,3,17,26,41,35,27,36),(5,3,18,27,43,36,28,37),(5,3,19,27,44,37,28,37),(5,3,20,28,46,38,29,38),(5,3,21,28,47,39,30,39),(5,3,22,29,49,40,30,39),(5,3,23,29,50,41,31,40),(5,3,24,30,52,42,32,41),(5,3,25,30,53,43,32,42),(5,3,26,31,55,44,33,42),(5,3,27,31,57,45,34,43),(5,3,28,32,58,46,34,44),(5,3,29,32,60,47,35,45),(5,3,30,33,62,48,36,45),(5,3,31,33,63,50,37,46),(5,3,32,34,65,51,37,47),(5,3,33,35,67,52,38,48),(5,3,34,35,68,53,39,49),(5,3,35,36,70,54,40,50),(5,3,36,37,72,56,41,51),(5,3,37,37,74,57,41,51),(5,3,38,38,76,58,42,52),(5,3,39,38,78,59,43,53),(5,3,40,39,79,61,44,54),(5,3,41,40,81,62,45,55),(5,3,42,40,83,63,45,56),(5,3,43,41,85,64,46,57),(5,3,44,42,87,66,47,58),(5,3,45,42,89,67,48,59),(5,3,46,43,91,69,49,60),(5,3,47,44,93,70,50,61),(5,3,48,45,96,71,51,62),(5,3,49,45,98,73,52,63),(5,3,50,46,100,74,53,64),(5,3,51,47,102,76,54,65),(5,3,52,48,104,77,55,66),(5,3,53,48,106,79,56,67),(5,3,54,49,109,80,57,68),(5,3,55,50,111,82,58,69),(5,3,56,51,113,83,59,70),(5,3,57,52,116,85,60,72),(5,3,58,52,118,87,61,73),(5,3,59,53,121,88,62,74),(5,3,60,54,123,90,63,75),(5,3,61,55,125,92,64,76),(5,3,62,56,128,93,65,77),(5,3,63,57,131,95,66,79),(5,3,64,57,133,97,67,80),(5,3,65,58,136,99,69,81),(5,3,66,59,138,100,70,82),(5,3,67,60,141,102,71,84),(5,3,68,61,144,104,72,85),(5,3,69,62,147,106,73,86),(5,3,70,63,149,108,75,88),(5,3,71,64,152,110,76,89),(5,3,72,65,155,112,77,90),(5,3,73,66,158,114,78,92),(5,3,74,67,161,116,80,93),(5,3,75,68,164,118,81,95),(5,3,76,69,175,120,82,96),(5,3,77,70,178,122,84,98),(5,3,78,71,181,124,85,99),(5,3,79,72,184,126,86,101),(5,3,80,73,188,128,88,102),(3,8,85,42,42,64,206,189),(3,8,84,42,41,63,203,186),(3,8,83,41,41,62,199,183),(3,8,82,41,40,61,196,179),(3,8,81,41,40,61,193,176),(2,8,1,17,22,19,27,21),(2,8,2,17,22,19,28,22),(2,8,3,17,22,20,30,24),(2,8,4,17,22,20,31,25),(2,8,5,17,23,20,32,26),(2,8,6,17,23,20,34,28),(2,8,7,18,23,21,35,29),(2,8,8,18,23,21,37,30),(2,8,9,18,23,21,38,32),(2,8,10,24,19,24,31,35),(2,8,11,24,19,24,32,36),(2,8,12,24,19,24,34,37),(2,8,13,24,19,25,35,38),(2,8,14,25,19,25,36,40),(2,8,15,25,20,25,38,41),(2,8,16,25,20,26,39,42),(2,8,17,25,20,26,40,44),(2,8,18,25,20,26,42,45),(2,8,19,25,20,27,43,46),(2,8,20,25,21,27,45,48),(2,8,21,26,21,27,46,49),(2,8,22,26,21,28,48,51),(2,8,23,26,21,28,49,52),(2,8,24,26,22,29,51,54),(2,8,25,26,22,29,52,55),(2,8,26,26,22,29,54,57),(2,8,27,26,22,30,56,58),(2,8,28,27,22,30,57,60),(2,8,29,27,23,31,59,61),(2,8,30,27,23,31,61,63),(2,8,31,27,23,31,62,65),(2,8,32,27,23,32,64,66),(2,8,33,27,24,32,66,68),(2,8,34,28,24,33,67,70),(2,8,35,28,24,33,69,71),(2,8,36,28,25,34,71,73),(2,8,37,28,25,34,73,75),(2,8,38,28,25,34,75,77),(2,8,39,29,25,35,77,78),(2,8,40,29,26,35,78,80),(2,8,41,29,26,36,80,82),(2,8,42,29,26,36,82,84),(2,8,43,29,26,37,84,86),(2,8,44,29,27,37,86,88),(2,8,45,30,27,38,88,90),(2,8,46,30,27,38,90,92),(2,8,47,30,28,39,92,94),(2,8,48,30,28,39,95,96),(2,8,49,31,28,40,97,98),(2,8,50,31,29,40,104,100),(2,8,51,31,29,41,106,102),(2,8,52,31,29,41,108,104),(2,8,53,31,30,42,110,106),(2,8,54,32,30,43,113,108),(2,8,55,32,30,43,115,111),(2,8,56,32,31,44,117,113),(2,8,57,32,31,44,120,115),(2,8,58,33,31,45,123,117),(2,8,59,33,32,45,126,120),(2,8,60,33,32,46,128,122),(2,8,61,33,32,47,130,124),(2,8,62,33,33,47,133,127),(2,8,63,34,33,48,136,129),(2,8,64,34,33,48,138,132),(2,8,65,34,34,49,141,134),(2,8,66,35,34,50,144,137),(2,8,67,35,35,50,147,139),(2,8,68,35,35,51,150,142),(2,8,69,35,35,52,153,145),(2,8,70,36,36,52,155,147),(2,8,71,36,36,53,158,150),(2,8,72,36,37,54,161,153),(2,8,73,36,37,55,165,156),(2,8,74,37,38,55,168,158),(2,8,75,37,38,56,171,161),(2,8,76,37,38,57,174,164),(2,8,77,38,39,58,177,167),(2,8,78,38,39,58,180,170),(2,8,79,38,40,59,183,173),(2,8,80,39,40,60,187,176),(7,5,85,41,56,71,202,198),(7,5,84,40,56,70,199,195),(7,5,83,40,55,69,196,191),(7,5,82,39,54,68,192,188),(7,5,81,39,54,68,188,185),(4,8,1,20,20,20,23,22),(4,8,2,20,20,20,24,23),(4,8,3,20,20,21,26,25),(4,8,4,20,20,21,27,26),(4,8,5,20,21,21,28,27),(4,8,6,20,21,21,30,29),(4,8,7,21,21,22,31,30),(4,8,8,21,21,22,33,31),(4,8,9,21,21,22,34,33),(4,8,10,17,26,23,34,33),(4,8,11,17,26,23,35,34),(4,8,12,17,26,23,37,35),(4,8,13,17,26,24,38,36),(4,8,14,18,26,24,39,38),(4,8,15,18,27,24,41,39),(4,8,16,18,27,25,42,40),(4,8,17,18,27,25,43,42),(4,8,18,18,27,25,45,43),(4,8,19,18,27,26,46,44),(4,8,20,18,28,26,48,46),(4,8,21,19,28,26,49,47),(4,8,22,19,28,27,51,49),(4,8,23,19,28,27,52,50),(4,8,24,19,29,28,54,52),(4,8,25,19,29,28,55,53),(4,8,26,19,29,28,57,55),(4,8,27,19,29,29,59,56),(4,8,28,20,29,29,60,58),(4,8,29,20,30,30,62,59),(4,8,30,20,30,30,64,61),(4,8,31,20,30,30,65,63),(4,8,32,20,30,31,67,64),(4,8,33,20,31,31,69,66),(4,8,34,21,31,32,70,68),(4,8,35,21,31,32,72,69),(4,8,36,21,32,33,74,71),(4,8,37,21,32,33,76,73),(4,8,38,21,32,33,78,75),(4,8,39,22,32,34,80,76),(4,8,40,22,33,34,81,78),(4,8,41,22,33,35,83,80),(4,8,42,22,33,35,85,82),(4,8,43,22,33,36,87,84),(4,8,44,22,34,36,89,86),(4,8,45,23,34,37,91,88),(4,8,46,23,34,37,93,90),(4,8,47,23,35,38,95,92),(4,8,48,23,35,38,98,94),(4,8,49,24,35,39,100,96),(4,8,50,24,36,39,107,98),(4,8,51,24,36,40,109,100),(4,8,52,24,36,40,111,102),(4,8,53,24,37,41,113,104),(4,8,54,25,37,42,116,106),(4,8,55,25,37,42,118,109),(4,8,56,25,38,43,120,111),(4,8,57,25,38,43,123,113),(4,8,58,26,38,44,126,115),(4,8,59,26,39,44,129,118),(4,8,60,26,39,45,131,120),(4,8,61,26,39,46,133,122),(4,8,62,26,40,46,136,125),(4,8,63,27,40,47,139,127),(4,8,64,27,40,47,141,130),(4,8,65,27,41,48,144,132),(4,8,66,28,41,49,147,135),(4,8,67,28,42,49,150,137),(4,8,68,28,42,50,153,140),(4,8,69,28,42,51,156,143),(4,8,70,29,43,51,158,145),(4,8,71,29,43,52,161,148),(4,8,72,29,44,53,164,151),(4,8,73,29,44,54,168,154),(4,8,74,30,45,54,171,156),(4,8,75,30,45,55,174,159),(4,8,76,30,45,56,177,162),(4,8,77,31,46,57,180,165),(4,8,78,31,46,57,183,168),(4,8,79,31,47,58,186,171),(4,8,80,32,47,59,190,174),(4,8,85,33,50,63,207,190),(4,8,84,33,49,62,204,187),(4,8,83,32,49,61,200,184),(4,8,82,32,48,60,197,180),(4,8,81,32,48,60,194,177),(7,5,1,20,20,20,22,23),(7,5,2,20,20,20,23,24),(7,5,3,20,20,21,25,26),(7,5,4,20,21,21,26,27),(7,5,5,21,21,21,27,28),(7,5,6,21,21,22,29,30),(7,5,7,21,21,22,30,31),(7,5,8,21,22,23,31,33),(7,5,9,21,22,23,33,34),(7,5,10,17,24,23,36,34),(7,5,11,17,24,24,37,35),(7,5,12,17,25,24,38,37),(7,5,13,17,25,24,39,38),(7,5,14,17,25,25,41,39),(7,5,15,18,25,25,42,41),(7,5,16,18,26,26,43,42),(7,5,17,18,26,26,45,43),(7,5,18,18,26,26,46,45),(7,5,19,18,27,27,47,46),(7,5,20,19,27,27,49,48),(7,5,21,19,27,28,50,49),(7,5,22,19,27,28,52,51),(7,5,23,19,28,29,53,52),(7,5,24,20,28,29,55,54),(7,5,25,20,28,30,56,55),(7,5,26,20,29,30,58,57),(7,5,27,20,29,30,59,59),(7,5,28,20,29,31,61,60),(7,5,29,21,30,31,62,62),(7,5,30,21,30,32,64,64),(7,5,31,21,30,32,66,65),(7,5,32,21,31,33,67,67),(7,5,33,22,31,33,69,69),(7,5,34,22,31,34,71,70),(7,5,35,22,32,34,72,72),(7,5,36,23,32,35,74,74),(7,5,37,23,32,36,76,76),(7,5,38,23,33,36,78,78),(7,5,39,23,33,37,79,80),(7,5,40,24,33,37,81,81),(7,5,41,24,34,38,83,83),(7,5,42,24,34,38,85,85),(7,5,43,24,35,39,87,87),(7,5,44,25,35,39,89,89),(7,5,45,25,35,40,91,91),(7,5,46,25,36,41,93,93),(7,5,47,26,36,41,95,95),(7,5,48,26,37,42,97,98),(7,5,49,26,37,43,99,100),(7,5,50,27,37,43,105,102),(7,5,51,27,38,44,108,104),(7,5,52,27,38,44,110,106),(7,5,53,28,39,45,112,108),(7,5,54,28,39,46,114,111),(7,5,55,28,40,46,117,113),(7,5,56,29,40,47,119,115),(7,5,57,29,41,48,121,118),(7,5,58,29,41,49,123,120),(7,5,59,30,42,49,126,123),(7,5,60,30,42,50,129,125),(7,5,61,30,42,51,131,127),(7,5,62,31,43,51,134,130),(7,5,63,31,43,52,136,133),(7,5,64,31,44,53,139,135),(7,5,65,32,45,54,141,138),(7,5,66,32,45,55,144,140),(7,5,67,33,46,55,146,143),(7,5,68,33,46,56,150,146),(7,5,69,33,47,57,153,149),(7,5,70,34,47,58,155,151),(7,5,71,34,48,59,158,154),(7,5,72,35,48,59,161,157),(7,5,73,35,49,60,164,160),(7,5,74,36,49,61,166,163),(7,5,75,36,50,62,169,166),(7,5,76,36,51,63,173,169),(7,5,77,37,51,64,176,172),(7,5,78,37,52,65,179,175),(7,5,79,38,52,66,182,178),(7,5,80,38,53,67,185,181),(2,8,85,40,43,64,204,192),(2,8,84,40,42,63,201,189),(2,8,83,39,42,62,197,186),(2,8,82,39,41,61,194,182),(2,8,81,39,41,61,191,179),(3,8,1,20,20,20,23,22),(3,8,2,20,20,20,24,23),(3,8,3,20,20,21,26,25),(3,8,4,20,20,21,27,26),(3,8,5,20,21,21,28,27),(3,8,6,20,21,21,30,29),(3,8,7,21,21,22,31,30),(3,8,8,21,21,22,33,31),(3,8,9,21,21,22,34,33),(3,8,10,26,18,24,33,32),(3,8,11,26,18,24,34,33),(3,8,12,26,18,24,36,34),(3,8,13,26,18,25,37,35),(3,8,14,27,18,25,38,37),(3,8,15,27,19,25,40,38),(3,8,16,27,19,26,41,39),(3,8,17,27,19,26,42,41),(3,8,18,27,19,26,44,42),(3,8,19,27,19,27,45,43),(3,8,20,27,20,27,47,45),(3,8,21,28,20,27,48,46),(3,8,22,28,20,28,50,48),(3,8,23,28,20,28,51,49),(3,8,24,28,21,29,53,51),(3,8,25,28,21,29,54,52),(3,8,26,28,21,29,56,54),(3,8,27,28,21,30,58,55),(3,8,28,29,21,30,59,57),(3,8,29,29,22,31,61,58),(3,8,30,29,22,31,63,60),(3,8,31,29,22,31,64,62),(3,8,32,29,22,32,66,63),(3,8,33,29,23,32,68,65),(3,8,34,30,23,33,69,67),(3,8,35,30,23,33,71,68),(3,8,36,30,24,34,73,70),(3,8,37,30,24,34,75,72),(3,8,38,30,24,34,77,74),(3,8,39,31,24,35,79,75),(3,8,40,31,25,35,80,77),(3,8,41,31,25,36,82,79),(3,8,42,31,25,36,84,81),(3,8,43,31,25,37,86,83),(3,8,44,31,26,37,88,85),(3,8,45,32,26,38,90,87),(3,8,46,32,26,38,92,89),(3,8,47,32,27,39,94,91),(3,8,48,32,27,39,97,93),(3,8,49,33,27,40,99,95),(3,8,50,33,28,40,106,97),(3,8,51,33,28,41,108,99),(3,8,52,33,28,41,110,101),(3,8,53,33,29,42,112,103),(3,8,54,34,29,43,115,105),(3,8,55,34,29,43,117,108),(3,8,56,34,30,44,119,110),(3,8,57,34,30,44,122,112),(3,8,58,35,30,45,125,114),(3,8,59,35,31,45,128,117),(3,8,60,35,31,46,130,119),(3,8,61,35,31,47,132,121),(3,8,62,35,32,47,135,124),(3,8,63,36,32,48,138,126),(3,8,64,36,32,48,140,129),(3,8,65,36,33,49,143,131),(3,8,66,37,33,50,146,134),(3,8,67,37,34,50,149,136),(3,8,68,37,34,51,152,139),(3,8,69,37,34,52,155,142),(3,8,70,38,35,52,157,144),(3,8,71,38,35,53,160,147),(3,8,72,38,36,54,163,150),(3,8,73,38,36,55,167,153),(3,8,74,39,37,55,170,155),(3,8,75,39,37,56,173,158),(3,8,76,39,37,57,176,161),(3,8,77,40,38,58,179,164),(3,8,78,40,38,58,182,167),(3,8,79,40,39,59,185,170),(3,8,80,41,39,60,189,173),(5,3,85,79,206,139,95,110),(5,3,84,78,202,137,94,108),(5,3,83,77,198,135,92,107),(5,3,82,76,195,133,91,105),(5,3,81,74,192,130,89,104),(3,7,1,20,20,20,22,23),(3,7,2,20,20,20,23,24),(3,7,3,20,20,21,25,26),(3,7,4,20,21,21,26,27),(3,7,5,21,21,21,27,28),(3,7,6,21,21,22,29,30),(3,7,7,21,21,22,30,31),(3,7,8,21,22,23,31,33),(3,7,9,21,22,23,33,34),(3,7,10,33,20,30,27,29),(3,7,11,34,20,31,28,30),(3,7,12,35,21,32,29,31),(3,7,13,35,21,33,30,32),(3,7,14,36,22,34,31,33),(3,7,15,37,22,35,32,35),(3,7,16,38,23,36,33,36),(3,7,17,39,23,37,34,37),(3,7,18,40,24,38,35,38),(3,7,19,41,24,39,36,39),(3,7,20,42,25,40,37,40),(3,7,21,43,25,41,38,41),(3,7,22,43,26,42,39,42),(3,7,23,44,26,43,40,43),(3,7,24,45,27,44,41,45),(3,7,25,46,27,46,42,46),(3,7,26,47,28,47,43,47),(3,7,27,48,28,48,44,48),(3,7,28,49,29,49,45,49),(3,7,29,50,29,50,46,51),(3,7,30,51,30,51,47,52),(3,7,31,53,30,53,49,53),(3,7,32,54,31,54,50,55),(3,7,33,55,32,55,51,56),(3,7,34,56,32,56,52,57),(3,7,35,57,33,58,53,59),(3,7,36,58,34,59,55,60),(3,7,37,59,34,60,56,61),(3,7,38,60,35,62,57,63),(3,7,39,61,35,63,58,64),(3,7,40,63,36,64,60,66),(3,7,41,64,37,66,61,67),(3,7,42,65,37,67,62,69),(3,7,43,66,38,69,63,70),(3,7,44,68,39,70,65,72),(3,7,45,69,39,72,66,73),(3,7,46,70,40,73,68,75),(3,7,47,71,41,75,69,76),(3,7,48,73,42,76,70,78),(3,7,49,74,42,78,72,80),(3,7,50,75,43,79,73,81),(3,7,51,77,44,81,75,83),(3,7,52,78,45,82,76,85),(3,7,53,80,45,84,78,86),(3,7,54,81,46,86,79,88),(3,7,55,82,47,87,81,90),(3,7,56,84,48,89,82,92),(3,7,57,85,49,91,84,93),(3,7,58,87,49,92,86,95),(3,7,59,88,50,94,87,97),(3,7,60,90,51,96,89,99),(3,7,61,92,52,98,91,101),(3,7,62,93,53,100,92,103),(3,7,63,95,54,101,94,105),(3,7,64,96,54,103,96,107),(3,7,65,98,55,105,98,109),(3,7,66,100,56,107,99,111),(3,7,67,101,57,109,101,113),(3,7,68,103,58,111,103,115),(3,7,69,105,59,113,105,117),(3,7,70,107,60,115,107,119),(3,7,71,108,61,117,109,121),(3,7,72,110,62,119,111,124),(3,7,73,112,63,121,113,126),(3,7,74,114,64,124,115,128),(3,7,75,116,65,126,117,130),(3,7,76,118,66,128,119,133),(3,7,77,119,67,130,121,135),(3,7,78,121,68,132,123,137),(3,7,79,123,69,135,125,140),(3,7,80,125,70,137,127,142),(6,5,85,51,50,72,195,200),(6,5,84,50,50,71,192,197),(6,5,83,50,49,70,189,193),(6,5,82,49,48,69,185,190),(6,5,81,49,48,69,181,187),(1,3,1,17,28,20,20,21),(1,3,2,17,29,21,21,22),(1,3,3,18,31,22,21,22),(1,3,4,18,32,23,22,23),(1,3,5,19,33,24,22,24),(1,3,6,19,35,25,23,24),(1,3,7,20,36,26,24,25),(1,3,8,20,38,27,24,25),(1,3,9,21,39,27,25,26),(1,3,10,24,34,28,25,26),(1,3,11,24,35,29,25,27),(1,3,12,25,37,30,26,28),(1,3,13,25,38,31,27,28),(1,3,14,26,39,32,27,29),(1,3,15,26,41,33,28,29),(1,3,16,27,42,34,28,30),(1,3,17,27,43,35,29,31),(1,3,18,28,45,36,30,32),(1,3,19,28,46,37,30,32),(1,3,20,29,48,38,31,33),(1,3,21,29,49,39,32,34),(1,3,22,30,51,40,32,34),(1,3,23,30,52,41,33,35),(1,3,24,31,54,42,34,36),(1,3,25,31,55,43,34,37),(1,3,26,32,57,44,35,37),(1,3,27,32,59,45,36,38),(1,3,28,33,60,46,36,39),(1,3,29,33,62,47,37,40),(1,3,30,34,64,48,38,40),(1,3,31,34,65,50,39,41),(1,3,32,35,67,51,39,42),(1,3,33,36,69,52,40,43),(1,3,34,36,70,53,41,44),(1,3,35,37,72,54,42,45),(1,3,36,38,74,56,43,46),(1,3,37,38,76,57,43,46),(1,3,38,39,78,58,44,47),(1,3,39,39,80,59,45,48),(1,3,40,40,81,61,46,49),(1,3,41,41,83,62,47,50),(1,3,42,41,85,63,47,51),(1,3,43,42,87,64,48,52),(1,3,44,43,89,66,49,53),(1,3,45,43,91,67,50,54),(1,3,46,44,93,69,51,55),(1,3,47,45,95,70,52,56),(1,3,48,46,98,71,53,57),(1,3,49,46,100,73,54,58),(1,3,50,47,102,74,55,59),(1,3,51,48,104,76,56,60),(1,3,52,49,106,77,57,61),(1,3,53,49,108,79,58,62),(1,3,54,50,111,80,59,63),(1,3,55,51,113,82,60,64),(1,3,56,52,115,83,61,65),(1,3,57,53,118,85,62,67),(1,3,58,53,120,87,63,68),(1,3,59,54,123,88,64,69),(1,3,60,55,125,90,65,70),(1,3,61,56,127,92,66,71),(1,3,62,57,130,93,67,72),(1,3,63,58,133,95,68,74),(1,3,64,58,135,97,69,75),(1,3,65,59,138,99,71,76),(1,3,66,60,140,100,72,77),(1,3,67,61,143,102,73,79),(1,3,68,62,146,104,74,80),(1,3,69,63,149,106,75,81),(1,3,70,64,151,108,77,83),(1,3,71,65,154,110,78,84),(1,3,72,66,157,112,79,85),(1,3,73,67,160,114,80,87),(1,3,74,68,163,116,82,88),(1,3,75,69,166,118,83,90),(1,3,76,70,177,120,84,91),(1,3,77,71,180,122,86,93),(1,3,78,72,183,124,87,94),(1,3,79,73,186,126,88,96),(1,3,80,74,190,128,90,97),(6,2,85,177,93,157,102,116),(6,2,84,175,92,154,100,114),(6,2,83,171,90,152,98,112),(6,2,82,168,89,149,97,110),(6,2,81,165,87,147,95,109),(3,9,1,19,18,22,20,27),(3,9,2,19,18,23,21,28),(3,9,3,20,19,23,22,29),(3,9,4,20,19,24,24,30),(3,9,5,20,19,24,25,32),(3,9,6,20,20,25,26,33),(3,9,7,21,20,25,27,34),(3,9,8,21,21,26,28,35),(3,9,9,21,21,26,30,36),(3,9,10,28,19,27,30,31),(3,9,11,28,20,27,32,32),(3,9,12,28,20,28,33,33),(3,9,13,29,20,28,34,35),(3,9,14,29,21,29,35,36),(3,9,15,29,21,30,36,37),(3,9,16,30,22,30,37,38),(3,9,17,30,22,31,39,40),(3,9,18,30,22,31,40,41),(3,9,19,31,23,32,41,42),(3,9,20,31,23,33,42,44),(3,9,21,31,24,33,44,45),(3,9,22,32,24,34,45,46),(3,9,23,32,25,35,46,48),(3,9,24,33,25,35,48,49),(3,9,25,33,26,36,49,51),(3,9,26,33,26,37,50,52),(3,9,27,34,26,37,52,53),(3,9,28,34,27,38,53,55),(3,9,29,35,27,39,55,56),(3,9,30,35,28,39,56,58),(3,9,31,35,28,40,57,60),(3,9,32,36,29,41,59,61),(3,9,33,36,29,42,60,63),(3,9,34,37,30,42,62,64),(3,9,35,37,30,43,63,66),(3,9,36,38,31,44,65,68),(3,9,37,38,32,45,67,69),(3,9,38,38,32,46,68,71),(3,9,39,39,33,46,70,73),(3,9,40,39,33,47,71,74),(3,9,41,40,34,48,73,76),(3,9,42,40,34,49,75,78),(3,9,43,41,35,50,76,80),(3,9,44,41,35,51,78,81),(3,9,45,42,36,51,80,83),(3,9,46,42,37,52,82,85),(3,9,47,43,37,53,83,87),(3,9,48,43,38,54,85,89),(3,9,49,44,39,55,87,91),(3,9,50,44,39,56,93,93),(3,9,51,45,40,57,95,95),(3,9,52,45,40,58,97,97),(3,9,53,46,41,59,99,99),(3,9,54,47,42,60,101,101),(3,9,55,47,42,61,104,103),(3,9,56,48,43,62,106,105),(3,9,57,48,44,63,108,107),(3,9,58,49,45,64,110,110),(3,9,59,49,45,65,112,112),(3,9,60,50,46,66,114,114),(3,9,61,51,47,67,116,116),(3,9,62,51,47,68,118,119),(3,9,63,52,48,69,121,121),(3,9,64,52,49,70,123,123),(3,9,65,53,50,72,126,126),(3,9,66,54,51,73,128,128),(3,9,67,54,51,74,131,131),(3,9,68,55,52,75,133,133),(3,9,69,56,53,76,135,135),(3,9,70,56,54,77,138,138),(3,9,71,57,55,79,140,141),(3,9,72,58,55,80,143,143),(3,9,73,59,56,81,146,146),(3,9,74,59,57,82,149,149),(3,9,75,60,58,84,151,151),(3,9,76,61,59,85,154,154),(3,9,77,62,60,86,157,157),(3,9,78,62,61,88,159,160),(3,9,79,63,62,89,162,163),(3,9,80,64,63,90,165,165),(8,9,1,19,18,22,20,27),(8,9,2,19,18,23,21,28),(8,9,3,20,19,23,22,29),(8,9,4,20,19,24,24,30),(8,9,5,20,19,24,25,32),(8,9,6,20,20,25,26,33),(8,9,7,21,20,25,27,34),(8,9,8,21,21,26,28,35),(8,9,9,21,21,26,30,36),(8,9,10,24,25,26,27,33),(8,9,11,24,26,26,29,34),(8,9,12,24,26,27,30,35),(8,9,13,25,26,27,31,37),(8,9,14,25,27,28,32,38),(8,9,15,25,27,29,33,39),(8,9,16,26,28,29,34,40),(8,9,17,26,28,30,36,42),(8,9,18,26,28,30,37,43),(8,9,19,27,29,31,38,44),(8,9,20,27,29,32,39,46),(8,9,21,27,30,32,41,47),(8,9,22,28,30,33,42,48),(8,9,23,28,31,34,43,50),(8,9,24,29,31,34,45,51),(8,9,25,29,32,35,46,53),(8,9,26,29,32,36,47,54),(8,9,27,30,32,36,49,55),(8,9,28,30,33,37,50,57),(8,9,29,31,33,38,52,58),(8,9,30,31,34,38,53,60),(8,9,31,31,34,39,54,62),(8,9,32,32,35,40,56,63),(8,9,33,32,35,41,57,65),(8,9,34,33,36,41,59,66),(8,9,35,33,36,42,60,68),(8,9,36,34,37,43,62,70),(8,9,37,34,38,44,64,71),(8,9,38,34,38,45,65,73),(8,9,39,35,39,45,67,75),(8,9,40,35,39,46,68,76),(8,9,41,36,40,47,70,78),(8,9,42,36,40,48,72,80),(8,9,43,37,41,49,73,82),(8,9,44,37,41,50,75,83),(8,9,45,38,42,50,77,85),(8,9,46,38,43,51,79,87),(8,9,47,39,43,52,80,89),(8,9,48,39,44,53,82,91),(8,9,49,40,45,54,84,93),(8,9,50,40,45,55,90,95),(8,9,51,41,46,56,92,97),(8,9,52,41,46,57,94,99),(8,9,53,42,47,58,96,101),(8,9,54,43,48,59,98,103),(8,9,55,43,48,60,101,105),(8,9,56,44,49,61,103,107),(8,9,57,44,50,62,105,109),(8,9,58,45,51,63,107,112),(8,9,59,45,51,64,109,114),(8,9,60,46,52,65,111,116),(8,9,61,47,53,66,113,118),(8,9,62,47,53,67,115,121),(8,9,63,48,54,68,118,123),(8,9,64,48,55,69,120,125),(8,9,65,49,56,71,123,128),(8,9,66,50,57,72,125,130),(8,9,67,50,57,73,128,133),(8,9,68,51,58,74,130,135),(8,9,69,52,59,75,132,137),(8,9,70,52,60,76,135,140),(8,9,71,53,61,78,137,143),(8,9,72,54,61,79,140,145),(8,9,73,55,62,80,143,148),(8,9,74,55,63,81,146,151),(8,9,75,56,64,83,148,153),(8,9,76,57,65,84,151,156),(8,9,77,58,66,85,154,159),(8,9,78,58,67,87,156,162),(8,9,79,59,68,88,159,165),(8,9,80,60,69,89,162,167),(8,11,85,97,91,106,156,174),(8,11,84,96,89,104,156,171),(8,11,83,95,88,102,147,168),(8,11,82,93,87,101,144,165),(8,11,81,92,85,99,142,162),(10,1,85,195,125,173,40,61),(10,1,84,192,123,170,40,60),(10,1,83,189,121,167,39,59),(10,1,82,186,119,164,39,58),(10,1,81,174,117,161,39,58),(22,11,85,144,127,150,188,235),(22,11,84,134,117,140,178,225),(22,11,83,124,107,130,168,215),(22,11,82,114,97,120,158,205),(22,11,81,104,87,110,148,195),(22,9,85,109,117,147,209,220),(22,9,84,99,107,137,199,210),(22,9,83,89,97,127,189,200),(22,9,82,79,87,117,179,190),(22,9,81,69,77,107,169,180),(22,8,85,86,93,109,231,229),(22,8,84,76,83,99,221,219),(22,8,83,66,73,89,211,209),(22,8,82,56,63,79,201,199),(22,8,81,46,53,69,191,189),(22,6,85,230,162,210,85,110),(22,6,84,220,152,200,75,100),(22,6,83,210,142,190,65,90),(22,6,82,200,132,180,55,80),(22,6,81,190,122,170,45,70),(22,5,85,93,101,117,224,236),(22,5,84,83,91,107,214,226),(22,5,83,73,81,97,204,216),(22,5,82,63,71,87,194,206),(22,5,81,53,61,77,184,196),(22,4,85,163,239,159,93,119),(22,4,84,153,229,149,83,109),(22,4,83,143,219,139,73,99),(22,4,82,133,209,129,63,89),(22,4,81,123,199,119,53,79),(22,3,85,223,140,210,148,158),(22,3,84,213,130,200,138,148),(22,3,83,203,120,190,128,138),(22,3,82,193,110,180,118,128),(22,3,81,183,100,170,108,118),(22,1,85,234,163,218,86,110),(22,1,84,224,153,208,76,100),(22,1,83,214,143,198,66,90),(22,1,82,204,133,188,56,80),(22,1,81,194,123,178,46,70),(9,9,85,109,117,147,209,220),(9,9,84,99,107,137,199,210),(9,9,83,89,97,127,189,200),(9,9,82,79,87,117,179,190),(9,9,81,69,77,107,169,180),(9,8,85,86,93,109,231,229),(9,8,84,76,83,99,221,219),(9,8,83,66,73,89,211,209),(9,8,82,56,63,79,201,199),(9,8,81,46,53,69,191,189),(9,7,85,173,121,188,187,196),(9,7,84,163,111,178,177,186),(9,7,83,153,101,168,167,176),(9,7,82,143,91,158,157,166),(9,7,81,133,81,148,147,156),(9,6,85,230,162,210,85,110),(9,6,84,220,152,200,75,100),(9,6,83,210,142,190,65,90),(9,6,82,200,132,180,55,80),(9,6,81,190,122,170,45,70),(9,5,85,93,101,117,224,236),(9,5,84,83,91,107,214,226),(9,5,83,73,81,97,204,216),(9,5,82,63,71,87,194,206),(9,5,81,53,61,77,184,196),(9,4,85,163,239,159,93,119),(9,4,84,153,229,149,83,109),(9,4,83,143,219,139,73,99),(9,4,82,133,209,129,63,89),(9,4,81,123,199,119,53,79),(9,3,85,223,140,210,148,158),(9,3,84,213,130,200,138,148),(9,3,83,203,120,190,128,138),(9,3,82,193,110,180,118,128),(9,3,81,183,100,170,108,118),(9,1,85,234,163,218,86,110),(9,1,84,224,153,208,76,100),(9,1,83,214,143,198,66,90),(9,1,82,204,133,188,56,80),(9,1,81,194,123,178,46,70),(11,8,85,38,43,63,207,192),(11,8,84,38,42,62,204,189),(11,8,83,37,42,61,200,186),(11,8,82,37,41,60,197,182),(11,8,81,37,41,60,194,179),(11,7,85,132,77,148,145,158),(11,7,84,130,76,145,139,155),(11,7,83,128,75,143,139,153),(11,7,82,125,74,141,139,150),(11,7,81,123,72,138,136,148),(11,6,85,192,118,174,36,65),(11,6,84,186,117,171,36,64),(11,6,83,186,115,168,36,63),(11,6,82,179,113,165,36,62),(11,6,81,179,111,163,35,62),(11,5,85,47,51,71,199,200),(11,5,84,46,51,70,196,197),(11,5,83,46,50,69,193,193),(11,5,82,45,49,68,189,190),(11,5,81,45,49,68,185,187),(11,3,85,81,205,139,97,107),(11,3,84,80,201,137,96,105),(11,3,83,79,197,135,94,104),(11,3,82,78,194,133,93,102),(11,3,81,76,191,130,91,101),(11,2,85,173,94,156,106,116),(11,2,84,171,93,153,104,114),(11,2,83,167,91,151,102,112),(11,2,82,164,90,148,101,110),(11,2,81,161,88,146,99,109),(11,1,85,199,120,173,37,65),(11,1,84,196,118,170,37,64),(11,1,83,193,116,167,36,63),(11,1,82,190,114,164,36,62),(11,1,81,178,112,161,36,62),(10,9,85,60,73,96,184,179),(10,9,84,59,72,95,181,176),(10,9,83,58,71,94,178,173),(10,9,82,57,70,92,175,170),(10,9,81,57,70,91,172,167),(10,8,85,34,48,63,210,188),(10,8,84,34,47,62,207,185),(10,8,83,33,47,61,203,182),(10,8,82,33,46,60,200,178),(10,8,81,33,46,60,197,175),(10,6,85,188,123,174,39,61),(10,6,84,182,122,171,39,60),(10,6,83,182,120,168,39,59),(10,6,82,175,118,165,39,58),(10,6,81,175,116,163,38,58),(10,5,85,43,56,71,202,196),(10,5,84,42,56,70,199,193),(10,5,83,42,55,69,196,189),(10,5,82,41,54,68,192,186),(10,5,81,41,54,68,188,183),(10,4,85,119,218,114,49,69),(10,4,84,117,215,112,48,68),(10,4,83,115,211,110,48,67),(10,4,82,113,207,108,47,66),(10,4,81,111,204,107,47,66),(10,3,85,77,210,139,100,103),(10,3,84,76,206,137,99,101),(10,3,83,75,202,135,97,100),(10,3,82,74,199,133,96,98),(10,3,81,72,196,130,94,97),(10,2,85,169,99,156,109,112),(10,2,84,167,98,153,107,110),(10,2,83,163,96,151,105,108),(10,2,82,160,95,148,104,106),(10,2,81,157,93,146,102,105),(8,8,85,38,48,63,203,191),(8,8,84,38,47,62,200,188),(8,8,83,37,47,61,196,185),(8,8,82,37,46,60,193,181),(8,8,81,37,46,60,190,178),(8,7,85,132,82,148,141,157),(8,7,84,130,81,145,135,154),(8,7,83,128,80,143,135,152),(8,7,82,125,79,141,135,149),(8,7,81,123,77,138,132,147),(8,6,85,192,123,174,32,64),(8,6,84,186,122,171,32,63),(8,6,83,186,120,168,32,62),(8,6,82,179,118,165,32,61),(8,6,81,179,116,163,31,61),(8,5,85,47,56,71,195,199),(8,5,84,46,56,70,192,196),(8,5,83,46,55,69,189,192),(8,5,82,45,54,68,185,189),(8,5,81,45,54,68,181,186),(8,4,85,123,218,114,42,72),(8,4,84,121,215,112,41,71),(8,4,83,119,211,110,41,70),(8,4,82,117,207,108,40,69),(8,4,81,115,204,107,40,69),(8,3,85,81,210,139,93,106),(8,3,84,80,206,137,92,104),(8,3,83,79,202,135,90,103),(8,3,82,78,199,133,89,101),(8,3,81,76,196,130,87,100),(8,1,85,199,125,173,33,64),(8,1,84,196,123,170,33,63),(8,1,83,193,121,167,32,62),(8,1,82,190,119,164,32,61),(8,1,81,178,117,161,32,61),(7,9,85,58,73,96,184,181),(7,9,84,57,72,95,181,178),(7,9,83,56,71,94,178,175),(7,9,82,55,70,92,175,172),(7,9,81,55,70,91,172,169),(7,8,85,32,48,63,210,190),(7,8,84,32,47,62,207,187),(7,8,83,31,47,61,203,184),(7,8,82,31,46,60,200,180),(7,8,81,31,46,60,197,177),(7,6,85,186,123,174,39,63),(7,6,84,180,122,171,39,62),(7,6,83,180,120,168,39,61),(7,6,82,173,118,165,39,60),(7,6,81,173,116,163,38,60),(7,4,85,117,218,114,49,71),(7,4,84,115,215,112,48,70),(7,4,83,113,211,110,48,69),(7,4,82,111,207,108,47,68),(7,4,81,109,204,107,47,68),(7,1,85,193,125,173,40,63),(7,1,84,190,123,170,40,62),(7,1,83,187,121,167,39,61),(7,1,82,184,119,164,39,60),(7,1,81,172,117,161,39,60),(6,11,85,101,85,107,156,175),(6,11,84,100,83,105,156,172),(6,11,83,99,82,103,147,169),(6,11,82,97,81,102,144,166),(6,11,81,96,79,100,142,163),(6,7,85,136,76,149,141,158),(6,7,84,134,75,146,135,155),(6,7,83,132,74,144,135,153),(6,7,82,129,73,142,135,150),(6,7,81,127,71,139,132,148),(6,6,85,196,117,175,32,65),(6,6,84,190,116,172,32,64),(6,6,83,190,114,169,32,63),(6,6,82,183,112,166,32,62),(6,6,81,183,110,164,31,62),(6,3,85,85,204,140,93,107),(6,3,84,84,200,138,92,105),(6,3,83,83,196,136,90,104),(6,3,82,82,193,134,89,102),(6,3,81,80,190,131,87,101),(6,1,85,203,119,174,33,65),(6,1,84,200,117,171,33,64),(6,1,83,197,115,168,32,63),(6,1,82,194,113,165,32,62),(6,1,81,182,111,162,32,62),(5,9,85,62,69,96,179,186),(5,9,84,61,68,95,176,183),(5,9,83,60,67,94,173,180),(5,9,82,59,66,92,170,177),(5,9,81,59,66,91,167,174),(5,8,85,36,44,63,205,195),(5,8,84,36,43,62,202,192),(5,8,83,35,43,61,198,189),(5,8,82,35,42,60,195,185),(5,8,81,35,42,60,192,182),(5,6,85,190,119,174,34,68),(5,6,84,184,118,171,34,67),(5,6,83,184,116,168,34,66),(5,6,82,177,114,165,34,65),(5,6,81,177,112,163,33,65),(5,5,85,45,52,71,197,203),(5,5,84,44,52,70,194,200),(5,5,83,44,51,69,191,196),(5,5,82,43,50,68,187,193),(5,5,81,43,50,68,183,190),(5,4,85,121,214,114,44,76),(5,4,84,119,211,112,43,75),(5,4,83,117,207,110,43,74),(5,4,82,115,203,108,42,73),(5,4,81,113,200,107,42,73),(5,1,85,197,121,173,35,68),(5,1,84,194,119,170,35,67),(5,1,83,191,117,167,34,66),(5,1,82,188,115,164,34,65),(5,1,81,176,113,161,34,65),(4,11,85,92,93,106,160,173),(4,11,84,91,91,104,160,170),(4,11,83,90,90,102,151,167),(4,11,82,88,89,101,148,164),(4,11,81,87,87,99,146,161),(4,6,85,187,125,174,36,63),(4,6,84,181,124,171,36,62),(4,6,83,181,122,168,36,61),(4,6,82,174,120,165,36,60),(4,6,81,174,118,163,35,60),(4,5,85,42,58,71,199,198),(4,5,84,41,58,70,196,195),(4,5,83,41,57,69,193,191),(4,5,82,40,56,68,189,188),(4,5,81,40,56,68,185,185),(4,4,85,118,220,114,46,71),(4,4,84,116,217,112,45,70),(4,4,83,114,213,110,45,69),(4,4,82,112,209,108,44,68),(4,4,81,110,206,107,44,68),(4,3,85,76,212,139,97,105),(4,3,84,75,208,137,96,103),(4,3,83,74,204,135,94,102),(4,3,82,73,201,133,93,100),(4,3,81,71,198,130,91,99),(4,1,85,194,127,173,37,63),(4,1,84,191,125,170,37,62),(4,1,83,188,123,167,36,61),(4,1,82,185,121,164,36,60),(4,1,81,173,119,161,36,60),(3,6,85,196,117,175,35,62),(3,6,84,190,116,172,35,61),(3,6,83,190,114,169,35,60),(3,6,82,183,112,166,35,59),(3,6,81,183,110,164,34,59),(3,5,85,51,50,72,198,197),(3,5,84,50,50,71,195,194),(3,5,83,50,49,70,192,190),(3,5,82,49,48,69,188,187),(3,5,81,49,48,69,184,184),(3,4,85,127,212,115,45,70),(3,4,84,125,209,113,44,69),(3,4,83,123,205,111,44,68),(3,4,82,121,201,109,43,67),(3,4,81,119,198,108,43,67),(3,3,85,85,204,140,96,104),(3,3,84,84,200,138,95,102),(3,3,83,83,196,136,93,101),(3,3,82,82,193,134,92,99),(3,3,81,80,190,131,90,98),(3,2,85,177,93,157,105,113),(3,2,84,175,92,154,103,111),(3,2,83,171,90,152,101,109),(3,2,82,168,89,149,100,107),(3,2,81,165,87,147,98,106),(3,1,85,203,119,174,36,62),(3,1,84,200,117,171,36,61),(3,1,83,197,115,168,35,60),(3,1,82,194,113,165,35,59),(3,1,81,182,111,162,35,59),(2,9,85,66,68,97,178,183),(2,9,84,65,67,96,175,180),(2,9,83,64,66,95,172,177),(2,9,82,63,65,93,169,174),(2,9,81,63,65,92,166,171),(2,7,85,134,77,149,142,158),(2,7,84,132,76,146,136,155),(2,7,83,130,75,144,136,153),(2,7,82,127,74,142,136,150),(2,7,81,125,72,139,133,148),(2,6,85,194,118,175,33,65),(2,6,84,188,117,172,33,64),(2,6,83,188,115,169,33,63),(2,6,82,181,113,166,33,62),(2,6,81,181,111,164,32,62),(2,4,85,125,213,115,43,73),(2,4,84,123,210,113,42,72),(2,4,83,121,206,111,42,71),(2,4,82,119,202,109,41,70),(2,4,81,117,199,108,41,70),(2,3,85,83,205,140,94,107),(2,3,84,82,201,138,93,105),(2,3,83,81,197,136,91,104),(2,3,82,80,194,134,90,102),(2,3,81,78,191,131,88,101),(2,1,85,201,120,174,34,65),(2,1,84,198,118,171,34,64),(2,1,83,195,116,168,33,63),(2,1,82,192,114,165,33,62),(2,1,81,180,112,162,33,62),(1,9,85,63,71,96,181,181),(1,9,84,62,70,95,178,178),(1,9,83,61,69,94,175,175),(1,9,82,60,68,92,172,172),(1,9,81,60,68,91,169,169),(1,8,85,37,46,63,207,190),(1,8,84,37,45,62,204,187),(1,8,83,36,45,61,200,184),(1,8,82,36,44,60,197,180),(1,8,81,36,44,60,194,177),(1,6,85,191,121,174,36,63),(1,6,84,185,120,171,36,62),(1,6,83,185,118,168,36,61),(1,6,82,178,116,165,36,60),(1,6,81,178,114,163,35,60),(1,5,85,46,54,71,199,198),(1,5,84,45,54,70,196,195),(1,5,83,45,53,69,193,191),(1,5,82,44,52,68,189,188),(1,5,81,44,52,68,185,185),(1,4,85,122,216,114,46,71),(1,4,84,120,213,112,45,70),(1,4,83,118,209,110,45,69),(1,4,82,116,205,108,44,68),(1,4,81,114,202,107,44,68),(1,2,85,172,97,156,106,114),(1,2,84,170,96,153,104,112),(1,2,83,166,94,151,102,110),(1,2,82,163,93,148,101,108),(1,2,81,160,91,146,99,107),(1,1,85,198,123,173,37,63),(1,1,84,195,121,170,37,62),(1,1,83,192,119,167,36,61),(1,1,82,189,117,164,36,60),(1,1,81,177,115,161,36,60),(1,7,10,28,24,29,28,30),(1,7,11,29,24,30,29,31),(1,7,12,30,25,31,30,32),(1,7,13,30,25,32,31,33),(1,7,14,31,26,33,32,34),(1,7,15,32,26,34,33,36),(1,7,16,33,27,35,34,37),(1,7,17,34,27,36,35,38),(1,7,18,35,28,37,36,39),(1,7,19,36,28,38,37,40),(1,7,20,37,29,39,38,41),(1,7,21,38,29,40,39,42),(1,7,22,38,30,41,40,43),(1,7,23,39,30,42,41,44),(1,7,24,40,31,43,42,46),(1,7,25,41,31,45,43,47),(1,7,26,42,32,46,44,48),(1,7,27,43,32,47,45,49),(1,7,28,44,33,48,46,50),(1,7,29,45,33,49,47,52),(1,7,30,46,34,50,48,53),(1,7,31,48,34,52,50,54),(1,7,32,49,35,53,51,56),(1,7,33,50,36,54,52,57),(1,7,34,51,36,55,53,58),(1,7,35,52,37,57,54,60),(1,7,36,53,38,58,56,61),(1,7,37,54,38,59,57,62),(1,7,38,55,39,61,58,64),(1,7,39,56,39,62,59,65),(1,7,40,58,40,63,61,67),(1,7,41,59,41,65,62,68),(1,7,42,60,41,66,63,70),(1,7,43,61,42,68,64,71),(1,7,44,63,43,69,66,73),(1,7,45,64,43,71,67,74),(1,7,46,65,44,72,69,76),(1,7,47,66,45,74,70,77),(1,7,48,68,46,75,71,79),(1,7,49,69,46,77,73,81),(1,7,50,70,47,78,74,82),(1,7,51,72,48,80,76,84),(1,7,52,73,49,81,77,86),(1,7,53,75,49,83,79,87),(1,7,54,76,50,85,80,89),(1,7,55,77,51,86,82,91),(1,7,56,79,52,88,83,93),(1,7,57,80,53,90,85,94),(1,7,58,82,53,91,87,96),(1,7,59,83,54,93,88,98),(1,7,60,85,55,95,90,100),(1,7,61,87,56,97,92,102),(1,7,62,88,57,99,93,104),(1,7,63,90,58,100,95,106),(1,7,64,91,58,102,97,108),(1,7,65,93,59,104,99,110),(1,7,66,95,60,106,100,112),(1,7,67,96,61,108,102,114),(1,7,68,98,62,110,104,116),(1,7,69,100,63,112,106,118),(1,7,70,102,64,114,108,120),(1,7,71,103,65,116,110,122),(1,7,72,105,66,118,112,125),(1,7,73,107,67,120,114,127),(1,7,74,109,68,123,116,129),(1,7,75,111,69,125,118,131),(1,7,76,113,70,127,120,134),(1,7,77,114,71,129,122,136),(1,7,78,116,72,131,124,138),(1,7,79,118,73,134,126,141),(1,7,80,120,74,136,128,143),(1,7,81,122,75,138,136,146),(1,7,82,124,77,141,139,148),(1,7,83,127,78,143,139,151),(1,7,84,129,79,145,139,153),(1,7,85,131,80,148,145,156),(1,11,10,26,24,25,30,31),(1,11,11,26,25,26,31,33),(1,11,12,27,25,27,32,34),(1,11,13,27,26,27,33,35),(1,11,14,28,26,28,34,36),(1,11,15,29,27,29,36,37),(1,11,16,29,27,29,37,38),(1,11,17,30,28,30,38,40),(1,11,18,30,29,31,39,41),(1,11,19,31,29,31,40,42),(1,11,20,32,30,32,41,43),(1,11,21,32,30,33,42,45),(1,11,22,33,31,34,43,46),(1,11,23,34,32,34,44,47),(1,11,24,34,32,35,46,49),(1,11,25,35,33,36,47,50),(1,11,26,36,33,37,48,51),(1,11,27,36,34,37,49,53),(1,11,28,37,35,38,50,54),(1,11,29,38,35,39,52,56),(1,11,30,38,36,40,53,57),(1,11,31,39,37,41,54,58),(1,11,32,40,37,42,56,60),(1,11,33,41,38,42,57,61),(1,11,34,41,39,43,58,63),(1,11,35,42,39,44,60,64),(1,11,36,43,40,45,61,66),(1,11,37,44,41,46,62,68),(1,11,38,45,41,47,64,69),(1,11,39,45,42,48,65,71),(1,11,40,46,43,49,67,72),(1,11,41,47,44,50,68,74),(1,11,42,48,44,51,70,76),(1,11,43,49,45,52,71,77),(1,11,44,50,46,52,73,79),(1,11,45,50,47,53,74,81),(1,11,46,51,48,54,76,83),(1,11,47,52,48,56,77,84),(1,11,48,53,49,57,79,86),(1,11,49,54,50,58,81,88),(1,11,50,55,51,59,82,90),(1,11,51,56,52,60,84,92),(1,11,52,57,53,61,86,94),(1,11,53,58,54,62,87,96),(1,11,54,59,54,63,89,98),(1,11,55,60,55,64,91,100),(1,11,56,61,56,65,93,102),(1,11,57,62,57,66,94,104),(1,11,58,63,58,68,96,106),(1,11,59,64,59,69,98,108),(1,11,60,65,60,70,100,110),(1,11,61,66,61,71,102,112),(1,11,62,67,62,72,104,114),(1,11,63,68,63,74,106,117),(1,11,64,69,64,75,108,119),(1,11,65,71,65,76,110,121),(1,11,66,72,66,78,112,123),(1,11,67,73,67,79,114,126),(1,11,68,74,68,80,116,128),(1,11,69,75,69,82,118,130),(1,11,70,76,70,83,120,133),(1,11,71,78,71,84,122,135),(1,11,72,79,73,86,125,138),(1,11,73,80,74,87,127,140),(1,11,74,81,75,89,129,143),(1,11,75,83,76,90,131,145),(1,11,76,84,77,92,134,148),(1,11,77,85,78,93,136,151),(1,11,78,87,80,95,138,153),(1,11,79,88,81,96,141,156),(1,11,80,89,82,98,143,159),(1,11,81,91,83,99,146,161),(1,11,82,92,85,101,148,164),(1,11,83,94,86,102,151,167),(1,11,84,95,87,104,160,170),(1,11,85,96,89,106,160,173),(2,2,10,34,22,31,22,29),(2,2,11,35,22,32,23,30),(2,2,12,36,23,33,24,30),(2,2,13,37,24,34,24,31),(2,2,14,38,24,35,25,32),(2,2,15,39,25,37,26,32),(2,2,16,41,25,38,26,33),(2,2,17,42,26,39,27,34),(2,2,18,43,27,40,28,35),(2,2,19,44,27,41,28,35),(2,2,20,45,28,42,29,36),(2,2,21,46,29,43,30,37),(2,2,22,48,29,44,31,38),(2,2,23,49,30,45,31,39),(2,2,24,50,31,47,32,39),(2,2,25,51,31,48,33,40),(2,2,26,53,32,49,34,41),(2,2,27,54,33,50,34,42),(2,2,28,55,33,51,35,43),(2,2,29,57,34,53,36,44),(2,2,30,58,35,54,37,44),(2,2,31,59,36,55,38,45),(2,2,32,61,36,57,39,46),(2,2,33,62,37,58,39,47),(2,2,34,64,38,59,40,48),(2,2,35,65,39,61,41,49),(2,2,36,67,40,62,42,50),(2,2,37,68,40,63,43,51),(2,2,38,70,41,65,44,52),(2,2,39,71,42,66,45,53),(2,2,40,73,43,68,46,54),(2,2,41,74,44,69,47,55),(2,2,42,76,44,71,48,56),(2,2,43,77,45,72,49,57),(2,2,44,79,46,74,49,58),(2,2,45,81,47,75,50,59),(2,2,46,82,48,77,51,60),(2,2,47,84,49,78,53,61),(2,2,48,86,50,80,54,62),(2,2,49,87,51,82,55,64),(2,2,50,89,52,83,56,65),(2,2,51,91,53,85,57,66),(2,2,52,93,54,87,58,67),(2,2,53,95,55,88,59,68),(2,2,54,96,56,90,60,69),(2,2,55,98,57,92,61,71),(2,2,56,100,58,94,62,72),(2,2,57,102,59,95,63,73),(2,2,58,104,60,97,65,74),(2,2,59,111,61,99,66,76),(2,2,60,111,62,101,67,77),(2,2,61,111,63,103,68,78),(2,2,62,117,64,105,69,80),(2,2,63,119,65,107,71,81),(2,2,64,119,66,109,72,82),(2,2,65,119,68,111,73,84),(2,2,66,121,69,113,75,85),(2,2,67,123,70,115,76,87),(2,2,68,125,71,117,77,88),(2,2,69,133,72,119,79,89),(2,2,70,133,74,121,80,91),(2,2,71,133,75,123,81,92),(2,2,72,140,76,126,83,94),(2,2,73,142,77,128,84,96),(2,2,74,142,79,130,86,97),(2,2,75,147,80,132,87,99),(2,2,76,147,81,135,89,100),(2,2,77,153,83,137,90,102),(2,2,78,156,84,139,92,104),(2,2,79,158,85,142,93,105),(2,2,80,158,87,144,95,107),(2,2,81,163,88,147,96,109),(2,2,82,166,90,149,98,110),(2,2,83,169,91,152,99,112),(2,2,84,173,93,154,101,114),(2,2,85,175,94,157,103,116),(2,5,10,25,19,24,30,36),(2,5,11,25,19,25,31,37),(2,5,12,25,20,25,32,39),(2,5,13,25,20,25,33,40),(2,5,14,25,20,26,35,41),(2,5,15,26,20,26,36,43),(2,5,16,26,21,27,37,44),(2,5,17,26,21,27,39,45),(2,5,18,26,21,27,40,47),(2,5,19,26,22,28,41,48),(2,5,20,27,22,28,43,50),(2,5,21,27,22,29,44,51),(2,5,22,27,22,29,46,53),(2,5,23,27,23,30,47,54),(2,5,24,28,23,30,49,56),(2,5,25,28,23,31,50,57),(2,5,26,28,24,31,52,59),(2,5,27,28,24,31,53,61),(2,5,28,28,24,32,55,62),(2,5,29,29,25,32,56,64),(2,5,30,29,25,33,58,66),(2,5,31,29,25,33,60,67),(2,5,32,29,26,34,61,69),(2,5,33,30,26,34,63,71),(2,5,34,30,26,35,65,72),(2,5,35,30,27,35,66,74),(2,5,36,31,27,36,68,76),(2,5,37,31,27,37,70,78),(2,5,38,31,28,37,72,80),(2,5,39,31,28,38,73,82),(2,5,40,32,28,38,75,83),(2,5,41,32,29,39,77,85),(2,5,42,32,29,39,79,87),(2,5,43,32,30,40,81,89),(2,5,44,33,30,40,83,91),(2,5,45,33,30,41,85,93),(2,5,46,33,31,42,87,95),(2,5,47,34,31,42,89,97),(2,5,48,34,32,43,91,100),(2,5,49,34,32,44,93,102),(2,5,50,35,32,44,99,104),(2,5,51,35,33,45,102,106),(2,5,52,35,33,45,104,108),(2,5,53,36,34,46,106,110),(2,5,54,36,34,47,108,113),(2,5,55,36,35,47,111,115),(2,5,56,37,35,48,113,117),(2,5,57,37,36,49,115,120),(2,5,58,37,36,50,117,122),(2,5,59,38,37,50,120,125),(2,5,60,38,37,51,123,127),(2,5,61,38,37,52,125,129),(2,5,62,39,38,52,128,132),(2,5,63,39,38,53,130,135),(2,5,64,39,39,54,133,137),(2,5,65,40,40,55,135,140),(2,5,66,40,40,56,138,142),(2,5,67,41,41,56,140,145),(2,5,68,41,41,57,144,148),(2,5,69,41,42,58,147,151),(2,5,70,42,42,59,149,153),(2,5,71,42,43,60,152,156),(2,5,72,43,43,60,155,159),(2,5,73,43,44,61,158,162),(2,5,74,44,44,62,160,165),(2,5,75,44,45,63,163,168),(2,5,76,44,46,64,167,171),(2,5,77,45,46,65,170,174),(2,5,78,45,47,66,173,177),(2,5,79,46,47,67,176,180),(2,5,80,46,48,68,179,183),(2,5,81,47,49,69,182,187),(2,5,82,47,49,69,186,190),(2,5,83,48,50,70,190,193),(2,5,84,48,51,71,193,197),(2,5,85,49,51,72,196,200),(2,11,10,29,21,26,27,33),(2,11,11,29,22,27,28,35),(2,11,12,30,22,28,29,36),(2,11,13,30,23,28,30,37),(2,11,14,31,23,29,31,38),(2,11,15,32,24,30,33,39),(2,11,16,32,24,30,34,40),(2,11,17,33,25,31,35,42),(2,11,18,33,26,32,36,43),(2,11,19,34,26,32,37,44),(2,11,20,35,27,33,38,45),(2,11,21,35,27,34,39,47),(2,11,22,36,28,35,40,48),(2,11,23,37,29,35,41,49),(2,11,24,37,29,36,43,51),(2,11,25,38,30,37,44,52),(2,11,26,39,30,38,45,53),(2,11,27,39,31,38,46,55),(2,11,28,40,32,39,47,56),(2,11,29,41,32,40,49,58),(2,11,30,41,33,41,50,59),(2,11,31,42,34,42,51,60),(2,11,32,43,34,43,53,62),(2,11,33,44,35,43,54,63),(2,11,34,44,36,44,55,65),(2,11,35,45,36,45,57,66),(2,11,36,46,37,46,58,68),(2,11,37,47,38,47,59,70),(2,11,38,48,38,48,61,71),(2,11,39,48,39,49,62,73),(2,11,40,49,40,50,64,74),(2,11,41,50,41,51,65,76),(2,11,42,51,41,52,67,78),(2,11,43,52,42,53,68,79),(2,11,44,53,43,53,70,81),(2,11,45,53,44,54,71,83),(2,11,46,54,45,55,73,85),(2,11,47,55,45,57,74,86),(2,11,48,56,46,58,76,88),(2,11,49,57,47,59,78,90),(2,11,50,58,48,60,79,92),(2,11,51,59,49,61,81,94),(2,11,52,60,50,62,83,96),(2,11,53,61,51,63,84,98),(2,11,54,62,51,64,86,100),(2,11,55,63,52,65,88,102),(2,11,56,64,53,66,90,104),(2,11,57,65,54,67,91,106),(2,11,58,66,55,69,93,108),(2,11,59,67,56,70,95,110),(2,11,60,68,57,71,97,112),(2,11,61,69,58,72,99,114),(2,11,62,70,59,73,101,116),(2,11,63,71,60,75,103,119),(2,11,64,72,61,76,105,121),(2,11,65,74,62,77,107,123),(2,11,66,75,63,79,109,125),(2,11,67,76,64,80,111,128),(2,11,68,77,65,81,113,130),(2,11,69,78,66,83,115,132),(2,11,70,79,67,84,117,135),(2,11,71,81,68,85,119,137),(2,11,72,82,70,87,122,140),(2,11,73,83,71,88,124,142),(2,11,74,84,72,90,126,145),(2,11,75,86,73,91,128,147),(2,11,76,87,74,93,131,150),(2,11,77,88,75,94,133,153),(2,11,78,90,77,96,135,155),(2,11,79,91,78,97,138,158),(2,11,80,92,79,99,140,161),(2,11,81,94,80,100,143,163),(2,11,82,95,82,102,145,166),(2,11,83,97,83,103,148,169),(2,11,84,98,84,105,157,172),(2,11,85,99,86,107,157,175),(3,11,10,31,20,26,29,30),(3,11,11,31,21,27,30,32),(3,11,12,32,21,28,31,33),(3,11,13,32,22,28,32,34),(3,11,14,33,22,29,33,35),(3,11,15,34,23,30,35,36),(3,11,16,34,23,30,36,37),(3,11,17,35,24,31,37,39),(3,11,18,35,25,32,38,40),(3,11,19,36,25,32,39,41),(3,11,20,37,26,33,40,42),(3,11,21,37,26,34,41,44),(3,11,22,38,27,35,42,45),(3,11,23,39,28,35,43,46),(3,11,24,39,28,36,45,48),(3,11,25,40,29,37,46,49),(3,11,26,41,29,38,47,50),(3,11,27,41,30,38,48,52),(3,11,28,42,31,39,49,53),(3,11,29,43,31,40,51,55),(3,11,30,43,32,41,52,56),(3,11,31,44,33,42,53,57),(3,11,32,45,33,43,55,59),(3,11,33,46,34,43,56,60),(3,11,34,46,35,44,57,62),(3,11,35,47,35,45,59,63),(3,11,36,48,36,46,60,65),(3,11,37,49,37,47,61,67),(3,11,38,50,37,48,63,68),(3,11,39,50,38,49,64,70),(3,11,40,51,39,50,66,71),(3,11,41,52,40,51,67,73),(3,11,42,53,40,52,69,75),(3,11,43,54,41,53,70,76),(3,11,44,55,42,53,72,78),(3,11,45,55,43,54,73,80),(3,11,46,56,44,55,75,82),(3,11,47,57,44,57,76,83),(3,11,48,58,45,58,78,85),(3,11,49,59,46,59,80,87),(3,11,50,60,47,60,81,89),(3,11,51,61,48,61,83,91),(3,11,52,62,49,62,85,93),(3,11,53,63,50,63,86,95),(3,11,54,64,50,64,88,97),(3,11,55,65,51,65,90,99),(3,11,56,66,52,66,92,101),(3,11,57,67,53,67,93,103),(3,11,58,68,54,69,95,105),(3,11,59,69,55,70,97,107),(3,11,60,70,56,71,99,109),(3,11,61,71,57,72,101,111),(3,11,62,72,58,73,103,113),(3,11,63,73,59,75,105,116),(3,11,64,74,60,76,107,118),(3,11,65,76,61,77,109,120),(3,11,66,77,62,79,111,122),(3,11,67,78,63,80,113,125),(3,11,68,79,64,81,115,127),(3,11,69,80,65,83,117,129),(3,11,70,81,66,84,119,132),(3,11,71,83,67,85,121,134),(3,11,72,84,69,87,124,137),(3,11,73,85,70,88,126,139),(3,11,74,86,71,90,128,142),(3,11,75,88,72,91,130,144),(3,11,76,89,73,93,133,147),(3,11,77,90,74,94,135,150),(3,11,78,92,76,96,137,152),(3,11,79,93,77,97,140,155),(3,11,80,94,78,99,142,158),(3,11,81,96,79,100,145,160),(3,11,82,97,81,102,147,163),(3,11,83,99,82,103,150,166),(3,11,84,100,83,105,159,169),(3,11,85,101,85,107,159,172),(4,2,10,27,29,30,25,27),(4,2,11,28,29,31,26,28),(4,2,12,29,30,32,27,28),(4,2,13,30,31,33,27,29),(4,2,14,31,31,34,28,30),(4,2,15,32,32,36,29,30),(4,2,16,34,32,37,29,31),(4,2,17,35,33,38,30,32),(4,2,18,36,34,39,31,33),(4,2,19,37,34,40,31,33),(4,2,20,38,35,41,32,34),(4,2,21,39,36,42,33,35),(4,2,22,41,36,43,34,36),(4,2,23,42,37,44,34,37),(4,2,24,43,38,46,35,37),(4,2,25,44,38,47,36,38),(4,2,26,46,39,48,37,39),(4,2,27,47,40,49,37,40),(4,2,28,48,40,50,38,41),(4,2,29,50,41,52,39,42),(4,2,30,51,42,53,40,42),(4,2,31,52,43,54,41,43),(4,2,32,54,43,56,42,44),(4,2,33,55,44,57,42,45),(4,2,34,57,45,58,43,46),(4,2,35,58,46,60,44,47),(4,2,36,60,47,61,45,48),(4,2,37,61,47,62,46,49),(4,2,38,63,48,64,47,50),(4,2,39,64,49,65,48,51),(4,2,40,66,50,67,49,52),(4,2,41,67,51,68,50,53),(4,2,42,69,51,70,51,54),(4,2,43,70,52,71,52,55),(4,2,44,72,53,73,52,56),(4,2,45,74,54,74,53,57),(4,2,46,75,55,76,54,58),(4,2,47,77,56,77,56,59),(4,2,48,79,57,79,57,60),(4,2,49,80,58,81,58,62),(4,2,50,82,59,82,59,63),(4,2,51,84,60,84,60,64),(4,2,52,86,61,86,61,65),(4,2,53,88,62,87,62,66),(4,2,54,89,63,89,63,67),(4,2,55,91,64,91,64,69),(4,2,56,93,65,93,65,70),(4,2,57,95,66,94,66,71),(4,2,58,97,67,96,68,72),(4,2,59,104,68,98,69,74),(4,2,60,104,69,100,70,75),(4,2,61,104,70,102,71,76),(4,2,62,110,71,104,72,78),(4,2,63,112,72,106,74,79),(4,2,64,112,73,108,75,80),(4,2,65,112,75,110,76,82),(4,2,66,114,76,112,78,83),(4,2,67,116,77,114,79,85),(4,2,68,118,78,116,80,86),(4,2,69,126,79,118,82,87),(4,2,70,126,81,120,83,89),(4,2,71,126,82,122,84,90),(4,2,72,133,83,125,86,92),(4,2,73,135,84,127,87,94),(4,2,74,135,86,129,89,95),(4,2,75,140,87,131,90,97),(4,2,76,140,88,134,92,98),(4,2,77,146,90,136,93,100),(4,2,78,149,91,138,95,102),(4,2,79,151,92,141,96,103),(4,2,80,151,94,143,98,105),(4,2,81,156,95,146,99,107),(4,2,82,159,97,148,101,108),(4,2,83,162,98,151,102,110),(4,2,84,166,100,153,104,112),(4,2,85,168,101,156,106,114),(4,7,10,24,28,29,28,30),(4,7,11,25,28,30,29,31),(4,7,12,26,29,31,30,32),(4,7,13,26,29,32,31,33),(4,7,14,27,30,33,32,34),(4,7,15,28,30,34,33,36),(4,7,16,29,31,35,34,37),(4,7,17,30,31,36,35,38),(4,7,18,31,32,37,36,39),(4,7,19,32,32,38,37,40),(4,7,20,33,33,39,38,41),(4,7,21,34,33,40,39,42),(4,7,22,34,34,41,40,43),(4,7,23,35,34,42,41,44),(4,7,24,36,35,43,42,46),(4,7,25,37,35,45,43,47),(4,7,26,38,36,46,44,48),(4,7,27,39,36,47,45,49),(4,7,28,40,37,48,46,50),(4,7,29,41,37,49,47,52),(4,7,30,42,38,50,48,53),(4,7,31,44,38,52,50,54),(4,7,32,45,39,53,51,56),(4,7,33,46,40,54,52,57),(4,7,34,47,40,55,53,58),(4,7,35,48,41,57,54,60),(4,7,36,49,42,58,56,61),(4,7,37,50,42,59,57,62),(4,7,38,51,43,61,58,64),(4,7,39,52,43,62,59,65),(4,7,40,54,44,63,61,67),(4,7,41,55,45,65,62,68),(4,7,42,56,45,66,63,70),(4,7,43,57,46,68,64,71),(4,7,44,59,47,69,66,73),(4,7,45,60,47,71,67,74),(4,7,46,61,48,72,69,76),(4,7,47,62,49,74,70,77),(4,7,48,64,50,75,71,79),(4,7,49,65,50,77,73,81),(4,7,50,66,51,78,74,82),(4,7,51,68,52,80,76,84),(4,7,52,69,53,81,77,86),(4,7,53,71,53,83,79,87),(4,7,54,72,54,85,80,89),(4,7,55,73,55,86,82,91),(4,7,56,75,56,88,83,93),(4,7,57,76,57,90,85,94),(4,7,58,78,57,91,87,96),(4,7,59,79,58,93,88,98),(4,7,60,81,59,95,90,100),(4,7,61,83,60,97,92,102),(4,7,62,84,61,99,93,104),(4,7,63,86,62,100,95,106),(4,7,64,87,62,102,97,108),(4,7,65,89,63,104,99,110),(4,7,66,91,64,106,100,112),(4,7,67,92,65,108,102,114),(4,7,68,94,66,110,104,116),(4,7,69,96,67,112,106,118),(4,7,70,98,68,114,108,120),(4,7,71,99,69,116,110,122),(4,7,72,101,70,118,112,125),(4,7,73,103,71,120,114,127),(4,7,74,105,72,123,116,129),(4,7,75,107,73,125,118,131),(4,7,76,109,74,127,120,134),(4,7,77,110,75,129,122,136),(4,7,78,112,76,131,124,138),(4,7,79,114,77,134,126,141),(4,7,80,116,78,136,128,143),(4,7,81,118,79,138,136,146),(4,7,82,120,81,141,139,148),(4,7,83,123,82,143,139,151),(4,7,84,125,83,145,139,153),(4,7,85,127,84,148,145,156),(4,9,10,19,27,26,31,32),(4,9,11,19,28,26,33,33),(4,9,12,19,28,27,34,34),(4,9,13,20,28,27,35,36),(4,9,14,20,29,28,36,37),(4,9,15,20,29,29,37,38),(4,9,16,21,30,29,38,39),(4,9,17,21,30,30,40,41),(4,9,18,21,30,30,41,42),(4,9,19,22,31,31,42,43),(4,9,20,22,31,32,43,45),(4,9,21,22,32,32,45,46),(4,9,22,23,32,33,46,47),(4,9,23,23,33,34,47,49),(4,9,24,24,33,34,49,50),(4,9,25,24,34,35,50,52),(4,9,26,24,34,36,51,53),(4,9,27,25,34,36,53,54),(4,9,28,25,35,37,54,56),(4,9,29,26,35,38,56,57),(4,9,30,26,36,38,57,59),(4,9,31,26,36,39,58,61),(4,9,32,27,37,40,60,62),(4,9,33,27,37,41,61,64),(4,9,34,28,38,41,63,65),(4,9,35,28,38,42,64,67),(4,9,36,29,39,43,66,69),(4,9,37,29,40,44,68,70),(4,9,38,29,40,45,69,72),(4,9,39,30,41,45,71,74),(4,9,40,30,41,46,72,75),(4,9,41,31,42,47,74,77),(4,9,42,31,42,48,76,79),(4,9,43,32,43,49,77,81),(4,9,44,32,43,50,79,82),(4,9,45,33,44,50,81,84),(4,9,46,33,45,51,83,86),(4,9,47,34,45,52,84,88),(4,9,48,34,46,53,86,90),(4,9,49,35,47,54,88,92),(4,9,50,35,47,55,94,94),(4,9,51,36,48,56,96,96),(4,9,52,36,48,57,98,98),(4,9,53,37,49,58,100,100),(4,9,54,38,50,59,102,102),(4,9,55,38,50,60,105,104),(4,9,56,39,51,61,107,106),(4,9,57,39,52,62,109,108),(4,9,58,40,53,63,111,111),(4,9,59,40,53,64,113,113),(4,9,60,41,54,65,115,115),(4,9,61,42,55,66,117,117),(4,9,62,42,55,67,119,120),(4,9,63,43,56,68,122,122),(4,9,64,43,57,69,124,124),(4,9,65,44,58,71,127,127),(4,9,66,45,59,72,129,129),(4,9,67,45,59,73,132,132),(4,9,68,46,60,74,134,134),(4,9,69,47,61,75,136,136),(4,9,70,47,62,76,139,139),(4,9,71,48,63,78,141,142),(4,9,72,49,63,79,144,144),(4,9,73,50,64,80,147,147),(4,9,74,50,65,81,150,150),(4,9,75,51,66,83,152,152),(4,9,76,52,67,84,155,155),(4,9,77,53,68,85,158,158),(4,9,78,53,69,87,160,161),(4,9,79,54,70,88,163,164),(4,9,80,55,71,89,166,166),(4,9,81,56,72,91,169,169),(4,9,82,56,72,92,172,172),(4,9,83,57,73,94,175,175),(4,9,84,58,74,95,178,178),(4,9,85,59,75,96,181,181),(5,2,10,30,23,30,23,32),(5,2,11,31,23,31,24,33),(5,2,12,32,24,32,25,33),(5,2,13,33,25,33,25,34),(5,2,14,34,25,34,26,35),(5,2,15,35,26,36,27,35),(5,2,16,37,26,37,27,36),(5,2,17,38,27,38,28,37),(5,2,18,39,28,39,29,38),(5,2,19,40,28,40,29,38),(5,2,20,41,29,41,30,39),(5,2,21,42,30,42,31,40),(5,2,22,44,30,43,32,41),(5,2,23,45,31,44,32,42),(5,2,24,46,32,46,33,42),(5,2,25,47,32,47,34,43),(5,2,26,49,33,48,35,44),(5,2,27,50,34,49,35,45),(5,2,28,51,34,50,36,46),(5,2,29,53,35,52,37,47),(5,2,30,54,36,53,38,47),(5,2,31,55,37,54,39,48),(5,2,32,57,37,56,40,49),(5,2,33,58,38,57,40,50),(5,2,34,60,39,58,41,51),(5,2,35,61,40,60,42,52),(5,2,36,63,41,61,43,53),(5,2,37,64,41,62,44,54),(5,2,38,66,42,64,45,55),(5,2,39,67,43,65,46,56),(5,2,40,69,44,67,47,57),(5,2,41,70,45,68,48,58),(5,2,42,72,45,70,49,59),(5,2,43,73,46,71,50,60),(5,2,44,75,47,73,50,61),(5,2,45,77,48,74,51,62),(5,2,46,78,49,76,52,63),(5,2,47,80,50,77,54,64),(5,2,48,82,51,79,55,65),(5,2,49,83,52,81,56,67),(5,2,50,85,53,82,57,68),(5,2,51,87,54,84,58,69),(5,2,52,89,55,86,59,70),(5,2,53,91,56,87,60,71),(5,2,54,92,57,89,61,72),(5,2,55,94,58,91,62,74),(5,2,56,96,59,93,63,75),(5,2,57,98,60,94,64,76),(5,2,58,100,61,96,66,77),(5,2,59,107,62,98,67,79),(5,2,60,107,63,100,68,80),(5,2,61,107,64,102,69,81),(5,2,62,113,65,104,70,83),(5,2,63,115,66,106,72,84),(5,2,64,115,67,108,73,85),(5,2,65,115,69,110,74,87),(5,2,66,117,70,112,76,88),(5,2,67,119,71,114,77,90),(5,2,68,121,72,116,78,91),(5,2,69,129,73,118,80,92),(5,2,70,129,75,120,81,94),(5,2,71,129,76,122,82,95),(5,2,72,136,77,125,84,97),(5,2,73,138,78,127,85,99),(5,2,74,138,80,129,87,100),(5,2,75,143,81,131,88,102),(5,2,76,143,82,134,90,103),(5,2,77,149,84,136,91,105),(5,2,78,152,85,138,93,107),(5,2,79,154,86,141,94,108),(5,2,80,154,88,143,96,110),(5,2,81,159,89,146,97,112),(5,2,82,162,91,148,99,113),(5,2,83,165,92,151,100,115),(5,2,84,169,94,153,102,117),(5,2,85,171,95,156,104,119),(5,7,10,27,22,29,26,35),(5,7,11,28,22,30,27,36),(5,7,12,29,23,31,28,37),(5,7,13,29,23,32,29,38),(5,7,14,30,24,33,30,39),(5,7,15,31,24,34,31,41),(5,7,16,32,25,35,32,42),(5,7,17,33,25,36,33,43),(5,7,18,34,26,37,34,44),(5,7,19,35,26,38,35,45),(5,7,20,36,27,39,36,46),(5,7,21,37,27,40,37,47),(5,7,22,37,28,41,38,48),(5,7,23,38,28,42,39,49),(5,7,24,39,29,43,40,51),(5,7,25,40,29,45,41,52),(5,7,26,41,30,46,42,53),(5,7,27,42,30,47,43,54),(5,7,28,43,31,48,44,55),(5,7,29,44,31,49,45,57),(5,7,30,45,32,50,46,58),(5,7,31,47,32,52,48,59),(5,7,32,48,33,53,49,61),(5,7,33,49,34,54,50,62),(5,7,34,50,34,55,51,63),(5,7,35,51,35,57,52,65),(5,7,36,52,36,58,54,66),(5,7,37,53,36,59,55,67),(5,7,38,54,37,61,56,69),(5,7,39,55,37,62,57,70),(5,7,40,57,38,63,59,72),(5,7,41,58,39,65,60,73),(5,7,42,59,39,66,61,75),(5,7,43,60,40,68,62,76),(5,7,44,62,41,69,64,78),(5,7,45,63,41,71,65,79),(5,7,46,64,42,72,67,81),(5,7,47,65,43,74,68,82),(5,7,48,67,44,75,69,84),(5,7,49,68,44,77,71,86),(5,7,50,69,45,78,72,87),(5,7,51,71,46,80,74,89),(5,7,52,72,47,81,75,91),(5,7,53,74,47,83,77,92),(5,7,54,75,48,85,78,94),(5,7,55,76,49,86,80,96),(5,7,56,78,50,88,81,98),(5,7,57,79,51,90,83,99),(5,7,58,81,51,91,85,101),(5,7,59,82,52,93,86,103),(5,7,60,84,53,95,88,105),(5,7,61,86,54,97,90,107),(5,7,62,87,55,99,91,109),(5,7,63,89,56,100,93,111),(5,7,64,90,56,102,95,113),(5,7,65,92,57,104,97,115),(5,7,66,94,58,106,98,117),(5,7,67,95,59,108,100,119),(5,7,68,97,60,110,102,121),(5,7,69,99,61,112,104,123),(5,7,70,101,62,114,106,125),(5,7,71,102,63,116,108,127),(5,7,72,104,64,118,110,130),(5,7,73,106,65,120,112,132),(5,7,74,108,66,123,114,134),(5,7,75,110,67,125,116,136),(5,7,76,112,68,127,118,139),(5,7,77,113,69,129,120,141),(5,7,78,115,70,131,122,143),(5,7,79,117,71,134,124,146),(5,7,80,119,72,136,126,148),(5,7,81,121,73,138,134,151),(5,7,82,123,75,141,137,153),(5,7,83,126,76,143,137,156),(5,7,84,128,77,145,137,158),(5,7,85,130,78,148,143,161),(5,11,10,25,22,25,28,36),(5,11,11,25,23,26,29,38),(5,11,12,26,23,27,30,39),(5,11,13,26,24,27,31,40),(5,11,14,27,24,28,32,41),(5,11,15,28,25,29,34,42),(5,11,16,28,25,29,35,43),(5,11,17,29,26,30,36,45),(5,11,18,29,27,31,37,46),(5,11,19,30,27,31,38,47),(5,11,20,31,28,32,39,48),(5,11,21,31,28,33,40,50),(5,11,22,32,29,34,41,51),(5,11,23,33,30,34,42,52),(5,11,24,33,30,35,44,54),(5,11,25,34,31,36,45,55),(5,11,26,35,31,37,46,56),(5,11,27,35,32,37,47,58),(5,11,28,36,33,38,48,59),(5,11,29,37,33,39,50,61),(5,11,30,37,34,40,51,62),(5,11,31,38,35,41,52,63),(5,11,32,39,35,42,54,65),(5,11,33,40,36,42,55,66),(5,11,34,40,37,43,56,68),(5,11,35,41,37,44,58,69),(5,11,36,42,38,45,59,71),(5,11,37,43,39,46,60,73),(5,11,38,44,39,47,62,74),(5,11,39,44,40,48,63,76),(5,11,40,45,41,49,65,77),(5,11,41,46,42,50,66,79),(5,11,42,47,42,51,68,81),(5,11,43,48,43,52,69,82),(5,11,44,49,44,52,71,84),(5,11,45,49,45,53,72,86),(5,11,46,50,46,54,74,88),(5,11,47,51,46,56,75,89),(5,11,48,52,47,57,77,91),(5,11,49,53,48,58,79,93),(5,11,50,54,49,59,80,95),(5,11,51,55,50,60,82,97),(5,11,52,56,51,61,84,99),(5,11,53,57,52,62,85,101),(5,11,54,58,52,63,87,103),(5,11,55,59,53,64,89,105),(5,11,56,60,54,65,91,107),(5,11,57,61,55,66,92,109),(5,11,58,62,56,68,94,111),(5,11,59,63,57,69,96,113),(5,11,60,64,58,70,98,115),(5,11,61,65,59,71,100,117),(5,11,62,66,60,72,102,119),(5,11,63,67,61,74,104,122),(5,11,64,68,62,75,106,124),(5,11,65,70,63,76,108,126),(5,11,66,71,64,78,110,128),(5,11,67,72,65,79,112,131),(5,11,68,73,66,80,114,133),(5,11,69,74,67,82,116,135),(5,11,70,75,68,83,118,138),(5,11,71,77,69,84,120,140),(5,11,72,78,71,86,123,143),(5,11,73,79,72,87,125,145),(5,11,74,80,73,89,127,148),(5,11,75,82,74,90,129,150),(5,11,76,83,75,92,132,153),(5,11,77,84,76,93,134,156),(5,11,78,86,78,95,136,158),(5,11,79,87,79,96,139,161),(5,11,80,88,80,98,141,164),(5,11,81,90,81,99,144,166),(5,11,82,91,83,101,146,169),(5,11,83,93,84,102,149,172),(5,11,84,94,85,104,158,175),(5,11,85,95,87,106,158,178),(6,4,10,32,31,28,18,25),(6,4,11,33,32,29,18,26),(6,4,12,34,33,29,18,26),(6,4,13,35,35,30,18,26),(6,4,14,35,36,31,18,27),(6,4,15,36,38,31,19,27),(6,4,16,37,39,32,19,28),(6,4,17,38,40,33,19,28),(6,4,18,39,42,34,19,28),(6,4,19,40,44,34,19,29),(6,4,20,40,45,35,20,29),(6,4,21,41,47,36,20,30),(6,4,22,42,48,37,20,30),(6,4,23,43,50,38,20,31),(6,4,24,44,51,38,21,31),(6,4,25,45,53,39,21,32),(6,4,26,46,55,40,21,32),(6,4,27,47,56,41,21,32),(6,4,28,48,58,42,21,33),(6,4,29,48,60,43,22,33),(6,4,30,49,62,43,22,34),(6,4,31,50,63,44,22,34),(6,4,32,51,65,45,22,35),(6,4,33,52,67,46,23,35),(6,4,34,53,69,47,23,36),(6,4,35,54,71,48,23,36),(6,4,36,56,73,49,24,37),(6,4,37,57,74,50,24,38),(6,4,38,58,76,51,24,38),(6,4,39,59,78,52,24,39),(6,4,40,60,80,53,25,39),(6,4,41,61,82,54,25,40),(6,4,42,62,84,55,25,40),(6,4,43,63,86,56,25,41),(6,4,44,64,89,57,26,41),(6,4,45,66,91,58,26,42),(6,4,46,67,93,59,26,43),(6,4,47,68,95,60,27,43),(6,4,48,69,97,61,27,44),(6,4,49,70,99,63,27,45),(6,4,50,72,102,64,28,45),(6,4,51,73,109,65,28,46),(6,4,52,74,109,66,28,46),(6,4,53,75,114,67,29,47),(6,4,54,77,116,68,29,48),(6,4,55,78,118,70,29,48),(6,4,56,79,122,71,30,49),(6,4,57,81,124,72,30,50),(6,4,58,82,127,73,30,51),(6,4,59,84,129,75,31,51),(6,4,60,85,132,76,31,52),(6,4,61,86,135,77,31,53),(6,4,62,88,137,79,32,53),(6,4,63,89,137,80,32,54),(6,4,64,91,144,81,32,55),(6,4,65,92,146,83,33,56),(6,4,66,94,149,84,33,57),(6,4,67,95,152,86,34,57),(6,4,68,97,155,87,34,58),(6,4,69,99,158,88,34,59),(6,4,70,100,158,90,35,60),(6,4,71,102,165,91,35,61),(6,4,72,104,168,93,36,61),(6,4,73,105,171,95,36,62),(6,4,74,107,174,96,37,63),(6,4,75,109,177,98,37,64),(6,4,76,110,180,99,37,65),(6,4,77,112,184,101,38,66),(6,4,78,114,188,103,38,67),(6,4,79,116,191,104,39,68),(6,4,80,118,194,106,39,69),(6,4,81,119,198,108,40,70),(6,4,82,121,201,109,40,70),(6,4,83,123,205,111,41,71),(6,4,84,125,209,113,41,72),(6,4,85,127,212,115,42,73),(6,8,10,26,18,24,30,35),(6,8,11,26,18,24,31,36),(6,8,12,26,18,24,33,37),(6,8,13,26,18,25,34,38),(6,8,14,27,18,25,35,40),(6,8,15,27,19,25,37,41),(6,8,16,27,19,26,38,42),(6,8,17,27,19,26,39,44),(6,8,18,27,19,26,41,45),(6,8,19,27,19,27,42,46),(6,8,20,27,20,27,44,48),(6,8,21,28,20,27,45,49),(6,8,22,28,20,28,47,51),(6,8,23,28,20,28,48,52),(6,8,24,28,21,29,50,54),(6,8,25,28,21,29,51,55),(6,8,26,28,21,29,53,57),(6,8,27,28,21,30,55,58),(6,8,28,29,21,30,56,60),(6,8,29,29,22,31,58,61),(6,8,30,29,22,31,60,63),(6,8,31,29,22,31,61,65),(6,8,32,29,22,32,63,66),(6,8,33,29,23,32,65,68),(6,8,34,30,23,33,66,70),(6,8,35,30,23,33,68,71),(6,8,36,30,24,34,70,73),(6,8,37,30,24,34,72,75),(6,8,38,30,24,34,74,77),(6,8,39,31,24,35,76,78),(6,8,40,31,25,35,77,80),(6,8,41,31,25,36,79,82),(6,8,42,31,25,36,81,84),(6,8,43,31,25,37,83,86),(6,8,44,31,26,37,85,88),(6,8,45,32,26,38,87,90),(6,8,46,32,26,38,89,92),(6,8,47,32,27,39,91,94),(6,8,48,32,27,39,94,96),(6,8,49,33,27,40,96,98),(6,8,50,33,28,40,103,100),(6,8,51,33,28,41,105,102),(6,8,52,33,28,41,107,104),(6,8,53,33,29,42,109,106),(6,8,54,34,29,43,112,108),(6,8,55,34,29,43,114,111),(6,8,56,34,30,44,116,113),(6,8,57,34,30,44,119,115),(6,8,58,35,30,45,122,117),(6,8,59,35,31,45,125,120),(6,8,60,35,31,46,127,122),(6,8,61,35,31,47,129,124),(6,8,62,35,32,47,132,127),(6,8,63,36,32,48,135,129),(6,8,64,36,32,48,137,132),(6,8,65,36,33,49,140,134),(6,8,66,37,33,50,143,137),(6,8,67,37,34,50,146,139),(6,8,68,37,34,51,149,142),(6,8,69,37,34,52,152,145),(6,8,70,38,35,52,154,147),(6,8,71,38,35,53,157,150),(6,8,72,38,36,54,160,153),(6,8,73,38,36,55,164,156),(6,8,74,39,37,55,167,158),(6,8,75,39,37,56,170,161),(6,8,76,39,37,57,173,164),(6,8,77,40,38,58,176,167),(6,8,78,40,38,58,179,170),(6,8,79,40,39,59,182,173),(6,8,80,41,39,60,186,176),(6,8,81,41,40,61,190,179),(6,8,82,41,40,61,193,182),(6,8,83,41,41,62,196,186),(6,8,84,42,41,63,200,189),(6,8,85,42,42,64,203,192),(6,9,10,28,19,27,27,34),(6,9,11,28,20,27,29,35),(6,9,12,28,20,28,30,36),(6,9,13,29,20,28,31,38),(6,9,14,29,21,29,32,39),(6,9,15,29,21,30,33,40),(6,9,16,30,22,30,34,41),(6,9,17,30,22,31,36,43),(6,9,18,30,22,31,37,44),(6,9,19,31,23,32,38,45),(6,9,20,31,23,33,39,47),(6,9,21,31,24,33,41,48),(6,9,22,32,24,34,42,49),(6,9,23,32,25,35,43,51),(6,9,24,33,25,35,45,52),(6,9,25,33,26,36,46,54),(6,9,26,33,26,37,47,55),(6,9,27,34,26,37,49,56),(6,9,28,34,27,38,50,58),(6,9,29,35,27,39,52,59),(6,9,30,35,28,39,53,61),(6,9,31,35,28,40,54,63),(6,9,32,36,29,41,56,64),(6,9,33,36,29,42,57,66),(6,9,34,37,30,42,59,67),(6,9,35,37,30,43,60,69),(6,9,36,38,31,44,62,71),(6,9,37,38,32,45,64,72),(6,9,38,38,32,46,65,74),(6,9,39,39,33,46,67,76),(6,9,40,39,33,47,68,77),(6,9,41,40,34,48,70,79),(6,9,42,40,34,49,72,81),(6,9,43,41,35,50,73,83),(6,9,44,41,35,51,75,84),(6,9,45,42,36,51,77,86),(6,9,46,42,37,52,79,88),(6,9,47,43,37,53,80,90),(6,9,48,43,38,54,82,92),(6,9,49,44,39,55,84,94),(6,9,50,44,39,56,90,96),(6,9,51,45,40,57,92,98),(6,9,52,45,40,58,94,100),(6,9,53,46,41,59,96,102),(6,9,54,47,42,60,98,104),(6,9,55,47,42,61,101,106),(6,9,56,48,43,62,103,108),(6,9,57,48,44,63,105,110),(6,9,58,49,45,64,107,113),(6,9,59,49,45,65,109,115),(6,9,60,50,46,66,111,117),(6,9,61,51,47,67,113,119),(6,9,62,51,47,68,115,122),(6,9,63,52,48,69,118,124),(6,9,64,52,49,70,120,126),(6,9,65,53,50,72,123,129),(6,9,66,54,51,73,125,131),(6,9,67,54,51,74,128,134),(6,9,68,55,52,75,130,136),(6,9,69,56,53,76,132,138),(6,9,70,56,54,77,135,141),(6,9,71,57,55,79,137,144),(6,9,72,58,55,80,140,146),(6,9,73,59,56,81,143,149),(6,9,74,59,57,82,146,152),(6,9,75,60,58,84,148,154),(6,9,76,61,59,85,151,157),(6,9,77,62,60,86,154,160),(6,9,78,62,61,88,156,163),(6,9,79,63,62,89,159,166),(6,9,80,64,63,90,162,168),(6,9,81,65,64,92,165,171),(6,9,82,65,64,93,168,174),(6,9,83,66,65,95,171,177),(6,9,84,67,66,96,174,180),(6,9,85,68,67,97,177,183),(7,2,10,26,27,30,28,27),(7,2,11,27,27,31,29,28),(7,2,12,28,28,32,30,28),(7,2,13,29,29,33,30,29),(7,2,14,30,29,34,31,30),(7,2,15,31,30,36,32,30),(7,2,16,33,30,37,32,31),(7,2,17,34,31,38,33,32),(7,2,18,35,32,39,34,33),(7,2,19,36,32,40,34,33),(7,2,20,37,33,41,35,34),(7,2,21,38,34,42,36,35),(7,2,22,40,34,43,37,36),(7,2,23,41,35,44,37,37),(7,2,24,42,36,46,38,37),(7,2,25,43,36,47,39,38),(7,2,26,45,37,48,40,39),(7,2,27,46,38,49,40,40),(7,2,28,47,38,50,41,41),(7,2,29,49,39,52,42,42),(7,2,30,50,40,53,43,42),(7,2,31,51,41,54,44,43),(7,2,32,53,41,56,45,44),(7,2,33,54,42,57,45,45),(7,2,34,56,43,58,46,46),(7,2,35,57,44,60,47,47),(7,2,36,59,45,61,48,48),(7,2,37,60,45,62,49,49),(7,2,38,62,46,64,50,50),(7,2,39,63,47,65,51,51),(7,2,40,65,48,67,52,52),(7,2,41,66,49,68,53,53),(7,2,42,68,49,70,54,54),(7,2,43,69,50,71,55,55),(7,2,44,71,51,73,55,56),(7,2,45,73,52,74,56,57),(7,2,46,74,53,76,57,58),(7,2,47,76,54,77,59,59),(7,2,48,78,55,79,60,60),(7,2,49,79,56,81,61,62),(7,2,50,81,57,82,62,63),(7,2,51,83,58,84,63,64),(7,2,52,85,59,86,64,65),(7,2,53,87,60,87,65,66),(7,2,54,88,61,89,66,67),(7,2,55,90,62,91,67,69),(7,2,56,92,63,93,68,70),(7,2,57,94,64,94,69,71),(7,2,58,96,65,96,71,72),(7,2,59,103,66,98,72,74),(7,2,60,103,67,100,73,75),(7,2,61,103,68,102,74,76),(7,2,62,109,69,104,75,78),(7,2,63,111,70,106,77,79),(7,2,64,111,71,108,78,80),(7,2,65,111,73,110,79,82),(7,2,66,113,74,112,81,83),(7,2,67,115,75,114,82,85),(7,2,68,117,76,116,83,86),(7,2,69,125,77,118,85,87),(7,2,70,125,79,120,86,89),(7,2,71,125,80,122,87,90),(7,2,72,132,81,125,89,92),(7,2,73,134,82,127,90,94),(7,2,74,134,84,129,92,95),(7,2,75,139,85,131,93,97),(7,2,76,139,86,134,95,98),(7,2,77,145,88,136,96,100),(7,2,78,148,89,138,98,102),(7,2,79,150,90,141,99,103),(7,2,80,150,92,143,101,105),(7,2,81,155,93,146,102,107),(7,2,82,158,95,148,104,108),(7,2,83,161,96,151,105,110),(7,2,84,165,98,153,107,112),(7,2,85,167,99,156,109,114),(7,3,10,19,36,28,28,26),(7,3,11,19,37,29,28,27),(7,3,12,20,39,30,29,28),(7,3,13,20,40,31,30,28),(7,3,14,21,41,32,30,29),(7,3,15,21,43,33,31,29),(7,3,16,22,44,34,31,30),(7,3,17,22,45,35,32,31),(7,3,18,23,47,36,33,32),(7,3,19,23,48,37,33,32),(7,3,20,24,50,38,34,33),(7,3,21,24,51,39,35,34),(7,3,22,25,53,40,35,34),(7,3,23,25,54,41,36,35),(7,3,24,26,56,42,37,36),(7,3,25,26,57,43,37,37),(7,3,26,27,59,44,38,37),(7,3,27,27,61,45,39,38),(7,3,28,28,62,46,39,39),(7,3,29,28,64,47,40,40),(7,3,30,29,66,48,41,40),(7,3,31,29,67,50,42,41),(7,3,32,30,69,51,42,42),(7,3,33,31,71,52,43,43),(7,3,34,31,72,53,44,44),(7,3,35,32,74,54,45,45),(7,3,36,33,76,56,46,46),(7,3,37,33,78,57,46,46),(7,3,38,34,80,58,47,47),(7,3,39,34,82,59,48,48),(7,3,40,35,83,61,49,49),(7,3,41,36,85,62,50,50),(7,3,42,36,87,63,50,51),(7,3,43,37,89,64,51,52),(7,3,44,38,91,66,52,53),(7,3,45,38,93,67,53,54),(7,3,46,39,95,69,54,55),(7,3,47,40,97,70,55,56),(7,3,48,41,100,71,56,57),(7,3,49,41,102,73,57,58),(7,3,50,42,104,74,58,59),(7,3,51,43,106,76,59,60),(7,3,52,44,108,77,60,61),(7,3,53,44,110,79,61,62),(7,3,54,45,113,80,62,63),(7,3,55,46,115,82,63,64),(7,3,56,47,117,83,64,65),(7,3,57,48,120,85,65,67),(7,3,58,48,122,87,66,68),(7,3,59,49,125,88,67,69),(7,3,60,50,127,90,68,70),(7,3,61,51,129,92,69,71),(7,3,62,52,132,93,70,72),(7,3,63,53,135,95,71,74),(7,3,64,53,137,97,72,75),(7,3,65,54,140,99,74,76),(7,3,66,55,142,100,75,77),(7,3,67,56,145,102,76,79),(7,3,68,57,148,104,77,80),(7,3,69,58,151,106,78,81),(7,3,70,59,153,108,80,83),(7,3,71,60,156,110,81,84),(7,3,72,61,159,112,82,85),(7,3,73,62,162,114,83,87),(7,3,74,63,165,116,85,88),(7,3,75,64,168,118,86,90),(7,3,76,65,179,120,87,91),(7,3,77,66,182,122,89,93),(7,3,78,67,185,124,90,94),(7,3,79,68,188,126,91,96),(7,3,80,69,192,128,93,97),(7,3,81,70,196,130,94,99),(7,3,82,72,199,133,96,100),(7,3,83,73,202,135,97,102),(7,3,84,74,206,137,99,103),(7,3,85,75,210,139,100,105),(7,7,10,23,26,29,31,30),(7,7,11,24,26,30,32,31),(7,7,12,25,27,31,33,32),(7,7,13,25,27,32,34,33),(7,7,14,26,28,33,35,34),(7,7,15,27,28,34,36,36),(7,7,16,28,29,35,37,37),(7,7,17,29,29,36,38,38),(7,7,18,30,30,37,39,39),(7,7,19,31,30,38,40,40),(7,7,20,32,31,39,41,41),(7,7,21,33,31,40,42,42),(7,7,22,33,32,41,43,43),(7,7,23,34,32,42,44,44),(7,7,24,35,33,43,45,46),(7,7,25,36,33,45,46,47),(7,7,26,37,34,46,47,48),(7,7,27,38,34,47,48,49),(7,7,28,39,35,48,49,50),(7,7,29,40,35,49,50,52),(7,7,30,41,36,50,51,53),(7,7,31,43,36,52,53,54),(7,7,32,44,37,53,54,56),(7,7,33,45,38,54,55,57),(7,7,34,46,38,55,56,58),(7,7,35,47,39,57,57,60),(7,7,36,48,40,58,59,61),(7,7,37,49,40,59,60,62),(7,7,38,50,41,61,61,64),(7,7,39,51,41,62,62,65),(7,7,40,53,42,63,64,67),(7,7,41,54,43,65,65,68),(7,7,42,55,43,66,66,70),(7,7,43,56,44,68,67,71),(7,7,44,58,45,69,69,73),(7,7,45,59,45,71,70,74),(7,7,46,60,46,72,72,76),(7,7,47,61,47,74,73,77),(7,7,48,63,48,75,74,79),(7,7,49,64,48,77,76,81),(7,7,50,65,49,78,77,82),(7,7,51,67,50,80,79,84),(7,7,52,68,51,81,80,86),(7,7,53,70,51,83,82,87),(7,7,54,71,52,85,83,89),(7,7,55,72,53,86,85,91),(7,7,56,74,54,88,86,93),(7,7,57,75,55,90,88,94),(7,7,58,77,55,91,90,96),(7,7,59,78,56,93,91,98),(7,7,60,80,57,95,93,100),(7,7,61,82,58,97,95,102),(7,7,62,83,59,99,96,104),(7,7,63,85,60,100,98,106),(7,7,64,86,60,102,100,108),(7,7,65,88,61,104,102,110),(7,7,66,90,62,106,103,112),(7,7,67,91,63,108,105,114),(7,7,68,93,64,110,107,116),(7,7,69,95,65,112,109,118),(7,7,70,97,66,114,111,120),(7,7,71,98,67,116,113,122),(7,7,72,100,68,118,115,125),(7,7,73,102,69,120,117,127),(7,7,74,104,70,123,119,129),(7,7,75,106,71,125,121,131),(7,7,76,108,72,127,123,134),(7,7,77,109,73,129,125,136),(7,7,78,111,74,131,127,138),(7,7,79,113,75,134,129,141),(7,7,80,115,76,136,131,143),(7,7,81,117,77,138,139,146),(7,7,82,119,79,141,142,148),(7,7,83,122,80,143,142,151),(7,7,84,124,81,145,142,153),(7,7,85,126,82,148,148,156),(7,11,10,21,26,25,33,31),(7,11,11,21,27,26,34,33),(7,11,12,22,27,27,35,34),(7,11,13,22,28,27,36,35),(7,11,14,23,28,28,37,36),(7,11,15,24,29,29,39,37),(7,11,16,24,29,29,40,38),(7,11,17,25,30,30,41,40),(7,11,18,25,31,31,42,41),(7,11,19,26,31,31,43,42),(7,11,20,27,32,32,44,43),(7,11,21,27,32,33,45,45),(7,11,22,28,33,34,46,46),(7,11,23,29,34,34,47,47),(7,11,24,29,34,35,49,49),(7,11,25,30,35,36,50,50),(7,11,26,31,35,37,51,51),(7,11,27,31,36,37,52,53),(7,11,28,32,37,38,53,54),(7,11,29,33,37,39,55,56),(7,11,30,33,38,40,56,57),(7,11,31,34,39,41,57,58),(7,11,32,35,39,42,59,60),(7,11,33,36,40,42,60,61),(7,11,34,36,41,43,61,63),(7,11,35,37,41,44,63,64),(7,11,36,38,42,45,64,66),(7,11,37,39,43,46,65,68),(7,11,38,40,43,47,67,69),(7,11,39,40,44,48,68,71),(7,11,40,41,45,49,70,72),(7,11,41,42,46,50,71,74),(7,11,42,43,46,51,73,76),(7,11,43,44,47,52,74,77),(7,11,44,45,48,52,76,79),(7,11,45,45,49,53,77,81),(7,11,46,46,50,54,79,83),(7,11,47,47,50,56,80,84),(7,11,48,48,51,57,82,86),(7,11,49,49,52,58,84,88),(7,11,50,50,53,59,85,90),(7,11,51,51,54,60,87,92),(7,11,52,52,55,61,89,94),(7,11,53,53,56,62,90,96),(7,11,54,54,56,63,92,98),(7,11,55,55,57,64,94,100),(7,11,56,56,58,65,96,102),(7,11,57,57,59,66,97,104),(7,11,58,58,60,68,99,106),(7,11,59,59,61,69,101,108),(7,11,60,60,62,70,103,110),(7,11,61,61,63,71,105,112),(7,11,62,62,64,72,107,114),(7,11,63,63,65,74,109,117),(7,11,64,64,66,75,111,119),(7,11,65,66,67,76,113,121),(7,11,66,67,68,78,115,123),(7,11,67,68,69,79,117,126),(7,11,68,69,70,80,119,128),(7,11,69,70,71,82,121,130),(7,11,70,71,72,83,123,133),(7,11,71,73,73,84,125,135),(7,11,72,74,75,86,128,138),(7,11,73,75,76,87,130,140),(7,11,74,76,77,89,132,143),(7,11,75,78,78,90,134,145),(7,11,76,79,79,92,137,148),(7,11,77,80,80,93,139,151),(7,11,78,82,82,95,141,153),(7,11,79,83,83,96,144,156),(7,11,80,84,84,98,146,159),(7,11,81,86,85,99,149,161),(7,11,82,87,87,101,151,164),(7,11,83,89,88,102,154,167),(7,11,84,90,89,104,163,170),(7,11,85,91,91,106,163,173),(8,2,10,32,27,30,21,28),(8,2,11,33,27,31,22,29),(8,2,12,34,28,32,23,29),(8,2,13,35,29,33,23,30),(8,2,14,36,29,34,24,31),(8,2,15,37,30,36,25,31),(8,2,16,39,30,37,25,32),(8,2,17,40,31,38,26,33),(8,2,18,41,32,39,27,34),(8,2,19,42,32,40,27,34),(8,2,20,43,33,41,28,35),(8,2,21,44,34,42,29,36),(8,2,22,46,34,43,30,37),(8,2,23,47,35,44,30,38),(8,2,24,48,36,46,31,38),(8,2,25,49,36,47,32,39),(8,2,26,51,37,48,33,40),(8,2,27,52,38,49,33,41),(8,2,28,53,38,50,34,42),(8,2,29,55,39,52,35,43),(8,2,30,56,40,53,36,43),(8,2,31,57,41,54,37,44),(8,2,32,59,41,56,38,45),(8,2,33,60,42,57,38,46),(8,2,34,62,43,58,39,47),(8,2,35,63,44,60,40,48),(8,2,36,65,45,61,41,49),(8,2,37,66,45,62,42,50),(8,2,38,68,46,64,43,51),(8,2,39,69,47,65,44,52),(8,2,40,71,48,67,45,53),(8,2,41,72,49,68,46,54),(8,2,42,74,49,70,47,55),(8,2,43,75,50,71,48,56),(8,2,44,77,51,73,48,57),(8,2,45,79,52,74,49,58),(8,2,46,80,53,76,50,59),(8,2,47,82,54,77,52,60),(8,2,48,84,55,79,53,61),(8,2,49,85,56,81,54,63),(8,2,50,87,57,82,55,64),(8,2,51,89,58,84,56,65),(8,2,52,91,59,86,57,66),(8,2,53,93,60,87,58,67),(8,2,54,94,61,89,59,68),(8,2,55,96,62,91,60,70),(8,2,56,98,63,93,61,71),(8,2,57,100,64,94,62,72),(8,2,58,102,65,96,64,73),(8,2,59,109,66,98,65,75),(8,2,60,109,67,100,66,76),(8,2,61,109,68,102,67,77),(8,2,62,115,69,104,68,79),(8,2,63,117,70,106,70,80),(8,2,64,117,71,108,71,81),(8,2,65,117,73,110,72,83),(8,2,66,119,74,112,74,84),(8,2,67,121,75,114,75,86),(8,2,68,123,76,116,76,87),(8,2,69,131,77,118,78,88),(8,2,70,131,79,120,79,90),(8,2,71,131,80,122,80,91),(8,2,72,138,81,125,82,93),(8,2,73,140,82,127,83,95),(8,2,74,140,84,129,85,96),(8,2,75,145,85,131,86,98),(8,2,76,145,86,134,88,99),(8,2,77,151,88,136,89,101),(8,2,78,154,89,138,91,103),(8,2,79,156,90,141,92,104),(8,2,80,156,92,143,94,106),(8,2,81,161,93,146,95,108),(8,2,82,164,95,148,97,109),(8,2,83,167,96,151,98,111),(8,2,84,171,98,153,100,113),(8,2,85,173,99,156,102,115),(10,7,10,25,26,29,31,28),(10,7,11,26,26,30,32,29),(10,7,12,27,27,31,33,30),(10,7,13,27,27,32,34,31),(10,7,14,28,28,33,35,32),(10,7,15,29,28,34,36,34),(10,7,16,30,29,35,37,35),(10,7,17,31,29,36,38,36),(10,7,18,32,30,37,39,37),(10,7,19,33,30,38,40,38),(10,7,20,34,31,39,41,39),(10,7,21,35,31,40,42,40),(10,7,22,35,32,41,43,41),(10,7,23,36,32,42,44,42),(10,7,24,37,33,43,45,44),(10,7,25,38,33,45,46,45),(10,7,26,39,34,46,47,46),(10,7,27,40,34,47,48,47),(10,7,28,41,35,48,49,48),(10,7,29,42,35,49,50,50),(10,7,30,43,36,50,51,51),(10,7,31,45,36,52,53,52),(10,7,32,46,37,53,54,54),(10,7,33,47,38,54,55,55),(10,7,34,48,38,55,56,56),(10,7,35,49,39,57,57,58),(10,7,36,50,40,58,59,59),(10,7,37,51,40,59,60,60),(10,7,38,52,41,61,61,62),(10,7,39,53,41,62,62,63),(10,7,40,55,42,63,64,65),(10,7,41,56,43,65,65,66),(10,7,42,57,43,66,66,68),(10,7,43,58,44,68,67,69),(10,7,44,60,45,69,69,71),(10,7,45,61,45,71,70,72),(10,7,46,62,46,72,72,74),(10,7,47,63,47,74,73,75),(10,7,48,65,48,75,74,77),(10,7,49,66,48,77,76,79),(10,7,50,67,49,78,77,80),(10,7,51,69,50,80,79,82),(10,7,52,70,51,81,80,84),(10,7,53,72,51,83,82,85),(10,7,54,73,52,85,83,87),(10,7,55,74,53,86,85,89),(10,7,56,76,54,88,86,91),(10,7,57,77,55,90,88,92),(10,7,58,79,55,91,90,94),(10,7,59,80,56,93,91,96),(10,7,60,82,57,95,93,98),(10,7,61,84,58,97,95,100),(10,7,62,85,59,99,96,102),(10,7,63,87,60,100,98,104),(10,7,64,88,60,102,100,106),(10,7,65,90,61,104,102,108),(10,7,66,92,62,106,103,110),(10,7,67,93,63,108,105,112),(10,7,68,95,64,110,107,114),(10,7,69,97,65,112,109,116),(10,7,70,99,66,114,111,118),(10,7,71,100,67,116,113,120),(10,7,72,102,68,118,115,123),(10,7,73,104,69,120,117,125),(10,7,74,106,70,123,119,127),(10,7,75,108,71,125,121,129),(10,7,76,110,72,127,123,132),(10,7,77,111,73,129,125,134),(10,7,78,113,74,131,127,136),(10,7,79,115,75,134,129,139),(10,7,80,117,76,136,131,141),(10,7,81,119,77,138,139,144),(10,7,82,121,79,141,142,146),(10,7,83,124,80,143,142,149),(10,7,84,126,81,145,142,151),(10,7,85,128,82,148,148,154),(10,11,10,23,26,25,33,29),(10,11,11,23,27,26,34,31),(10,11,12,24,27,27,35,32),(10,11,13,24,28,27,36,33),(10,11,14,25,28,28,37,34),(10,11,15,26,29,29,39,35),(10,11,16,26,29,29,40,36),(10,11,17,27,30,30,41,38),(10,11,18,27,31,31,42,39),(10,11,19,28,31,31,43,40),(10,11,20,29,32,32,44,41),(10,11,21,29,32,33,45,43),(10,11,22,30,33,34,46,44),(10,11,23,31,34,34,47,45),(10,11,24,31,34,35,49,47),(10,11,25,32,35,36,50,48),(10,11,26,33,35,37,51,49),(10,11,27,33,36,37,52,51),(10,11,28,34,37,38,53,52),(10,11,29,35,37,39,55,54),(10,11,30,35,38,40,56,55),(10,11,31,36,39,41,57,56),(10,11,32,37,39,42,59,58),(10,11,33,38,40,42,60,59),(10,11,34,38,41,43,61,61),(10,11,35,39,41,44,63,62),(10,11,36,40,42,45,64,64),(10,11,37,41,43,46,65,66),(10,11,38,42,43,47,67,67),(10,11,39,42,44,48,68,69),(10,11,40,43,45,49,70,70),(10,11,41,44,46,50,71,72),(10,11,42,45,46,51,73,74),(10,11,43,46,47,52,74,75),(10,11,44,47,48,52,76,77),(10,11,45,47,49,53,77,79),(10,11,46,48,50,54,79,81),(10,11,47,49,50,56,80,82),(10,11,48,50,51,57,82,84),(10,11,49,51,52,58,84,86),(10,11,50,52,53,59,85,88),(10,11,51,53,54,60,87,90),(10,11,52,54,55,61,89,92),(10,11,53,55,56,62,90,94),(10,11,54,56,56,63,92,96),(10,11,55,57,57,64,94,98),(10,11,56,58,58,65,96,100),(10,11,57,59,59,66,97,102),(10,11,58,60,60,68,99,104),(10,11,59,61,61,69,101,106),(10,11,60,62,62,70,103,108),(10,11,61,63,63,71,105,110),(10,11,62,64,64,72,107,112),(10,11,63,65,65,74,109,115),(10,11,64,66,66,75,111,117),(10,11,65,68,67,76,113,119),(10,11,66,69,68,78,115,121),(10,11,67,70,69,79,117,124),(10,11,68,71,70,80,119,126),(10,11,69,72,71,82,121,128),(10,11,70,73,72,83,123,131),(10,11,71,75,73,84,125,133),(10,11,72,76,75,86,128,136),(10,11,73,77,76,87,130,138),(10,11,74,78,77,89,132,141),(10,11,75,80,78,90,134,143),(10,11,76,81,79,92,137,146),(10,11,77,82,80,93,139,149),(10,11,78,84,82,95,141,151),(10,11,79,85,83,96,144,154),(10,11,80,86,84,98,146,157),(10,11,81,88,85,99,149,159),(10,11,82,89,87,101,151,162),(10,11,83,91,88,102,154,165),(10,11,84,92,89,104,163,168),(10,11,85,93,91,106,163,171),(11,4,10,28,32,27,22,25),(11,4,11,29,33,28,22,26),(11,4,12,30,34,28,22,26),(11,4,13,31,36,29,22,26),(11,4,14,31,37,30,22,27),(11,4,15,32,39,30,23,27),(11,4,16,33,40,31,23,28),(11,4,17,34,41,32,23,28),(11,4,18,35,43,33,23,28),(11,4,19,36,45,33,23,29),(11,4,20,36,46,34,24,29),(11,4,21,37,48,35,24,30),(11,4,22,38,49,36,24,30),(11,4,23,39,51,37,24,31),(11,4,24,40,52,37,25,31),(11,4,25,41,54,38,25,32),(11,4,26,42,56,39,25,32),(11,4,27,43,57,40,25,32),(11,4,28,44,59,41,25,33),(11,4,29,44,61,42,26,33),(11,4,30,45,63,42,26,34),(11,4,31,46,64,43,26,34),(11,4,32,47,66,44,26,35),(11,4,33,48,68,45,27,35),(11,4,34,49,70,46,27,36),(11,4,35,50,72,47,27,36),(11,4,36,52,74,48,28,37),(11,4,37,53,75,49,28,38),(11,4,38,54,77,50,28,38),(11,4,39,55,79,51,28,39),(11,4,40,56,81,52,29,39),(11,4,41,57,83,53,29,40),(11,4,42,58,85,54,29,40),(11,4,43,59,87,55,29,41),(11,4,44,60,90,56,30,41),(11,4,45,62,92,57,30,42),(11,4,46,63,94,58,30,43),(11,4,47,64,96,59,31,43),(11,4,48,65,98,60,31,44),(11,4,49,66,100,62,31,45),(11,4,50,68,103,63,32,45),(11,4,51,69,110,64,32,46),(11,4,52,70,110,65,32,46),(11,4,53,71,115,66,33,47),(11,4,54,73,117,67,33,48),(11,4,55,74,119,69,33,48),(11,4,56,75,123,70,34,49),(11,4,57,77,125,71,34,50),(11,4,58,78,128,72,34,51),(11,4,59,80,130,74,35,51),(11,4,60,81,133,75,35,52),(11,4,61,82,136,76,35,53),(11,4,62,84,138,78,36,53),(11,4,63,85,138,79,36,54),(11,4,64,87,145,80,36,55),(11,4,65,88,147,82,37,56),(11,4,66,90,150,83,37,57),(11,4,67,91,153,85,38,57),(11,4,68,93,156,86,38,58),(11,4,69,95,159,87,38,59),(11,4,70,96,159,89,39,60),(11,4,71,98,166,90,39,61),(11,4,72,100,169,92,40,61),(11,4,73,101,172,94,40,62),(11,4,74,103,175,95,41,63),(11,4,75,105,178,97,41,64),(11,4,76,106,181,98,41,65),(11,4,77,108,185,100,42,66),(11,4,78,110,189,102,42,67),(11,4,79,112,192,103,43,68),(11,4,80,114,195,105,43,69),(11,4,81,115,199,107,44,70),(11,4,82,117,202,108,44,70),(11,4,83,119,206,110,45,71),(11,4,84,121,210,112,45,72),(11,4,85,123,213,114,46,73),(11,9,10,24,20,26,31,34),(11,9,11,24,21,26,33,35),(11,9,12,24,21,27,34,36),(11,9,13,25,21,27,35,38),(11,9,14,25,22,28,36,39),(11,9,15,25,22,29,37,40),(11,9,16,26,23,29,38,41),(11,9,17,26,23,30,40,43),(11,9,18,26,23,30,41,44),(11,9,19,27,24,31,42,45),(11,9,20,27,24,32,43,47),(11,9,21,27,25,32,45,48),(11,9,22,28,25,33,46,49),(11,9,23,28,26,34,47,51),(11,9,24,29,26,34,49,52),(11,9,25,29,27,35,50,54),(11,9,26,29,27,36,51,55),(11,9,27,30,27,36,53,56),(11,9,28,30,28,37,54,58),(11,9,29,31,28,38,56,59),(11,9,30,31,29,38,57,61),(11,9,31,31,29,39,58,63),(11,9,32,32,30,40,60,64),(11,9,33,32,30,41,61,66),(11,9,34,33,31,41,63,67),(11,9,35,33,31,42,64,69),(11,9,36,34,32,43,66,71),(11,9,37,34,33,44,68,72),(11,9,38,34,33,45,69,74),(11,9,39,35,34,45,71,76),(11,9,40,35,34,46,72,77),(11,9,41,36,35,47,74,79),(11,9,42,36,35,48,76,81),(11,9,43,37,36,49,77,83),(11,9,44,37,36,50,79,84),(11,9,45,38,37,50,81,86),(11,9,46,38,38,51,83,88),(11,9,47,39,38,52,84,90),(11,9,48,39,39,53,86,92),(11,9,49,40,40,54,88,94),(11,9,50,40,40,55,94,96),(11,9,51,41,41,56,96,98),(11,9,52,41,41,57,98,100),(11,9,53,42,42,58,100,102),(11,9,54,43,43,59,102,104),(11,9,55,43,43,60,105,106),(11,9,56,44,44,61,107,108),(11,9,57,44,45,62,109,110),(11,9,58,45,46,63,111,113),(11,9,59,45,46,64,113,115),(11,9,60,46,47,65,115,117),(11,9,61,47,48,66,117,119),(11,9,62,47,48,67,119,122),(11,9,63,48,49,68,122,124),(11,9,64,48,50,69,124,126),(11,9,65,49,51,71,127,129),(11,9,66,50,52,72,129,131),(11,9,67,50,52,73,132,134),(11,9,68,51,53,74,134,136),(11,9,69,52,54,75,136,138),(11,9,70,52,55,76,139,141),(11,9,71,53,56,78,141,144),(11,9,72,54,56,79,144,146),(11,9,73,55,57,80,147,149),(11,9,74,55,58,81,150,152),(11,9,75,56,59,83,152,154),(11,9,76,57,60,84,155,157),(11,9,77,58,61,85,158,160),(11,9,78,58,62,87,160,163),(11,9,79,59,63,88,163,166),(11,9,80,60,64,89,166,168),(11,9,81,61,65,91,169,171),(11,9,82,61,65,92,172,174),(11,9,83,62,66,94,175,177),(11,9,84,63,67,95,178,180),(11,9,85,64,68,96,181,183),(11,11,10,27,21,25,30,33),(11,11,11,27,22,26,31,35),(11,11,12,28,22,27,32,36),(11,11,13,28,23,27,33,37),(11,11,14,29,23,28,34,38),(11,11,15,30,24,29,36,39),(11,11,16,30,24,29,37,40),(11,11,17,31,25,30,38,42),(11,11,18,31,26,31,39,43),(11,11,19,32,26,31,40,44),(11,11,20,33,27,32,41,45),(11,11,21,33,27,33,42,47),(11,11,22,34,28,34,43,48),(11,11,23,35,29,34,44,49),(11,11,24,35,29,35,46,51),(11,11,25,36,30,36,47,52),(11,11,26,37,30,37,48,53),(11,11,27,37,31,37,49,55),(11,11,28,38,32,38,50,56),(11,11,29,39,32,39,52,58),(11,11,30,39,33,40,53,59),(11,11,31,40,34,41,54,60),(11,11,32,41,34,42,56,62),(11,11,33,42,35,42,57,63),(11,11,34,42,36,43,58,65),(11,11,35,43,36,44,60,66),(11,11,36,44,37,45,61,68),(11,11,37,45,38,46,62,70),(11,11,38,46,38,47,64,71),(11,11,39,46,39,48,65,73),(11,11,40,47,40,49,67,74),(11,11,41,48,41,50,68,76),(11,11,42,49,41,51,70,78),(11,11,43,50,42,52,71,79),(11,11,44,51,43,52,73,81),(11,11,45,51,44,53,74,83),(11,11,46,52,45,54,76,85),(11,11,47,53,45,56,77,86),(11,11,48,54,46,57,79,88),(11,11,49,55,47,58,81,90),(11,11,50,56,48,59,82,92),(11,11,51,57,49,60,84,94),(11,11,52,58,50,61,86,96),(11,11,53,59,51,62,87,98),(11,11,54,60,51,63,89,100),(11,11,55,61,52,64,91,102),(11,11,56,62,53,65,93,104),(11,11,57,63,54,66,94,106),(11,11,58,64,55,68,96,108),(11,11,59,65,56,69,98,110),(11,11,60,66,57,70,100,112),(11,11,61,67,58,71,102,114),(11,11,62,68,59,72,104,116),(11,11,63,69,60,74,106,119),(11,11,64,70,61,75,108,121),(11,11,65,72,62,76,110,123),(11,11,66,73,63,78,112,125),(11,11,67,74,64,79,114,128),(11,11,68,75,65,80,116,130),(11,11,69,76,66,82,118,132),(11,11,70,77,67,83,120,135),(11,11,71,79,68,84,122,137),(11,11,72,80,70,86,125,140),(11,11,73,81,71,87,127,142),(11,11,74,82,72,89,129,145),(11,11,75,84,73,90,131,147),(11,11,76,85,74,92,134,150),(11,11,77,86,75,93,136,153),(11,11,78,88,77,95,138,155),(11,11,79,89,78,96,141,158),(11,11,80,90,79,98,143,161),(11,11,81,92,80,99,146,163),(11,11,82,93,82,101,148,166),(11,11,83,95,83,102,151,169),(11,11,84,96,84,104,160,172),(11,11,85,97,86,106,160,175);
/*!40000 ALTER TABLE `player_levelstats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `player_xp_for_level`
--

DROP TABLE IF EXISTS `player_xp_for_level`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `player_xp_for_level` (
  `lvl` int(3) unsigned NOT NULL,
  `xp_for_next_level` int(10) unsigned NOT NULL,
  PRIMARY KEY (`lvl`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Player xp for level';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `player_xp_for_level`
--

LOCK TABLES `player_xp_for_level` WRITE;
/*!40000 ALTER TABLE `player_xp_for_level` DISABLE KEYS */;
INSERT INTO `player_xp_for_level` VALUES (1,400),(2,900),(3,1400),(4,2100),(5,2800),(6,3600),(7,4500),(8,5400),(9,6500),(10,7600),(11,8700),(12,9800),(13,11000),(14,12300),(15,13600),(16,15000),(17,16400),(18,17800),(19,19300),(20,20800),(21,22400),(22,24000),(23,25500),(24,27200),(25,28900),(26,30500),(27,32200),(28,33900),(29,36300),(30,38800),(31,41600),(32,44600),(33,48000),(34,51400),(35,55000),(36,58700),(37,62400),(38,66200),(39,70200),(40,74300),(41,78500),(42,82800),(43,87100),(44,91600),(45,96300),(46,101000),(47,105800),(48,110700),(49,115700),(50,120900),(51,126100),(52,131500),(53,137000),(54,142500),(55,148200),(56,154000),(57,159900),(58,165800),(59,172000),(60,290000),(61,317000),(62,349000),(63,386000),(64,428000),(65,475000),(66,527000),(67,585000),(68,648000),(69,717000),(70,1219040),(71,1231680),(72,1244560),(73,1257440),(74,1270320),(75,1283360),(76,1296560),(77,1309920),(78,1323120),(79,1336640),(80,1686300),(81,2121500),(82,4004000),(83,5203400),(84,9165100);
/*!40000 ALTER TABLE `player_xp_for_level` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `playercreateinfo`
--

DROP TABLE IF EXISTS `playercreateinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `playercreateinfo` (
  `race` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `class` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `map` smallint(5) unsigned NOT NULL DEFAULT '0',
  `zone` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `position_x` float NOT NULL DEFAULT '0',
  `position_y` float NOT NULL DEFAULT '0',
  `position_z` float NOT NULL DEFAULT '0',
  `orientation` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`race`,`class`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `playercreateinfo`
--

LOCK TABLES `playercreateinfo` WRITE;
/*!40000 ALTER TABLE `playercreateinfo` DISABLE KEYS */;
INSERT INTO `playercreateinfo` VALUES (1,1,0,12,-8914.57,-133.909,80.5378,0),(1,2,0,12,-8914.57,-133.909,80.5378,0),(1,4,0,12,-8914.57,-133.909,80.5378,0),(1,5,0,12,-8914.57,-133.909,80.5378,0),(1,8,0,12,-8914.57,-133.909,80.5378,0),(1,9,0,12,-8914.57,-133.909,80.5378,0),(2,1,1,14,-618.518,-4251.67,38.718,0),(2,3,1,14,-618.518,-4251.67,38.718,0),(2,4,1,14,-618.518,-4251.67,38.718,0),(2,7,1,14,-618.518,-4251.67,38.718,0),(2,9,1,14,-618.518,-4251.67,38.718,0),(3,1,0,1,-6230.42,330.232,383.105,6.17716),(3,2,0,1,-6230.42,330.232,383.105,6.17716),(3,3,0,1,-6230.42,330.232,383.105,6.17716),(3,4,0,1,-6230.42,330.232,383.105,6.17716),(3,5,0,1,-6230.42,330.232,383.105,6.17716),(4,1,1,141,10311.3,831.463,1326.41,5.69632),(4,3,1,141,10311.3,831.463,1326.41,5.69632),(4,4,1,141,10311.3,831.463,1326.41,5.69632),(4,5,1,141,10311.3,831.463,1326.41,5.69632),(4,11,1,141,10311.3,831.463,1326.41,5.69632),(5,1,0,85,1699.85,1706.56,135.928,2.70526),(5,4,0,85,1699.85,1706.56,135.928,2.70526),(5,5,0,85,1699.85,1706.56,135.928,2.70526),(5,8,0,85,1699.85,1706.56,135.928,2.70526),(5,9,0,85,1699.85,1706.56,135.928,2.70526),(6,1,1,215,-2915.55,-257.347,59.2693,0),(6,3,1,215,-2915.55,-257.347,59.2693,0),(6,7,1,215,-2915.55,-257.347,59.2693,0),(6,11,1,215,-2915.55,-257.347,59.2693,0),(7,1,0,1,-4983.42,877.7,274.31,0),(7,9,0,1,-4983.42,877.7,274.31,0),(7,8,0,1,-4983.42,877.7,274.31,0),(7,4,0,1,-4983.42,877.7,274.31,0),(8,1,1,14,-1171.45,-5263.65,0.847728,0),(8,3,1,14,-1171.45,-5263.65,0.847728,0),(8,4,1,14,-1171.45,-5263.65,0.847728,0),(8,5,1,14,-1171.45,-5263.65,0.847728,0),(8,7,1,14,-1171.45,-5263.65,0.847728,0),(8,8,1,14,-1171.45,-5263.65,0.847728,0),(10,2,530,3431,10349.6,-6357.29,33.4026,5.31605),(10,3,530,3431,10349.6,-6357.29,33.4026,5.31605),(10,4,530,3431,10349.6,-6357.29,33.4026,5.31605),(10,5,530,3431,10349.6,-6357.29,33.4026,5.31605),(10,8,530,3431,10349.6,-6357.29,33.4026,5.31605),(10,9,530,3431,10349.6,-6357.29,33.4026,5.31605),(11,1,530,3526,-3961.64,-13931.2,100.615,2.08364),(11,2,530,3526,-3961.64,-13931.2,100.615,2.08364),(11,3,530,3526,-3961.64,-13931.2,100.615,2.08364),(11,5,530,3526,-3961.64,-13931.2,100.615,2.08364),(11,7,530,3526,-3961.64,-13931.2,100.615,2.08364),(11,8,530,3526,-3961.64,-13931.2,100.615,2.08364),(1,6,609,4298,2355.84,-5662.21,426.028,3.93485),(2,6,609,4298,2355.84,-5662.21,426.028,3.93485),(3,6,609,4298,2355.84,-5662.21,426.028,3.93485),(4,6,609,4298,2355.84,-5662.21,426.028,3.93485),(5,6,609,4298,2355.84,-5662.21,426.028,3.93485),(6,6,609,4298,2355.84,-5662.21,426.028,3.93485),(7,6,609,4298,2355.84,-5662.21,426.028,3.93485),(8,6,609,4298,2355.84,-5662.21,426.028,3.93485),(10,6,609,4298,2355.84,-5662.21,426.028,3.93485),(11,6,609,4298,2355.84,-5662.21,426.028,3.93485),(10,1,530,3431,10349.6,-6357.29,33.4026,5.31605),(3,8,0,1,-6230.42,330.232,383.105,6.17716),(3,7,0,1,-6230.42,330.232,383.105,6.17716),(3,9,0,1,-6230.42,330.232,383.105,6.17716),(7,5,0,1,-4983.42,877.7,274.31,0),(9,6,609,4298,-8423.78,-5662.21,426.028,3.93485),(9,3,648,4737,-8423.78,1363.93,104.679,1.56294),(9,8,648,4737,-8423.78,1363.93,104.679,1.56294),(9,5,648,4737,-8423.78,1363.93,104.679,1.56294),(9,4,648,4737,-8423.78,1363.93,104.679,1.56294),(9,7,648,4737,-8423.78,1363.93,104.679,1.56294),(9,1,648,4737,-8423.78,1363.93,104.679,1.56294),(9,9,648,4737,-8423.78,1363.93,104.679,1.56294),(1,3,0,12,-8914.57,-133.909,80.5378,0),(4,8,1,141,10311.3,831.463,1326.41,5.69632),(2,8,1,12,-618.518,-4251.67,38.718,0),(6,2,1,215,-2915.55,-257.347,59.2693,0),(6,5,1,215,-2915.55,-257.347,59.2693,0),(8,11,1,14,-1171.45,-5263.65,0.847728,0),(8,9,1,14,-1171.45,-5263.65,0.847728,0),(5,3,0,85,1699.85,1706.56,135.928,2.70526),(22,6,609,4298,2355.84,-5662.21,426.028,3.93485),(22,11,638,4714,-1443.62,1409.88,35.5561,3.19265),(22,3,638,4714,-1443.62,1409.88,35.5561,3.19265),(22,8,638,4714,-1443.62,1409.88,35.5561,3.19265),(22,5,638,4714,-1443.62,1409.88,35.5561,3.19265),(22,4,638,4714,-1443.62,1409.88,35.5561,3.19265),(22,1,638,4714,-1443.62,1409.88,35.5561,3.19265),(22,9,638,4714,-1443.62,1409.88,35.5561,3.19265);
/*!40000 ALTER TABLE `playercreateinfo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `playercreateinfo_action`
--

DROP TABLE IF EXISTS `playercreateinfo_action`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `playercreateinfo_action` (
  `race` tinyint(3) unsigned DEFAULT NULL,
  `class` tinyint(3) unsigned DEFAULT NULL,
  `button` tinyint(3) unsigned DEFAULT NULL,
  `action` mediumint(10) unsigned DEFAULT NULL,
  `type` tinyint(3) unsigned DEFAULT NULL,
  UNIQUE KEY `race` (`race`,`class`,`button`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Player System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `playercreateinfo_action`
--

LOCK TABLES `playercreateinfo_action` WRITE;
/*!40000 ALTER TABLE `playercreateinfo_action` DISABLE KEYS */;
INSERT INTO `playercreateinfo_action` VALUES (1,1,72,88163,0),(1,1,73,88161,0),(1,1,81,59752,0),(1,1,84,6603,0),(1,1,96,6603,0),(1,1,108,6603,0),(1,2,0,35395,0),(1,2,9,59752,0),(1,3,0,3044,0),(1,3,9,59752,0),(1,3,10,9,48),(1,3,11,982,0),(1,4,0,1752,0),(1,4,9,59752,0),(1,5,0,585,0),(1,5,9,59752,0),(1,8,0,133,0),(1,8,9,59752,0),(1,9,0,686,0),(1,9,9,59752,0),(1,9,10,10,48),(2,1,72,88163,0),(2,1,73,88161,0),(2,1,81,20572,0),(2,1,84,6603,0),(2,1,96,6603,0),(2,1,108,6603,0),(2,3,0,3044,0),(2,3,9,20572,0),(2,3,10,9,48),(2,3,11,982,0),(2,4,0,1752,0),(2,4,9,20572,0),(2,7,0,403,0),(2,7,9,20572,0),(2,7,72,403,0),(2,8,0,133,0),(2,8,9,33702,0),(2,9,0,686,0),(2,9,9,33702,0),(2,9,10,10,48),(3,1,72,88163,0),(3,1,73,88161,0),(3,1,81,20594,0),(3,1,84,6603,0),(3,1,96,6603,0),(3,1,108,6603,0),(3,2,0,35395,0),(3,2,9,20594,0),(3,3,0,3044,0),(3,3,9,20594,0),(3,3,10,9,48),(3,3,11,982,0),(3,3,75,20594,0),(3,4,0,1752,0),(3,4,9,20594,0),(3,5,0,585,0),(3,5,9,20594,0),(3,7,0,6603,0),(3,7,1,403,0),(3,7,9,20594,0),(3,7,72,403,0),(3,8,0,133,0),(3,8,9,20594,0),(3,9,0,686,0),(3,9,9,20594,0),(3,9,10,10,48),(4,1,72,88163,0),(4,1,73,88161,0),(4,1,81,58984,0),(4,1,84,6603,0),(4,1,96,6603,0),(4,1,108,6603,0),(4,3,0,3044,0),(4,3,9,58984,0),(4,3,10,9,48),(4,3,11,982,0),(4,3,81,58984,0),(4,4,0,1752,0),(4,4,9,58984,0),(4,4,81,58984,0),(4,5,0,585,0),(4,5,9,58984,0),(4,5,81,58984,0),(4,8,0,133,0),(4,8,9,58984,0),(4,11,0,5176,0),(4,11,9,58984,0),(4,11,72,6603,0),(4,11,74,58984,0),(4,11,84,6603,0),(4,11,96,6603,0),(4,11,108,6603,0),(5,1,72,88163,0),(5,1,73,88161,0),(5,1,81,20577,0),(5,1,84,6603,0),(5,1,96,6603,0),(5,1,108,6603,0),(5,3,0,3044,0),(5,3,9,20577,0),(5,3,10,9,48),(5,3,11,982,0),(5,4,0,1752,0),(5,4,9,20577,0),(5,5,0,585,0),(5,5,9,20577,0),(5,8,0,133,0),(5,8,9,20577,0),(5,9,0,686,0),(5,9,9,20577,0),(5,9,10,10,48),(6,1,72,88163,0),(6,1,73,88161,0),(6,1,81,20549,0),(6,1,84,6603,0),(6,1,96,6603,0),(6,1,108,6603,0),(6,2,0,35395,0),(6,2,9,20549,0),(6,3,0,3044,0),(6,3,9,20549,0),(6,3,10,9,48),(6,3,11,982,0),(6,3,75,20549,0),(6,5,0,585,0),(6,5,9,20549,0),(6,7,0,403,0),(6,7,9,20549,0),(6,7,75,20549,0),(6,11,0,403,0),(6,11,9,20549,0),(6,11,72,6603,0),(6,11,75,20549,0),(6,11,84,6603,0),(6,11,96,6603,0),(6,11,108,6603,0),(7,1,72,88163,0),(7,1,73,88161,0),(7,1,84,6603,0),(7,1,96,6603,0),(7,1,108,6603,0),(7,4,0,1752,0),(7,5,0,585,0),(7,8,0,133,0),(7,9,0,686,0),(7,9,10,10,48),(8,1,72,88163,0),(8,1,73,88161,0),(8,1,81,26297,0),(8,1,84,6603,0),(8,1,96,6603,0),(8,1,108,6603,0),(8,3,0,3044,0),(8,3,9,26297,0),(8,3,10,9,48),(8,3,11,982,0),(8,4,0,1752,0),(8,4,9,26297,0),(8,4,76,26297,0),(8,5,0,585,0),(8,5,9,26297,0),(8,7,0,403,0),(8,7,9,26297,0),(8,7,72,403,0),(8,8,0,133,0),(8,8,9,26297,0),(8,9,0,686,0),(8,9,9,26297,0),(8,9,10,10,48),(8,11,0,5176,0),(8,11,9,26297,0),(8,11,72,6603,0),(8,11,84,6603,0),(8,11,96,6603,0),(9,1,72,88163,0),(9,1,73,88161,0),(9,1,81,69070,0),(9,1,82,69041,0),(9,3,0,3044,0),(9,3,8,982,0),(9,3,9,69070,0),(9,3,10,69041,0),(9,3,11,9,48),(9,4,0,1752,0),(9,4,9,69070,0),(9,4,10,69041,0),(9,5,0,585,0),(9,5,9,69070,0),(9,5,10,69041,0),(9,7,0,403,0),(9,7,9,69070,0),(9,7,10,69041,0),(9,7,72,403,0),(9,8,0,133,0),(9,8,9,69070,0),(9,8,10,69041,0),(9,9,0,686,0),(9,9,9,69070,0),(9,9,10,69041,0),(9,9,11,10,48),(10,1,72,88163,0),(10,1,73,88161,0),(10,1,81,69179,0),(10,2,0,35395,0),(10,2,9,28730,0),(10,3,0,3044,0),(10,3,9,80483,0),(10,3,10,9,48),(10,3,11,982,0),(10,4,0,1752,0),(10,4,9,25046,0),(10,5,0,585,0),(10,5,9,28730,0),(10,8,0,133,0),(10,8,9,28730,0),(10,9,0,686,0),(10,9,9,28730,0),(10,9,10,10,48),(11,1,72,88163,0),(11,1,73,88161,0),(11,1,81,28880,0),(11,1,84,6603,0),(11,1,96,6603,0),(11,1,108,6603,0),(11,2,0,35395,0),(11,2,9,59542,0),(11,2,83,4540,128),(11,3,0,3044,0),(11,3,9,59543,0),(11,3,10,9,48),(11,3,11,982,0),(11,3,72,6603,0),(11,3,74,75,0),(11,3,82,159,128),(11,3,83,4540,128),(11,5,0,585,0),(11,5,9,59544,0),(11,5,83,4540,128),(11,7,0,403,0),(11,7,9,59547,0),(11,8,0,133,0),(11,8,9,59548,0),(11,8,83,4540,128),(22,1,72,88163,0),(22,1,73,88161,0),(22,3,0,3044,0),(22,3,10,9,48),(22,3,11,982,0),(22,4,0,1752,0),(22,5,0,585,0),(22,8,0,133,0),(22,9,0,686,0),(22,9,10,10,48),(22,11,0,5176,0),(22,11,72,6603,0),(22,11,84,6603,0),(22,11,96,6603,0),(1,6,0,6603,0),(1,6,1,49576,0),(1,6,2,45477,0),(1,6,3,45462,0),(1,6,4,45902,0),(1,6,5,47541,0),(1,6,11,59752,0),(2,6,0,6603,0),(2,6,1,49576,0),(2,6,2,45477,0),(2,6,3,45462,0),(2,6,4,45902,0),(2,6,5,47541,0),(2,6,10,20572,0),(5,6,0,0,0),(5,6,2,64,0),(5,6,3,512,0),(5,6,4,0,2),(5,6,5,0,0),(5,6,8,512,0),(5,6,9,0,64),(5,6,10,0,0),(5,6,12,64,0),(5,6,13,3072,0),(5,6,14,0,14),(5,6,15,0,0),(5,6,18,4352,0),(5,6,19,0,17),(5,6,20,0,0),(5,6,22,17,0),(5,6,23,1536,0),(5,6,24,0,6),(5,6,25,0,0),(5,6,27,6,0),(5,6,28,1024,0),(5,6,29,0,4),(5,6,30,0,0),(5,6,32,4,0),(5,6,33,1024,0),(5,6,34,0,6),(5,6,35,0,0),(5,6,37,4,0),(5,6,38,1024,0),(5,6,39,0,4),(5,6,40,0,0),(5,6,42,4,0),(5,6,43,1024,0),(5,6,47,16,0),(5,6,48,512,0),(5,6,49,0,16),(5,6,50,0,0),(5,6,52,16,0),(5,6,54,0,24),(5,6,55,0,0),(5,6,57,6,0),(5,6,58,4096,0),(5,6,59,0,14),(5,6,60,0,0),(5,6,62,6,0),(5,6,63,4096,0),(5,6,64,0,16),(5,6,65,0,0),(5,6,67,2,0),(5,6,68,512,0),(5,6,69,0,17),(5,6,70,0,0),(5,6,72,16,0),(5,6,73,20480,0),(5,6,74,0,28),(5,6,75,0,0),(5,6,77,16,0),(5,6,78,20480,0),(5,6,79,0,16),(5,6,80,0,0),(5,6,83,512,0),(5,6,84,0,16),(5,6,87,16,0),(5,6,88,4096,0),(5,6,89,0,2),(5,6,90,0,0),(5,6,92,16,0),(5,6,93,1536,0),(5,6,94,0,152),(5,6,95,0,0),(5,6,97,16,0),(5,6,98,1536,0),(5,6,99,0,16),(5,6,100,0,0),(5,6,102,16,0),(5,6,103,512,0),(5,6,104,0,16),(5,6,105,0,0),(5,6,107,24,0),(5,6,108,2048,0),(5,6,110,0,0),(5,6,112,12,0),(5,6,113,1536,0),(5,6,114,0,16),(5,6,115,0,0),(5,6,117,16,0),(5,6,118,4096,0),(5,6,119,0,12),(5,6,120,0,0),(5,6,122,2,0),(5,6,123,4096,0),(5,6,124,0,2),(5,6,125,0,0),(5,6,127,4,0),(5,6,128,1024,0),(5,6,129,0,4),(5,6,130,0,0),(5,6,132,17,0),(5,6,133,1536,0),(5,6,138,4096,0),(5,6,139,0,8),(5,6,142,16,0),(5,6,143,4096,0),(6,6,0,6603,0),(6,6,1,49576,0),(6,6,2,45477,0),(6,6,3,45462,0),(6,6,4,45902,0),(6,6,5,47541,0),(6,6,10,20549,0),(6,6,75,20549,0),(7,6,0,6603,0),(7,6,1,49576,0),(7,6,2,45477,0),(7,6,3,45462,0),(7,6,4,45902,0),(7,6,5,47541,0),(7,6,10,20589,0),(7,6,72,6603,0),(7,6,83,117,0),(7,6,84,6603,0),(7,6,96,6603,0),(7,6,108,6603,0),(8,6,0,6603,0),(8,6,1,49576,0),(8,6,2,45477,0),(8,6,3,45462,0),(8,6,4,45902,0),(8,6,5,47541,0),(8,6,10,26297,0),(9,6,0,6603,0),(9,6,1,49576,0),(9,6,2,45477,0),(9,6,3,45462,0),(9,6,4,45902,0),(9,6,5,47541,0),(9,6,9,3534,1),(9,6,10,3505,1),(9,6,11,3510,1),(10,6,0,6603,0),(10,6,1,49576,0),(10,6,2,45477,0),(10,6,3,45462,0),(10,6,4,45902,0),(10,6,5,47541,0),(10,6,6,50613,0),(11,6,0,6603,0),(11,6,1,49576,0),(11,6,2,45477,0),(11,6,3,45462,0),(11,6,4,45902,0),(11,6,5,47541,0),(11,6,10,59545,0),(22,6,0,6603,0),(22,6,1,49576,0),(22,6,2,45477,0),(22,6,3,45462,0),(22,6,4,45902,0),(22,6,5,47541,0),(22,6,9,3456,1),(3,6,0,6603,0),(3,6,1,49576,0),(3,6,2,45477,0),(3,6,3,45462,0),(3,6,4,45902,0),(3,6,5,47541,0),(3,6,11,59752,0),(4,6,0,6603,0),(4,6,1,49576,0),(4,6,2,45477,0),(4,6,3,45462,0),(4,6,4,45902,0),(4,6,5,47541,0),(4,6,11,59752,0);
/*!40000 ALTER TABLE `playercreateinfo_action` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `playercreateinfo_item`
--

DROP TABLE IF EXISTS `playercreateinfo_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `playercreateinfo_item` (
  `race` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `class` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `itemid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `amount` tinyint(3) NOT NULL DEFAULT '1',
  KEY `playercreateinfo_race_class_index` (`race`,`class`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `playercreateinfo_item`
--

LOCK TABLES `playercreateinfo_item` WRITE;
/*!40000 ALTER TABLE `playercreateinfo_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `playercreateinfo_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `playercreateinfo_spell`
--

DROP TABLE IF EXISTS `playercreateinfo_spell`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `playercreateinfo_spell` (
  `race` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `class` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `Spell` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `Note` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`race`,`class`,`Spell`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `playercreateinfo_spell`
--

LOCK TABLES `playercreateinfo_spell` WRITE;
/*!40000 ALTER TABLE `playercreateinfo_spell` DISABLE KEYS */;
INSERT INTO `playercreateinfo_spell` VALUES (1,1,202,'Two-Handed Swords'),(1,1,196,'One-Handed Axes'),(1,1,2457,'Battle Stance'),(1,1,88161,'Strike'),(1,1,5011,'Crossbows'),(1,1,3018,'Shoot'),(1,1,197,'Two-Handed Axes'),(1,1,1180,'Daggers'),(1,1,45927,'Summon Friend'),(1,1,6478,'Opening'),(1,1,6603,'Auto Attack'),(1,1,22027,'Remove Insignia'),(1,1,22810,'Opening - No Text'),(1,1,21651,'Opening'),(1,1,21652,'Closing'),(1,1,2382,'Generic'),(1,1,3365,'Opening'),(1,1,3050,'Detect'),(1,1,9125,'Generic'),(1,1,8386,'Attacking'),(1,1,7266,'Duel'),(1,1,7267,'Grovel'),(1,1,7355,'Stuck'),(1,1,6233,'Closing'),(1,1,6246,'Closing'),(1,1,6247,'Opening'),(1,1,6477,'Opening'),(1,1,61437,'Opening'),(1,1,68398,'Opening'),(1,1,96220,'Opening'),(1,1,203,'Unarmed'),(1,1,20864,'Mace Specialization'),(1,1,20599,'Diplomacy'),(1,1,20597,'Sword Specialization'),(1,1,20598,'The Human Spirit'),(1,1,59752,'Every Man for Himself'),(1,1,79738,'Languages'),(1,1,49410,'Forceful Deflection'),(1,1,199,'Two-Handed Maces'),(1,1,15590,'Fist Weapons'),(1,1,76268,'Armor Skills'),(1,1,76290,'Weapon Skills'),(1,1,88163,'Attack'),(1,1,200,'Polearms'),(1,1,668,'Language Common'),(1,1,227,'Staves'),(1,1,9116,'Shield'),(1,1,8737,'Mail'),(1,1,9077,'Leather'),(1,1,264,'Bows'),(1,1,266,'Guns'),(1,1,107,'Block'),(1,1,81,'Dodge'),(1,1,32215,'Victorious State'),(1,1,5301,'Defensive State (DND)'),(1,1,204,'Defense'),(1,1,522,'SPELLDEFENSE (DND)'),(1,1,2764,'Throw'),(1,1,2567,'Thrown'),(1,1,9078,'Cloth'),(1,1,198,'One-Handed Maces'),(1,1,201,'One-Handed Swords'),(1,2,202,'Two-Handed Swords'),(1,2,196,'One-Handed Axes'),(1,2,197,'Two-Handed Axes'),(1,2,45927,'Summon Friend'),(1,2,6478,'Opening'),(1,2,6603,'Auto Attack'),(1,2,22027,'Remove Insignia'),(1,2,22810,'Opening - No Text'),(1,2,21651,'Opening'),(1,2,21652,'Closing'),(1,2,2382,'Generic'),(1,2,3365,'Opening'),(1,2,3050,'Detect'),(1,2,9125,'Generic'),(1,2,8386,'Attacking'),(1,2,7266,'Duel'),(1,2,7267,'Grovel'),(1,2,7355,'Stuck'),(1,2,6233,'Closing'),(1,2,6246,'Closing'),(1,2,6247,'Opening'),(1,2,6477,'Opening'),(1,2,27762,'Relic'),(1,2,61437,'Opening'),(1,2,68398,'Opening'),(1,2,96220,'Opening'),(1,2,49410,'Forceful Deflection'),(1,2,203,'Unarmed'),(1,2,20864,'Mace Specialization'),(1,2,20599,'Diplomacy'),(1,2,20597,'Sword Specialization'),(1,2,20598,'The Human Spirit'),(1,2,59752,'Every Man for Himself'),(1,2,79738,'Languages'),(1,2,199,'Two-Handed Maces'),(1,2,76271,'Armor Skills'),(1,2,76294,'Weapon Skills'),(1,2,200,'Polearms'),(1,2,668,'Language Common'),(1,2,35395,'Crusader Strike'),(1,2,60091,'Judgement Anti-Parry/Dodge Passive'),(1,2,9116,'Shield'),(1,2,8737,'Mail'),(1,2,9077,'Leather'),(1,2,107,'Block'),(1,2,81,'Dodge'),(1,2,204,'Defense'),(1,2,522,'SPELLDEFENSE (DND)'),(1,2,9078,'Cloth'),(1,2,20208,'Paladin pushback resistance'),(1,2,198,'One-Handed Maces'),(1,2,201,'One-Handed Swords'),(1,3,202,'Two-Handed Swords'),(1,3,196,'One-Handed Axes'),(1,3,3044,'Arcane Shot'),(1,3,75,'Auto Shot'),(1,3,82928,'Aimed Shot!'),(1,3,5011,'Crossbows'),(1,3,197,'Two-Handed Axes'),(1,3,1180,'Daggers'),(1,3,45927,'Summon Friend'),(1,3,6478,'Opening'),(1,3,6603,'Auto Attack'),(1,3,22027,'Remove Insignia'),(1,3,22810,'Opening - No Text'),(1,3,21651,'Opening'),(1,3,21652,'Closing'),(1,3,34082,'Hunter Passive Auras (DND)'),(1,3,2382,'Generic'),(1,3,3365,'Opening'),(1,3,3050,'Detect'),(1,3,9125,'Generic'),(1,3,8386,'Attacking'),(1,3,7266,'Duel'),(1,3,7267,'Grovel'),(1,3,7355,'Stuck'),(1,3,6233,'Closing'),(1,3,6246,'Closing'),(1,3,6247,'Opening'),(1,3,6477,'Opening'),(1,3,61437,'Opening'),(1,3,68398,'Opening'),(1,3,96220,'Opening'),(1,3,203,'Unarmed'),(1,3,883,'Call Pet 1'),(1,3,982,'Revive Pet'),(1,3,20864,'Mace Specialization'),(1,3,20599,'Diplomacy'),(1,3,20597,'Sword Specialization'),(1,3,20598,'The Human Spirit'),(1,3,59752,'Every Man for Himself'),(1,3,79738,'Languages'),(1,3,15590,'Fist Weapons'),(1,3,200,'Polearms'),(1,3,668,'Language Common'),(1,3,227,'Staves'),(1,3,9077,'Leather'),(1,3,264,'Bows'),(1,3,266,'Guns'),(1,3,13358,'Defensive State (DND)'),(1,3,81,'Dodge'),(1,3,204,'Defense'),(1,3,522,'SPELLDEFENSE (DND)'),(1,3,24949,'Defensive State 2 (DND)'),(1,3,9078,'Cloth'),(1,3,77442,'Focus'),(1,3,76249,'Weapon Skills'),(1,3,76250,'Armor Skills'),(1,3,87324,'Focused Aim'),(1,3,87816,'General Hunter Passives'),(1,3,201,'One-Handed Swords'),(1,4,196,'One-Handed Axes'),(1,4,1752,'Sinister Strike'),(1,4,5011,'Crossbows'),(1,4,3018,'Shoot'),(1,4,674,'Dual Wield'),(1,4,1180,'Daggers'),(1,4,45927,'Summon Friend'),(1,4,6478,'Opening'),(1,4,6603,'Auto Attack'),(1,4,22027,'Remove Insignia'),(1,4,22810,'Opening - No Text'),(1,4,21651,'Opening'),(1,4,21652,'Closing'),(1,4,2382,'Generic'),(1,4,3365,'Opening'),(1,4,3050,'Detect'),(1,4,9125,'Generic'),(1,4,8386,'Attacking'),(1,4,7266,'Duel'),(1,4,7267,'Grovel'),(1,4,7355,'Stuck'),(1,4,6233,'Closing'),(1,4,6246,'Closing'),(1,4,6247,'Opening'),(1,4,6477,'Opening'),(1,4,61437,'Opening'),(1,4,68398,'Opening'),(1,4,96220,'Opening'),(1,4,203,'Unarmed'),(1,4,20864,'Mace Specialization'),(1,4,20599,'Diplomacy'),(1,4,20597,'Sword Specialization'),(1,4,20598,'The Human Spirit'),(1,4,59752,'Every Man for Himself'),(1,4,79738,'Languages'),(1,4,15590,'Fist Weapons'),(1,4,76273,'Armor Skills'),(1,4,76297,'Weapon Skills'),(1,4,668,'Language Common'),(1,4,9077,'Leather'),(1,4,264,'Bows'),(1,4,266,'Guns'),(1,4,81,'Dodge'),(1,4,204,'Defense'),(1,4,522,'SPELLDEFENSE (DND)'),(1,4,16092,'Defensive State (DND)'),(1,4,2764,'Throw'),(1,4,2567,'Thrown'),(1,4,9078,'Cloth'),(1,4,198,'One-Handed Maces'),(1,4,201,'One-Handed Swords'),(1,5,5009,'Wands'),(1,5,5019,'Shoot'),(1,5,1180,'Daggers'),(1,5,45927,'Summon Friend'),(1,5,6478,'Opening'),(1,5,6603,'Auto Attack'),(1,5,22027,'Remove Insignia'),(1,5,22810,'Opening - No Text'),(1,5,21651,'Opening'),(1,5,21652,'Closing'),(1,5,2382,'Generic'),(1,5,3365,'Opening'),(1,5,3050,'Detect'),(1,5,9125,'Generic'),(1,5,8386,'Attacking'),(1,5,7266,'Duel'),(1,5,7267,'Grovel'),(1,5,7355,'Stuck'),(1,5,6233,'Closing'),(1,5,6246,'Closing'),(1,5,6247,'Opening'),(1,5,6477,'Opening'),(1,5,61437,'Opening'),(1,5,68398,'Opening'),(1,5,96220,'Opening'),(1,5,203,'Unarmed'),(1,5,585,'Smite'),(1,5,84733,'Holy Focus'),(1,5,88685,'Holy Word: Sanctuary'),(1,5,88684,'Holy Word: Serenity'),(1,5,101062,'Flash Heal'),(1,5,20864,'Mace Specialization'),(1,5,20599,'Diplomacy'),(1,5,20597,'Sword Specialization'),(1,5,20598,'The Human Spirit'),(1,5,59752,'Every Man for Himself'),(1,5,79738,'Languages'),(1,5,76301,'Weapon Skills'),(1,5,76279,'Armor Skills'),(1,5,84734,'Dark Thoughts'),(1,5,668,'Language Common'),(1,5,227,'Staves'),(1,5,77486,'Shadow Orb Power'),(1,5,81,'Dodge'),(1,5,204,'Defense'),(1,5,522,'SPELLDEFENSE (DND)'),(1,5,9078,'Cloth'),(1,5,198,'One-Handed Maces'),(1,8,5009,'Wands'),(1,8,5019,'Shoot'),(1,8,1180,'Daggers'),(1,8,45927,'Summon Friend'),(1,8,6478,'Opening'),(1,8,6603,'Auto Attack'),(1,8,22027,'Remove Insignia'),(1,8,22810,'Opening - No Text'),(1,8,21651,'Opening'),(1,8,21652,'Closing'),(1,8,2382,'Generic'),(1,8,3365,'Opening'),(1,8,3050,'Detect'),(1,8,9125,'Generic'),(1,8,8386,'Attacking'),(1,8,7266,'Duel'),(1,8,7267,'Grovel'),(1,8,7355,'Stuck'),(1,8,6233,'Closing'),(1,8,6246,'Closing'),(1,8,6247,'Opening'),(1,8,6477,'Opening'),(1,8,61437,'Opening'),(1,8,68398,'Opening'),(1,8,96220,'Opening'),(1,8,79684,'Offensive State (DND)'),(1,8,203,'Unarmed'),(1,8,20864,'Mace Specialization'),(1,8,20599,'Diplomacy'),(1,8,20597,'Sword Specialization'),(1,8,20598,'The Human Spirit'),(1,8,59752,'Every Man for Himself'),(1,8,79738,'Languages'),(1,8,133,'Fireball'),(1,8,92315,'Pyroblast!'),(1,8,85801,'DPS Caster Crit Damage Bonus'),(1,8,76298,'Weapon Skills'),(1,8,76276,'Armor Skills'),(1,8,668,'Language Common'),(1,8,227,'Staves'),(1,8,81,'Dodge'),(1,8,204,'Defense'),(1,8,522,'SPELLDEFENSE (DND)'),(1,8,9078,'Cloth'),(1,8,201,'One-Handed Swords'),(1,9,5009,'Wands'),(1,9,5019,'Shoot'),(1,9,1180,'Daggers'),(1,9,45927,'Summon Friend'),(1,9,6478,'Opening'),(1,9,6603,'Auto Attack'),(1,9,22027,'Remove Insignia'),(1,9,22810,'Opening - No Text'),(1,9,21651,'Opening'),(1,9,21652,'Closing'),(1,9,2382,'Generic'),(1,9,3365,'Opening'),(1,9,3050,'Detect'),(1,9,9125,'Generic'),(1,9,8386,'Attacking'),(1,9,7266,'Duel'),(1,9,7267,'Grovel'),(1,9,7355,'Stuck'),(1,9,6233,'Closing'),(1,9,6246,'Closing'),(1,9,6247,'Opening'),(1,9,6477,'Opening'),(1,9,61437,'Opening'),(1,9,68398,'Opening'),(1,9,96220,'Opening'),(1,9,203,'Unarmed'),(1,9,20864,'Mace Specialization'),(1,9,20599,'Diplomacy'),(1,9,20597,'Sword Specialization'),(1,9,20598,'The Human Spirit'),(1,9,59752,'Every Man for Himself'),(1,9,79738,'Languages'),(1,9,688,'Summon Imp'),(1,9,86213,'Soul Swap Exhale'),(1,9,89420,'Drain Life'),(1,9,85801,'DPS Caster Crit Damage Bonus'),(1,9,76299,'Weapon Skills'),(1,9,76277,'Armor Skills'),(1,9,87330,'Suppression'),(1,9,668,'Language Common'),(1,9,227,'Staves'),(1,9,75445,'Demonic Immolate'),(1,9,686,'Shadow Bolt'),(1,9,58284,'Chaos Bolt Passive'),(1,9,81,'Dodge'),(1,9,204,'Defense'),(1,9,522,'SPELLDEFENSE (DND)'),(1,9,9078,'Cloth'),(1,9,201,'One-Handed Swords'),(2,1,202,'Two-Handed Swords'),(2,1,196,'One-Handed Axes'),(2,1,2457,'Battle Stance'),(2,1,88161,'Strike'),(2,1,5011,'Crossbows'),(2,1,3018,'Shoot'),(2,1,669,'Language Orcish'),(2,1,197,'Two-Handed Axes'),(2,1,1180,'Daggers'),(2,1,45927,'Summon Friend'),(2,1,6478,'Opening'),(2,1,6603,'Auto Attack'),(2,1,22027,'Remove Insignia'),(2,1,22810,'Opening - No Text'),(2,1,21651,'Opening'),(2,1,21652,'Closing'),(2,1,2382,'Generic'),(2,1,3365,'Opening'),(2,1,3050,'Detect'),(2,1,9125,'Generic'),(2,1,8386,'Attacking'),(2,1,7266,'Duel'),(2,1,7267,'Grovel'),(2,1,7355,'Stuck'),(2,1,6233,'Closing'),(2,1,6246,'Closing'),(2,1,6247,'Opening'),(2,1,6477,'Opening'),(2,1,61437,'Opening'),(2,1,68398,'Opening'),(2,1,96220,'Opening'),(2,1,203,'Unarmed'),(2,1,49410,'Forceful Deflection'),(2,1,21563,'Command'),(2,1,20572,'Blood Fury'),(2,1,20573,'Hardiness'),(2,1,20574,'Axe Specialization'),(2,1,79743,'Languages'),(2,1,199,'Two-Handed Maces'),(2,1,15590,'Fist Weapons'),(2,1,76268,'Armor Skills'),(2,1,76290,'Weapon Skills'),(2,1,88163,'Attack'),(2,1,200,'Polearms'),(2,1,227,'Staves'),(2,1,9116,'Shield'),(2,1,8737,'Mail'),(2,1,9077,'Leather'),(2,1,264,'Bows'),(2,1,266,'Guns'),(2,1,107,'Block'),(2,1,81,'Dodge'),(2,1,32215,'Victorious State'),(2,1,5301,'Defensive State (DND)'),(2,1,204,'Defense'),(2,1,522,'SPELLDEFENSE (DND)'),(2,1,2764,'Throw'),(2,1,2567,'Thrown'),(2,1,9078,'Cloth'),(2,1,198,'One-Handed Maces'),(2,1,201,'One-Handed Swords'),(2,3,202,'Two-Handed Swords'),(2,3,196,'One-Handed Axes'),(2,3,3044,'Arcane Shot'),(2,3,75,'Auto Shot'),(2,3,82928,'Aimed Shot!'),(2,3,5011,'Crossbows'),(2,3,669,'Language Orcish'),(2,3,197,'Two-Handed Axes'),(2,3,1180,'Daggers'),(2,3,45927,'Summon Friend'),(2,3,6478,'Opening'),(2,3,6603,'Auto Attack'),(2,3,22027,'Remove Insignia'),(2,3,22810,'Opening - No Text'),(2,3,21651,'Opening'),(2,3,21652,'Closing'),(2,3,34082,'Hunter Passive Auras (DND)'),(2,3,2382,'Generic'),(2,3,3365,'Opening'),(2,3,3050,'Detect'),(2,3,9125,'Generic'),(2,3,8386,'Attacking'),(2,3,7266,'Duel'),(2,3,7267,'Grovel'),(2,3,7355,'Stuck'),(2,3,6233,'Closing'),(2,3,6246,'Closing'),(2,3,6247,'Opening'),(2,3,6477,'Opening'),(2,3,61437,'Opening'),(2,3,68398,'Opening'),(2,3,96220,'Opening'),(2,3,203,'Unarmed'),(2,3,883,'Call Pet 1'),(2,3,982,'Revive Pet'),(2,3,20572,'Blood Fury'),(2,3,20573,'Hardiness'),(2,3,20574,'Axe Specialization'),(2,3,20576,'Command'),(2,3,79743,'Languages'),(2,3,15590,'Fist Weapons'),(2,3,200,'Polearms'),(2,3,227,'Staves'),(2,3,9077,'Leather'),(2,3,264,'Bows'),(2,3,266,'Guns'),(2,3,13358,'Defensive State (DND)'),(2,3,81,'Dodge'),(2,3,204,'Defense'),(2,3,522,'SPELLDEFENSE (DND)'),(2,3,24949,'Defensive State 2 (DND)'),(2,3,9078,'Cloth'),(2,3,77442,'Focus'),(2,3,76249,'Weapon Skills'),(2,3,76250,'Armor Skills'),(2,3,87324,'Focused Aim'),(2,3,87816,'General Hunter Passives'),(2,3,201,'One-Handed Swords'),(2,4,196,'One-Handed Axes'),(2,4,1752,'Sinister Strike'),(2,4,5011,'Crossbows'),(2,4,3018,'Shoot'),(2,4,669,'Language Orcish'),(2,4,674,'Dual Wield'),(2,4,1180,'Daggers'),(2,4,45927,'Summon Friend'),(2,4,6478,'Opening'),(2,4,6603,'Auto Attack'),(2,4,22027,'Remove Insignia'),(2,4,22810,'Opening - No Text'),(2,4,21651,'Opening'),(2,4,21652,'Closing'),(2,4,2382,'Generic'),(2,4,3365,'Opening'),(2,4,3050,'Detect'),(2,4,9125,'Generic'),(2,4,8386,'Attacking'),(2,4,7266,'Duel'),(2,4,7267,'Grovel'),(2,4,7355,'Stuck'),(2,4,6233,'Closing'),(2,4,6246,'Closing'),(2,4,6247,'Opening'),(2,4,6477,'Opening'),(2,4,61437,'Opening'),(2,4,68398,'Opening'),(2,4,96220,'Opening'),(2,4,203,'Unarmed'),(2,4,21563,'Command'),(2,4,20572,'Blood Fury'),(2,4,20573,'Hardiness'),(2,4,20574,'Axe Specialization'),(2,4,79743,'Languages'),(2,4,15590,'Fist Weapons'),(2,4,76273,'Armor Skills'),(2,4,76297,'Weapon Skills'),(2,4,9077,'Leather'),(2,4,264,'Bows'),(2,4,266,'Guns'),(2,4,81,'Dodge'),(2,4,204,'Defense'),(2,4,522,'SPELLDEFENSE (DND)'),(2,4,16092,'Defensive State (DND)'),(2,4,2764,'Throw'),(2,4,2567,'Thrown'),(2,4,9078,'Cloth'),(2,4,198,'One-Handed Maces'),(2,4,201,'One-Handed Swords'),(2,7,196,'One-Handed Axes'),(2,7,669,'Language Orcish'),(2,7,197,'Two-Handed Axes'),(2,7,1180,'Daggers'),(2,7,45927,'Summon Friend'),(2,7,6478,'Opening'),(2,7,6603,'Auto Attack'),(2,7,22027,'Remove Insignia'),(2,7,22810,'Opening - No Text'),(2,7,21651,'Opening'),(2,7,21652,'Closing'),(2,7,2382,'Generic'),(2,7,3365,'Opening'),(2,7,3050,'Detect'),(2,7,9125,'Generic'),(2,7,8386,'Attacking'),(2,7,7266,'Duel'),(2,7,7267,'Grovel'),(2,7,7355,'Stuck'),(2,7,6233,'Closing'),(2,7,6246,'Closing'),(2,7,6247,'Opening'),(2,7,6477,'Opening'),(2,7,27763,'Relic'),(2,7,61437,'Opening'),(2,7,68398,'Opening'),(2,7,96220,'Opening'),(2,7,203,'Unarmed'),(2,7,20573,'Hardiness'),(2,7,20574,'Axe Specialization'),(2,7,65222,'Command'),(2,7,79743,'Languages'),(2,7,33697,'Blood Fury'),(2,7,199,'Two-Handed Maces'),(2,7,15590,'Fist Weapons'),(2,7,76272,'Armor Skills'),(2,7,76296,'Weapon Skills'),(2,7,89920,'Ancestral Focus'),(2,7,227,'Staves'),(2,7,9116,'Shield'),(2,7,9077,'Leather'),(2,7,107,'Block'),(2,7,81,'Dodge'),(2,7,204,'Defense'),(2,7,522,'SPELLDEFENSE (DND)'),(2,7,403,'Lightning Bolt'),(2,7,9078,'Cloth'),(2,7,198,'One-Handed Maces'),(22,8,71761,'Deep Freeze Immunity State'),(2,8,5009,'Wands'),(2,8,5019,'Shoot'),(2,8,669,'Language Orcish'),(2,8,1180,'Daggers'),(2,8,45927,'Summon Friend'),(2,8,6478,'Opening'),(2,8,6603,'Auto Attack'),(2,8,22027,'Remove Insignia'),(2,8,22810,'Opening - No Text'),(2,8,21651,'Opening'),(2,8,21652,'Closing'),(2,8,2382,'Generic'),(2,8,3365,'Opening'),(2,8,3050,'Detect'),(2,8,9125,'Generic'),(2,8,8386,'Attacking'),(2,8,7266,'Duel'),(2,8,7267,'Grovel'),(2,8,7355,'Stuck'),(2,8,6233,'Closing'),(2,8,6246,'Closing'),(2,8,6247,'Opening'),(2,8,6477,'Opening'),(2,8,61437,'Opening'),(2,8,68398,'Opening'),(2,8,96220,'Opening'),(2,8,79684,'Offensive State (DND)'),(2,8,203,'Unarmed'),(2,8,21563,'Command'),(2,8,33702,'Blood Fury'),(2,8,20573,'Hardiness'),(2,8,20574,'Axe Specialization'),(2,8,79743,'Languages'),(2,8,133,'Fireball'),(2,8,92315,'Pyroblast!'),(2,8,85801,'DPS Caster Crit Damage Bonus'),(2,8,76298,'Weapon Skills'),(2,8,76276,'Armor Skills'),(2,8,227,'Staves'),(2,8,81,'Dodge'),(2,8,204,'Defense'),(2,8,522,'SPELLDEFENSE (DND)'),(2,8,9078,'Cloth'),(2,8,201,'One-Handed Swords'),(2,9,5009,'Wands'),(2,9,5019,'Shoot'),(2,9,669,'Language Common'),(2,9,1180,'Daggers'),(2,9,45927,'Summon Friend'),(2,9,6478,'Opening'),(2,9,6603,'Auto Attack'),(2,9,22027,'Remove Insignia'),(2,9,22810,'Opening - No Text'),(2,9,21651,'Opening'),(2,9,21652,'Closing'),(2,9,2382,'Generic'),(2,9,3365,'Opening'),(2,9,3050,'Detect'),(2,9,9125,'Generic'),(2,9,8386,'Attacking'),(2,9,7266,'Duel'),(2,9,7267,'Grovel'),(2,9,7355,'Stuck'),(2,9,6233,'Closing'),(2,9,6246,'Closing'),(2,9,6247,'Opening'),(2,9,6477,'Opening'),(2,9,61437,'Opening'),(2,9,68398,'Opening'),(2,9,96220,'Opening'),(2,9,203,'Unarmed'),(2,9,33702,'Blood Fury'),(2,9,20573,'Hardiness'),(2,9,20574,'Axe Specialization'),(2,9,79743,'Languages'),(2,9,688,'Summon Imp'),(2,9,86213,'Soul Swap Exhale'),(2,9,89420,'Drain Life'),(2,9,85801,'DPS Caster Crit Damage Bonus'),(2,9,76299,'Weapon Skills'),(2,9,76277,'Armor Skills'),(2,9,87330,'Suppression'),(2,9,227,'Staves'),(2,9,75445,'Demonic Immolate'),(2,9,686,'Shadow Bolt'),(2,9,58284,'Chaos Bolt Passive'),(2,9,81,'Dodge'),(2,9,204,'Defense'),(2,9,522,'SPELLDEFENSE (DND)'),(2,9,9078,'Cloth'),(2,9,201,'One-Handed Swords'),(3,1,202,'Two-Handed Swords'),(3,1,196,'One-Handed Axes'),(3,1,2457,'Battle Stance'),(3,1,88161,'Strike'),(3,1,5011,'Crossbows'),(3,1,3018,'Shoot'),(3,1,197,'Two-Handed Axes'),(3,1,1180,'Daggers'),(3,1,45927,'Summon Friend'),(3,1,6478,'Opening'),(3,1,6603,'Auto Attack'),(3,1,22027,'Remove Insignia'),(3,1,22810,'Opening - No Text'),(3,1,21651,'Opening'),(3,1,21652,'Closing'),(3,1,2382,'Generic'),(3,1,3365,'Opening'),(3,1,3050,'Detect'),(3,1,9125,'Generic'),(3,1,8386,'Attacking'),(3,1,7266,'Duel'),(3,1,7267,'Grovel'),(3,1,7355,'Stuck'),(3,1,6233,'Closing'),(3,1,6246,'Closing'),(3,1,6247,'Opening'),(3,1,6477,'Opening'),(3,1,61437,'Opening'),(3,1,68398,'Opening'),(3,1,96220,'Opening'),(3,1,203,'Unarmed'),(3,1,20596,'Frost Resistance'),(3,1,20595,'Gun Specialization'),(3,1,20594,'Stoneform'),(3,1,59224,'Mace Specialization'),(3,1,92682,'Explorer'),(3,1,79739,'Languages'),(3,1,672,'Language Dwarven'),(3,1,49410,'Forceful Deflection'),(3,1,199,'Two-Handed Maces'),(3,1,15590,'Fist Weapons'),(3,1,76268,'Armor Skills'),(3,1,76290,'Weapon Skills'),(3,1,88163,'Attack'),(3,1,200,'Polearms'),(3,1,668,'Language Common'),(3,1,227,'Staves'),(3,1,9116,'Shield'),(3,1,8737,'Mail'),(3,1,9077,'Leather'),(3,1,264,'Bows'),(3,1,266,'Guns'),(3,1,107,'Block'),(3,1,81,'Dodge'),(3,1,32215,'Victorious State'),(3,1,5301,'Defensive State (DND)'),(3,1,204,'Defense'),(3,1,522,'SPELLDEFENSE (DND)'),(3,1,2764,'Throw'),(3,1,2567,'Thrown'),(3,1,9078,'Cloth'),(3,1,198,'One-Handed Maces'),(3,1,201,'One-Handed Swords'),(3,2,202,'Two-Handed Swords'),(3,2,196,'One-Handed Axes'),(3,2,197,'Two-Handed Axes'),(3,2,45927,'Summon Friend'),(3,2,6478,'Opening'),(3,2,6603,'Auto Attack'),(3,2,22027,'Remove Insignia'),(3,2,22810,'Opening - No Text'),(3,2,21651,'Opening'),(3,2,21652,'Closing'),(3,2,2382,'Generic'),(3,2,3365,'Opening'),(3,2,3050,'Detect'),(3,2,9125,'Generic'),(3,2,8386,'Attacking'),(3,2,7266,'Duel'),(3,2,7267,'Grovel'),(3,2,7355,'Stuck'),(3,2,6233,'Closing'),(3,2,6246,'Closing'),(3,2,6247,'Opening'),(3,2,6477,'Opening'),(3,2,27762,'Relic'),(3,2,61437,'Opening'),(3,2,68398,'Opening'),(3,2,96220,'Opening'),(3,2,49410,'Forceful Deflection'),(3,2,203,'Unarmed'),(3,2,20596,'Frost Resistance'),(3,2,20595,'Gun Specialization'),(3,2,20594,'Stoneform'),(3,2,59224,'Mace Specialization'),(3,2,92682,'Explorer'),(3,2,79739,'Languages'),(3,2,672,'Language Dwarven'),(3,2,199,'Two-Handed Maces'),(3,2,76271,'Armor Skills'),(3,2,76294,'Weapon Skills'),(3,2,200,'Polearms'),(3,2,668,'Language Common'),(3,2,35395,'Crusader Strike'),(3,2,60091,'Judgement Anti-Parry/Dodge Passive'),(3,2,9116,'Shield'),(3,2,8737,'Mail'),(3,2,9077,'Leather'),(3,2,107,'Block'),(3,2,81,'Dodge'),(3,2,204,'Defense'),(3,2,522,'SPELLDEFENSE (DND)'),(3,2,9078,'Cloth'),(3,2,20208,'Paladin pushback resistance'),(3,2,198,'One-Handed Maces'),(3,2,201,'One-Handed Swords'),(3,3,202,'Two-Handed Swords'),(3,3,196,'One-Handed Axes'),(3,3,3044,'Arcane Shot'),(3,3,75,'Auto Shot'),(3,3,82928,'Aimed Shot!'),(3,3,5011,'Crossbows'),(3,3,197,'Two-Handed Axes'),(3,3,1180,'Daggers'),(3,3,45927,'Summon Friend'),(3,3,6478,'Opening'),(3,3,6603,'Auto Attack'),(3,3,22027,'Remove Insignia'),(3,3,22810,'Opening - No Text'),(3,3,21651,'Opening'),(3,3,21652,'Closing'),(3,3,34082,'Hunter Passive Auras (DND)'),(3,3,2382,'Generic'),(3,3,3365,'Opening'),(3,3,3050,'Detect'),(3,3,9125,'Generic'),(3,3,8386,'Attacking'),(3,3,7266,'Duel'),(3,3,7267,'Grovel'),(3,3,7355,'Stuck'),(3,3,6233,'Closing'),(3,3,6246,'Closing'),(3,3,6247,'Opening'),(3,3,6477,'Opening'),(3,3,61437,'Opening'),(3,3,68398,'Opening'),(3,3,96220,'Opening'),(3,3,203,'Unarmed'),(3,3,883,'Call Pet 1'),(3,3,982,'Revive Pet'),(3,3,20596,'Frost Resistance'),(3,3,20595,'Gun Specialization'),(3,3,20594,'Stoneform'),(3,3,59224,'Mace Specialization'),(3,3,92682,'Explorer'),(3,3,79739,'Languages'),(3,3,672,'Language Dwarven'),(3,3,15590,'Fist Weapons'),(3,3,200,'Polearms'),(3,3,668,'Language Common'),(3,3,227,'Staves'),(3,3,9077,'Leather'),(3,3,264,'Bows'),(3,3,266,'Guns'),(3,3,13358,'Defensive State (DND)'),(3,3,81,'Dodge'),(3,3,204,'Defense'),(3,3,522,'SPELLDEFENSE (DND)'),(3,3,24949,'Defensive State 2 (DND)'),(3,3,9078,'Cloth'),(3,3,77442,'Focus'),(3,3,76249,'Weapon Skills'),(3,3,76250,'Armor Skills'),(3,3,87324,'Focused Aim'),(3,3,87816,'General Hunter Passives'),(3,3,201,'One-Handed Swords'),(3,4,196,'One-Handed Axes'),(3,4,1752,'Sinister Strike'),(3,4,5011,'Crossbows'),(3,4,3018,'Shoot'),(3,4,674,'Dual Wield'),(3,4,1180,'Daggers'),(3,4,45927,'Summon Friend'),(3,4,6478,'Opening'),(3,4,6603,'Auto Attack'),(3,4,22027,'Remove Insignia'),(3,4,22810,'Opening - No Text'),(3,4,21651,'Opening'),(3,4,21652,'Closing'),(3,4,2382,'Generic'),(3,4,3365,'Opening'),(3,4,3050,'Detect'),(3,4,9125,'Generic'),(3,4,8386,'Attacking'),(3,4,7266,'Duel'),(3,4,7267,'Grovel'),(3,4,7355,'Stuck'),(3,4,6233,'Closing'),(3,4,6246,'Closing'),(3,4,6247,'Opening'),(3,4,6477,'Opening'),(3,4,61437,'Opening'),(3,4,68398,'Opening'),(3,4,96220,'Opening'),(3,4,203,'Unarmed'),(3,4,20596,'Frost Resistance'),(3,4,20595,'Gun Specialization'),(3,4,20594,'Stoneform'),(3,4,59224,'Mace Specialization'),(3,4,92682,'Explorer'),(3,4,79739,'Languages'),(3,4,672,'Language Dwarven'),(3,4,15590,'Fist Weapons'),(3,4,76273,'Armor Skills'),(3,4,76297,'Weapon Skills'),(3,4,668,'Language Common'),(3,4,9077,'Leather'),(3,4,264,'Bows'),(3,4,266,'Guns'),(3,4,81,'Dodge'),(3,4,204,'Defense'),(3,4,522,'SPELLDEFENSE (DND)'),(3,4,16092,'Defensive State (DND)'),(3,4,2764,'Throw'),(3,4,2567,'Thrown'),(3,4,9078,'Cloth'),(3,4,198,'One-Handed Maces'),(3,4,201,'One-Handed Swords'),(3,5,5009,'Wands'),(3,5,5019,'Shoot'),(3,5,1180,'Daggers'),(3,5,45927,'Summon Friend'),(3,5,6478,'Opening'),(3,5,6603,'Auto Attack'),(3,5,22027,'Remove Insignia'),(3,5,22810,'Opening - No Text'),(3,5,21651,'Opening'),(3,5,21652,'Closing'),(3,5,2382,'Generic'),(3,5,3365,'Opening'),(3,5,3050,'Detect'),(3,5,9125,'Generic'),(3,5,8386,'Attacking'),(3,5,7266,'Duel'),(3,5,7267,'Grovel'),(3,5,7355,'Stuck'),(3,5,6233,'Closing'),(3,5,6246,'Closing'),(3,5,6247,'Opening'),(3,5,6477,'Opening'),(3,5,61437,'Opening'),(3,5,68398,'Opening'),(3,5,96220,'Opening'),(3,5,203,'Unarmed'),(3,5,585,'Smite'),(3,5,84733,'Holy Focus'),(3,5,88685,'Holy Word: Sanctuary'),(3,5,88684,'Holy Word: Serenity'),(3,5,101062,'Flash Heal'),(3,5,20596,'Frost Resistance'),(3,5,20595,'Gun Specialization'),(3,5,20594,'Stoneform'),(3,5,59224,'Mace Specialization'),(3,5,92682,'Explorer'),(3,5,79739,'Languages'),(3,5,672,'Language Dwarven'),(3,5,76301,'Weapon Skills'),(3,5,76279,'Armor Skills'),(3,5,84734,'Dark Thoughts'),(3,5,668,'Language Common'),(3,5,227,'Staves'),(3,5,77486,'Shadow Orb Power'),(3,5,81,'Dodge'),(3,5,204,'Defense'),(3,5,522,'SPELLDEFENSE (DND)'),(3,5,9078,'Cloth'),(3,5,198,'One-Handed Maces'),(3,7,196,'One-Handed Axes'),(3,7,197,'Two-Handed Axes'),(3,7,1180,'Daggers'),(3,7,45927,'Summon Friend'),(3,7,6478,'Opening'),(3,7,6603,'Auto Attack'),(3,7,22027,'Remove Insignia'),(3,7,22810,'Opening - No Text'),(3,7,21651,'Opening'),(3,7,21652,'Closing'),(3,7,2382,'Generic'),(3,7,3365,'Opening'),(3,7,3050,'Detect'),(3,7,9125,'Generic'),(3,7,8386,'Attacking'),(3,7,7266,'Duel'),(3,7,7267,'Grovel'),(3,7,7355,'Stuck'),(3,7,6233,'Closing'),(3,7,6246,'Closing'),(3,7,6247,'Opening'),(3,7,6477,'Opening'),(3,7,27763,'Relic'),(3,7,61437,'Opening'),(3,7,68398,'Opening'),(3,7,96220,'Opening'),(3,7,203,'Unarmed'),(3,7,20596,'Frost Resistance'),(3,7,20595,'Gun Specialization'),(3,7,20594,'Stoneform'),(3,7,59224,'Mace Specialization'),(3,7,92682,'Explorer'),(3,7,79739,'Languages'),(3,7,672,'Language Dwarven'),(3,7,199,'Two-Handed Maces'),(3,7,15590,'Fist Weapons'),(3,7,76272,'Armor Skills'),(3,7,76296,'Weapon Skills'),(3,7,89920,'Ancestral Focus'),(3,7,668,'Language Common'),(3,7,227,'Staves'),(3,7,9116,'Shield'),(3,7,9077,'Leather'),(3,7,107,'Block'),(3,7,81,'Dodge'),(3,7,204,'Defense'),(3,7,522,'SPELLDEFENSE (DND)'),(3,7,403,'Lightning Bolt'),(3,7,9078,'Cloth'),(3,7,198,'One-Handed Maces'),(11,8,71761,'Deep Freeze Immunity State'),(3,8,5009,'Wands'),(3,8,5019,'Shoot'),(3,8,1180,'Daggers'),(3,8,45927,'Summon Friend'),(3,8,6478,'Opening'),(3,8,6603,'Auto Attack'),(3,8,22027,'Remove Insignia'),(3,8,22810,'Opening - No Text'),(3,8,21651,'Opening'),(3,8,21652,'Closing'),(3,8,2382,'Generic'),(3,8,3365,'Opening'),(3,8,3050,'Detect'),(3,8,9125,'Generic'),(3,8,8386,'Attacking'),(3,8,7266,'Duel'),(3,8,7267,'Grovel'),(3,8,7355,'Stuck'),(3,8,6233,'Closing'),(3,8,6246,'Closing'),(3,8,6247,'Opening'),(3,8,6477,'Opening'),(3,8,61437,'Opening'),(3,8,68398,'Opening'),(3,8,96220,'Opening'),(3,8,79684,'Offensive State (DND)'),(3,8,203,'Unarmed'),(3,8,20596,'Frost Resistance'),(3,8,20595,'Gun Specialization'),(3,8,20594,'Stoneform'),(3,8,59224,'Mace Specialization'),(3,8,92682,'Explorer'),(3,8,79739,'Languages'),(3,8,672,'Language Dwarven'),(3,8,133,'Fireball'),(3,8,92315,'Pyroblast!'),(3,8,85801,'DPS Caster Crit Damage Bonus'),(3,8,76298,'Weapon Skills'),(3,8,76276,'Armor Skills'),(3,8,668,'Language Common'),(3,8,227,'Staves'),(3,8,81,'Dodge'),(3,8,204,'Defense'),(3,8,522,'SPELLDEFENSE (DND)'),(3,8,9078,'Cloth'),(3,8,201,'One-Handed Swords'),(3,9,5009,'Wands'),(3,9,5019,'Shoot'),(3,9,1180,'Daggers'),(3,9,45927,'Summon Friend'),(3,9,6478,'Opening'),(3,9,6603,'Auto Attack'),(3,9,22027,'Remove Insignia'),(3,9,22810,'Opening - No Text'),(3,9,21651,'Opening'),(3,9,21652,'Closing'),(3,9,2382,'Generic'),(3,9,3365,'Opening'),(3,9,3050,'Detect'),(3,9,9125,'Generic'),(3,9,8386,'Attacking'),(3,9,7266,'Duel'),(3,9,7267,'Grovel'),(3,9,7355,'Stuck'),(3,9,6233,'Closing'),(3,9,6246,'Closing'),(3,9,6247,'Opening'),(3,9,6477,'Opening'),(3,9,61437,'Opening'),(3,9,68398,'Opening'),(3,9,96220,'Opening'),(3,9,203,'Unarmed'),(3,9,20596,'Frost Resistance'),(3,9,20595,'Gun Specialization'),(3,9,20594,'Stoneform'),(3,9,59224,'Mace Specialization'),(3,9,92682,'Explorer'),(3,9,79739,'Languages'),(3,9,672,'Language Dwarven'),(3,9,688,'Summon Imp'),(3,9,86213,'Soul Swap Exhale'),(3,9,89420,'Drain Life'),(3,9,85801,'DPS Caster Crit Damage Bonus'),(3,9,76299,'Weapon Skills'),(3,9,76277,'Armor Skills'),(3,9,87330,'Suppression'),(3,9,668,'Language Common'),(3,9,227,'Staves'),(3,9,75445,'Demonic Immolate'),(3,9,686,'Shadow Bolt'),(3,9,58284,'Chaos Bolt Passive'),(3,9,81,'Dodge'),(3,9,204,'Defense'),(3,9,522,'SPELLDEFENSE (DND)'),(3,9,9078,'Cloth'),(3,9,201,'One-Handed Swords'),(4,1,202,'Two-Handed Swords'),(4,1,196,'One-Handed Axes'),(4,1,2457,'Battle Stance'),(4,1,88161,'Strike'),(4,1,5011,'Crossbows'),(4,1,3018,'Shoot'),(4,1,197,'Two-Handed Axes'),(4,1,1180,'Daggers'),(4,1,45927,'Summon Friend'),(4,1,6478,'Opening'),(4,1,6603,'Auto Attack'),(4,1,22027,'Remove Insignia'),(4,1,22810,'Opening - No Text'),(4,1,21651,'Opening'),(4,1,21652,'Closing'),(4,1,2382,'Generic'),(4,1,3365,'Opening'),(4,1,3050,'Detect'),(4,1,9125,'Generic'),(4,1,8386,'Attacking'),(4,1,7266,'Duel'),(4,1,7267,'Grovel'),(4,1,7355,'Stuck'),(4,1,6233,'Closing'),(4,1,6246,'Closing'),(4,1,6247,'Opening'),(4,1,6477,'Opening'),(4,1,61437,'Opening'),(4,1,68398,'Opening'),(4,1,96220,'Opening'),(4,1,203,'Unarmed'),(4,1,76252,'Languages'),(4,1,20583,'Nature Resistance'),(4,1,20582,'Quickness'),(4,1,20585,'Wisp Spirit'),(4,1,21009,'Elusiveness'),(4,1,58984,'Shadowmeld'),(4,1,671,'Language Darnassian'),(4,1,49410,'Forceful Deflection'),(4,1,199,'Two-Handed Maces'),(4,1,15590,'Fist Weapons'),(4,1,76268,'Armor Skills'),(4,1,76290,'Weapon Skills'),(4,1,88163,'Attack'),(4,1,200,'Polearms'),(4,1,668,'Language Common'),(4,1,227,'Staves'),(4,1,9116,'Shield'),(4,1,8737,'Mail'),(4,1,9077,'Leather'),(4,1,264,'Bows'),(4,1,266,'Guns'),(4,1,107,'Block'),(4,1,81,'Dodge'),(4,1,32215,'Victorious State'),(4,1,5301,'Defensive State (DND)'),(4,1,204,'Defense'),(4,1,522,'SPELLDEFENSE (DND)'),(4,1,2764,'Throw'),(4,1,2567,'Thrown'),(4,1,9078,'Cloth'),(4,1,198,'One-Handed Maces'),(4,1,201,'One-Handed Swords'),(4,3,202,'Two-Handed Swords'),(4,3,196,'One-Handed Axes'),(4,3,3044,'Arcane Shot'),(4,3,75,'Auto Shot'),(4,3,82928,'Aimed Shot!'),(4,3,5011,'Crossbows'),(4,3,197,'Two-Handed Axes'),(4,3,1180,'Daggers'),(4,3,45927,'Summon Friend'),(4,3,6478,'Opening'),(4,3,6603,'Auto Attack'),(4,3,22027,'Remove Insignia'),(4,3,22810,'Opening - No Text'),(4,3,21651,'Opening'),(4,3,21652,'Closing'),(4,3,34082,'Hunter Passive Auras (DND)'),(4,3,2382,'Generic'),(4,3,3365,'Opening'),(4,3,3050,'Detect'),(4,3,9125,'Generic'),(4,3,8386,'Attacking'),(4,3,7266,'Duel'),(4,3,7267,'Grovel'),(4,3,7355,'Stuck'),(4,3,6233,'Closing'),(4,3,6246,'Closing'),(4,3,6247,'Opening'),(4,3,6477,'Opening'),(4,3,61437,'Opening'),(4,3,68398,'Opening'),(4,3,96220,'Opening'),(4,3,203,'Unarmed'),(4,3,883,'Call Pet 1'),(4,3,982,'Revive Pet'),(4,3,76252,'Languages'),(4,3,20583,'Nature Resistance'),(4,3,20582,'Quickness'),(4,3,20585,'Wisp Spirit'),(4,3,21009,'Elusiveness'),(4,3,58984,'Shadowmeld'),(4,3,671,'Language Darnassian'),(4,3,15590,'Fist Weapons'),(4,3,200,'Polearms'),(4,3,668,'Language Common'),(4,3,227,'Staves'),(4,3,9077,'Leather'),(4,3,264,'Bows'),(4,3,266,'Guns'),(4,3,13358,'Defensive State (DND)'),(4,3,81,'Dodge'),(4,3,204,'Defense'),(4,3,522,'SPELLDEFENSE (DND)'),(4,3,24949,'Defensive State 2 (DND)'),(4,3,9078,'Cloth'),(4,3,77442,'Focus'),(4,3,76249,'Weapon Skills'),(4,3,76250,'Armor Skills'),(4,3,87324,'Focused Aim'),(4,3,87816,'General Hunter Passives'),(4,3,201,'One-Handed Swords'),(4,4,196,'One-Handed Axes'),(4,4,1752,'Sinister Strike'),(4,4,5011,'Crossbows'),(4,4,3018,'Shoot'),(4,4,674,'Dual Wield'),(4,4,1180,'Daggers'),(4,4,45927,'Summon Friend'),(4,4,6478,'Opening'),(4,4,6603,'Auto Attack'),(4,4,22027,'Remove Insignia'),(4,4,22810,'Opening - No Text'),(4,4,21651,'Opening'),(4,4,21652,'Closing'),(4,4,2382,'Generic'),(4,4,3365,'Opening'),(4,4,3050,'Detect'),(4,4,9125,'Generic'),(4,4,8386,'Attacking'),(4,4,7266,'Duel'),(4,4,7267,'Grovel'),(4,4,7355,'Stuck'),(4,4,6233,'Closing'),(4,4,6246,'Closing'),(4,4,6247,'Opening'),(4,4,6477,'Opening'),(4,4,61437,'Opening'),(4,4,68398,'Opening'),(4,4,96220,'Opening'),(4,4,203,'Unarmed'),(4,4,20583,'Nature Resistance'),(4,4,20582,'Quickness'),(4,4,20585,'Elusiveness'),(4,4,21009,'Wisp Spirit'),(4,4,58984,'Shadowmeld'),(4,4,76252,'Languages'),(4,4,671,'Language Darnassian'),(4,4,15590,'Fist Weapons'),(4,4,76273,'Armor Skills'),(4,4,76297,'Weapon Skills'),(4,4,668,'Language Common'),(4,4,9077,'Leather'),(4,4,264,'Bows'),(4,4,266,'Guns'),(4,4,81,'Dodge'),(4,4,204,'Defense'),(4,4,522,'SPELLDEFENSE (DND)'),(4,4,16092,'Defensive State (DND)'),(4,4,2764,'Throw'),(4,4,2567,'Thrown'),(4,4,9078,'Cloth'),(4,4,198,'One-Handed Maces'),(4,4,201,'One-Handed Swords'),(4,5,5009,'Wands'),(4,5,5019,'Shoot'),(4,5,1180,'Daggers'),(4,5,45927,'Summon Friend'),(4,5,6478,'Opening'),(4,5,6603,'Auto Attack'),(4,5,22027,'Remove Insignia'),(4,5,22810,'Opening - No Text'),(4,5,21651,'Opening'),(4,5,21652,'Closing'),(4,5,2382,'Generic'),(4,5,3365,'Opening'),(4,5,3050,'Detect'),(4,5,9125,'Generic'),(4,5,8386,'Attacking'),(4,5,7266,'Duel'),(4,5,7267,'Grovel'),(4,5,7355,'Stuck'),(4,5,6233,'Closing'),(4,5,6246,'Closing'),(4,5,6247,'Opening'),(4,5,6477,'Opening'),(4,5,61437,'Opening'),(4,5,68398,'Opening'),(4,5,96220,'Opening'),(4,5,203,'Unarmed'),(4,5,585,'Smite'),(4,5,84733,'Holy Focus'),(4,5,88685,'Holy Word: Sanctuary'),(4,5,88684,'Holy Word: Serenity'),(4,5,101062,'Flash Heal'),(4,5,20583,'Nature Resistance'),(4,5,20582,'Quickness'),(4,5,20585,'Wisp Spirit'),(4,5,21009,'Elusiveness'),(4,5,58984,'Shadowmeld'),(4,5,76252,'Languages'),(4,5,671,'Language Darnassian'),(4,5,76301,'Weapon Skills'),(4,5,76279,'Armor Skills'),(4,5,84734,'Dark Thoughts'),(4,5,668,'Language Common'),(4,5,227,'Staves'),(4,5,77486,'Shadow Orb Power'),(4,5,81,'Dodge'),(4,5,204,'Defense'),(4,5,522,'SPELLDEFENSE (DND)'),(4,5,9078,'Cloth'),(4,5,198,'One-Handed Maces'),(10,8,71761,'Deep Freeze Immunity State'),(4,8,5009,'Wands'),(4,8,5019,'Shoot'),(4,8,1180,'Daggers'),(4,8,45927,'Summon Friend'),(4,8,6478,'Opening'),(4,8,6603,'Auto Attack'),(4,8,22027,'Remove Insignia'),(4,8,22810,'Opening - No Text'),(4,8,21651,'Opening'),(4,8,21652,'Closing'),(4,8,2382,'Generic'),(4,8,3365,'Opening'),(4,8,3050,'Detect'),(4,8,9125,'Generic'),(4,8,8386,'Attacking'),(4,8,7266,'Duel'),(4,8,7267,'Grovel'),(4,8,7355,'Stuck'),(4,8,6233,'Closing'),(4,8,6246,'Closing'),(4,8,6247,'Opening'),(4,8,6477,'Opening'),(4,8,61437,'Opening'),(4,8,68398,'Opening'),(4,8,96220,'Opening'),(4,8,79684,'Offensive State (DND)'),(4,8,203,'Unarmed'),(4,8,20583,'Nature Resistance'),(4,8,20582,'Quickness'),(4,8,20585,'Wisp Spirit'),(4,8,21009,'Mace Specialization'),(4,8,58984,'Shadowmeld'),(4,8,76252,'Languages'),(4,8,671,'Language Darnassian'),(4,8,133,'Fireball'),(4,8,92315,'Pyroblast!'),(4,8,85801,'DPS Caster Crit Damage Bonus'),(4,8,76298,'Weapon Skills'),(4,8,76276,'Armor Skills'),(4,8,668,'Language Common'),(4,8,227,'Staves'),(4,8,81,'Dodge'),(4,8,204,'Defense'),(4,8,522,'SPELLDEFENSE (DND)'),(4,8,9078,'Cloth'),(4,8,201,'One-Handed Swords'),(4,11,1180,'Daggers'),(4,11,45927,'Summon Friend'),(4,11,6478,'Opening'),(4,11,6603,'Auto Attack'),(4,11,22027,'Remove Insignia'),(4,11,22810,'Opening - No Text'),(4,11,21651,'Opening'),(4,11,21652,'Closing'),(4,11,2382,'Generic'),(4,11,3365,'Opening'),(4,11,3050,'Detect'),(4,11,9125,'Generic'),(4,11,8386,'Attacking'),(4,11,7266,'Duel'),(4,11,7267,'Grovel'),(4,11,7355,'Stuck'),(4,11,6233,'Closing'),(4,11,6246,'Closing'),(4,11,6247,'Opening'),(4,11,6477,'Opening'),(4,11,61437,'Opening'),(4,11,68398,'Opening'),(4,11,96220,'Opening'),(4,11,203,'Unarmed'),(4,11,20583,'Nature Resistance'),(4,11,20582,'Quickness'),(4,11,20585,'Wisp Spirit'),(4,11,21009,'Mace Specialization'),(4,11,58984,'Shadowmeld'),(4,11,76252,'Languages'),(4,11,671,'Language Darnassian'),(4,11,84736,'Nature\'s Focus'),(4,11,81170,'Ravage!'),(4,11,79577,'Eclipse Mastery Driver Passive'),(4,11,76300,'Weapon Skills'),(4,11,76275,'Armor Skills'),(4,11,5176,'Wrath'),(4,11,199,'Two-Handed Maces'),(4,11,15590,'Fist Weapons'),(4,11,84738,'Celestial Focus'),(4,11,668,'Language Common'),(4,11,200,'Polearms'),(4,11,227,'Staves'),(4,11,81,'Dodge'),(4,11,204,'Defense'),(4,11,522,'SPELLDEFENSE (DND)'),(4,11,9077,'Leather'),(4,11,9078,'Cloth'),(4,11,198,'One-Handed Maces'),(5,1,202,'Two-Handed Swords'),(5,1,196,'One-Handed Axes'),(5,1,2457,'Battle Stance'),(5,1,88161,'Strike'),(5,1,5011,'Crossbows'),(5,1,3018,'Shoot'),(5,1,669,'Language Orcish'),(5,1,197,'Two-Handed Axes'),(5,1,1180,'Daggers'),(5,1,45927,'Summon Friend'),(5,1,6478,'Opening'),(5,1,6603,'Auto Attack'),(5,1,22027,'Remove Insignia'),(5,1,22810,'Opening - No Text'),(5,1,21651,'Opening'),(5,1,21652,'Closing'),(5,1,2382,'Generic'),(5,1,3365,'Opening'),(5,1,3050,'Detect'),(5,1,9125,'Generic'),(5,1,8386,'Attacking'),(5,1,7266,'Duel'),(5,1,7267,'Grovel'),(5,1,7355,'Stuck'),(5,1,6233,'Closing'),(5,1,6246,'Closing'),(5,1,6247,'Opening'),(5,1,6477,'Opening'),(5,1,61437,'Opening'),(5,1,68398,'Opening'),(5,1,96220,'Opening'),(5,1,203,'Unarmed'),(5,1,49410,'Forceful Deflection'),(5,1,17737,'Language Gutterspeak'),(5,1,5227,'Underwater Breathing'),(5,1,7744,'Will of the Forsaken'),(5,1,20577,'Cannibalize'),(5,1,20579,'Shadow Resistance'),(5,1,79747,'Languages'),(5,1,199,'Two-Handed Maces'),(5,1,15590,'Fist Weapons'),(5,1,76268,'Armor Skills'),(5,1,76290,'Weapon Skills'),(5,1,88163,'Attack'),(5,1,200,'Polearms'),(5,1,227,'Staves'),(5,1,9116,'Shield'),(5,1,8737,'Mail'),(5,1,9077,'Leather'),(5,1,264,'Bows'),(5,1,266,'Guns'),(5,1,107,'Block'),(5,1,81,'Dodge'),(5,1,32215,'Victorious State'),(5,1,5301,'Defensive State (DND)'),(5,1,204,'Defense'),(5,1,522,'SPELLDEFENSE (DND)'),(5,1,2764,'Throw'),(5,1,2567,'Thrown'),(5,1,9078,'Cloth'),(5,1,198,'One-Handed Maces'),(5,1,201,'One-Handed Swords'),(5,3,202,'Two-Handed Swords'),(5,3,196,'One-Handed Axes'),(5,3,3044,'Arcane Shot'),(5,3,75,'Auto Shot'),(5,3,82928,'Aimed Shot!'),(5,3,5011,'Crossbows'),(5,3,669,'Language Orcish'),(5,3,197,'Two-Handed Axes'),(5,3,1180,'Daggers'),(5,3,45927,'Summon Friend'),(5,3,6478,'Opening'),(5,3,6603,'Auto Attack'),(5,3,22027,'Remove Insignia'),(5,3,22810,'Opening - No Text'),(5,3,21651,'Opening'),(5,3,21652,'Closing'),(5,3,34082,'Hunter Passive Auras (DND)'),(5,3,2382,'Generic'),(5,3,3365,'Opening'),(5,3,3050,'Detect'),(5,3,9125,'Generic'),(5,3,8386,'Attacking'),(5,3,7266,'Duel'),(5,3,7267,'Grovel'),(5,3,7355,'Stuck'),(5,3,6233,'Closing'),(5,3,6246,'Closing'),(5,3,6247,'Opening'),(5,3,6477,'Opening'),(5,3,61437,'Opening'),(5,3,68398,'Opening'),(5,3,96220,'Opening'),(5,3,203,'Unarmed'),(5,3,883,'Call Pet 1'),(5,3,982,'Revive Pet'),(5,3,17737,'Language Gutterspeak'),(5,3,5227,'Underwater Breathing'),(5,3,7744,'Will of the Forsaken'),(5,3,20577,'Cannibalize'),(5,3,20579,'Shadow Resistance'),(5,3,79747,'Languages'),(5,3,15590,'Fist Weapons'),(5,3,200,'Polearms'),(5,3,227,'Staves'),(5,3,9077,'Leather'),(5,3,264,'Bows'),(5,3,266,'Guns'),(5,3,13358,'Defensive State (DND)'),(5,3,81,'Dodge'),(5,3,204,'Defense'),(5,3,522,'SPELLDEFENSE (DND)'),(5,3,24949,'Defensive State 2 (DND)'),(5,3,9078,'Cloth'),(5,3,77442,'Focus'),(5,3,76249,'Weapon Skills'),(5,3,76250,'Armor Skills'),(5,3,87324,'Focused Aim'),(5,3,87816,'General Hunter Passives'),(5,3,201,'One-Handed Swords'),(5,4,196,'One-Handed Axes'),(5,4,1752,'Sinister Strike'),(5,4,5011,'Crossbows'),(5,4,3018,'Shoot'),(5,4,669,'Language Orcish'),(5,4,674,'Dual Wield'),(5,4,1180,'Daggers'),(5,4,45927,'Summon Friend'),(5,4,6478,'Opening'),(5,4,6603,'Auto Attack'),(5,4,22027,'Remove Insignia'),(5,4,22810,'Opening - No Text'),(5,4,21651,'Opening'),(5,4,21652,'Closing'),(5,4,2382,'Generic'),(5,4,3365,'Opening'),(5,4,3050,'Detect'),(5,4,9125,'Generic'),(5,4,8386,'Attacking'),(5,4,7266,'Duel'),(5,4,7267,'Grovel'),(5,4,7355,'Stuck'),(5,4,6233,'Closing'),(5,4,6246,'Closing'),(5,4,6247,'Opening'),(5,4,6477,'Opening'),(5,4,61437,'Opening'),(5,4,68398,'Opening'),(5,4,96220,'Opening'),(5,4,203,'Unarmed'),(5,4,17737,'Language Gutterspeak'),(5,4,5227,'Underwater Breathing'),(5,4,7744,'Will of the Forsaken'),(5,4,20577,'Cannibalize'),(5,4,20579,'Shadow Resistance'),(5,4,79747,'Languages'),(5,4,15590,'Fist Weapons'),(5,4,76273,'Armor Skills'),(5,4,76297,'Weapon Skills'),(5,4,9077,'Leather'),(5,4,264,'Bows'),(5,4,266,'Guns'),(5,4,81,'Dodge'),(5,4,204,'Defense'),(5,4,522,'SPELLDEFENSE (DND)'),(5,4,16092,'Defensive State (DND)'),(5,4,2764,'Throw'),(5,4,2567,'Thrown'),(5,4,9078,'Cloth'),(5,4,198,'One-Handed Maces'),(5,4,201,'One-Handed Swords'),(5,5,5009,'Wands'),(5,5,5019,'Shoot'),(5,5,669,'Language Orcish'),(5,5,1180,'Daggers'),(5,5,45927,'Summon Friend'),(5,5,6478,'Opening'),(5,5,6603,'Auto Attack'),(5,5,22027,'Remove Insignia'),(5,5,22810,'Opening - No Text'),(5,5,21651,'Opening'),(5,5,21652,'Closing'),(5,5,2382,'Generic'),(5,5,3365,'Opening'),(5,5,3050,'Detect'),(5,5,9125,'Generic'),(5,5,8386,'Attacking'),(5,5,7266,'Duel'),(5,5,7267,'Grovel'),(5,5,7355,'Stuck'),(5,5,6233,'Closing'),(5,5,6246,'Closing'),(5,5,6247,'Opening'),(5,5,6477,'Opening'),(5,5,61437,'Opening'),(5,5,68398,'Opening'),(5,5,96220,'Opening'),(5,5,203,'Unarmed'),(5,5,585,'Smite'),(5,5,84733,'Holy Focus'),(5,5,88685,'Holy Word: Sanctuary'),(5,5,88684,'Holy Word: Serenity'),(5,5,101062,'Flash Heal'),(5,5,17737,'Language Gutterspeak'),(5,5,5227,'Underwater Breathing'),(5,5,7744,'Will of the Forsaken'),(5,5,20577,'Cannibalize'),(5,5,20579,'Shadow Resistance'),(5,5,79747,'Languages'),(5,5,76301,'Weapon Skills'),(5,5,76279,'Armor Skills'),(5,5,84734,'Dark Thoughts'),(5,5,227,'Staves'),(5,5,77486,'Shadow Orb Power'),(5,5,81,'Dodge'),(5,5,204,'Defense'),(5,5,522,'SPELLDEFENSE (DND)'),(5,5,9078,'Cloth'),(5,5,198,'One-Handed Maces'),(9,8,71761,'Deep Freeze Immunity State'),(5,8,5009,'Wands'),(5,8,5019,'Shoot'),(5,8,669,'Language Orcish'),(5,8,1180,'Daggers'),(5,8,45927,'Summon Friend'),(5,8,6478,'Opening'),(5,8,6603,'Auto Attack'),(5,8,22027,'Remove Insignia'),(5,8,22810,'Opening - No Text'),(5,8,21651,'Opening'),(5,8,21652,'Closing'),(5,8,2382,'Generic'),(5,8,3365,'Opening'),(5,8,3050,'Detect'),(5,8,9125,'Generic'),(5,8,8386,'Attacking'),(5,8,7266,'Duel'),(5,8,7267,'Grovel'),(5,8,7355,'Stuck'),(5,8,6233,'Closing'),(5,8,6246,'Closing'),(5,8,6247,'Opening'),(5,8,6477,'Opening'),(5,8,61437,'Opening'),(5,8,68398,'Opening'),(5,8,96220,'Opening'),(5,8,79684,'Offensive State (DND)'),(5,8,203,'Unarmed'),(5,8,17737,'Language Gutterspeak'),(5,8,5227,'Underwater Breathing'),(5,8,7744,'Will of the Forsaken'),(5,8,20577,'Cannibalize'),(5,8,20579,'Shadow Resistance'),(5,8,79747,'Languages'),(5,8,133,'Fireball'),(5,8,92315,'Pyroblast!'),(5,8,85801,'DPS Caster Crit Damage Bonus'),(5,8,76298,'Weapon Skills'),(5,8,76276,'Armor Skills'),(5,8,227,'Staves'),(5,8,81,'Dodge'),(5,8,204,'Defense'),(5,8,522,'SPELLDEFENSE (DND)'),(5,8,9078,'Cloth'),(5,8,201,'One-Handed Swords'),(5,9,5009,'Wands'),(5,9,5019,'Shoot'),(5,9,669,'Language Common'),(5,9,1180,'Daggers'),(5,9,45927,'Summon Friend'),(5,9,6478,'Opening'),(5,9,6603,'Auto Attack'),(5,9,22027,'Remove Insignia'),(5,9,22810,'Opening - No Text'),(5,9,21651,'Opening'),(5,9,21652,'Closing'),(5,9,2382,'Generic'),(5,9,3365,'Opening'),(5,9,3050,'Detect'),(5,9,9125,'Generic'),(5,9,8386,'Attacking'),(5,9,7266,'Duel'),(5,9,7267,'Grovel'),(5,9,7355,'Stuck'),(5,9,6233,'Closing'),(5,9,6246,'Closing'),(5,9,6247,'Opening'),(5,9,6477,'Opening'),(5,9,61437,'Opening'),(5,9,68398,'Opening'),(5,9,96220,'Opening'),(5,9,203,'Unarmed'),(5,9,17747,'Language Gutterspeak'),(5,9,5227,'Underwater Breathing'),(5,9,7744,'Will of the Forsaken'),(5,9,20577,'Cannibalize'),(5,9,20579,'Shadow Resistance'),(5,9,79747,'Languages'),(5,9,688,'Summon Imp'),(5,9,86213,'Soul Swap Exhale'),(5,9,89420,'Drain Life'),(5,9,85801,'DPS Caster Crit Damage Bonus'),(5,9,76299,'Weapon Skills'),(5,9,76277,'Armor Skills'),(5,9,87330,'Suppression'),(5,9,227,'Staves'),(5,9,75445,'Demonic Immolate'),(5,9,686,'Shadow Bolt'),(5,9,58284,'Chaos Bolt Passive'),(5,9,81,'Dodge'),(5,9,204,'Defense'),(5,9,522,'SPELLDEFENSE (DND)'),(5,9,9078,'Cloth'),(5,9,201,'One-Handed Swords'),(6,1,202,'Two-Handed Swords'),(6,1,196,'One-Handed Axes'),(6,1,2457,'Battle Stance'),(6,1,88161,'Strike'),(6,1,5011,'Crossbows'),(6,1,3018,'Shoot'),(6,1,669,'Language Orcish'),(6,1,197,'Two-Handed Axes'),(6,1,1180,'Daggers'),(6,1,45927,'Summon Friend'),(6,1,6478,'Opening'),(6,1,6603,'Auto Attack'),(6,1,22027,'Remove Insignia'),(6,1,22810,'Opening - No Text'),(6,1,21651,'Opening'),(6,1,21652,'Closing'),(6,1,2382,'Generic'),(6,1,3365,'Opening'),(6,1,3050,'Detect'),(6,1,9125,'Generic'),(6,1,8386,'Attacking'),(6,1,7266,'Duel'),(6,1,7267,'Grovel'),(6,1,7355,'Stuck'),(6,1,6233,'Closing'),(6,1,6246,'Closing'),(6,1,6247,'Opening'),(6,1,6477,'Opening'),(6,1,61437,'Opening'),(6,1,68398,'Opening'),(6,1,96220,'Opening'),(6,1,203,'Unarmed'),(6,1,670,'Language Taurahe'),(6,1,49410,'Forceful Deflection'),(6,1,20549,'War Stomp'),(6,1,20550,'Endurance'),(6,1,20551,'Nature Resistance'),(6,1,20552,'Cultivation'),(6,1,79746,'Languages'),(6,1,199,'Two-Handed Maces'),(6,1,15590,'Fist Weapons'),(6,1,76268,'Armor Skills'),(6,1,76290,'Weapon Skills'),(6,1,88163,'Attack'),(6,1,200,'Polearms'),(6,1,227,'Staves'),(6,1,9116,'Shield'),(6,1,8737,'Mail'),(6,1,9077,'Leather'),(6,1,264,'Bows'),(6,1,266,'Guns'),(6,1,107,'Block'),(6,1,81,'Dodge'),(6,1,32215,'Victorious State'),(6,1,5301,'Defensive State (DND)'),(6,1,204,'Defense'),(6,1,522,'SPELLDEFENSE (DND)'),(6,1,2764,'Throw'),(6,1,2567,'Thrown'),(6,1,9078,'Cloth'),(6,1,198,'One-Handed Maces'),(6,1,201,'One-Handed Swords'),(6,2,202,'Two-Handed Swords'),(6,2,196,'One-Handed Axes'),(6,2,669,'Language Orcish'),(6,2,197,'Two-Handed Axes'),(6,2,45927,'Summon Friend'),(6,2,6478,'Opening'),(6,2,6603,'Auto Attack'),(6,2,22027,'Remove Insignia'),(6,2,22810,'Opening - No Text'),(6,2,21651,'Opening'),(6,2,21652,'Closing'),(6,2,2382,'Generic'),(6,2,3365,'Opening'),(6,2,3050,'Detect'),(6,2,9125,'Generic'),(6,2,8386,'Attacking'),(6,2,7266,'Duel'),(6,2,7267,'Grovel'),(6,2,7355,'Stuck'),(6,2,6233,'Closing'),(6,2,6246,'Closing'),(6,2,6247,'Opening'),(6,2,6477,'Opening'),(6,2,27762,'Relic'),(6,2,61437,'Opening'),(6,2,68398,'Opening'),(6,2,96220,'Opening'),(6,2,49410,'Forceful Deflection'),(6,2,203,'Unarmed'),(6,2,20549,'War Stomp'),(6,2,20550,'Endurance'),(6,2,20551,'Nature Resistance'),(6,2,20552,'Cultivation'),(6,2,79746,'Languages'),(6,2,199,'Two-Handed Maces'),(6,2,76271,'Armor Skills'),(6,2,76294,'Weapon Skills'),(6,2,200,'Polearms'),(6,2,670,'Language Taurahe'),(6,2,35395,'Crusader Strike'),(6,2,60091,'Judgement Anti-Parry/Dodge Passive'),(6,2,9116,'Shield'),(6,2,8737,'Mail'),(6,2,9077,'Leather'),(6,2,107,'Block'),(6,2,81,'Dodge'),(6,2,204,'Defense'),(6,2,522,'SPELLDEFENSE (DND)'),(6,2,9078,'Cloth'),(6,2,20208,'Paladin pushback resistance'),(6,2,198,'One-Handed Maces'),(6,2,201,'One-Handed Swords'),(6,3,202,'Two-Handed Swords'),(6,3,196,'One-Handed Axes'),(6,3,3044,'Arcane Shot'),(6,3,75,'Auto Shot'),(6,3,82928,'Aimed Shot!'),(6,3,5011,'Crossbows'),(6,3,669,'Language Orcish'),(6,3,197,'Two-Handed Axes'),(6,3,1180,'Daggers'),(6,3,45927,'Summon Friend'),(6,3,6478,'Opening'),(6,3,6603,'Auto Attack'),(6,3,22027,'Remove Insignia'),(6,3,22810,'Opening - No Text'),(6,3,21651,'Opening'),(6,3,21652,'Closing'),(6,3,34082,'Hunter Passive Auras (DND)'),(6,3,2382,'Generic'),(6,3,3365,'Opening'),(6,3,3050,'Detect'),(6,3,9125,'Generic'),(6,3,8386,'Attacking'),(6,3,7266,'Duel'),(6,3,7267,'Grovel'),(6,3,7355,'Stuck'),(6,3,6233,'Closing'),(6,3,6246,'Closing'),(6,3,6247,'Opening'),(6,3,6477,'Opening'),(6,3,61437,'Opening'),(6,3,68398,'Opening'),(6,3,96220,'Opening'),(6,3,203,'Unarmed'),(6,3,883,'Call Pet 1'),(6,3,982,'Revive Pet'),(6,3,20549,'War Stomp'),(6,3,20550,'Endurance'),(6,3,20551,'Nature Resistance'),(6,3,20552,'Cultivation'),(6,3,79746,'Languages'),(6,3,15590,'Fist Weapons'),(6,3,200,'Polearms'),(6,3,227,'Staves'),(6,3,9077,'Leather'),(6,3,264,'Bows'),(6,3,266,'Guns'),(6,3,670,'Language Taurahe'),(6,3,13358,'Defensive State (DND)'),(6,3,81,'Dodge'),(6,3,204,'Defense'),(6,3,522,'SPELLDEFENSE (DND)'),(6,3,24949,'Defensive State 2 (DND)'),(6,3,9078,'Cloth'),(6,3,77442,'Focus'),(6,3,76249,'Weapon Skills'),(6,3,76250,'Armor Skills'),(6,3,87324,'Focused Aim'),(6,3,87816,'General Hunter Passives'),(6,3,201,'One-Handed Swords'),(6,5,5009,'Wands'),(6,5,5019,'Shoot'),(6,5,669,'Language Orcish'),(6,5,1180,'Daggers'),(6,5,45927,'Summon Friend'),(6,5,6478,'Opening'),(6,5,6603,'Auto Attack'),(6,5,22027,'Remove Insignia'),(6,5,22810,'Opening - No Text'),(6,5,21651,'Opening'),(6,5,21652,'Closing'),(6,5,2382,'Generic'),(6,5,3365,'Opening'),(6,5,3050,'Detect'),(6,5,9125,'Generic'),(6,5,8386,'Attacking'),(6,5,7266,'Duel'),(6,5,7267,'Grovel'),(6,5,7355,'Stuck'),(6,5,6233,'Closing'),(6,5,6246,'Closing'),(6,5,6247,'Opening'),(6,5,6477,'Opening'),(6,5,61437,'Opening'),(6,5,68398,'Opening'),(6,5,96220,'Opening'),(6,5,203,'Unarmed'),(6,5,585,'Smite'),(6,5,84733,'Holy Focus'),(6,5,88685,'Holy Word: Sanctuary'),(6,5,88684,'Holy Word: Serenity'),(6,5,101062,'Flash Heal'),(6,5,20549,'War Stomp'),(6,5,20550,'Endurance'),(6,5,20551,'Nature Resistance'),(6,5,20552,'Cultivation'),(6,5,79746,'Languages'),(6,5,670,'Language Taurahe'),(6,5,76301,'Weapon Skills'),(6,5,76279,'Armor Skills'),(6,5,84734,'Dark Thoughts'),(6,5,227,'Staves'),(6,5,77486,'Shadow Orb Power'),(6,5,81,'Dodge'),(6,5,204,'Defense'),(6,5,522,'SPELLDEFENSE (DND)'),(6,5,9078,'Cloth'),(6,5,198,'One-Handed Maces'),(8,8,71761,'Deep Freeze Immunity State'),(6,8,5009,'Wands'),(6,8,5019,'Shoot'),(6,8,669,'Language Orcish'),(6,8,1180,'Daggers'),(6,8,45927,'Summon Friend'),(6,8,6478,'Opening'),(6,8,6603,'Auto Attack'),(6,8,22027,'Remove Insignia'),(6,8,22810,'Opening - No Text'),(6,8,21651,'Opening'),(6,8,21652,'Closing'),(6,8,2382,'Generic'),(6,8,3365,'Opening'),(6,8,3050,'Detect'),(6,8,9125,'Generic'),(6,8,8386,'Attacking'),(6,8,7266,'Duel'),(6,8,7267,'Grovel'),(6,8,7355,'Stuck'),(6,8,6233,'Closing'),(6,8,6246,'Closing'),(6,8,6247,'Opening'),(6,8,6477,'Opening'),(6,8,61437,'Opening'),(6,8,68398,'Opening'),(6,8,96220,'Opening'),(6,8,79684,'Offensive State (DND)'),(6,8,203,'Unarmed'),(6,8,670,'Language Taurahe'),(6,8,20549,'War Stomp'),(6,8,20550,'Endurance'),(6,8,20551,'Nature Resistance'),(6,8,20552,'Cultivation'),(6,8,79746,'Languages'),(6,8,133,'Fireball'),(6,8,92315,'Pyroblast!'),(6,8,85801,'DPS Caster Crit Damage Bonus'),(6,8,76298,'Weapon Skills'),(6,8,76276,'Armor Skills'),(6,8,227,'Staves'),(6,8,81,'Dodge'),(6,8,204,'Defense'),(6,8,522,'SPELLDEFENSE (DND)'),(6,8,9078,'Cloth'),(6,8,201,'One-Handed Swords'),(6,9,5009,'Wands'),(6,9,5019,'Shoot'),(6,9,669,'Language Common'),(6,9,1180,'Daggers'),(6,9,45927,'Summon Friend'),(6,9,6478,'Opening'),(6,9,6603,'Auto Attack'),(6,9,22027,'Remove Insignia'),(6,9,22810,'Opening - No Text'),(6,9,21651,'Opening'),(6,9,21652,'Closing'),(6,9,2382,'Generic'),(6,9,3365,'Opening'),(6,9,3050,'Detect'),(6,9,9125,'Generic'),(6,9,8386,'Attacking'),(6,9,7266,'Duel'),(6,9,7267,'Grovel'),(6,9,7355,'Stuck'),(6,9,6233,'Closing'),(6,9,6246,'Closing'),(6,9,6247,'Opening'),(6,9,6477,'Opening'),(6,9,61437,'Opening'),(6,9,68398,'Opening'),(6,9,96220,'Opening'),(6,9,203,'Unarmed'),(6,9,670,'Language Taurahe'),(6,9,20549,'War Stomp'),(6,9,20550,'Endurance'),(6,9,20551,'Nature Resistance'),(6,9,20552,'Cultivation'),(6,9,79746,'Languages'),(6,9,688,'Summon Imp'),(6,9,86213,'Soul Swap Exhale'),(6,9,89420,'Drain Life'),(6,9,85801,'DPS Caster Crit Damage Bonus'),(6,9,76299,'Weapon Skills'),(6,9,76277,'Armor Skills'),(6,9,87330,'Suppression'),(6,9,227,'Staves'),(6,9,75445,'Demonic Immolate'),(6,9,686,'Shadow Bolt'),(6,9,58284,'Chaos Bolt Passive'),(6,9,81,'Dodge'),(6,9,204,'Defense'),(6,9,522,'SPELLDEFENSE (DND)'),(6,9,9078,'Cloth'),(6,9,201,'One-Handed Swords'),(6,11,669,'Language Orcish'),(6,11,1180,'Daggers'),(6,11,45927,'Summon Friend'),(6,11,6478,'Opening'),(6,11,6603,'Auto Attack'),(6,11,22027,'Remove Insignia'),(6,11,22810,'Opening - No Text'),(6,11,21651,'Opening'),(6,11,21652,'Closing'),(6,11,2382,'Generic'),(6,11,3365,'Opening'),(6,11,3050,'Detect'),(6,11,9125,'Generic'),(6,11,8386,'Attacking'),(6,11,7266,'Duel'),(6,11,7267,'Grovel'),(6,11,7355,'Stuck'),(6,11,6233,'Closing'),(6,11,6246,'Closing'),(6,11,6247,'Opening'),(6,11,6477,'Opening'),(6,11,61437,'Opening'),(6,11,68398,'Opening'),(6,11,96220,'Opening'),(6,11,203,'Unarmed'),(6,11,670,'Language Taurahe'),(6,11,20549,'War Stomp'),(6,11,20550,'Endurance'),(6,11,20551,'Nature Resistance'),(6,11,20552,'Cultivation'),(6,11,79746,'Languages'),(6,11,84736,'Nature\'s Focus'),(6,11,81170,'Ravage!'),(6,11,79577,'Eclipse Mastery Driver Passive'),(6,11,76300,'Weapon Skills'),(6,11,76275,'Armor Skills'),(6,11,5176,'Wrath'),(6,11,199,'Two-Handed Maces'),(6,11,15590,'Fist Weapons'),(6,11,84738,'Celestial Focus'),(6,11,200,'Polearms'),(6,11,227,'Staves'),(6,11,81,'Dodge'),(6,11,204,'Defense'),(6,11,522,'SPELLDEFENSE (DND)'),(6,11,9077,'Leather'),(6,11,9078,'Cloth'),(6,11,198,'One-Handed Maces'),(7,1,202,'Two-Handed Swords'),(7,1,196,'One-Handed Axes'),(7,1,20591,'Expansive Mind'),(7,1,20593,'Engineering Specialization'),(7,1,20592,'Arcane Resistance'),(7,1,92680,'Shortblade Specialization'),(7,1,79740,'Languages'),(7,1,7340,'Language Gnomish'),(7,1,2457,'Battle Stance'),(7,1,88161,'Strike'),(7,1,5011,'Crossbows'),(7,1,3018,'Shoot'),(7,1,197,'Two-Handed Axes'),(7,1,1180,'Daggers'),(7,1,45927,'Summon Friend'),(7,1,6478,'Opening'),(7,1,6603,'Auto Attack'),(7,1,22027,'Remove Insignia'),(7,1,22810,'Opening - No Text'),(7,1,21651,'Opening'),(7,1,21652,'Closing'),(7,1,2382,'Generic'),(7,1,3365,'Opening'),(7,1,3050,'Detect'),(7,1,9125,'Generic'),(7,1,8386,'Attacking'),(7,1,7266,'Duel'),(7,1,7267,'Grovel'),(7,1,7355,'Stuck'),(7,1,6233,'Closing'),(7,1,6246,'Closing'),(7,1,6247,'Opening'),(7,1,6477,'Opening'),(7,1,61437,'Opening'),(7,1,68398,'Opening'),(7,1,96220,'Opening'),(7,1,203,'Unarmed'),(7,1,49410,'Forceful Deflection'),(7,1,199,'Two-Handed Maces'),(7,1,15590,'Fist Weapons'),(7,1,76268,'Armor Skills'),(7,1,76290,'Weapon Skills'),(7,1,88163,'Attack'),(7,1,200,'Polearms'),(7,1,668,'Language Common'),(7,1,227,'Staves'),(7,1,9116,'Shield'),(7,1,8737,'Mail'),(7,1,9077,'Leather'),(7,1,264,'Bows'),(7,1,266,'Guns'),(7,1,107,'Block'),(7,1,81,'Dodge'),(7,1,32215,'Victorious State'),(7,1,5301,'Defensive State (DND)'),(7,1,204,'Defense'),(7,1,522,'SPELLDEFENSE (DND)'),(7,1,2764,'Throw'),(7,1,2567,'Thrown'),(7,1,9078,'Cloth'),(7,1,198,'One-Handed Maces'),(7,1,201,'One-Handed Swords'),(7,4,196,'One-Handed Axes'),(7,4,20591,'Expansive Mind'),(7,4,20593,'Engineering Specialization'),(7,4,20592,'Arcane Resistance'),(7,4,92680,'Shortblade Specialization'),(7,4,79740,'Languages'),(7,4,7340,'Language Gnomish'),(7,4,1752,'Sinister Strike'),(7,4,5011,'Crossbows'),(7,4,3018,'Shoot'),(7,4,674,'Dual Wield'),(7,4,1180,'Daggers'),(7,4,45927,'Summon Friend'),(7,4,6478,'Opening'),(7,4,6603,'Auto Attack'),(7,4,22027,'Remove Insignia'),(7,4,22810,'Opening - No Text'),(7,4,21651,'Opening'),(7,4,21652,'Closing'),(7,4,2382,'Generic'),(7,4,3365,'Opening'),(7,4,3050,'Detect'),(7,4,9125,'Generic'),(7,4,8386,'Attacking'),(7,4,7266,'Duel'),(7,4,7267,'Grovel'),(7,4,7355,'Stuck'),(7,4,6233,'Closing'),(7,4,6246,'Closing'),(7,4,6247,'Opening'),(7,4,6477,'Opening'),(7,4,61437,'Opening'),(7,4,68398,'Opening'),(7,4,96220,'Opening'),(7,4,203,'Unarmed'),(7,4,15590,'Fist Weapons'),(7,4,76273,'Armor Skills'),(7,4,76297,'Weapon Skills'),(7,4,668,'Language Common'),(7,4,9077,'Leather'),(7,4,264,'Bows'),(7,4,266,'Guns'),(7,4,81,'Dodge'),(7,4,204,'Defense'),(7,4,522,'SPELLDEFENSE (DND)'),(7,4,16092,'Defensive State (DND)'),(7,4,2764,'Throw'),(7,4,2567,'Thrown'),(7,4,9078,'Cloth'),(7,4,198,'One-Handed Maces'),(7,4,201,'One-Handed Swords'),(7,5,20591,'Expansive Mind'),(7,5,20593,'Engineering Specialization'),(7,5,20592,'Arcane Resistance'),(7,5,92680,'Shortblade Specialization'),(7,5,79740,'Languages'),(7,5,7340,'Language Gnomish'),(7,5,5009,'Wands'),(7,5,5019,'Shoot'),(7,5,1180,'Daggers'),(7,5,45927,'Summon Friend'),(7,5,6478,'Opening'),(7,5,6603,'Auto Attack'),(7,5,22027,'Remove Insignia'),(7,5,22810,'Opening - No Text'),(7,5,21651,'Opening'),(7,5,21652,'Closing'),(7,5,2382,'Generic'),(7,5,3365,'Opening'),(7,5,3050,'Detect'),(7,5,9125,'Generic'),(7,5,8386,'Attacking'),(7,5,7266,'Duel'),(7,5,7267,'Grovel'),(7,5,7355,'Stuck'),(7,5,6233,'Closing'),(7,5,6246,'Closing'),(7,5,6247,'Opening'),(7,5,6477,'Opening'),(7,5,61437,'Opening'),(7,5,68398,'Opening'),(7,5,96220,'Opening'),(7,5,203,'Unarmed'),(7,5,585,'Smite'),(7,5,84733,'Holy Focus'),(7,5,88685,'Holy Word: Sanctuary'),(7,5,88684,'Holy Word: Serenity'),(7,5,101062,'Flash Heal'),(7,5,76301,'Weapon Skills'),(7,5,76279,'Armor Skills'),(7,5,84734,'Dark Thoughts'),(7,5,668,'Language Common'),(7,5,227,'Staves'),(7,5,77486,'Shadow Orb Power'),(7,5,81,'Dodge'),(7,5,204,'Defense'),(7,5,522,'SPELLDEFENSE (DND)'),(7,5,9078,'Cloth'),(7,5,198,'One-Handed Maces'),(7,8,71761,'Deep Freeze Immunity State'),(7,8,20591,'Expansive Mind'),(7,8,20593,'Engineering Specialization'),(7,8,20592,'Arcane Resistance'),(7,8,92680,'Shortblade Specialization'),(7,8,79740,'Languages'),(7,8,7340,'Language Gnomish'),(7,8,5009,'Wands'),(7,8,5019,'Shoot'),(7,8,1180,'Daggers'),(7,8,45927,'Summon Friend'),(7,8,6478,'Opening'),(7,8,6603,'Auto Attack'),(7,8,22027,'Remove Insignia'),(7,8,22810,'Opening - No Text'),(7,8,21651,'Opening'),(7,8,21652,'Closing'),(7,8,2382,'Generic'),(7,8,3365,'Opening'),(7,8,3050,'Detect'),(7,8,9125,'Generic'),(7,8,8386,'Attacking'),(7,8,7266,'Duel'),(7,8,7267,'Grovel'),(7,8,7355,'Stuck'),(7,8,6233,'Closing'),(7,8,6246,'Closing'),(7,8,6247,'Opening'),(7,8,6477,'Opening'),(7,8,61437,'Opening'),(7,8,68398,'Opening'),(7,8,96220,'Opening'),(7,8,79684,'Offensive State (DND)'),(7,8,203,'Unarmed'),(7,8,133,'Fireball'),(7,8,92315,'Pyroblast!'),(7,8,85801,'DPS Caster Crit Damage Bonus'),(7,8,76298,'Weapon Skills'),(7,8,76276,'Armor Skills'),(7,8,668,'Language Common'),(7,8,227,'Staves'),(7,8,81,'Dodge'),(7,8,204,'Defense'),(7,8,522,'SPELLDEFENSE (DND)'),(7,8,9078,'Cloth'),(7,8,201,'One-Handed Swords'),(7,9,20591,'Expansive Mind'),(7,9,20593,'Engineering Specialization'),(7,9,20592,'Arcane Resistance'),(7,9,92680,'Shortblade Specialization'),(7,9,79740,'Languages'),(7,9,7340,'Language Gnomish'),(7,9,5009,'Wands'),(7,9,5019,'Shoot'),(7,9,1180,'Daggers'),(7,9,45927,'Summon Friend'),(7,9,6478,'Opening'),(7,9,6603,'Auto Attack'),(7,9,22027,'Remove Insignia'),(7,9,22810,'Opening - No Text'),(7,9,21651,'Opening'),(7,9,21652,'Closing'),(7,9,2382,'Generic'),(7,9,3365,'Opening'),(7,9,3050,'Detect'),(7,9,9125,'Generic'),(7,9,8386,'Attacking'),(7,9,7266,'Duel'),(7,9,7267,'Grovel'),(7,9,7355,'Stuck'),(7,9,6233,'Closing'),(7,9,6246,'Closing'),(7,9,6247,'Opening'),(7,9,6477,'Opening'),(7,9,61437,'Opening'),(7,9,68398,'Opening'),(7,9,96220,'Opening'),(7,9,203,'Unarmed'),(7,9,688,'Summon Imp'),(7,9,86213,'Soul Swap Exhale'),(7,9,89420,'Drain Life'),(7,9,85801,'DPS Caster Crit Damage Bonus'),(7,9,76299,'Weapon Skills'),(7,9,76277,'Armor Skills'),(7,9,87330,'Suppression'),(7,9,668,'Language Common'),(7,9,227,'Staves'),(7,9,75445,'Demonic Immolate'),(7,9,686,'Shadow Bolt'),(7,9,58284,'Chaos Bolt Passive'),(7,9,81,'Dodge'),(7,9,204,'Defense'),(7,9,522,'SPELLDEFENSE (DND)'),(7,9,9078,'Cloth'),(7,9,201,'One-Handed Swords'),(8,1,202,'Two-Handed Swords'),(8,1,7341,'Language Troll'),(8,1,26290,'Bow Specialization'),(8,1,26297,'Berserking'),(8,1,58943,'Da Voodoo Shuffle'),(8,1,20555,'Regeneration'),(8,1,20557,'Beast Slaying'),(8,1,20558,'Throwing Specialization'),(8,1,79744,'Languages'),(8,1,196,'One-Handed Axes'),(8,1,2457,'Battle Stance'),(8,1,88161,'Strike'),(8,1,5011,'Crossbows'),(8,1,3018,'Shoot'),(8,1,669,'Language Orcish'),(8,1,197,'Two-Handed Axes'),(8,1,1180,'Daggers'),(8,1,45927,'Summon Friend'),(8,1,6478,'Opening'),(8,1,6603,'Auto Attack'),(8,1,22027,'Remove Insignia'),(8,1,22810,'Opening - No Text'),(8,1,21651,'Opening'),(8,1,21652,'Closing'),(8,1,2382,'Generic'),(8,1,3365,'Opening'),(8,1,3050,'Detect'),(8,1,9125,'Generic'),(8,1,8386,'Attacking'),(8,1,7266,'Duel'),(8,1,7267,'Grovel'),(8,1,7355,'Stuck'),(8,1,6233,'Closing'),(8,1,6246,'Closing'),(8,1,6247,'Opening'),(8,1,6477,'Opening'),(8,1,61437,'Opening'),(8,1,68398,'Opening'),(8,1,96220,'Opening'),(8,1,203,'Unarmed'),(8,1,49410,'Forceful Deflection'),(8,1,199,'Two-Handed Maces'),(8,1,15590,'Fist Weapons'),(8,1,76268,'Armor Skills'),(8,1,76290,'Weapon Skills'),(8,1,88163,'Attack'),(8,1,200,'Polearms'),(8,1,227,'Staves'),(8,1,9116,'Shield'),(8,1,8737,'Mail'),(8,1,9077,'Leather'),(8,1,264,'Bows'),(8,1,266,'Guns'),(8,1,107,'Block'),(8,1,81,'Dodge'),(8,1,32215,'Victorious State'),(8,1,5301,'Defensive State (DND)'),(8,1,204,'Defense'),(8,1,522,'SPELLDEFENSE (DND)'),(8,1,2764,'Throw'),(8,1,2567,'Thrown'),(8,1,9078,'Cloth'),(8,1,198,'One-Handed Maces'),(8,1,201,'One-Handed Swords'),(8,3,202,'Two-Handed Swords'),(8,3,7341,'Language Troll'),(8,3,26290,'Bow Specialization'),(8,3,26297,'Berserking'),(8,3,58943,'Da Voodoo Shuffle'),(8,3,20555,'Regeneration'),(8,3,20557,'Beast Slaying'),(8,3,20558,'Throwing Specialization'),(8,3,79744,'Languages'),(8,3,196,'One-Handed Axes'),(8,3,3044,'Arcane Shot'),(8,3,75,'Auto Shot'),(8,3,82928,'Aimed Shot!'),(8,3,5011,'Crossbows'),(8,3,669,'Language Orcish'),(8,3,197,'Two-Handed Axes'),(8,3,1180,'Daggers'),(8,3,45927,'Summon Friend'),(8,3,6478,'Opening'),(8,3,6603,'Auto Attack'),(8,3,22027,'Remove Insignia'),(8,3,22810,'Opening - No Text'),(8,3,21651,'Opening'),(8,3,21652,'Closing'),(8,3,34082,'Hunter Passive Auras (DND)'),(8,3,2382,'Generic'),(8,3,3365,'Opening'),(8,3,3050,'Detect'),(8,3,9125,'Generic'),(8,3,8386,'Attacking'),(8,3,7266,'Duel'),(8,3,7267,'Grovel'),(8,3,7355,'Stuck'),(8,3,6233,'Closing'),(8,3,6246,'Closing'),(8,3,6247,'Opening'),(8,3,6477,'Opening'),(8,3,61437,'Opening'),(8,3,68398,'Opening'),(8,3,96220,'Opening'),(8,3,203,'Unarmed'),(8,3,883,'Call Pet 1'),(8,3,982,'Revive Pet'),(8,3,15590,'Fist Weapons'),(8,3,200,'Polearms'),(8,3,227,'Staves'),(8,3,9077,'Leather'),(8,3,264,'Bows'),(8,3,266,'Guns'),(8,3,13358,'Defensive State (DND)'),(8,3,81,'Dodge'),(8,3,204,'Defense'),(8,3,522,'SPELLDEFENSE (DND)'),(8,3,24949,'Defensive State 2 (DND)'),(8,3,9078,'Cloth'),(8,3,77442,'Focus'),(8,3,76249,'Weapon Skills'),(8,3,76250,'Armor Skills'),(8,3,87324,'Focused Aim'),(8,3,87816,'General Hunter Passives'),(8,3,201,'One-Handed Swords'),(8,4,7341,'Language Troll'),(8,4,26290,'Bow Specialization'),(8,4,26297,'Berserking'),(8,4,58943,'Da Voodoo Shuffle'),(8,4,20555,'Regeneration'),(8,4,20557,'Beast Slaying'),(8,4,20558,'Throwing Specialization'),(8,4,79744,'Languages'),(8,4,196,'One-Handed Axes'),(8,4,1752,'Sinister Strike'),(8,4,5011,'Crossbows'),(8,4,3018,'Shoot'),(8,4,669,'Language Orcish'),(8,4,674,'Dual Wield'),(8,4,1180,'Daggers'),(8,4,45927,'Summon Friend'),(8,4,6478,'Opening'),(8,4,6603,'Auto Attack'),(8,4,22027,'Remove Insignia'),(8,4,22810,'Opening - No Text'),(8,4,21651,'Opening'),(8,4,21652,'Closing'),(8,4,2382,'Generic'),(8,4,3365,'Opening'),(8,4,3050,'Detect'),(8,4,9125,'Generic'),(8,4,8386,'Attacking'),(8,4,7266,'Duel'),(8,4,7267,'Grovel'),(8,4,7355,'Stuck'),(8,4,6233,'Closing'),(8,4,6246,'Closing'),(8,4,6247,'Opening'),(8,4,6477,'Opening'),(8,4,61437,'Opening'),(8,4,68398,'Opening'),(8,4,96220,'Opening'),(8,4,203,'Unarmed'),(8,4,15590,'Fist Weapons'),(8,4,76273,'Armor Skills'),(8,4,76297,'Weapon Skills'),(8,4,9077,'Leather'),(8,4,264,'Bows'),(8,4,266,'Guns'),(8,4,81,'Dodge'),(8,4,204,'Defense'),(8,4,522,'SPELLDEFENSE (DND)'),(8,4,16092,'Defensive State (DND)'),(8,4,2764,'Throw'),(8,4,2567,'Thrown'),(8,4,9078,'Cloth'),(8,4,198,'One-Handed Maces'),(8,4,201,'One-Handed Swords'),(8,5,7341,'Language Troll'),(8,5,26290,'Bow Specialization'),(8,5,26297,'Berserking'),(8,5,58943,'Da Voodoo Shuffle'),(8,5,20555,'Regeneration'),(8,5,20557,'Beast Slaying'),(8,5,20558,'Throwing Specialization'),(8,5,79744,'Languages'),(8,5,5009,'Wands'),(8,5,5019,'Shoot'),(8,5,669,'Language Orcish'),(8,5,1180,'Daggers'),(8,5,45927,'Summon Friend'),(8,5,6478,'Opening'),(8,5,6603,'Auto Attack'),(8,5,22027,'Remove Insignia'),(8,5,22810,'Opening - No Text'),(8,5,21651,'Opening'),(8,5,21652,'Closing'),(8,5,2382,'Generic'),(8,5,3365,'Opening'),(8,5,3050,'Detect'),(8,5,9125,'Generic'),(8,5,8386,'Attacking'),(8,5,7266,'Duel'),(8,5,7267,'Grovel'),(8,5,7355,'Stuck'),(8,5,6233,'Closing'),(8,5,6246,'Closing'),(8,5,6247,'Opening'),(8,5,6477,'Opening'),(8,5,61437,'Opening'),(8,5,68398,'Opening'),(8,5,96220,'Opening'),(8,5,203,'Unarmed'),(8,5,585,'Smite'),(8,5,84733,'Holy Focus'),(8,5,88685,'Holy Word: Sanctuary'),(8,5,88684,'Holy Word: Serenity'),(8,5,101062,'Flash Heal'),(8,5,76301,'Weapon Skills'),(8,5,76279,'Armor Skills'),(8,5,84734,'Dark Thoughts'),(8,5,227,'Staves'),(8,5,77486,'Shadow Orb Power'),(8,5,81,'Dodge'),(8,5,204,'Defense'),(8,5,522,'SPELLDEFENSE (DND)'),(8,5,9078,'Cloth'),(8,5,198,'One-Handed Maces'),(8,7,196,'One-Handed Axes'),(8,7,7341,'Language Troll'),(8,7,26290,'Bow Specialization'),(8,7,26297,'Berserking'),(8,7,58943,'Da Voodoo Shuffle'),(8,7,20555,'Regeneration'),(8,7,20557,'Beast Slaying'),(8,7,20558,'Throwing Specialization'),(8,7,79744,'Languages'),(8,7,669,'Language Orcish'),(8,7,197,'Two-Handed Axes'),(8,7,1180,'Daggers'),(8,7,45927,'Summon Friend'),(8,7,6478,'Opening'),(8,7,6603,'Auto Attack'),(8,7,22027,'Remove Insignia'),(8,7,22810,'Opening - No Text'),(8,7,21651,'Opening'),(8,7,21652,'Closing'),(8,7,2382,'Generic'),(8,7,3365,'Opening'),(8,7,3050,'Detect'),(8,7,9125,'Generic'),(8,7,8386,'Attacking'),(8,7,7266,'Duel'),(8,7,7267,'Grovel'),(8,7,7355,'Stuck'),(8,7,6233,'Closing'),(8,7,6246,'Closing'),(8,7,6247,'Opening'),(8,7,6477,'Opening'),(8,7,27763,'Relic'),(8,7,61437,'Opening'),(8,7,68398,'Opening'),(8,7,96220,'Opening'),(8,7,203,'Unarmed'),(8,7,199,'Two-Handed Maces'),(8,7,15590,'Fist Weapons'),(8,7,76272,'Armor Skills'),(8,7,76296,'Weapon Skills'),(8,7,89920,'Ancestral Focus'),(8,7,227,'Staves'),(8,7,9116,'Shield'),(8,7,9077,'Leather'),(8,7,107,'Block'),(8,7,81,'Dodge'),(8,7,204,'Defense'),(8,7,522,'SPELLDEFENSE (DND)'),(8,7,403,'Lightning Bolt'),(8,7,9078,'Cloth'),(8,7,198,'One-Handed Maces'),(8,8,7341,'Language Troll'),(8,8,26290,'Bow Specialization'),(8,8,26297,'Berserking'),(8,8,58943,'Da Voodoo Shuffle'),(8,8,20555,'Regeneration'),(8,8,20557,'Beast Slaying'),(8,8,20558,'Throwing Specialization'),(8,8,79744,'Languages'),(5,8,71761,'Deep Freeze Immunity State'),(8,8,5009,'Wands'),(8,8,5019,'Shoot'),(8,8,669,'Language Orcish'),(8,8,1180,'Daggers'),(8,8,45927,'Summon Friend'),(8,8,6478,'Opening'),(8,8,6603,'Auto Attack'),(8,8,22027,'Remove Insignia'),(8,8,22810,'Opening - No Text'),(8,8,21651,'Opening'),(8,8,21652,'Closing'),(8,8,2382,'Generic'),(8,8,3365,'Opening'),(8,8,3050,'Detect'),(8,8,9125,'Generic'),(8,8,8386,'Attacking'),(8,8,7266,'Duel'),(8,8,7267,'Grovel'),(8,8,7355,'Stuck'),(8,8,6233,'Closing'),(8,8,6246,'Closing'),(8,8,6247,'Opening'),(8,8,6477,'Opening'),(8,8,61437,'Opening'),(8,8,68398,'Opening'),(8,8,96220,'Opening'),(8,8,79684,'Offensive State (DND)'),(8,8,203,'Unarmed'),(8,8,133,'Fireball'),(8,8,92315,'Pyroblast!'),(8,8,85801,'DPS Caster Crit Damage Bonus'),(8,8,76298,'Weapon Skills'),(8,8,76276,'Armor Skills'),(8,8,227,'Staves'),(8,8,81,'Dodge'),(8,8,204,'Defense'),(8,8,522,'SPELLDEFENSE (DND)'),(8,8,9078,'Cloth'),(8,8,201,'One-Handed Swords'),(8,9,7341,'Language Troll'),(8,9,26290,'Bow Specialization'),(8,9,26297,'Berserking'),(8,9,58943,'Da Voodoo Shuffle'),(8,9,20555,'Regeneration'),(8,9,20557,'Beast Slaying'),(8,9,20558,'Throwing Specialization'),(8,9,79744,'Languages'),(8,9,5009,'Wands'),(8,9,5019,'Shoot'),(8,9,669,'Language Common'),(8,9,1180,'Daggers'),(8,9,45927,'Summon Friend'),(8,9,6478,'Opening'),(8,9,6603,'Auto Attack'),(8,9,22027,'Remove Insignia'),(8,9,22810,'Opening - No Text'),(8,9,21651,'Opening'),(8,9,21652,'Closing'),(8,9,2382,'Generic'),(8,9,3365,'Opening'),(8,9,3050,'Detect'),(8,9,9125,'Generic'),(8,9,8386,'Attacking'),(8,9,7266,'Duel'),(8,9,7267,'Grovel'),(8,9,7355,'Stuck'),(8,9,6233,'Closing'),(8,9,6246,'Closing'),(8,9,6247,'Opening'),(8,9,6477,'Opening'),(8,9,61437,'Opening'),(8,9,68398,'Opening'),(8,9,96220,'Opening'),(8,9,203,'Unarmed'),(8,9,688,'Summon Imp'),(8,9,86213,'Soul Swap Exhale'),(8,9,89420,'Drain Life'),(8,9,85801,'DPS Caster Crit Damage Bonus'),(8,9,76299,'Weapon Skills'),(8,9,76277,'Armor Skills'),(8,9,87330,'Suppression'),(8,9,227,'Staves'),(8,9,75445,'Demonic Immolate'),(8,9,686,'Shadow Bolt'),(8,9,58284,'Chaos Bolt Passive'),(8,9,81,'Dodge'),(8,9,204,'Defense'),(8,9,522,'SPELLDEFENSE (DND)'),(8,9,9078,'Cloth'),(8,9,201,'One-Handed Swords'),(8,11,7341,'Language Troll'),(8,11,26290,'Bow Specialization'),(8,11,26297,'Berserking'),(8,11,58943,'Da Voodoo Shuffle'),(8,11,20555,'Regeneration'),(8,11,20557,'Beast Slaying'),(8,11,20558,'Throwing Specialization'),(8,11,79744,'Languages'),(8,11,669,'Language Orcish'),(8,11,1180,'Daggers'),(8,11,45927,'Summon Friend'),(8,11,6478,'Opening'),(8,11,6603,'Auto Attack'),(8,11,22027,'Remove Insignia'),(8,11,22810,'Opening - No Text'),(8,11,21651,'Opening'),(8,11,21652,'Closing'),(8,11,2382,'Generic'),(8,11,3365,'Opening'),(8,11,3050,'Detect'),(8,11,9125,'Generic'),(8,11,8386,'Attacking'),(8,11,7266,'Duel'),(8,11,7267,'Grovel'),(8,11,7355,'Stuck'),(8,11,6233,'Closing'),(8,11,6246,'Closing'),(8,11,6247,'Opening'),(8,11,6477,'Opening'),(8,11,61437,'Opening'),(8,11,68398,'Opening'),(8,11,96220,'Opening'),(8,11,203,'Unarmed'),(8,11,84736,'Nature\'s Focus'),(8,11,81170,'Ravage!'),(8,11,79577,'Eclipse Mastery Driver Passive'),(8,11,76300,'Weapon Skills'),(8,11,76275,'Armor Skills'),(8,11,5176,'Wrath'),(8,11,199,'Two-Handed Maces'),(8,11,15590,'Fist Weapons'),(8,11,84738,'Celestial Focus'),(8,11,200,'Polearms'),(8,11,227,'Staves'),(8,11,81,'Dodge'),(8,11,204,'Defense'),(8,11,522,'SPELLDEFENSE (DND)'),(8,11,9077,'Leather'),(8,11,9078,'Cloth'),(8,11,198,'One-Handed Maces'),(9,1,202,'Two-Handed Swords'),(9,1,69269,'Language Goblin'),(9,1,69070,'Rocket Jump'),(9,1,69041,'Rocket Barrage'),(9,1,69044,'Best Deals Anywhere'),(9,1,69045,'Better Living Through Chemistry'),(9,1,69042,'Time is Money'),(9,1,79749,'Languages'),(9,1,196,'One-Handed Axes'),(9,1,2457,'Battle Stance'),(9,1,88161,'Strike'),(9,1,5011,'Crossbows'),(9,1,3018,'Shoot'),(9,1,669,'Language Orcish'),(9,1,197,'Two-Handed Axes'),(9,1,1180,'Daggers'),(9,1,45927,'Summon Friend'),(9,1,6478,'Opening'),(9,1,6603,'Auto Attack'),(9,1,22027,'Remove Insignia'),(9,1,22810,'Opening - No Text'),(9,1,21651,'Opening'),(9,1,21652,'Closing'),(9,1,2382,'Generic'),(9,1,3365,'Opening'),(9,1,3050,'Detect'),(9,1,9125,'Generic'),(9,1,8386,'Attacking'),(9,1,7266,'Duel'),(9,1,7267,'Grovel'),(9,1,7355,'Stuck'),(9,1,6233,'Closing'),(9,1,6246,'Closing'),(9,1,6247,'Opening'),(9,1,6477,'Opening'),(9,1,61437,'Opening'),(9,1,68398,'Opening'),(9,1,96220,'Opening'),(9,1,203,'Unarmed'),(9,1,49410,'Forceful Deflection'),(9,1,199,'Two-Handed Maces'),(9,1,15590,'Fist Weapons'),(9,1,76268,'Armor Skills'),(9,1,76290,'Weapon Skills'),(9,1,88163,'Attack'),(9,1,200,'Polearms'),(9,1,227,'Staves'),(9,1,9116,'Shield'),(9,1,8737,'Mail'),(9,1,9077,'Leather'),(9,1,264,'Bows'),(9,1,266,'Guns'),(9,1,107,'Block'),(9,1,81,'Dodge'),(9,1,32215,'Victorious State'),(9,1,5301,'Defensive State (DND)'),(9,1,204,'Defense'),(9,1,522,'SPELLDEFENSE (DND)'),(9,1,2764,'Throw'),(9,1,2567,'Thrown'),(9,1,9078,'Cloth'),(9,1,198,'One-Handed Maces'),(9,1,201,'One-Handed Swords'),(9,3,202,'Two-Handed Swords'),(9,3,69269,'Language Goblin'),(9,3,69070,'Rocket Jump'),(9,3,69041,'Rocket Barrage'),(9,3,69044,'Best Deals Anywhere'),(9,3,69045,'Better Living Through Chemistry'),(9,3,69042,'Time is Money'),(9,3,79749,'Languages'),(9,3,196,'One-Handed Axes'),(9,3,3044,'Arcane Shot'),(9,3,75,'Auto Shot'),(9,3,82928,'Aimed Shot!'),(9,3,5011,'Crossbows'),(9,3,669,'Language Orcish'),(9,3,197,'Two-Handed Axes'),(9,3,1180,'Daggers'),(9,3,45927,'Summon Friend'),(9,3,6478,'Opening'),(9,3,6603,'Auto Attack'),(9,3,22027,'Remove Insignia'),(9,3,22810,'Opening - No Text'),(9,3,21651,'Opening'),(9,3,21652,'Closing'),(9,3,34082,'Hunter Passive Auras (DND)'),(9,3,2382,'Generic'),(9,3,3365,'Opening'),(9,3,3050,'Detect'),(9,3,9125,'Generic'),(9,3,8386,'Attacking'),(9,3,7266,'Duel'),(9,3,7267,'Grovel'),(9,3,7355,'Stuck'),(9,3,6233,'Closing'),(9,3,6246,'Closing'),(9,3,6247,'Opening'),(9,3,6477,'Opening'),(9,3,61437,'Opening'),(9,3,68398,'Opening'),(9,3,96220,'Opening'),(9,3,203,'Unarmed'),(9,3,883,'Call Pet 1'),(9,3,982,'Revive Pet'),(9,3,15590,'Fist Weapons'),(9,3,200,'Polearms'),(9,3,227,'Staves'),(9,3,9077,'Leather'),(9,3,264,'Bows'),(9,3,266,'Guns'),(9,3,13358,'Defensive State (DND)'),(9,3,81,'Dodge'),(9,3,204,'Defense'),(9,3,522,'SPELLDEFENSE (DND)'),(9,3,24949,'Defensive State 2 (DND)'),(9,3,9078,'Cloth'),(9,3,77442,'Focus'),(9,3,76249,'Weapon Skills'),(9,3,76250,'Armor Skills'),(9,3,87324,'Focused Aim'),(9,3,87816,'General Hunter Passives'),(9,3,201,'One-Handed Swords'),(9,4,69269,'Language Goblin'),(9,4,69070,'Rocket Jump'),(9,4,69041,'Rocket Barrage'),(9,4,69044,'Best Deals Anywhere'),(9,4,69045,'Better Living Through Chemistry'),(9,4,69042,'Time is Money'),(9,4,79749,'Languages'),(9,4,196,'One-Handed Axes'),(9,4,1752,'Sinister Strike'),(9,4,5011,'Crossbows'),(9,4,3018,'Shoot'),(9,4,669,'Language Orcish'),(9,4,674,'Dual Wield'),(9,4,1180,'Daggers'),(9,4,45927,'Summon Friend'),(9,4,6478,'Opening'),(9,4,6603,'Auto Attack'),(9,4,22027,'Remove Insignia'),(9,4,22810,'Opening - No Text'),(9,4,21651,'Opening'),(9,4,21652,'Closing'),(9,4,2382,'Generic'),(9,4,3365,'Opening'),(9,4,3050,'Detect'),(9,4,9125,'Generic'),(9,4,8386,'Attacking'),(9,4,7266,'Duel'),(9,4,7267,'Grovel'),(9,4,7355,'Stuck'),(9,4,6233,'Closing'),(9,4,6246,'Closing'),(9,4,6247,'Opening'),(9,4,6477,'Opening'),(9,4,61437,'Opening'),(9,4,68398,'Opening'),(9,4,96220,'Opening'),(9,4,203,'Unarmed'),(9,4,15590,'Fist Weapons'),(9,4,76273,'Armor Skills'),(9,4,76297,'Weapon Skills'),(9,4,9077,'Leather'),(9,4,264,'Bows'),(9,4,266,'Guns'),(9,4,81,'Dodge'),(9,4,204,'Defense'),(9,4,522,'SPELLDEFENSE (DND)'),(9,4,16092,'Defensive State (DND)'),(9,4,2764,'Throw'),(9,4,2567,'Thrown'),(9,4,9078,'Cloth'),(9,4,198,'One-Handed Maces'),(9,4,201,'One-Handed Swords'),(9,5,69269,'Language Goblin'),(9,5,69070,'Rocket Jump'),(9,5,69041,'Rocket Barrage'),(9,5,69044,'Best Deals Anywhere'),(9,5,69045,'Better Living Through Chemistry'),(9,5,69042,'Time is Money'),(9,5,79749,'Languages'),(9,5,5009,'Wands'),(9,5,5019,'Shoot'),(9,5,669,'Language Orcish'),(9,5,1180,'Daggers'),(9,5,45927,'Summon Friend'),(9,5,6478,'Opening'),(9,5,6603,'Auto Attack'),(9,5,22027,'Remove Insignia'),(9,5,22810,'Opening - No Text'),(9,5,21651,'Opening'),(9,5,21652,'Closing'),(9,5,2382,'Generic'),(9,5,3365,'Opening'),(9,5,3050,'Detect'),(9,5,9125,'Generic'),(9,5,8386,'Attacking'),(9,5,7266,'Duel'),(9,5,7267,'Grovel'),(9,5,7355,'Stuck'),(9,5,6233,'Closing'),(9,5,6246,'Closing'),(9,5,6247,'Opening'),(9,5,6477,'Opening'),(9,5,61437,'Opening'),(9,5,68398,'Opening'),(9,5,96220,'Opening'),(9,5,203,'Unarmed'),(9,5,585,'Smite'),(9,5,84733,'Holy Focus'),(9,5,88685,'Holy Word: Sanctuary'),(9,5,88684,'Holy Word: Serenity'),(9,5,101062,'Flash Heal'),(9,5,76301,'Weapon Skills'),(9,5,76279,'Armor Skills'),(9,5,84734,'Dark Thoughts'),(9,5,227,'Staves'),(9,5,77486,'Shadow Orb Power'),(9,5,81,'Dodge'),(9,5,204,'Defense'),(9,5,522,'SPELLDEFENSE (DND)'),(9,5,9078,'Cloth'),(9,5,198,'One-Handed Maces'),(9,7,196,'One-Handed Axes'),(9,7,69269,'Language Goblin'),(9,7,69070,'Rocket Jump'),(9,7,69041,'Rocket Barrage'),(9,7,69044,'Best Deals Anywhere'),(9,7,69045,'Better Living Through Chemistry'),(9,7,69042,'Time is Money'),(9,7,79749,'Languages'),(9,7,669,'Language Orcish'),(9,7,197,'Two-Handed Axes'),(9,7,1180,'Daggers'),(9,7,45927,'Summon Friend'),(9,7,6478,'Opening'),(9,7,6603,'Auto Attack'),(9,7,22027,'Remove Insignia'),(9,7,22810,'Opening - No Text'),(9,7,21651,'Opening'),(9,7,21652,'Closing'),(9,7,2382,'Generic'),(9,7,3365,'Opening'),(9,7,3050,'Detect'),(9,7,9125,'Generic'),(9,7,8386,'Attacking'),(9,7,7266,'Duel'),(9,7,7267,'Grovel'),(9,7,7355,'Stuck'),(9,7,6233,'Closing'),(9,7,6246,'Closing'),(9,7,6247,'Opening'),(9,7,6477,'Opening'),(9,7,27763,'Relic'),(9,7,61437,'Opening'),(9,7,68398,'Opening'),(9,7,96220,'Opening'),(9,7,203,'Unarmed'),(9,7,199,'Two-Handed Maces'),(9,7,15590,'Fist Weapons'),(9,7,76272,'Armor Skills'),(9,7,76296,'Weapon Skills'),(9,7,89920,'Ancestral Focus'),(9,7,227,'Staves'),(9,7,9116,'Shield'),(9,7,9077,'Leather'),(9,7,107,'Block'),(9,7,81,'Dodge'),(9,7,204,'Defense'),(9,7,522,'SPELLDEFENSE (DND)'),(9,7,403,'Lightning Bolt'),(9,7,9078,'Cloth'),(9,7,198,'One-Handed Maces'),(9,8,69269,'Language Goblin'),(9,8,69070,'Rocket Jump'),(9,8,69041,'Rocket Barrage'),(9,8,69044,'Best Deals Anywhere'),(9,8,69045,'Better Living Through Chemistry'),(9,8,69042,'Time is Money'),(9,8,79749,'Languages'),(4,8,71761,'Deep Freeze Immunity State'),(9,8,5009,'Wands'),(9,8,5019,'Shoot'),(9,8,669,'Language Orcish'),(9,8,1180,'Daggers'),(9,8,45927,'Summon Friend'),(9,8,6478,'Opening'),(9,8,6603,'Auto Attack'),(9,8,22027,'Remove Insignia'),(9,8,22810,'Opening - No Text'),(9,8,21651,'Opening'),(9,8,21652,'Closing'),(9,8,2382,'Generic'),(9,8,3365,'Opening'),(9,8,3050,'Detect'),(9,8,9125,'Generic'),(9,8,8386,'Attacking'),(9,8,7266,'Duel'),(9,8,7267,'Grovel'),(9,8,7355,'Stuck'),(9,8,6233,'Closing'),(9,8,6246,'Closing'),(9,8,6247,'Opening'),(9,8,6477,'Opening'),(9,8,61437,'Opening'),(9,8,68398,'Opening'),(9,8,96220,'Opening'),(9,8,79684,'Offensive State (DND)'),(9,8,203,'Unarmed'),(9,8,133,'Fireball'),(9,8,92315,'Pyroblast!'),(9,8,85801,'DPS Caster Crit Damage Bonus'),(9,8,76298,'Weapon Skills'),(9,8,76276,'Armor Skills'),(9,8,227,'Staves'),(9,8,81,'Dodge'),(9,8,204,'Defense'),(9,8,522,'SPELLDEFENSE (DND)'),(9,8,9078,'Cloth'),(9,8,201,'One-Handed Swords'),(9,9,69269,'Language Goblin'),(9,9,69070,'Rocket Jump'),(9,9,69041,'Rocket Barrage'),(9,9,69044,'Best Deals Anywhere'),(9,9,69045,'Better Living Through Chemistry'),(9,9,69042,'Time is Money'),(9,9,79749,'Languages'),(9,9,5009,'Wands'),(9,9,5019,'Shoot'),(9,9,669,'Language Common'),(9,9,1180,'Daggers'),(9,9,45927,'Summon Friend'),(9,9,6478,'Opening'),(9,9,6603,'Auto Attack'),(9,9,22027,'Remove Insignia'),(9,9,22810,'Opening - No Text'),(9,9,21651,'Opening'),(9,9,21652,'Closing'),(9,9,2382,'Generic'),(9,9,3365,'Opening'),(9,9,3050,'Detect'),(9,9,9125,'Generic'),(9,9,8386,'Attacking'),(9,9,7266,'Duel'),(9,9,7267,'Grovel'),(9,9,7355,'Stuck'),(9,9,6233,'Closing'),(9,9,6246,'Closing'),(9,9,6247,'Opening'),(9,9,6477,'Opening'),(9,9,61437,'Opening'),(9,9,68398,'Opening'),(9,9,96220,'Opening'),(9,9,203,'Unarmed'),(9,9,688,'Summon Imp'),(9,9,86213,'Soul Swap Exhale'),(9,9,89420,'Drain Life'),(9,9,85801,'DPS Caster Crit Damage Bonus'),(9,9,76299,'Weapon Skills'),(9,9,76277,'Armor Skills'),(9,9,87330,'Suppression'),(9,9,227,'Staves'),(9,9,75445,'Demonic Immolate'),(9,9,686,'Shadow Bolt'),(9,9,58284,'Chaos Bolt Passive'),(9,9,81,'Dodge'),(9,9,204,'Defense'),(9,9,522,'SPELLDEFENSE (DND)'),(9,9,9078,'Cloth'),(9,9,201,'One-Handed Swords'),(10,1,202,'Two-Handed Swords'),(10,1,813,'Language Thalassian'),(10,1,822,'Arcane Resistance'),(10,1,28877,'Arcane Affinity'),(10,1,69179,'Arcane Torrent'),(10,1,79748,'Languages'),(10,1,196,'One-Handed Axes'),(10,1,2457,'Battle Stance'),(10,1,88161,'Strike'),(10,1,5011,'Crossbows'),(10,1,3018,'Shoot'),(10,1,669,'Language Orcish'),(10,1,197,'Two-Handed Axes'),(10,1,1180,'Daggers'),(10,1,45927,'Summon Friend'),(10,1,6478,'Opening'),(10,1,6603,'Auto Attack'),(10,1,22027,'Remove Insignia'),(10,1,22810,'Opening - No Text'),(10,1,21651,'Opening'),(10,1,21652,'Closing'),(10,1,2382,'Generic'),(10,1,3365,'Opening'),(10,1,3050,'Detect'),(10,1,9125,'Generic'),(10,1,8386,'Attacking'),(10,1,7266,'Duel'),(10,1,7267,'Grovel'),(10,1,7355,'Stuck'),(10,1,6233,'Closing'),(10,1,6246,'Closing'),(10,1,6247,'Opening'),(10,1,6477,'Opening'),(10,1,61437,'Opening'),(10,1,68398,'Opening'),(10,1,96220,'Opening'),(10,1,203,'Unarmed'),(10,1,49410,'Forceful Deflection'),(10,1,199,'Two-Handed Maces'),(10,1,15590,'Fist Weapons'),(10,1,76268,'Armor Skills'),(10,1,76290,'Weapon Skills'),(10,1,88163,'Attack'),(10,1,200,'Polearms'),(10,1,227,'Staves'),(10,1,9116,'Shield'),(10,1,8737,'Mail'),(10,1,9077,'Leather'),(10,1,264,'Bows'),(10,1,266,'Guns'),(10,1,107,'Block'),(10,1,81,'Dodge'),(10,1,32215,'Victorious State'),(10,1,5301,'Defensive State (DND)'),(10,1,204,'Defense'),(10,1,522,'SPELLDEFENSE (DND)'),(10,1,2764,'Throw'),(10,1,2567,'Thrown'),(10,1,9078,'Cloth'),(10,1,198,'One-Handed Maces'),(10,1,201,'One-Handed Swords'),(10,2,202,'Two-Handed Swords'),(10,2,813,'Language Thalassian'),(10,2,822,'Arcane Resistance'),(10,2,28877,'Arcane Affinity'),(10,2,28730,'Arcane Torrent'),(10,2,79748,'Languages'),(10,2,196,'One-Handed Axes'),(10,2,669,'Language Orcish'),(10,2,197,'Two-Handed Axes'),(10,2,45927,'Summon Friend'),(10,2,6478,'Opening'),(10,2,6603,'Auto Attack'),(10,2,22027,'Remove Insignia'),(10,2,22810,'Opening - No Text'),(10,2,21651,'Opening'),(10,2,21652,'Closing'),(10,2,2382,'Generic'),(10,2,3365,'Opening'),(10,2,3050,'Detect'),(10,2,9125,'Generic'),(10,2,8386,'Attacking'),(10,2,7266,'Duel'),(10,2,7267,'Grovel'),(10,2,7355,'Stuck'),(10,2,6233,'Closing'),(10,2,6246,'Closing'),(10,2,6247,'Opening'),(10,2,6477,'Opening'),(10,2,27762,'Relic'),(10,2,61437,'Opening'),(10,2,68398,'Opening'),(10,2,96220,'Opening'),(10,2,49410,'Forceful Deflection'),(10,2,203,'Unarmed'),(10,2,199,'Two-Handed Maces'),(10,2,76271,'Armor Skills'),(10,2,76294,'Weapon Skills'),(10,2,200,'Polearms'),(10,2,35395,'Crusader Strike'),(10,2,60091,'Judgement Anti-Parry/Dodge Passive'),(10,2,9116,'Shield'),(10,2,8737,'Mail'),(10,2,9077,'Leather'),(10,2,107,'Block'),(10,2,81,'Dodge'),(10,2,204,'Defense'),(10,2,522,'SPELLDEFENSE (DND)'),(10,2,9078,'Cloth'),(10,2,20208,'Paladin pushback resistance'),(10,2,198,'One-Handed Maces'),(10,2,201,'One-Handed Swords'),(10,3,202,'Two-Handed Swords'),(10,3,813,'Language Thalassian'),(10,3,822,'Arcane Resistance'),(10,3,28877,'Arcane Affinity'),(10,3,80483,'Arcane Torrent'),(10,3,79748,'Languages'),(10,3,196,'One-Handed Axes'),(10,3,3044,'Arcane Shot'),(10,3,75,'Auto Shot'),(10,3,82928,'Aimed Shot!'),(10,3,5011,'Crossbows'),(10,3,669,'Language Orcish'),(10,3,197,'Two-Handed Axes'),(10,3,1180,'Daggers'),(10,3,45927,'Summon Friend'),(10,3,6478,'Opening'),(10,3,6603,'Auto Attack'),(10,3,22027,'Remove Insignia'),(10,3,22810,'Opening - No Text'),(10,3,21651,'Opening'),(10,3,21652,'Closing'),(10,3,34082,'Hunter Passive Auras (DND)'),(10,3,2382,'Generic'),(10,3,3365,'Opening'),(10,3,3050,'Detect'),(10,3,9125,'Generic'),(10,3,8386,'Attacking'),(10,3,7266,'Duel'),(10,3,7267,'Grovel'),(10,3,7355,'Stuck'),(10,3,6233,'Closing'),(10,3,6246,'Closing'),(10,3,6247,'Opening'),(10,3,6477,'Opening'),(10,3,61437,'Opening'),(10,3,68398,'Opening'),(10,3,96220,'Opening'),(10,3,203,'Unarmed'),(10,3,883,'Call Pet 1'),(10,3,982,'Revive Pet'),(10,3,15590,'Fist Weapons'),(10,3,200,'Polearms'),(10,3,227,'Staves'),(10,3,9077,'Leather'),(10,3,264,'Bows'),(10,3,266,'Guns'),(10,3,13358,'Defensive State (DND)'),(10,3,81,'Dodge'),(10,3,204,'Defense'),(10,3,522,'SPELLDEFENSE (DND)'),(10,3,24949,'Defensive State 2 (DND)'),(10,3,9078,'Cloth'),(10,3,77442,'Focus'),(10,3,76249,'Weapon Skills'),(10,3,76250,'Armor Skills'),(10,3,87324,'Focused Aim'),(10,3,87816,'General Hunter Passives'),(10,3,201,'One-Handed Swords'),(10,4,196,'One-Handed Axes'),(10,4,813,'Language Thalassian'),(10,4,822,'Arcane Resistance'),(10,4,28877,'Arcane Affinity'),(10,4,25046,'Arcane Torrent'),(10,4,79748,'Languages'),(10,4,1752,'Sinister Strike'),(10,4,5011,'Crossbows'),(10,4,3018,'Shoot'),(10,4,669,'Language Orcish'),(10,4,674,'Dual Wield'),(10,4,1180,'Daggers'),(10,4,45927,'Summon Friend'),(10,4,6478,'Opening'),(10,4,6603,'Auto Attack'),(10,4,22027,'Remove Insignia'),(10,4,22810,'Opening - No Text'),(10,4,21651,'Opening'),(10,4,21652,'Closing'),(10,4,2382,'Generic'),(10,4,3365,'Opening'),(10,4,3050,'Detect'),(10,4,9125,'Generic'),(10,4,8386,'Attacking'),(10,4,7266,'Duel'),(10,4,7267,'Grovel'),(10,4,7355,'Stuck'),(10,4,6233,'Closing'),(10,4,6246,'Closing'),(10,4,6247,'Opening'),(10,4,6477,'Opening'),(10,4,61437,'Opening'),(10,4,68398,'Opening'),(10,4,96220,'Opening'),(10,4,203,'Unarmed'),(10,4,15590,'Fist Weapons'),(10,4,76273,'Armor Skills'),(10,4,76297,'Weapon Skills'),(10,4,9077,'Leather'),(10,4,264,'Bows'),(10,4,266,'Guns'),(10,4,81,'Dodge'),(10,4,204,'Defense'),(10,4,522,'SPELLDEFENSE (DND)'),(10,4,16092,'Defensive State (DND)'),(10,4,2764,'Throw'),(10,4,2567,'Thrown'),(10,4,9078,'Cloth'),(10,4,198,'One-Handed Maces'),(10,4,201,'One-Handed Swords'),(10,5,813,'Language Thalassian'),(10,5,822,'Arcane Resistance'),(10,5,28877,'Arcane Affinity'),(10,5,28730,'Arcane Torrent'),(10,5,79748,'Languages'),(10,5,5009,'Wands'),(10,5,5019,'Shoot'),(10,5,669,'Language Orcish'),(10,5,1180,'Daggers'),(10,5,45927,'Summon Friend'),(10,5,6478,'Opening'),(10,5,6603,'Auto Attack'),(10,5,22027,'Remove Insignia'),(10,5,22810,'Opening - No Text'),(10,5,21651,'Opening'),(10,5,21652,'Closing'),(10,5,2382,'Generic'),(10,5,3365,'Opening'),(10,5,3050,'Detect'),(10,5,9125,'Generic'),(10,5,8386,'Attacking'),(10,5,7266,'Duel'),(10,5,7267,'Grovel'),(10,5,7355,'Stuck'),(10,5,6233,'Closing'),(10,5,6246,'Closing'),(10,5,6247,'Opening'),(10,5,6477,'Opening'),(10,5,61437,'Opening'),(10,5,68398,'Opening'),(10,5,96220,'Opening'),(10,5,203,'Unarmed'),(10,5,585,'Smite'),(10,5,84733,'Holy Focus'),(10,5,88685,'Holy Word: Sanctuary'),(10,5,88684,'Holy Word: Serenity'),(10,5,101062,'Flash Heal'),(10,5,76301,'Weapon Skills'),(10,5,76279,'Armor Skills'),(10,5,84734,'Dark Thoughts'),(10,5,227,'Staves'),(10,5,77486,'Shadow Orb Power'),(10,5,81,'Dodge'),(10,5,204,'Defense'),(10,5,522,'SPELLDEFENSE (DND)'),(10,5,9078,'Cloth'),(10,5,198,'One-Handed Maces'),(10,8,813,'Language Thalassian'),(10,8,822,'Arcane Resistance'),(10,8,28877,'Arcane Affinity'),(10,8,28730,'Arcane Torrent'),(10,8,79748,'Languages'),(3,8,71761,'Deep Freeze Immunity State'),(10,8,5009,'Wands'),(10,8,5019,'Shoot'),(10,8,669,'Language Orcish'),(10,8,1180,'Daggers'),(10,8,45927,'Summon Friend'),(10,8,6478,'Opening'),(10,8,6603,'Auto Attack'),(10,8,22027,'Remove Insignia'),(10,8,22810,'Opening - No Text'),(10,8,21651,'Opening'),(10,8,21652,'Closing'),(10,8,2382,'Generic'),(10,8,3365,'Opening'),(10,8,3050,'Detect'),(10,8,9125,'Generic'),(10,8,8386,'Attacking'),(10,8,7266,'Duel'),(10,8,7267,'Grovel'),(10,8,7355,'Stuck'),(10,8,6233,'Closing'),(10,8,6246,'Closing'),(10,8,6247,'Opening'),(10,8,6477,'Opening'),(10,8,61437,'Opening'),(10,8,68398,'Opening'),(10,8,96220,'Opening'),(10,8,79684,'Offensive State (DND)'),(10,8,203,'Unarmed'),(10,8,133,'Fireball'),(10,8,92315,'Pyroblast!'),(10,8,85801,'DPS Caster Crit Damage Bonus'),(10,8,76298,'Weapon Skills'),(10,8,76276,'Armor Skills'),(10,8,227,'Staves'),(10,8,81,'Dodge'),(10,8,204,'Defense'),(10,8,522,'SPELLDEFENSE (DND)'),(10,8,9078,'Cloth'),(10,8,201,'One-Handed Swords'),(10,9,813,'Language Thalassian'),(10,9,822,'Arcane Resistance'),(10,9,28877,'Arcane Affinity'),(10,9,28730,'Arcane Torrent'),(10,9,79748,'Languages'),(10,9,5009,'Wands'),(10,9,5019,'Shoot'),(10,9,669,'Language Common'),(10,9,1180,'Daggers'),(10,9,45927,'Summon Friend'),(10,9,6478,'Opening'),(10,9,6603,'Auto Attack'),(10,9,22027,'Remove Insignia'),(10,9,22810,'Opening - No Text'),(10,9,21651,'Opening'),(10,9,21652,'Closing'),(10,9,2382,'Generic'),(10,9,3365,'Opening'),(10,9,3050,'Detect'),(10,9,9125,'Generic'),(10,9,8386,'Attacking'),(10,9,7266,'Duel'),(10,9,7267,'Grovel'),(10,9,7355,'Stuck'),(10,9,6233,'Closing'),(10,9,6246,'Closing'),(10,9,6247,'Opening'),(10,9,6477,'Opening'),(10,9,61437,'Opening'),(10,9,68398,'Opening'),(10,9,96220,'Opening'),(10,9,203,'Unarmed'),(10,9,688,'Summon Imp'),(10,9,86213,'Soul Swap Exhale'),(10,9,89420,'Drain Life'),(10,9,85801,'DPS Caster Crit Damage Bonus'),(10,9,76299,'Weapon Skills'),(10,9,76277,'Armor Skills'),(10,9,87330,'Suppression'),(10,9,227,'Staves'),(10,9,75445,'Demonic Immolate'),(10,9,686,'Shadow Bolt'),(10,9,58284,'Chaos Bolt Passive'),(10,9,81,'Dodge'),(10,9,204,'Defense'),(10,9,522,'SPELLDEFENSE (DND)'),(10,9,9078,'Cloth'),(10,9,201,'One-Handed Swords'),(11,1,202,'Two-Handed Swords'),(11,1,196,'One-Handed Axes'),(11,1,2457,'Battle Stance'),(11,1,88161,'Strike'),(11,1,5011,'Crossbows'),(11,1,3018,'Shoot'),(11,1,197,'Two-Handed Axes'),(11,1,1180,'Daggers'),(11,1,45927,'Summon Friend'),(11,1,6478,'Opening'),(11,1,6603,'Auto Attack'),(11,1,22027,'Remove Insignia'),(11,1,22810,'Opening - No Text'),(11,1,21651,'Opening'),(11,1,21652,'Closing'),(11,1,2382,'Generic'),(11,1,3365,'Opening'),(11,1,3050,'Detect'),(11,1,9125,'Generic'),(11,1,8386,'Attacking'),(11,1,7266,'Duel'),(11,1,7267,'Grovel'),(11,1,7355,'Stuck'),(11,1,6233,'Closing'),(11,1,6246,'Closing'),(11,1,6247,'Opening'),(11,1,6477,'Opening'),(11,1,61437,'Opening'),(11,1,68398,'Opening'),(11,1,96220,'Opening'),(11,1,203,'Unarmed'),(11,1,29932,'Language Draenei'),(11,1,79741,'Languages'),(11,1,49410,'Forceful Deflection'),(11,1,199,'Two-Handed Maces'),(11,1,15590,'Fist Weapons'),(11,1,76268,'Armor Skills'),(11,1,76290,'Weapon Skills'),(11,1,88163,'Attack'),(11,1,200,'Polearms'),(11,1,6562,'Heroic Presence'),(11,1,28880,'Gift of the Naaru'),(11,1,28875,'Gemcutting'),(11,1,59221,'Shadow Resistance'),(11,1,668,'Language Common'),(11,1,227,'Staves'),(11,1,9116,'Shield'),(11,1,8737,'Mail'),(11,1,9077,'Leather'),(11,1,264,'Bows'),(11,1,266,'Guns'),(11,1,107,'Block'),(11,1,81,'Dodge'),(11,1,32215,'Victorious State'),(11,1,5301,'Defensive State (DND)'),(11,1,204,'Defense'),(11,1,522,'SPELLDEFENSE (DND)'),(11,1,2764,'Throw'),(11,1,2567,'Thrown'),(11,1,9078,'Cloth'),(11,1,198,'One-Handed Maces'),(11,1,201,'One-Handed Swords'),(11,2,202,'Two-Handed Swords'),(11,2,196,'One-Handed Axes'),(11,2,197,'Two-Handed Axes'),(11,2,45927,'Summon Friend'),(11,2,6478,'Opening'),(11,2,6603,'Auto Attack'),(11,2,22027,'Remove Insignia'),(11,2,22810,'Opening - No Text'),(11,2,21651,'Opening'),(11,2,21652,'Closing'),(11,2,2382,'Generic'),(11,2,3365,'Opening'),(11,2,3050,'Detect'),(11,2,9125,'Generic'),(11,2,8386,'Attacking'),(11,2,7266,'Duel'),(11,2,7267,'Grovel'),(11,2,7355,'Stuck'),(11,2,6233,'Closing'),(11,2,6246,'Closing'),(11,2,6247,'Opening'),(11,2,6477,'Opening'),(11,2,27762,'Relic'),(11,2,61437,'Opening'),(11,2,68398,'Opening'),(11,2,96220,'Opening'),(11,2,49410,'Forceful Deflection'),(11,2,203,'Unarmed'),(11,2,29932,'Language Draenei'),(11,2,79741,'Languages'),(11,2,59535,'Shadow Resistance'),(11,2,6562,'Heroic Presence'),(11,2,28875,'Gemcutting'),(11,2,59542,'Gift of the Naaru'),(11,2,199,'Two-Handed Maces'),(11,2,76271,'Armor Skills'),(11,2,76294,'Weapon Skills'),(11,2,200,'Polearms'),(11,2,668,'Language Common'),(11,2,35395,'Crusader Strike'),(11,2,60091,'Judgement Anti-Parry/Dodge Passive'),(11,2,9116,'Shield'),(11,2,8737,'Mail'),(11,2,9077,'Leather'),(11,2,107,'Block'),(11,2,81,'Dodge'),(11,2,204,'Defense'),(11,2,522,'SPELLDEFENSE (DND)'),(11,2,9078,'Cloth'),(11,2,20208,'Paladin pushback resistance'),(11,2,198,'One-Handed Maces'),(11,2,201,'One-Handed Swords'),(11,3,202,'Two-Handed Swords'),(11,3,196,'One-Handed Axes'),(11,3,3044,'Arcane Shot'),(11,3,75,'Auto Shot'),(11,3,82928,'Aimed Shot!'),(11,3,5011,'Crossbows'),(11,3,197,'Two-Handed Axes'),(11,3,1180,'Daggers'),(11,3,45927,'Summon Friend'),(11,3,6478,'Opening'),(11,3,6603,'Auto Attack'),(11,3,22027,'Remove Insignia'),(11,3,22810,'Opening - No Text'),(11,3,21651,'Opening'),(11,3,21652,'Closing'),(11,3,34082,'Hunter Passive Auras (DND)'),(11,3,2382,'Generic'),(11,3,3365,'Opening'),(11,3,3050,'Detect'),(11,3,9125,'Generic'),(11,3,8386,'Attacking'),(11,3,7266,'Duel'),(11,3,7267,'Grovel'),(11,3,7355,'Stuck'),(11,3,6233,'Closing'),(11,3,6246,'Closing'),(11,3,6247,'Opening'),(11,3,6477,'Opening'),(11,3,61437,'Opening'),(11,3,68398,'Opening'),(11,3,96220,'Opening'),(11,3,203,'Unarmed'),(11,3,883,'Call Pet 1'),(11,3,982,'Revive Pet'),(11,3,29932,'Language Draenei'),(11,3,59543,'Gift of the Naaru'),(11,3,6562,'Heroic Presence'),(11,3,28875,'Gemcutting'),(11,3,59536,'Shadow Resistance'),(11,3,79741,'Languages'),(11,3,15590,'Fist Weapons'),(11,3,200,'Polearms'),(11,3,668,'Language Common'),(11,3,227,'Staves'),(11,3,9077,'Leather'),(11,3,264,'Bows'),(11,3,266,'Guns'),(11,3,13358,'Defensive State (DND)'),(11,3,81,'Dodge'),(11,3,204,'Defense'),(11,3,522,'SPELLDEFENSE (DND)'),(11,3,24949,'Defensive State 2 (DND)'),(11,3,9078,'Cloth'),(11,3,77442,'Focus'),(11,3,76249,'Weapon Skills'),(11,3,76250,'Armor Skills'),(11,3,87324,'Focused Aim'),(11,3,87816,'General Hunter Passives'),(11,3,201,'One-Handed Swords'),(11,5,5009,'Wands'),(11,5,5019,'Shoot'),(11,5,1180,'Daggers'),(11,5,45927,'Summon Friend'),(11,5,6478,'Opening'),(11,5,6603,'Auto Attack'),(11,5,22027,'Remove Insignia'),(11,5,22810,'Opening - No Text'),(11,5,21651,'Opening'),(11,5,21652,'Closing'),(11,5,2382,'Generic'),(11,5,3365,'Opening'),(11,5,3050,'Detect'),(11,5,9125,'Generic'),(11,5,8386,'Attacking'),(11,5,7266,'Duel'),(11,5,7267,'Grovel'),(11,5,7355,'Stuck'),(11,5,6233,'Closing'),(11,5,6246,'Closing'),(11,5,6247,'Opening'),(11,5,6477,'Opening'),(11,5,61437,'Opening'),(11,5,68398,'Opening'),(11,5,96220,'Opening'),(11,5,203,'Unarmed'),(11,5,585,'Smite'),(11,5,84733,'Holy Focus'),(11,5,88685,'Holy Word: Sanctuary'),(11,5,88684,'Holy Word: Serenity'),(11,5,101062,'Flash Heal'),(11,5,29932,'Language Draenei'),(11,5,59538,'Shadow Resistance'),(11,5,28878,'Heroic Presence'),(11,5,28875,'Gemcutting'),(11,5,59544,'Gift of the Naaru'),(11,5,79741,'Languages'),(11,5,76301,'Weapon Skills'),(11,5,76279,'Armor Skills'),(11,5,84734,'Dark Thoughts'),(11,5,668,'Language Common'),(11,5,227,'Staves'),(11,5,77486,'Shadow Orb Power'),(11,5,81,'Dodge'),(11,5,204,'Defense'),(11,5,522,'SPELLDEFENSE (DND)'),(11,5,9078,'Cloth'),(11,5,198,'One-Handed Maces'),(11,7,196,'One-Handed Axes'),(11,7,197,'Two-Handed Axes'),(11,7,1180,'Daggers'),(11,7,45927,'Summon Friend'),(11,7,6478,'Opening'),(11,7,6603,'Auto Attack'),(11,7,22027,'Remove Insignia'),(11,7,22810,'Opening - No Text'),(11,7,21651,'Opening'),(11,7,21652,'Closing'),(11,7,2382,'Generic'),(11,7,3365,'Opening'),(11,7,3050,'Detect'),(11,7,9125,'Generic'),(11,7,8386,'Attacking'),(11,7,7266,'Duel'),(11,7,7267,'Grovel'),(11,7,7355,'Stuck'),(11,7,6233,'Closing'),(11,7,6246,'Closing'),(11,7,6247,'Opening'),(11,7,6477,'Opening'),(11,7,27763,'Relic'),(11,7,61437,'Opening'),(11,7,68398,'Opening'),(11,7,96220,'Opening'),(11,7,203,'Unarmed'),(11,7,29932,'Language Draenei'),(11,7,28878,'Heroic Presence'),(11,7,28875,'Gemcutting'),(11,7,59540,'Shadow Resistance'),(11,7,59547,'Gift of the Naaru'),(11,7,79741,'Languages'),(11,7,199,'Two-Handed Maces'),(11,7,15590,'Fist Weapons'),(11,7,76272,'Armor Skills'),(11,7,76296,'Weapon Skills'),(11,7,89920,'Ancestral Focus'),(11,7,668,'Language Common'),(11,7,227,'Staves'),(11,7,9116,'Shield'),(11,7,9077,'Leather'),(11,7,107,'Block'),(11,7,81,'Dodge'),(11,7,204,'Defense'),(11,7,522,'SPELLDEFENSE (DND)'),(11,7,403,'Lightning Bolt'),(11,7,9078,'Cloth'),(11,7,198,'One-Handed Maces'),(2,8,71761,'Deep Freeze Immunity State'),(11,8,5009,'Wands'),(11,8,5019,'Shoot'),(11,8,1180,'Daggers'),(11,8,45927,'Summon Friend'),(11,8,6478,'Opening'),(11,8,6603,'Auto Attack'),(11,8,22027,'Remove Insignia'),(11,8,22810,'Opening - No Text'),(11,8,21651,'Opening'),(11,8,21652,'Closing'),(11,8,2382,'Generic'),(11,8,3365,'Opening'),(11,8,3050,'Detect'),(11,8,9125,'Generic'),(11,8,8386,'Attacking'),(11,8,7266,'Duel'),(11,8,7267,'Grovel'),(11,8,7355,'Stuck'),(11,8,6233,'Closing'),(11,8,6246,'Closing'),(11,8,6247,'Opening'),(11,8,6477,'Opening'),(11,8,61437,'Opening'),(11,8,68398,'Opening'),(11,8,96220,'Opening'),(11,8,79684,'Offensive State (DND)'),(11,8,203,'Unarmed'),(11,8,29932,'Language Draenei'),(11,8,59541,'Shadow Resistance'),(11,8,28878,'Heroic Presence'),(11,8,28875,'Gemcutting'),(11,8,59548,'Gift of the Naaru'),(11,8,79741,'Languages'),(11,8,133,'Fireball'),(11,8,92315,'Pyroblast!'),(11,8,85801,'DPS Caster Crit Damage Bonus'),(11,8,76298,'Weapon Skills'),(11,8,76276,'Armor Skills'),(11,8,668,'Language Common'),(11,8,227,'Staves'),(11,8,81,'Dodge'),(11,8,204,'Defense'),(11,8,522,'SPELLDEFENSE (DND)'),(11,8,9078,'Cloth'),(11,8,201,'One-Handed Swords'),(22,1,202,'Two-Handed Swords'),(22,1,196,'One-Handed Axes'),(22,1,2457,'Battle Stance'),(22,1,88161,'Strike'),(22,1,5011,'Crossbows'),(22,1,3018,'Shoot'),(22,1,197,'Two-Handed Axes'),(22,1,1180,'Daggers'),(22,1,45927,'Summon Friend'),(22,1,6478,'Opening'),(22,1,6603,'Auto Attack'),(22,1,22027,'Remove Insignia'),(22,1,22810,'Opening - No Text'),(22,1,21651,'Opening'),(22,1,21652,'Closing'),(22,1,2382,'Generic'),(22,1,3365,'Opening'),(22,1,3050,'Detect'),(22,1,9125,'Generic'),(22,1,8386,'Attacking'),(22,1,7266,'Duel'),(22,1,7267,'Grovel'),(22,1,7355,'Stuck'),(22,1,6233,'Closing'),(22,1,6246,'Closing'),(22,1,6247,'Opening'),(22,1,6477,'Opening'),(22,1,61437,'Opening'),(22,1,68398,'Opening'),(22,1,96220,'Opening'),(22,1,203,'Unarmed'),(22,1,79742,'Languages'),(22,1,69001,''),(22,1,49410,'Forceful Deflection'),(22,1,199,'Two-Handed Maces'),(22,1,15590,'Fist Weapons'),(22,1,76268,'Armor Skills'),(22,1,76290,'Weapon Skills'),(22,1,88163,'Attack'),(22,1,200,'Polearms'),(22,1,668,'Language Common'),(22,1,227,'Staves'),(22,1,9116,'Shield'),(22,1,8737,'Mail'),(22,1,9077,'Leather'),(22,1,264,'Bows'),(22,1,266,'Guns'),(22,1,107,'Block'),(22,1,81,'Dodge'),(22,1,32215,'Victorious State'),(22,1,5301,'Defensive State (DND)'),(22,1,204,'Defense'),(22,1,522,'SPELLDEFENSE (DND)'),(22,1,2764,'Throw'),(22,1,2567,'Thrown'),(22,1,9078,'Cloth'),(22,1,198,'One-Handed Maces'),(22,1,201,'One-Handed Swords'),(22,3,202,'Two-Handed Swords'),(22,3,196,'One-Handed Axes'),(22,3,3044,'Arcane Shot'),(22,3,75,'Auto Shot'),(22,3,82928,'Aimed Shot!'),(22,3,5011,'Crossbows'),(22,3,197,'Two-Handed Axes'),(22,3,1180,'Daggers'),(22,3,45927,'Summon Friend'),(22,3,6478,'Opening'),(22,3,6603,'Auto Attack'),(22,3,22027,'Remove Insignia'),(22,3,22810,'Opening - No Text'),(22,3,21651,'Opening'),(22,3,21652,'Closing'),(22,3,34082,'Hunter Passive Auras (DND)'),(22,3,2382,'Generic'),(22,3,3365,'Opening'),(22,3,3050,'Detect'),(22,3,9125,'Generic'),(22,3,8386,'Attacking'),(22,3,7266,'Duel'),(22,3,7267,'Grovel'),(22,3,7355,'Stuck'),(22,3,6233,'Closing'),(22,3,6246,'Closing'),(22,3,6247,'Opening'),(22,3,6477,'Opening'),(22,3,61437,'Opening'),(22,3,68398,'Opening'),(22,3,96220,'Opening'),(22,3,203,'Unarmed'),(22,3,883,'Call Pet 1'),(22,3,982,'Revive Pet'),(22,3,79742,'Languages'),(22,3,69001,''),(22,3,15590,'Fist Weapons'),(22,3,200,'Polearms'),(22,3,668,'Language Common'),(22,3,227,'Staves'),(22,3,9077,'Leather'),(22,3,264,'Bows'),(22,3,266,'Guns'),(22,3,13358,'Defensive State (DND)'),(22,3,81,'Dodge'),(22,3,204,'Defense'),(22,3,522,'SPELLDEFENSE (DND)'),(22,3,24949,'Defensive State 2 (DND)'),(22,3,9078,'Cloth'),(22,3,77442,'Focus'),(22,3,76249,'Weapon Skills'),(22,3,76250,'Armor Skills'),(22,3,87324,'Focused Aim'),(22,3,87816,'General Hunter Passives'),(22,3,201,'One-Handed Swords'),(22,4,196,'One-Handed Axes'),(22,4,1752,'Sinister Strike'),(22,4,5011,'Crossbows'),(22,4,3018,'Shoot'),(22,4,674,'Dual Wield'),(22,4,1180,'Daggers'),(22,4,45927,'Summon Friend'),(22,4,6478,'Opening'),(22,4,6603,'Auto Attack'),(22,4,22027,'Remove Insignia'),(22,4,22810,'Opening - No Text'),(22,4,21651,'Opening'),(22,4,21652,'Closing'),(22,4,2382,'Generic'),(22,4,3365,'Opening'),(22,4,3050,'Detect'),(22,4,9125,'Generic'),(22,4,8386,'Attacking'),(22,4,7266,'Duel'),(22,4,7267,'Grovel'),(22,4,7355,'Stuck'),(22,4,6233,'Closing'),(22,4,6246,'Closing'),(22,4,6247,'Opening'),(22,4,6477,'Opening'),(22,4,61437,'Opening'),(22,4,68398,'Opening'),(22,4,96220,'Opening'),(22,4,203,'Unarmed'),(22,4,79742,'Languages'),(22,4,69001,''),(22,4,15590,'Fist Weapons'),(22,4,76273,'Armor Skills'),(22,4,76297,'Weapon Skills'),(22,4,668,'Language Common'),(22,4,9077,'Leather'),(22,4,264,'Bows'),(22,4,266,'Guns'),(22,4,81,'Dodge'),(22,4,204,'Defense'),(22,4,522,'SPELLDEFENSE (DND)'),(22,4,16092,'Defensive State (DND)'),(22,4,2764,'Throw'),(22,4,2567,'Thrown'),(22,4,9078,'Cloth'),(22,4,198,'One-Handed Maces'),(22,4,201,'One-Handed Swords'),(22,5,5009,'Wands'),(22,5,5019,'Shoot'),(22,5,1180,'Daggers'),(22,5,45927,'Summon Friend'),(22,5,6478,'Opening'),(22,5,6603,'Auto Attack'),(22,5,22027,'Remove Insignia'),(22,5,22810,'Opening - No Text'),(22,5,21651,'Opening'),(22,5,21652,'Closing'),(22,5,2382,'Generic'),(22,5,3365,'Opening'),(22,5,3050,'Detect'),(22,5,9125,'Generic'),(22,5,8386,'Attacking'),(22,5,7266,'Duel'),(22,5,7267,'Grovel'),(22,5,7355,'Stuck'),(22,5,6233,'Closing'),(22,5,6246,'Closing'),(22,5,6247,'Opening'),(22,5,6477,'Opening'),(22,5,61437,'Opening'),(22,5,68398,'Opening'),(22,5,96220,'Opening'),(22,5,203,'Unarmed'),(22,5,585,'Smite'),(22,5,84733,'Holy Focus'),(22,5,88685,'Holy Word: Sanctuary'),(22,5,88684,'Holy Word: Serenity'),(22,5,101062,'Flash Heal'),(22,5,79742,'Languages'),(22,5,69001,''),(22,5,76301,'Weapon Skills'),(22,5,76279,'Armor Skills'),(22,5,84734,'Dark Thoughts'),(22,5,668,'Language Common'),(22,5,227,'Staves'),(22,5,77486,'Shadow Orb Power'),(22,5,81,'Dodge'),(22,5,204,'Defense'),(22,5,522,'SPELLDEFENSE (DND)'),(22,5,9078,'Cloth'),(22,5,198,'One-Handed Maces'),(1,8,71761,'Deep Freeze Immunity State'),(22,8,5009,'Wands'),(22,8,5019,'Shoot'),(22,8,1180,'Daggers'),(22,8,45927,'Summon Friend'),(22,8,6478,'Opening'),(22,8,6603,'Auto Attack'),(22,8,22027,'Remove Insignia'),(22,8,22810,'Opening - No Text'),(22,8,21651,'Opening'),(22,8,21652,'Closing'),(22,8,2382,'Generic'),(22,8,3365,'Opening'),(22,8,3050,'Detect'),(22,8,9125,'Generic'),(22,8,8386,'Attacking'),(22,8,7266,'Duel'),(22,8,7267,'Grovel'),(22,8,7355,'Stuck'),(22,8,6233,'Closing'),(22,8,6246,'Closing'),(22,8,6247,'Opening'),(22,8,6477,'Opening'),(22,8,61437,'Opening'),(22,8,68398,'Opening'),(22,8,96220,'Opening'),(22,8,79684,'Offensive State (DND)'),(22,8,203,'Unarmed'),(22,8,79742,'Languages'),(22,8,69001,''),(22,8,133,'Fireball'),(22,8,92315,'Pyroblast!'),(22,8,85801,'DPS Caster Crit Damage Bonus'),(22,8,76298,'Weapon Skills'),(22,8,76276,'Armor Skills'),(22,8,668,'Language Common'),(22,8,227,'Staves'),(22,8,81,'Dodge'),(22,8,204,'Defense'),(22,8,522,'SPELLDEFENSE (DND)'),(22,8,9078,'Cloth'),(22,8,201,'One-Handed Swords'),(22,9,5009,'Wands'),(22,9,5019,'Shoot'),(22,9,1180,'Daggers'),(22,9,45927,'Summon Friend'),(22,9,6478,'Opening'),(22,9,6603,'Auto Attack'),(22,9,22027,'Remove Insignia'),(22,9,22810,'Opening - No Text'),(22,9,21651,'Opening'),(22,9,21652,'Closing'),(22,9,2382,'Generic'),(22,9,3365,'Opening'),(22,9,3050,'Detect'),(22,9,9125,'Generic'),(22,9,8386,'Attacking'),(22,9,7266,'Duel'),(22,9,7267,'Grovel'),(22,9,7355,'Stuck'),(22,9,6233,'Closing'),(22,9,6246,'Closing'),(22,9,6247,'Opening'),(22,9,6477,'Opening'),(22,9,61437,'Opening'),(22,9,68398,'Opening'),(22,9,96220,'Opening'),(22,9,203,'Unarmed'),(22,9,79742,'Languages'),(22,9,69001,''),(22,9,688,'Summon Imp'),(22,9,86213,'Soul Swap Exhale'),(22,9,89420,'Drain Life'),(22,9,85801,'DPS Caster Crit Damage Bonus'),(22,9,76299,'Weapon Skills'),(22,9,76277,'Armor Skills'),(22,9,87330,'Suppression'),(22,9,668,'Language Common'),(22,9,227,'Staves'),(22,9,75445,'Demonic Immolate'),(22,9,686,'Shadow Bolt'),(22,9,58284,'Chaos Bolt Passive'),(22,9,81,'Dodge'),(22,9,204,'Defense'),(22,9,522,'SPELLDEFENSE (DND)'),(22,9,9078,'Cloth'),(22,9,201,'One-Handed Swords'),(22,11,1180,'Daggers'),(22,11,45927,'Summon Friend'),(22,11,6478,'Opening'),(22,11,6603,'Auto Attack'),(22,11,22027,'Remove Insignia'),(22,11,22810,'Opening - No Text'),(22,11,21651,'Opening'),(22,11,21652,'Closing'),(22,11,2382,'Generic'),(22,11,3365,'Opening'),(22,11,3050,'Detect'),(22,11,9125,'Generic'),(22,11,8386,'Attacking'),(22,11,7266,'Duel'),(22,11,7267,'Grovel'),(22,11,7355,'Stuck'),(22,11,6233,'Closing'),(22,11,6246,'Closing'),(22,11,6247,'Opening'),(22,11,6477,'Opening'),(22,11,61437,'Opening'),(22,11,68398,'Opening'),(22,11,96220,'Opening'),(22,11,203,'Unarmed'),(22,11,79742,'Languages'),(22,11,69001,''),(22,11,84736,'Nature\'s Focus'),(22,11,81170,'Ravage!'),(22,11,79577,'Eclipse Mastery Driver Passive'),(22,11,76300,'Weapon Skills'),(22,11,76275,'Armor Skills'),(22,11,5176,'Wrath'),(22,11,199,'Two-Handed Maces'),(22,11,15590,'Fist Weapons'),(22,11,84738,'Celestial Focus'),(22,11,668,'Language Common'),(22,11,200,'Polearms'),(22,11,227,'Staves'),(22,11,81,'Dodge'),(22,11,204,'Defense'),(22,11,522,'SPELLDEFENSE (DND)'),(22,11,9077,'Leather'),(22,11,9078,'Cloth'),(22,11,198,'One-Handed Maces'),(11,6,29932,'Language Draenei'),(1,6,81,'Dodge'),(1,6,196,'One-Handed Axes'),(1,6,197,'Two-Handed Axes'),(1,6,200,'Polearms'),(1,6,201,'One-Handed Swords'),(1,6,202,'Two-Handed Swords'),(1,6,203,'Unarmed'),(1,6,204,'Defense'),(1,6,522,'SPELLDEFENSE (DND)'),(1,6,750,'Plate Mail'),(1,6,1843,'Disarm'),(1,6,2382,'Generic'),(1,6,2479,'Honorless Target'),(1,6,3050,'Detect'),(1,6,3127,'Parry'),(1,6,3275,'Linen Bandage'),(1,6,3276,'Heavy Linen Bandage'),(1,6,3277,'Wool Bandage'),(1,6,3278,'Heavy Wool Bandage'),(1,6,3365,'Opening'),(1,6,6233,'Closing'),(1,6,6246,'Closing'),(1,6,6247,'Opening'),(1,6,6477,'Opening'),(1,6,6478,'Opening'),(1,6,6603,'Attack'),(1,6,7266,'Duel'),(1,6,7267,'Grovel'),(1,6,7355,'Stuck'),(1,6,7928,'Silk Bandage'),(1,6,7929,'Heavy Silk Bandage'),(1,6,7934,'Anti-Venom'),(1,6,8386,'Attacking'),(1,6,8737,'Mail'),(1,6,9077,'Leather'),(1,6,9078,'Cloth'),(1,6,9125,'Generic'),(1,6,10840,'Mageweave Bandage'),(1,6,10841,'Heavy Mageweave Bandage'),(1,6,10846,'First Aid'),(1,6,18629,'Runecloth Bandage'),(1,6,18630,'Heavy Runecloth Bandage'),(1,6,20597,'Sword Specialization'),(1,6,20598,'The Human Spirit'),(1,6,20599,'Diplomacy'),(1,6,20864,'Mace Specialization'),(1,6,21651,'Opening'),(1,6,21652,'Closing'),(1,6,22027,'Remove Insignia'),(1,6,22810,'Opening - No Text'),(1,6,33391,'Journeyman Riding'),(1,6,45462,'Plague Strike'),(1,6,45477,'Icy Touch'),(1,6,45902,'Blood Strike'),(1,6,45903,'Offensive State (DND)'),(1,6,45927,'Summon Friend'),(1,6,47541,'Death Coil'),(1,6,48266,'Blood Presence'),(1,6,49410,'Forceful Deflection'),(1,6,49576,'Death Grip'),(1,6,52665,'Relic - Sigil'),(1,6,59752,'Every Man for Himself'),(1,6,59879,'Blood Plague'),(1,6,59921,'Frost Fever'),(1,6,61437,'Opening'),(1,6,61455,'Runic Focus'),(2,6,81,'Dodge'),(2,6,196,'One-Handed Axes'),(2,6,197,'Two-Handed Axes'),(2,6,200,'Polearms'),(2,6,201,'One-Handed Swords'),(2,6,202,'Two-Handed Swords'),(2,6,203,'Unarmed'),(2,6,204,'Defense'),(2,6,522,'SPELLDEFENSE (DND)'),(2,6,750,'Plate Mail'),(2,6,1843,'Disarm'),(2,6,2382,'Generic'),(2,6,2479,'Honorless Target'),(2,6,3050,'Detect'),(2,6,3127,'Parry'),(2,6,3275,'Linen Bandage'),(2,6,3276,'Heavy Linen Bandage'),(2,6,3277,'Wool Bandage'),(2,6,3278,'Heavy Wool Bandage'),(2,6,3365,'Opening'),(2,6,6233,'Closing'),(2,6,6246,'Closing'),(2,6,6247,'Opening'),(2,6,6477,'Opening'),(2,6,6478,'Opening'),(2,6,6603,'Attack'),(2,6,7266,'Duel'),(2,6,7267,'Grovel'),(2,6,7355,'Stuck'),(2,6,7928,'Silk Bandage'),(2,6,7929,'Heavy Silk Bandage'),(2,6,7934,'Anti-Venom'),(2,6,8386,'Attacking'),(2,6,8737,'Mail'),(2,6,9077,'Leather'),(2,6,9078,'Cloth'),(2,6,9125,'Generic'),(2,6,10840,'Mageweave Bandage'),(2,6,10841,'Heavy Mageweave Bandage'),(2,6,10846,'First Aid'),(2,6,18629,'Runecloth Bandage'),(2,6,18630,'Heavy Runecloth Bandage'),(2,6,20572,'Blood Fury'),(2,6,20573,'Hardiness'),(2,6,20574,'Axe Specialization'),(2,6,21651,'Opening'),(2,6,21652,'Closing'),(2,6,22027,'Remove Insignia'),(2,6,22810,'Opening - No Text'),(2,6,33391,'Journeyman Riding'),(2,6,45462,'Plague Strike'),(2,6,45477,'Icy Touch'),(2,6,45902,'Blood Strike'),(2,6,45903,'Offensive State (DND)'),(2,6,45927,'Summon Friend'),(2,6,47541,'Death Coil'),(2,6,48266,'Blood Presence'),(2,6,49410,'Forceful Deflection'),(2,6,49576,'Death Grip'),(2,6,52665,'Relic - Sigil'),(2,6,54562,'Command'),(2,6,59879,'Blood Plague'),(2,6,59921,'Frost Fever'),(2,6,61437,'Opening'),(2,6,61455,'Runic Focus'),(4,6,671,'Language Darnassian'),(10,6,669,'Language Orcish'),(3,6,81,'Dodge'),(3,6,196,'One-Handed Axes'),(3,6,197,'Two-Handed Axes'),(3,6,200,'Polearms'),(3,6,201,'One-Handed Swords'),(3,6,202,'Two-Handed Swords'),(3,6,203,'Unarmed'),(3,6,204,'Defense'),(3,6,522,'SPELLDEFENSE (DND)'),(3,6,750,'Plate Mail'),(3,6,1843,'Disarm'),(3,6,2382,'Generic'),(3,6,2479,'Honorless Target'),(3,6,2481,'Find Treasure'),(3,6,3050,'Detect'),(3,6,3127,'Parry'),(3,6,3275,'Linen Bandage'),(3,6,3276,'Heavy Linen Bandage'),(3,6,3277,'Wool Bandage'),(3,6,3278,'Heavy Wool Bandage'),(3,6,3365,'Opening'),(3,6,6233,'Closing'),(3,6,6246,'Closing'),(3,6,6247,'Opening'),(3,6,6477,'Opening'),(3,6,6478,'Opening'),(3,6,6603,'Attack'),(3,6,7266,'Duel'),(3,6,7267,'Grovel'),(3,6,7355,'Stuck'),(3,6,7928,'Silk Bandage'),(3,6,7929,'Heavy Silk Bandage'),(3,6,7934,'Anti-Venom'),(3,6,8386,'Attacking'),(3,6,8737,'Mail'),(3,6,9077,'Leather'),(3,6,9078,'Cloth'),(3,6,9125,'Generic'),(3,6,10840,'Mageweave Bandage'),(3,6,10841,'Heavy Mageweave Bandage'),(3,6,10846,'First Aid'),(3,6,18629,'Runecloth Bandage'),(3,6,18630,'Heavy Runecloth Bandage'),(3,6,20594,'Stoneform'),(3,6,20595,'Gun Specialization'),(3,6,20596,'Frost Resistance'),(3,6,21651,'Opening'),(3,6,21652,'Closing'),(3,6,22027,'Remove Insignia'),(3,6,22810,'Opening - No Text'),(3,6,33391,'Journeyman Riding'),(3,6,45462,'Plague Strike'),(3,6,45477,'Icy Touch'),(3,6,45902,'Blood Strike'),(3,6,45903,'Offensive State (DND)'),(3,6,45927,'Summon Friend'),(3,6,47541,'Death Coil'),(3,6,48266,'Blood Presence'),(3,6,49410,'Forceful Deflection'),(3,6,49576,'Death Grip'),(3,6,52665,'Relic - Sigil'),(3,6,59224,'Mace Specialization'),(3,6,59879,'Blood Plague'),(3,6,59921,'Frost Fever'),(3,6,61437,'Opening'),(3,6,61455,'Runic Focus'),(9,6,669,'Language Orcish'),(4,6,81,'Dodge'),(4,6,196,'One-Handed Axes'),(4,6,197,'Two-Handed Axes'),(4,6,200,'Polearms'),(4,6,201,'One-Handed Swords'),(4,6,202,'Two-Handed Swords'),(4,6,203,'Unarmed'),(4,6,204,'Defense'),(4,6,522,'SPELLDEFENSE (DND)'),(4,6,750,'Plate Mail'),(4,6,1843,'Disarm'),(4,6,2382,'Generic'),(4,6,2479,'Honorless Target'),(4,6,3050,'Detect'),(4,6,3127,'Parry'),(4,6,3275,'Linen Bandage'),(4,6,3276,'Heavy Linen Bandage'),(4,6,3277,'Wool Bandage'),(4,6,3278,'Heavy Wool Bandage'),(4,6,3365,'Opening'),(4,6,6233,'Closing'),(4,6,6246,'Closing'),(4,6,6247,'Opening'),(4,6,6477,'Opening'),(4,6,6478,'Opening'),(4,6,6603,'Attack'),(4,6,7266,'Duel'),(4,6,7267,'Grovel'),(4,6,7355,'Stuck'),(4,6,7928,'Silk Bandage'),(4,6,7929,'Heavy Silk Bandage'),(4,6,7934,'Anti-Venom'),(4,6,8386,'Attacking'),(4,6,8737,'Mail'),(4,6,9077,'Leather'),(4,6,9078,'Cloth'),(4,6,9125,'Generic'),(4,6,10840,'Mageweave Bandage'),(4,6,10841,'Heavy Mageweave Bandage'),(4,6,10846,'First Aid'),(4,6,18629,'Runecloth Bandage'),(4,6,18630,'Heavy Runecloth Bandage'),(4,6,20582,'Quickness'),(4,6,20583,'Nature Resistance'),(4,6,20585,'Wisp Spirit'),(4,6,21651,'Opening'),(4,6,21652,'Closing'),(4,6,22027,'Remove Insignia'),(4,6,22810,'Opening - No Text'),(4,6,33391,'Journeyman Riding'),(4,6,45462,'Plague Strike'),(4,6,45477,'Icy Touch'),(4,6,45902,'Blood Strike'),(4,6,45903,'Offensive State (DND)'),(4,6,45927,'Summon Friend'),(4,6,47541,'Death Coil'),(4,6,48266,'Blood Presence'),(4,6,49410,'Forceful Deflection'),(4,6,49576,'Death Grip'),(4,6,52665,'Relic - Sigil'),(4,6,58984,'Shadowmeld'),(4,6,59879,'Blood Plague'),(4,6,59921,'Frost Fever'),(4,6,61437,'Opening'),(4,6,61455,'Runic Focus'),(8,6,669,'Language Orcish'),(5,6,81,'Dodge'),(5,6,196,'One-Handed Axes'),(5,6,197,'Two-Handed Axes'),(5,6,200,'Polearms'),(5,6,201,'One-Handed Swords'),(5,6,202,'Two-Handed Swords'),(5,6,203,'Unarmed'),(5,6,204,'Defense'),(5,6,522,'SPELLDEFENSE (DND)'),(5,6,750,'Plate Mail'),(5,6,1843,'Disarm'),(5,6,2382,'Generic'),(5,6,2479,'Honorless Target'),(5,6,3050,'Detect'),(5,6,3127,'Parry'),(5,6,3275,'Linen Bandage'),(5,6,3276,'Heavy Linen Bandage'),(5,6,3277,'Wool Bandage'),(5,6,3278,'Heavy Wool Bandage'),(5,6,3365,'Opening'),(5,6,5227,'Underwater Breathing'),(5,6,6233,'Closing'),(5,6,6246,'Closing'),(5,6,6247,'Opening'),(5,6,6477,'Opening'),(5,6,6478,'Opening'),(5,6,6603,'Attack'),(5,6,7266,'Duel'),(5,6,7267,'Grovel'),(5,6,7355,'Stuck'),(5,6,7744,'Will of the Forsaken'),(5,6,7928,'Silk Bandage'),(5,6,7929,'Heavy Silk Bandage'),(5,6,7934,'Anti-Venom'),(5,6,8386,'Attacking'),(5,6,8737,'Mail'),(5,6,9077,'Leather'),(5,6,9078,'Cloth'),(5,6,9125,'Generic'),(5,6,10840,'Mageweave Bandage'),(5,6,10841,'Heavy Mageweave Bandage'),(5,6,10846,'First Aid'),(5,6,18629,'Runecloth Bandage'),(5,6,18630,'Heavy Runecloth Bandage'),(5,6,20577,'Cannibalize'),(5,6,20579,'Shadow Resistance'),(5,6,21651,'Opening'),(5,6,21652,'Closing'),(5,6,22027,'Remove Insignia'),(5,6,22810,'Opening - No Text'),(5,6,33391,'Journeyman Riding'),(5,6,45462,'Plague Strike'),(5,6,45477,'Icy Touch'),(5,6,45902,'Blood Strike'),(5,6,45903,'Offensive State (DND)'),(5,6,45927,'Summon Friend'),(5,6,47541,'Death Coil'),(5,6,48266,'Blood Presence'),(5,6,49410,'Forceful Deflection'),(5,6,49576,'Death Grip'),(5,6,52665,'Relic - Sigil'),(5,6,59879,'Blood Plague'),(5,6,59921,'Frost Fever'),(5,6,61437,'Opening'),(5,6,61455,'Runic Focus'),(6,6,669,'Language Orcish'),(6,6,81,'Dodge'),(6,6,196,'One-Handed Axes'),(6,6,197,'Two-Handed Axes'),(6,6,200,'Polearms'),(6,6,201,'One-Handed Swords'),(6,6,202,'Two-Handed Swords'),(6,6,203,'Unarmed'),(6,6,204,'Defense'),(6,6,522,'SPELLDEFENSE (DND)'),(5,6,669,'Language Orcish'),(6,6,750,'Plate Mail'),(6,6,1843,'Disarm'),(6,6,2382,'Generic'),(6,6,2479,'Honorless Target'),(6,6,3050,'Detect'),(6,6,3127,'Parry'),(6,6,3275,'Linen Bandage'),(6,6,3276,'Heavy Linen Bandage'),(6,6,3277,'Wool Bandage'),(6,6,3278,'Heavy Wool Bandage'),(6,6,3365,'Opening'),(6,6,6233,'Closing'),(6,6,6246,'Closing'),(6,6,6247,'Opening'),(6,6,6477,'Opening'),(6,6,6478,'Opening'),(6,6,6603,'Attack'),(6,6,7266,'Duel'),(6,6,7267,'Grovel'),(6,6,7355,'Stuck'),(6,6,7928,'Silk Bandage'),(6,6,7929,'Heavy Silk Bandage'),(6,6,7934,'Anti-Venom'),(6,6,8386,'Attacking'),(6,6,8737,'Mail'),(6,6,9077,'Leather'),(6,6,9078,'Cloth'),(6,6,9125,'Generic'),(6,6,10840,'Mageweave Bandage'),(6,6,10841,'Heavy Mageweave Bandage'),(6,6,10846,'First Aid'),(6,6,18629,'Runecloth Bandage'),(6,6,18630,'Heavy Runecloth Bandage'),(6,6,20549,'War Stomp'),(6,6,20550,'Endurance'),(6,6,20551,'Nature Resistance'),(6,6,20552,'Cultivation'),(6,6,21651,'Opening'),(6,6,21652,'Closing'),(6,6,22027,'Remove Insignia'),(6,6,22810,'Opening - No Text'),(6,6,33391,'Journeyman Riding'),(6,6,45462,'Plague Strike'),(6,6,45477,'Icy Touch'),(6,6,45902,'Blood Strike'),(6,6,45903,'Offensive State (DND)'),(6,6,45927,'Summon Friend'),(6,6,47541,'Death Coil'),(6,6,48266,'Blood Presence'),(6,6,49410,'Forceful Deflection'),(6,6,49576,'Death Grip'),(6,6,52665,'Relic - Sigil'),(6,6,59879,'Blood Plague'),(6,6,59921,'Frost Fever'),(6,6,61437,'Opening'),(6,6,61455,'Runic Focus'),(22,6,6246,'Closing'),(2,6,669,'Language Orcish'),(7,6,81,'Dodge'),(7,6,196,'One-Handed Axes'),(7,6,197,'Two-Handed Axes'),(7,6,200,'Polearms'),(7,6,201,'One-Handed Swords'),(7,6,202,'Two-Handed Swords'),(7,6,203,'Unarmed'),(7,6,204,'Defense'),(7,6,522,'SPELLDEFENSE (DND)'),(7,6,750,'Plate Mail'),(7,6,1843,'Disarm'),(7,6,2382,'Generic'),(7,6,2479,'Honorless Target'),(7,6,3050,'Detect'),(7,6,3127,'Parry'),(7,6,3275,'Linen Bandage'),(7,6,3276,'Heavy Linen Bandage'),(7,6,3277,'Wool Bandage'),(7,6,3278,'Heavy Wool Bandage'),(7,6,3365,'Opening'),(7,6,6233,'Closing'),(7,6,6246,'Closing'),(7,6,6247,'Opening'),(7,6,6477,'Opening'),(7,6,6478,'Opening'),(7,6,6603,'Attack'),(7,6,7266,'Duel'),(7,6,7267,'Grovel'),(7,6,7355,'Stuck'),(7,6,7928,'Silk Bandage'),(7,6,7929,'Heavy Silk Bandage'),(7,6,7934,'Anti-Venom'),(7,6,8386,'Attacking'),(7,6,8737,'Mail'),(7,6,9077,'Leather'),(7,6,9078,'Cloth'),(7,6,9125,'Generic'),(7,6,10840,'Mageweave Bandage'),(7,6,10841,'Heavy Mageweave Bandage'),(7,6,10846,'First Aid'),(7,6,18629,'Runecloth Bandage'),(7,6,18630,'Heavy Runecloth Bandage'),(7,6,20589,'Escape Artist'),(7,6,20591,'Expansive Mind'),(7,6,20592,'Arcane Resistance'),(7,6,20593,'Engineering Specialization'),(7,6,21651,'Opening'),(7,6,21652,'Closing'),(7,6,22027,'Remove Insignia'),(7,6,22810,'Opening - No Text'),(7,6,33391,'Journeyman Riding'),(7,6,45462,'Plague Strike'),(7,6,45477,'Icy Touch'),(7,6,45902,'Blood Strike'),(7,6,45903,'Offensive State (DND)'),(7,6,45927,'Summon Friend'),(7,6,47541,'Death Coil'),(7,6,48266,'Blood Presence'),(7,6,49410,'Forceful Deflection'),(7,6,49576,'Death Grip'),(7,6,52665,'Relic - Sigil'),(7,6,59879,'Blood Plague'),(7,6,59921,'Frost Fever'),(7,6,61437,'Opening'),(7,6,61455,'Runic Focus'),(22,6,21652,'Closing'),(22,6,6233,'Closing'),(22,6,668,'Language Common'),(11,6,668,'Language Common'),(8,6,81,'Dodge'),(8,6,196,'One-Handed Axes'),(8,6,197,'Two-Handed Axes'),(8,6,200,'Polearms'),(8,6,201,'One-Handed Swords'),(8,6,202,'Two-Handed Swords'),(8,6,203,'Unarmed'),(8,6,204,'Defense'),(8,6,522,'SPELLDEFENSE (DND)'),(8,6,750,'Plate Mail'),(8,6,1843,'Disarm'),(8,6,2382,'Generic'),(8,6,2479,'Honorless Target'),(8,6,3050,'Detect'),(8,6,3127,'Parry'),(8,6,3275,'Linen Bandage'),(8,6,3276,'Heavy Linen Bandage'),(8,6,3277,'Wool Bandage'),(8,6,3278,'Heavy Wool Bandage'),(8,6,3365,'Opening'),(8,6,6233,'Closing'),(8,6,6246,'Closing'),(8,6,6247,'Opening'),(8,6,6477,'Opening'),(8,6,6478,'Opening'),(8,6,6603,'Attack'),(8,6,7266,'Duel'),(8,6,7267,'Grovel'),(8,6,7355,'Stuck'),(8,6,7928,'Silk Bandage'),(8,6,7929,'Heavy Silk Bandage'),(8,6,7934,'Anti-Venom'),(8,6,8386,'Attacking'),(8,6,8737,'Mail'),(8,6,9077,'Leather'),(8,6,9078,'Cloth'),(8,6,9125,'Generic'),(8,6,10840,'Mageweave Bandage'),(8,6,10841,'Heavy Mageweave Bandage'),(8,6,10846,'First Aid'),(8,6,18629,'Runecloth Bandage'),(8,6,18630,'Heavy Runecloth Bandage'),(8,6,20555,'Regeneration'),(8,6,20557,'Beast Slaying'),(8,6,20558,'Throwing Specialization'),(8,6,21651,'Opening'),(8,6,21652,'Closing'),(8,6,22027,'Remove Insignia'),(8,6,22810,'Opening - No Text'),(8,6,26290,'Bow Specialization'),(8,6,33391,'Journeyman Riding'),(8,6,45462,'Plague Strike'),(8,6,45477,'Icy Touch'),(8,6,45902,'Blood Strike'),(8,6,45903,'Offensive State (DND)'),(8,6,45927,'Summon Friend'),(8,6,47541,'Death Coil'),(8,6,48266,'Blood Presence'),(8,6,49410,'Forceful Deflection'),(8,6,49576,'Death Grip'),(8,6,26297,'Berserking'),(8,6,52665,'Relic - Sigil'),(8,6,58943,'Da Voodoo Shuffle'),(8,6,59879,'Blood Plague'),(8,6,59921,'Frost Fever'),(8,6,61437,'Opening'),(8,6,61455,'Runic Focus'),(7,6,668,'Language Common'),(4,6,668,'Language Common'),(10,6,81,'Dodge'),(10,6,196,'One-Handed Axes'),(10,6,197,'Two-Handed Axes'),(10,6,200,'Polearms'),(10,6,201,'One-Handed Swords'),(10,6,202,'Two-Handed Swords'),(10,6,203,'Unarmed'),(10,6,204,'Defense'),(10,6,522,'SPELLDEFENSE (DND)'),(10,6,750,'Plate Mail'),(10,6,822,'Magic Resistance'),(10,6,1843,'Disarm'),(10,6,2382,'Generic'),(10,6,2479,'Honorless Target'),(10,6,3050,'Detect'),(10,6,3127,'Parry'),(10,6,3275,'Linen Bandage'),(10,6,3276,'Heavy Linen Bandage'),(10,6,3277,'Wool Bandage'),(10,6,3278,'Heavy Wool Bandage'),(10,6,3365,'Opening'),(10,6,6233,'Closing'),(10,6,6246,'Closing'),(10,6,6247,'Opening'),(10,6,6477,'Opening'),(10,6,6478,'Opening'),(10,6,6603,'Attack'),(10,6,7266,'Duel'),(10,6,7267,'Grovel'),(10,6,7355,'Stuck'),(10,6,7928,'Silk Bandage'),(10,6,7929,'Heavy Silk Bandage'),(10,6,7934,'Anti-Venom'),(10,6,8386,'Attacking'),(10,6,8737,'Mail'),(10,6,9077,'Leather'),(10,6,9078,'Cloth'),(10,6,9125,'Generic'),(10,6,10840,'Mageweave Bandage'),(10,6,10841,'Heavy Mageweave Bandage'),(10,6,10846,'First Aid'),(10,6,18629,'Runecloth Bandage'),(10,6,18630,'Heavy Runecloth Bandage'),(10,6,21651,'Opening'),(10,6,21652,'Closing'),(10,6,22027,'Remove Insignia'),(10,6,22810,'Opening - No Text'),(10,6,28877,'Arcane Affinity'),(10,6,33391,'Journeyman Riding'),(10,6,45462,'Plague Strike'),(10,6,45477,'Icy Touch'),(10,6,45902,'Blood Strike'),(10,6,45903,'Offensive State (DND)'),(10,6,45927,'Summon Friend'),(10,6,47541,'Death Coil'),(10,6,48266,'Blood Presence'),(10,6,49410,'Forceful Deflection'),(10,6,49576,'Death Grip'),(10,6,50613,'Arcane Torrent'),(10,6,52665,'Relic - Sigil'),(10,6,59879,'Blood Plague'),(10,6,59921,'Frost Fever'),(10,6,61437,'Opening'),(10,6,61455,'Runic Focus'),(3,6,668,'Language Common'),(11,6,59539,'Shadow Resistance'),(11,6,81,'Dodge'),(11,6,196,'One-Handed Axes'),(11,6,197,'Two-Handed Axes'),(11,6,200,'Polearms'),(11,6,201,'One-Handed Swords'),(11,6,202,'Two-Handed Swords'),(11,6,203,'Unarmed'),(11,6,204,'Defense'),(11,6,522,'SPELLDEFENSE (DND)'),(1,6,668,'Language Common'),(11,6,750,'Plate Mail'),(11,6,1843,'Disarm'),(11,6,2382,'Generic'),(11,6,2479,'Honorless Target'),(11,6,3050,'Detect'),(11,6,3127,'Parry'),(11,6,3275,'Linen Bandage'),(11,6,3276,'Heavy Linen Bandage'),(11,6,3277,'Wool Bandage'),(11,6,3278,'Heavy Wool Bandage'),(11,6,3365,'Opening'),(11,6,6233,'Closing'),(11,6,6246,'Closing'),(11,6,6247,'Opening'),(11,6,6477,'Opening'),(11,6,6478,'Opening'),(11,6,6562,'Heroic Presence'),(11,6,6603,'Attack'),(11,6,7266,'Duel'),(11,6,7267,'Grovel'),(11,6,7355,'Stuck'),(11,6,7928,'Silk Bandage'),(11,6,7929,'Heavy Silk Bandage'),(11,6,7934,'Anti-Venom'),(11,6,8386,'Attacking'),(11,6,8737,'Mail'),(11,6,9077,'Leather'),(11,6,9078,'Cloth'),(11,6,9125,'Generic'),(11,6,10840,'Mageweave Bandage'),(11,6,10841,'Heavy Mageweave Bandage'),(11,6,10846,'First Aid'),(11,6,18629,'Runecloth Bandage'),(11,6,18630,'Heavy Runecloth Bandage'),(11,6,21651,'Opening'),(11,6,21652,'Closing'),(11,6,22027,'Remove Insignia'),(11,6,22810,'Opening - No Text'),(11,6,28875,'Gemcutting'),(11,6,33391,'Journeyman Riding'),(11,6,45462,'Plague Strike'),(11,6,45477,'Icy Touch'),(11,6,45902,'Blood Strike'),(11,6,45903,'Offensive State (DND)'),(11,6,45927,'Summon Friend'),(11,6,47541,'Death Coil'),(11,6,48266,'Blood Presence'),(11,6,49410,'Forceful Deflection'),(11,6,49576,'Death Grip'),(11,6,52665,'Relic - Sigil'),(11,6,59545,'Gift of the Naaru'),(11,6,59879,'Blood Plague'),(11,6,59921,'Frost Fever'),(11,6,61437,'Opening'),(11,6,61455,'Runic Focus'),(9,6,69070,'Rocket Jump'),(9,6,69041,'Rocket Barrage'),(9,6,69045,'Better Living Through Chemistry'),(9,6,69044,'Best Deals Anywhere'),(22,6,69270,'Language Gilnean'),(3,6,672,'Language Dwarven'),(22,6,6603,'Attack'),(22,6,2382,'Generic'),(5,6,17737,'Language Gutterspeak'),(9,6,69269,'Language Goblin'),(7,6,7340,'Language Gnomish'),(7,6,92680,'Shortblade Specialization'),(8,6,7341,'Language Troll'),(10,6,813,'Language Thalassian'),(9,6,2382,'Generic'),(22,6,9125,'Generic'),(9,6,9125,'Generic'),(22,6,8386,'Attacking'),(22,6,76282,'Armor Skills'),(22,6,76292,'Weapon Skills'),(22,6,203,'Unarmed'),(22,6,202,'Two-Handed Swords'),(22,6,199,'Two-Handed Maces'),(22,6,197,'Two-Handed Axes'),(22,6,200,'Polearms'),(22,6,201,'One-Handed Swords'),(22,6,198,'One-Handed Maces'),(22,6,196,'One-Handed Axes'),(22,6,204,'Defense'),(9,6,76282,'Armor Skills'),(9,6,76292,'Weapon Skills'),(9,6,203,'Unarmed'),(9,6,202,'Two-Handed Swords'),(9,6,197,'Two-Handed Axes'),(9,6,200,'Polearms'),(9,6,201,'One-Handed Swords'),(9,6,198,'One-Handed Maces'),(9,6,196,'One-Handed Axes'),(9,6,81,'Dodge'),(9,6,204,'Defense'),(22,6,750,'Plate Mail'),(9,6,750,'Plate Mail'),(22,6,8737,'Mail'),(9,6,8737,'Mail'),(22,6,9077,'Leather'),(9,6,9077,'Leather'),(22,6,9078,'Cloth'),(9,6,9078,'Cloth'),(3,6,92682,'Explorer'),(9,6,69042,'Time is Money'),(22,6,81,'Dodge'),(22,6,52665,'Relic - Sigil'),(9,6,52665,'Relic - Sigil'),(9,6,199,'Two-Handed Maces'),(1,6,198,'One-Handed Maces'),(1,6,199,'Two-Handed Maces'),(1,6,76292,'Weapon Skills'),(1,6,76282,'Armor Skills'),(2,6,198,'One-Handed Maces'),(2,6,199,'Two-Handed Maces'),(2,6,76292,'Weapon Skills'),(2,6,76282,'Armor Skills'),(3,6,198,'One-Handed Maces'),(3,6,199,'Two-Handed Maces'),(3,6,76292,'Weapon Skills'),(3,6,76282,'Armor Skills'),(4,6,198,'One-Handed Maces'),(4,6,199,'Two-Handed Maces'),(4,6,76292,'Weapon Skills'),(4,6,76282,'Armor Skills'),(5,6,198,'One-Handed Maces'),(5,6,199,'Two-Handed Maces'),(5,6,76292,'Weapon Skills'),(5,6,76282,'Armor Skills'),(6,6,198,'One-Handed Maces'),(6,6,199,'Two-Handed Maces'),(6,6,76292,'Weapon Skills'),(6,6,76282,'Armor Skills'),(7,6,198,'One-Handed Maces'),(7,6,199,'Two-Handed Maces'),(7,6,76292,'Weapon Skills'),(7,6,76282,'Armor Skills'),(8,6,198,'One-Handed Maces'),(8,6,199,'Two-Handed Maces'),(8,6,76292,'Weapon Skills'),(8,6,76282,'Armor Skills'),(10,6,198,'One-Handed Maces'),(10,6,199,'Two-Handed Maces'),(10,6,76292,'Weapon Skills'),(10,6,76282,'Armor Skills'),(11,6,198,'One-Handed Maces'),(11,6,199,'Two-Handed Maces'),(11,6,76292,'Weapon Skills'),(11,6,76282,'Armor Skills'),(6,6,670,'Language Taurahe'),(22,6,3050,'Detect'),(22,6,1843,'Disarm'),(22,6,7267,'Grovel'),(22,6,2479,'Honorless Target'),(22,6,61437,'Opening'),(22,6,21651,'Opening'),(22,6,6478,'Opening'),(22,6,6477,'Opening'),(22,6,6247,'Opening'),(22,6,3365,'Opening'),(22,6,22810,'Opening - No Text'),(22,6,22027,'Remove Insignia'),(22,6,522,'SPELLDEFENSE (DND)'),(22,6,7355,'Stuck'),(22,6,45927,'Summon Friend'),(22,6,7934,'Anti-Venom'),(22,6,59879,'Blood Plague'),(22,6,48266,'Blood Presence'),(22,6,45902,'Blood Strike'),(22,6,47541,'Death Coil'),(22,6,49576,'Death Grip'),(22,6,10846,'First Aid'),(22,6,49410,'Forceful Deflection'),(22,6,59921,'Frost Fever'),(22,6,3276,'Heavy Linen Bandage'),(22,6,10841,'Heavy Mageweave Bandage'),(22,6,18630,'Heavy Runecloth Bandage'),(22,6,7929,'Heavy Silk Bandage'),(22,6,3278,'Heavy Wool Bandage'),(22,6,45477,'Icy Touch'),(22,6,33391,'Journeyman Riding'),(22,6,3275,'Linen Bandage'),(22,6,10840,'Mageweave Bandage'),(22,6,45462,'Plague Strike'),(22,6,18629,'Runecloth Bandage'),(22,6,61455,'Runic Focus'),(22,6,7928,'Silk Bandage'),(22,6,3277,'Wool Bandage'),(22,6,7266,'Duel'),(22,6,45903,'Offensive State (DND)'),(22,6,3127,'Parry'),(22,6,68976,'Aberration'),(22,6,68992,'Darkflight'),(22,6,68978,'Flayer'),(22,6,69001,'Transform: Worgen'),(22,6,68996,'Two Forms'),(22,6,68975,'Viciousness'),(9,6,69046,'Pack Hobgoblin'),(1,6,674,'Dual Wield'),(2,6,674,'Dual Wield'),(3,6,674,'Dual Wield'),(4,6,674,'Dual Wield'),(5,6,674,'Dual Wield'),(7,6,674,'Dual Wield'),(6,6,674,'Dual Wield'),(8,6,674,'Dual Wield'),(9,6,674,'Dual Wield'),(10,6,674,'Dual Wield'),(11,6,674,'Dual Wield'),(22,6,674,'Dual Wield'),(9,6,7266,'Duel'),(9,6,48266,'Frost Presence'),(9,6,6603,'Auto Attack'),(9,6,61455,'Runic Focus'),(9,6,59921,'Frost Fever'),(9,6,45477,'Icy Touch'),(9,6,49576,'Death Grip'),(9,6,45462,'Plague Strike'),(9,6,45902,'Blood Strike'),(9,6,59879,'Blood Plague'),(9,6,23548,'Parry'),(9,6,47541,'Death Coil'),(22,1,68976,'Aberration'),(22,1,68992,'Darkflight'),(22,1,94293,'Enable Worgen Altered Form'),(22,1,68978,'Flayer'),(22,1,87840,'Running Wild'),(22,1,68996,'Two Forms'),(22,1,68975,'Viciousness'),(22,9,68976,'Aberration'),(22,9,68992,'Darkflight'),(22,9,94293,'Enable Worgen Altered Form'),(22,9,68978,'Flayer'),(22,9,87840,'Running Wild'),(22,9,68996,'Two Forms'),(22,9,68975,'Viciousness'),(22,4,68976,'Aberration'),(22,4,68992,'Darkflight'),(22,4,94293,'Enable Worgen Altered Form'),(22,4,68978,'Flayer'),(22,4,87840,'Running Wild'),(22,4,68996,'Two Forms'),(22,4,68975,'Viciousness'),(22,5,68976,'Aberration'),(22,5,68992,'Darkflight'),(22,5,94293,'Enable Worgen Altered Form'),(22,5,68978,'Flayer'),(22,5,87840,'Running Wild'),(22,5,68996,'Two Forms'),(22,5,68975,'Viciousness'),(22,8,68976,'Aberration'),(22,8,68992,'Darkflight'),(22,8,94293,'Enable Worgen Altered Form'),(22,8,68978,'Flayer'),(22,8,87840,'Running Wild'),(22,8,68996,'Two Forms'),(22,8,68975,'Viciousness'),(22,3,68976,'Aberration'),(22,3,68992,'Darkflight'),(22,3,94293,'Enable Worgen Altered Form'),(22,3,68978,'Flayer'),(22,3,87840,'Running Wild'),(22,3,68996,'Two Forms'),(22,3,68975,'Viciousness'),(22,11,68976,'Aberration'),(22,11,68992,'Darkflight'),(22,11,94293,'Enable Worgen Altered Form'),(22,11,68978,'Flayer'),(22,11,87840,'Running Wild'),(22,11,68996,'Two Forms'),(22,11,68975,'Viciousness'),(22,6,94293,'Enable Worgen Altered Form'),(22,6,87840,'Running Wild');
/*!40000 ALTER TABLE `playercreateinfo_spell` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `playercreateinfo_spell_custom`
--

DROP TABLE IF EXISTS `playercreateinfo_spell_custom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `playercreateinfo_spell_custom` (
  `race` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `class` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `Spell` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `Note` varchar(255) DEFAULT NULL,
  `Active` tinyint(3) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`race`,`class`,`Spell`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `playercreateinfo_spell_custom`
--

LOCK TABLES `playercreateinfo_spell_custom` WRITE;
/*!40000 ALTER TABLE `playercreateinfo_spell_custom` DISABLE KEYS */;
/*!40000 ALTER TABLE `playercreateinfo_spell_custom` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `points_of_interest`
--

DROP TABLE IF EXISTS `points_of_interest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `points_of_interest` (
  `entry` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `x` float NOT NULL DEFAULT '0',
  `y` float NOT NULL DEFAULT '0',
  `icon` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `flags` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `data` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `icon_name` text NOT NULL,
  PRIMARY KEY (`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Points of interest';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `points_of_interest`
--

LOCK TABLES `points_of_interest` WRITE;
/*!40000 ALTER TABLE `points_of_interest` DISABLE KEYS */;
/*!40000 ALTER TABLE `points_of_interest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pool_creature`
--

DROP TABLE IF EXISTS `pool_creature`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pool_creature` (
  `guid` int(10) unsigned NOT NULL DEFAULT '0',
  `pool_entry` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `chance` float unsigned NOT NULL DEFAULT '0',
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`guid`),
  KEY `idx_guid` (`guid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Pool creatures';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pool_creature`
--

LOCK TABLES `pool_creature` WRITE;
/*!40000 ALTER TABLE `pool_creature` DISABLE KEYS */;
/*!40000 ALTER TABLE `pool_creature` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pool_gameobject`
--

DROP TABLE IF EXISTS `pool_gameobject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pool_gameobject` (
  `guid` int(10) unsigned NOT NULL DEFAULT '0',
  `pool_entry` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `chance` float unsigned NOT NULL DEFAULT '0',
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`guid`),
  KEY `idx_guid` (`guid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Pool gameobjects';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pool_gameobject`
--

LOCK TABLES `pool_gameobject` WRITE;
/*!40000 ALTER TABLE `pool_gameobject` DISABLE KEYS */;
/*!40000 ALTER TABLE `pool_gameobject` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pool_pool`
--

DROP TABLE IF EXISTS `pool_pool`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pool_pool` (
  `pool_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `mother_pool` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `chance` float NOT NULL DEFAULT '0',
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`pool_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Pool System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pool_pool`
--

LOCK TABLES `pool_pool` WRITE;
/*!40000 ALTER TABLE `pool_pool` DISABLE KEYS */;
/*!40000 ALTER TABLE `pool_pool` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pool_quest`
--

DROP TABLE IF EXISTS `pool_quest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pool_quest` (
  `entry` int(10) unsigned NOT NULL DEFAULT '0',
  `pool_entry` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entry`),
  KEY `idx_guid` (`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pool_quest`
--

LOCK TABLES `pool_quest` WRITE;
/*!40000 ALTER TABLE `pool_quest` DISABLE KEYS */;
/*!40000 ALTER TABLE `pool_quest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pool_quest_save`
--

DROP TABLE IF EXISTS `pool_quest_save`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pool_quest_save` (
  `pool_id` int(10) unsigned NOT NULL DEFAULT '0',
  `quest_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`pool_id`,`quest_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pool_quest_save`
--

LOCK TABLES `pool_quest_save` WRITE;
/*!40000 ALTER TABLE `pool_quest_save` DISABLE KEYS */;
/*!40000 ALTER TABLE `pool_quest_save` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pool_template`
--

DROP TABLE IF EXISTS `pool_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pool_template` (
  `entry` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'Pool entry',
  `max_limit` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Max number of objects (0) is no limit',
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Pool Template';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pool_template`
--

LOCK TABLES `pool_template` WRITE;
/*!40000 ALTER TABLE `pool_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `pool_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prospecting_loot_template`
--

DROP TABLE IF EXISTS `prospecting_loot_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prospecting_loot_template` (
  `entry` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `item` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `ChanceOrQuestChance` float NOT NULL DEFAULT '100',
  `lootmode` smallint(5) unsigned NOT NULL DEFAULT '1',
  `groupid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `mincountOrRef` mediumint(9) NOT NULL DEFAULT '1',
  `maxcount` tinyint(3) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`entry`,`item`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='Loot System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prospecting_loot_template`
--

LOCK TABLES `prospecting_loot_template` WRITE;
/*!40000 ALTER TABLE `prospecting_loot_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `prospecting_loot_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quest_end_scripts`
--

DROP TABLE IF EXISTS `quest_end_scripts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quest_end_scripts` (
  `id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `delay` int(10) unsigned NOT NULL DEFAULT '0',
  `command` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `datalong` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `datalong2` int(10) unsigned NOT NULL DEFAULT '0',
  `dataint` int(11) NOT NULL DEFAULT '0',
  `x` float NOT NULL DEFAULT '0',
  `y` float NOT NULL DEFAULT '0',
  `z` float NOT NULL DEFAULT '0',
  `o` float NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Quest end scripts';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quest_end_scripts`
--

LOCK TABLES `quest_end_scripts` WRITE;
/*!40000 ALTER TABLE `quest_end_scripts` DISABLE KEYS */;
/*!40000 ALTER TABLE `quest_end_scripts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quest_poi`
--

DROP TABLE IF EXISTS `quest_poi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quest_poi` (
  `questId` int(10) unsigned NOT NULL DEFAULT '0',
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `objIndex` int(10) NOT NULL DEFAULT '0',
  `mapid` int(10) unsigned NOT NULL DEFAULT '0',
  `WorldMapAreaId` int(10) unsigned NOT NULL DEFAULT '0',
  `FloorId` int(10) unsigned NOT NULL DEFAULT '0',
  `unk3` int(10) unsigned NOT NULL DEFAULT '0',
  `unk4` int(10) unsigned NOT NULL DEFAULT '0',
  KEY `questId` (`questId`,`id`),
  KEY `id` (`id`,`questId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci COMMENT='CTDB Quest poi System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quest_poi`
--

LOCK TABLES `quest_poi` WRITE;
/*!40000 ALTER TABLE `quest_poi` DISABLE KEYS */;
/*!40000 ALTER TABLE `quest_poi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quest_poi_points`
--

DROP TABLE IF EXISTS `quest_poi_points`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quest_poi_points` (
  `questId` int(10) unsigned NOT NULL DEFAULT '0',
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `idx` int(10) unsigned NOT NULL DEFAULT '0',
  `x` int(10) NOT NULL DEFAULT '0',
  `y` int(10) NOT NULL DEFAULT '0',
  KEY `questId_id` (`questId`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci COMMENT='CTDB Quest poi points';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quest_poi_points`
--

LOCK TABLES `quest_poi_points` WRITE;
/*!40000 ALTER TABLE `quest_poi_points` DISABLE KEYS */;
/*!40000 ALTER TABLE `quest_poi_points` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quest_start_scripts`
--

DROP TABLE IF EXISTS `quest_start_scripts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quest_start_scripts` (
  `id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `delay` int(10) unsigned NOT NULL DEFAULT '0',
  `command` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `datalong` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `datalong2` int(10) unsigned NOT NULL DEFAULT '0',
  `dataint` int(11) NOT NULL DEFAULT '0',
  `x` float NOT NULL DEFAULT '0',
  `y` float NOT NULL DEFAULT '0',
  `z` float NOT NULL DEFAULT '0',
  `o` float NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Quest start scripts';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quest_start_scripts`
--

LOCK TABLES `quest_start_scripts` WRITE;
/*!40000 ALTER TABLE `quest_start_scripts` DISABLE KEYS */;
/*!40000 ALTER TABLE `quest_start_scripts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quest_template`
--

DROP TABLE IF EXISTS `quest_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quest_template` (
  `entry` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `Method` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `ZoneOrSort` smallint(6) NOT NULL DEFAULT '0',
  `SkillOrClassMask` mediumint(8) NOT NULL DEFAULT '0',
  `MinLevel` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `MaxLevel` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `QuestLevel` smallint(3) NOT NULL DEFAULT '1',
  `Type` smallint(5) unsigned NOT NULL DEFAULT '0',
  `RequiredRaces` int(15) unsigned NOT NULL DEFAULT '0',
  `RequiredSkillValue` smallint(5) unsigned NOT NULL DEFAULT '0',
  `RepObjectiveFaction` smallint(5) unsigned NOT NULL DEFAULT '0',
  `RepObjectiveValue` mediumint(9) NOT NULL DEFAULT '0',
  `RepObjectiveFaction2` smallint(5) unsigned NOT NULL DEFAULT '0',
  `RepObjectiveValue2` mediumint(9) NOT NULL DEFAULT '0',
  `RequiredMinRepFaction` smallint(5) unsigned NOT NULL DEFAULT '0',
  `RequiredMinRepValue` mediumint(9) NOT NULL DEFAULT '0',
  `RequiredMaxRepFaction` smallint(5) unsigned NOT NULL DEFAULT '0',
  `RequiredMaxRepValue` mediumint(9) NOT NULL DEFAULT '0',
  `SuggestedPlayers` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `LimitTime` int(10) unsigned NOT NULL DEFAULT '0',
  `QuestFlags` int(10) unsigned NOT NULL DEFAULT '0',
  `SpecialFlags` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `CharTitleId` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `PlayersSlain` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `BonusTalents` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `RewardArenaPoints` smallint(5) unsigned NOT NULL DEFAULT '0',
  `PrevQuestId` mediumint(9) NOT NULL DEFAULT '0',
  `NextQuestId` mediumint(9) NOT NULL DEFAULT '0',
  `ExclusiveGroup` mediumint(9) NOT NULL DEFAULT '0',
  `NextQuestInChain` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `RewXPId` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `SrcItemId` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `SrcItemCount` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `SrcSpell` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `Title` text,
  `Details` text,
  `Objectives` text,
  `OfferRewardText` text,
  `RequestItemsText` text,
  `EndText` text,
  `CompletedText` text,
  `ObjectiveText1` text,
  `ObjectiveText2` text,
  `ObjectiveText3` text,
  `ObjectiveText4` text,
  `ReqItemId1` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `ReqItemId2` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `ReqItemId3` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `ReqItemId4` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `ReqItemId5` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `ReqItemId6` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `ReqItemCount1` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ReqItemCount2` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ReqItemCount3` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ReqItemCount4` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ReqItemCount5` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ReqItemCount6` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ReqSourceId1` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `ReqSourceId2` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `ReqSourceId3` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `ReqSourceId4` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `ReqSourceCount1` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ReqSourceCount2` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ReqSourceCount3` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ReqSourceCount4` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ReqCreatureOrGOId1` int(15) NOT NULL DEFAULT '0',
  `ReqCreatureOrGOId2` int(15) NOT NULL DEFAULT '0',
  `ReqCreatureOrGOId3` int(15) NOT NULL DEFAULT '0',
  `ReqCreatureOrGOId4` int(15) NOT NULL DEFAULT '0',
  `ReqCreatureOrGOCount1` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ReqCreatureOrGOCount2` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ReqCreatureOrGOCount3` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ReqCreatureOrGOCount4` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ReqSpellCast1` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `ReqSpellCast2` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `ReqSpellCast3` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `ReqSpellCast4` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `RewChoiceItemId1` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `RewChoiceItemId2` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `RewChoiceItemId3` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `RewChoiceItemId4` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `RewChoiceItemId5` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `RewChoiceItemId6` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `RewChoiceItemCount1` smallint(5) unsigned NOT NULL DEFAULT '0',
  `RewChoiceItemCount2` smallint(5) unsigned NOT NULL DEFAULT '0',
  `RewChoiceItemCount3` smallint(5) unsigned NOT NULL DEFAULT '0',
  `RewChoiceItemCount4` smallint(5) unsigned NOT NULL DEFAULT '0',
  `RewChoiceItemCount5` smallint(5) unsigned NOT NULL DEFAULT '0',
  `RewChoiceItemCount6` smallint(5) unsigned NOT NULL DEFAULT '0',
  `RewItemId1` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `RewItemId2` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `RewItemId3` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `RewItemId4` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `RewItemCount1` smallint(5) unsigned NOT NULL DEFAULT '0',
  `RewItemCount2` smallint(5) unsigned NOT NULL DEFAULT '0',
  `RewItemCount3` smallint(5) unsigned NOT NULL DEFAULT '0',
  `RewItemCount4` smallint(5) unsigned NOT NULL DEFAULT '0',
  `RewRepFaction1` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'faction id from Faction.dbc in this case',
  `RewRepFaction2` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'faction id from Faction.dbc in this case',
  `RewRepFaction3` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'faction id from Faction.dbc in this case',
  `RewRepFaction4` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'faction id from Faction.dbc in this case',
  `RewRepFaction5` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'faction id from Faction.dbc in this case',
  `RewRepValueId1` mediumint(6) NOT NULL DEFAULT '0',
  `RewRepValueId2` mediumint(6) NOT NULL DEFAULT '0',
  `RewRepValueId3` mediumint(6) NOT NULL DEFAULT '0',
  `RewRepValueId4` mediumint(6) NOT NULL DEFAULT '0',
  `RewRepValueId5` mediumint(6) NOT NULL DEFAULT '0',
  `RewRepValue1` int(15) NOT NULL DEFAULT '0',
  `RewRepValue2` int(15) NOT NULL DEFAULT '0',
  `RewRepValue3` int(15) NOT NULL DEFAULT '0',
  `RewRepValue4` int(15) NOT NULL DEFAULT '0',
  `RewRepValue5` int(15) NOT NULL DEFAULT '0',
  `RewHonorAddition` int(10) NOT NULL DEFAULT '0',
  `RewHonorMultiplier` float NOT NULL DEFAULT '1',
  `unk0` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `RewOrReqMoney` int(11) NOT NULL DEFAULT '0',
  `RewMoneyMaxLevel` int(10) unsigned NOT NULL DEFAULT '0',
  `RewSpell` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `RewSpellCast` int(11) NOT NULL DEFAULT '0',
  `RewMailTemplateId` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `RewMailDelaySecs` int(11) unsigned NOT NULL DEFAULT '0',
  `PointMapId` smallint(5) unsigned NOT NULL DEFAULT '0',
  `PointX` float NOT NULL DEFAULT '0',
  `PointY` float NOT NULL DEFAULT '0',
  `PointOpt` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `DetailsEmote1` smallint(5) unsigned NOT NULL DEFAULT '0',
  `DetailsEmote2` smallint(5) unsigned NOT NULL DEFAULT '0',
  `DetailsEmote3` smallint(5) unsigned NOT NULL DEFAULT '0',
  `DetailsEmote4` smallint(5) unsigned NOT NULL DEFAULT '0',
  `DetailsEmoteDelay1` int(11) unsigned NOT NULL DEFAULT '0',
  `DetailsEmoteDelay2` int(11) unsigned NOT NULL DEFAULT '0',
  `DetailsEmoteDelay3` int(11) unsigned NOT NULL DEFAULT '0',
  `DetailsEmoteDelay4` int(11) unsigned NOT NULL DEFAULT '0',
  `IncompleteEmote` smallint(5) unsigned NOT NULL DEFAULT '0',
  `CompleteEmote` smallint(5) unsigned NOT NULL DEFAULT '0',
  `OfferRewardEmote1` smallint(5) unsigned NOT NULL DEFAULT '0',
  `OfferRewardEmote2` smallint(5) unsigned NOT NULL DEFAULT '0',
  `OfferRewardEmote3` smallint(5) unsigned NOT NULL DEFAULT '0',
  `OfferRewardEmote4` smallint(5) unsigned NOT NULL DEFAULT '0',
  `OfferRewardEmoteDelay1` int(11) unsigned NOT NULL DEFAULT '0',
  `OfferRewardEmoteDelay2` int(11) unsigned NOT NULL DEFAULT '0',
  `OfferRewardEmoteDelay3` int(11) unsigned NOT NULL DEFAULT '0',
  `OfferRewardEmoteDelay4` int(11) unsigned NOT NULL DEFAULT '0',
  `RewSkillLineId` int(11) unsigned NOT NULL DEFAULT '0',
  `RewSkillPoints` int(11) unsigned NOT NULL DEFAULT '0',
  `RewRepMask` int(11) unsigned NOT NULL DEFAULT '0',
  `QuestGiverPortrait` int(11) unsigned NOT NULL DEFAULT '0',
  `QuestTurnInPortrait` int(11) unsigned NOT NULL DEFAULT '0',
  `RewCurrencyId1` int(11) unsigned NOT NULL DEFAULT '0',
  `RewCurrencyCount1` int(11) unsigned NOT NULL DEFAULT '0',
  `RewCurrencyId2` int(11) unsigned NOT NULL DEFAULT '0',
  `RewCurrencyCount2` int(11) unsigned NOT NULL DEFAULT '0',
  `RewCurrencyId3` int(11) unsigned NOT NULL DEFAULT '0',
  `RewCurrencyCount3` int(11) unsigned NOT NULL DEFAULT '0',
  `RewCurrencyId4` int(11) unsigned NOT NULL DEFAULT '0',
  `RewCurrencyCount4` int(11) unsigned NOT NULL DEFAULT '0',
  `ReqCurrencyId1` int(11) unsigned NOT NULL DEFAULT '0',
  `ReqCurrencyCount1` int(11) unsigned NOT NULL DEFAULT '0',
  `ReqCurrencyId2` int(11) unsigned NOT NULL DEFAULT '0',
  `ReqCurrencyCount2` int(11) unsigned NOT NULL DEFAULT '0',
  `ReqCurrencyId3` int(11) unsigned NOT NULL DEFAULT '0',
  `ReqCurrencyCount3` int(11) unsigned NOT NULL DEFAULT '0',
  `ReqCurrencyId4` int(11) unsigned NOT NULL DEFAULT '0',
  `ReqCurrencyCount4` int(11) unsigned NOT NULL DEFAULT '0',
  `QuestGiverPortraitText` text,
  `QuestGiverPortraitUnk` text,
  `QuestTurnInPortraitText` text,
  `QuestTurnInPortraitUnk` text,
  `QuestTargetMark` int(11) DEFAULT '0',
  `QuestStartType` int(11) DEFAULT '0',
  `SoundAccept` int(11) unsigned NOT NULL DEFAULT '0',
  `SoundTurnIn` int(11) unsigned NOT NULL DEFAULT '0',
  `RequiredSpell` int(11) unsigned NOT NULL DEFAULT '0',
  `StartScript` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `CompleteScript` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `WDBVerified` smallint(5) DEFAULT '1',
  PRIMARY KEY (`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Quest System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quest_template`
--

LOCK TABLES `quest_template` WRITE;
/*!40000 ALTER TABLE `quest_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `quest_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reference_loot_template`
--

DROP TABLE IF EXISTS `reference_loot_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reference_loot_template` (
  `entry` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `item` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `ChanceOrQuestChance` float NOT NULL DEFAULT '100',
  `lootmode` smallint(5) unsigned NOT NULL DEFAULT '1',
  `groupid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `mincountOrRef` mediumint(9) NOT NULL DEFAULT '1',
  `maxcount` tinyint(3) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`entry`,`item`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='Loot System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reference_loot_template`
--

LOCK TABLES `reference_loot_template` WRITE;
/*!40000 ALTER TABLE `reference_loot_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `reference_loot_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `remote_task`
--

DROP TABLE IF EXISTS `remote_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `remote_task` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `command` varchar(255) NOT NULL,
  `response` varchar(255) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `execute_count` int(11) NOT NULL,
  `last_updated` datetime NOT NULL,
  `realm` varchar(255) DEFAULT NULL,
  `status` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `remote_task_status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `remote_task`
--

LOCK TABLES `remote_task` WRITE;
/*!40000 ALTER TABLE `remote_task` DISABLE KEYS */;
/*!40000 ALTER TABLE `remote_task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reputation_reward_rate`
--

DROP TABLE IF EXISTS `reputation_reward_rate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reputation_reward_rate` (
  `faction` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `quest_rate` float NOT NULL DEFAULT '1',
  `creature_rate` float NOT NULL DEFAULT '1',
  `spell_rate` float NOT NULL DEFAULT '1',
  PRIMARY KEY (`faction`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reputation_reward_rate`
--

LOCK TABLES `reputation_reward_rate` WRITE;
/*!40000 ALTER TABLE `reputation_reward_rate` DISABLE KEYS */;
/*!40000 ALTER TABLE `reputation_reward_rate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reputation_spillover_template`
--

DROP TABLE IF EXISTS `reputation_spillover_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reputation_spillover_template` (
  `faction` smallint(6) unsigned NOT NULL DEFAULT '0' COMMENT 'faction entry',
  `faction1` smallint(6) unsigned NOT NULL DEFAULT '0' COMMENT 'faction to give spillover for',
  `rate_1` float NOT NULL DEFAULT '0' COMMENT 'the given rep points * rate',
  `rank_1` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'max rank,above this will not give any spillover',
  `faction2` smallint(6) unsigned NOT NULL DEFAULT '0',
  `rate_2` float NOT NULL DEFAULT '0',
  `rank_2` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `faction3` smallint(6) unsigned NOT NULL DEFAULT '0',
  `rate_3` float NOT NULL DEFAULT '0',
  `rank_3` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `faction4` smallint(6) unsigned NOT NULL DEFAULT '0',
  `rate_4` float NOT NULL DEFAULT '0',
  `rank_4` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`faction`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='Reputation spillover reputation gain';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reputation_spillover_template`
--

LOCK TABLES `reputation_spillover_template` WRITE;
/*!40000 ALTER TABLE `reputation_spillover_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `reputation_spillover_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reserved_name`
--

DROP TABLE IF EXISTS `reserved_name`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reserved_name` (
  `name` varchar(12) NOT NULL DEFAULT '',
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='Player Reserved Names';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reserved_name`
--

LOCK TABLES `reserved_name` WRITE;
/*!40000 ALTER TABLE `reserved_name` DISABLE KEYS */;
/*!40000 ALTER TABLE `reserved_name` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `script_texts`
--

DROP TABLE IF EXISTS `script_texts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `script_texts` (
  `npc_entry` mediumint(8) NOT NULL DEFAULT '0' COMMENT 'creature_template entry',
  `entry` mediumint(8) NOT NULL,
  `content_default` text NOT NULL,
  `content_loc1` text,
  `content_loc2` text,
  `content_loc3` text,
  `content_loc4` text,
  `content_loc5` text,
  `content_loc6` text,
  `content_loc7` text,
  `content_loc8` text,
  `sound` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `language` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `emote` smallint(5) unsigned NOT NULL DEFAULT '0',
  `comment` text,
  PRIMARY KEY (`npc_entry`,`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='Script Texts';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `script_texts`
--

LOCK TABLES `script_texts` WRITE;
/*!40000 ALTER TABLE `script_texts` DISABLE KEYS */;
/*!40000 ALTER TABLE `script_texts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `script_waypoint`
--

DROP TABLE IF EXISTS `script_waypoint`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `script_waypoint` (
  `entry` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'creature_template entry',
  `pointid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `location_x` float NOT NULL DEFAULT '0',
  `location_y` float NOT NULL DEFAULT '0',
  `location_z` float NOT NULL DEFAULT '0',
  `waittime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'waittime in millisecs',
  `point_comment` text,
  PRIMARY KEY (`entry`,`pointid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='Script Creature waypoints';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `script_waypoint`
--

LOCK TABLES `script_waypoint` WRITE;
/*!40000 ALTER TABLE `script_waypoint` DISABLE KEYS */;
/*!40000 ALTER TABLE `script_waypoint` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `season_linked_event`
--

DROP TABLE IF EXISTS `season_linked_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `season_linked_event` (
  `season` int(3) unsigned NOT NULL DEFAULT '0',
  `event` int(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`season`),
  UNIQUE KEY `season` (`season`,`event`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Season linked by events system';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `season_linked_event`
--

LOCK TABLES `season_linked_event` WRITE;
/*!40000 ALTER TABLE `season_linked_event` DISABLE KEYS */;
/*!40000 ALTER TABLE `season_linked_event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `skill_discovery_template`
--

DROP TABLE IF EXISTS `skill_discovery_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `skill_discovery_template` (
  `spellId` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'SpellId of the discoverable spell',
  `reqSpell` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'spell requirement',
  `reqSkillValue` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'skill points requirement',
  `chance` float NOT NULL DEFAULT '0' COMMENT 'chance to discover',
  PRIMARY KEY (`spellId`,`reqSpell`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='Skill Discovery System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `skill_discovery_template`
--

LOCK TABLES `skill_discovery_template` WRITE;
/*!40000 ALTER TABLE `skill_discovery_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `skill_discovery_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `skill_extra_item_template`
--

DROP TABLE IF EXISTS `skill_extra_item_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `skill_extra_item_template` (
  `spellId` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'SpellId of the item creation spell',
  `requiredSpecialization` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'Specialization spell id',
  `additionalCreateChance` float NOT NULL DEFAULT '0' COMMENT 'chance to create add',
  `additionalMaxNum` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'max num of adds',
  PRIMARY KEY (`spellId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='Skill Specialization System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `skill_extra_item_template`
--

LOCK TABLES `skill_extra_item_template` WRITE;
/*!40000 ALTER TABLE `skill_extra_item_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `skill_extra_item_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `skill_fishing_base_level`
--

DROP TABLE IF EXISTS `skill_fishing_base_level`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `skill_fishing_base_level` (
  `entry` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'Area identifier',
  `skill` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Base skill level requirement',
  PRIMARY KEY (`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='Fishing system';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `skill_fishing_base_level`
--

LOCK TABLES `skill_fishing_base_level` WRITE;
/*!40000 ALTER TABLE `skill_fishing_base_level` DISABLE KEYS */;
/*!40000 ALTER TABLE `skill_fishing_base_level` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `skinning_loot_template`
--

DROP TABLE IF EXISTS `skinning_loot_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `skinning_loot_template` (
  `entry` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `item` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `ChanceOrQuestChance` float NOT NULL DEFAULT '100',
  `lootmode` smallint(5) unsigned NOT NULL DEFAULT '1',
  `groupid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `mincountOrRef` mediumint(9) NOT NULL DEFAULT '1',
  `maxcount` tinyint(3) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`entry`,`item`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='Loot System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `skinning_loot_template`
--

LOCK TABLES `skinning_loot_template` WRITE;
/*!40000 ALTER TABLE `skinning_loot_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `skinning_loot_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `skyfire_string`
--

DROP TABLE IF EXISTS `skyfire_string`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `skyfire_string` (
  `entry` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `content_default` text NOT NULL,
  `content_loc1` text,
  `content_loc2` text,
  `content_loc3` text,
  `content_loc4` text,
  `content_loc5` text,
  `content_loc6` text,
  `content_loc7` text,
  `content_loc8` text,
  PRIMARY KEY (`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `skyfire_string`
--

LOCK TABLES `skyfire_string` WRITE;
/*!40000 ALTER TABLE `skyfire_string` DISABLE KEYS */;
INSERT INTO `skyfire_string` VALUES (1,'You should select a character or a creature.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,'You should select a creature.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,'|cff00ccff[SERVER ANNOUNCE]:|r %s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,'|cff00ccff[Event Active]:|r %s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,'|cffff6060Failed: |cff00ccffThere is no help for that command|r',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6,'|cffff6060Failed: |cff00ccffThere is no such command|r',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(7,'|cffff6060Failed: |cff00ccffThere is no such subcommand|r',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(8,'Command %s have subcommands:%s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(9,'Commands available to you:',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10,'|cffff6060Failed: |cff00ccffIncorrect syntax.|r',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(11,'Your account level is: %i',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(12,'Active connections: %u (max: %u) Queued connections: %u (max: %u)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(13,'Server uptime: %s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(14,'|cff00ff00Success: |cff00ccffYour character is now saved|r',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(15,'|cff00ff00Success: |cff00ccffAll players saved|r',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(16,'There are the following active GMs on this server:',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(17,'There are no GMs currently logged in on this server.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(18,'Cannot do that while flying.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(19,'Cannot do that in Battlegrounds.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(20,'Target is flying you can\'t do that.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(21,'%s is flying command failed.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(22,'You are not mounted so you can\'t dismount.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(23,'Cannot do that while fighting.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(24,'You used it recently.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(25,'Password not changed (unknown error)!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(26,'The password was changed',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(27,'The old password is wrong',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(28,'Your account is now locked.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(29,'Your account is now unlocked.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(30,', rank ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(31,' [known]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(32,' [learn]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(33,' [passive]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(34,' [talent]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(35,' [active]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(36,' [complete]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(37,' (offline)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(38,'on',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(39,'off',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(40,'You are: %s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(41,'visible',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(42,'invisible',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(43,'done',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(44,'You',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(45,' <unknown> ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(46,'<error>',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(47,'<non-existing character>',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(48,'UNKNOWN',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(49,'You must be at least level %u to enter.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(50,'You must be at least level %u and have item %s to enter.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(51,'Hello! Ready for some training?',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(52,'Invaid item count (%u) for item %u',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(53,'Mail can\'t have more %u item stacks',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(54,'The new passwords do not match',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(55,'Your password can\'t be longer than 16 characters (client limit), password not changed!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(56,'Current Message of the day: \r\n%s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57,'Using World DB: %s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(58,'Using script library: %s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(59,'Using creature EventAI: %s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(60,'Online players: %u (max: %u)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(61,'Up to %u expansion allowed now.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(100,'Global notify: ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(101,'Map: %u (%s) Zone: %u (%s) Area: %u (%s) Phase: %u\nX: %f Y: %f Z: %f Orientation: %f\ngrid[%u,%u]cell[%u,%u] InstanceID: %u\n ZoneX: %f ZoneY: %f\nGroundZ: %f FloorZ: %f Have height data (Map: %u VMap: %u)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(102,'%s is already being teleported.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(103,'You can summon a player to your instance only if he is in your party with you as leader.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(104,'You cannot go to the player\'s instance because you are in a party now.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(105,'You can go to the player\'s instance while not being in his party only if your GM mode is on.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(106,'You can not go to player %s from instance to instance.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(107,'You can not summon player %s from instance to instance.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(108,'You are summoning %s%s.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(109,'You are being summoned by %s.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(110,'You are teleporting %s%s to %s.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(111,'You are being teleported by %s.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(112,'Player (%s) does not exist.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(113,'Appearing at %s\'s location.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(114,'%s is appearing to your location.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(115,'Incorrect values.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(116,'No character selected.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(117,'%s is not in a group.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(118,'You changed HP of %s to %i/%i.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(119,'%s changed your HP to %i/%i.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(120,'You changed MANA of %s to %i/%i.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(121,'%s changed your MANA to %i/%i.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(122,'You changed ENERGY of %s to %i/%i.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(123,'%s changed your ENERGY to %i/%i.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(124,'Current energy: %u',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(125,'You changed rage of %s to %i/%i.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(126,'%s changed your rage to %i/%i.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(127,'You changed level of %s to %i.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(128,'GUID %i, faction is %i, flags is %i, npcflag is %i, DY flag is %i',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(129,'Wrong faction: %u (not found in factiontemplate.dbc).',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(130,'You changed GUID=%i \'s Faction to %i, flags to %i, npcflag to %i, dyflag to %i.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(131,'You changed the spellflatid=%i, val= %i, mark =%i to %s.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(132,'%s changed your spellflatid=%i, val= %i, mark =%i.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(133,'%s has access to all taxi nodes now (until logout).',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(134,'%s has no more access to all taxi nodes now (only visited accessible).',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(135,'%s has given you access to all taxi nodes (until logout).',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(136,'%s has removed access to all taxi nodes (only visited still accessible).',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(137,'You set all speeds to %2.2f from normal of %s.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(138,'%s set all your speeds to %2.2f from normal.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(139,'You set the speed to %2.2f from normal of %s.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(140,'%s set your speed to %2.2f from normal.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(141,'You set the swim speed to %2.2f from normal of %s.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(142,'%s set your swim speed to %2.2f from normal.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(143,'You set the backwards run speed to %2.2f from normal of %s.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(144,'%s set your backwards run speed to %2.2f from normal.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(145,'You set the fly speed to %2.2f from normal of %s.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(146,'%s set your fly speed to %2.2f from normal.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(147,'You set the size %2.2f of %s.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(148,'%s set your size to %2.2f.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(149,'There is no such mount.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(150,'You give a mount to %s.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(151,'%s gave you a mount.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(152,'USER1: %i, ADD: %i, DIF: %i\n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(153,'You take all copper of %s.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(154,'%s took you all of your copper.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(155,'You take %i copper from %s.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(156,'%s took %i copper from you.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(157,'You give %i copper to %s.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(158,'%s gave you %i copper.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(159,'You hear sound %u.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(160,'USER2: %i, ADD: %i, RESULT: %i\n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(161,'Removed bit %i in field %i.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(162,'Set bit %i in field %i.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(163,'Teleport location table is empty!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(164,'Teleport location not found!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(165,'Requires search parameter.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(166,'There are no teleport locations matching your request.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(168,'Locations found are:\n%s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(169,'Mail sent to %s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(170,'You try to hear sound %u but it doesn\'t exist.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(171,'You can\'t teleport self to self!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(172,'server console command',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(173,'You changed runic power of %s to %i/%i.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(174,'%s changed your runic power to %i/%i.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(175,'Liquid level: %f, ground: %f, type: %d, status: %d',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(200,'No selection.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(201,'Object GUID is: lowpart %u highpart %X',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(202,'The name was too long by %i characters.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(203,'Error, name can only contain characters A-Z and a-z.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(204,'The subname was too long by %i characters.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(205,'Not yet implemented',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(206,'Item \'%i\' \'%s\' added to list with maxcount \'%i\' and incrtime \'%i\' and extendedcost \'%i\'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(207,'Item \'%i\' not found in database.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(208,'Item \'%i\' \'%s\' deleted from vendor list',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(209,'Item \'%i\' not found in vendor list.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(211,'Spells of %s reset.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(212,'Spells of %s will reset at next login.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(213,'Talents of %s reset.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(214,'Talents of %s will reset at next login.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(215,'Your spells have been reset.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(216,'Your talents have been reset.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(217,'Unknown case \'%s\' for .resetall command. Type full correct case name.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(218,'Spells will reset for all players at login. Strongly recommend re-login!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(219,'Talents will reset for all players at login. Strongly recommend re-login!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(220,'Creature (GUID: %u) No waypoint found.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(221,'Creature (GUID: %u) Last waypoint not found.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(222,'Creature (GUID: %u) No waypoint found - used \'wpguid\'. Now trying to find it by its position...',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(223,'Creature (GUID: %u) No waypoints found - This is a MaNGOS db problem (single float).',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(224,'Selected creature is ignored - provided GUID is used',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(225,'Creature (GUID: %u) not found',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(226,'You must select a visual waypoint.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(227,'No visual waypoints found',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(228,'Could not create visual waypoint with creatureID: %d',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(229,'All visual waypoints removed',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(230,'Could not create waypoint-creature with ID: %d',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(231,'No GUID provided.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(232,'No waypoint number provided.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(233,'Argument required for \'%s\'.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(234,'Waypoint %i added to GUID: %d',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(235,'Waypoint %d added.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(236,'Waypoint changed.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(237,'Waypoint %s modified.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(238,'WP export successfull.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(239,'No waypoints found inside the database.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(240,'File imported.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(241,'Waypoint removed.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(242,'Warning: Could not delete WP from the world with ID: %d',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(243,'This happens if the waypoint is too far away from your char.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(244,'The WP is deleted from the database, but not from the world here.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(245,'They will disappear after a server restart.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(246,'Waypoint %d: Info for creature: %s, GUID: %d',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(247,'Waittime: %d',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(248,'Model %d: %d',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(249,'Emote: %d',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(250,'Spell: %d',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(251,'Text%d (ID: %i): %s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(252,'AIScript: %s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(253,'Forced rename for player %s will be requested at next login.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(254,'Forced rename for player %s (GUID #%u) will be requested at next login.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(255,'Waypoint-Creature (GUID: %u) Not found',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(256,'Could not find NPC...',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(257,'Creature movement type set to \'%s\', waypoints removed (if any).',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(258,'Creature movement type set to \'%s\', waypoints were not removed.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(259,'Incorrect value, use on or off',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(260,'Value saved.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(261,'Value saved, you may need to rejoin or clean your client cache.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(262,'Areatrigger ID %u not found!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(263,'Target map or coordinates is invalid (X: %f Y: %f MapId: %u)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(264,'Zone coordinates is invalid (X: %f Y: %f AreaId: %u)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(265,'Zone %u (%s) is part of instanceable map %u (%s)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(266,'Nothing found!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(267,'Object not found!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(268,'Creature not found!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(269,'Warning: Mob found more than once - you will be teleported to the first one found in DB.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(270,'Creature Removed',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(271,'Creature moved.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(272,'Creature (GUID:%u) must be on the same map as player!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(273,'Game Object (GUID: %u) not found',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(274,'Game Object (GUID: %u) has references in not found creature %u GO list, can\'t be deleted.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(275,'Game Object (GUID: %u) removed',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(276,'Game Object |cffffffff|Hgameobject:%d|h[%s]|h|r (GUID: %u) turned',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(277,'Game Object |cffffffff|Hgameobject:%d|h[%s]|h|r (GUID: %u) moved',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(278,'You must select a vendor',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(279,'You must send id for item',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(280,'Vendor has too many items (max 128)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(281,'You can\'t kick self, logout instead',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(282,'Player %s kicked.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(284,'Accepting Whisper: %s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(285,'Accepting Whisper: ON',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(286,'Accepting Whisper: OFF',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(287,'Creature (GUID: %u) not found',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(288,'Tickets count: %i show new tickets: %s\n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(289,'New ticket from %s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(290,'Ticket of %s (Last updated: %s):\n%s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(291,'New ticket show: ON',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(292,'New ticket show: OFF',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(293,'Ticket %i doesn\'t exist',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(294,'All tickets deleted.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(295,'Character %s ticket deleted.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(296,'Ticket deleted.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(297,'Spawn distance changed to: %f',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(298,'Spawn time changed to: %i',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(299,'The honor of %s was set to %u!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(300,'Your chat has been disabled for %u minutes. Reason: %s.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(301,'You have disabled %s\'s chat for %u minutes. Reason: %s.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(302,'Player\'s chat is already enabled.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(303,'Your chat has been enabled.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(304,'You have enabled %s\'s chat.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(305,'Faction %s (%u) reputation of %s was set to %5d!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(306,'The arena points of %s was set to %u!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(307,'No faction found!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(308,'Faction %i unknown!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(309,'Invalid parameter %s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(310,'delta must be between 0 and %d (inclusive)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(311,'%d - |cffffffff|Hfaction:%d|h[%s]|h|r',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(312,' [visible]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(313,' [at war]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(314,' [peace forced]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(315,' [hidden]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(316,' [invisible forced]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(317,' [inactive]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(318,'Hated',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(319,'Hostile',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(320,'Unfriendly',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(321,'Neutral',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(322,'Friendly',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(323,'Honored',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(324,'Revered',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(325,'Exalted',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(326,'Faction %s (%u) can\'t have reputation.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(327,' [no reputation]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(328,'Characters at account %s (Id: %u)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(329,'  %s (GUID %u)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(330,'No players found!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(331,'Extended item cost %u not exist',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(332,'GM mode is ON',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(333,'GM mode is OFF',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(334,'GM Chat Badge is ON',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(335,'GM Chat Badge is OFF',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(336,'You repair all %s\'s items.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(337,'All your items repaired by %s.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(338,'You set waterwalk mode %s for %s.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(339,'Your waterwalk mode %s by %s.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(340,'%s is now following you.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(341,'%s is not following you.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(342,'%s is now not following you.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(343,'Creature (Entry: %u) cannot be tamed.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(344,'You already have pet.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(345,'Forced customize for player %s will be requested at next login.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(346,'Forced customize for player %s (GUID #%u) will be requested at next login.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(347,'TaxiNode ID %u not found!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(348,'Game Object (Entry: %u) have invalid data and can\'t be spawned',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(400,'|cffff0000[System Message]:|rScripts reloaded',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(401,'You change security level of account %s to %i.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(402,'%s changed your security level to %i.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(403,'You have low security level for this.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(404,'Creature movement disabled.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(405,'Creature movement enabled.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(406,'Weather can\'t be changed for this zone.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(407,'Weather system disabled at server.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(408,'%s is banned for %s. Reason: %s.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(409,'%s is banned permanently for %s.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(410,'%s %s not found',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(411,'%s unbanned.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(412,'There was an error removing the ban on %s.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(413,'Account not exist: %s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(414,'There is no such character.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(415,'There is no such IP in banlist.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(416,'Account %s has never been banned',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(417,'Ban history for account %s:',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(418,'Ban Date: %s Bantime: %s Still active: %s  Reason: %s Set by: %s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(419,'Inf.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(420,'Never',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(421,'Yes',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(422,'No',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(423,'IP: %s\nBan Date: %s\nUnban Date: %s\nRemaining: %s\nReason: %s\nSet by: %s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(424,'There is no matching IPban.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(425,'There is no matching account.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(426,'There is no banned account owning a character matching this part.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(427,'The following IPs match your pattern:',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(428,'The following accounts match your query:',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(429,'You learned many spells/skills.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(430,'You learned all spells for class.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(431,'You learned all talents for class.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(432,'You learned all languages.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(433,'You learned all craft skills and recipes.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(434,'Could not find \'%s\'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(435,'Invalid item id: %u',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(436,'No items found!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(437,'Invalid gameobject id: %u',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(438,'Found items %u: %u ( inventory %u mail %u auction %u guild %u)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(439,'Found gameobjects %u: %u ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(440,'Invalid creature id: %u',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(441,'Found creatures %u: %u ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(442,'No area found!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(443,'No item sets found!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(444,'No skills found!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(445,'No spells found!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(446,'No quests found!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(447,'No creatures found!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(448,'No gameobjects found!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(449,'Graveyard #%u doesn\'t exist.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(450,'Graveyard #%u already linked to zone #%u (current).',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(451,'Graveyard #%u linked to zone #%u (current).',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(452,'Graveyard #%u can\'t be linked to subzone or not existed zone #%u (internal error).',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(454,'No faction in Graveyard with id= #%u , fix your DB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(455,'invalid team, please fix database',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(456,'any',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(457,'alliance',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(458,'horde',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(459,'Graveyard #%u (faction: %s) is nearest from linked to zone #%u.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(460,'Zone #%u doesn\'t have linked graveyards.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(461,'Zone #%u doesn\'t have linked graveyards for faction: %s.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(462,'Teleport location already exists!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(463,'Teleport location added.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(464,'Teleport location NOT added: database error.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(465,'Teleport location deleted.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(466,'No taxinodes found!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(467,'Target unit has %d auras:',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(468,'id: %d effmask: %d charges: %d stack: %d slot %d duration: %d maxduration: %d',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(469,'Target unit has %d auras of type %d:',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(470,'id: %d eff: %d amount: %d',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(471,'Quest %u not found.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(472,'Quest %u started from item. For correct work, please, add item to inventory and start quest in normal way: .additem %u',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(473,'Quest removed.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(474,' [rewarded]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(475,' [complete]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(476,' [active]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(477,'%s\'s Fly Mode %s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(478,'Opcode %u sent to %s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(479,'Character loaded successfully!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(480,'Failed to load the character!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(481,'Character dumped successfully!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(482,'Character dump failed!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(483,'Spell %u broken and not allowed to cast or learn!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(484,'Skill %u (%s) for player %s set to %u and current maximum set to %u (without permanent (talent) bonuses).',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(485,'Player %s must have skill %u (%s) before using this command.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(486,'Invalid skill id (%u)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(487,'You learned default GM spells/skills.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(488,'You already know that spell.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(489,'Target(%s) already know that spell.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(490,'%s doesn\'t know that spell.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(491,'You already forgot that spell.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(492,'All spell cooldowns removed for %s.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(493,'Spell %u cooldown removed for %s.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(494,'Command : Additem, itemId = %i, amount = %i',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(495,'Command : Additemset, itemsetId = %i',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(496,'Removed itemID = %i, amount = %i from %s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(497,'Cannot create item \'%i\' (amount: %i)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(498,'You need to provide a guild name!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(499,'Player not found!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(500,'Player already has a guild!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(501,'Guild not created! (already exists?)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(502,'No items from itemset \'%u\' found.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(503,'The distance is: (3D) %f (2D) %f (Exact 3D) %f (Exact 2D) %f yards.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(504,'Item \'%i\' \'%s\' Item Slot %i',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(505,'Item \'%i\' doesn\'t exist.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(506,'Item \'%i\' \'%s\' Added to Slot %i',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(507,'Item save failed!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(508,'%d - owner: %s (guid: %u account: %u ) %s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(509,'%d - sender: %s (guid: %u account: %u ) receiver: %s (guid: %u account: %u ) %s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(510,'%d - owner: %s (guid: %u account: %u ) %s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(511,'Wrong link type!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(512,'%d - |cffffffff|Hitem:%d:0:0:0:0:0:0:0:0|h[%s]|h|r ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(513,'%d - |cffffffff|Hquest:%d:%d|h[%s]|h|r %s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(514,'%d - |cffffffff|Hcreature_entry:%d|h[%s]|h|r ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(515,'%d - |cffffffff|Hcreature:%d|h[%s X:%f Y:%f Z:%f MapId:%d]|h|r ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(516,'%d - |cffffffff|Hgameobject_entry:%d|h[%s]|h|r ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(517,'%d - |cffffffff|Hgameobject:%d|h[%s X:%f Y:%f Z:%f MapId:%d]|h|r ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(518,'%d - |cffffffff|Hitemset:%d|h[%s %s]|h|r ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(519,'|cffffffff|Htele:%s|h[%s]|h|r ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(520,'%d - |cffffffff|Hspell:%d|h[%s]|h|r ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(521,'%d - |cffffffff|Hskill:%d|h[%s %s]|h|r %s %s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(522,'Game Object (Entry: %u) not found',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(523,'>> Game Object %s (GUID: %u) at %f %f %f. Orientation %f.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(524,'Selected object:\n|cffffffff|Hgameobject:%d|h[%s]|h|r GUID: %u ID: %u\nX: %f Y: %f Z: %f MapId: %u\nOrientation: %f\nPhasemask %u',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(525,'>> Add Game Object \'%i\' (%s) (GUID: %i) added at \'%f %f %f\'.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(526,'%s (lowguid: %u) movement generators stack:',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(527,'   Idle',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(528,'   Random',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(529,'   Waypoint',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(530,'   Animal random',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(531,'   Confused',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(532,'   Targeted to player %s (lowguid %u)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(533,'   Targeted to creature %s (lowguid %u)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(534,'   Targeted to <NULL>',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(535,'   Home movement to (X:%f Y:%f Z:%f)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(536,'   Home movement used for player?!?',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(537,'   Taxi flight',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(538,'   Unknown movement generator (%u)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(539,'Player selected NPC\nGUID: %u.\nFaction: %u.\nnpcFlags: %u.\nEntry: %u.\nDisplayID: %u (Native: %u).',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(540,'Level: %u.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(541,'Health (base): %u. (max): %u. (current): %u.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(542,'Field Flags: %u.\nDynamic Flags: %u.\nFaction Template: %u.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(543,'Loot: %u Pickpocket: %u Skinning: %u',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(544,'Position: %f %f %f.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(545,'*** Is a vendor!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(546,'*** Is a trainer!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(547,'InstanceID: %u',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(548,'Player%s %s (guid: %u) Account: %s (id: %u) Email: %s GMLevel: %u Last IP: %s Last login: %s Latency: %ums',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(549,'Race: %s Class: %s Played time: %s Level: %u Money: %ug%us%uc',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(550,'Command .pinfo doesn\'t support \'rep\' option for offline players.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(551,'%s has explored all zones now.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(552,'%s has no more explored zones.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(553,'%s has explored all zones for you.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(554,'%s has hidden all zones from you.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(555,'Hover enabled',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(556,'Hover disabled',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(557,'%s level up you to (%i)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(558,'%s level down you to (%i)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(559,'%s reset your level progress.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(560,'The area has been set as explored.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(561,'The area has been set as not explored.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(562,'GUID=%i \'s updateIndex: %i, value:  %i.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(563,'You change GUID=%i \'s UpdateIndex: %i value to %i.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(564,'The value index %u is too big to %u(count: %u).',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(565,'Set %u uint32 Value:[OPCODE]:%u [VALUE]:%u',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(566,'You Set %u Field:%u to uint32 Value: %u',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(567,'Set %u float Value:[OPCODE]:%u [VALUE]:%f',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(568,'You Set %u Field:%i to float Value: %f',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(569,'Get %u uint32 Value:[OPCODE]:%u [VALUE]:%u',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(570,'The uint32 value of %u in %u is: %u',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(571,'Get %u float Value:[OPCODE]:%u [VALUE]:%f',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(572,'The float of %u value in %u is: %f',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(573,'.Set32Bit:[OPCODE]:%u [VALUE]:%u',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(574,'You set Bit of Field:%u to Value: %u',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(575,'.Mod32Value:[OPCODE]:%u [VALUE]:%i',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(576,'You modified the value of Field:%u to Value: %u',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(577,'You are now invisible.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(578,'You are now visible.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(579,'Selected player or creature not have victim.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(580,'Player %s learned all default spells for race/class and completed quests rewarded spells.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(581,'Found near gameobjects (distance %f): %u ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(582,'SpawnTime: Full:%s Remain:%s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(583,'%d - |cffffffff|Hgameevent:%d|h[%s]|h|r%s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(584,'No event found!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(585,'Event not exist!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(586,'Event %u: %s%s\nStart: %s End: %s Occurence: %s Length: %s\nNext state change: %s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(587,'Event %u already active!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(588,'Event %u not active!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(589,'   Point movement to (X:%f Y:%f Z:%f)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(590,'   Fear movement',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(591,'   Distract movement',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(592,'You have learned all spells in craft: %s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(593,'Currently Banned Accounts:',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(594,'|    Account    |   BanDate    |   UnbanDate  |  Banned By    |   Ban Reason  |',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(595,'Currently Banned IPs:',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(596,'|      IP       |   BanDate    |   UnbanDate  |  Banned By    |   Ban Reason  |',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(597,'Current gamemasters:',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(598,'|    Account    |  GM  |',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(599,'No gamemasters.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(600,'The Alliance wins!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(601,'The Horde wins!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(602,'The battle for Warsong Gulch begins in 1 minute.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(603,'The battle for Warsong Gulch begins in 30 seconds. Prepare yourselves!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(604,'Let the battle for Warsong Gulch begin!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(605,'$n captured the Horde flag!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(606,'$n captured the Alliance flag!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(607,'The Horde flag was dropped by $n!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(608,'The Alliance Flag was dropped by $n!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(609,'The Alliance Flag was returned to its base by $n!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(610,'The Horde flag was returned to its base by $n!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(611,'The Horde flag was picked up by $n!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(612,'The Alliance Flag was picked up by $n!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(613,'The flags are now placed at their bases.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(614,'The Alliance flag is now placed at its base.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(615,'The Horde flag is now placed at its base.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(636,'The Battle for Eye of the Storm begins in 1 minute.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(637,'The Battle for Eye of the Storm begins in 30 seconds.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(638,'The Battle for Eye of the Storm has begun!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(650,'Alliance',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(651,'Horde',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(652,'stables',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(653,'blacksmith',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(654,'farm',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(655,'lumber mill',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(656,'mine',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(657,'The %s has taken the %s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(658,'$n has defended the %s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(659,'$n has assaulted the %s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(660,'$n claims the %s! If left unchallenged, the %s will control it in 1 minute!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(661,'The Battle for Arathi Basin begins in 1 minute.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(662,'The Battle for Arathi Basin begins in 30 seconds. Prepare yourselves!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(663,'The Battle for Arathi Basin has begun!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(664,'The Alliance has gathered $1776W resources, and is near victory!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(665,'The Horde has gathered $1777W resources, and is near victory!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(666,'After your recent battle in %s our best attempts to award you a Mark of Honor failed. Enclosed you will find the Mark of Honor we were not able to deliver to you at the time. Thanks for fighting in %s!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(667,'The Alliance has taken control of the Mage Tower!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(668,'The Horde has taken control of the Mage Tower!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(669,'The Alliance has taken control of the Draenei Ruins!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(670,'The Horde has taken control of the Draenei Ruins!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(671,'The Alliance has taken control of the Blood Elf Tower!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(672,'The Horde has taken control of the Blood Elf Tower!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(673,'The Alliance has taken control of the Fel Reaver Ruins!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(674,'The Horde has taken control of the Fel Reaver Ruins!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(675,'The Alliance has lost control of the Mage Tower!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(676,'The Horde has lost control of the Mage Tower!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(677,'The Alliance has lost control of the Draenei Ruins!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(678,'The Horde has lost control of the Draenei Ruins!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(679,'The Alliance has lost control of the Blood Elf Tower!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(680,'The Horde has lost control of the Blood Elf Tower!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(681,'The Alliance has lost control of the Fel Reaver Ruins!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(682,'The Horde has lost control of the Fel Reaver Ruins!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(683,'%s has taken the flag!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(684,'The Alliance have captured the flag!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(685,'The Horde have captured the flag!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(686,'The flag has been dropped.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(687,'The flag has been reset.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(700,'You must be level %u to form an arena team',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(701,'One minute until the Arena battle begins!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(702,'Thirty seconds until the Arena battle begins!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(703,'Fifteen seconds until the Arena battle begins!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(704,'The Arena battle has begun!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(705,'You must wait %s before speaking again.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(706,'This item(s) have problems with equipping/storing in inventory.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(707,'%s wishes to not be disturbed and cannot receive whisper messages: %s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(708,'%s is Away from Keyboard: %s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(709,'Do not Disturb',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(710,'Away from Keyboard',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(711,'Queue status for %s (Lvl: %u to %u)\nQueued alliances: %u (Need at least %u more)\nQueued hordes: %u (Need at least %u more)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(712,'|cffff0000[BG Queue Announcer]:|r %s -- [%u-%u] A: %u/%u, H: %u/%u|r',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(713,'You must be level %u to join an arena team!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(715,'You don\'t meet Battleground level requirements',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(717,'|cffff0000[BG Queue Announcer]:|r %s -- [%u-%u] Started!|r',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(718,'|cffff0000[Arena Queue Announcer]:|r %s -- Joined : %ux%u : %u|r',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(719,'|cffff0000[Arena Queue Announcer]:|r %s -- Exited : %ux%u : %u|r',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(720,'Your group is too large for this battleground. Please regroup to join.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(721,'Your group is too large for this arena. Please regroup to join.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(722,'Your group has members not in your arena team. Please regroup to join.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(723,'Your group does not have enough players to join this match.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(724,'The Gold Team wins!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(725,'The Green Team wins!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(726,'There aren\'t enough players in this battleground. It will end soon unless some more players join to balance the fight.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(727,'Your group has an offline member. Please remove him before joining.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(728,'Your group has players from the opposing faction. You can\'t join the battleground as a group.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(729,'Your group has players from different battleground brakets. You can\'t join as group.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(730,'Someone in your party is already in this battleground queue. (S)he must leave it before joining as group.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(731,'Someone in your party is Deserter. You can\'t join as group.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(732,'Someone in your party is already in three battleground queues. You cannot join as group.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(733,'You cannot teleport to a battleground or arena map.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(734,'You cannot summon players to a battleground or arena map.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(735,'You must be in GM mode to teleport to a player in a battleground.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(736,'You cannot teleport to a battleground from another battleground. Please leave the current battleground first.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(737,'Arenas are set to 1v1 for debugging. So, don\'t join as group.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(738,'Arenas are set to normal playercount.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(739,'Battlegrounds are set to 1v0 for debugging.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(740,'Battlegrounds are set to normal playercount.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(741,'Flushing Arena points based on team ratings, this may take a few minutes. Please stand by...',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Îáíîâëåíèå ñòàòèñòèêè Àðåíû ïî ðåçóëüòàòàì ïðîøåäøåé íåäåëè. Ïðîöåäóðà çàéìåò íåêîòîðîå âðåìÿ. Óëûáàåìñÿ è ìàøåì!'),(742,'Distributing arena points to players...',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Îáíîâëåíèå íåäåëüíûõ êàïîâ Î÷êîâ çàâîåâàíèÿ äëÿ âñåõ èãðîêîâ...'),(743,'Finished setting arena points for online players.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Íåäåëüíûå êàïû óñïåøíî îáíîâëåíû. Ðåçóëüòàòû îòïðàâëåíû âñåì îíëàéí èãðîêàì.'),(744,'Modifying played count, arena points etc. for loaded arena teams, sending updated stats to online players...',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Îáíîâëåíèå ñòàòèñòèêè Àðåíû äëÿ âñåõ êîìàíä àðåíû.'),(745,'Modification done.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Ñòàòèñòèêà Àðåíû óñïåøíî îáíîâëåíà. Èçìåíåíèÿ îòïðàâëåíû âñåì èãðîêàì îíëàéí.'),(746,'Done flushing Arena points.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Ïðîöåäóðà îáíîâëåíèÿ íåäåëüíîé ñòàòèñòèêè çàâåðøåíà. Æåëàåì âàì íîâûõ ïîáåä è ïðèÿòíîé èãðû!'),(750,'Not enough players. This game will close in %u mins.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(751,'Not enough players. This game will close in %u seconds.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(753,'The battle for Warsong Gulch begins in 2 minutes.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(754,'The battle for Arathi Basin begins in 2 minutes.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(755,'The battle for Eye of the Storm begins in 2 minutes.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(356,'Current selected title for player %s reset as not known now.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(355,'Title %u (%s) set as current seelcted title for player %s.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(354,'Title %u (%s) removed from known titles list for player %s.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(350,'%d (idx:%d) - [%s %s] %s %s ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(351,'No titles found!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(352,'Invalid title id: %u',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(353,'Title %u (%s) added to known titles list for player %s.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(349,'%d (idx:%d) - |cffffffff|Htitle:%d|h[%s %s]|h|r %s %s ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(785,'Arena testing turned %s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(786,'|cffff0000[Automatic]:|r',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(787,'|cffffff00[|cff00ff00Global Message by|r |cff00ccff%s|cffffff00]:|r %s|r',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(800,'Invalid name',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(801,'You do not have enough gold',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(802,'You do not have enough free slots',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(803,'Your partner does not have enough free bag slots',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(804,'You do not have permission to perform that function',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(805,'Unknown language',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(806,'You don\'t know that language',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(807,'Please provide character name',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(808,'Player %s not found or offline',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(809,'Account for character %s not found',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(810,'|Hplayer:$N|h[$N]|h has earned the achievement $a!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(811,'Guild Master',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(812,'Officer',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(813,'Veteran',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(814,'Member',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(815,'Initiate',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(816,'Warning: You\'ve entered a no-fly zone and are about to be dismounted!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1000,'Exiting daemon...',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1001,'Account deleted: %s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1002,'Account %s NOT deleted (probably sql file format was updated)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1003,'Account %s NOT deleted (unknown error)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1004,'Account created: %s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1005,'Account name can\'t be longer than 16 characters (client limit), account not created!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1006,'Account with this name already exist!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1007,'Account %s NOT created (probably sql file format was updated)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1008,'Account %s NOT created (unknown error)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1009,'Player %s (Guid: %u) Account %s (Id: %u) deleted.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1012,'-==================================================================-',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1011,'|<Error>        | %20s |<Error>          |<Er>| <Error>   |',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1010,'-[         Account][   Character][             IP][Map][Zone][Exp][GMLev]-',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1014,'No online players.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1100,'Account %s (Id: %u) have up to %u expansion allowed now.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1101,'Message of the day changed to:\r\n%s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1102,'Message sent to %s: %s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1103,'%d - %s %s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1104,'%d - %s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1105,'%d - %s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1106,'%d - %s %s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1107,'%d - %s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1108,'%d - %s %s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1109,'%d - %s %s %s %s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1110,'%d - %s X:%f Y:%f Z:%f MapId:%d',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1111,'%d - %s X:%f Y:%f Z:%f MapId:%d',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1112,'Failed to open file: %s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1113,'Account %s (%u) have max amount allowed characters (client limit)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1114,'Dump file have broken data!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1115,'Invalid character name!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1116,'Invalid character guid!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1117,'Character guid %u in use!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1118,'%d - guild: %s (guid: %u) %s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1119,'You must use male or female as gender.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1120,'You change gender of %s to %s.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1121,'Your gender changed to %s by %s.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1122,'(%u/%u +perm %u +temp %u)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1123,'Not pet found',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1124,'Wrong pet type',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1125,'Your pet learned all talents',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1126,'Your pet talents have been reset.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1127,'Talents of %s\'s pet reset.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1128,'%d - |cffffffff|Htaxinode:%u|h[%s %s]|h|r (Map:%u X:%f Y:%f Z:%f)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1129,'%d - %s %s (Map:%u X:%f Y:%f Z:%f)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1200,'You try to view cinemitic %u but it doesn\'t exist.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1201,'You try to view movie %u but it doesn\'t exist.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1300,'Alliance',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1301,'Horde',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1302,'%s was destroyed by the %s!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1303,'The %s is under attack! If left unchecked, the %s will destroy it!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1304,'The %s was taken by the %s!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1305,'The %s was taken by the %s!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1306,'The %s was taken by the %s!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1307,'The %s is under attack! If left unchecked, the %s will capture it!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1308,'The %s has taken the %s! Its supplies will now be used for reinforcements!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1309,'Irondeep Mine',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1310,'Coldtooth Mine',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1311,'Stormpike Aid Station',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1312,'Dun Baldar South Bunker',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1313,'Dun Baldar North Bunker',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1314,'Stormpike Graveyard',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1315,'Icewing Bunker',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1316,'Stonehearth Graveyard',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1317,'Stonehearth Bunker',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1318,'Snowfall Graveyard',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1319,'Iceblood Tower',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1320,'Iceblood Graveyard',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1321,'Tower Point',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1322,'Frostwolf Graveyard',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1323,'East Frostwolf Tower',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1324,'West Frostwolf Tower',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1325,'Frostwolf Relief Hut',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1326,'The Battle for Alterac Valley begins in 1 minute.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1327,'The Battle for Alterac Valley begins in 30 seconds. Prepare yourselves!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1328,'The Battle for Alterac Valley has begun!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1329,'The Alliance Team is running out of reinforcements!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1330,'The Horde Team is running out of reinforcements!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1331,'The Frostwolf General is Dead!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1332,'The Stormpike General is Dead!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1333,'The Battle for Alterac Valley begins in 2 minutes.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2000,'|cff00ff00New ticket from|r|cffff00ff %s.|r |cff00ff00Ticket entry:|r|cffff00ff %d.|r',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2001,'|cff00ff00Character|r|cffff00ff %s |r|cff00ff00edited his/her ticket:|r|cffff00ff %d.|r',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2002,'|cff00ff00Character|r|cffff00ff %s |r|cff00ff00abandoned ticket entry:|r|cffff00ff %d.|r',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2003,'|cff00ff00Closed by|r:|cff00ccff %s|r ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2004,'|cff00ff00Deleted by|r:|cff00ccff %s|r ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2005,'Ticket not found.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2006,'Please close ticket before deleting it permanently.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2007,'Ticket %d is already assigned.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2008,'%u Tickets succesfully reloaded from the database.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2009,'Showing list of open tickets.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2010,'Showing list of open tickets whose creator is online.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2011,'Showing list of closed tickets.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2012,'Invalid name specified. Name should be that of an online Gamemaster.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2013,'This ticket is already assigned to yourself. To unassign use .ticket unassign %d and then reassign.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2014,'Ticket %d is not assigned, you cannot unassign it.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2015,'You cannot unassign tickets from staffmembers with a higher security level than yourself.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2016,'Cannot close ticket %d, it is assigned to another GM.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2023,'|cff00ff00GM Comment|r: [%s]|r',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2018,'|cff00ff00Created by|r:|cff00ccff %s|r ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2019,'|cff00ff00Last change|r:|cff00ccff %s ago|r ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2020,'|cff00ff00Assigned to|r:|cff00ccff %s|r ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2021,'|cff00ff00Unassigned by|r:|cff00ccff %s|r ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2022,'|cff00ff00Ticket Message|r: [%s]|r',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2017,'|cffaaffaaTicket|r:|cffaaccff %d.|r ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2025,'|cff00ff00Created|r:|cff00ccff %s ago|r ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5000,'You froze player %s.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5001,'It might be amusing but no... you cant freeze yourself!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5002,'Invalid input check the name of target.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5003,'You unfroze player %s.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5004,'There are no frozen players.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5005,'Following players are frozen on the server:',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5006,'- %s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5007,'You must be in a raid group to enter this instance.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5009,'Sound %u Played to server',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5010,'linkGUID: %u, Entry: %u (%s)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5011,'You can\'t teleport self to self!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5012,'No maps found!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5013,'[Continent]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5014,'[Instance]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5015,'[Battleground]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5016,'[Arena]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5017,'[Raid reset time: %s]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5018,'[Heroic reset time: %s]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5019,'[Mountable]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6613,'|cfff00000[GM Announcement]: %s|r',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6614,'Notification to GM\'s - ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6615,'|cffffff00[|c1f40af20Global Message by|r |cffff0000%s|cffffff00]:|r %s|r',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6616,'Silence is ON for %s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(7523,'WORLD: Denying connections.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(7524,'WORLD: Accepting connections.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10001,'The Horde has taken The Overlook!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10002,'The Alliance has taken The Overlook!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10003,'The Horde has taken The Stadium!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10004,'The Alliance has taken The Stadium!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10005,'The Horde has taken Broken Hill!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10006,'The Alliance has taken Broken Hill!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10007,'The Horde lost The Overlook!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10008,'The Alliance lost The Overlook!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10009,'The Horde lost The Stadium!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10010,'The Alliance lost The Stadium!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10011,'The Horde lost Broken Hill!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10012,'The Alliance lost Broken Hill!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10013,'The Horde has taken the West Beacon!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10014,'The Alliance has taken the West Beacon!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10015,'The Horde has taken the East Beacon!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10016,'The Alliance has taken the East Beacon!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10017,'The Horde has captured the Twin Spire Graveyard!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10018,'The Alliance has captured the Twin Spire Graveyard!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10019,'The Horde lost the West Beacon!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10020,'The Alliance lost the West Beacon!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10021,'The Horde lost the East Beacon!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10022,'The Alliance lost the East Beacon!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10023,'The Horde lost the Twin Spire Graveyard!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10024,'The Alliance lost the Twin Spire Graveyard!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10025,'The Horde has captured Halaa!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10026,'The Alliance has captured Halaa!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10027,'The Horde lost Halaa!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10028,'The Alliance lost Halaa!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10029,'The Horde has taken a Spirit Tower!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10030,'The Alliance has taken a Spirit Tower!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10031,'The Horde lost a Spirit Tower!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10032,'The Alliance lost a Spirit Tower!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10033,'The Horde has taken the Northpass Tower!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10034,'The Alliance has taken the Northpass Tower!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10035,'The Horde has taken the Eastwall Tower!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10036,'The Alliance has taken the Eastwall Tower!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10037,'The Horde has taken the Crown Guard Tower!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10038,'The Alliance has taken the Crown Guard Tower!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10039,'The Horde has taken the Plaguewood Tower!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10040,'The Alliance has taken the Plaguewood Tower!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10041,'The Horde lost the Northpass Tower!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10042,'The Alliance lost the Northpass Tower!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10043,'The Horde lost the Eastwall Tower!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10044,'The Alliance lost the Eastwall Tower!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10045,'The Horde lost the Crown Guard Tower!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10046,'The Alliance lost the Crown Guard Tower!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10047,'The Horde lost the Plaguewood Tower!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10048,'The Alliance lost the Plaguewood Tower!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10049,'The Horde has collected 200 silithyst!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10050,'The Alliance has collected 200 silithyst!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10051,'Take me to Northpass Tower.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10052,'Take me to Eastwall Tower.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10053,'Take me to Crown Guard Tower.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10054,'Give me the flag, I\'ll take it to the central beacon for the glory of the Alliance!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10055,'Give me the flag, I\'ll take it to the central beacon for the glory of the Horde!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(11000,'|cffffff00[|c00077766Autobroadcast|cffffff00]: |cFFF222FF%s|r',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(11001,'You have not chosen -1 or the current realmID that you are on.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5020,'Phasemask: %u',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5021,'Armor: %u',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10063,'The Alliance captured the titan portal!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10064,'The Horde captured the titan portal!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10061,'The %s was destroyed!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10062,'Round 1 -  finished!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10059,'Let the battle for Strand of the Ancients begin!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10060,'The %s is under attack!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10058,'The battle for Strand of the Ancients begins in 30 seconds. Prepare yourselves!.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10056,'The battle for Strand of the Ancients begins in 2 minutes.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10057,'The battle for Strand of the Ancients begins in 1 minute.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5022,'Granting ownership to first person that joins the channel \"%s\": Enabled.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5008,'This instance is closed.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5024,'Entry: %u',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5025,'Type: %u',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5026,'DisplayID: %u',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5027,'Name: %s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6604,'You cannot say, yell or emote until you become level %d.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6605,'You cannot whisper until you become level %d.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6606,'You cannot write to channels until you become level %d.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6607,'You cannot use auction until you become level %d.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6608,'You cannot write tickets until you become level %d.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6609,'You cannot trade until you become level %d.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6610,'You cannot trade with characters lower than level %d.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6611,'You cannot send mail until you become level %d.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6612,'You cannot send mail to characters lower than level %d.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2024,'|cff00ccff%s|r |cff00ff00Added comment|r: [%s]|r',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1013,'-[%16s][%12s][%15s][%3d][%4d][%d][%d]-',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1015,'-======================== Characters Online =======================-',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(747,'This Battleground has been disabled. You can\'t join the queue.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(748,'Arenas have been disabled. You can\'t join the queue.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(210,'Item \'%u\' (with extended cost %u) already in vendor list.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(817,'Entry %u not found in creature_template table.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(818,'Entry %u not found in sCreatureStorage. Possible new line in creature_template, but you can not add new creatures without restarting. Only modifing is allowed.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10065,'Round 2 of the Battle for the Strand of the Ancients begins in 1 minute.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10066,'Round 2 begins in 30 seconds. Prepare yourselves!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10067,'The chamber has been breached! The titan relic is vulnerable!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10068,'The Alliance captured the South Graveyard!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10069,'The Alliance captured the West Graveyard!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10070,'The Alliance captured the East Graveyard!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10071,'The Horde captured the South Graveyard!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10072,'The Horde captured the West Graveyard!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10073,'The Horde captured the East Graveyard!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(62,'One on more parameters have incorrect values',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1016,'| GUID       | Name                 | Account                      | Delete Date         |',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1017,'| %10u | %20s | %15s (%10u) | %19s |',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1018,'==========================================================================================',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1019,'No characters found.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1020,'Restoring the following characters:',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1021,'Deleting the following characters:',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1022,'ERROR: You can only assign a new name if you have only selected a single character!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1023,'Character \'%s\' (GUID: %u Account %u) can\'t be restored: account not exist!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1024,'Character \'%s\' (GUID: %u Account %u) can\'t be restored: account character list full!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1025,'Character \'%s\' (GUID: %u Account %u) can\'t be restored: new name already used!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1026,'GUID: %u Name: %s Account: %s (%u) Date: %s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5028,'Lootid: %u',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1130,'Can\'t dump deleted characters,aborting.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5029,'Result limit reached (max results: %d)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1131,'The following characters match your query:',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1132,'Currently Banned Characters:',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1133,'|   Character   |   BanDate    |   UnbanDate  |  Banned By    |   Ban Reason  |',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(819,'City',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1205,'The battle will begin in two minutes.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1206,'The battle will begin in 1 minute.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1208,'The battle has begun!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1207,'The battle will begin in 30 seconds!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1209,'the alliance keep',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1210,'the horde keep',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1211,'%s wins!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1212,'The west gate of %s is destroyed!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1213,'The east gate of %s is destroyed!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1214,'The south gate of %s is destroyed!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1215,'The north gate of %s is destroyed!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1216,'$n has assaulted the %s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1217,'$n has defended the %s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1218,'$n claims the %s! If left unchallenged, the %s will control it in 1 minute!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1219,'The %s has taken the %s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1220,'Workshop',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1221,'Docks',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1222,'Refinery',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1223,'Quarry',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1224,'Hangar',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1134,'Sending tickets is allowed.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1135,'Sending tickets is not allowed.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5023,'Granting ownership to first person that joins the channel \"%s\": Disabled.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1240,'The Horde flag was picked up by $n!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1239,'The Horde flag was returned to its base by $n!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1238,'The Alliance Flag was returned to its base by $n!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1237,'The Alliance Flag was dropped by $n!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1236,'The Horde flag was dropped by $n!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1235,'$n captured the Alliance flag!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1234,'$n captured the Horde flag!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1233,'Let the battle for Twin Peaks begin!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1232,'The battle for Twin Peaks begins in 30 seconds. Prepare yourselves!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1231,'The battle for Twin Peaks begins in 1 minute.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1230,'The battle for Twin Peaks begins in 2 minutes.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1241,'The Alliance Flag was picked up by $n!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1242,'The flags are now placed at their bases.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1243,'The Alliance flag is now placed at its base.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1244,'The Horde flag is now placed at its base.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1334,'Requires Maiden of Winter\'s Breath Lake',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1250,'The Battle for Gilneas begins in 2 minutes.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1251,'The Battle for Gilneas begins in 1 minute.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1252,'The Battle for Gilneas begins in 30 seconds. Prepare yourselves!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1253,'The Battle for Gilneas has begun!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1254,'Alliance',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1255,'Horde',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1256,'lighthouse',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1257,'waterworks',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1258,'mine',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1259,'The %s has taken the %s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1260,'$n has defended the %s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1261,'$n has assaulted the %s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1262,'$n claims the %s! If left unchallenged, the %s will control it in 1 minute!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1263,'The Alliance has gathered $1776W resources, and is near victory!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1264,'The Horde has gathered $1777W resources, and is near victory!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1335,'You can\'t use that right now',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5030,'%s attempts to run away in fear!',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `skyfire_string` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `smart_scripts`
--

DROP TABLE IF EXISTS `smart_scripts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `smart_scripts` (
  `entryorguid` mediumint(11) NOT NULL,
  `source_type` mediumint(5) unsigned NOT NULL DEFAULT '0',
  `id` mediumint(11) unsigned NOT NULL DEFAULT '0',
  `link` mediumint(11) unsigned NOT NULL DEFAULT '0',
  `event_type` mediumint(5) unsigned NOT NULL DEFAULT '0',
  `event_phase_mask` mediumint(11) unsigned NOT NULL DEFAULT '0',
  `event_chance` mediumint(5) unsigned NOT NULL DEFAULT '100',
  `event_flags` mediumint(11) unsigned NOT NULL DEFAULT '0',
  `event_param1` mediumint(11) unsigned NOT NULL DEFAULT '0',
  `event_param2` mediumint(11) unsigned NOT NULL DEFAULT '0',
  `event_param3` mediumint(11) unsigned NOT NULL DEFAULT '0',
  `event_param4` mediumint(11) unsigned NOT NULL DEFAULT '0',
  `action_type` mediumint(5) unsigned NOT NULL DEFAULT '0',
  `action_param1` mediumint(11) unsigned NOT NULL DEFAULT '0',
  `action_param2` mediumint(11) unsigned NOT NULL DEFAULT '0',
  `action_param3` mediumint(11) unsigned NOT NULL DEFAULT '0',
  `action_param4` mediumint(11) unsigned NOT NULL DEFAULT '0',
  `action_param5` mediumint(11) unsigned NOT NULL DEFAULT '0',
  `action_param6` mediumint(11) unsigned NOT NULL DEFAULT '0',
  `target_type` mediumint(11) unsigned NOT NULL DEFAULT '0',
  `target_param1` mediumint(11) unsigned NOT NULL DEFAULT '0',
  `target_param2` mediumint(11) unsigned NOT NULL DEFAULT '0',
  `target_param3` mediumint(11) unsigned NOT NULL DEFAULT '0',
  `target_x` float NOT NULL DEFAULT '0',
  `target_y` float NOT NULL DEFAULT '0',
  `target_z` float NOT NULL DEFAULT '0',
  `target_o` float NOT NULL DEFAULT '0',
  `comment` varchar(255) NOT NULL DEFAULT '' COMMENT 'Event Comment',
  PRIMARY KEY (`entryorguid`,`source_type`,`id`,`link`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='CTDB Smart Scripts';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `smart_scripts`
--

LOCK TABLES `smart_scripts` WRITE;
/*!40000 ALTER TABLE `smart_scripts` DISABLE KEYS */;
/*!40000 ALTER TABLE `smart_scripts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spell_area`
--

DROP TABLE IF EXISTS `spell_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spell_area` (
  `spell` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `area` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `quest_start` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `quest_start_active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `quest_end` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `aura_spell` mediumint(8) NOT NULL DEFAULT '0',
  `racemask` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `gender` tinyint(1) unsigned NOT NULL DEFAULT '2',
  `autocast` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`spell`,`area`,`quest_start`,`quest_start_active`,`aura_spell`,`racemask`,`gender`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Spell area';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spell_area`
--

LOCK TABLES `spell_area` WRITE;
/*!40000 ALTER TABLE `spell_area` DISABLE KEYS */;
/*!40000 ALTER TABLE `spell_area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spell_bonus_data`
--

DROP TABLE IF EXISTS `spell_bonus_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spell_bonus_data` (
  `entry` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `direct_bonus` float NOT NULL DEFAULT '0',
  `dot_bonus` float NOT NULL DEFAULT '0',
  `ap_bonus` float NOT NULL DEFAULT '0',
  `ap_dot_bonus` float NOT NULL DEFAULT '0',
  `comments` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Spell bonus data';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spell_bonus_data`
--

LOCK TABLES `spell_bonus_data` WRITE;
/*!40000 ALTER TABLE `spell_bonus_data` DISABLE KEYS */;
INSERT INTO `spell_bonus_data` VALUES (55078,0,0,0,0.06325,'Death Knight - Blood Plague'),(47632,0,0,0.3,0,'Death Knight - DeathCoil Damage'),(47633,0,0,0.3,0,'Death Knight - Death Coil Heal'),(58621,0,0,0.08,0,'Death Knight - Glyph of Chains of Ice'),(49184,0,0,0.1,0,'Death Knight - Howling Blast'),(50842,0,0,0.04,0,'Death Knight - Pestilence'),(47476,0,0,0.06,0,'Death Knight - Strangulate'),(55095,0,0,0,0.06325,'Death Knight - Frost Fever'),(60089,0,0,0.15,0,'Druid - Faerie Fire (feral)'),(48628,0,0,0,0.15,'Druid - Lock Jaw'),(1822,0,0,0.01,0.06,'Druid - Rake ($AP*0.18 / number of ticks)'),(779,1,-1,0.063,-1,'Druid - Swipe (Bear)'),(3044,0,0,0.15,0,'Hunter - Arcane Shot'),(3674,0,0,0,0.02,'Hunter - Black Arrow($RAP*0.1 / number of ticks)'),(19306,1,-1,0.2,-1,'Hunter - Counterattack'),(13812,0,0,0.1,0,'Hunter - Explosive Trap Effect'),(13797,0,0,0,0.02,'Hunter - Immolation Trap($RAP*0.1 / number of ticks)'),(1978,0,0,0,0.04,'Hunter - Serpent Sting($RAP*0.2 / number of ticks)'),(56641,0,0,0.1,0,'Hunter - Steady Shot'),(31935,0.21,-1,0.07,-1,'Paladin - Avenger Shield'),(26573,0,0,0,0.04,'Paladin - Consecration'),(879,0,0,0.15,0,'Paladin - Exorcism'),(24275,0.117,-1,0.15,-1,'Paladin - Hammer of Wrath'),(20925,0,0,0.056,0,'Paladin - Holy Shield'),(31803,0.01,-1,-1,0.03,'Paladin - Holy Vengeance'),(2812,0.61,0,0,0,'Holy Wrath'),(53733,0.22,-1,0.14,-1,'Paladin - Judgement of Corruption'),(20187,0.32,-1,0.2,0,'Paladin - Judgement of Righteousness'),(54158,0.25,-1,0.16,0,'Paladin - Judgement (Seal of Light, Seal of Wisdom, Seal of Justice)'),(20167,0,0,0.15,0,'Paladin - Seal of Light Proc'),(25742,0,0,0.039,0,'Paladin - Seal of Righteousness Dummy Proc'),(50256,0,0,0.08,0,'Pet Skills - Bear (Swipe)'),(2818,0,0,0,0.03,'Rogue - Deadly Poison All Ranks($AP*0.12 / number of ticks)'),(703,0,0,0,0.07,'Rogue - Garrote'),(1776,1,-1,0.21,-1,'Rogue - Gouge'),(8680,0,0,0.1,0,'Rogue - Instant Poison Rank 1'),(13218,0,0,0.04,0,'Rogue - Wound Poison Rank 1'),(57755,0,0,0.5,0,'Warrior - Heroic Throw'),(20253,0,0,0.12,0,'Warrior - Intercept'),(61491,0,0,0.12,0,'Warrior - Intercept'),(6572,1,-1,0.207,-1,'Warrior - Revenge'),(64382,0,0,0.5,0,'Warrior - Shattering Throw'),(6343,0,0,0.12,0,'Warrior - Thunder Clap'),(52212,0,0,0.0475,0,'Death Knight - Death and Decay'),(48721,0,0,0.04,0,'Death Knight - Blood Boil'),(45477,0,0,0.1,0,'Death Knight - Icy Touch'),(7001,0,0.308,0,0,'Lightwell Renew'),(30108,0,0.2,0,0,'Unstable Affliction'),(31117,1.8,0,0,0,'Unstable Affliction'),(6229,0.807,0,0,0,'Shadow Ward'),(48181,0.429,0,0,0,'Haunt'),(34433,0.3568,0,0,0,'Shadowfiend'),(33110,0.318,0,0,0,'Prayer of Mending'),(17,0.87,0,0,0,'Power Word: Shield'),(1463,0.807,0,0,0,'Mana Shield'),(11426,0.807,0,0,0,'Ice Barrier'),(543,0.807,0,0,0,'Mage Ward'),(33778,0.58,0,0,0,'Lifebloom'),(31804,0.223,-1,0.14,0,'Paladin - Judgement of Vengeance'),(33745,0,0,0,0.01,'Druid - Lacerate Rank 1($AP*0.05/number of ticks)'),(64844,0.429,-1,-1,-1,'Priest - Divine Hymn'),(50288,0.247,-1,-1,-1,'Druid - Starfall (DIRECT)'),(31707,0.8333,0,0,0,'Mage - Water Elemental Waterbolt'),(34914,-1,0.4,-1,-1,'Priest - Vampiric Touch'),(64085,1.2,-1,-1,-1,'Priest - Vampiric Touch (Dispelled)'),(53301,0,0,0,0.232,'Hunter - Explosive Shot'),(50536,-1,0,-1,-1,'Death Knight - Unholy Blight (Rank 1)'),(54181,0,-1,-1,-1,'Warlock - Fel Synergy'),(63544,0,-1,-1,-1,'Priest - Empowered Renew'),(56903,0,0,0,0,'Death Knight - Lichflame'),(50401,0,0,0,0,'Death Knight - Razor Frost'),(339,-1,0.1,-1,-1,'Druid - Entangling Roots'),(5185,0.806,-1,-1,-1,'Druid - Healing Touch'),(42231,0.095,-1,-1,-1,'Druid - Hurricane Triggered'),(5570,-1,0.2,-1,-1,'Druid - Insect Swarm'),(33763,0.0234,0.09518,0,0,'Druid - Lifebloom HoT(rank 1)'),(8921,0.18,0.13,-1,-1,'Druid - Moonfire'),(50464,0.266,-1,-1,-1,'Druid - Nourish'),(8936,0.2936,0.188,-1,-1,'Druid - Regrowth'),(774,-1,0.37604,-1,-1,'Druid - Rejuvenation'),(2912,1,-1,-1,-1,'Druid - Starfire'),(18562,0.536,0,0,0,'Druid - Swiftmend'),(44203,0.398,-1,-1,-1,'Druid - Tranquility Triggered'),(61391,0.126,-1,-1,-1,'Druid - Typhoon'),(48438,-1,0.11505,-1,-1,'Druid - Wild Growth'),(5176,0.714,-1,-1,-1,'Druid - Wrath'),(55039,0,0,0,0,'Item - Gnomish Lightning Generator'),(40293,0,0,0,0,'Item - Siphon Essence'),(44425,0.803,-1,-1,-1,'Mage - Arcane Barrage'),(30451,1.057,-1,-1,-1,'Mage - Arcane Blast'),(1449,0.143,-1,-1,-1,'Mage - Arcane Explosion'),(7268,0.246,-1,-1,-1,'Mage - Arcane Missiles Triggered Spell Rank 1'),(11113,0.143,-1,-1,-1,'Mage - Blast Wave'),(42208,0.095,-1,-1,-1,'Mage - Blizzard Triggered Spell'),(120,0.214,-1,-1,-1,'Mage - Cone of Cold'),(31661,0.193,-1,-1,-1,'Mage - Dragons Breath'),(133,1.124,-1,-1,-1,'Mage - Fire Ball'),(2136,0.429,-1,-1,-1,'Mage - Fire Blast'),(2120,0.146,0.122,-1,-1,'Mage - Flamestrike'),(116,0.857,-1,-1,-1,'Mage - Frostbolt'),(122,0.193,-1,-1,-1,'Mage - Frost Nova'),(44614,0.977,-1,-1,-1,'Mage - Frostfire Bolt'),(30455,0.378,-1,-1,-1,'Mage - Ice Lance'),(44457,0.233,0.2,-1,-1,'Mage - Living Bomb'),(34913,0,0,0,0,'Mage - Molten Armor Triggered Rank 1'),(11366,1.25,0.05,-1,-1,'Mage - Pyroblast'),(2948,0.512,-1,-1,-1,'Mage - Scorch'),(19750,0.863,-1,-1,-1,'Paladin - Flash of Light'),(53595,0,0,0,0,'Paladin - Hammer of the Righteous'),(62124,1,-1,-1,-1,'Paladin - Hand of Reckoning'),(635,0.432,-1,-1,-1,'Paladin - Holy Light'),(25914,0.269,-1,-1,-1,'Paladin - Holy Shock Triggered Heal Rank 1'),(25912,0.429,-1,-1,-1,'Paladin - Holy Shock Triggered Hurt Rank 1'),(20424,0,0,0,0,'Paladin - Seal of Command Proc'),(32546,0.544,-1,-1,-1,'Priest - Binding Heal'),(27813,0,0,0,0,'Priest - Blessed Recovery Rank 1'),(34861,0.26,-1,-1,-1,'Priest - Circle of Healing'),(19236,0.8068,-1,-1,-1,'Priest - Desperate Prayer'),(2944,-1,0.1849,-1,-1,'Priest - Devouring Plague'),(2061,0.725,-1,-1,-1,'Priest - Flash Heal'),(2060,0.967,-1,-1,-1,'Priest - Greater Heal'),(14914,0.571,0.024,-1,-1,'Priest - Holy Fire'),(15237,0.143,-1,-1,-1,'Priest - Holy Nova Damage'),(23455,0.1963,-1,-1,-1,'Priest - Holy Nova Heal Rank 1'),(8129,0,0,0,0,'Priest - Mana Burn'),(8092,0.9858,-1,-1,-1,'Priest - Mind Blast'),(49821,0.1311,-1,-1,-1,'Priest - Mind Sear Trigger Rank 1'),(47750,0.321,-1,-1,-1,'Priest - Penance Heal (Rank 1)'),(52954,0.5362,-1,-1,-1,'Priest - Penance Heal (Rank 3)'),(58985,0.5362,-1,-1,-1,'Priest - Penance Heal (Rank 4)'),(47666,0.458,-1,-1,-1,'Priest - Penance Hurt (Rank 1)'),(596,0.34,-1,-1,-1,'Priest - Prayer of Healing'),(33619,0,0,0,0,'Priest - Reflective Shield'),(139,-1,0.376,-1,-1,'Priest - Renew'),(32379,0.282,-1,-1,-1,'Priest - Shadow Word: Death'),(589,-1,0.1829,-1,-1,'Priest - Shadow Word: Pain'),(585,0.856,-1,-1,-1,'Priest - Smite'),(1064,0.35,-1,-1,-1,'Shaman - Chain Heal'),(421,0.571,-1,-1,-1,'Shaman - Chain Lightning'),(974,0.243625,-1,-1,-1,'Shaman - Earth Shield'),(379,0,0,0,0,'Shaman - Earth Shield Triggered'),(8042,0.386,-1,-1,-1,'Shaman - Earth Shock'),(8050,0.214,0.1,-1,-1,'Shaman - Flame Shock'),(8056,0.386,-1,-1,-1,'Shaman - Frost Shock'),(8034,0.1,-1,-1,-1,'Shaman - Frostbrand Attack Rank 1'),(2645,0,0,0,0,'Shaman - Glyph of Ghost Wolf'),(52042,0.0827,0.083,-1,-1,'Shaman - Healing Stream Totem Triggered Heal'),(331,0.302,-1,-1,-1,'Shaman - Healing Wave'),(51505,0.628,-1,-1,-1,'Shaman - Lava Burst'),(8004,0.604,-1,-1,-1,'Shaman - Healing Surge'),(403,0.714,-1,-1,-1,'Shaman - Lightning Bolt'),(26364,0.267,-1,-1,-1,'Shaman - Lightning Shield Proc Rank 1'),(8188,0.1,-1,-1,-1,'Shaman - Magma Totam Passive Rank 1'),(61295,0.238,0.18,-1,-1,'Shaman - Riptide'),(3606,0.167,-1,-1,-1,'Shaman - Searing Totem Attack Rank 1'),(50796,0.628,-1,-1,-1,'Warlock - Chaos Bolt'),(17962,0,0,0,0,'Warlock - Conflagrate'),(172,-1,0.2,-1,-1,'Warlock - Corruption'),(980,-1,0.1,-1,-1,'Warlock - Curse of Agony'),(603,-1,2,-1,-1,'Warlock - Curse of Doom'),(6789,0.188,-1,-1,-1,'Warlock - Death Coil'),(689,-1,0.143,-1,-1,'Warlock - Drain Life'),(1120,-1,0.429,-1,-1,'Warlock - Drain Soul'),(28176,0,0,0,0,'Warlock - Fel Armor'),(18790,0,0,0,0,'Warlock - Fel Stamina'),(755,-1,0.4485,-1,-1,'Warlock - Health Funnel'),(1949,-1,0.0949,-1,-1,'Warlock - Hellfire'),(5857,0.095,-1,-1,-1,'Warlock - Hellfire Effect on Enemy Rank 1'),(348,0.22,0.2,-1,-1,'Warlock - Immolate'),(29722,0.539,-1,-1,-1,'Warlock - Incinerate'),(42223,0.191,-1,-1,-1,'Warlock - Rain of Fire Triggered Rank 1'),(5676,0.378,-1,-1,-1,'Warlock - Searing Pain'),(27243,0.25,0.25,-1,-1,'Warlock - Seed of Corruption'),(686,0.754,-1,-1,-1,'Warlock - Shadow Bolt'),(17877,1.056,-1,-1,-1,'Warlock - Shadowburn'),(47960,0.2,0.0667,-1,-1,'Warlock - Shadowflame Rank 1'),(30283,0.214,-1,-1,-1,'Warlock - Shadowfury'),(63106,0,0,0,0,'Warlock - Siphon Life Triggered'),(6353,0.628,-1,-1,-1,'Warlock - Soul Fire'),(30294,0,0,0,0,'Warlock - Soul Leech'),(12654,0,0,0,0,'Mage - Ignite'),(51460,0,-1,-1,-1,'Death Knight - Necrosis'),(54757,0,-1,-1,-1,'Hand-Mounted Pyro Rocket - Pyro Rocket'),(45055,0,-1,-1,-1,'Timbal\'s Focusing Crystal - Shadow Bolt'),(60203,0,-1,-1,-1,'Darkmoon Card: Death'),(60488,0,-1,-1,-1,'Extract of Necromatic Power'),(45429,0,-1,-1,-1,'Shattered Sun Pendant of Acumen - Arcane Bolt'),(63675,0,0,0,0,'Priest - Improved Devouring Plague'),(25997,0,0,0,0,'Paladin - Eye for an Eye'),(59638,0.25,-1,0,-1,'Mage - Mirror Image Frostbolt'),(59637,0.15,-1,0,-1,'Mage - Mirror Image Fire Blast'),(56131,0,0,0,0,'Priest - Glyph of Dispel Magic'),(56160,0,0,0,0,'Priest - Glyph of Power Word: Shield'),(52752,0,0,0,0,'Ancestral Awakening'),(55533,0,0,0,0,'Shaman - Glyph of Healing Wave'),(45284,0.536,-1,-1,-1,'Shaman - LO Lightning Bolt'),(45297,0.429,-1,-1,-1,'Shaman - LO Chain Lightning'),(10444,0,0,0,0,'Shaman - Flametongue Trigger'),(70809,0,0,0,0,'Item - Shaman T10 Restoration 4P Bonus'),(56161,0,0,0,0,'Priest - Glyph of Prayer of Healing'),(77478,0.21,-1,-1,-1,'Shaman - Earthquake'),(51490,0.571,-1,-1,-1,'Shaman - Thunderstorm'),(73683,0.429,-1,-1,-1,'Shaman - Unleash Flame'),(77472,0.967,-1,-1,-1,'Shaman - Greater Healing Wave'),(8187,0.067,-1,-1,-1,'Shaman - Magma Totem'),(88767,0,-1,-1,-1,'Shaman - Fulmination'),(70691,0,0,0,0,'Druid - Rejuvenation T10 4P proc'),(64891,0,0,0,0,'Paladin T8 Holy 2P Bonus'),(27285,0.191,-1,-1,-1,'Warlock - Seed of Corruption Proc'),(64801,0.47,0,0,0,'Druid - T8 Restoration 4P Bonus'),(7294,0.033,-1,-1,-1,'Paladin - Retribution Aura'),(467,0.033,-1,-1,-1,'Druid - Thorns'),(69729,-1,0,-1,-1,'Item - Onyxia 10 Caster Trinket - Searing Flames'),(69730,-1,0,-1,-1,'Item - Onyxia 25 Caster Trinket - Searing Flames'),(69733,0,-1,-1,-1,'Item - Onyxia 10 Caster Trinket - Cauterizing Heal'),(69734,0,-1,-1,-1,'Item - Onyxia 25 Caster Trinket - Cauterizing Heal'),(50294,0.13,-1,-1,-1,'Druid - Starfall (AOE)'),(71757,0,0,0,0,'Mage - Deep Freeze'),(79136,0,0,0.1459,0,'Rogue - Venomous Wound');
/*!40000 ALTER TABLE `spell_bonus_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spell_dbc`
--

DROP TABLE IF EXISTS `spell_dbc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spell_dbc` (
  `Id` int(10) unsigned NOT NULL,
  `Dispel` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `Mechanic` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `Attributes` int(10) unsigned NOT NULL DEFAULT '0',
  `AttributesEx` int(10) unsigned NOT NULL DEFAULT '0',
  `AttributesEx2` int(10) unsigned NOT NULL DEFAULT '0',
  `AttributesEx3` int(10) unsigned NOT NULL DEFAULT '0',
  `AttributesEx4` int(10) unsigned NOT NULL DEFAULT '0',
  `AttributesEx5` int(10) unsigned NOT NULL DEFAULT '0',
  `Stances` int(10) unsigned NOT NULL DEFAULT '0',
  `StancesNot` int(10) unsigned NOT NULL DEFAULT '0',
  `Targets` int(10) unsigned NOT NULL DEFAULT '0',
  `CastingTimeIndex` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `AuraInterruptFlags` int(10) unsigned NOT NULL DEFAULT '0',
  `ProcFlags` int(10) unsigned NOT NULL DEFAULT '0',
  `ProcChance` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `ProcCharges` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `MaxLevel` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `BaseLevel` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `SpellLevel` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `DurationIndex` smallint(5) unsigned NOT NULL DEFAULT '0',
  `RangeIndex` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `StackAmount` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `EquippedItemClass` int(11) NOT NULL DEFAULT '-1',
  `EquippedItemSubClassMask` int(11) NOT NULL DEFAULT '0',
  `EquippedItemInventoryTypeMask` int(11) NOT NULL DEFAULT '0',
  `Effect1` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `Effect2` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `Effect3` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `EffectDieSides1` int(11) NOT NULL DEFAULT '0',
  `EffectDieSides2` int(11) NOT NULL DEFAULT '0',
  `EffectDieSides3` int(11) NOT NULL DEFAULT '0',
  `EffectRealPointsPerLevel1` float NOT NULL DEFAULT '0',
  `EffectRealPointsPerLevel2` float NOT NULL DEFAULT '0',
  `EffectRealPointsPerLevel3` float NOT NULL DEFAULT '0',
  `EffectBasePoints1` int(11) NOT NULL DEFAULT '0',
  `EffectBasePoints2` int(11) NOT NULL DEFAULT '0',
  `EffectBasePoints3` int(11) NOT NULL DEFAULT '0',
  `EffectMechanic1` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `EffectMechanic2` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `EffectMechanic3` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `EffectImplicitTargetA1` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `EffectImplicitTargetA2` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `EffectImplicitTargetA3` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `EffectImplicitTargetB1` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `EffectImplicitTargetB2` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `EffectImplicitTargetB3` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `EffectRadiusIndex1` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `EffectRadiusIndex2` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `EffectRadiusIndex3` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `EffectApplyAuraName1` smallint(5) unsigned NOT NULL DEFAULT '0',
  `EffectApplyAuraName2` smallint(5) unsigned NOT NULL DEFAULT '0',
  `EffectApplyAuraName3` smallint(5) unsigned NOT NULL DEFAULT '0',
  `EffectAmplitude1` int(11) NOT NULL DEFAULT '0',
  `EffectAmplitude2` int(11) NOT NULL DEFAULT '0',
  `EffectAmplitude3` int(11) NOT NULL DEFAULT '0',
  `EffectMultipleValue1` float NOT NULL DEFAULT '0',
  `EffectMultipleValue2` float NOT NULL DEFAULT '0',
  `EffectMultipleValue3` float NOT NULL DEFAULT '0',
  `EffectMiscValue1` int(11) NOT NULL DEFAULT '0',
  `EffectMiscValue2` int(11) NOT NULL DEFAULT '0',
  `EffectMiscValue3` int(11) NOT NULL DEFAULT '0',
  `EffectMiscValueB1` int(11) NOT NULL DEFAULT '0',
  `EffectMiscValueB2` int(11) NOT NULL DEFAULT '0',
  `EffectMiscValueB3` int(11) NOT NULL DEFAULT '0',
  `EffectTriggerSpell1` int(10) unsigned NOT NULL DEFAULT '0',
  `EffectTriggerSpell2` int(10) unsigned NOT NULL DEFAULT '0',
  `EffectTriggerSpell3` int(10) unsigned NOT NULL DEFAULT '0',
  `EffectSpellClassMaskA1` int(10) unsigned NOT NULL DEFAULT '0',
  `EffectSpellClassMaskA2` int(10) unsigned NOT NULL DEFAULT '0',
  `EffectSpellClassMaskA3` int(10) unsigned NOT NULL DEFAULT '0',
  `EffectSpellClassMaskB1` int(10) unsigned NOT NULL DEFAULT '0',
  `EffectSpellClassMaskB2` int(10) unsigned NOT NULL DEFAULT '0',
  `EffectSpellClassMaskB3` int(10) unsigned NOT NULL DEFAULT '0',
  `EffectSpellClassMaskC1` int(10) unsigned NOT NULL DEFAULT '0',
  `EffectSpellClassMaskC2` int(10) unsigned NOT NULL DEFAULT '0',
  `EffectSpellClassMaskC3` int(10) unsigned NOT NULL DEFAULT '0',
  `MaxTargetLevel` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `SpellFamilyName` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `SpellFamilyFlags1` int(10) unsigned NOT NULL DEFAULT '0',
  `SpellFamilyFlags2` int(10) unsigned NOT NULL DEFAULT '0',
  `SpellFamilyFlags3` int(10) unsigned NOT NULL DEFAULT '0',
  `MaxAffectedTargets` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `DmgClass` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `PreventionType` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `DmgMultiplier1` float NOT NULL DEFAULT '0',
  `DmgMultiplier2` float NOT NULL DEFAULT '0',
  `DmgMultiplier3` float NOT NULL DEFAULT '0',
  `AreaGroupId` int(11) NOT NULL DEFAULT '0',
  `SchoolMask` int(10) unsigned NOT NULL DEFAULT '0',
  `Comment` text NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='Custom spell.dbc entries';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spell_dbc`
--

LOCK TABLES `spell_dbc` WRITE;
/*!40000 ALTER TABLE `spell_dbc` DISABLE KEYS */;
/*!40000 ALTER TABLE `spell_dbc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spell_enchant_proc_data`
--

DROP TABLE IF EXISTS `spell_enchant_proc_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spell_enchant_proc_data` (
  `entry` int(10) unsigned NOT NULL,
  `customChance` int(10) unsigned NOT NULL DEFAULT '0',
  `PPMChance` float unsigned NOT NULL DEFAULT '0',
  `procEx` float unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='Spell enchant proc data';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spell_enchant_proc_data`
--

LOCK TABLES `spell_enchant_proc_data` WRITE;
/*!40000 ALTER TABLE `spell_enchant_proc_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `spell_enchant_proc_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spell_group`
--

DROP TABLE IF EXISTS `spell_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spell_group` (
  `id` int(11) unsigned NOT NULL DEFAULT '0',
  `spell_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`spell_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='Spell System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spell_group`
--

LOCK TABLES `spell_group` WRITE;
/*!40000 ALTER TABLE `spell_group` DISABLE KEYS */;
INSERT INTO `spell_group` VALUES (1,2367),(1,2374),(1,3160),(1,3164),(1,7844),(1,8212),(1,10667),(1,10669),(1,11328),(1,11334),(1,11390),(1,11405),(1,11406),(1,11474),(1,16322),(1,16323),(1,16329),(1,17038),(1,17537),(1,17538),(1,17539),(1,17624),(1,17626),(1,17627),(1,17628),(1,17629),(1,21920),(1,26276),(1,28486),(1,28488),(1,28490),(1,28491),(1,28493),(1,28497),(1,28501),(1,28503),(1,28518),(1,28519),(1,28520),(1,28521),(1,28540),(1,33720),(1,33721),(1,33726),(1,38954),(1,40567),(1,40568),(1,40572),(1,40573),(1,40575),(1,40576),(1,41608),(1,41609),(1,41610),(1,41611),(1,42735),(1,45373),(1,46837),(1,46839),(1,53746),(1,53748),(1,53749),(1,53752),(1,53755),(1,53758),(1,53760),(1,54212),(1,54452),(1,54494),(1,60340),(1,60341),(1,60344),(1,60345),(1,60346),(1,62380),(1,67016),(1,67017),(1,67018),(1,79468),(1,79469),(1,79470),(1,79471),(1,79472),(1,79474),(1,79477),(1,79481),(1,79632),(1,79635),(1,79637),(1,92679),(1,94160),(2,673),(2,2378),(2,2380),(2,3166),(2,3219),(2,3220),(2,3222),(2,3223),(2,3593),(2,10668),(2,10692),(2,10693),(2,11319),(2,11348),(2,11349),(2,11364),(2,11371),(2,11396),(2,15231),(2,15233),(2,16321),(2,16325),(2,16326),(2,16327),(2,17535),(2,17624),(2,17626),(2,17627),(2,17628),(2,17629),(2,24361),(2,24363),(2,24382),(2,24383),(2,24417),(2,27652),(2,27653),(2,28502),(2,28509),(2,28514),(2,28518),(2,28519),(2,28520),(2,28521),(2,28540),(2,29348),(2,39625),(2,39626),(2,39627),(2,39628),(2,40567),(2,40568),(2,40572),(2,40573),(2,40575),(2,40576),(2,41608),(2,41609),(2,41610),(2,41611),(2,42735),(2,46837),(2,46839),(2,53747),(2,53751),(2,53752),(2,53755),(2,53758),(2,53760),(2,53763),(2,53764),(2,54212),(2,60343),(2,60347),(2,62380),(2,67016),(2,67017),(2,67018),(2,79469),(2,79470),(2,79471),(2,79472),(2,79480),(2,79631),(2,79637),(2,92679),(2,94160),(3,40567),(3,40568),(3,40572),(3,40573),(3,40575),(3,40576),(4,41608),(4,41609),(4,41610),(4,41611),(4,46837),(4,46839),(1001,18125),(1001,18141),(1001,19705),(1001,19706),(1001,19708),(1001,19709),(1001,19710),(1001,19711),(1001,23697),(1001,24799),(1001,24870),(1001,25694),(1001,25941),(1001,33254),(1001,33256),(1001,33257),(1001,33259),(1001,33261),(1001,33263),(1001,33265),(1001,33268),(1001,33272),(1001,35272),(1001,40323),(1001,42293),(1001,43764),(1001,43771),(1001,44097),(1001,44098),(1001,44099),(1001,44100),(1001,44101),(1001,44102),(1001,44104),(1001,44105),(1001,44106),(1001,45245),(1001,45619),(1001,46682),(1001,46687),(1001,46899),(1001,53284),(1001,57079),(1001,57097),(1001,57100),(1001,57102),(1001,57107),(1001,57111),(1001,57139),(1001,57286),(1001,57288),(1001,57291),(1001,57294),(1001,57325),(1001,57327),(1001,57329),(1001,57332),(1001,57334),(1001,57356),(1001,57358),(1001,57360),(1001,57363),(1001,57365),(1001,57367),(1001,57371),(1001,57373),(1001,57399),(1001,58468),(1001,58479),(1001,59230),(1001,62349),(1001,64057),(1001,65247),(1001,65365),(1001,65410),(1001,65412),(1001,65414),(1001,65415),(1001,65416),(1001,66623),(1001,66624),(1001,69559),(1002,19740),(1002,56520),(1003,6673),(1004,-1003),(1004,-1002),(1005,56521),(1006,20217),(1006,43223),(1006,56525),(1006,58054),(1006,72586),(1006,79060),(1006,79061),(1006,79062),(1006,79063),(1007,20911),(1008,23415),(1008,41450),(1009,32770),(1010,-1009),(1010,-1008),(1010,-1007),(1010,-1006),(1010,-1005),(1010,-1002),(1011,-1083),(1011,-1003),(1012,55749),(1013,8647),(1014,7386),(1015,-1014),(1015,-1013),(1015,-1012),(1016,770),(1016,16857),(1017,56626),(1018,16231),(1019,-1018),(1019,-1017),(1019,-1016),(1020,55610),(1021,8515),(1022,-1021),(1022,-1020),(1023,17007),(1024,29801),(1025,-1024),(1025,-1023),(1026,53137),(1027,19506),(1028,30802),(1029,-1028),(1029,-1027),(1029,-1026),(1030,33878),(1031,33876),(1032,46856),(1033,-2010),(1033,-1032),(1033,-1031),(1033,-1030),(1034,24907),(1035,51466),(1036,-1035),(1036,-1034),(1037,11095),(1038,11180),(1039,-1038),(1039,-1037),(1040,51099),(1041,48506),(1042,1490),(1043,-1042),(1043,-1041),(1043,-1040),(1044,54646),(1045,52109),(1046,57658),(1047,53646),(1048,-1047),(1048,-1046),(1048,-1045),(1048,-1044),(1049,33600),(1050,33191),(1051,-1050),(1051,-1049),(1052,-1051),(1053,-1052),(1054,-1053),(1054,-1052),(1055,-1054),(1056,-1055),(1057,-1056),(1057,-1055),(1058,-1057),(1059,-1058),(1059,-1046),(1060,45477),(1061,48483),(1062,53695),(1063,6343),(1064,-2009),(1064,-1063),(1064,-1062),(1064,-1061),(1064,-1060),(1066,5570),(1067,-1066),(1068,-1067),(1068,-1066),(1070,13218),(1071,19434),(1072,12294),(1073,46910),(1074,-1073),(1074,-1072),(1074,-1071),(1074,-1070),(1076,99),(1077,702),(1078,1160),(1079,-2008),(1079,-1078),(1079,-1077),(1079,-1076),(1080,8076),(1081,57330),(1082,-1081),(1082,-1080),(1083,469),(1084,6307),(1085,-1084),(1085,-1083),(1086,1459),(1087,54424),(1088,-1104),(1088,-1087),(1088,-1086),(1090,-1106),(1090,-1105),(1090,-1087),(1091,47930),(1092,20911),(1093,-1092),(1093,-1091),(1094,-1093),(1095,20138),(1096,-1095),(1096,-1094),(1097,14892),(1097,14893),(1098,16176),(1098,16177),(1099,-1098),(1099,-1097),(1100,1714),(1101,31589),(1102,5760),(1103,-1102),(1103,-1101),(1103,-1100),(1104,-1103),(1105,-1104),(1106,-1105),(1107,122),(1107,33395),(1107,55080),(1108,1126),(1108,79060),(1108,79061),(1109,21562),(1109,72590),(1110,27683),(1112,348),(1112,30108),(1113,588),(1113,73413),(1250,79104),(1250,79105),(1251,79060),(1251,79061),(1252,79062),(1252,79063),(1253,79101),(1253,79102),(1254,79062),(1254,79063),(1254,79101),(1254,79102),(1337,20154),(1337,20164),(1337,20165),(1337,31801),(1500,588),(1500,73413),(2004,12880),(2004,57514),(2004,57518),(2005,5677),(2006,-2005),(2006,-1005),(2007,6562),(2007,28878),(2008,67),(2009,8042),(2010,57386),(2011,49016),(2012,57933),(2013,12292),(2014,12042),(2015,34471),(2016,31884),(2017,-2016),(2017,-2015),(2017,-2014),(2017,-2013),(2017,-2012),(2017,-2011);
/*!40000 ALTER TABLE `spell_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spell_group_stack_rules`
--

DROP TABLE IF EXISTS `spell_group_stack_rules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spell_group_stack_rules` (
  `group_id` int(11) unsigned NOT NULL DEFAULT '0',
  `stack_rule` tinyint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Spell Group Stack Rules';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spell_group_stack_rules`
--

LOCK TABLES `spell_group_stack_rules` WRITE;
/*!40000 ALTER TABLE `spell_group_stack_rules` DISABLE KEYS */;
INSERT INTO `spell_group_stack_rules` VALUES (1,1),(2,1),(1001,1),(1002,1),(1003,1),(1004,1),(1005,1),(1006,1),(1007,1),(1008,1),(1009,1),(1010,2),(1011,2),(1015,1),(1016,1),(1019,1),(1022,1),(1025,1),(1029,1),(1033,1),(1036,1),(1043,1),(1046,1),(1048,1),(1051,1),(1055,1),(1053,1),(1059,1),(1064,1),(1068,1),(1074,1),(1079,1),(1082,1),(1085,1),(1088,1),(1090,1),(1093,1),(1096,1),(1099,1),(1103,1),(1107,1),(1108,1),(1109,1),(1110,1),(1112,2),(1052,1),(1056,1),(1058,1),(1067,1),(1094,1),(1104,1),(1105,1),(1106,1),(1250,2),(1500,2),(1113,2),(1252,2),(1253,2),(1254,2),(1251,2),(2017,1),(2006,1),(2004,1),(2007,1),(1337,1);
/*!40000 ALTER TABLE `spell_group_stack_rules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spell_learn_spell`
--

DROP TABLE IF EXISTS `spell_learn_spell`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spell_learn_spell` (
  `entry` mediumint(7) unsigned NOT NULL DEFAULT '0',
  `SpellID` mediumint(7) unsigned NOT NULL DEFAULT '0',
  `Active` tinyint(3) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`entry`,`SpellID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='Item System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spell_learn_spell`
--

LOCK TABLES `spell_learn_spell` WRITE;
/*!40000 ALTER TABLE `spell_learn_spell` DISABLE KEYS */;
INSERT INTO `spell_learn_spell` VALUES (53428,53341,1),(53428,53343,1),(17002,24867,0),(24866,24864,0),(33872,47179,0),(33873,47180,0),(33943,34090,1),(58984,21009,1),(2098,79327,0),(87491,86470,1),(87492,86471,1),(87493,86472,1),(86467,86473,1),(87494,86474,1),(87495,86475,1),(87496,86476,1),(87497,86477,1),(87498,86478,1),(87500,86479,1);
/*!40000 ALTER TABLE `spell_learn_spell` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spell_linked_spell`
--

DROP TABLE IF EXISTS `spell_linked_spell`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spell_linked_spell` (
  `spell_trigger` mediumint(8) NOT NULL,
  `spell_effect` mediumint(8) NOT NULL DEFAULT '0',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  UNIQUE KEY `trigger_effect_type` (`spell_trigger`,`spell_effect`,`type`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Spell System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spell_linked_spell`
--

LOCK TABLES `spell_linked_spell` WRITE;
/*!40000 ALTER TABLE `spell_linked_spell` DISABLE KEYS */;
INSERT INTO `spell_linked_spell` VALUES (31224,-1543,2,'Cloak of Shadows - Flare'),(15237,23455,0,'Holy Nova (rank1)'),(-19386,24131,0,'Wyvern Sting'),(47585,60069,2,'Dispersion (transform/regen)'),(47585,63230,2,'Dispersion (immunity)'),(17767,54501,2,'Consume Shadows - Rank 1'),(-5143,-36032,0,'Arcane Missiles Rank 1'),(53563,53651,2,'Beacon of Light'),(781,56446,0,'Disengage'),(57635,57636,0,'Disengage'),(60932,60934,0,'Disengage'),(61507,61508,0,'Disengage'),(49576,49560,0,'Death Grip'),(47897,47960,1,'Shadowflame Rank 1'),(33206,44416,2,'Pain Suppression (threat)'),(52415,52418,0,'Carrying Seaforium - Add'),(52610,62071,0,'Savage Roar'),(-52610,-62071,0,'Savage Roar'),(51209,55095,1,'Hungering cold - frost fever'),(50334,58923,2,'Berserk - modify target number aura'),(-5229,-51185,0,'King of the Jungle - remove with enrage'),(20594,65116,0,'Stoneform'),(49039,50397,2,'Lichborne - shapeshift'),(64382,64380,0,'Shattering Throw'),(-59907,7,0,'Lightwell Charges - Suicide'),(40214,40216,2,'Dragonmaw Illusion'),(40214,42016,2,'Dragonmaw Illusion'),(36574,36650,0,'Apply Phase Slip Vulnerability'),(-30410,44032,0,'Manticron Cube Mind Exhaustion'),(-33711,33686,0,'Murmur\'s Shockwave (Normal)'),(-38794,33686,0,'Murmur\'s Shockwave (Heroic)'),(33686,31705,0,'Murmur\'s Shockwave Jump'),(44008,45265,1,'Static Disruption Visual'),(43648,44007,1,'Storm Eye Safe Zone'),(44007,-43657,2,'Storm Eye Safe Zone Immune'),(43658,43653,0,'Electrical Arc Visual'),(43658,43654,0,'Electrical Arc Visual'),(43658,43655,0,'Electrical Arc Visual'),(43658,43656,0,'Electrical Arc Visual'),(43658,43659,0,'Electrical Arc Visual'),(39835,39968,1,'Needle Spine'),(-41376,41377,0,'Spite'),(41126,41131,1,'Flame Crash'),(-41914,41915,0,'Summon Parasitic Shadowfiend'),(-41917,41915,0,'Summon Parasitic Shadowfiend'),(39908,40017,1,'Eye Blast'),(40604,40616,1,'Fel Rage Aura'),(40616,41625,1,'Fel Rage Aura'),(41292,42017,2,'Aura of Suffering'),(44869,44866,1,'Spectral Blast Portal'),(44869,46019,1,'Spectral Blast Teleport'),(46019,46021,1,'Spectral Realm Aura'),(-46021,46020,0,'Teleport: Normal Realm'),(46020,44867,1,'Spectral Exhaustion'),(44867,-46019,2,'Spectral Exhaustion - Teleport: Spectral Realm'),(45661,45665,1,'Encapsulate'),(45347,-45348,1,'Remove Flame Touched'),(45348,-45347,1,'Remove Dark Touched'),(45248,45347,1,'Apply Dark Touched'),(45329,45347,1,'Apply Dark Touched'),(45256,45347,1,'Apply Dark Touched'),(45270,45347,1,'Apply Dark Touched'),(45342,45348,1,'Apply Flame Touched'),(46771,45348,1,'Apply Flame Touched'),(45271,45347,1,'Apply Dark Touched'),(45246,45348,1,'Apply Flame Touched'),(44869,-45018,1,'Remove Arcane Buffet'),(46019,-45018,1,'Remove Arcane Buffet'),(46242,46247,0,'Black Hole Visual (Birth)'),(46228,46235,0,'Black Hole Visual (Grown)'),(46228,-46247,0,'Black Hole Visual (Grown)'),(46262,46265,0,'Void Zone Visual'),(-55053,55601,0,'Deathbloom (H)'),(-29865,55594,0,'Deathbloom'),(54097,-54100,1,'Widow\'s Embrace - Frenzy (H)'),(28732,-28798,1,'Widow\'s Embrace - Frenzy'),(58666,58672,1,'Impale (Archavon)'),(60882,58672,1,'Impale (Archavon)'),(16857,60089,0,'Faerie Fire (Feral)'),(-54361,54343,0,'Void Shift (Normal) - Void Shifted'),(-59743,54343,0,'Void Shift (Heroic) - Void Shifted'),(-28169,28206,0,'Mutating Injection - Mutagen Explosion'),(-28169,28240,0,'Mutating Injection - Poison Cloud'),(28059,-28084,1,'Positive Charge - Negative Charge'),(-28059,-29659,0,'Positive Charge'),(28084,-28059,1,'Negative Charge - Positive Charge'),(-28084,-29660,0,'Negative Charge'),(39088,-39091,1,'Positive Charge - Negative Charge'),(-39088,-29659,0,'Positive Charge'),(39091,-39088,1,'Negative Charge - Positive Charge'),(-39091,-39092,0,'Negative Charge'),(33878,-33876,1,'Mangle - Remover'),(33876,-33878,1,'Mangle - Remover'),(-62475,-62399,0,'System Shutdown - Overload Circuit'),(-62475,-62375,0,'System Shutdown - Gathering Speed'),(-62475,62472,0,'System Shutdown'),(62427,62340,2,'Load into Catapult - Passenger Loaded'),(54643,-54643,2,'Wintergrasp Defender Teleport'),(54850,54851,1,'Emerge - Emerge Summon'),(-58600,44795,0,'No fly zone - Parachute'),(-30421,38637,0,'Netherspite\'s Perseverence'),(-30422,38638,0,'Netherspite\'s Serenity'),(-30423,38639,0,'Netherspite\'s Dominance'),(66680,66547,0,'Confess - Confess'),(66889,-66865,0,'Remove Vengeance'),(19263,67801,2,'Deterrence'),(66744,66747,0,'totem of the earthen ring'),(53099,53098,0,'trigger teleport to acherus (for quest 12757)'),(45524,55095,0,'Chains of Ice - Frost Fever'),(52410,-52418,0,'Carrying Seaforium - Remove'),(69378,72586,1,'Drums of the Forgotten Kings'),(69377,72590,1,'Runescroll of Fortitude'),(54861,-23335,0,'Drop Flag on Nitro Boost WSG'),(54861,-23333,0,'Drop Flag on Nitro Boost WSG'),(55004,-23335,0,'Drop Flag on Nitro Boost WSG'),(55004,-23333,0,'Drop Flag on Nitro Boost WSG'),(54861,-34976,0,'Drop Flag on Nitro Boost EOS'),(55004,-34976,0,'Drop Flag on Nitro Boost EOS'),(50141,50001,0,'Blood Oath to Blood Oath Aura'),(61263,61267,0,'Intravenous Healing Effect'),(61263,61268,0,'Intravenous Mana Regeneration Effect'),(66870,-66823,1,'Remove Paralytic Toxin when hit by Burning Bite'),(67621,-67618,1,'Remove Paralytic Toxin when hit by Burning Bite'),(67622,-67619,1,'Remove Paralytic Toxin when hit by Burning Bite'),(67623,-67620,1,'Remove Paralytic Toxin when hit by Burning Bite'),(-66683,68667,0,'Icehowl - Surge of Adrenaline'),(-67661,68667,0,'Icehowl - Surge of Adrenaline'),(58875,58876,1,'Spirit Walk'),(7744,72757,0,'Will of the Forsaken Cooldown Trigger (WOTF)'),(42292,72752,0,'Will of the Forsaken Cooldown Trigger'),(59752,72752,0,'Will of the Forsaken Cooldown Trigger'),(-73023,69706,0,'Rotface: Mutated Infection Summon'),(-73022,69706,0,'Rotface: Mutated Infection Summon'),(-71224,69706,0,'Rotface: Mutated Infection Summon'),(-69674,69706,0,'Rotface: Mutated Infection Summon'),(-68839,68846,0,'Bronjahm: Corrupt Soul Summon'),(66548,66550,0,'Isle of Conquest (IN>OUT)'),(66549,66551,0,'Isle of Conquest (OUT>IN)'),(66551,-66548,2,'Isle of Conquest Teleport (OUT>IN) Debuff limit'),(66550,-66549,2,'Isle of Conquest Teleport (IN>OUT) Debuff limit'),(57994,38328,0,'Wind Shear'),(-82691,91264,0,'Ring of frost immune'),(120,83301,1,'Improved Cone of Cold trigger - rank 1'),(120,83302,1,'Improved Cone of Cold trigger - rank 2'),(20473,-88819,0,'Holy Shock - removes Daybreak'),(122,69571,0,'Frost Nova'),(86719,82739,0,'Flame Orb - Damage'),(17962,-18118,0,'Conflagrate Daze Fix'),(73510,-34914,1,'Mind Spike rem dots'),(73510,-2944,1,'Mind Spike rem dots'),(55342,58832,0,'Mirror Image Trigger'),(100,96273,0,'Warrior - Charge Stun'),(73510,-589,1,'Mind Spike rem dots'),(53343,54586,0,'Runeforging Credit'),(53341,54586,0,'Runeforging Credit'),(-74562,74610,0,'Fiery Combustion removed -> Combustion'),(-74792,74800,0,'Soul Consumption removed -> Consumption'),(34709,-1784,1,'Shadow Sight - Stealth'),(54744,-54742,1,'Archerus Teleporter Hall -> Heart'),(54725,-54724,1,'Archerus Teleporter Phases Heart->Hall'),(54699,-54700,1,'Archerus Teleporter Phase Hall -> Heart'),(54746,-54745,1,'Archerus Teleporter Heart->Hall'),(32612,54661,0,'Invisibility Sanctuary Effect'),(32223,63510,2,'Improved Concentration Aura linked spell'),(19891,63510,2,'Improved Concentration Aura linked spell'),(19746,63510,2,'Improved Concentration Aura linked spell'),(7294,63510,2,'Improved Concentration Aura linked spell'),(5215,54661,0,'Prowl Sanctuary Effect'),(1856,54661,0,'Stealth Sanctuary Effect'),(1784,54661,0,'Stealth Sanctuary Effect'),(465,63510,2,'Improved Concentration Aura linked spell'),(34709,-5215,1,'Shadow Sight - Prowl'),(1064,51562,1,'Tidal Waves (Chain Heal) (rank 1)'),(61295,51564,1,'Tidal Waves (Rip Tide) (rank 3)'),(61295,51563,1,'Tidal Waves (Rip Tide) (rank 2'),(80964,93985,1,'Skull Bash(Bear) Interrupt'),(80965,93985,1,'Skull Bash(Cat) Interrupt'),(1064,51564,1,'Tidal Waves (Chain Heal) (rank 3)'),(1064,51563,1,'Tidal Waves (Chain Heal) (rank 2)'),(61295,51562,1,'Tidal Waves (Rip Tide) (rank 1)'),(-16689,19975,1,'Nature\'s Grasp'),(1159,11196,0,' '),(33891,5420,2,'Tree of Life (armor bonus'),(-33891,-5420,2,'Tree of Life (removes armor bonus)'),(33891,52553,2,'Tree of Life (Empowered Tree of Life (+10% Healing))'),(-33891,-52553,2,'Tree of Life (removes Empowered Tree of Life (+10% Healing)'),(86507,96175,1,'Runescroll of Fortitude II'),(7376,57339,2,'Defensive Stance Passive - Tank Class Passive Threat'),(21178,57339,2,'Bear Form (Passive2) - Tank Class Passive Threat'),(25780,57340,2,'Righteous Fury - Tank Class Passive Threat'),(48263,57340,2,'Frost Presence - Tank Class Passive Threat'),(44572,71757,0,'Deep Freeze - Damage Proc'),(57294,59690,2,'Well Fed - Well Fed (DND)'),(57399,59699,2,'Well Fed - Well Fed (DND)'),(45980,46022,0,'Re-Cursive quest'),(65686,-65684,2,'Remove Dark Essence 10M'),(65684,-65686,2,'Remove Light Essence 10M'),(67222,-67176,2,'Remove Dark essence 10M H'),(67176,-67222,2,'Remove Light essence 10M H'),(67223,-67177,2,'Remove Dark essence 25M'),(67177,-67223,2,'Remove Light essence 25M'),(67224,-67178,2,'Remove Dark essence 25M H'),(67178,-67224,2,'Remove Light essence 25M H'),(66512,66510,0,'Summon Deep Jormungar on Pound Drum'),(55288,55289,0,'Ocular on script cast killcredit'),(24071,24020,1,'Axe Flurry will now throw axes at nearby players, stunning them'),(24023,12021,1,'Charge (24023) will now trigger Fixate'),(1022,25771,0,'Forbearance'),(633,25771,0,'Forbearance'),(642,25771,0,'Forbearance'),(34428,-32216,0,'-Victorious after Victory Rush'),(74554,11196,0,' '),(45543,11196,0,' '),(74555,11196,0,' '),(27031,11196,0,' '),(51827,11196,0,' '),(45544,11196,0,' '),(51803,11196,0,' '),(3268,11196,0,' '),(7926,11196,0,' '),(27030,11196,0,' '),(7927,11196,0,' '),(23567,11196,0,' '),(23569,11196,0,' '),(24414,11196,0,' '),(24412,11196,0,' '),(23696,11196,0,' '),(10838,11196,0,' '),(18610,11196,0,' '),(10839,11196,0,' '),(23568,11196,0,' '),(746,11196,0,' '),(24413,11196,0,' '),(18608,11196,0,' '),(74553,11196,0,' ');
/*!40000 ALTER TABLE `spell_linked_spell` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spell_loot_template`
--

DROP TABLE IF EXISTS `spell_loot_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spell_loot_template` (
  `entry` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `item` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `ChanceOrQuestChance` float NOT NULL DEFAULT '100',
  `lootmode` smallint(5) unsigned NOT NULL DEFAULT '1',
  `groupid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `mincountOrRef` mediumint(9) NOT NULL DEFAULT '1',
  `maxcount` tinyint(3) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`entry`,`item`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='Loot System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spell_loot_template`
--

LOCK TABLES `spell_loot_template` WRITE;
/*!40000 ALTER TABLE `spell_loot_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `spell_loot_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spell_map`
--

DROP TABLE IF EXISTS `spell_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spell_map` (
  `spell` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `map` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `quest_start` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `quest_start_active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `quest_end` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `aura_spell` mediumint(8) NOT NULL DEFAULT '0',
  `racemask` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `gender` tinyint(1) unsigned NOT NULL DEFAULT '2',
  `autocast` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`spell`,`map`,`quest_start`,`quest_start_active`,`aura_spell`,`racemask`,`gender`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spell_map`
--

LOCK TABLES `spell_map` WRITE;
/*!40000 ALTER TABLE `spell_map` DISABLE KEYS */;
/*!40000 ALTER TABLE `spell_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spell_pet_auras`
--

DROP TABLE IF EXISTS `spell_pet_auras`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spell_pet_auras` (
  `spell` mediumint(8) unsigned NOT NULL COMMENT 'dummy spell id',
  `effectId` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `pet` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'pet id; 0 = all',
  `aura` mediumint(8) unsigned NOT NULL COMMENT 'pet aura id',
  PRIMARY KEY (`spell`,`effectId`,`pet`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='CTDB Spell Pet Auras';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spell_pet_auras`
--

LOCK TABLES `spell_pet_auras` WRITE;
/*!40000 ALTER TABLE `spell_pet_auras` DISABLE KEYS */;
/*!40000 ALTER TABLE `spell_pet_auras` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spell_proc_event`
--

DROP TABLE IF EXISTS `spell_proc_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spell_proc_event` (
  `entry` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `SchoolMask` tinyint(4) NOT NULL DEFAULT '0',
  `SpellFamilyName` smallint(5) unsigned NOT NULL DEFAULT '0',
  `SpellFamilyMask0` int(10) unsigned NOT NULL DEFAULT '0',
  `SpellFamilyMask1` int(10) unsigned NOT NULL DEFAULT '0',
  `SpellFamilyMask2` int(10) unsigned NOT NULL DEFAULT '0',
  `procFlags` int(10) unsigned NOT NULL DEFAULT '0',
  `procEx` int(10) unsigned NOT NULL DEFAULT '0',
  `ppmRate` float NOT NULL DEFAULT '0',
  `CustomChance` float NOT NULL DEFAULT '0',
  `Cooldown` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Spell Proc Events';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spell_proc_event`
--

LOCK TABLES `spell_proc_event` WRITE;
/*!40000 ALTER TABLE `spell_proc_event` DISABLE KEYS */;
INSERT INTO `spell_proc_event` VALUES (324,0,0,0,0,0,0,0,0,0,3),(974,0,0,0,0,0,139944,0,0,0,3),(1463,0,0,0,0,0,0,1024,0,0,0),(3232,0,0,0,0,0,0,2,0,0,0),(5952,0,8,1,1,0,0,0,0,0,0),(6346,0,0,0,0,0,0,256,0,0,0),(7383,1,0,0,0,0,0,256,0,0,0),(7434,0,0,0,0,0,0,2,0,0,0),(8178,0,0,0,0,0,0,65536,0,0,0),(9782,0,0,0,0,0,0,64,0,0,0),(9784,0,0,0,0,0,0,64,0,0,0),(9799,0,0,0,0,0,0,2,0,0,0),(11095,0,3,16,0,0,0,0,0,0,0),(11119,4,3,0,0,0,0,2,0,0,0),(11120,4,3,0,0,0,0,2,0,0,0),(11180,16,3,0,0,0,0,0,0,0,0),(11185,0,3,128,0,0,327680,0,0,0,0),(11255,0,3,16384,0,0,0,0,0,0,0),(12169,0,0,0,0,0,0,64,0,0,0),(12668,0,4,2,0,0,0,0,0,0,30),(12298,0,0,0,0,0,0,112,0,0,0),(12311,0,4,2048,1,0,0,0,0,0,0),(12319,0,0,0,0,0,0,2,0,0,0),(12487,0,3,128,0,0,327680,0,0,0,0),(12488,0,3,128,0,0,327680,0,0,0,0),(12598,0,3,16384,0,0,0,0,0,0,0),(12289,0,4,2,0,0,0,0,0,0,60),(12724,0,0,0,0,0,0,112,0,0,0),(12725,0,0,0,0,0,0,112,0,0,0),(12797,0,4,1024,0,0,0,0,0,0,0),(12799,0,4,1024,0,0,0,0,0,0,0),(12834,0,0,0,0,0,0,2,0,0,0),(12846,4,3,0,0,0,0,2,0,0,0),(12849,0,0,0,0,0,0,2,0,0,0),(12867,0,0,0,0,0,0,2,0,0,0),(12872,0,3,16,0,0,0,0,0,0,0),(12873,0,3,16,0,0,0,0,0,0,0),(12958,0,4,2048,1,0,0,0,0,0,0),(12966,0,0,0,0,0,0,65536,0,0,0),(12967,0,0,0,0,0,0,65536,0,0,0),(12968,0,0,0,0,0,0,65536,0,0,0),(12971,0,0,0,0,0,0,2,0,0,0),(12972,0,0,0,0,0,0,2,0,0,0),(12322,0,0,33554432,1536,0,0,0,3,0,0),(13754,0,8,16,0,0,0,0,0,0,0),(13867,0,8,16,0,0,0,0,0,0,0),(13983,0,0,0,0,0,0,24,0,0,0),(14070,0,0,0,0,0,0,24,0,0,0),(14071,0,0,0,0,0,0,24,0,0,0),(14156,0,8,4063232,8,0,0,0,0,0,0),(14160,0,8,4063232,8,0,0,0,0,0,0),(14161,0,8,4063232,8,0,0,0,0,0,0),(14186,0,8,1082131720,6,0,0,2,0,0,1),(14190,0,8,1082131720,6,0,0,2,0,0,1),(14892,0,6,268443136,65540,0,0,2,0,0,0),(15088,0,0,0,0,0,0,2,0,0,0),(15128,4,0,0,0,0,0,0,0,0,0),(15277,0,0,0,0,0,0,0,6,0,0),(15286,32,6,0,0,0,0,0,0,0,0),(15346,0,0,0,0,0,0,0,6,0,0),(15362,0,6,268443136,65540,0,0,2,0,0,0),(15600,0,0,0,0,0,0,0,1,0,0),(16544,4,11,0,262144,0,196608,0,0,0,0),(16086,4,11,0,262144,0,196608,0,0,0,0),(16235,0,11,448,0,16,0,2,0,0,0),(16180,0,11,448,0,16,0,2,0,100,0),(16196,0,11,448,0,16,0,2,0,100,0),(16256,0,0,0,0,0,0,2,0,0,0),(16257,0,0,0,0,0,0,65536,0,0,0),(16277,0,0,0,0,0,0,65536,0,0,0),(16278,0,0,0,0,0,0,65536,0,0,0),(16281,0,0,0,0,0,0,2,0,0,0),(16282,0,0,0,0,0,0,2,0,0,0),(16492,0,0,0,0,0,0,0,0,10,0),(16489,0,0,0,0,0,0,0,0,10,0),(16487,0,0,0,0,0,0,0,0,10,0),(16550,0,0,0,0,0,0,2,0,0,0),(16620,0,0,0,0,0,0,0,0,0,30),(16624,0,0,0,0,0,0,64,0,0,0),(16864,0,0,0,0,0,0,0,2,3,0),(16880,72,7,103,58720258,0,0,2,0,0,60),(48483,0,7,71680,1088,0,0,0,0,0,0),(48484,0,7,71680,1088,0,0,0,0,0,0),(16958,0,0,0,0,0,0,2,0,0,0),(16961,0,0,0,0,0,0,2,0,0,0),(17106,0,7,524288,0,0,0,0,0,0,0),(17107,0,7,524288,0,0,0,0,0,0,0),(17108,0,7,524288,0,0,0,0,0,0,0),(17495,0,0,0,0,0,0,64,0,0,0),(17619,0,13,0,0,0,32768,0,0,0,0),(17793,0,5,1,0,0,0,0,0,0,0),(17796,0,5,1,0,0,0,0,0,0,0),(17801,0,5,1,0,0,0,0,0,0,0),(18094,0,5,10,0,0,0,0,0,0,0),(18095,0,5,10,0,0,0,0,0,0,0),(18820,0,0,0,0,0,0,65536,0,0,0),(19387,0,9,16,8192,0,0,0,0,0,0),(19184,0,9,16,8192,0,0,0,0,0,0),(19572,0,9,8388608,0,0,16384,0,0,0,0),(19573,0,9,8388608,0,0,16384,0,0,0,0),(20049,0,0,0,0,0,0,2,0,0,0),(20056,0,0,0,0,0,0,2,0,0,0),(20057,0,0,0,0,0,0,2,0,0,0),(20128,0,0,0,0,0,0,64,0,0,0),(20131,0,0,0,0,0,0,64,0,0,0),(20132,0,0,0,0,0,0,64,0,0,0),(20164,0,0,0,0,0,0,0,5,0,0),(20165,0,0,0,0,0,0,0,15,0,0),(20177,0,0,0,0,0,0,67,0,0,0),(20179,0,0,0,0,0,0,67,0,0,0),(20705,0,0,0,0,0,0,2,0,0,0),(20911,0,0,0,0,0,0,112,0,0,0),(21185,0,0,0,0,0,0,0,0,0,10),(21882,0,0,0,0,0,0,2,0,0,0),(21890,0,4,712396527,876,0,0,0,0,0,0),(22007,0,3,2097185,0,0,0,65536,0,0,0),(22618,0,0,0,0,0,0,64,0,0,0),(22648,0,0,0,0,0,0,2,0,0,0),(23547,0,0,0,0,0,0,32,0,0,0),(23548,0,0,0,0,0,0,64,0,0,0),(23551,0,11,192,0,0,0,0,0,0,0),(23552,0,0,0,0,0,0,0,0,0,3),(23572,0,11,192,0,0,0,0,0,0,0),(23578,0,0,0,0,0,0,0,2,0,0),(23581,0,0,0,0,0,0,0,2,0,0),(71519,0,0,0,0,0,0,0,0,0,105),(23686,0,0,0,0,0,0,0,2,0,0),(23688,0,0,0,0,0,0,65536,0,0,0),(23689,0,0,0,0,0,0,0,4,0,0),(23721,0,9,2048,0,0,0,0,0,0,0),(23920,0,0,0,0,0,0,2048,0,0,0),(24353,0,0,0,0,0,0,2,0,0,0),(24389,0,3,12582935,64,0,0,0,0,0,0),(24658,0,0,0,0,0,82192,0,0,0,0),(24905,0,0,0,0,0,0,2,15,0,0),(25050,4,0,0,0,0,0,0,0,0,0),(25669,0,0,0,0,0,0,0,1,0,0),(25988,0,0,0,0,0,0,2,0,0,0),(26016,0,0,0,0,0,0,0,2,0,0),(26107,0,0,0,0,0,0,100,0,0,0),(26119,0,10,2416967683,0,0,0,65536,0,0,0),(26128,0,0,0,0,0,0,8,0,0,0),(26135,0,10,8388608,0,0,0,65536,0,0,0),(26480,0,0,0,0,0,0,0,3,0,0),(26605,0,0,0,0,0,0,2,0,0,0),(27419,0,0,0,0,0,0,0,3,0,0),(27498,0,0,0,0,0,0,0,3,0,0),(27521,0,0,0,0,0,0,65536,0,0,15),(27656,0,0,0,0,0,0,0,3,0,0),(27774,0,0,0,0,0,0,65536,0,0,0),(27787,0,0,0,0,0,0,0,3,0,0),(27811,0,0,0,0,0,0,2,0,0,0),(27815,0,0,0,0,0,0,2,0,0,0),(27816,0,0,0,0,0,0,2,0,0,0),(28592,16,3,0,0,0,0,0,0,0,0),(28593,16,3,0,0,0,0,0,0,0,0),(28716,0,7,16,0,0,262144,0,0,0,0),(28719,0,7,32,0,0,0,2,0,0,0),(28744,0,7,64,0,0,278528,0,0,0,0),(28752,0,0,0,0,0,0,2,0,0,0),(28789,0,10,3221225472,0,0,0,0,0,0,0),(28802,0,0,0,0,0,0,65536,0,0,0),(28809,0,6,4096,0,0,0,2,0,0,0),(28812,0,8,33554438,0,0,0,2,0,0,0),(28816,0,0,0,0,0,0,0,3,0,0),(28823,0,11,192,0,0,0,0,0,0,0),(28847,0,7,32,0,0,0,0,0,0,0),(28849,0,11,128,0,0,0,0,0,0,0),(29074,20,3,0,0,0,0,2,0,0,0),(29075,20,3,0,0,0,0,2,0,0,0),(29076,20,3,0,0,0,0,2,0,0,0),(29150,0,0,0,0,0,0,0,3,0,0),(29179,0,0,0,0,0,0,2,0,0,0),(29180,0,0,0,0,0,0,2,0,0,0),(29385,0,0,0,0,0,0,0,7,0,0),(29441,0,0,0,0,0,0,8,0,0,1),(29444,0,0,0,0,0,0,8,0,0,1),(29455,0,0,0,0,0,0,64,0,0,0),(29501,0,0,0,0,0,0,0,3,0,0),(29593,0,0,0,0,0,0,112,0,0,0),(29594,0,0,0,0,0,0,112,0,0,0),(29624,0,0,0,0,0,0,0,3,0,0),(29625,0,0,0,0,0,0,0,3,0,0),(29626,0,0,0,0,0,0,0,3,0,0),(29632,0,0,0,0,0,0,0,3,0,0),(29633,0,0,0,0,0,0,0,3,0,0),(29634,0,0,0,0,0,0,0,3,0,0),(29635,0,0,0,0,0,0,0,3,0,0),(29636,0,0,0,0,0,0,0,3,0,0),(29637,0,0,0,0,0,0,0,3,0,0),(29834,0,0,0,0,0,0,65536,0,0,0),(29838,0,0,0,0,0,0,65536,0,0,0),(29977,0,3,12582935,64,0,0,0,0,0,0),(30003,0,0,0,0,0,0,2048,0,0,0),(30160,0,0,0,0,0,0,2,0,0,0),(30295,0,5,897,8519872,0,0,0,0,0,0),(30293,0,5,897,8519872,0,0,0,0,0,0),(30299,126,0,0,0,0,0,0,0,0,0),(30823,0,0,0,0,0,0,0,10,0,0),(30881,0,0,0,0,0,0,0,0,0,30),(30883,0,0,0,0,0,0,0,0,0,30),(30884,0,0,0,0,0,0,0,0,0,30),(30937,32,0,0,0,0,0,0,0,0,0),(31124,0,8,16777222,0,0,0,0,0,0,0),(31126,0,8,16777222,0,0,0,0,0,0,0),(31394,32,0,0,0,0,0,0,0,0,0),(31569,0,3,65536,0,0,0,0,0,0,0),(31570,0,3,65536,0,0,0,0,0,0,0),(31571,0,3,0,34,0,16384,0,0,0,0),(31572,0,3,0,34,0,16384,0,0,0,0),(31794,0,0,0,0,0,0,65536,0,0,0),(31878,0,10,8388608,0,0,0,262144,0,0,0),(31904,0,0,0,0,0,0,64,0,0,0),(32385,0,5,1,262144,0,0,0,0,0,0),(32387,0,5,1,262144,0,0,0,0,0,0),(32392,0,5,1,262144,0,0,0,0,0,0),(32409,0,0,0,0,0,0,0,0,0,0),(32587,0,0,0,0,0,0,64,0,0,0),(32642,0,0,0,0,0,0,64,0,0,0),(32734,0,0,0,0,0,0,0,0,0,3),(32748,0,8,0,1,0,320,0,0,0,0),(32776,0,0,0,0,0,0,64,0,0,0),(32777,0,0,0,0,0,0,64,0,0,0),(32837,0,0,0,0,0,0,65536,0,0,45),(32844,0,0,0,0,0,0,0,2,0,0),(32885,0,0,0,0,0,0,2,0,0,0),(33076,0,0,0,0,0,664232,0,0,0,0),(33089,0,0,0,0,0,0,64,0,0,0),(33127,0,0,0,0,0,0,0,7,0,0),(33142,0,0,0,0,0,0,2,0,0,0),(33145,0,0,0,0,0,0,2,0,0,0),(33150,0,0,0,0,0,0,2,0,0,0),(33151,0,0,0,0,0,0,2,0,0,0),(33154,0,0,0,0,0,0,2,0,0,0),(33191,0,0,0,0,0,1048576,2,0,100,0),(33297,0,0,0,0,0,0,0,0,0,45),(33299,0,0,0,0,0,0,65536,0,0,0),(33510,0,0,0,0,0,0,0,5,0,0),(33648,0,0,0,0,0,0,2,0,0,0),(33719,0,0,0,0,0,0,2048,0,0,0),(33746,0,0,0,0,0,0,0,0,0,10),(33757,0,0,0,0,0,0,0,0,100,3),(33759,0,0,0,0,0,0,0,0,0,10),(33882,0,0,0,0,0,1048576,0,0,100,12),(33881,0,0,0,0,0,1048576,0,0,50,12),(33953,0,0,0,0,0,278528,0,0,0,45),(34080,0,0,0,0,0,0,8,0,0,0),(34138,0,11,128,0,0,0,0,0,0,0),(34139,0,10,1073741824,0,0,0,0,0,0,0),(34258,0,10,8388608,0,0,0,262144,0,0,0),(34262,0,10,8388608,0,0,0,262144,0,0,0),(34320,0,0,0,0,0,0,2,0,0,45),(34355,0,0,0,0,0,0,0,0,0,3),(34497,0,0,0,0,0,0,2,0,0,0),(34498,0,0,0,0,0,0,2,0,0,0),(34499,0,0,0,0,0,0,2,0,0,0),(34584,0,0,0,0,0,0,0,0,0,30),(34586,0,0,0,0,0,0,0,1.5,0,0),(34598,0,0,0,0,0,0,0,0,0,45),(34749,0,0,0,0,0,0,8,0,0,0),(34774,0,0,0,0,0,0,0,1.5,0,20),(34783,0,0,0,0,0,0,2048,0,0,0),(34827,0,0,0,0,0,0,0,0,0,3),(34914,0,6,8192,0,0,0,0,0,0,0),(34935,0,0,0,0,0,0,0,0,0,8),(34938,0,0,0,0,0,0,0,0,0,8),(34939,0,0,0,0,0,0,0,0,0,8),(34950,0,0,0,0,0,0,2,0,0,0),(34954,0,0,0,0,0,0,2,0,0,0),(35077,0,0,0,0,0,0,0,0,0,60),(35080,0,0,0,0,0,0,0,1,0,60),(35083,0,0,0,0,0,0,0,0,0,60),(35086,0,0,0,0,0,0,0,0,0,60),(35121,0,0,0,0,0,0,2,0,0,0),(35541,0,0,0,0,0,8388608,0,0,0,0),(35550,0,0,0,0,0,8388608,0,0,0,0),(35551,0,0,0,0,0,8388608,0,0,0,0),(36032,0,3,4096,32768,0,0,0,0,0,0),(36096,0,0,0,0,0,0,2048,0,0,0),(36111,0,0,0,0,0,0,0,0,0,0),(36541,4,0,0,0,0,0,0,0,0,0),(37165,0,8,2098176,0,0,0,0,0,0,0),(37168,0,8,4063232,9,0,0,0,0,0,0),(37170,0,0,0,0,0,0,0,1,0,0),(37173,0,8,750519704,262,0,0,0,0,0,30),(37189,0,10,3221225472,0,0,0,2,0,0,60),(37193,0,0,0,0,0,0,64,0,0,0),(37195,0,10,8388608,0,0,0,262144,0,0,0),(37197,0,0,0,0,0,0,65536,0,0,45),(37213,0,0,0,0,0,0,2,0,0,0),(37214,0,0,0,0,0,0,65536,0,0,0),(37227,0,11,448,0,0,0,2,0,0,60),(37237,0,11,1,0,0,0,2,0,0,0),(37247,8,0,0,0,0,0,65536,0,0,45),(37377,32,0,0,0,0,0,65536,0,0,0),(37379,32,5,0,0,0,0,0,0,0,0),(37384,0,5,1,0,0,0,0,0,0,0),(37443,0,0,0,0,0,0,2,0,0,0),(37514,0,0,0,0,0,0,32,0,0,0),(37516,0,4,1024,0,0,0,0,0,0,0),(37519,0,0,0,0,0,0,48,0,0,0),(37523,0,0,0,0,0,0,64,0,0,0),(37528,0,4,4,0,0,0,0,0,0,0),(37536,0,4,65536,0,0,0,0,0,0,0),(37568,0,6,2048,0,0,0,0,0,0,0),(37594,0,6,4096,0,0,0,0,0,0,0),(37600,0,0,0,0,0,0,65536,0,0,0),(37601,0,0,0,0,0,0,65536,0,0,0),(37603,0,6,32768,0,0,0,0,0,0,0),(37655,0,0,0,0,0,0,0,0,0,60),(37657,0,0,0,0,0,0,2,0,0,3),(38026,1,0,0,0,0,0,256,0,0,0),(38031,0,0,0,0,0,0,64,0,0,0),(38290,0,0,0,0,0,0,0,1.6,0,0),(38299,0,0,0,0,0,0,0,0,0,15),(38326,0,0,0,0,0,0,2,0,0,0),(38327,0,0,0,0,0,0,2,0,0,0),(38334,0,0,0,0,0,0,0,0,0,60),(38347,0,0,0,0,0,0,2,0,0,45),(38350,0,0,0,0,0,0,2,0,0,0),(38394,0,5,6,0,0,0,0,0,0,0),(38857,0,0,0,0,0,0,65536,0,0,0),(39027,0,0,0,0,0,0,0,0,0,3),(39372,48,0,0,0,0,0,0,0,0,0),(39437,4,5,4964,192,0,0,65536,0,0,0),(39442,0,0,0,0,0,0,1,0,0,0),(39443,0,0,0,0,0,0,2,0,0,0),(39530,0,0,0,0,0,0,65536,0,0,0),(39958,0,0,0,0,0,0,0,0.7,0,40),(40407,0,0,0,0,0,0,0,6,0,0),(40438,0,6,32832,0,0,0,0,0,0,0),(40442,0,7,20,1088,0,0,0,0,0,0),(40444,0,0,0,0,0,0,64,0,0,0),(40458,0,4,33554432,1537,0,0,0,0,0,0),(40463,0,11,129,16,0,0,0,0,0,0),(40470,0,10,3229614080,0,0,0,262144,0,0,0),(40475,0,0,0,0,0,0,0,3,0,0),(40478,0,5,2,0,0,0,0,0,0,0),(40482,0,0,0,0,0,0,2,0,0,0),(40485,0,9,0,1,0,0,0,0,0,0),(40899,0,0,0,0,0,0,0,0,0,3),(41034,0,0,0,0,0,0,1024,0,0,0),(41260,0,0,0,0,0,0,0,0,0,10),(41262,0,0,0,0,0,0,0,0,0,10),(41381,0,0,0,0,0,0,256,0,0,0),(41393,0,0,0,0,0,0,32,0,0,0),(41434,0,0,0,0,0,0,0,2,0,45),(41469,0,0,0,0,0,0,0,7,0,0),(41635,0,0,0,0,0,664232,0,0,0,0),(41989,0,0,0,0,0,0,0,0.5,0,0),(42083,0,0,0,0,0,0,2,0,0,45),(42135,0,0,0,0,0,0,0,0,0,90),(42136,0,0,0,0,0,0,0,0,0,90),(42368,0,10,1073741824,0,0,0,0,0,0,0),(42370,0,11,128,0,0,0,0,0,0,0),(42770,0,0,0,0,0,0,65536,0,0,0),(43338,0,0,0,0,0,0,2,0,0,0),(43443,0,0,0,0,0,0,2048,0,0,0),(43726,0,10,1073741824,0,0,0,0,0,0,0),(43728,0,11,128,0,0,0,0,0,0,0),(43737,0,7,0,1088,0,0,0,0,0,10),(43739,0,7,2,0,0,0,0,0,0,0),(43741,0,10,2147483648,0,0,0,0,0,0,0),(43745,0,10,0,512,0,0,0,0,0,0),(43748,0,11,2416967680,0,0,0,0,0,0,0),(43750,0,11,1,0,0,0,0,0,0,0),(43819,0,0,0,0,0,0,65536,0,0,0),(44448,0,3,4194323,4096,4120,0,2,0,0,0),(44446,0,3,4194323,4096,4120,0,2,0,0,0),(44445,0,3,4194323,4096,4120,0,2,0,33,0),(44545,0,3,1049120,4096,0,65536,0,0,14,0),(44543,0,3,1049120,4096,0,65536,0,0,7,0),(44546,0,3,1049120,4096,0,0,0,0,0,0),(44548,0,3,1049120,4096,0,0,0,0,0,0),(44549,0,3,1049120,4096,0,0,0,0,0,0),(44561,0,3,32,0,0,0,0,0,0,0),(44835,0,7,0,128,0,16,0,0,0,0),(45054,0,0,0,0,0,0,0,0,0,15),(45057,0,0,0,0,0,0,0,0,0,30),(45234,0,0,0,0,0,0,2,0,0,0),(45243,0,0,0,0,0,0,2,0,0,0),(45354,0,0,0,0,0,0,0,0,0,45),(45469,0,15,16,0,0,16,0,0,0,0),(45481,0,0,0,0,0,0,0,0,0,45),(45482,0,0,0,0,0,0,0,0,0,45),(45483,0,0,0,0,0,0,0,0,0,45),(45484,0,0,0,0,0,16384,0,0,0,45),(46025,32,6,0,0,0,0,0,0,0,0),(46092,0,10,1073741824,0,0,0,0,0,0,0),(46098,0,11,128,0,0,0,0,0,0,0),(46569,0,0,0,0,0,0,0,0,0,45),(46662,0,0,0,0,0,0,0,0,0,20),(46832,0,7,1,0,0,0,65536,0,0,0),(46867,0,0,33554432,0,0,0,2,0,0,0),(46910,0,0,0,0,0,0,1,5.5,0,0),(46915,0,4,0,1024,0,0,0,0,0,0),(46914,0,4,0,1024,0,0,0,0,0,0),(46913,0,4,0,1024,0,0,0,0,0,0),(46916,0,4,0,1024,0,0,2,0,0,0),(46951,0,4,1024,64,0,0,0,0,0,0),(46952,0,0,1024,64,0,0,0,0,0,0),(46953,0,0,1024,64,0,0,0,0,0,0),(47195,0,5,2,0,0,0,0,0,0,30),(47196,0,5,2,0,0,0,0,0,0,30),(47197,0,5,2,0,0,0,0,0,0,30),(47203,0,5,16393,262144,0,0,0,0,0,0),(47202,0,5,16393,262144,0,0,0,0,0,0),(47201,0,5,16393,262144,0,0,0,0,0,0),(47245,0,5,4,0,0,327680,0,0,6,0),(47246,0,5,4,0,0,327680,0,0,6,0),(47247,0,5,4,0,0,327680,0,0,6,0),(47258,0,5,0,8388608,0,0,65536,0,0,0),(47259,0,5,0,8388608,0,0,65536,0,0,0),(47260,0,5,0,8388608,0,0,65536,0,0,0),(47263,32,5,0,0,0,0,2,0,0,20),(47264,32,5,0,0,0,0,2,0,0,20),(47265,32,5,0,0,0,0,2,0,0,20),(47509,0,0,0,0,0,0,2,0,0,0),(47511,0,0,0,0,0,0,2,0,0,0),(47515,0,0,0,0,0,0,2,0,0,0),(47516,0,6,6144,65536,0,0,0,0,0,0),(47517,0,6,6144,65536,0,0,0,0,0,0),(47569,0,6,16384,0,0,16384,0,0,0,0),(47570,0,6,16384,0,0,16384,0,0,0,0),(47580,0,6,0,0,64,0,65536,0,0,0),(47581,0,6,0,0,64,0,65536,0,0,0),(48110,0,0,0,0,0,664232,0,0,0,0),(48111,0,0,0,0,0,664232,0,0,0,0),(16954,0,7,233472,1024,262144,87376,2,0,0,0),(16952,0,7,233472,1024,262144,87376,2,0,0,0),(48495,0,7,524288,0,2048,1024,0,0,100,0),(48494,0,7,524288,0,2048,1024,0,0,100,0),(48492,0,7,524288,0,2048,1024,0,0,100,0),(48496,0,7,96,33554434,0,0,2,0,0,0),(48499,0,7,96,33554434,0,0,2,0,0,0),(48500,0,7,96,33554434,0,0,2,0,0,0),(48506,0,7,5,0,0,0,0,0,0,0),(48525,0,7,5,0,0,0,2,0,0,30),(48833,0,7,0,1088,0,0,0,0,0,0),(48835,0,10,8388608,0,0,0,262144,0,0,0),(48837,0,11,2416967680,0,0,0,0,0,0,0),(49018,0,15,0,0,0,0,0,0,5,0),(49222,0,0,0,0,0,139944,0,0,0,3),(49529,0,15,0,0,0,0,0,0,10,0),(49530,0,15,0,0,0,0,0,0,15,0),(49622,0,0,0,0,0,0,0,0,0,60),(50781,0,0,0,0,0,0,2,0,0,6),(51123,0,0,0,0,0,0,2,0,0,0),(51127,0,0,0,0,0,0,2,0,0,0),(51128,0,0,0,0,0,0,2,0,0,0),(51346,0,0,0,0,0,0,0,0,0,10),(51349,0,0,0,0,0,0,0,0,0,10),(51352,0,0,0,0,0,0,0,0,0,10),(51359,0,0,0,0,0,0,0,0,0,10),(51414,0,0,0,0,0,0,0,0,0,45),(51474,0,0,0,0,0,0,65536,0,0,0),(51478,0,0,0,0,0,0,65536,0,0,0),(51479,0,0,0,0,0,0,65536,0,0,0),(51483,1,11,536870912,0,0,16384,1,0,0,0),(51485,1,11,536870912,0,0,16384,1,0,0,0),(51522,0,11,0,16777216,0,0,0,0,0,0),(51523,0,11,0,1,0,65536,0,0,50,0),(51524,0,11,0,1,0,65536,0,0,50,0),(51528,0,0,0,0,0,0,0,2.5,0,0),(51529,0,0,0,0,0,0,0,5,0,0),(51530,0,0,0,0,0,0,0,7.5,0,0),(51556,0,11,192,0,16,0,2,0,0,0),(51557,0,11,192,0,16,0,2,0,0,0),(51558,0,11,192,0,16,0,2,0,0,0),(51562,0,11,256,0,16,0,0,0,0,0),(51563,0,11,256,0,16,0,0,0,0,0),(51564,0,11,256,0,16,0,0,0,0,0),(51625,0,8,268476416,0,0,0,0,0,0,0),(51626,0,8,268476416,0,0,0,0,0,0,0),(51627,0,0,0,0,0,0,112,0,0,0),(51628,0,0,0,0,0,0,112,0,0,0),(51629,0,0,0,0,0,0,112,0,0,0),(51664,0,8,131072,8,0,0,0,0,0,0),(51665,0,8,131072,8,0,0,0,0,0,0),(51667,0,8,131072,8,0,0,0,0,0,0),(51672,0,0,0,0,0,0,16,0,0,1),(51674,0,0,0,0,0,0,16,0,0,1),(51679,0,8,1,1,0,0,0,0,0,0),(51692,0,8,516,0,0,0,0,0,0,0),(51696,0,8,516,0,0,0,0,0,0,0),(51698,0,0,0,0,0,0,2,0,0,1),(51700,0,0,0,0,0,0,2,0,0,1),(51701,0,0,0,0,0,0,2,0,0,1),(51915,0,0,0,0,0,16777216,0,0,100,600),(52007,0,0,0,0,0,0,0,0,20,0),(52020,0,7,32768,1048576,0,0,0,0,0,0),(52127,0,0,0,0,0,0,0,0,0,3),(52420,0,0,0,0,0,0,0,0,0,30),(52423,0,0,0,0,0,0,32,0,0,0),(52795,0,6,1,0,0,0,0,0,0,0),(52797,0,6,1,0,0,0,0,0,0,0),(52798,0,6,1,0,0,0,0,0,0,0),(52898,0,0,0,0,0,0,2,0,0,0),(66192,0,15,4194320,537001988,0,16,0,0,100,0),(66191,0,15,4194320,537001988,0,16,0,0,100,0),(53216,0,9,1,0,0,0,0,0,0,0),(53221,0,9,0,1,0,0,0,0,0,0),(53222,0,9,0,1,0,0,0,0,0,0),(53224,0,9,0,1,0,0,0,0,0,0),(53228,0,9,32,16777216,0,0,0,0,0,0),(53232,0,9,32,16777216,0,0,0,0,0,0),(53256,0,9,2048,8388609,0,0,2,0,0,0),(53259,0,9,2048,8388609,0,0,2,0,0,0),(53260,0,9,2048,8388609,0,0,2,0,0,0),(53375,0,10,0,8192,0,1024,0,0,0,6),(53376,0,10,0,8192,0,1024,0,0,0,6),(53397,0,0,0,0,0,0,2,0,0,0),(53486,0,10,8388608,163840,0,0,262146,0,0,0),(53488,0,10,8388608,163840,0,0,262146,0,0,0),(53569,0,10,1075838976,65536,0,0,2,0,0,0),(53576,0,10,1075838976,65536,0,0,2,0,0,0),(67353,0,7,32768,1049856,0,0,0,0,0,0),(53671,0,10,8388608,0,0,0,262144,0,0,0),(53672,0,10,2097152,65536,0,0,2,0,0,0),(53673,0,10,8388608,0,0,0,262144,0,0,0),(53709,2,10,16384,0,0,0,0,0,0,0),(53710,2,10,16384,0,0,0,0,0,0,0),(54149,0,10,2097152,65536,0,0,2,0,0,0),(54151,0,10,8388608,0,0,0,262144,0,0,0),(54278,0,0,0,0,0,0,2,0,0,0),(54695,0,0,0,0,0,0,0,0,0,45),(54707,0,0,0,0,0,0,0,0,0,60),(54738,0,0,0,0,0,0,2,0,0,45),(54808,0,0,0,0,0,0,0,0,0,60),(54838,0,0,0,0,0,0,0,0,0,45),(54841,0,0,0,0,0,0,2,0,0,3),(54936,0,10,1073741824,0,0,0,0,0,0,0),(54939,0,10,32768,0,0,0,0,0,0,0),(55198,0,11,448,0,0,16384,2,0,0,0),(55380,0,0,0,0,0,0,0,0,0,45),(55381,0,0,0,0,0,0,65536,0,0,15),(55440,0,11,64,0,0,0,0,0,0,0),(55610,0,15,0,67108864,0,4096,0,0,0,0),(55640,0,0,0,0,0,0,0,0,0,45),(55677,0,6,0,1,0,0,0,0,0,0),(55680,0,6,512,0,0,0,0,0,0,0),(55747,0,0,0,0,0,0,0,0,0,45),(55768,0,0,0,0,0,0,0,0,0,45),(55776,0,0,0,0,0,0,0,0,0,45),(56218,0,5,2,0,0,0,0,0,0,0),(56249,0,5,0,0,1024,0,0,0,0,0),(56355,0,0,0,0,0,0,64,0,0,0),(56364,0,3,0,16777216,0,0,0,0,0,0),(56372,0,3,0,128,0,16384,0,0,0,0),(56374,0,3,0,16384,0,16384,0,0,0,0),(56451,0,0,0,0,0,0,0,0,0,3),(56611,0,0,33554432,0,0,0,2,0,0,0),(56612,0,0,0,0,0,0,2,0,0,0),(56816,0,0,0,0,0,0,48,0,0,0),(56821,0,8,2,0,0,0,2,0,0,0),(56835,0,15,4194304,65536,0,0,0,0,0,0),(57345,0,0,0,0,0,0,0,0,0,45),(57352,0,0,0,0,0,332116,0,0,0,45),(57878,0,0,0,0,0,0,16,0,0,0),(57880,0,0,0,0,0,0,16,0,0,0),(57907,0,7,2,0,0,0,0,0,0,0),(58357,0,4,64,0,0,0,2,0,0,0),(58364,0,4,1024,0,0,0,0,0,0,0),(58372,0,4,2,0,0,0,0,0,0,0),(58386,0,0,0,0,0,0,32,0,0,0),(58442,0,0,0,0,0,0,0,0,0,15),(58444,0,0,0,0,0,0,0,0,0,5),(58626,0,15,33554432,0,0,0,0,0,0,0),(64955,0,10,0,64,0,0,0,0,0,0),(58647,0,15,0,4,0,0,0,0,0,0),(58677,0,15,8192,0,0,0,0,0,0,0),(58901,0,0,0,0,0,0,2,0,0,45),(59176,0,0,0,0,0,0,2,0,0,0),(59327,0,15,134217728,0,0,0,0,0,0,0),(59345,0,0,0,0,0,0,0,0,0,45),(59630,0,0,0,0,0,0,0,0,0,45),(59725,0,0,0,0,0,0,2048,0,0,0),(60061,0,0,0,0,0,0,0,0,0,45),(60063,0,0,0,0,0,0,0,0,0,45),(60132,0,15,0,134348800,0,0,0,0,0,0),(60170,0,5,6,0,0,0,0,0,0,0),(60172,0,5,262144,0,0,0,65536,0,0,0),(60176,0,4,32,16,0,0,0,0,0,0),(60221,0,0,0,0,0,0,0,0,0,45),(60301,0,0,0,0,0,0,0,0,0,45),(60306,0,0,0,0,0,0,0,0,0,45),(60317,0,0,0,0,0,0,0,0,0,45),(60436,0,0,0,0,0,0,0,0,0,45),(60442,0,0,0,0,0,0,0,0,0,45),(60473,0,0,0,0,0,0,0,0,0,45),(60482,0,0,0,0,0,0,0,0,0,45),(60487,0,0,0,0,0,0,0,0,0,15),(60490,0,0,0,0,0,0,0,0,0,45),(60493,0,0,0,0,0,0,0,0,0,45),(60519,0,0,0,0,0,0,0,0,0,45),(60529,0,0,0,0,0,0,0,0,0,45),(60537,0,0,0,0,0,0,2,0,0,45),(60564,0,11,2416967680,0,0,0,0,0,0,0),(60571,0,11,2416967680,0,0,0,0,0,0,0),(60572,0,11,2416967680,0,0,0,0,0,0,0),(60573,0,11,2416967680,0,0,0,0,0,0,0),(60574,0,11,2416967680,0,0,0,0,0,0,0),(60575,0,11,2416967680,0,0,0,0,0,0,0),(60617,0,0,0,0,0,0,32,0,0,0),(60710,0,7,2,0,0,0,0,0,0,0),(60717,0,7,2,0,0,0,0,0,100,0),(60719,0,7,2,0,0,0,0,0,0,0),(60722,0,7,2,0,0,0,0,0,0,0),(60724,0,7,2,0,0,0,0,0,0,0),(60726,0,7,2,0,0,0,0,0,0,0),(60770,0,11,1,0,0,0,0,0,0,0),(60818,0,10,0,512,0,0,0,0,0,0),(60826,0,15,20971520,0,0,0,0,0,0,0),(61188,0,5,4,0,0,0,0,0,0,0),(61324,0,10,0,131072,0,0,0,0,0,0),(61345,72,7,103,58720258,0,0,2,0,0,60),(61346,72,7,103,58720258,0,0,2,0,0,60),(61356,0,0,0,0,0,0,2,0,0,90),(61618,0,0,0,0,0,0,0,0,0,45),(62147,0,15,2,0,0,0,0,0,0,0),(62459,0,15,4,0,0,0,0,0,0,0),(63108,0,5,2,0,0,0,0,0,0,0),(63158,0,5,1,192,0,0,0,0,0,0),(63156,0,5,1,192,0,0,0,0,0,0),(64343,0,3,2,0,0,0,0,0,0,0),(64976,0,4,1,0,0,0,0,0,0,0),(64914,0,8,65536,0,0,0,0,0,0,0),(64938,0,4,2097216,0,0,0,2,0,0,0),(64952,0,7,0,1088,0,0,0,0,0,0),(64964,0,15,0,536870912,0,0,0,0,0,0),(65002,0,0,0,0,0,0,0,0,0,45),(65005,0,0,0,0,0,0,0,0,0,45),(64999,0,0,0,0,0,0,0,5,0,0),(65007,0,0,0,0,0,0,0,5,0,0),(65013,0,0,0,0,0,0,2,0,0,45),(65020,0,0,0,0,0,0,0,0,0,45),(65025,0,0,0,0,0,0,0,0,0,45),(46949,0,4,0,65536,0,0,0,0,0,0),(46945,0,4,0,65536,0,0,0,0,0,0),(64415,0,0,0,0,0,0,0,0,0,45),(60066,0,0,0,0,0,0,2,0,0,45),(62115,0,0,0,0,0,0,0,0,0,45),(62114,0,0,0,0,0,0,0,0,0,45),(62600,0,0,0,0,0,0,2,0,0,0),(63310,0,5,0,65536,0,65536,0,0,0,0),(31801,1,0,0,0,0,0,0,0,0,0),(63335,0,15,0,2,0,0,0,0,0,0),(63730,0,6,2048,4,0,0,0,0,0,0),(63733,0,6,2048,4,0,0,0,0,0,0),(84583,0,4,33554432,0,0,0,3,0,0,0),(64571,0,0,0,0,0,0,0,0,0,10),(64440,0,0,0,0,0,0,32,0,0,20),(64714,0,0,0,0,0,0,0,0,0,45),(64738,0,0,0,0,0,0,0,0,0,45),(64742,0,0,0,0,0,0,0,0,0,45),(64752,0,7,8388608,268435712,0,0,0,0,0,0),(64786,0,0,0,0,0,0,0,0,0,15),(64792,0,0,0,0,0,0,2,0,0,45),(64824,0,7,2097152,0,0,0,0,0,0,0),(64928,0,11,1,0,0,0,2,0,0,0),(64860,0,9,0,1,0,0,0,0,0,0),(64867,0,3,536870945,4096,0,0,0,0,0,0),(64882,0,10,0,1048576,0,0,0,0,0,0),(64890,0,10,0,65536,0,0,2,0,0,0),(64908,0,6,0,0,64,0,0,0,0,0),(64912,0,6,1,0,0,0,0,0,0,0),(57470,0,6,1,0,0,0,0,0,0,0),(57472,0,6,1,0,0,0,0,0,0,0),(35100,0,9,4096,0,1,0,0,0,0,0),(35102,0,9,4096,0,1,0,0,0,0,0),(13165,0,0,0,0,0,64,0,0,0,0),(49188,0,15,0,131072,0,0,0,0,0,0),(56822,0,15,0,131072,0,0,0,0,0,0),(59057,0,15,0,131072,0,0,0,0,0,0),(55666,0,15,1,134217728,0,0,0,0,0,0),(55667,0,15,1,134217728,0,0,0,0,0,0),(58616,0,15,16777216,0,0,0,0,0,0,0),(16164,0,11,2416967875,4096,0,0,2,0,0,0),(49149,0,15,6,131074,0,0,0,0,0,0),(50115,0,15,6,131074,0,0,0,0,0,0),(56342,0,9,24,134217728,147456,0,0,0,0,22),(56343,0,9,24,134217728,147456,0,0,0,0,22),(48539,0,7,16,67108864,0,262144,0,0,0,0),(48544,0,7,16,67108864,0,262144,0,0,0,0),(53234,0,9,131072,1,1,0,2,0,0,0),(53237,0,9,131072,1,1,0,2,0,0,0),(53238,0,9,131072,1,1,0,2,0,0,0),(56636,0,4,32,0,0,0,0,0,0,6),(56637,0,4,32,0,0,0,0,0,0,6),(56638,0,4,32,0,0,0,0,0,0,6),(56375,0,3,16777216,0,0,65536,0,0,0,0),(54637,0,15,4194304,65536,0,0,0,0,0,0),(65661,0,15,4194320,537001988,0,16,0,0,100,0),(57989,0,0,0,0,0,1,0,0,0,0),(50034,0,15,16,131072,0,0,0,0,0,0),(63374,0,11,2147483648,0,0,65536,0,0,100,0),(63373,0,11,2147483648,0,0,65536,0,0,50,0),(54821,0,7,4096,0,0,16,0,0,0,0),(54815,0,7,32768,0,0,16,0,0,0,0),(54845,0,7,4,0,0,65536,0,0,0,0),(56800,0,8,8388612,0,0,16,2,0,0,0),(54832,0,7,0,4096,0,16384,0,0,0,0),(49027,0,0,0,0,0,0,0,0,3,20),(49542,0,0,0,0,0,0,0,0,6,20),(58387,0,4,16384,64,0,16,0,0,0,0),(58375,0,4,0,512,0,16,0,0,0,0),(49194,0,15,8192,0,0,0,0,0,0,0),(31656,4,3,134217728,0,0,0,0,0,0,0),(31657,4,3,134217728,0,0,0,0,0,0,0),(31658,4,3,134217728,0,0,0,0,0,0,0),(67356,8,7,16,0,0,0,0,0,0,0),(67771,1,0,0,0,0,8720724,3,0,35,45),(67702,1,0,0,0,0,8720724,3,0,35,45),(54925,2,10,0,512,0,0,0,0,0,0),(63320,0,5,2147745792,0,32768,1024,0,0,0,0),(71562,0,0,0,0,0,0,0,0,0,105),(57870,0,9,8388608,0,0,262144,0,0,0,0),(70807,0,11,0,0,16,0,0,0,100,0),(67667,0,0,0,0,0,16384,0,0,0,45),(67672,0,0,0,0,0,8388948,0,0,0,45),(67670,0,0,0,0,0,65536,0,0,0,45),(67653,0,0,0,0,0,4194344,0,0,0,45),(70761,0,10,0,2147500032,1,1024,0,0,0,0),(67758,0,0,0,0,0,0,2,0,0,2),(51459,0,0,0,0,0,4,0,0,0,0),(51462,0,0,0,0,0,4,0,0,0,0),(49219,0,0,0,0,0,4,0,0,0,0),(49627,0,0,0,0,0,4,0,0,0,0),(49628,0,0,0,0,0,4,0,0,0,0),(53178,0,9,0,268435456,0,65536,0,0,100,0),(53179,0,9,0,268435456,0,65536,0,0,100,0),(62764,0,9,0,268435456,0,65536,0,0,100,0),(62765,0,9,0,268435456,0,65536,0,0,100,0),(71642,0,0,0,0,0,0,0,0,0,45),(71611,0,0,0,0,0,0,0,0,0,45),(71640,0,0,0,0,0,0,0,0,0,30),(71634,0,0,0,0,0,0,0,0,0,30),(71645,0,0,0,0,0,0,0,0,0,45),(71602,0,0,0,0,0,0,0,0,0,45),(71606,0,0,0,0,0,0,0,0,0,100),(71637,0,0,0,0,0,0,0,0,0,100),(71540,0,0,0,0,0,0,0,0,0,45),(71402,0,0,0,0,0,0,0,0,0,45),(72417,0,0,0,0,0,0,0,0,0,60),(72413,0,0,0,0,0,0,0,0,0,60),(72419,0,0,0,0,0,0,0,0,0,60),(70748,0,3,0,2097152,0,1024,0,0,0,0),(70830,0,11,0,131072,0,0,0,0,0,0),(70727,0,9,0,0,0,64,0,0,0,0),(70730,0,9,16384,4096,0,262144,0,0,0,0),(70803,0,8,4063232,8,0,0,0,0,0,0),(70805,0,8,0,131072,0,16384,0,0,0,0),(70841,0,5,4,256,0,262144,0,0,0,0),(67361,0,7,2,0,0,262144,0,0,0,0),(67363,0,10,0,2147483648,0,0,0,0,0,10),(67365,0,10,0,2048,0,262144,0,0,0,6),(67379,0,10,0,262144,0,0,0,0,0,0),(67381,0,15,0,536870912,0,0,0,0,0,10),(67384,0,15,16,134348800,0,0,0,0,80,10),(67386,0,11,1,0,0,65536,0,0,0,6),(67389,0,11,256,0,0,16384,0,0,0,8),(67392,0,11,0,0,4,16,0,0,0,0),(71176,0,7,2097154,0,0,262144,0,0,0,0),(71178,0,7,16,0,0,262144,0,0,0,0),(71186,0,10,0,32768,0,0,0,0,0,0),(71191,0,10,0,65536,0,0,0,0,0,0),(71194,0,10,0,1048576,0,0,0,0,0,0),(71214,0,11,5120,16,0,16,0,0,0,6),(71217,0,11,0,0,16,16384,0,0,0,0),(71226,0,15,16,134348800,0,0,0,0,0,0),(71228,0,15,0,536870912,0,0,0,0,0,0),(70854,0,4,0,16,0,0,0,0,0,0),(71404,0,0,0,0,0,0,2,0,0,45),(70652,0,15,8,0,0,0,0,0,0,0),(70756,0,10,2097152,0,0,0,0,0,0,0),(70656,0,15,0,0,0,81920,0,0,0,0),(53386,48,0,0,0,0,0,0,0,0,0),(50421,0,0,0,0,0,0,0,0,0,0),(71564,0,0,0,0,0,0,2,0,0,0),(75474,0,0,0,0,0,0,0,0,0,45),(75465,0,0,0,0,0,0,0,0,0,45),(75457,0,0,0,0,0,0,0,0,0,45),(75455,0,0,0,0,0,0,0,0,0,45),(71545,0,0,0,0,0,0,0,0,50,0),(71406,0,0,0,0,0,0,0,0,50,0),(16176,0,11,448,0,16,0,2,0,0,0),(52437,1,4,536870912,0,0,16,0,0,0,0),(60503,1,4,4,0,0,16,0,0,0,0),(71903,0,0,0,0,0,0,0,0,20,0),(70672,0,0,0,0,0,0,0,0,0,0),(72455,0,0,0,0,0,0,0,0,0,0),(72832,0,0,0,0,0,0,0,0,0,0),(72833,0,0,0,0,0,0,0,0,0,0),(12575,0,3,0,0,0,65536,0,0,10,0),(12574,0,3,0,0,0,65536,0,0,6,0),(11213,0,3,0,0,0,65536,0,0,3,0),(88756,0,11,3,0,0,65536,0,0,30,0),(88764,0,11,3,0,0,65536,0,0,60,0),(81913,0,0,0,0,0,1048576,0,0,100,120),(81914,0,0,0,0,0,1048576,0,0,100,120),(13877,0,0,0,0,0,20,2147483648,0,100,0),(84587,0,4,33554432,0,0,0,3,0,0,0),(84588,0,4,33554432,0,0,0,3,0,0,0),(20502,0,4,536870912,0,0,16,0,0,50,0),(20503,0,4,536870912,0,0,16,0,0,100,0),(12329,0,4,4194304,0,0,0,0,0,0,0),(12950,0,4,4194304,0,0,0,0,0,0,0),(71892,0,0,0,0,0,0,0,1,0,0),(71880,0,0,0,0,0,0,0,1,0,0),(85113,0,5,0,8388608,0,87376,0,0,0,0),(85114,0,5,0,8388608,0,87376,0,0,0,0),(18120,0,5,0,128,0,0,0,0,100,0),(18119,0,5,0,128,0,0,0,0,100,0),(91023,0,8,1792,0,0,4112,0,0,0,0),(51632,0,8,1792,0,0,4112,0,0,0,0),(33603,0,7,2,0,0,0,0,0,0,0),(33604,0,7,2,0,0,0,0,0,0,0),(33605,0,7,2,0,0,0,0,0,0,0),(85510,0,0,2097152,0,0,81920,0,0,50,0),(31825,0,0,2097152,0,0,81920,0,0,50,0),(87192,0,0,8192,0,0,65536,2,0,0,0),(87195,0,0,8192,0,0,65536,2,0,0,0),(14910,0,6,0,0,65536,65536,0,0,0,0),(33371,0,6,0,0,65536,65536,0,0,0,0),(50685,0,4,64,0,0,16,2,0,0,0),(50686,0,4,64,0,0,16,2,0,0,0),(50687,0,4,64,0,0,16,2,0,0,0),(61216,0,4,8,0,0,4112,0,0,100,120),(61221,0,4,8,0,0,4112,0,0,100,120),(31834,0,10,2147483648,0,0,16384,0,0,0,0),(70817,0,11,0,4096,0,65536,0,0,0,0),(32216,0,4,0,256,0,18,0,0,0,0),(64568,0,0,0,0,0,0,0,0,0,3),(53695,0,10,8388608,0,8,0,0,0,0,0),(53696,0,10,8388608,0,8,0,0,0,0,0),(71585,0,0,0,0,0,0,0,0,0,45),(70664,0,7,16,0,0,0,0,0,0,0),(31829,0,0,0,0,0,0,0,0,100,8),(31828,0,0,0,0,0,0,0,0,50,8),(67712,0,0,0,0,0,0,2,0,100,2),(87172,0,10,8388608,0,0,272,262144,0,100,0),(87168,0,0,8388608,0,0,272,262144,0,50,0),(89901,0,0,8388608,0,0,272,262144,0,100,0),(83074,0,3,1049120,4096,0,65536,0,0,20,0),(86172,0,10,8388608,2228354,40960,66832,0,0,15,0),(85117,0,10,8388608,2228354,40960,66832,0,0,7,0),(76672,0,10,0,163840,8192,66832,0,0,100,0),(34487,0,9,0,1,0,256,0,0,60,0),(34486,0,9,0,1,0,256,0,0,40,0),(34485,0,9,0,1,0,1280,0,0,20,0),(35104,0,9,4096,0,0,4416,2,0,0,0),(35110,0,9,4096,0,0,4416,2,0,0,0),(83340,0,9,133120,2147483648,0,0,2,0,100,0),(83356,0,9,133120,2147483648,0,0,2,0,100,0),(56824,0,9,131072,0,0,320,2,0,100,0),(48532,0,7,0,0,0,0,0,0,0,3),(80552,0,7,0,0,0,0,0,0,0,3),(80553,0,7,0,0,0,0,0,0,0,3),(75171,0,0,0,0,0,0,0,0,0,60),(75177,0,0,0,0,0,0,0,0,0,60),(75174,0,0,0,0,0,0,0,0,0,60),(51525,0,11,0,16777232,131076,4112,0,0,0,0),(51526,0,11,0,16777232,131076,4112,0,0,0,0),(51527,0,11,0,16777232,131076,4112,0,0,0,0),(379,0,0,0,0,0,0,0,0,0,3),(77655,0,0,1073741824,0,0,65536,0,0,100,0),(77656,0,0,1073741824,0,0,65536,0,0,100,0),(77657,0,0,1073741824,0,0,65536,0,0,100,0),(88994,0,0,0,0,0,0,0,0,0,0),(88995,0,0,0,0,0,0,0,0,0,0),(82984,0,11,1,0,0,0,0,0,100,0),(82988,0,11,1,0,0,0,0,0,100,0),(71761,0,3,0,1048576,0,0,256,0,0,0),(86303,0,0,0,0,0,1048576,0,0,50,0),(86304,0,0,0,0,0,1048576,0,0,100,0),(94746,0,0,0,0,0,0,0,0,0,45),(85741,0,0,33554432,1536,0,0,0,3,0,0),(85742,0,0,33554432,1536,0,0,0,3,0,0),(95740,0,6,32768,0,1088,327680,0,0,10,0),(78228,0,0,0,0,0,1048576,2,0,100,0),(53556,0,10,8388608,0,0,0,0,0,0,0),(53557,0,10,8388608,0,0,0,0,0,0,0),(14523,2,6,1048704,0,0,65536,0,0,100,0),(81749,2,6,1048704,0,0,65536,0,0,100,0),(89488,3,6,7168,1073741824,0,16384,0,0,100,0),(89489,3,6,7168,1073741824,0,16384,0,0,100,0),(56414,0,0,16384,0,0,0,0,0,0,0),(74245,0,0,0,0,0,0,0,1,0,0),(58096,0,4,268435456,67108864,0,0,0,0,0,0),(88676,0,0,0,0,0,0,0,0,0,15),(85126,0,10,0,2684356608,0,0,0,0,0,0),(17007,0,0,0,0,0,0,2,0,0,6),(86181,64,3,536870912,0,0,65536,0,0,50,0),(86209,64,3,536870912,0,0,65536,0,0,100,0);
/*!40000 ALTER TABLE `spell_proc_event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spell_ranks`
--

DROP TABLE IF EXISTS `spell_ranks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spell_ranks` (
  `first_spell_id` int(10) unsigned NOT NULL DEFAULT '0',
  `spell_id` int(10) unsigned NOT NULL DEFAULT '0',
  `rank` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`first_spell_id`,`rank`),
  UNIQUE KEY `spell_id` (`spell_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='Spell Rank Data';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spell_ranks`
--

LOCK TABLES `spell_ranks` WRITE;
/*!40000 ALTER TABLE `spell_ranks` DISABLE KEYS */;
INSERT INTO `spell_ranks` VALUES (53137,53137,1),(53137,53138,2),(18119,18119,1),(18119,18120,2),(18427,18427,1),(18427,18428,2),(18427,18429,3),(8115,8115,1),(8115,8116,2),(8115,8117,3),(8115,12174,4),(8115,33077,5),(8115,43194,6),(8115,58450,7),(8115,58451,8),(2259,3101,2),(2259,3464,3),(2259,11611,4),(2259,28596,5),(2259,51304,6),(2259,80731,7),(1267,1267,1),(1267,8456,2),(1267,10171,3),(1267,10172,4),(1267,27397,5),(1267,33947,6),(51556,51556,1),(51556,51557,2),(51556,51558,3),(16177,16177,1),(16177,16236,2),(16176,16176,1),(16176,16235,2),(17485,17485,1),(17485,17486,2),(17485,17487,3),(33704,33704,1),(33704,33705,2),(33704,33706,3),(51468,51468,1),(51468,51472,2),(51468,51473,3),(11213,11213,1),(11213,12574,2),(11213,12575,3),(31579,31579,1),(31579,31582,2),(31579,31583,3),(44378,44378,1),(44378,44379,2),(11222,11222,1),(11222,12839,2),(11222,12840,3),(28574,28574,1),(28574,54658,2),(28574,54659,3),(15058,15058,1),(15058,15059,2),(15058,15060,3),(16734,16734,1),(16734,16735,2),(16734,16736,3),(16734,16737,4),(16734,16738,5),(18462,18462,1),(18462,18463,2),(18462,18464,3),(11232,11232,1),(11232,12500,2),(11232,12501,3),(11232,12502,4),(11232,12503,5),(31571,31571,1),(31571,31572,2),(11252,11252,1),(11252,12605,2),(11237,11237,1),(11237,12463,2),(11237,12464,3),(11237,16769,4),(11237,16770,5),(11210,11210,1),(11210,12592,2),(16757,16757,1),(16757,16758,2),(31674,31674,1),(31674,31675,2),(31674,31676,3),(8091,8091,1),(8091,8094,2),(8091,8095,3),(8091,12175,4),(8091,33079,5),(8091,43196,6),(8091,58452,7),(8091,58453,8),(61216,61216,1),(61216,61221,2),(51474,51474,1),(51474,51478,2),(51474,51479,3),(47258,47258,1),(47258,47259,2),(47258,47260,3),(34935,34935,1),(34935,34938,2),(34935,34939,3),(33592,33592,1),(33592,33596,2),(17788,17788,1),(17788,17789,2),(17788,17790,3),(19590,19590,1),(19590,19592,2),(19603,19603,1),(19603,19605,2),(19603,19606,3),(19603,19607,4),(19603,19608,5),(17254,17254,1),(17254,17262,2),(17254,17263,3),(17254,17264,4),(17254,17265,5),(17254,17266,6),(17254,17267,7),(17254,17268,8),(17254,27348,9),(2018,3100,2),(2018,3538,3),(2018,9785,4),(2018,29844,5),(2018,51300,6),(2018,76666,7),(51789,51789,1),(51789,64855,2),(51789,64856,3),(49182,49182,1),(49182,49500,2),(49182,49501,3),(31124,31124,1),(31124,31126,2),(48978,48978,1),(48978,49390,2),(48978,49391,3),(31641,31641,1),(31641,31642,2),(31828,31828,1),(31828,31829,2),(51554,51554,1),(51554,51555,2),(16487,16487,1),(16487,16489,2),(16487,16492,3),(30069,30069,1),(30069,30070,2),(29836,29836,1),(29836,29859,2),(16952,16952,1),(16952,16954,2),(53481,53481,1),(53481,53482,2),(49219,49219,1),(49219,49627,2),(49219,49628,3),(46913,46913,1),(46913,46914,2),(46913,46915,3),(53186,53186,1),(53186,53187,2),(49027,49027,1),(49027,49542,2),(64127,64127,1),(64127,64129,2),(63370,63370,1),(63370,63372,2),(12321,12321,1),(12321,12835,2),(52795,52795,1),(52795,52797,2),(52795,52798,3),(44546,44546,1),(44546,44548,2),(44546,44549,3),(16940,16940,1),(16940,16941,2),(54747,54747,1),(54747,54749,2),(44449,44449,1),(44449,44469,2),(44449,44470,3),(44449,44471,4),(44449,44472,5),(48979,48979,1),(48979,49483,2),(16038,16038,1),(16038,16160,2),(13975,13975,1),(13975,14062,2),(34482,34482,1),(34482,34483,2),(17778,17778,1),(17778,17779,2),(17778,17780,3),(113,113,1),(113,512,2),(12695,12695,1),(12695,12696,2),(31228,31228,1),(31228,31229,2),(31228,31230,3),(50040,50040,1),(50040,50041,2),(49149,49149,1),(49149,50115,2),(12484,12484,1),(12484,12485,2),(12484,12486,3),(44566,44566,1),(44566,44567,2),(44566,44568,3),(44566,44570,4),(44566,44571,5),(30214,30214,1),(30214,30222,2),(30214,30224,3),(5508,5508,1),(5508,5480,2),(13706,13706,1),(13706,13804,2),(13706,13805,3),(13706,13806,4),(13706,13807,5),(61682,61682,1),(61682,61683,2),(53256,53256,1),(53256,53259,2),(53256,53260,3),(55091,55091,1),(55091,55092,2),(17427,17427,1),(17427,17428,2),(17427,17429,3),(35542,35542,1),(35542,35545,2),(35542,35546,3),(35541,35541,1),(35541,35550,2),(35541,35551,3),(16035,16035,1),(16035,16105,2),(16035,16106,3),(35100,35100,1),(35100,35102,2),(587,587,1),(587,597,2),(587,990,3),(587,6129,4),(587,10144,5),(587,10145,6),(587,28612,7),(587,33717,8),(759,759,1),(759,3552,2),(759,10053,3),(759,10054,4),(759,27101,5),(759,42985,6),(42955,42955,1),(42955,42956,2),(5504,5504,1),(5504,5505,2),(5504,5506,3),(5504,6127,4),(5504,10138,5),(5504,10139,6),(5504,10140,7),(5504,37420,8),(5504,27090,9),(26573,26573,1),(26573,20116,2),(30060,30060,1),(30060,30061,2),(30060,30062,3),(30060,30063,4),(30060,30064,5),(16039,16039,1),(16039,16109,2),(2550,3102,2),(2550,3413,3),(2550,18260,4),(2550,33359,5),(2550,51296,6),(2550,88053,7),(52234,52234,1),(52234,53497,2),(11115,11115,1),(11115,11367,2),(12320,12320,1),(12320,12852,2),(31866,31866,1),(31866,31867,2),(31866,31868,3),(61680,61680,1),(61680,61681,2),(61680,52858,3),(51664,51664,1),(51664,51665,2),(51664,51667,3),(1266,1266,1),(1266,8452,2),(1266,8453,3),(1266,10175,4),(1266,10176,5),(15259,15259,1),(15259,15307,2),(15259,15308,3),(31380,31380,1),(31380,31382,2),(31380,31383,3),(30902,30902,1),(30902,30903,2),(30902,30904,3),(30902,30905,4),(30902,30906,5),(51625,51625,1),(51625,51626,2),(47198,47198,1),(47198,47199,2),(47198,47200,3),(63156,63156,1),(63156,63158,2),(12162,12162,1),(12162,12850,2),(12162,12868,3),(12834,12834,1),(12834,12849,2),(12834,12867,3),(29559,29559,1),(29559,29588,2),(29559,29589,3),(13713,13713,1),(13713,13853,2),(13713,13854,3),(30143,30143,1),(30143,30144,2),(18705,18705,1),(18705,18706,2),(18705,18707,3),(18697,18697,1),(18697,18698,2),(18697,18699,3),(35691,35691,1),(35691,35692,2),(35691,35693,3),(18126,18126,1),(18126,18127,2),(30319,30319,1),(30319,30320,2),(30319,30321,3),(30242,30242,1),(30242,30245,2),(30242,30246,3),(30242,30247,4),(30242,30248,5),(24424,24424,1),(24424,24580,2),(24424,24581,3),(24424,24582,4),(24424,27349,5),(55666,55666,1),(55666,55667,2),(17917,17917,1),(17917,17918,2),(30251,30251,1),(30251,30256,2),(14082,14082,1),(14082,14083,2),(34478,34478,1),(34478,34479,2),(34478,34481,3),(23146,23146,1),(23146,23149,2),(23146,23150,3),(47509,47509,1),(47509,47511,2),(47509,47515,3),(18530,18530,1),(18530,18531,2),(18530,18533,3),(47562,47562,1),(47562,47564,2),(47562,47565,3),(47562,47566,4),(47562,47567,5),(63646,63646,1),(63646,63647,2),(63646,63648,3),(33597,33597,1),(33597,33599,2),(51523,51523,1),(51523,51524,2),(51099,51099,1),(51099,51160,2),(48516,48516,1),(48516,48521,2),(48516,48525,3),(19416,19416,1),(19416,19417,2),(19416,19418,3),(51466,51466,1),(51466,51470,2),(30672,30672,1),(30672,30673,2),(30672,30674,3),(28999,28999,1),(28999,29000,2),(28996,28996,1),(28996,28997,2),(28996,28998,3),(16266,16266,1),(16266,29079,2),(13981,13981,1),(13981,14066,2),(17954,17954,1),(17954,17955,2),(32381,32381,1),(32381,32382,2),(32381,32383,3),(31656,31656,1),(31656,31657,2),(31656,31658,3),(31682,31682,1),(31682,31683,2),(33158,33158,1),(33158,33159,2),(33158,33160,3),(47220,47220,1),(47220,47221,2),(63534,63534,1),(63534,63542,2),(33879,33879,1),(33879,33880,2),(7411,7412,2),(7411,7413,3),(7411,13920,4),(7411,28029,5),(7411,51313,6),(7411,74258,7),(49137,49137,1),(49137,49657,2),(4036,4037,2),(4036,4038,3),(4036,12656,4),(4036,30350,5),(4036,51306,6),(4036,82774,7),(53556,53556,1),(53556,53557,2),(12317,12317,1),(12317,13045,2),(12317,13046,3),(19184,19184,1),(19184,19387,2),(31211,31211,1),(31211,31212,2),(31211,31213,3),(49036,49036,1),(49036,49562,2),(47195,47195,1),(47195,47196,2),(47195,47197,3),(47201,47201,1),(47201,47202,2),(47201,47203,3),(13424,13424,1),(13424,13752,2),(60096,60096,1),(60096,60097,2),(53511,53511,1),(53511,53512,2),(17783,17783,1),(17783,17784,2),(17783,17785,3),(47230,47230,1),(47230,47231,2),(18731,18731,1),(18731,18743,2),(18731,18744,3),(16858,16858,1),(16858,16859,2),(58414,58414,1),(58414,58415,2),(44543,44543,1),(44543,44545,2),(47266,47266,1),(47266,47267,2),(47266,47268,3),(47266,47269,4),(47266,47270,5),(2141,2141,1),(2141,2142,2),(2141,2143,3),(2141,8414,4),(2141,8415,5),(2141,10198,6),(2141,10200,7),(2141,27378,8),(2141,27379,9),(11124,11124,1),(11124,12378,2),(3011,3011,1),(3011,6979,2),(3011,6980,3),(1035,1035,1),(1035,8459,2),(1035,8460,3),(1035,10224,4),(1035,10226,5),(1035,27395,6),(3273,3274,2),(3273,7924,3),(3273,10846,4),(3273,27028,5),(3273,45542,6),(3273,74559,7),(31208,31208,1),(31208,31209,2),(16257,16257,1),(16257,16277,2),(16257,16278,3),(16256,16256,1),(16256,16281,2),(16256,16282,3),(35029,35029,1),(35029,35030,2),(30864,30864,1),(30864,30865,2),(30864,30866,3),(19621,19621,1),(19621,19622,2),(19621,19623,3),(11160,11160,1),(11160,12518,2),(11160,12519,3),(6144,6144,1),(6144,8463,2),(6144,8464,3),(6144,10178,4),(6144,27396,5),(6144,32797,6),(11189,11189,1),(11189,28332,2),(31667,31667,1),(31667,31668,2),(31667,31669,3),(63373,63373,1),(63373,63374,2),(17056,17056,1),(17056,17058,2),(17056,17059,3),(12311,12311,1),(12311,12958,2),(48488,48488,1),(48488,48514,2),(57810,57810,1),(57810,57811,2),(57810,57812,3),(17104,17104,1),(17104,24943,2),(51179,51179,1),(51179,51180,2),(51179,51181,3),(34952,34952,1),(34952,34953,2),(34950,34950,1),(34950,34954,2),(35299,35299,1),(35299,35300,2),(35299,35302,3),(35299,35303,4),(35299,35304,5),(35299,35305,6),(35299,35306,7),(35299,35307,8),(35299,35308,9),(47516,47516,1),(47516,47517,2),(53450,53450,1),(53450,53451,2),(53427,53427,1),(53427,53429,2),(53427,53430,3),(4195,4195,1),(4195,4196,2),(4195,4197,3),(4195,4198,4),(4195,4199,5),(4195,4200,6),(4195,4201,7),(4195,4202,8),(4195,5048,9),(4195,5049,10),(4195,27364,11),(61686,61686,1),(61686,61687,2),(61686,61688,3),(18218,18218,1),(18218,18219,2),(1853,1853,1),(1853,14922,2),(1853,14923,3),(1853,14924,4),(1853,14925,5),(1853,14926,6),(1853,14927,7),(1853,27344,8),(53178,53178,1),(53178,53179,2),(20174,20174,1),(20174,20175,2),(13960,13960,1),(13960,13961,2),(13960,13962,3),(13960,13963,4),(13960,13964,5),(16181,16181,1),(16181,16230,2),(16181,16232,3),(29187,29187,1),(29187,29189,2),(29187,29191,3),(20237,20237,1),(20237,20238,2),(14911,14911,1),(14911,15018,2),(29206,29206,1),(29206,29205,2),(29206,29202,3),(17003,17003,1),(17003,17004,2),(17003,17005,3),(30894,30894,1),(30894,30895,2),(2366,2368,2),(2366,3570,3),(2366,11993,4),(2366,28695,5),(2366,50300,6),(2366,74519,7),(34753,34753,1),(34753,34859,2),(27789,27789,1),(27789,27790,2),(14889,14889,1),(14889,15008,2),(14889,15009,3),(14889,15010,4),(14889,15011,5),(51698,51698,1),(51698,51700,2),(51698,51701,3),(44445,44445,1),(44445,44446,2),(44445,44448,3),(56339,56339,1),(56339,56340,2),(56339,56341,3),(1214,1214,1),(1214,1228,2),(1214,10221,3),(1214,10222,4),(1214,27391,5),(31670,31670,1),(31670,31672,2),(31670,55094,3),(55061,55061,1),(55061,55062,2),(11119,11119,1),(11119,11120,2),(11119,12846,3),(16493,16493,1),(16493,16494,2),(14079,14079,1),(14079,14080,2),(35104,35104,1),(35104,35110,2),(31569,31569,1),(31569,31570,2),(11185,11185,1),(11185,12487,2),(11185,12488,3),(50365,50365,1),(50365,50371,2),(30872,30872,1),(30872,30873,2),(12329,12329,1),(12329,12950,2),(11190,11190,1),(11190,12489,2),(17810,17810,1),(17810,17811,2),(17810,17812,3),(17810,17813,4),(17810,17814,5),(11255,11255,1),(11255,12598,2),(53180,53180,1),(53180,53181,2),(18827,18827,1),(18827,18829,2),(18179,18179,1),(18179,18180,2),(30049,30049,1),(30049,30051,2),(30049,30052,3),(29593,29593,1),(29593,29594,2),(54347,54347,1),(54347,54348,2),(54347,54349,3),(20138,20138,1),(20138,20139,2),(20138,20140,3),(63625,63625,1),(63625,63626,2),(14162,14162,1),(14162,14163,2),(14162,14164,3),(20502,20502,1),(20502,20503,2),(14168,14168,1),(14168,14169,2),(19557,19557,1),(19557,19558,2),(33600,33600,1),(33600,33601,2),(33600,33602,3),(53754,53754,1),(53754,53759,2),(11078,11078,1),(11078,11080,2),(16086,16086,1),(16086,16544,2),(11069,11069,1),(11069,12338,2),(11069,12339,3),(11069,12340,4),(11069,12341,5),(50384,50384,1),(50384,50385,2),(11070,11070,1),(11070,12473,2),(11070,16763,3),(11070,16765,4),(11070,16766,5),(16262,16262,1),(16262,16287,2),(13741,13741,1),(13741,13793,2),(20487,20487,1),(20487,20488,2),(12289,12289,1),(12289,12668,2),(14912,14912,1),(14912,15013,2),(18703,18703,1),(18703,18704,2),(30054,30054,1),(30054,30057,2),(17815,17815,1),(17815,17833,2),(17815,17834,3),(18694,18694,1),(18694,18695,2),(18694,18696,3),(14747,14747,1),(14747,14770,2),(14747,14771,3),(57849,57849,1),(57849,57850,2),(57849,57851,3),(29888,29888,1),(29888,29889,2),(13754,13754,1),(13754,13867,2),(14174,14174,1),(14174,14175,2),(14174,14176,3),(20234,20234,1),(20234,20235,2),(18182,18182,1),(18182,18183,2),(15273,15273,1),(15273,15312,2),(15273,15313,3),(12290,12290,1),(12290,12963,2),(14113,14113,1),(14113,14114,2),(14113,14115,3),(14113,14116,4),(14113,14117,5),(14748,14748,1),(14748,14768,2),(14748,14769,3),(15392,15392,1),(15392,15448,2),(17111,17111,1),(17111,17112,2),(17111,17113,3),(14908,14908,1),(14908,15020,2),(12797,12797,1),(12797,12799,2),(48985,48985,1),(48985,49488,2),(48985,49489,3),(11095,11095,1),(11095,12872,2),(11095,12873,3),(19491,19491,1),(19491,19493,2),(19491,19494,3),(17927,17927,1),(17927,17929,2),(17793,17793,1),(17793,17796,2),(17793,17801,3),(15275,15275,1),(15275,15317,2),(47569,47569,1),(47569,47570,2),(16261,16261,1),(16261,16290,2),(16261,51881,3),(13732,13732,1),(13732,13863,2),(14165,14165,1),(14165,14166,2),(13743,13743,1),(13743,13875,2),(53221,53221,1),(53221,53222,2),(53221,53224,3),(18754,18754,1),(18754,18755,2),(18754,18756,3),(52783,52783,1),(52783,52785,2),(52783,52786,3),(17123,17123,1),(17123,17124,2),(50391,50391,1),(50391,50392,2),(16180,16180,1),(16180,16196,2),(44394,44394,1),(44394,44395,2),(18459,18459,1),(18459,18460,2),(18459,54734,3),(50685,50685,1),(50685,50686,2),(50685,50687,3),(48483,48483,1),(48483,48484,2),(45357,45358,2),(45357,45359,3),(45357,45360,4),(45357,45361,5),(45357,45363,6),(45357,86008,7),(14893,14893,1),(14893,15357,2),(14892,14892,1),(14892,15362,2),(46908,46908,1),(46908,46909,2),(17080,17080,1),(17080,35358,2),(17080,35359,3),(17106,17106,1),(17106,17107,2),(17106,17108,3),(18135,18135,1),(18135,18136,2),(53252,53252,1),(53252,53253,2),(25229,25230,2),(25229,28894,3),(25229,28895,4),(25229,28897,5),(25229,51311,6),(25229,73318,7),(53695,53695,1),(53695,53696,2),(491,491,1),(491,857,2),(491,10165,3),(491,10166,4),(51123,51123,1),(51123,51127,2),(51123,51128,3),(47426,47426,1),(47426,47427,2),(47261,47261,1),(47261,47262,2),(56314,56314,1),(56314,56315,2),(48492,48492,1),(48492,48494,2),(48492,48495,3),(51480,51480,1),(51480,51481,2),(51480,51482,3),(2108,3104,2),(2108,3811,3),(2108,10662,4),(2108,32549,5),(2108,51302,6),(2108,81199,7),(19426,19426,1),(19426,19427,2),(19426,19429,3),(19426,19430,4),(19426,19431,5),(14128,14128,1),(14128,14132,2),(14128,14135,3),(81708,55428,2),(81708,55480,3),(81708,55500,4),(81708,55501,5),(81708,55502,6),(81708,55503,7),(24845,24845,1),(24845,25013,2),(24845,25014,3),(24845,25015,4),(24845,25016,5),(24845,25017,6),(30675,30675,1),(30675,30678,2),(30675,30679,3),(13712,13712,1),(13712,13788,2),(13712,13789,3),(53409,53409,1),(53409,53411,2),(48496,48496,1),(48496,48499,2),(48496,48500,3),(56342,56342,1),(56342,56343,2),(1809,1809,1),(1809,1810,2),(1809,6460,3),(53262,53262,1),(53262,53263,2),(53262,53264,3),(33589,33589,1),(33589,33590,2),(33589,33591,3),(51528,51528,1),(51528,51529,2),(51528,51530,3),(6121,6121,1),(6121,22784,2),(6121,22785,3),(6121,27392,4),(29441,29441,1),(29441,29444,2),(11247,11247,1),(11247,12606,2),(49224,49224,1),(49224,49610,2),(49224,49611,3),(32477,32477,1),(32477,32483,2),(32477,32484,3),(14138,14138,1),(14138,14139,2),(14138,14140,3),(14138,14141,4),(14138,14142,5),(1481,1481,1),(1481,8496,2),(1481,8497,3),(1481,10194,4),(1481,10195,5),(1481,10196,6),(1481,27398,7),(53241,53241,1),(53241,53243,2),(18767,18767,1),(18767,18768,2),(14904,14904,1),(14904,15024,2),(14904,15025,3),(14904,15026,4),(14904,15027,5),(34485,34485,1),(34485,34486,2),(34485,34487,3),(53125,53662,2),(53125,53663,3),(53125,53664,4),(53125,53665,5),(53125,53666,6),(53125,74495,7),(29074,29074,1),(29074,29075,2),(29074,29076,3),(18709,18709,1),(18709,18710,2),(19381,19381,1),(19381,19382,2),(19381,19383,3),(19381,19384,4),(19381,19385,5),(58378,58378,1),(58378,58379,2),(14520,14520,1),(14520,14780,2),(14520,14781,3),(18551,18551,1),(18551,18552,2),(18551,18553,3),(18551,18554,4),(18551,18555,5),(49024,49024,1),(49024,49538,2),(31584,31584,1),(31584,31585,2),(31584,31586,3),(31584,31587,4),(31584,31588,5),(14910,14910,1),(14910,33371,2),(2575,2576,2),(2575,3564,3),(2575,10248,4),(2575,29354,5),(2575,50310,6),(2575,74517,7),(44404,44404,1),(44404,54486,2),(47245,47245,1),(47245,47246,2),(47245,47247,3),(31679,31679,1),(31679,31680,2),(16896,16896,1),(16896,16897,2),(16896,16899,3),(16845,16845,1),(16845,16846,2),(16845,16847,3),(48963,48963,1),(48963,49564,2),(48963,49565,3),(14158,14158,1),(14158,14159,2),(24547,24547,1),(24547,24556,2),(24547,24557,3),(24547,24558,4),(24547,24559,5),(24547,24560,6),(24547,24561,7),(24547,24562,8),(24547,24631,9),(24547,24632,10),(24547,27362,11),(61689,61689,1),(61689,61690,2),(45281,45281,1),(45281,45282,2),(45281,45283,3),(33881,33881,1),(33881,33882,2),(57878,57878,1),(57878,57880,2),(16833,16833,1),(16833,16834,2),(17069,17069,1),(17069,17070,2),(30867,30867,1),(30867,30868,2),(30867,30869,3),(17074,17074,1),(17074,17075,2),(17074,17076,3),(30881,30881,1),(30881,30883,2),(30881,30884,3),(51459,51459,1),(51459,51462,2),(63117,63117,1),(63117,63121,2),(49226,49226,1),(49226,50137,2),(49226,50138,3),(31130,31130,1),(31130,31131,2),(30299,30299,1),(30299,30301,2),(44400,44400,1),(44400,44402,2),(44400,44403,3),(18094,18094,1),(18094,18095,2),(53295,53295,1),(53295,53296,2),(47179,47179,1),(47179,47180,2),(33872,33872,1),(33872,33873,2),(14057,14057,1),(14057,14072,2),(48389,48389,1),(48389,48392,2),(48389,48393,3),(53514,53514,1),(53514,53516,2),(47580,47580,1),(47580,47581,2),(19559,19559,1),(19559,19560,2),(11175,11175,1),(11175,12569,2),(11175,12571,3),(6311,6311,1),(6311,6314,2),(6311,6315,3),(6311,6316,4),(6311,6317,5),(53175,53175,1),(53175,53176,2),(6280,6280,1),(6280,6281,2),(6280,6282,3),(6280,6283,4),(6280,6286,5),(6328,6328,1),(6328,6331,2),(6328,6332,3),(6328,6333,4),(6328,6334,5),(6443,6443,1),(6443,6444,2),(6443,6445,3),(6443,6446,4),(6443,6447,5),(11151,11151,1),(11151,12952,2),(11151,12953,3),(53234,53234,1),(53234,53237,2),(53234,53238,3),(31638,31638,1),(31638,31639,2),(31638,31640,3),(53298,53298,1),(53298,53299,2),(41635,41635,1),(41635,48110,2),(41635,48111,3),(29438,29438,1),(29438,29439,2),(29438,29440,3),(13705,13705,1),(13705,13832,2),(13705,13843,3),(33859,33859,1),(33859,33866,2),(33859,33867,3),(16972,16972,1),(16972,16974,2),(51685,51685,1),(51685,51686,2),(51685,51687,3),(51685,51688,4),(51685,51689,5),(31574,31574,1),(31574,31575,2),(31574,54354,3),(57873,57873,1),(57873,57876,2),(57873,57877,3),(13733,13733,1),(13733,13865,2),(13733,13866,3),(31822,31822,1),(31822,31823,2),(26022,26022,1),(26022,26023,2),(34293,34293,1),(34293,34295,2),(34293,34296,3),(31244,31244,1),(31244,31245,2),(53228,53228,1),(53228,53232,2),(47535,47535,1),(47535,47536,2),(47535,47537,3),(48965,48965,1),(48965,49571,2),(48965,49572,3),(20177,20177,1),(20177,20179,2),(33201,33201,1),(33201,33202,2),(14179,14179,1),(14179,58422,2),(14179,58423,3),(14143,14143,1),(14143,14149,2),(14144,14144,1),(14144,14148,2),(48432,48432,1),(48432,48433,2),(48432,48434,3),(57470,57470,1),(57470,57472,2),(5405,5405,1),(5405,10052,2),(5405,10057,3),(5405,10058,4),(5405,27103,5),(5405,42987,6),(5405,42988,7),(34491,34491,1),(34491,34492,2),(34491,34493,3),(16187,16187,1),(16187,16205,2),(16040,16040,1),(16040,16113,2),(48539,48539,1),(48539,48544,2),(53380,53380,1),(53380,53381,2),(53380,53382,3),(49188,49188,1),(49188,56822,2),(49188,59057,3),(17959,17959,1),(17959,59738,2),(17959,59739,3),(17959,59740,4),(17959,59741,5),(49455,49455,1),(49455,50147,2),(14156,14156,1),(14156,14160,2),(14156,14161,3),(31848,31848,1),(31848,31849,2),(20359,20359,1),(20359,20360,2),(53375,53375,1),(53375,53376,2),(58684,58684,1),(58684,58683,2),(51682,51682,1),(51682,58413,2),(49004,49004,1),(49004,49508,2),(49004,49509,3),(1811,1811,1),(1811,8447,2),(1811,8448,3),(1811,8449,4),(1811,10208,5),(1811,10209,6),(1811,10210,7),(1811,27375,8),(1811,27376,9),(24641,24641,1),(24641,24584,2),(24641,24588,3),(24641,24589,4),(24641,27361,5),(14186,14186,1),(14186,14190,2),(20224,20224,1),(20224,20225,2),(14909,14909,1),(14909,15017,2),(2075,2075,1),(2075,38116,2),(63730,63730,1),(63730,63733,2),(5597,5597,1),(5597,5598,2),(14171,14171,1),(14171,14172,2),(13983,13983,1),(13983,14070,2),(13983,14071,3),(32385,32385,1),(32385,32387,2),(32385,32392,3),(18271,18271,1),(18271,18272,2),(18271,18273,3),(18271,18274,4),(18271,18275,5),(62759,62759,1),(62759,62760,2),(11170,11170,1),(11170,12982,2),(11170,12983,3),(44745,44745,1),(44745,54787,2),(53709,53709,1),(53709,53710,2),(12298,12298,1),(12298,12724,2),(12298,12725,3),(2607,2607,1),(2607,2606,2),(2607,2608,3),(2607,2609,4),(2607,2610,5),(18469,18469,1),(18469,55021,2),(62764,62764,1),(62764,62765,2),(8613,8617,2),(8613,8618,3),(8613,10768,4),(8613,32678,5),(8613,50305,6),(8613,74522,7),(51708,51708,1),(51708,51709,2),(51708,51710,3),(700,700,1),(700,1090,2),(30892,30892,1),(30892,30893,2),(246,246,1),(246,6146,2),(53302,53302,1),(53302,53303,2),(53302,53304,3),(30293,30293,1),(30293,30295,2),(17804,17804,1),(17804,17805,2),(48435,48435,1),(48435,48436,2),(48435,48437,3),(11242,11242,1),(11242,12467,2),(11242,12469,3),(35578,35578,1),(35578,35581,2),(53203,53203,1),(53203,53204,2),(53203,53205,3),(53182,53182,1),(53182,53183,2),(53182,53184,3),(8112,8112,1),(8112,8113,2),(8112,8114,3),(8112,12177,4),(8112,33080,5),(8112,43197,6),(8112,48103,7),(8112,48104,8),(16814,16814,1),(16814,16815,2),(16814,16816,3),(51525,51525,1),(51525,51526,2),(51525,51527,3),(8073,8073,1),(8073,38115,2),(51483,51483,1),(51483,51485,2),(8118,8118,1),(8118,8119,2),(8118,8120,3),(8118,12179,4),(8118,33082,5),(8118,43199,6),(8118,58448,7),(8118,58449,8),(44397,44397,1),(44397,44398,2),(44397,44399,3),(5648,5648,1),(5648,5649,2),(5726,5726,1),(5726,5727,2),(29723,29723,1),(29723,29725,2),(49018,49018,1),(49018,49529,2),(49018,49530,3),(19290,19290,1),(19290,19294,2),(19290,24283,3),(19286,19286,1),(19286,19287,2),(46951,46951,1),(46951,46952,2),(46951,46953,3),(56333,56333,1),(56333,56336,2),(12295,12295,1),(12295,12676,2),(12295,12677,3),(3908,3909,2),(3908,3910,3),(3908,12180,4),(3908,26790,5),(3908,51309,6),(3908,75156,7),(56636,56636,1),(56636,56637,2),(56636,56638,3),(47558,47558,1),(47558,47559,2),(47558,47560,3),(16929,16929,1),(16929,16930,2),(16929,16931,3),(65661,65661,1),(65661,66191,2),(65661,66192,3),(34497,34497,1),(34497,34498,2),(34497,34499,3),(5952,5952,1),(5952,51679,2),(16179,16179,1),(16179,16214,2),(16179,16215,3),(51562,51562,1),(51562,51563,2),(51562,51564,3),(29447,29447,1),(29447,55339,2),(29447,55340,3),(47263,47263,1),(47263,47264,2),(47263,47265,3),(16173,16173,1),(16173,16222,2),(53120,53121,2),(53120,53122,3),(53120,53123,4),(53120,53124,5),(53120,53040,6),(53120,74496,7),(12299,12299,1),(12299,12761,2),(12299,12762,3),(20143,20143,1),(20143,20144,2),(20143,20145,3),(16252,16252,1),(16252,16306,2),(16252,16307,3),(49042,49042,1),(49042,49786,2),(49042,49787,3),(19376,19376,1),(19376,63457,2),(19376,63458,3),(51627,51627,1),(51627,51628,2),(51627,51629,3),(47586,47586,1),(47586,47587,2),(47586,47588,3),(47573,47573,1),(47573,47577,2),(14522,14522,1),(14522,14788,2),(14522,14789,3),(14522,14790,4),(14522,14791,5),(49588,49588,1),(49588,49589,2),(18769,18769,1),(18769,18770,2),(18769,18771,3),(18769,18772,4),(18769,18773,5),(30664,30664,1),(30664,30665,2),(30664,30666,3),(15274,15274,1),(15274,15311,2),(20049,20049,1),(20049,20056,2),(20049,20057,3),(51745,51745,1),(51745,51746,2),(16513,16513,1),(16513,16514,2),(16513,16515,3),(48962,48962,1),(48962,49567,2),(48962,49568,3),(14524,14524,1),(14524,14525,2),(14524,14526,3),(14524,14527,4),(14524,14528,5),(51692,51692,1),(51692,51696,2),(30919,30919,1),(30919,30920,2),(62758,62758,1),(62758,62762,2),(53215,53215,1),(53215,53216,2),(11180,11180,1),(11180,28592,2),(11180,28593,3),(11108,11108,1),(11108,12349,2),(11108,12350,3),(33603,33603,1),(33603,33604,2),(33603,33605,3),(46867,46867,1),(46867,56611,2),(46867,56612,3),(23030,23030,1),(23030,27394,2),(1467,1467,1),(1467,8440,2),(1467,8441,3),(1467,8442,4),(1467,10203,5),(1467,10204,6),(1467,27380,7),(1467,27381,8),(1472,1472,1),(1472,1473,2),(1472,1474,3),(1472,1475,4),(1472,10158,5),(1472,27393,6),(1472,42999,7),(27811,27811,1),(27811,27815,2),(27811,27816,3),(27813,27813,1),(27813,27817,2),(27813,27818,3),(33142,33142,1),(33142,33145,2),(1196,1196,1),(1196,6142,2),(1196,8428,3),(1196,10188,4),(1196,10189,5),(1196,10190,6),(1196,27384,7),(7370,7370,1),(7370,26184,2),(7370,26185,3),(7370,26186,4),(7370,26202,5),(7370,28343,6),(20387,20387,1),(20387,20388,2),(20387,20389,3),(20387,20390,4),(20387,20391,5),(20387,20392,6),(20387,27491,7),(1747,1747,1),(1747,1748,2),(1747,1749,3),(1747,1750,4),(1747,1751,5),(1747,16698,6),(1747,27346,7),(6966,6966,1),(6966,30880,2),(6966,30683,3),(6966,30682,4),(6966,29520,5),(6964,6964,1),(6964,11413,2),(6964,11414,3),(6964,11415,4),(6964,1386,5),(9799,9799,1),(9799,25988,2),(7230,7230,1),(7230,7231,2),(7230,7232,3),(7230,7233,4),(7230,7234,5),(24440,24440,1),(24440,24441,2),(24440,24463,3),(24440,24464,4),(24440,27351,5),(20322,20322,1),(20322,20323,2),(20322,20324,3),(20322,20326,4),(20322,20327,5),(20322,27489,6),(20322,47998,7),(7829,7829,1),(7829,7874,2),(7829,7875,3),(7806,7806,1),(7806,7807,2),(7806,7808,3),(12319,12319,1),(12319,12971,2),(12319,12972,3),(12966,12966,1),(12966,12967,2),(12966,12968,3),(45234,45234,1),(45234,45243,2),(1194,1194,1),(1194,1225,2),(1194,6132,3),(1194,10231,4),(1194,27387,5),(7240,7240,1),(7240,7236,2),(7240,7238,3),(7240,7237,4),(7240,7239,5),(24475,24475,1),(24475,24476,2),(24475,24477,3),(24475,24478,4),(24475,27352,5),(7245,7245,1),(7245,7246,2),(7245,7247,3),(7245,7248,4),(7245,7249,5),(7245,17545,6),(11103,11103,1),(11103,12357,2),(11103,12358,3),(19572,19572,1),(19572,19573,2),(53569,53569,1),(53569,53576,2),(13976,13976,1),(13976,13979,2),(30154,30154,1),(30154,30199,2),(30154,30200,3),(17233,17233,1),(17233,9257,2),(7250,7250,1),(7250,7251,2),(7250,7252,3),(7250,7253,4),(7250,7254,5),(24494,24494,1),(24494,24511,2),(24494,24512,3),(24494,24513,4),(24494,27354,5),(24451,24451,1),(24451,24454,2),(24451,24455,3),(20381,20381,1),(20381,20382,2),(20381,20383,3),(20381,20384,4),(20381,20385,5),(20381,20386,6),(20381,27492,7),(20381,48001,8),(20381,48002,9),(46945,46945,1),(46945,46949,2),(46946,46946,1),(46946,46947,2),(7235,7235,1),(7235,7241,2),(7235,7242,3),(7235,7243,4),(7235,7244,5),(24490,24490,1),(24490,24514,2),(24490,24515,3),(24490,24516,4),(24490,27353,5),(20403,20403,1),(20403,20404,2),(20403,20405,3),(20403,20406,4),(20403,27494,5),(20393,20393,1),(20393,20394,2),(20393,20395,3),(20393,20396,4),(20393,27500,5),(20393,33703,6),(20393,48005,7),(20393,48006,8),(33150,33150,1),(33150,33154,2),(20429,20429,1),(20429,20430,2),(20429,20431,3),(20429,20432,4),(20429,27497,5),(53486,53486,1),(53486,53488,2),(5364,5364,1),(5364,5368,2),(5364,5369,3),(5364,5370,4),(5363,5363,1),(5363,5365,2),(5363,5366,3),(5363,5367,4),(4112,4112,1),(4112,4113,2),(4112,4115,3),(4112,4114,4),(4107,4107,1),(4107,4108,2),(4107,4109,3),(4107,4111,4),(51675,51675,1),(51675,51677,2),(51672,51672,1),(51672,51674,2),(30802,30802,1),(30802,30808,2),(746,746,1),(746,1159,2),(746,3267,3),(746,3268,4),(746,7926,5),(746,7927,6),(746,10838,7),(746,10839,8),(746,18608,9),(746,18610,10),(746,27030,11),(746,27031,12),(746,45543,13),(746,51827,14),(746,45544,15),(746,51803,16),(33191,33191,1),(33191,33192,2),(33196,33196,1),(33196,33197,2),(33196,33198,3),(19578,19578,1),(19578,20895,2),(19579,19579,1),(19579,24529,2),(30160,30160,1),(30160,29179,2),(30160,29180,3),(7620,7731,2),(7620,7732,3),(7620,18248,4),(7620,33095,5),(7620,51294,6),(7620,88868,7),(53671,53671,1),(53671,53673,2),(53671,54151,3),(53655,53655,1),(53655,53656,2),(53655,53657,3),(34506,34506,1),(34506,34507,2),(34506,34508,3),(34506,34838,4),(34506,34839,5),(34833,34833,1),(34833,34834,2),(34833,34835,3),(34833,34836,4),(34833,34837,5),(34948,34948,1),(34948,34949,2),(35098,35098,1),(35098,35099,2),(29841,29841,1),(29841,29842,2),(29834,29834,1),(29834,29838,2),(46856,46856,1),(46856,46857,2),(1241,1241,1),(1241,8493,2),(1241,10162,3),(1241,10163,4),(1241,10164,5),(1241,27386,6),(2124,2124,1),(2124,2125,2),(2124,8425,3),(2124,8426,4),(2124,10217,5),(2124,10218,6),(2124,27385,7),(33388,33391,2),(33388,34090,3),(33388,34091,4),(33388,90265,5),(16958,16958,1),(16958,16961,2),(37116,37116,1),(37116,37117,2),(2980,2980,1),(2980,2981,2),(2980,2982,3),(2980,3667,4),(2980,2975,5),(2980,2976,6),(2980,2977,7),(2980,3666,8),(2980,27347,9),(51983,51983,1),(51983,51986,2),(26094,26094,1),(26094,26189,2),(26094,26190,3),(26094,27366,4),(44440,44440,1),(44440,44441,2),(64353,64353,1),(64353,64357,2),(17002,17002,1),(17002,24866,2),(35363,35363,1),(35363,35364,2),(16880,16880,1),(16880,61345,2),(16880,61346,3),(63349,63349,1),(63349,63350,2),(63349,63351,3),(23785,23785,1),(23785,23822,2),(23785,23823,3),(23785,23824,4),(23785,23825,5),(62905,62905,1),(62905,62908,2),(53483,53483,1),(53483,53485,2),(53554,53554,1),(53554,53555,2),(8096,8096,1),(8096,8097,2),(8096,8098,3),(8096,12176,4),(8096,33078,5),(8096,43195,6),(8096,48099,7),(8096,48100,8),(8099,8099,1),(8099,8100,2),(8099,8101,3),(8099,12178,4),(8099,33081,5),(8099,48101,6),(8099,48102,7),(8099,43198,8),(64418,64418,1),(64418,64419,2),(64418,64420,3),(2259,2259,1),(2018,2018,1),(7411,7411,1),(4036,4036,1),(2366,2366,1),(81708,81708,1),(45357,45357,1),(25229,25229,1),(2108,2108,1),(2575,2575,1),(53120,53120,1),(8613,8613,1),(53125,53125,1),(3908,3908,1),(78670,89722,7),(78670,89721,6),(78670,89720,5),(78670,89719,4),(78670,89718,3),(78670,88961,2),(78670,78670,1),(2550,2550,1),(3273,3273,1),(7620,7620,1),(33388,33388,1),(1178,9635,2),(1178,1178,1);
/*!40000 ALTER TABLE `spell_ranks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spell_required`
--

DROP TABLE IF EXISTS `spell_required`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spell_required` (
  `spell_id` mediumint(9) NOT NULL DEFAULT '0',
  `req_spell` mediumint(9) NOT NULL DEFAULT '0',
  PRIMARY KEY (`spell_id`,`req_spell`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='Spell Additinal Data';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spell_required`
--

LOCK TABLES `spell_required` WRITE;
/*!40000 ALTER TABLE `spell_required` DISABLE KEYS */;
/*!40000 ALTER TABLE `spell_required` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spell_script_names`
--

DROP TABLE IF EXISTS `spell_script_names`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spell_script_names` (
  `spell_id` int(11) NOT NULL,
  `ScriptName` char(64) NOT NULL,
  UNIQUE KEY `spell_id` (`spell_id`,`ScriptName`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spell_script_names`
--

LOCK TABLES `spell_script_names` WRITE;
/*!40000 ALTER TABLE `spell_script_names` DISABLE KEYS */;
/*!40000 ALTER TABLE `spell_script_names` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spell_scripts`
--

DROP TABLE IF EXISTS `spell_scripts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spell_scripts` (
  `id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `effIndex` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `delay` int(10) unsigned NOT NULL DEFAULT '0',
  `command` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `datalong` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `datalong2` int(10) unsigned NOT NULL DEFAULT '0',
  `dataint` int(11) NOT NULL DEFAULT '0',
  `x` float NOT NULL DEFAULT '0',
  `y` float NOT NULL DEFAULT '0',
  `z` float NOT NULL DEFAULT '0',
  `o` float NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Spell Scripts';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spell_scripts`
--

LOCK TABLES `spell_scripts` WRITE;
/*!40000 ALTER TABLE `spell_scripts` DISABLE KEYS */;
/*!40000 ALTER TABLE `spell_scripts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spell_target_position`
--

DROP TABLE IF EXISTS `spell_target_position`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spell_target_position` (
  `id` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'Identifier',
  `target_map` smallint(5) unsigned NOT NULL DEFAULT '0',
  `target_position_x` float NOT NULL DEFAULT '0',
  `target_position_y` float NOT NULL DEFAULT '0',
  `target_position_z` float NOT NULL DEFAULT '0',
  `target_orientation` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='Spell System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spell_target_position`
--

LOCK TABLES `spell_target_position` WRITE;
/*!40000 ALTER TABLE `spell_target_position` DISABLE KEYS */;
/*!40000 ALTER TABLE `spell_target_position` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spell_threat`
--

DROP TABLE IF EXISTS `spell_threat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spell_threat` (
  `entry` mediumint(8) unsigned NOT NULL,
  `Threat` smallint(6) NOT NULL,
  PRIMARY KEY (`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='CTDB Spell Threat';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spell_threat`
--

LOCK TABLES `spell_threat` WRITE;
/*!40000 ALTER TABLE `spell_threat` DISABLE KEYS */;
INSERT INTO `spell_threat` VALUES (78,20),(770,108),(1715,61),(6572,155),(7386,100),(17735,200),(20736,100),(23922,160),(24394,580),(20243,101),(33745,285),(16857,108),(6343,17),(33878,129),(20925,20),(2139,300),(6673,1),(469,68),(12797,25),(12799,25),(72,36);
/*!40000 ALTER TABLE `spell_threat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spelldifficulty_dbc`
--

DROP TABLE IF EXISTS `spelldifficulty_dbc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spelldifficulty_dbc` (
  `id` int(11) unsigned NOT NULL DEFAULT '0',
  `spellid0` int(11) unsigned NOT NULL DEFAULT '0',
  `spellid1` int(11) unsigned NOT NULL DEFAULT '0',
  `spellid2` int(11) unsigned NOT NULL DEFAULT '0',
  `spellid3` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Spelldificulty based on dbc';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spelldifficulty_dbc`
--

LOCK TABLES `spelldifficulty_dbc` WRITE;
/*!40000 ALTER TABLE `spelldifficulty_dbc` DISABLE KEYS */;
/*!40000 ALTER TABLE `spelldifficulty_dbc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transports`
--

DROP TABLE IF EXISTS `transports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transports` (
  `guid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `name` text,
  `period` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `ScriptName` char(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`guid`),
  UNIQUE KEY `idx_entry` (`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='Transports';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transports`
--

LOCK TABLES `transports` WRITE;
/*!40000 ALTER TABLE `transports` DISABLE KEYS */;
/*!40000 ALTER TABLE `transports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vehicle_accessory`
--

DROP TABLE IF EXISTS `vehicle_accessory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vehicle_accessory` (
  `entry` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `accessory_entry` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `seat_id` tinyint(1) NOT NULL DEFAULT '0',
  `minion` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  PRIMARY KEY (`entry`,`seat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='CTDB Vehicle Accessory System.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vehicle_accessory`
--

LOCK TABLES `vehicle_accessory` WRITE;
/*!40000 ALTER TABLE `vehicle_accessory` DISABLE KEYS */;
INSERT INTO `vehicle_accessory` VALUES (28782,28768,0,0,'Acherus Deathcharger'),(28312,28319,7,1,'Wintergrasp Siege Engine'),(32627,32629,7,1,'Wintergrasp Siege Engine'),(32930,32933,0,1,'Kologarn'),(32930,32934,1,1,'Kologarn'),(33109,33167,1,1,'Salvaged Demolisher'),(33060,33067,7,1,'Salvaged Siege Engine'),(33113,33114,0,1,'Flame Leviathan'),(33113,33114,1,1,'Flame Leviathan'),(33113,33114,2,1,'Flame Leviathan'),(33113,33114,3,1,'Flame Leviathan'),(33113,33139,7,1,'Flame Leviathan'),(36678,38309,0,1,'Professor Putricide - trigger'),(33214,33218,1,1,'Mechanolift 304-A'),(35637,34705,0,0,'Marshal Jacob Alerius\' Mount'),(35633,34702,0,0,'Ambrose Boltspark\'s Mount'),(35768,34701,0,0,'Colosos\' Mount'),(34658,34657,0,0,'Jaelyne Evensong\'s Mount'),(35636,34703,0,0,'Lana Stouthammer\'s Mount'),(35638,35572,0,0,'Mokra the Skullcrusher\'s Mount'),(35635,35569,0,0,'Eressea Dawnsinger\'s Mount'),(35640,35571,0,0,'Runok Wildmane\'s Mount'),(35641,35570,0,0,'Zul\'tore\'s Mount'),(35634,35617,0,0,'Deathstalker Visceri\'s Mount'),(27241,27268,0,0,'Risen Gryphon'),(27661,27662,0,0,'Wintergarde Gryphon'),(29698,29699,0,0,'Drakuru Raptor'),(33778,33780,0,0,'Tournament Hippogryph'),(33687,33695,0,0,'Chillmaw'),(33687,33695,1,0,'Chillmaw'),(33687,33695,2,0,'Chillmaw'),(29625,29694,0,0,'Hyldsmeet Proto-Drake'),(30330,30332,0,0,'Jotunheim Proto-Drake'),(32189,32190,0,0,'Skybreaker Recon Fighter'),(36678,38308,1,1,'Professor Putricide - trigger'),(32640,32642,1,0,'Traveler Mammoth (H) - Vendor'),(32640,32641,2,0,'Traveler Mammoth (H) - Vendor & Repairer'),(32633,32638,1,0,'Traveler Mammoth (A) - Vendor'),(32633,32639,2,0,'Traveler Mammoth (A) - Vendor & Repairer'),(33669,33666,0,0,'Demolisher Engineer Blastwrench'),(29555,29556,0,0,'Goblin Sapper'),(28018,28006,0,1,'Thiassi the Light Bringer'),(28054,28053,0,0,'Lucky Wilhelm - Apple'),(35491,35451,0,0,'Black Knight'),(33299,35323,0,1,'Darkspear Raptor'),(33418,35326,0,1,'Silvermoon Hawkstrider'),(33409,35314,0,1,'Orgrimmar Wolf'),(33300,35325,0,1,'Thunder Bluff Kodo'),(33408,35329,0,1,'Ironforge Ram'),(33301,35331,0,1,'Gnomeregan Mechanostrider'),(33414,35327,0,1,'Forsaken Warhorse'),(33297,35328,0,1,'Stormwind Steed'),(33416,35330,0,1,'Exodar Elekk'),(33298,35332,0,1,'Darnassian Nightsaber'),(33318,35330,0,1,'Exodar Elekk'),(33319,35332,0,1,'Darnassian Nightsaber'),(33316,35329,0,1,'Ironforge Ram'),(33317,35331,0,1,'Gnomeregan Mechanostrider'),(33217,35328,0,1,'Stormwind Steed'),(33324,35327,0,1,'Forsaken Warhorse'),(33322,35325,0,1,'Thunder Bluff Kodo'),(33320,35314,0,1,'Orgrimmar Wolf'),(33323,35326,0,1,'Silvermoon Hawkstrider'),(33321,35323,0,1,'Darkspear Raptor'),(28614,28616,0,1,'Scarlet Gryphon Rider'),(36891,31260,0,0,'Ymirjar Skycaller on Drake'),(38500,38493,0,0,'Argent Crusader'),(27626,27627,0,0,'Tatjana\'s Horse'),(28009,28093,0,0,'Sholazar Tickbird'),(28451,1412,0,1,'Hemet Nesingwary'),(30204,30268,0,0,'Webbed Crusader'),(29351,29558,0,0,'Frost Giant Target Bunny'),(29708,29805,0,1,'Captive Proto Drake Beam Bunny'),(30174,30175,0,0,'Hyldsmeet Bear Rider'),(29460,29458,0,0,'Brunnhildar Drakerider'),(29500,29498,0,0,'Brunnhildar Warmaiden'),(29358,29558,0,0,'Frost Giant Target Bunny'),(25968,25801,0,0,'Nedar, Lord of Rhinos'),(38431,38309,0,0,'Slimy Tentacle Stalker'),(38585,38309,0,0,'Slimy Tentacle Stalker'),(38586,38309,0,0,'Slimy Tentacle Stalker'),(38431,38308,1,1,'Ooze Covered Tentacle Stalker'),(38585,38308,1,1,'Ooze Covered Tentacle Stalker'),(38586,38308,1,1,'Ooze Covered Tentacle Stalker'),(28669,28717,0,0,'Overlord Drakuru'),(39860,39264,0,0,'Gnomeregan Mechano-Tank Pilot'),(36896,28717,0,0,'Overlord Drakuru'),(36794,36658,0,0,'Scourgelord Tyrannus'),(29931,29982,0,0,'Drakkari Rider on Drakkari Rhino, not minion'),(24750,24751,0,1,'Excelsior rides Hidalgo the Master Falconer'),(36661,36658,0,0,'Scourgelord Tyrannus and Rimefang'),(36476,36477,0,0,'Krick on Ick'),(30234,30245,0,0,'Hover Disk - Nexus Lord'),(30248,30249,0,0,'Hover Disk - Scion of Eternity'),(46012,46363,0,0,'Target Acquisition Device'),(13210,466,0,0,'General Marcus Jonathan'),(17804,721,0,1,'Rowe'),(2041,7555,0,0,'Ancient Protector'),(4262,5945,0,1,'Darnassus Sentinels'),(6588,49877,0,0,'Golden King'),(2500,25111,0,1,'Captain Hecklebury Smotts'),(35999,35063,0,0,'Kezan Citizen'),(37968,38505,0,0,'Argent Hippogryph'),(25049,24976,0,0,'Dawnstar Charger'),(31269,27559,0,0,'Kor\'kron Battle Wyvern'),(33293,33329,0,1,'XT-002 Deconstructor - Heart'),(29433,29440,0,0,'Goblin Sapper in K3'),(29838,29836,0,0,'Drakkari Battle Rider on Drakkari Rhino, not minion'),(31262,31263,0,0,'Carrion Hunter rides Blight Falconer'),(31406,31408,0,1,'Alliance Bomber Seat on Alliance Infra-green Bomber'),(31406,31407,1,1,'Alliance Turret Seat on Alliance Infra-green Bomber'),(31406,31409,2,1,'Alliance Engineering Seat on rides Alliance Infra-green Bomber'),(31406,32217,3,1,'Banner Bunny, Hanging, Alliance on Alliance Infra-green Bomber'),(31406,32221,4,1,'Banner Bunny, Side, Alliance on Alliance Infra-green Bomber'),(31406,32221,5,1,'Banner Bunny, Side, Alliance on Alliance Infra-green Bomber'),(31406,32256,6,1,'Shield Visual Loc Bunny on Alliance Infra-green Bomber'),(31406,32274,7,0,'Alliance Bomber Pilot rides Alliance Infra-green Bomber'),(31583,31630,1,1,'Skytalon Explosion Bunny on Frostbrood Skytalon'),(31881,31891,0,0,'Kor\'kron Transport Pilot rides Kor\'kron Troop Transport'),(31881,31884,1,1,'Kor\'kron Suppression Turret on Kor\'kron Troop Transport'),(31881,31882,2,0,'Kor\'kron Infiltrator on Kor\'kron Troop Transport'),(31881,31882,3,0,'Kor\'kron Infiltrator on Kor\'kron Troop Transport'),(31881,31882,4,0,'Kor\'kron Infiltrator on Kor\'kron Troop Transport'),(31881,31882,5,0,'Kor\'kron Infiltrator on Kor\'kron Troop Transport'),(31884,31882,0,1,'Kor\'kron Infiltrator rides Kor\'kron Suppression Turret'),(32225,32223,0,0,'Skybreaker Transport Pilot rides Skybreaker Troop Transport'),(32225,32227,1,1,'Skybreaker Suppression Turret on Skybreaker Troop Transport'),(32225,32222,2,0,'Skybreaker Infiltrator on Skybreaker Troop Transport'),(32225,32222,3,0,'Skybreaker Infiltrator on Skybreaker Troop Transport'),(32225,32222,4,0,'Skybreaker Infiltrator on Skybreaker Troop Transport'),(32225,32222,5,0,'Skybreaker Infiltrator on Skybreaker Troop Transport'),(32227,32222,0,1,'Skybreaker Infiltrator rides Skybreaker Suppression Turret'),(32490,32486,0,0,'Scourge Death Knight rides Scourge Deathcharger'),(32344,32274,0,0,'Alliance Bomber Pilot rides Alliance Rescue Craft'),(32344,32531,2,1,'Banner Bunny, Side, Alliance, Small rides Alliance Rescue Craft');
/*!40000 ALTER TABLE `vehicle_accessory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vehicle_scaling_info`
--

DROP TABLE IF EXISTS `vehicle_scaling_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vehicle_scaling_info` (
  `entry` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `baseItemLevel` float NOT NULL DEFAULT '0',
  `scalingFactor` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='CTDB Vehicle Scaling Info';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vehicle_scaling_info`
--

LOCK TABLES `vehicle_scaling_info` WRITE;
/*!40000 ALTER TABLE `vehicle_scaling_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `vehicle_scaling_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `version`
--

DROP TABLE IF EXISTS `version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `version` (
  `core_version` varchar(120) DEFAULT NULL COMMENT 'Core revision dumped at startup.',
  `core_revision` varchar(120) DEFAULT NULL,
  `db_version` varchar(120) DEFAULT NULL COMMENT 'Version of world DB.',
  `script_version` varchar(120) DEFAULT NULL COMMENT 'Version of scripts DB.',
  `cache_id` int(10) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='CTDB and TrinityCore Version Notes';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `version`
--

LOCK TABLES `version` WRITE;
/*!40000 ALTER TABLE `version` DISABLE KEYS */;
/*!40000 ALTER TABLE `version` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `waypoint_data`
--

DROP TABLE IF EXISTS `waypoint_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `waypoint_data` (
  `id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Creature GUID',
  `point` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `position_x` float NOT NULL DEFAULT '0',
  `position_y` float NOT NULL DEFAULT '0',
  `position_z` float NOT NULL DEFAULT '0',
  `delay` int(10) unsigned NOT NULL DEFAULT '0',
  `move_flag` tinyint(1) NOT NULL DEFAULT '0',
  `action` int(11) NOT NULL DEFAULT '0',
  `action_chance` smallint(3) NOT NULL DEFAULT '100',
  `wpguid` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `waypoint_data`
--

LOCK TABLES `waypoint_data` WRITE;
/*!40000 ALTER TABLE `waypoint_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `waypoint_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `waypoint_scripts`
--

DROP TABLE IF EXISTS `waypoint_scripts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `waypoint_scripts` (
  `id` int(11) unsigned NOT NULL DEFAULT '0',
  `delay` int(11) unsigned NOT NULL DEFAULT '0',
  `command` int(11) unsigned NOT NULL DEFAULT '0',
  `datalong` int(11) unsigned NOT NULL DEFAULT '0',
  `datalong2` int(11) unsigned NOT NULL DEFAULT '0',
  `dataint` int(11) unsigned NOT NULL DEFAULT '0',
  `x` float NOT NULL DEFAULT '0',
  `y` float NOT NULL DEFAULT '0',
  `z` float NOT NULL DEFAULT '0',
  `o` float NOT NULL DEFAULT '0',
  `guid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`guid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='CTDB Waypoint Scripts';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `waypoint_scripts`
--

LOCK TABLES `waypoint_scripts` WRITE;
/*!40000 ALTER TABLE `waypoint_scripts` DISABLE KEYS */;
/*!40000 ALTER TABLE `waypoint_scripts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `waypoints`
--

DROP TABLE IF EXISTS `waypoints`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `waypoints` (
  `entry` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `pointid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `position_x` float NOT NULL DEFAULT '0',
  `position_y` float NOT NULL DEFAULT '0',
  `position_z` float NOT NULL DEFAULT '0',
  `point_comment` text,
  PRIMARY KEY (`entry`,`pointid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='CTDB Creature waypoints';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `waypoints`
--

LOCK TABLES `waypoints` WRITE;
/*!40000 ALTER TABLE `waypoints` DISABLE KEYS */;
/*!40000 ALTER TABLE `waypoints` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2011-11-25 11:41:07
