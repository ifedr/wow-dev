-- instance_blackrock_caverns 

UPDATE creature_template SET ScriptName = 'boss_ascendant' WHERE entry = 39705;
UPDATE creature_template SET  AIName = '' WHERE entry = 39705;
UPDATE creature_template SET ScriptName = 'boss_beauty'  WHERE entry = 39700;
UPDATE creature_template SET AIName = '' WHERE entry = 39700;
UPDATE creature_template SET ScriptName = 'boss_corla' WHERE entry = 39679;
UPDATE creature_template SET AIName = '' WHERE entry = 39679;
UPDATE creature_template SET ScriptName = 'boss_karsh' WHERE entry = 39698;
UPDATE creature_template SET AIName = '' WHERE entry = 39698;
UPDATE creature_template SET ScriptName = 'boss_romogg' WHERE entry = 39665;
UPDATE creature_template SET AIName = '' WHERE entry = 39665;

-- instance_deadmines'

UPDATE creature_template SET ScriptName = 'boss_glubtok' WHERE entry = 47162;
UPDATE creature_template SET AIName = '' WHERE entry = 47162;
UPDATE creature_template SET ScriptName = 'boss_reaper'  WHERE entry = 43778;
UPDATE creature_template SET AIName = '' WHERE entry = 43778;
UPDATE creature_template SET ScriptName = 'boss_admiral' WHERE entry = 47626;
UPDATE creature_template SET  AIName = '' WHERE entry = 47626;
UPDATE creature_template SET ScriptName = 'boss_captain' WHERE entry = 47739;
UPDATE creature_template SET AIName = '' WHERE entry = 47739;

--  instance_shadowfang'

UPDATE creature_template SET ScriptName = 'boss_walden' WHERE entry = 46963;
UPDATE creature_template SET AIName = '' WHERE entry = 46963;
UPDATE creature_template SET ScriptName = 'boss_springvale'  WHERE entry = 4278;
UPDATE creature_template SET AIName = '' where entry = 4278;
UPDATE creature_template SET ScriptName = 'boss_silverlaine' WHERE entry = 3887;
UPDATE creature_template SET AIName = '' WHERE entry = 3887;
UPDATE creature_template SET ScriptName = 'boss_godfrey' WHERE entry = 46964;
UPDATE creature_template SET AIName = '' WHERE entry = 46964;
UPDATE creature_template SET ScriptName = 'boss_ashbury' WHERE entry = 46962;
UPDATE creature_template SET AIName = '' WHERE entry = 46962;


-- For cancel this update use something like this:

-- UPDATE creature_template SET ScriptName = '' WHERE entry = 39705;