UPDATE creature_template SET ScriptName = 'boss_magmaw' WHERE entry = 41570;
UPDATE creature_template SET  AIName = '' WHERE entry = 41570;

UPDATE creature_template SET ScriptName = 'boss_maloriak' WHERE entry = 41378;
UPDATE creature_template SET  AIName = '' WHERE entry = 41378;

UPDATE creature_template SET ScriptName = 'boss_atramedes' WHERE entry = 41442;
UPDATE creature_template SET  AIName = '' WHERE entry = 41442;

UPDATE creature_template SET ScriptName = 'boss_chimaeron' WHERE entry = 43296;
UPDATE creature_template SET  AIName = '' WHERE entry = 43296;

UPDATE creature_template SET  flags_extra = 1 WHERE entry IN  (43296, 41570, 41378, 41442);
UPDATE creature_template SET mechanic_immune_mask = 801849343 WHERE entry IN  (43296, 41570, 41378, 41442);