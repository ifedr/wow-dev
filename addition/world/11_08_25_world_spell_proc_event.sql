DELETE FROM `spell_proc_event` WHERE `entry` = 75171;
DELETE FROM `spell_proc_event` WHERE `entry` = 75177;

INSERT INTO `spell_proc_event` (`entry`, `SchoolMask`, `SpellFamilyName`, `SpellFamilyMask0`, `SpellFamilyMask1`, `SpellFamilyMask2`, `procFlags`, `procEx`, `ppmRate`, `CustomChance`, `Cooldown`) VALUES (75171, 0, 0, 0, 0, 0, 0, 0, 0, 0, 60);
INSERT INTO `spell_proc_event` (`entry`, `SchoolMask`, `SpellFamilyName`, `SpellFamilyMask0`, `SpellFamilyMask1`, `SpellFamilyMask2`, `procFlags`, `procEx`, `ppmRate`, `CustomChance`, `Cooldown`) VALUES (75177, 0, 0, 0, 0, 0, 0, 0, 0, 0, 60);
