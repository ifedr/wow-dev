DELETE FROM `creature` WHERE `id` in (41378,41442,41570,42166,42178,42179,42180,43296);

-- Maloriak is a giant Dragonkin, and a boss in Blackwing Descent.
INSERT INTO `creature` (`id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `DeathState`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) VALUES (41378, 669, 3, 1, 0, 0, -106.158, -480.57, 73.4549, 1.54218, 604800, 0, 0, 23207146, 0, 0, 0, 0, 0, 0);

-- Atramedes is a blind dragon, and a boss in Blackwing Descent.
INSERT INTO `creature` (`id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `DeathState`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) VALUES (41442, 669, 3, 1, 0, 0, 198.175, -224.467, 75.4534, 3.18759, 604800, 0, 0, 32610030, 0, 0, 0, 0, 0, 0);

-- Magmaw is a giant lava worm, and one of the first bosses in Blackwing Descent.
INSERT INTO `creature` (`id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `DeathState`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) VALUES (41570, 669, 3, 1, 0, 0, -325.247, -34.8583, 211.343, 4.63664, 604800, 0, 0, 31426324, 0, 0, 0, 0, 0, 0);

-- Arcanotron is one member of the Omnotron Defense System, a council-style encounter in Blackwing Descent.
INSERT INTO `creature` (`id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `DeathState`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) VALUES (42166, 669, 1, 1, 0, 0, -343.418, -411.733, 214.013, 1.52646, 604800, 0, 0, 30217622, 0, 0, 0, 0, 0, 0);

-- Magmatron is one member of the Omnotron Defense System, a council-style encounter in Blackwing Descent.
INSERT INTO `creature` (`id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `DeathState`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) VALUES (42178, 669, 1, 1, 0, 0, -318.217, -412.241, 214.04, 1.63249, 604800, 0, 0, 30217622, 0, 0, 0, 0, 0, 0);

-- Electron is one member of the Omnotron Defense System, a council-style encounter in Blackwing Descent.
INSERT INTO `creature` (`id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `DeathState`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) VALUES (42179, 669, 1, 1, 0, 0, -330.779, -412.499, 213.875, 1.58144, 604800, 0, 0, 30217622, 0, 0, 0, 0, 0, 0);

-- Toxitron is one member of the Omnotron Defense System, a council-style encounter in Blackwing Descent.
INSERT INTO `creature` (`id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `DeathState`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) VALUES (42180, 669, 1, 1, 0, 0, -308.786, -412.743, 214.002, 1.6482, 604800, 0, 0, 30217622, 0, 0, 0, 0, 0, 0);

-- Chimaeron is an altered hydra and one of the bosses in Blackwing Descent.
INSERT INTO `creature` (`id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `DeathState`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) VALUES (43296, 669, 3, 1, 0, 0, -107.928, 30.1186, 72.3528, 4.82907, 604800, 0, 0, 32610030, 0, 0, 0, 0, 0, 0);