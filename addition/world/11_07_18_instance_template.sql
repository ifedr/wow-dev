UPDATE `instance_template` SET `script` = 'instance_blackrock_caverns' WHERE `instance_template`.`map` = 645;

UPDATE `instance_template` SET `script` = 'instance_deadmines' WHERE `instance_template`.`map` = 36;

UPDATE `instance_template` SET `script` = 'instance_shadowfang' WHERE `instance_template`.`map` = 33;

-- For cancel this update use something like this:

-- UPDATE `instance_template` SET `script` = '' WHERE `instance_template`.`map` = 645;