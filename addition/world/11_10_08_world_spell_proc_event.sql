-- Priest: Shadow Orbs
DELETE FROM spell_proc_event WHERE entry = 95740;
INSERT INTO spell_proc_event VALUES 
(95740, 0, 6, 0x00008000, 0, 0x00000440, 0x00050000, 0, 0, 10, 0);

-- Priest: Harnessed Shadows talent
DELETE FROM spell_proc_event WHERE entry IN (33191, 78228);
INSERT INTO spell_proc_event VALUES 
(33191, 0, 0, 0, 0, 0, 0x00100000, 0x2, 0, 100, 0), 
(78228, 0, 0, 0, 0, 0, 0x00100000, 0x2, 0, 100, 0);
