DELETE FROM `spell_proc_event` WHERE `entry` = 12322;
DELETE FROM `spell_proc_event` WHERE `entry` = 85741;
DELETE FROM `spell_proc_event` WHERE `entry` = 85742;

INSERT INTO `spell_proc_event` (`entry`, `SchoolMask`, `SpellFamilyName`, `SpellFamilyMask0`, `SpellFamilyMask1`, `SpellFamilyMask2`, `procFlags`, `procEx`, `ppmRate`, `CustomChance`, `Cooldown`) VALUES (12322, 0, 0, 33554432, 1536, 0, 0, 0, 3, 0, 0);
INSERT INTO `spell_proc_event` (`entry`, `SchoolMask`, `SpellFamilyName`, `SpellFamilyMask0`, `SpellFamilyMask1`, `SpellFamilyMask2`, `procFlags`, `procEx`, `ppmRate`, `CustomChance`, `Cooldown`) VALUES (85741, 0, 0, 33554432, 1536, 0, 0, 0, 3, 0, 0);
INSERT INTO `spell_proc_event` (`entry`, `SchoolMask`, `SpellFamilyName`, `SpellFamilyMask0`, `SpellFamilyMask1`, `SpellFamilyMask2`, `procFlags`, `procEx`, `ppmRate`, `CustomChance`, `Cooldown`) VALUES (85742, 0, 0, 33554432, 1536, 0, 0, 0, 3, 0, 0);
