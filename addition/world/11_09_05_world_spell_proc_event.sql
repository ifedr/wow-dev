DELETE FROM `spell_proc_event` WHERE `entry` = 51632;
DELETE FROM `spell_proc_event` WHERE `entry` = 91023;

INSERT INTO `spell_proc_event` (`entry`, `SchoolMask`, `SpellFamilyName`, `SpellFamilyMask0`, `SpellFamilyMask1`, `SpellFamilyMask2`, `procFlags`, `procEx`, `ppmRate`, `CustomChance`, `Cooldown`) VALUES (51632, 0, 8, 1792, 0, 0, 4112, 0, 0, 0, 0);
INSERT INTO `spell_proc_event` (`entry`, `SchoolMask`, `SpellFamilyName`, `SpellFamilyMask0`, `SpellFamilyMask1`, `SpellFamilyMask2`, `procFlags`, `procEx`, `ppmRate`, `CustomChance`, `Cooldown`) VALUES (91023, 0, 8, 1792, 0, 0, 4112, 0, 0, 0, 0);
