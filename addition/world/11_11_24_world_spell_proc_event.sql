-- Mage: Reactive Barrier talent
DELETE FROM `spell_proc_event` WHERE `entry` IN (86303, 86304);
INSERT INTO `spell_proc_event` VALUES 
(86303, 0, 0, 0x00000000, 0x00000000, 0x00000000, 0x00100000, 0, 0, 50, 0),
(86304, 0, 0, 0x00000000, 0x00000000, 0x00000000, 0x00100000, 0, 0, 100, 0);