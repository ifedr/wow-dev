-- Mage: Nether Vortex talent
DELETE FROM `spell_proc_event` WHERE `entry` IN (86181, 86209);
INSERT INTO `spell_proc_event` VALUES 
(86181, 64, 3, 0x20000000, 0, 0, 0x10000, 0, 0, 50, 0),
(86209, 64, 3, 0x20000000, 0, 0, 0x10000, 0, 0, 100, 0);
