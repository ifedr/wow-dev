/* Forbearance */
DELETE FROM `spell_linked_spell` WHERE `spell_trigger`=633 AND `spell_effect`=25771 AND `type`=0 LIMIT 1;
DELETE FROM `spell_linked_spell` WHERE `spell_trigger`=642 AND `spell_effect`=25771 AND `type`=0 LIMIT 1;
DELETE FROM `spell_linked_spell` WHERE `spell_trigger`=1022 AND `spell_effect`=25771 AND `type`=0 LIMIT 1;
INSERT INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (1022, 25771, 0, 'Forbearance');
INSERT INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (633, 25771, 0, 'Forbearance');
INSERT INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (642, 25771, 0, 'Forbearance');
