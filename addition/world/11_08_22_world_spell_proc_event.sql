DELETE FROM `spell_proc_event` WHERE `entry` = 16952;
DELETE FROM `spell_proc_event` WHERE `entry` = 16954;
DELETE FROM `spell_proc_event` WHERE `entry` = 48483;
DELETE FROM `spell_proc_event` WHERE `entry` = 48484;
DELETE FROM `spell_proc_event` WHERE `entry` = 48532;
DELETE FROM `spell_proc_event` WHERE `entry` = 80552;
DELETE FROM `spell_proc_event` WHERE `entry` = 80553;

INSERT INTO `spell_proc_event` (`entry`, `SchoolMask`, `SpellFamilyName`, `SpellFamilyMask0`, `SpellFamilyMask1`, `SpellFamilyMask2`, `procFlags`, `procEx`, `ppmRate`, `CustomChance`, `Cooldown`) VALUES (16952, 0, 7, 233472, 1024, 262144, 87376, 2, 0, 0, 0);
INSERT INTO `spell_proc_event` (`entry`, `SchoolMask`, `SpellFamilyName`, `SpellFamilyMask0`, `SpellFamilyMask1`, `SpellFamilyMask2`, `procFlags`, `procEx`, `ppmRate`, `CustomChance`, `Cooldown`) VALUES (16954, 0, 7, 233472, 1024, 262144, 87376, 2, 0, 0, 0);

INSERT INTO `spell_proc_event` (`entry`, `SchoolMask`, `SpellFamilyName`, `SpellFamilyMask0`, `SpellFamilyMask1`, `SpellFamilyMask2`, `procFlags`, `procEx`, `ppmRate`, `CustomChance`, `Cooldown`) VALUES (48483, 0, 7, 71680, 1088, 0, 0, 0, 0, 0, 0);
INSERT INTO `spell_proc_event` (`entry`, `SchoolMask`, `SpellFamilyName`, `SpellFamilyMask0`, `SpellFamilyMask1`, `SpellFamilyMask2`, `procFlags`, `procEx`, `ppmRate`, `CustomChance`, `Cooldown`) VALUES (48484, 0, 7, 71680, 1088, 0, 0, 0, 0, 0, 0);

INSERT INTO `spell_proc_event` (`entry`, `SpellFamilyName`, `Cooldown`) VALUES (48532, 7, 3);
INSERT INTO `spell_proc_event` (`entry`, `SpellFamilyName`, `Cooldown`) VALUES (80552, 7, 3);
INSERT INTO `spell_proc_event` (`entry`, `SpellFamilyName`, `Cooldown`) VALUES (80553, 7, 3);