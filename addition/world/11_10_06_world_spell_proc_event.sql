DELETE FROM `spell_proc_event` WHERE `entry` = 46913;
DELETE FROM `spell_proc_event` WHERE `entry` = 46914;
DELETE FROM `spell_proc_event` WHERE `entry` = 46915;

INSERT INTO `spell_proc_event` (`entry`, `SchoolMask`, `SpellFamilyName`, `SpellFamilyMask0`, `SpellFamilyMask1`, `SpellFamilyMask2`, `procFlags`, `procEx`, `ppmRate`, `CustomChance`, `Cooldown`) VALUES (46913, 0, 4, 0, 1024, 0, 0, 0, 0, 0, 0);
INSERT INTO `spell_proc_event` (`entry`, `SchoolMask`, `SpellFamilyName`, `SpellFamilyMask0`, `SpellFamilyMask1`, `SpellFamilyMask2`, `procFlags`, `procEx`, `ppmRate`, `CustomChance`, `Cooldown`) VALUES (46914, 0, 4, 0, 1024, 0, 0, 0, 0, 0, 0);
INSERT INTO `spell_proc_event` (`entry`, `SchoolMask`, `SpellFamilyName`, `SpellFamilyMask0`, `SpellFamilyMask1`, `SpellFamilyMask2`, `procFlags`, `procEx`, `ppmRate`, `CustomChance`, `Cooldown`) VALUES (46915, 0, 4, 0, 1024, 0, 0, 0, 0, 0, 0);

UPDATE `spell_proc_event` SET `CustomChance`=3 WHERE `entry`=16864 LIMIT 1;
