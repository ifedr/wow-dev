UPDATE `quest_template` SET `ReqCreatureOrGOId1`='39148', `ReqCreatureOrGOCount1`='1',`StartScript`='25094' WHERE (`entry`='25094');
DELETE FROM `quest_start_scripts` WHERE `id` = 25094;
INSERT INTO `quest_start_scripts` (`id`, `delay`, `command`, `datalong`, `datalong2`, `x`, `y`, `z`, `o`) VALUES ('25094', '0', '10', '39148', '180000', '-7139.273926', '-3787.143066', '9.004193', '6.027874');

UPDATE `quest_template` SET `ReqCreatureOrGOId1`='39075', `ReqCreatureOrGOCount1`='1',`StartScript`='25067' WHERE (`entry`='25067');
DELETE FROM `quest_start_scripts` WHERE `id` = 25067;
INSERT INTO `quest_start_scripts` (`id`, `delay`, `command`, `datalong`, `datalong2`, `x`, `y`, `z`, `o`) VALUES ('25067', '0', '10', '39075', '180000', '-7139.273926', '-3787.143066', '9.004193', '6.027874');

UPDATE `quest_template` SET `ReqCreatureOrGOId1`='39149', `ReqCreatureOrGOCount1`='1',`StartScript`='25095' WHERE (`entry`='25095');
DELETE FROM `quest_start_scripts` WHERE `id` = 25095;
INSERT INTO `quest_start_scripts` (`id`, `delay`, `command`, `datalong`, `datalong2`, `x`, `y`, `z`, `o`) VALUES ('25095', '0', '10', '39149', '180000', '-7139.273926', '-3787.143066', '9.004193', '6.027874');

UPDATE `quest_template` SET `ReqCreatureOrGOId1`='40542', `ReqCreatureOrGOCount1`='1',`StartScript`='25513' WHERE (`entry`='25513');
DELETE FROM `quest_start_scripts` WHERE `id` = 25513;
INSERT INTO `quest_start_scripts` (`id`, `delay`, `command`, `datalong`, `datalong2`, `x`, `y`, `z`, `o`) VALUES ('25513', '0', '10', '40542', '180000', '-7139.273926', '-3787.143066', '9.004193', '6.027874');

UPDATE `quest_template` SET `ReqCreatureOrGOId1`='40547', `ReqCreatureOrGOCount1`='1',`StartScript`='25591' WHERE (`entry`='25591');
DELETE FROM `quest_start_scripts` WHERE `id` = 25591;
INSERT INTO `quest_start_scripts` (`id`, `delay`, `command`, `datalong`, `datalong2`, `x`, `y`, `z`, `o`) VALUES ('25591', '0', '10', '40547', '180000', '-7139.273926', '-3787.143066', '9.004193', '6.027874');

-- Kelsey Steelspark
UPDATE `creature_template` SET `minlevel`='49', `maxlevel`='49',`faction_A`='413',`faction_H`='413',`mindmg`='80', `maxdmg`='109', `attackpower`='197', `dmg_multiplier`='1.5', `baseattacktime`='2200', `minrangedmg`='58', `maxrangedmg`='84', `rangedattackpower`='20' WHERE (`entry`='40547');
SET @ENTRY := 40547;
SET @SOURCETYPE := 0;

DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=@SOURCETYPE;
UPDATE creature_template SET AIName="SmartAI" WHERE entry=@ENTRY LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES 
(@ENTRY,@SOURCETYPE,0,0,0,0,100,0,1000,1000,6000,8000,11,76732,4,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"");

-- Megs Dreadshredder
UPDATE `creature_template` SET `minlevel`='49', `maxlevel`='49',`faction_A`='413',`faction_H`='413',`mindmg`='80', `maxdmg`='109', `attackpower`='197', `dmg_multiplier`='1.5', `baseattacktime`='2200', `minrangedmg`='58', `maxrangedmg`='84', `rangedattackpower`='20' WHERE (`entry`='40542');
SET @ENTRY := 40542;
SET @SOURCETYPE := 0;

DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=@SOURCETYPE;
UPDATE creature_template SET AIName="SmartAI" WHERE entry=@ENTRY LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES 
(@ENTRY,@SOURCETYPE,0,0,4,0,100,0,0,0,0,0,11,76746,0,0,0,0,0,0,0,0,0,0.0,0.0,0.0,0.0,""),
(@ENTRY,@SOURCETYPE,1,0,0,0,100,0,2000,3000,8000,10000,11,76735,4,0,0,0,0,0,0,0,0,0.0,0.0,0.0,0.0,"Type a script description here.");

-- Sarinexx
UPDATE `creature_template` SET `minlevel`='49', `maxlevel`='49',`faction_A`='413',`faction_H`='413',`mindmg`='80', `maxdmg`='109', `attackpower`='197', `dmg_multiplier`='1.5', `baseattacktime`='2200', `minrangedmg`='58', `maxrangedmg`='84', `rangedattackpower`='20' WHERE (`entry`='39149');
SET @ENTRY := 39149;
SET @SOURCETYPE := 0;

DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=@SOURCETYPE;
UPDATE creature_template SET AIName="SmartAI" WHERE entry=@ENTRY LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES 
(@ENTRY,@SOURCETYPE,0,0,0,0,100,0,1000,2000,2000,3000,11,73863,4,0,0,0,0,0,0,0,0,0.0,0.0,0.0,0.0,"Type a script description here.");

-- The Ginormus
UPDATE `creature_template` SET `minlevel`='49', `maxlevel`='49',`faction_A`='413',`faction_H`='413',`mindmg`='80', `maxdmg`='109', `attackpower`='197', `dmg_multiplier`='1.5', `baseattacktime`='2200', `minrangedmg`='58', `maxrangedmg`='84', `rangedattackpower`='20' WHERE (`entry`='39075');
SET @ENTRY := 39075;
SET @SOURCETYPE := 0;

DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=@SOURCETYPE;
UPDATE creature_template SET AIName="SmartAI" WHERE entry=@ENTRY LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES 
(@ENTRY,@SOURCETYPE,0,0,4,0,100,0,0,0,0,0,11,73854,0,0,0,0,0,0,0,0,0,0.0,0.0,0.0,0.0,"Type a script description here."),
(@ENTRY,@SOURCETYPE,1,0,0,0,100,0,2000,3000,8000,10000,11,73855,4,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Type a script description here.");

-- Zumonga
UPDATE `creature_template` SET `minlevel`='49', `maxlevel`='49',`faction_A`='413',`faction_H`='413',`mindmg`='80', `maxdmg`='109', `attackpower`='197', `dmg_multiplier`='1.5', `baseattacktime`='2200', `minrangedmg`='58', `maxrangedmg`='84', `rangedattackpower`='20' WHERE (`entry`='39148');
SET @ENTRY := 39148;
SET @SOURCETYPE := 0;

DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=@SOURCETYPE;
UPDATE creature_template SET AIName="SmartAI" WHERE entry=@ENTRY LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES 
(@ENTRY,@SOURCETYPE,0,0,0,0,100,0,3000,4000,5000,8000,11,73859,4,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Type a script description here."),
(@ENTRY,@SOURCETYPE,1,0,0,0,100,0,0,1000,14000,15000,11,73858,4,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Type a script description here.");

UPDATE `quest_template` SET `ReqCreatureOrGOId1`=31271, `ReqCreatureOrGOCount1`=1 WHERE  `entry`=13217;
UPDATE `quest_template` SET `ReqCreatureOrGOId1`=31195, `ReqCreatureOrGOId2`=31193, `ReqCreatureOrGOId3`=31191, `ReqCreatureOrGOId4`=31192, `ReqCreatureOrGOCount1`=1, `ReqCreatureOrGOCount2`=1, `ReqCreatureOrGOCount3`=1, `ReqCreatureOrGOCount4`=1 WHERE  `entry`=13214;
UPDATE `quest_template` SET `ReqCreatureOrGOId1`=14688, `ReqCreatureOrGOCount1`=1 WHERE  `entry`=13219;
UPDATE `quest_template` SET `ReqCreatureOrGOId1`=31222, `ReqCreatureOrGOCount1`=1 WHERE  `entry`=13215;
UPDATE `quest_template` SET `ReqCreatureOrGOId1`=31277, `ReqCreatureOrGOCount1`=1 WHERE  `entry`=13218;
UPDATE `quest_template` SET `ReqCreatureOrGOId1`=31242, `ReqCreatureOrGOCount1`=1 WHERE  `entry`=13216;

UPDATE creature_template SET AIName="SmartAI" WHERE entry=31135;

DELETE FROM `smart_scripts` WHERE `entryorguid`=31135;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (31135,0,0,0,19,0,100,0,13217,0,0,0,12,31271,1,255000,0,0,0,8,0,0,0,8164.46,3474.56,626.634,0.61,"Spawn npc - Icecrown arena 1");
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (31135,0,1,0,19,0,100,0,13214,0,0,0,12,31195,1,255000,0,0,0,8,0,0,0,8164.46,3474.56,626.634,0.61,"Spawn npc - Icecrown arena 2");
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (31135,0,2,0,19,0,100,0,13214,0,0,0,12,31193,1,255000,0,0,0,8,0,0,0,8164.46,3474.56,626.634,0.61,"Spawn npc - Icecrown arena 3");
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (31135,0,3,0,19,0,100,0,13214,0,0,0,12,31191,1,255000,0,0,0,8,0,0,0,8164.46,3474.56,626.634,0.61,"Spawn npc - Icecrown arena 4");
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (31135,0,4,0,19,0,100,0,13214,0,0,0,12,31192,1,255000,0,0,0,8,0,0,0,8164.46,3474.56,626.634,0.61,"Spawn npc - Icecrown arena 5");
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (31135,0,5,0,19,0,100,0,13219,0,0,0,12,14688,1,255000,0,0,0,8,0,0,0,8164.46,3474.56,626.634,0.61,"Spawn npc - Icecrown arena 6");
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (31135,0,6,0,19,0,100,0,13215,0,0,0,12,31222,1,255000,0,0,0,8,0,0,0,8164.46,3474.56,626.634,0.61,"Spawn npc - Icecrown arena 7");
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (31135,0,7,0,19,0,100,0,13218,0,0,0,12,31277,1,255000,0,0,0,8,0,0,0,8164.46,3474.56,626.634,0.61,"Spawn npc - Icecrown arena 8");
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES (31135,0,8,0,19,0,100,0,13216,0,0,0,12,31242,1,255000,0,0,0,8,0,0,0,8164.46,3474.56,626.634,0.61,"Spawn npc - Icecrown arena 9");
