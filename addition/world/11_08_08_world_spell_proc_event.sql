DELETE FROM `spell_proc_event` WHERE `entry` = 12289;
DELETE FROM `spell_proc_event` WHERE `entry` = 12668;
INSERT INTO `spell_proc_event` (`entry`, `SchoolMask`, `SpellFamilyName`, `SpellFamilyMask0`, `SpellFamilyMask1`, `SpellFamilyMask2`, `procFlags`, `procEx`, `ppmRate`, `CustomChance`, `Cooldown`) VALUES (12289, 0, 4, 2, 0, 0, 0, 0, 0, 0, 60);
INSERT INTO `spell_proc_event` (`entry`, `SchoolMask`, `SpellFamilyName`, `SpellFamilyMask0`, `SpellFamilyMask1`, `SpellFamilyMask2`, `procFlags`, `procEx`, `ppmRate`, `CustomChance`, `Cooldown`) VALUES (12668, 0, 4, 2, 0, 0, 0, 0, 0, 0, 30);
