CREATE TABLE `remote_task` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `command` varchar(255) NOT NULL,
  `response` varchar(255) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `execute_count` int(11) NOT NULL,
  `last_updated` datetime NOT NULL,
  `realm` varchar(255) DEFAULT NULL,
  `status` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `remote_task_status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8