DELETE FROM `spell_proc_event` WHERE `entry` = 61216;
DELETE FROM `spell_proc_event` WHERE `entry` = 61221;

INSERT INTO `spell_proc_event` (`entry`, `SchoolMask`, `SpellFamilyName`, `SpellFamilyMask0`, `SpellFamilyMask1`, `SpellFamilyMask2`, `procFlags`, `procEx`, `ppmRate`, `CustomChance`, `Cooldown`) VALUES (61216, 0, 4, 8, 0, 0, 4112, 0, 0, 100, 120);
INSERT INTO `spell_proc_event` (`entry`, `SchoolMask`, `SpellFamilyName`, `SpellFamilyMask0`, `SpellFamilyMask1`, `SpellFamilyMask2`, `procFlags`, `procEx`, `ppmRate`, `CustomChance`, `Cooldown`) VALUES (61221, 0, 4, 8, 0, 0, 4112, 0, 0, 100, 120);
