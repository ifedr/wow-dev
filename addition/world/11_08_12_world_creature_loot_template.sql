UPDATE creature_loot_template SET groupid=1 WHERE `entry` in (40586,40765,40788,42172,39665,39679,39698,39700,39705,39625,40177,40319,40484,39425,39428,39788,39587,39731,39732,39378,44577,43612,43614,44819,43438,43214,42188,42333,43878,43873,43875);

DELETE FROM creature_loot_template WHERE entry = 42172 and item = 52078;
INSERT INTO `creature_loot_template` (`entry`, `item`, `ChanceOrQuestChance`) VALUES ('42172', '52078', '25');

DELETE FROM creature_loot_template WHERE entry = 39705 and item = 52078;
INSERT INTO `creature_loot_template` (`entry`, `item`, `ChanceOrQuestChance`) VALUES ('39705', '52078', '25');

DELETE FROM creature_loot_template WHERE entry = 40484 and item = 52078;
INSERT INTO `creature_loot_template` (`entry`, `item`, `ChanceOrQuestChance`) VALUES ('40484', '52078', '25');

DELETE FROM creature_loot_template WHERE entry = 39378 and item = 52078;
INSERT INTO `creature_loot_template` (`entry`, `item`, `ChanceOrQuestChance`) VALUES ('39378', '52078', '25');

DELETE FROM creature_loot_template WHERE entry = 44819 and item = 52078;
INSERT INTO `creature_loot_template` (`entry`, `item`, `ChanceOrQuestChance`) VALUES ('44819', '52078', '25');

DELETE FROM creature_loot_template WHERE entry = 42333 and item = 52078;
INSERT INTO `creature_loot_template` (`entry`, `item`, `ChanceOrQuestChance`) VALUES ('42333', '52078', '25');

DELETE FROM creature_loot_template WHERE entry = 43875 and item = 52078;
INSERT INTO `creature_loot_template` (`entry`, `item`, `ChanceOrQuestChance`) VALUES ('43875', '52078', '25');


UPDATE creature_template SET lootid=39666 WHERE entry=39666;
DELETE FROM `creature_loot_template` WHERE entry = 39666;
INSERT INTO `creature_loot_template` VALUES (39666, 56313, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (39666, 56310, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (39666, 56312, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (39666, 56311, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (39666, 56314, 20, 1, 1, 1, 1);

UPDATE creature_template SET lootid=39680 WHERE entry=39680;
DELETE FROM `creature_loot_template` WHERE entry = 39680;
INSERT INTO `creature_loot_template` VALUES (39680, 56295, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (39680, 56298, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (39680, 56297, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (39680, 56296, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (39680, 56299, 20, 1, 1, 1, 1);

UPDATE creature_template SET lootid=39699 WHERE entry=39699;
DELETE FROM `creature_loot_template` WHERE entry = 39699;
INSERT INTO `creature_loot_template` VALUES (39699, 56300, 21, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (39699, 56303, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (39699, 56302, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (39699, 56304, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (39699, 56301, 19, 1, 1, 1, 1);

UPDATE creature_template SET lootid=39701 WHERE entry=39701;
DELETE FROM `creature_loot_template` WHERE entry = 39701;
INSERT INTO `creature_loot_template` VALUES (39701, 56307, 21, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (39701, 56306, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (39701, 56308, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (39701, 56305, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (39701, 56309, 19, 1, 1, 1, 1);

UPDATE creature_template SET lootid=39706 WHERE entry=39706;
DELETE FROM `creature_loot_template` WHERE entry = 39706;
INSERT INTO `creature_loot_template` VALUES (39706, 52078, 91, 1, 0, 1, 1);
INSERT INTO `creature_loot_template` VALUES (39706, 56320, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (39706, 56321, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (39706, 56318, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (39706, 56315, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (39706, 56319, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (39706, 56316, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (39706, 56322, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (39706, 56324, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (39706, 56323, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (39706, 56317, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (39706, 52506, 1.2, 1, 0, 1, 1);

UPDATE creature_template SET lootid=49262 WHERE entry=49262;
DELETE FROM `creature_loot_template` WHERE entry = 49262;
INSERT INTO `creature_loot_template` VALUES (49262, 56411, 21, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (49262, 56409, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (49262, 56407, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (49262, 56410, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (49262, 56408, 19, 1, 1, 1, 1);

UPDATE creature_template SET lootid=48714 WHERE entry=48714;
DELETE FROM `creature_loot_template` WHERE entry = 48714;
INSERT INTO `creature_loot_template` VALUES (48714, 56423, 21, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48714, 56425, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48714, 56424, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48714, 56426, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48714, 56422, 19, 1, 1, 1, 1);

UPDATE creature_template SET lootid=48902 WHERE entry=48902;
DELETE FROM `creature_loot_template` WHERE entry = 48902;
INSERT INTO `creature_loot_template` VALUES (48902, 57870, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48902, 57869, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48902, 57866, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48902, 57868, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48902, 57867, 20, 1, 1, 1, 1);

UPDATE creature_template SET lootid=48710 WHERE entry=48710;
DELETE FROM `creature_loot_template` WHERE entry = 48710;
INSERT INTO `creature_loot_template` VALUES (48710, 56414, 21, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48710, 56415, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48710, 56416, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48710, 56412, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48710, 56413, 19, 1, 1, 1, 1);

UPDATE creature_template SET lootid=48715 WHERE entry=48715;
DELETE FROM `creature_loot_template` WHERE entry = 48715;
INSERT INTO `creature_loot_template` VALUES (48715, 56418, 21, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48715, 56419, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48715, 56421, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48715, 56417, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48715, 56420, 19, 1, 1, 1, 1);

UPDATE creature_template SET lootid=48776 WHERE entry=48776;
DELETE FROM `creature_loot_template` WHERE entry = 48776;
INSERT INTO `creature_loot_template` VALUES (48776, 57873, 21, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48776, 57875, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48776, 57872, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48776, 57871, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48776, 57874, 19, 1, 1, 1, 1);

UPDATE creature_template SET lootid=48815 WHERE entry=48815;
DELETE FROM `creature_loot_template` WHERE entry = 48815;
INSERT INTO `creature_loot_template` VALUES (48815, 52078, 92, 1, 0, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48815, 56429, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48815, 56428, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48815, 56434, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48815, 56430, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48815, 56436, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48815, 56433, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48815, 56435, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48815, 56431, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48815, 56427, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48815, 56432, 10, 1, 1, 1, 1);

UPDATE `creature_template` SET `difficulty_entry_1`='49642' WHERE (`entry`='43438');
UPDATE creature_template SET lootid=49642 WHERE entry=49642;
DELETE FROM `creature_loot_template` WHERE entry = 49642;
INSERT INTO `creature_loot_template` VALUES (49642, 56328, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (49642, 56332, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (49642, 56329, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (49642, 56330, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (49642, 56331, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (49642, 52506, 1.8, 1, 0, 1, 1);

UPDATE `creature_template` SET `difficulty_entry_1`='49538' WHERE (`entry`='43214');
UPDATE creature_template SET lootid=49538 WHERE entry=49538;
DELETE FROM `creature_loot_template` WHERE entry = 49538;
INSERT INTO `creature_loot_template` VALUES (49538, 56337, 21, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (49538, 56336, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (49538, 56333, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (49538, 56334, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (49538, 56335, 19, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (49538, 63043, 0.9, 1, 0, 1, 1);

UPDATE `creature_template` SET `difficulty_entry_1`='49654' WHERE (`entry`='42188');
UPDATE creature_template SET lootid=49654 WHERE entry=49654;
DELETE FROM `creature_loot_template` WHERE entry = 49654;
INSERT INTO `creature_loot_template` VALUES (49654, 56339, 21, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (49654, 56338, 21, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (49654, 56340, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (49654, 56342, 19, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (49654, 56341, 19, 1, 1, 1, 1);

UPDATE `creature_template` SET `difficulty_entry_1`='49624' WHERE (`entry`='42333');
UPDATE creature_template SET lootid=49624 WHERE entry=49624;
DELETE FROM `creature_loot_template` WHERE entry = 49624;
INSERT INTO `creature_loot_template` VALUES (49624, 52078, 87, 1, 0, 1, 1);
INSERT INTO `creature_loot_template` VALUES (49624, 56350, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (49624, 56351, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (49624, 56347, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (49624, 56345, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (49624, 56349, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (49624, 56343, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (49624, 56346, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (49624, 56352, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (49624, 56348, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (49624, 56344, 10, 1, 1, 1, 1);

UPDATE creature_template SET lootid=48932 WHERE entry=48932;
DELETE FROM `creature_loot_template` WHERE entry = 48932;
INSERT INTO `creature_loot_template` VALUES (48932, 56383, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48932, 56380, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48932, 56382, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48932, 56381, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48932, 56379, 20, 1, 1, 1, 1);

UPDATE creature_template SET lootid=48951 WHERE entry=48951;
DELETE FROM `creature_loot_template` WHERE entry = 48951;
INSERT INTO `creature_loot_template` VALUES (48951, 56386, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48951, 56388, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48951, 56387, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48951, 56385, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48951, 56384, 20, 1, 1, 1, 1);

UPDATE creature_template SET lootid=49045 WHERE entry=49045;
DELETE FROM `creature_loot_template` WHERE entry = 49045;
INSERT INTO `creature_loot_template` VALUES (49045, 56391, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (49045, 56390, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (49045, 56389, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (49045, 56392, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (49045, 56393, 20, 1, 1, 1, 1);

UPDATE creature_template SET lootid=51088 WHERE entry=51088;
DELETE FROM `creature_loot_template` WHERE entry = 51088;
INSERT INTO `creature_loot_template` VALUES (51088, 52078, 91, 1, 0, 1, 1);
INSERT INTO `creature_loot_template` VALUES (51088, 56396, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (51088, 56400, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (51088, 56397, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (51088, 56401, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (51088, 56402, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (51088, 56395, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (51088, 56398, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (51088, 56394, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (51088, 56399, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (51088, 56403, 10, 1, 1, 1, 1);

UPDATE creature_template SET lootid=48337 WHERE entry=48337;
DELETE FROM `creature_loot_template` WHERE entry = 48337;
INSERT INTO `creature_loot_template` VALUES (48337, 56441, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48337, 56440, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48337, 56444, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48337, 56442, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48337, 56443, 20, 1, 1, 1, 1);

UPDATE creature_template SET lootid=48702 WHERE entry=48702;
DELETE FROM `creature_loot_template` WHERE entry = 48702;
INSERT INTO `creature_loot_template` VALUES (48702, 56446, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48702, 56445, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48702, 56449, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48702, 56448, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48702, 56447, 20, 1, 1, 1, 1);

UPDATE creature_template SET lootid=48784 WHERE entry=48784;
DELETE FROM `creature_loot_template` WHERE entry = 48784;
INSERT INTO `creature_loot_template` VALUES (48784, 56452, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48784, 56454, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48784, 56451, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48784, 56450, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48784, 56453, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48784, 66927, 12, 1, 0, 1, 1);

UPDATE creature_template SET lootid=48822 WHERE entry=48822;
DELETE FROM `creature_loot_template` WHERE entry = 48822;
INSERT INTO `creature_loot_template` VALUES (48822, 52078, 91, 1, 0, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48822, 56462, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48822, 56456, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48822, 56458, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48822, 56457, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48822, 56463, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48822, 56455, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48822, 56460, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48822, 56464, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48822, 56459, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (48822, 56461, 10, 1, 1, 1, 1);

UPDATE `creature_template` SET `difficulty_entry_1`='43879' WHERE (`entry`='43878');
UPDATE creature_template SET lootid=43879 WHERE entry=43879;
DELETE FROM `creature_loot_template` WHERE entry = 43879;
INSERT INTO `creature_loot_template` VALUES (43879, 56358, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (43879, 56360, 21, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (43879, 56359, 21, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (43879, 56357, 19, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (43879, 56356, 19, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (43879, 65660, 9, 1, 0, 1, 1);

UPDATE `creature_template` SET `difficulty_entry_1`='43874' WHERE (`entry`='43873');
UPDATE creature_template SET lootid=43874 WHERE entry=43874;
DELETE FROM `creature_loot_template` WHERE entry = 43874;
INSERT INTO `creature_loot_template` VALUES (43874, 56362, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (43874, 56363, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (43874, 56365, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (43874, 56361, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (43874, 56354, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (43874, 63040, 0.8, 1, 0, 1, 1);

UPDATE `creature_template` SET `difficulty_entry_1`='43876' WHERE (`entry`='43875');
UPDATE creature_template SET lootid=43876 WHERE entry=43876;
DELETE FROM `creature_loot_template` WHERE entry = 43876;
INSERT INTO `creature_loot_template` VALUES (43876, 52078, 91, 1, 0, 1, 1);
INSERT INTO `creature_loot_template` VALUES (43876, 56373, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (43876, 56372, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (43876, 56371, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (43876, 56374, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (43876, 56375, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (43876, 56367, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (43876, 56368, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (43876, 56369, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (43876, 56366, 10, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (43876, 56370, 5, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (43876, 56376, 5, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (43876, 52506, 0.3, 1, 0, 1, 1);

UPDATE `creature_template` SET `difficulty_entry_1`='49080' WHERE (`entry`='40586');
UPDATE creature_template SET lootid=49080 WHERE entry=49080;
DELETE FROM `creature_loot_template` WHERE entry = 49080;
INSERT INTO `creature_loot_template` VALUES (49080, 56269, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (49080, 56266, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (49080, 56267, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (49080, 56268, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (49080, 56270, 20, 1, 1, 1, 1);

UPDATE `creature_template` SET `difficulty_entry_1`='49064' WHERE (`entry`='40765');
UPDATE creature_template SET lootid=49064 WHERE entry=49064;
DELETE FROM `creature_loot_template` WHERE entry = 49064;
INSERT INTO `creature_loot_template` VALUES (49064, 56273, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (49064, 56274, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (49064, 56272, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (49064, 56275, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (49064, 56271, 20, 1, 1, 1, 1);

UPDATE `creature_template` SET `difficulty_entry_1`='49082' WHERE (`entry`='40788');
UPDATE creature_template SET lootid=49082 WHERE entry=49082;
DELETE FROM `creature_loot_template` WHERE entry = 49082;
INSERT INTO `creature_loot_template` VALUES (49082, 56278, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (49082, 56276, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (49082, 56279, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (49082, 56280, 20, 1, 1, 1, 1);
INSERT INTO `creature_loot_template` VALUES (49082, 56277, 20, 1, 1, 1, 1);


