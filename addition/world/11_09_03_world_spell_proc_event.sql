DELETE FROM `spell_proc_event` WHERE `entry` = 63373;
DELETE FROM `spell_proc_event` WHERE `entry` = 63374;

INSERT INTO `spell_proc_event` (`entry`, `SchoolMask`, `SpellFamilyName`, `SpellFamilyMask0`, `SpellFamilyMask1`, `SpellFamilyMask2`, `procFlags`, `procEx`, `ppmRate`, `CustomChance`, `Cooldown`) VALUES (63373, 0, 11, 2147483648, 0, 0, 65536, 0, 0, 50, 0);
INSERT INTO `spell_proc_event` (`entry`, `SchoolMask`, `SpellFamilyName`, `SpellFamilyMask0`, `SpellFamilyMask1`, `SpellFamilyMask2`, `procFlags`, `procEx`, `ppmRate`, `CustomChance`, `Cooldown`) VALUES (63374, 0, 11, 2147483648, 0, 0, 65536, 0, 0, 100, 0);
