-- Mage: Cauterize talent
DELETE FROM `spell_script_names` WHERE `spell_id` IN (86948, 86949);
INSERT INTO `spell_script_names` VALUES 
('86948', 'spell_mage_cauterize'),
('86949', 'spell_mage_cauterize');
