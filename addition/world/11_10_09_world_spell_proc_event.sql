DELETE FROM spell_proc_event WHERE entry = 53556;
DELETE FROM spell_proc_event WHERE entry = 53557;

INSERT INTO `spell_proc_event` (`entry`, `SchoolMask`, `SpellFamilyName`, `SpellFamilyMask0`, `SpellFamilyMask1`, `SpellFamilyMask2`, `procFlags`, `procEx`, `ppmRate`, `CustomChance`, `Cooldown`) VALUES (53556, 0, 10, 8388608, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO `spell_proc_event` (`entry`, `SchoolMask`, `SpellFamilyName`, `SpellFamilyMask0`, `SpellFamilyMask1`, `SpellFamilyMask2`, `procFlags`, `procEx`, `ppmRate`, `CustomChance`, `Cooldown`) VALUES (53557, 0, 10, 8388608, 0, 0, 0, 0, 0, 0, 0);
