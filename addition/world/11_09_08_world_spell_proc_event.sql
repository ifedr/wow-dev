DELETE FROM `spell_proc_event` WHERE `entry` = 77655;
DELETE FROM `spell_proc_event` WHERE `entry` = 77656;
DELETE FROM `spell_proc_event` WHERE `entry` = 77657;

INSERT INTO `spell_proc_event` (`entry`, `SchoolMask`, `SpellFamilyName`, `SpellFamilyMask0`, `SpellFamilyMask1`, `SpellFamilyMask2`, `procFlags`, `procEx`, `ppmRate`, `CustomChance`, `Cooldown`) VALUES (77655, 0, 0, 1073741824, 0, 0, 65536, 0, 0, 100, 0);
INSERT INTO `spell_proc_event` (`entry`, `SchoolMask`, `SpellFamilyName`, `SpellFamilyMask0`, `SpellFamilyMask1`, `SpellFamilyMask2`, `procFlags`, `procEx`, `ppmRate`, `CustomChance`, `Cooldown`) VALUES (77656, 0, 0, 1073741824, 0, 0, 65536, 0, 0, 100, 0);
INSERT INTO `spell_proc_event` (`entry`, `SchoolMask`, `SpellFamilyName`, `SpellFamilyMask0`, `SpellFamilyMask1`, `SpellFamilyMask2`, `procFlags`, `procEx`, `ppmRate`, `CustomChance`, `Cooldown`) VALUES (77657, 0, 0, 1073741824, 0, 0, 65536, 0, 0, 100, 0);
