UPDATE `creature_template` SET `lootid` = 39708 WHERE `entry`= 39709;
UPDATE `creature_template` SET `lootid` = 39978 WHERE `entry`= 39979;
UPDATE `creature_template` SET `lootid` = 39980 WHERE `entry`= 39981;
UPDATE `creature_template` SET `lootid` = 39982 WHERE `entry`= 39983;
UPDATE `creature_template` SET `lootid` = 39985 WHERE `entry`= 39986;
UPDATE `creature_template` SET `lootid` = 39987 WHERE `entry`= 39988;
UPDATE `creature_template` SET `lootid` = 39990 WHERE `entry`= 39991;
UPDATE `creature_template` SET `lootid` = 39994 WHERE `entry`= 39995;
UPDATE `creature_template` SET `lootid` = 40008 WHERE `entry`= 40009;
UPDATE `creature_template` SET `lootid` = 40013 WHERE `entry`= 40014;
UPDATE `creature_template` SET `lootid` = 40015 WHERE `entry`= 40016;
UPDATE `creature_template` SET `lootid` = 40017 WHERE `entry`= 40018;
UPDATE `creature_template` SET `lootid` = 40019 WHERE `entry`= 40020;
UPDATE `creature_template` SET `lootid` = 40021 WHERE `entry`= 40022;
UPDATE `creature_template` SET `lootid` = 40023 WHERE `entry`= 40024;
UPDATE `creature_template` SET `lootid` = 40084 WHERE `entry`= 40085;
UPDATE `creature_template` SET `lootid` = 50284 WHERE `entry`= 50285;

update creature_template set health_mod=15, minlevel=85, maxlevel=85, faction_A=16, faction_H=16, mindmg=530, maxdmg=713, attackpower=827, dmg_multiplier=7.5, baseattacktime=2000 where entry in (39708,39980,39982,39985,39978,39985,40004,40019,40017,40021,50284);
update creature_template set health_mod=15, minlevel=85, maxlevel=85, faction_A=16, faction_H=16, mindmg=530, maxdmg=713, attackpower=827, dmg_multiplier=12.5, baseattacktime=2000 where entry in (39709,39981,39983,39986,39979,39986,40020,40018,40022,50285);

update creature_template SET `Health_mod`=22.8152 WHERE entry in (39665, 39698);
update creature_template SET `Health_mod`=45.6304 WHERE entry in (39666, 39699);

update creature_template set health_mod=45.05232558139535, minlevel=85, maxlevel=85, faction_A=16, faction_H=16, mindmg=545, maxdmg=723, attackpower=839, dmg_multiplier=7.5, baseattacktime=2000 where entry in (39987,39994,40023);

update creature_template set health_mod=90.05232558139535, minlevel=85, maxlevel=85, faction_A=16, faction_H=16, mindmg=545, maxdmg=723, attackpower=839, dmg_multiplier=12.5, baseattacktime=2000 where entry in (39988,39995,40024);

update creature_template set health_mod=3, minlevel=85, maxlevel=85, faction_A=16, faction_H=16, mindmg=470, maxdmg=650, attackpower=750, dmg_multiplier=7.5, baseattacktime=2000 where entry in (40084);

update creature_template set health_mod=6, minlevel=85, maxlevel=85, faction_A=16, faction_H=16, mindmg=470, maxdmg=650, attackpower=750, dmg_multiplier=12.5, baseattacktime=2000 where entry in (40085);