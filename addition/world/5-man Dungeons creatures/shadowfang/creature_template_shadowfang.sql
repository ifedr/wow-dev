REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `modelid2`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `pickpocketloot`, `skinloot`, `Health_mod`, `Mana_mod`) VALUES (3864, 48968, 1951, 0, 19, 19, 0, 15, 15, 27, 37, 0, 68, 1.7, 2000, 3864, 0, 3864, 3, 1);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `modelid2`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `pickpocketloot`, `skinloot`, `Health_mod`, `Mana_mod`) VALUES (48968, 0, 1951, 0, 19, 19, 0, 85, 85, 27, 37, 0, 68, 25.7, 2000, 48968, 0, 48968, 15, 1);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `modelid2`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `pickpocketloot`, `skinloot`, `Health_mod`, `Mana_mod`) VALUES (3870, 49160, 7534, 0, 20, 20, 0, 16, 16, 29, 39, 0, 25.7, 1.7, 2000, 3870, 0, 0, 3, 1);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `modelid2`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `pickpocketloot`, `skinloot`, `Health_mod`, `Mana_mod`) VALUES (49160, 0, 7534, 0, 20, 20, 0, 85, 85, 29, 39, 0, 70, 25.7, 2000, 49160, 0, 0, 15, 1);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `modelid2`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `pickpocketloot`, `skinloot`, `Health_mod`, `Mana_mod`) VALUES (3875, 48942, 3229, 3230, 19, 20, 0, 24, 24, 29, 39, 0, 70, 1.7, 2000, 3875, 0, 0, 3, 1);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `modelid2`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `pickpocketloot`, `skinloot`, `Health_mod`, `Mana_mod`) VALUES (48942, 0, 3229, 3230, 19, 20, 0, 85, 85, 29, 39, 0, 70, 25.7, 2000, 48942, 0, 0, 20, 1);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `modelid2`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `pickpocketloot`, `skinloot`, `Health_mod`, `Mana_mod`) VALUES (47132, 49125, 37301, 0, 20, 20, 0, 16, 16, 29, 39, 0, 70, 1.7, 2000, 47132, 0, 0, 1, 1);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `modelid2`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `pickpocketloot`, `skinloot`, `Health_mod`, `Mana_mod`) VALUES (49125, 0, 37301, 0, 20, 20, 0, 85, 85, 29, 39, 0, 70, 25.7, 2000, 49125, 0, 0, 10, 1);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `modelid2`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (47134, 48851, 25001, 0, 19, 19, 0, 16, 16, 27, 37, 0, 68, 1.7, 2000, 47134, 4, 2);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `modelid2`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (48851, 0, 25001, 0, 19, 19, 0, 16, 16, 85, 85, 0, 68, 25.7, 2000, 48851, 20, 2);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `modelid2`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (47136, 49184, 24998, 0, 20, 20, 0, 16, 16, 29, 39, 0, 70, 1.7, 2000, 47136, 4, 2);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `modelid2`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (49184, 0, 24998, 0, 20, 20, 0, 16, 16, 85, 85, 0, 70, 100.7, 2000, 49184, 20, 2);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `modelid2`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (47138, 49187, 27323, 0, 20, 20, 0, 16, 16, 29, 39, 0, 70, 1.7, 2000, 47138, 8, 2);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `modelid2`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (49187, 0, 27323, 0, 20, 20, 0, 16, 16, 85, 85, 0, 70, 100.7, 2000, 49187, 26, 2);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `modelid2`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (47143, 48988, 36374, 0, 19, 19, 0, 16, 16, 27, 37, 0, 68, 1.7, 2000, 47143, 8, 2);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `modelid2`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (48988, 0, 36374, 0, 19, 19, 0, 16, 16, 85, 85, 0, 68, 25.7, 2000, 48988, 26, 2);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `modelid2`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (47146, 49186, 28022, 0, 20, 20, 0, 16, 16, 29, 39, 0, 70, 1.7, 1500, 47146, 8, 2);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `modelid2`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (49186, 0, 28022, 0, 20, 20, 0, 16, 16, 85, 85, 0, 70, 25.7, 1500, 49186, 26, 2);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `modelid2`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (47232, 48981, 37299, 37300, 19, 19, 0, 16, 16, 27, 37, 0, 68, 1.7, 2000, 47232, 4, 1);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `modelid2`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (48981, 0, 37299, 37300, 19, 19, 0, 85, 85, 27, 37, 0, 68, 100.7, 2000, 47232, 20, 1);

UPDATE `creature_template` SET `difficulty_entry_1` = 49708 WHERE `entry` = 46962;
UPDATE `creature_template` SET `minlevel` = 87, `maxlevel` = 87, `exp` = 3, `faction_A` = 16, `faction_H` = 16, `mindmg` = 18000, `maxdmg` = 24200, `attackpower` = 30000, `baseattacktime` = 2000, `unit_class` = 2, `lootid` = 49708, `dmg_multiplier`=1 ,`Health_mod` = 66, `Mana_mod` = 931000, `mechanic_immune_mask` = 613122047 WHERE `entry` = 49708;
DELETE FROM `creature_loot_template` WHERE (`entry`=49708);
INSERT INTO `creature_loot_template` VALUES 
(49708, 63434, 0, 1, 1, 1, 1),
(49708, 63437, 0, 1, 2, 1, 1),
(49708, 63436, 0, 1, 1, 1, 1),
(49708, 63433, 0, 1, 2, 1, 1),
(49708, 63435, 0, 1, 1, 1, 1);

UPDATE `creature_template` SET `difficulty_entry_1` = 49711 WHERE `entry` = 46963;
UPDATE `creature_template` SET `minlevel` = 87, `maxlevel` = 87, `exp` = 3, `faction_A` = 16, `faction_H` = 16, `mindmg` = 19300, `maxdmg` = 24000, `attackpower` = 30000, `baseattacktime` = 2000, `unit_class` = 2, `dmg_multiplier`=1, `lootid` = 49711, `Health_mod` = 66, `Mana_mod` = 93100, `mechanic_immune_mask` = 613122047 WHERE `entry` = 49711;
DELETE FROM `creature_loot_template` WHERE (`entry`=49711);
INSERT INTO `creature_loot_template` VALUES 
(49711, 63453, 0, 1, 1, 1, 1),
(49711, 63452, 0, 1, 2, 1, 1),
(49711, 63450, 0, 1, 2, 1, 1),
(49711, 63454, 0, 1, 1, 1, 1),
(49711, 63455, 0, 1, 1, 1, 1);

UPDATE `creature_template` SET `difficulty_entry_1` = 49712 WHERE `entry` = 46964;
UPDATE `creature_template` SET `minlevel` = 87, `maxlevel` = 87, `exp` = 3, `faction_A` = 16, `faction_H` = 16, `mindmg` = 23000, `maxdmg` = 25000, `attackpower` = 32000, `baseattacktime` = 2000, `unit_class` = 2, `dmg_multiplier`=1, `Health_mod` = 66, `Mana_mod` = 93100, `mechanic_immune_mask` = 613122047 WHERE `entry` = 49712;
UPDATE `creature_template` SET `lootid` = 49712 WHERE `entry` = 49712;
DELETE FROM `creature_loot_template` WHERE (`entry`=49712);
INSERT INTO `creature_loot_template` VALUES 
(49712, 52078, 100, 1, 0, 1, 1),
(49712, 63457, 0, 1, 1, 1, 1),
(49712, 63458, 0, 1, 1, 1, 1),
(49712, 63464, 0, 1, 2, 1, 1),
(49712, 63461, 0, 1, 2, 1, 1),
(49712, 63460, 0, 1, 1, 1, 1),
(49712, 63462, 0, 1, 2, 1, 1),
(49712, 63459, 0, 1, 1, 1, 1),
(49712, 63463, 0, 1, 2, 1, 1),
(49712, 63465, 0, 1, 1, 1, 1),
(49712, 63456, 0, 1, 2, 1, 1);

UPDATE `creature_template` SET `difficulty_entry_1` = 49709 WHERE `entry` = 3887;
UPDATE `creature_template` SET `minlevel` = 87, `maxlevel` = 87, `exp` = 3, `mindmg` = 24000, `faction_A` = 16, `faction_H` = 16, `maxdmg` = 27000, `attackpower` = 25000, `dmg_multiplier` = 1.7, `baseattacktime` = 2000, `lootid` = 49709, `Health_mod` = 66, `mechanic_immune_mask` = 613122047 WHERE `entry` = 49709;
DELETE FROM `creature_loot_template` WHERE (`entry`=49709);
INSERT INTO `creature_loot_template` VALUES 
(49709, 63438, 0, 1, 1, 1, 1),
(49709, 63444, 0, 1, 2, 1, 1),
(49709, 63441, 0, 1, 1, 1, 1),
(49709, 63440, 0, 1, 2, 1, 1),
(49709, 63439, 0, 1, 1, 1, 1);

UPDATE `creature_template` SET `difficulty_entry_1` = 48919 WHERE `entry` = 47137;
UPDATE `creature_template` SET `minlevel` = 85, `maxlevel` = 85, `exp` = 3, `faction_A` = 16, `faction_H` = 16, `mindmg` = 12000, `maxdmg` = 14000, `attackpower` = 13000, `baseattacktime` = 2000, `dmg_multiplier`=1, `lootid` = 48778, `spell1` = 91217, `spell2` = 91220, `Health_mod` = 22 WHERE `entry` = 48919;