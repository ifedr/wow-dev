UPDATE `creature_template` SET `Health_mod`='70.572' WHERE (`entry`='49262');
UPDATE `creature_template` SET `Health_mod`='70.572' WHERE (`entry`='48710');
UPDATE `creature_template` SET `Health_mod`='70.572' WHERE (`entry`='48776');

REPLACE INTO `creature_template` (`entry`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `Health_mod`, `Mana_mod`) VALUES (49309, 85, 85, 2, 16, 16, 509, 683, 0, 805, 12.5, 2000, 40, 1);

REPLACE INTO `creature_template` (`entry`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `Health_mod`, `Mana_mod`) VALUES (49308, 85, 85, 2, 16, 16, 509, 683, 0, 805, 12.5, 2000, 40, 1);
REPLACE INTO `creature_template` (`entry`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `Health_mod`, `Mana_mod`) VALUES (49307, 85, 85, 2, 16, 16, 509, 683, 0, 805, 12.5, 2000, 40, 1);
REPLACE INTO `creature_template` (`entry`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `Health_mod`, `Mana_mod`) VALUES (49310, 85, 85, 2, 16, 16, 509, 683, 0, 805, 12.5, 2000, 40, 1);

INSERT INTO `disables` (`sourceType`, `entry`) VALUES ('0', '74938');

UPDATE `creature_template` set `gossip_menu_id`=39908 WHERE `entry`=39908;
DELETE FROM `gossip_menu_option` WHERE `menu_id` = 39908;
INSERT INTO `gossip_menu_option` (`menu_id`, `id`, `option_text`, `option_id`, `npc_option_npcflag`, `action_menu_id`, `action_script_id`) VALUES ('39908', 0, 'Up', '1','1','1', '39908');
INSERT INTO `gossip_menu_option` (`menu_id`, `id`, `option_text`, `option_id`, `npc_option_npcflag`, `action_menu_id`, `action_script_id`) VALUES ('39908', 1, 'Down', '1', '1', '2', '39909');
DELETE FROM `gossip_scripts` WHERE `id` IN (39908,39909);
INSERT INTO `gossip_scripts` (`id`, `command`, `datalong`, `x`, `y`, `z`, `o`) VALUES ('39908', '6', 644, '-524.845947', '212.650528', '330.650421', '5.555866');
INSERT INTO `gossip_scripts` (`id`, `command`, `datalong`, `x`, `y`, `z`, `o`) VALUES ('39909', '6', 644, '-524.845947', '212.650528', '79.813882', '5.555866');

UPDATE `creature_template` SET `AIName`='EventAI' WHERE `entry` IN (39587,39731,39732,39378,41126);

DELETE FROM `creature_ai_scripts` WHERE (`creature_id`=39587);
INSERT INTO `creature_ai_scripts` VALUES 
( 3958700, 39587, 0, 0, 100, 7, 0, 1000, 30000, 35000, 11, 74137, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, ''),
( 3958701, 39587, 0, 0, 100, 7, 1000, 5000, 10000, 15000, 11, 89883, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, ''),
(3958702, 39587, 0, 0, 100, 7, 5000, 15000, 50000, 55000, 11, 74133, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, '');

DELETE FROM `creature_ai_scripts` WHERE (`creature_id`=39731);
INSERT INTO `creature_ai_scripts` VALUES 
( 3973100, 39731, 0, 0, 100, 7, 0, 1000, 10000, 15000, 11, 76043, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, ''),
( 3973101, 39731, 0, 0, 100, 7, 1000, 5000, 12000, 20000, 11, 79768, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, ''),
(3973102, 39731, 0, 0, 100, 7, 10000, 15000, 32000, 35000, 11, 75790, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, '');

DELETE FROM `creature_ai_scripts` WHERE (`creature_id`=41126);
INSERT INTO `creature_ai_scripts` VALUES 
(4112600, 41126, 0, 0, 100, 7, 0, 1000, 0, 1000, 11, 76870, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, '');

DELETE FROM `creature_ai_scripts` WHERE (`creature_id`=39732);
INSERT INTO `creature_ai_scripts` VALUES 
( 3973200, 39732, 0, 0, 100, 7, 0, 1000, 4000, 5000, 11, 77069, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, ''),
( 3973201, 39732, 0, 0, 100, 7, 20000, 25000, 25000, 30000, 12, 41126, 4, 30000, 12, 41126, 4, 30000, 12, 41126, 4, 30000, ''),
(3973202, 39732, 2, 0, 100, 7, 30, 0, 0, 0, 11, 77026, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, '');

DELETE FROM `creature_ai_scripts` WHERE (`creature_id`=39378);
INSERT INTO `creature_ai_scripts` VALUES 
( 3937800, 39378, 0, 0, 100, 7, 0, 1000, 15000, 17000, 11, 73872, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, ''),
( 3937801, 39378, 0, 0, 100, 7, 2000, 5000, 10000, 20000, 11, 82856, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, ''),
(3937802, 39378, 0, 0, 100, 7, 20000, 50000, 50000, 60000, 11, 76352, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, '');

INSERT INTO `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `DeathState`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) VALUES (10414637, 39908, 644, 1, 1, 0, 0, -524.846, 212.651, 79.8139, 5.55587, 300, 0, 0, 42, 0, 0, 0, 0, 0, 0);
INSERT INTO `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `DeathState`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) VALUES (10415057, 39908, 644, 1, 1, 0, 0, -524.6, 211.017, 330.65, 5.55585, 300, 0, 0, 42, 0, 0, 0, 0, 0, 0);

UPDATE `creature` SET `spawntimesecs` = 43200 WHERE `map`=644;