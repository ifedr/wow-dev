REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (39381, 48654, 31430, 85, 85, 3, 16, 16, 530, 713, 0, 827, 7.5, 2000, 39381, 7.1882, 100);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (48654, 0, 31430, 85, 85, 3, 16, 16, 530, 713, 0, 827, 12.5, 2000, 39381, 15.1882, 100);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (39414, 48676, 35649, 85, 85, 3, 16, 16, 530, 713, 0, 827, 7.5, 2000, 39414, 8.38623, 100);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (48676, 0, 35649, 85, 85, 3, 16, 16, 530, 713, 0, 827, 12.5, 2000, 39414, 16.38623, 100);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (39415, 48677, 35179, 85, 85, 3, 16, 16, 530, 713, 0, 827, 7.5, 2000, 39415, 8.38623, 1);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (48677, 0, 35179, 85, 85, 3, 16, 16, 530, 713, 0, 827, 12.5, 2000, 39415, 16.38623, 1);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (39854, 48661, 31430, 85, 85, 3, 16, 16, 530, 713, 0, 827, 7.5, 2000, 39854, 7.1882, 100);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (48661, 0, 31430, 85, 85, 3, 16, 16, 530, 713, 0, 827, 12.5, 2000, 39854, 15.1882, 100);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (39855, 48746, 31512, 84, 85, 3, 14, 14, 530, 713, 0, 827, 7.5, 2000, 39855, 17.7819, 44.39);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (48746, 0, 31512, 84, 85, 3, 14, 14, 530, 713, 0, 827, 12.5, 2000, 39855, 34.7819, 44.39);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (39870, 48748, 31590, 85, 85, 3, 16, 16, 516, 696, 0, 813, 7.5, 2000, 39870, 1.32, 1.44979);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (48748, 0, 31590, 85, 85, 3, 16, 16, 516, 696, 0, 813, 12.5, 2000, 39870, 2.32, 1.44979);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (39873, 48754, 31602, 85, 85, 3, 16, 16, 530, 713, 0, 827, 7.5, 2000, 39873, 13.1784, 100);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (48754, 0, 31602, 85, 85, 3, 16, 16, 530, 713, 0, 827, 12.5, 2000, 39873, 26.1784, 100);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (39890, 48595, 31606, 85, 85, 3, 16, 16, 530, 713, 0, 827, 7.5, 2000, 39890, 5.75056, 100);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (48595, 0, 31606, 85, 85, 3, 16, 16, 530, 713, 0, 827, 12.5, 2000, 39890, 10.75056, 100);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (39892, 48747, 34999, 83, 84, 2, 14, 14, 519, 693, 0, 815, 7.5, 0, 0, 2.5, 1);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (39892, 48747, 34999, 83, 84, 2, 14, 14, 519, 693, 0, 815, 12.5, 0, 0, 5, 1);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (39900, 48594, 33693, 82, 83, 2, 14, 14, 509, 683, 0, 805, 7.5, 0, 0, 2.5, 1);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (48594, 0, 33693, 82, 83, 2, 14, 14, 509, 683, 0, 805, 12.5, 0, 0, 5, 1);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (39909, 48680, 31499, 85, 85, 3, 16, 16, 530, 713, 0, 827, 7.5, 2000, 39909, 7.1882, 100);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (48680, 0, 31499, 85, 85, 3, 16, 16, 530, 713, 0, 827, 12.5, 2000, 39909, 14.1882, 100);


REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (39954, 48669, 31770, 85, 85, 3, 16, 16, 530, 713, 0, 827, 7.5, 2000, 0, 5.75056, 100);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (48669, 0, 31770, 85, 85, 3, 16, 16, 530, 713, 0, 827, 12.5, 2000, 0, 10.75056, 100);


REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (39956, 48667, 25192, 82, 83, 2, 14, 14, 509, 683, 0, 805, 7.5, 0, 39956, 7.1882, 1);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (48667, 0, 25192, 82, 83, 2, 14, 14, 509, 683, 0, 805, 12.5, 0, 39956, 14.1882, 1);



REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (39961, 48596, 34272, 82, 83, 2, 14, 14, 509, 683, 0, 805, 7.5, 0, 0, 2.5, 1);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (48596, 0, 34272, 82, 83, 2, 14, 14, 509, 683, 0, 805, 12.5, 0, 0, 5, 1);


REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (39962, 48597, 31584, 82, 83, 2, 14, 14, 509, 683, 0, 805, 7.5, 0, 39962, 4, 10);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (48597, 0, 31584, 82, 83, 2, 14, 14, 509, 683, 0, 805, 12.5, 0, 39962, 8, 10);


REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (39984, 0, 33289, 85, 85, 3, 16, 16, 530, 713, 0, 827, 1, 2000, 39381, 0.898533, 100);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (40166, 48693, 17764, 85, 85, 3, 16, 16, 530, 713, 0, 827, 7.5, 2000, 40166, 6.99303, 100);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (48693, 0, 17764, 85, 85, 3, 16, 16, 530, 713, 0, 827, 12.5, 2000, 40166, 13.99303, 100);


REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (40167, 48695, 31584, 85, 85, 3, 16, 16, 530, 713, 0, 827, 7.5, 2000, 40167, 5.75056, 100);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (48695, 0, 31584, 85, 85, 3, 16, 16, 530, 713, 0, 827, 12.5, 2000, 40167, 11.75056, 100);


REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (40268, 48744, 31769, 85, 85, 3, 16, 16, 530, 713, 0, 827, 7.5, 2000, 40268, 7.1882, 100);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (48744, 0, 31769, 85, 85, 3, 16, 16, 530, 713, 0, 827, 12.5, 2000, 40268, 15.1882, 100);


REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (40269, 48749, 34542, 82, 83, 2, 14, 14, 509, 683, 0, 805, 7.5, 0, 0, 2.5, 1);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (48749, 0, 34542, 82, 83, 2, 14, 14, 509, 683, 0, 805, 12.5, 0, 0, 5, 1);


REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (40270, 48750, 31789, 82, 83, 2, 14, 14, 509, 683, 0, 805, 7.5, 0, 40270, 4, 10);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (48750, 0, 31789, 82, 83, 2, 14, 14, 509, 683, 0, 805, 12.5, 0, 40270, 8, 10);


REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (40272, 48751, 36266, 85, 85, 3, 16, 16, 530, 713, 0, 827, 7.5, 2000, 40272, 8.38623, 100);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (48751, 0, 36266, 85, 85, 3, 16, 16, 530, 713, 0, 827, 12.5, 2000, 40272, 16.38623, 100);


REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (40273, 48753, 35648, 85, 85, 3, 16, 16, 530, 713, 0, 827, 7.5, 2000, 40273, 8.38623, 100);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (48753, 0, 35648, 85, 85, 3, 16, 16, 530, 713, 0, 827, 12.5, 2000, 40273, 17.38623, 100);


REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (40290, 48652, 31440, 83, 83, 2, 14, 14, 509, 683, 0, 805, 7.5, 0, 40290, 4, 10);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (48652, 0, 31440, 83, 83, 2, 14, 14, 509, 683, 0, 805, 12.5, 0, 40290, 8, 10);


REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (40291, 48745, 31512, 85, 85, 3, 16, 16, 516, 696, 0, 813, 7.5, 2000, 40291, 1.32, 36.8847);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (48745, 0, 31512, 85, 85, 3, 16, 16, 516, 696, 0, 813, 12.5, 2000, 40291, 3.32, 36.8847);


REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (40306, 48611, 31783, 85, 85, 3, 16, 16, 530, 713, 0, 827, 7.5, 2000, 40306, 7.1882, 100);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (48611, 0, 31783, 85, 85, 3, 16, 16, 530, 713, 0, 827, 12.5, 2000, 40306, 15.1882, 100);


REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (40320, 0, 31795, 87, 87, 3, 16, 16, 2390, 3834, 0, 0, 7.5, 2000, 0, 25.6626, 20);


REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (40448, 48666, 25192, 82, 83, 2, 14, 14, 509, 683, 0, 805, 7.5, 0, 40448, 4, 1);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (48666, 0, 25192, 82, 83, 2, 14, 14, 509, 683, 0, 805, 12.5, 0, 40448, 8, 1);


REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (41073, 48610, 31783, 82, 83, 2, 14, 14, 509, 683, 0, 805, 7.5, 2000, 41073, 22.2273, 1);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (48610, 0, 31783, 82, 83, 2, 14, 14, 509, 683, 0, 805, 12.5, 2000, 41073, 44.2273, 1);

UPDATE `creature_template` SET `Health_mod`='22.613' WHERE (`entry`='48784');
UPDATE `creature_template` SET `AIName`='EventAI' WHERE `entry` IN (39625,40177,40319,40484,40320);
update creature_template SET dmg_multiplier=12.5 WHERE `entry` IN (48337,48702,48784,48822);
DELETE FROM `creature_ai_scripts` WHERE (`creature_id`=39625);
INSERT INTO `creature_ai_scripts` VALUES 
( 3962500, 39625, 0, 0, 100, 7, 1000, 5000, 7000, 15000, 11, 74846, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, ''),
( 3962501, 39625, 0, 0, 100, 7, 10000, 15000, 10000, 25000, 11, 74670, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, ''),
( 3962502, 39625, 0, 0, 100, 7, 20000, 25000, 20000, 25000, 11, 74634, 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, ''),
(3962503, 39625, 2, 0, 100, 7, 30, 0, 0, 0, 11, 74853, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, '');

DELETE FROM `creature_ai_scripts` WHERE (`creature_id`=40177);
INSERT INTO `creature_ai_scripts` VALUES 
( 4017700, 40177, 0, 0, 100, 7, 1000, 5000, 30000, 50000, 11, 74976, 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, ''),
( 4017701, 40177, 0, 0, 100, 7, 1000, 15000, 30000, 50000, 11, 75007, 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, ''),
( 4017702, 40177, 0, 0, 100, 7, 10000, 15000, 15000, 25000, 11, 75056, 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, ''),
(4017703, 40177, 0, 0, 100, 7, 10000, 25000, 50000, 75000, 11, 74908, 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, '');

DELETE FROM `creature_ai_scripts` WHERE (`creature_id`=40319);
INSERT INTO `creature_ai_scripts` VALUES 
(4031900, 40319, 0, 0, 100, 7, 1000, 5000, 1000, 1000, 11, 75245, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, '');

DELETE FROM `creature_ai_scripts` WHERE (`creature_id`=40320);
INSERT INTO `creature_ai_scripts` VALUES 
(4032000, 40320, 0, 0, 100, 7, 1000, 5000, 10000, 15000, 11, 90950, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, '');

DELETE FROM `creature_ai_scripts` WHERE (`creature_id`=40484);
INSERT INTO `creature_ai_scripts` VALUES 
( 4048400, 40484, 0, 0, 100, 7, 1000, 5000, 10000, 20000, 11, 75809, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, ''),
( 4048401, 40484, 0, 0, 100, 7, 50000, 60000, 55000, 60000, 11, 91086, 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, ''),
( 4048402, 40484, 0, 0, 100, 7, 10000, 15000, 25000, 30000, 11, 75789, 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, ''),
(4048403, 40484, 0, 0, 100, 7, 10000, 12000, 20000, 30000, 11, 91081, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, '');