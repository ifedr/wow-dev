DELETE FROM `creature` WHERE `id` in (42188,42333,43214,43438);
INSERT INTO `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `DeathState`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) VALUES (10071893, 42188, 725, 3, 1, 0, 0, 1491.52, 1070.29, 217.823, 0.394437, 300, 0, 0, 2443837, 0, 0, 0, 0, 0, 0);
INSERT INTO `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `DeathState`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) VALUES (10071976, 42333, 725, 3, 1, 0, 0, 1337.66, 962.682, 214.183, 1.83564, 300, 0, 0, 1607789, 0, 0, 0, 0, 0, 0);
INSERT INTO `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `DeathState`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) VALUES (10071510, 43214, 725, 3, 1, 0, 0, 1274.94, 1221.82, 247.091, 3.91694, 300, 0, 0, 4137831, 0, 0, 0, 0, 0, 0);
INSERT INTO `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `DeathState`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) VALUES (10070873, 43438, 725, 3, 1, 0, 0, 1155.85, 877.595, 284.992, 3.32396, 300, 0, 0, 1931709, 0, 0, 0, 0, 0, 0);

UPDATE `creature` SET `spawntimesecs` = 43200 WHERE `map` = 725;

UPDATE `creature_template` SET `AIName`='EventAI' WHERE `entry` IN (43438,43214,42188,42333);

DELETE FROM `creature_ai_scripts` WHERE (`creature_id`=43438);
INSERT INTO `creature_ai_scripts` VALUES 
( 4343800, 43438, 0, 0, 100, 7, 0, 1000, 10000, 15000, 11, 86881, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, ''),
(4343801, 43438, 0, 0, 100, 7, 1000, 5000, 15000, 25000, 11, 82415, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, '');

DELETE FROM `creature_ai_scripts` WHERE (`creature_id`=43214);
INSERT INTO `creature_ai_scripts` VALUES 
( 4321400, 43214, 0, 0, 1000, 7, 0, 1000, 15000, 25000, 11, 80807, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, ''),
(4321401, 43214, 0, 0, 1000, 5, 25000, 30000, 35000, 55000, 11, 92265, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, '');

DELETE FROM `creature_ai_scripts` WHERE (`creature_id`=42188);
INSERT INTO `creature_ai_scripts` VALUES 
( 4218800, 42188, 0, 0, 100, 7, 1000, 5000, 20000, 30000, 11, 78807, 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, ''),
( 4218801, 42188, 0, 0, 100, 7, 15000, 20000, 40000, 50000, 11, 78903, 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, ''),
(4218802, 42188, 2, 0, 100, 7, 30, 0, 40000, 0, 11, 80467, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, '');

DELETE FROM `creature_ai_scripts` WHERE (`creature_id`=42333);
INSERT INTO `creature_ai_scripts` VALUES 
( 4233300, 42333, 0, 0, 100, 7, 0, 1000, 15000, 20000, 11, 79345, 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, ''),
(4233301, 42333, 0, 0, 100, 7, 15000, 20000, 25000, 30000, 11, 79351, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, '');