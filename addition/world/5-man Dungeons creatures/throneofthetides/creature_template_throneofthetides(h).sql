REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `name`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (39616, 49085, 37370, 'Naz\'jar Invader', 77, 83, 2, 26, 26, 509, 683, 0, 805, 7.5, 2000, 39616, 5, 1);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `name`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (49085, 0, 37370, 'Naz\'jar Invader', 85, 85, 2, 26, 26, 509, 683, 0, 805, 12.5, 2000, 39616, 10, 1);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `name`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (39959, 49079, 34342, 'Lady Naz\'jar', 82, 84, 2, 26, 26, 519, 693, 0, 815, 7.5, 2000, 0, 5, 1);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `name`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (49079, 0, 34342, 'Lady Naz\'jar', 85, 85, 2, 26, 26, 519, 693, 0, 815, 12.5, 2000, 0, 10, 1);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `name`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (39960, 49066, 32311, 'Deep Murloc Drudge', 78, 85, 2, 26, 26, 530, 713, 0, 827, 7.5, 2000, 0, 50, 1);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `name`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (49066, 0, 32311, 'Deep Murloc Drudge', 85, 85, 2, 26, 26, 530, 713, 0, 827, 12.5, 2000, 0, 100, 1);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `name`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (40577, 49085, 32568, 'Naz\'jar Sentinel', 78, 83, 2, 26, 26, 509, 683, 0, 805, 7.5, 2000, 40577, 5, 1);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `name`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (49085, 0, 32568, 'Naz\'jar Sentinel', 85, 85, 2, 26, 26, 509, 683, 0, 805, 12.5, 2000, 40577, 10, 1);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `name`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (40579, 49067, 32311, 'Deep Murloc Hunter', 80, 85, 2, 26, 26, 530, 713, 0, 827, 7.5, 2000, 0, 0.841735, 1);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `name`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (49067, 0, 32311, 'Deep Murloc Hunter', 85, 85, 2, 26, 26, 530, 713, 0, 827, 12.5, 2000, 0, 2.841735, 1);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `name`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (40586, 49080, 34342, 'Lady Naz\'jar', 81, 84, 2, 26, 26, 519, 693, 0, 815, 7.5, 2000, 40586, 22.8152, 1);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `name`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `Health_mod`, `Mana_mod`) VALUES (49080, 0, 34342, 'Lady Naz\'jar', 85, 85, 2, 26, 26, 519, 693, 0, 815, 12.5, 2000, 45.6304, 1);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `name`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (40633, 49084, 32348, 'Naz\'jar Honor Guard', 79, 83, 2, 26, 26, 509, 683, 0, 805, 7.5, 2000, 0, 5, 1);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `name`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (49084, 0, 32348, 'Naz\'jar Honor Guard', 85, 85, 2, 26, 26, 509, 683, 0, 805, 12.5, 2000, 0, 10, 1);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `name`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (40634, 49092, 37388, 'Naz\'jar Tempest Witch', 78, 83, 2, 26, 26, 509, 683, 0, 805, 7.5, 2000, 40634, 5, 7988);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `name`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (49092, 0, 37388, 'Naz\'jar Tempest Witch', 85, 85, 2, 26, 26, 509, 683, 0, 805, 12.5, 2000, 40634, 10, 7988);


REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `name`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (40765, 49064, 33792, 'Commander Ulthok', 81, 83, 2, 26, 26, 509, 683, 0, 805, 7.5, 2000, 40765, 22.8152, 1);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `name`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `Health_mod`, `Mana_mod`) VALUES (49064, 0, 33792, 'Commander Ulthok', 85, 85, 2, 26, 26, 509, 683, 0, 805, 12.5, 2000, 45.6304, 1);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `name`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (40788, 49082, 32259, 'Mindbender Ghur\'sha', 81, 84, 2, 26, 26, 519, 693, 0, 815, 7.5, 2000, 40788, 22.8152, 1);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `name`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `Health_mod`, `Mana_mod`) VALUES (49082, 0, 32259, 'Mindbender Ghur\'sha', 85, 85, 2, 26, 26, 519, 693, 0, 815, 7.5, 2000, 45.6304, 1);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `name`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (40792, 49094, 32081, 'Neptulon', 81, 85, 2, 26, 26, 530, 713, 0, 827, 7.5, 0, 0, 5, 1);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `name`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (49094, 0, 32081, 'Neptulon', 85, 85, 2, 26, 26, 530, 713, 0, 827, 12.5, 0, 0, 10, 1);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `name`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (40825, 49072, 30408, 'Erunak Stonespeaker', 81, 84, 2, 26, 26, 519, 693, 0, 815, 7.5, 2000, 0, 5, 83380);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `name`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (49072, 0, 30408, 'Erunak Stonespeaker', 85, 85, 2, 26, 26, 519, 693, 0, 815, 12.5, 2000, 0, 10, 83380);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `name`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (40923, 49103, 33845, 'Unstable Corruption', 77, 85, 2, 26, 26, 530, 713, 0, 827, 3, 2000, 0, 5, 1);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `name`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (49103, 0, 33845, 'Unstable Corruption', 85, 85, 2, 26, 26, 530, 713, 0, 827, 6, 2000, 0, 10, 1);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `name`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (40925, 49102, 33846, 'Tainted Sentry', 79, 83, 2, 26, 26, 509, 683, 0, 805, 7.5, 2000, 40925, 5, 1);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `name`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (49102, 0, 33846, 'Tainted Sentry', 85, 85, 2, 26, 26, 509, 683, 0, 805, 12.5, 2000, 40925, 10, 1);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `name`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (40935, 49078, 31982, 'Gilgoblin Hunter', 77, 83, 2, 26, 26, 509, 683, 0, 805, 7.5, 2000, 40935, 5, 1);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `name`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (49078, 0, 31982, 'Gilgoblin Hunter', 85, 85, 2, 26, 26, 509, 683, 0, 805, 12.5, 2000, 40935, 10, 1);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `name`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (40936, 49074, 31674, 'Faceless Watcher', 77, 85, 2, 26, 26, 545, 723, 0, 839, 7.5, 2000, 40936, 5, 1);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `name`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (49074, 0, 31674, 'Faceless Watcher', 85, 85, 2, 26, 26, 545, 723, 0, 839, 12.5, 2000, 40936, 10, 1);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `name`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (40943, 49077, 32121, 'Gilgoblin Aquamage', 79, 85, 2, 26, 26, 530, 713, 0, 827, 7.5, 0, 40943, 5, 1);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `name`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (49077, 0, 32121, 'Gilgoblin Aquamage', 85, 85, 2, 26, 26, 530, 713, 0, 827, 12.5, 0, 40943, 10, 1);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `name`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (41139, 49091, 37385, 'Naz\'jar Spiritmender', 78, 83, 2, 26, 26, 509, 683, 0, 805, 7.5, 2000, 41139, 5, 7988);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `name`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (49091, 0, 37385, 'Naz\'jar Spiritmender', 85, 85, 2, 26, 26, 509, 683, 0, 805, 12.5, 2000, 41139, 10, 7988);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `name`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (44404, 49093, 37388, 'Naz\'jar Tempest Witch', 79, 83, 2, 26, 26, 509, 683, 0, 805, 7.5, 2000, 0, 5, 7988);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `name`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (49093, 0, 37388, 'Naz\'jar Tempest Witch', 85, 85, 2, 26, 26, 509, 683, 0, 805, 12.5, 2000, 0, 10, 7988);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `name`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (44648, 49104, 32018, 'Unyielding Behemoth', 79, 83, 2, 15, 15, 509, 683, 0, 805, 7.5, 2000, 0, 5, 1);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `name`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (49104, 0, 32018, 'Unyielding Behemoth', 85, 85, 2, 15, 15, 509, 683, 0, 805, 12.5, 2000, 0, 10, 1);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `name`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (44658, 49068, 34352, 'Deep Murloc Invader', 80, 85, 2, 15, 15, 545, 723, 0, 839, 3, 2000, 0, 10, 1);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `name`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (49068, 0, 34352, 'Deep Murloc Invader', 85, 85, 2, 15, 15, 545, 723, 0, 839, 6, 2000, 0, 20, 1);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `name`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (44715, 49107, 32708, 'Vicious Mindlasher', 80, 83, 2, 15, 15, 509, 683, 0, 805, 7.5, 2000, 0, 5, 8162);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `name`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (49107, 0, 32708, 'Vicious Mindlasher', 85, 85, 2, 15, 15, 509, 683, 0, 805, 12.5, 2000, 0, 10, 8162);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `name`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (44752, 49073, 32708, 'Faceless Sapper', 81, 85, 2, 15, 15, 545, 723, 0, 839, 7.5, 2000, 0, 5, 1);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `name`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (49073, 0, 32708, 'Faceless Sapper', 85, 85, 2, 15, 15, 545, 723, 0, 839, 12.5, 2000, 0, 10, 1);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `name`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (44841, 49061, 19874, 'Blight Beast', 81, 85, 2, 15, 15, 545, 723, 0, 839, 7.5, 2000, 0, 5, 1);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `name`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (49061, 0, 19874, 'Blight Beast', 85, 85, 2, 15, 15, 545, 723, 0, 839, 12.5, 2000, 0, 10, 1);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `name`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (45620, 49088, 37386, 'Naz\'jar Soldier', 80, 85, 2, 15, 15, 530, 713, 0, 827, 7.5, 2000, 0, 5, 1);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `name`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (49088, 0, 37386, 'Naz\'jar Soldier', 85, 85, 2, 15, 15, 530, 713, 0, 827, 12.5, 2000, 0, 10, 1);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `name`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (50270, 0, 36746, 'Captain Taylor', 81, 81, 2, 2130, 2130, 464, 604, 0, 708, 7.5, 2000, 0, 5, 1);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `modelid1`, `name`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (50272, 0, 36747, 'Legionnaire Nazgrim', 81, 81, 2, 2123, 2123, 464, 604, 0, 708, 7.5, 2000, 0, 5, 1);

UPDATE `creature_template` SET `npcflag`=3 where `entry` in (50270,50272);
UPDATE `creature_template` SET `AIName`='EventAI' WHERE `entry` IN (40586,40765,40825,42172,40788);

UPDATE `creature_template` SET `modelid1`='8716',`npcflag`=1,`gossip_menu_id`=51395 WHERE (`entry`='51395');
DELETE FROM `gossip_menu_option` WHERE `menu_id` = 51395;
INSERT INTO `gossip_menu_option` (`menu_id`, `id`, `option_text`, `option_id`, `npc_option_npcflag`, `action_menu_id`, `action_script_id`) VALUES ('51395', 0, 'Up', '1','1','1', '51395');
INSERT INTO `gossip_menu_option` (`menu_id`, `id`, `option_text`, `option_id`, `npc_option_npcflag`, `action_menu_id`, `action_script_id`) VALUES ('51395', 1, 'Down', '1', '1', '2', '51396');
DELETE FROM `gossip_scripts` WHERE `id` IN (51396,51395);
INSERT INTO `gossip_scripts` (`id`, `command`, `datalong`, `x`, `y`, `z`, `o`) VALUES ('51396', '6', 643, '-222.399', '809.2789', '262.0444', '3.193775');
INSERT INTO `gossip_scripts` (`id`, `command`, `datalong`, `x`, `y`, `z`, `o`) VALUES ('51395', '6', 643, '-181', '802.9789', '796.98', '0');

DELETE FROM `creature_ai_scripts` WHERE (`creature_id`=40586);
INSERT INTO `creature_ai_scripts` VALUES 
( 4058600, 40586, 0, 0, 100, 7, 5000, 10000, 10000, 15000, 11, 80564, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, ''),
(4058601, 40586, 0, 0, 100, 7, 5000, 10000, 15000, 25000, 11, 76008, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, '');

DELETE FROM `creature_ai_scripts` WHERE (`creature_id`=40765);
INSERT INTO `creature_ai_scripts` VALUES 
( 4076500, 40765, 0, 0, 100, 7, 10000, 15000, 15000, 25000, 11, 76094, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, ''),
( 4076501, 40765, 0, 0, 100, 5, 20000, 25000, 25000, 30000, 11, 96311, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, ''),
( 4076502, 40765, 0, 0, 100, 7, 20000, 25000, 25000, 30000, 11, 76100, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, ''),
(4076503, 40765, 0, 0, 100, 7, 15000, 25000, 28000, 35000, 11, 76026, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, '');

DELETE FROM `creature_ai_scripts` WHERE (`creature_id`=40825);
INSERT INTO `creature_ai_scripts` VALUES 
( 4082500, 40825, 0, 0, 100, 7, 15000, 20000, 20000, 25000, 11, 76171, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, ''),
( 4082501, 40825, 0, 0, 100, 7, 25000, 30000, 30000, 35000, 11, 76170, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, ''),
( 4082502, 40825, 0, 0, 100, 7, 5000, 10000, 10000, 15000, 11, 76165, 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, ''),
(4082503, 40825, 2, 0, 100, 6, 50, 0, 0, 15000, 12, 40788, 4, 3600000, 0, 0, 0, 0, 0, 0, 0, 0, '');

DELETE FROM `creature_ai_scripts` WHERE (`creature_id`=40788);
INSERT INTO `creature_ai_scripts` VALUES 
( 4078800, 40788, 0, 0, 100, 7, 10000, 15000, 25000, 30000, 11, 76307, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, ''),
(4078801, 40788, 0, 0, 100, 7, 10000, 15000, 25000, 30000, 11, 76339, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, '');

