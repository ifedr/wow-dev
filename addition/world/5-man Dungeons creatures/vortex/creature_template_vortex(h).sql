REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `Health_mod`, `Mana_mod`) VALUES (43873, 43874, 30.9114, 41.4892);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `Health_mod`, `Mana_mod`) VALUES (43874, 0, 61.9114, 82.9783);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `Health_mod`, `Mana_mod`) VALUES (43875, 43876, 39.8856, 41.4892);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `Health_mod`, `Mana_mod`) VALUES (43876, 0, 80.8856, 82.9783);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `Health_mod`, `Mana_mod`) VALUES (43878, 43879, 30.9114, 41.4892);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `Health_mod`, `Mana_mod`) VALUES (43879, 0, 61.9114, 82.9783);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (45704, 46450, 80, 85, 2, 16, 16, 530, 713, 827, 7.5, 2000, 0, 7.21769, 1);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (46450, 0, 85, 85, 2, 16, 16, 530, 713, 827, 12.5, 2000, 0, 14.21769, 1);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (45912, 45913, 80, 85, 2, 16, 16, 516, 696, 813, 7.5, 2000, 45912, 7.21769, 10.4382);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (45913, 0, 85, 85, 2, 16, 16, 516, 696, 813, 12.5, 2000, 45912, 14.21769, 20.4382);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (45915, 0, 85, 85, 2, 16, 16, 530, 713, 827, 7.5, 2000, 45915, 7.21769, 1);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (45919, 0, 85, 85, 2, 16, 16, 530, 713, 827, 7.5, 2000, 45919, 17.9229, 1);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (45922, 0, 85, 85, 2, 16, 16, 516, 696, 813, 7.5, 2000, 45922, 7.45868, 10.709);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (45924, 45925, 79, 85, 2, 16, 16, 516, 696, 813, 7.5, 2000, 45924, 7.45868, 10.709);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (45925, 0, 85, 85, 2, 16, 16, 516, 696, 813, 12.5, 2000, 45924, 14.45868, 20.709);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (45926, 45927, 82, 82, 2, 16, 16, 488, 642, 782, 7.5, 2000, 45926, 7.45868, 41690);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (45927, 0, 85, 85, 2, 16, 16, 488, 642, 782, 12.5, 2000, 45926, 14.45868, 41690);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (45928, 45929, 80, 85, 2, 16, 16, 530, 713, 827, 7.5, 2000, 45928, 7.21769, 1);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (45929, 0, 85, 85, 2, 16, 16, 530, 713, 827, 12.5, 2000, 45928, 14.21769, 1);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (45930, 45931, 80, 85, 2, 16, 16, 516, 696, 813, 7.5, 2000, 45930, 5.77412, 22.9344);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (45931, 0, 85, 85, 2, 16, 16, 516, 696, 813, 7.5, 2000, 45930, 11.77412, 22.9344);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (45932, 45933, 82, 82, 2, 16, 16, 488, 642, 782, 7.5, 2000, 0, 1.11868, 0);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (45933, 0, 85, 85, 2, 16, 16, 488, 642, 782, 12.5, 2000, 0, 3.11868, 0);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (45935, 45936, 80, 85, 2, 16, 16, 516, 696, 813, 7.5, 2000, 45935, 5.77412, 22.9344);
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (45936, 0, 85, 85, 2, 16, 16, 516, 696, 813, 7.5, 2000, 45935, 10.77412, 44.9344);

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `mindmg`, `maxdmg`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `lootid`, `Health_mod`, `Mana_mod`) VALUES (49943, 0, 84, 84, 0, 35, 35, 2, 2, 24, 1, 0, 0, 1, 1);
