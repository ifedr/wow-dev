/* Arcane Concentration proc chance fix */
DELETE FROM `spell_proc_event` WHERE `entry` = 11213;
DELETE FROM `spell_proc_event` WHERE `entry` = 12574;
DELETE FROM `spell_proc_event` WHERE `entry` = 12575;
INSERT INTO `spell_proc_event` (`entry`, `SchoolMask`, `SpellFamilyName`, `SpellFamilyMask0`, `SpellFamilyMask1`, `SpellFamilyMask2`, `procFlags`, `procEx`, `ppmRate`, `CustomChance`, `Cooldown`) VALUES (11213, 0, 3, 0, 0, 0, 65536, 0, 0, 3, 0);
INSERT INTO `spell_proc_event` (`entry`, `SchoolMask`, `SpellFamilyName`, `SpellFamilyMask0`, `SpellFamilyMask1`, `SpellFamilyMask2`, `procFlags`, `procEx`, `ppmRate`, `CustomChance`, `Cooldown`) VALUES (12574, 0, 3, 0, 0, 0, 65536, 0, 0, 6, 0);
INSERT INTO `spell_proc_event` (`entry`, `SchoolMask`, `SpellFamilyName`, `SpellFamilyMask0`, `SpellFamilyMask1`, `SpellFamilyMask2`, `procFlags`, `procEx`, `ppmRate`, `CustomChance`, `Cooldown`) VALUES (12575, 0, 3, 0, 0, 0, 65536, 0, 0, 10, 0);