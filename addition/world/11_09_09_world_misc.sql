DELETE FROM `npc_vendor` WHERE (`entry`='45289') AND (`item`='60355');DELETE FROM `item_loot_template` WHERE `entry`=39883;
INSERT INTO `item_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES 
(39883, 44707, 2, 1, 1, 1, 1),
(39883, 44721, 8, 1, 1, 1, 1),
(39883, 39898, 20, 1, 1, 1, 1),
(39883, 39899, 20, 1, 1, 1, 1),
(39883, 39896, 20, 1, 1, 1, 1),
(39883, 44722, 30, 1, 1, 5, 8);

UPDATE `item_template` SET `RequiredSkill`='0', `RequiredSkillRank`='0' WHERE `entry` IN (34061,44558,54797);

UPDATE `creature_template` SET `lootid`='0' WHERE (`entry`='240416');
DELETE FROM `creature_loot_template` WHERE (`entry`='240416');
DELETE FROM `disables` WHERE (`entry`='74938');
INSERT INTO `disables` (`sourceType`, `entry`, `flags`) VALUES ('0', '74938', '3');

UPDATE `item_template` SET `RequiredLevel`='84' WHERE (`entry`='67239');
UPDATE `item_template` SET `RequiredLevel`='83' WHERE (`entry`='67237');
UPDATE `item_template` SET `RequiredLevel`='83' WHERE (`entry`='67238');
UPDATE `item_template` SET `RequiredLevel`='85' WHERE (`entry`='67240');
UPDATE `item_template` SET `RequiredLevel`='81' WHERE (`entry`='67243');
UPDATE `item_template` SET `RequiredLevel`='82' WHERE (`entry`='67242');
UPDATE `item_template` SET `RequiredLevel`='85' WHERE (`entry`='67244');
UPDATE `item_template` SET `RequiredLevel`='85' WHERE (`entry`='67246');
UPDATE `item_template` SET `RequiredLevel`='85' WHERE (`entry`='67235');
UPDATE `item_template` SET `RequiredLevel`='85' WHERE (`entry`='67236');
UPDATE `item_template` SET `RequiredLevel`='85' WHERE (`entry`='67234');


REPLACE INTO `spell_proc_event` (`entry`, `Cooldown`) VALUES ('88994', '15');
REPLACE INTO `spell_proc_event` (`entry`, `Cooldown`) VALUES ('88995', '25');

-- Netherwing fix

update creature_template set  faction_A=1015, faction_H=1015 where entry in (23340);
update creature_template set  faction_A=1015, faction_H=1015 where entry in (23342);
update creature_template set  faction_A=1015, faction_H=1015 where entry in (23344);
update creature_template set  faction_A=1015, faction_H=1015 where entry in (23345);
update creature_template set  faction_A=1015, faction_H=1015 where entry in (23346);
update creature_template set  faction_A=1015, faction_H=1015 where entry in (23348);
update creature_template set  faction_A=1015, faction_H=1015 where entry in (23141);
update creature_template set  faction_A=1015, faction_H=1015 where entry in (23427);
update creature_template set  faction_A=1015, faction_H=1015 where entry in (23439);
update creature_template set  faction_A=1015, faction_H=1015 where entry in (23291);
update creature_template set  faction_A=1015, faction_H=1015 where entry in (23144);
update creature_template set  faction_A=1015, faction_H=1015 where entry in (23146);
update creature_template set  faction_A=1015, faction_H=1015 where entry in (23145);
update creature_template set  faction_A=1015, faction_H=1015 where entry in (23489);
update creature_template set  faction_A=1015, faction_H=1015 where entry in (23142);
update creature_template set  faction_A=1015, faction_H=1015 where entry in (23143);
update creature_template set  faction_A=1015, faction_H=1015 where entry in (21801);
update creature_template set  faction_A=1015, faction_H=1015 where entry in (22253);
update creature_template set  faction_A=1015, faction_H=1015 where entry in (23283);
update creature_template set  faction_A=1015, faction_H=1015 where entry in (23501);
update creature_template set  faction_A=1015, faction_H=1015 where entry in (23150);

