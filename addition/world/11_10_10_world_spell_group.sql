DELETE FROM `spell_group` WHERE `spell_id` IN (79474,79635,79477,79632,79481,79468,79631,79480,79471,79470,79472,79469,94160,92679,79637);
-- Battle Elixir
INSERT INTO `spell_group` (`id`, `spell_id`) VALUES ('1', '79474');
INSERT INTO `spell_group` (`id`, `spell_id`) VALUES ('1', '79635');
INSERT INTO `spell_group` (`id`, `spell_id`) VALUES ('1', '79477');
INSERT INTO `spell_group` (`id`, `spell_id`) VALUES ('1', '79632');
INSERT INTO `spell_group` (`id`, `spell_id`) VALUES ('1', '79481');
INSERT INTO `spell_group` (`id`, `spell_id`) VALUES ('1', '79468');
-- Guardian Elixir
INSERT INTO `spell_group` (`id`, `spell_id`) VALUES ('2', '79631');
INSERT INTO `spell_group` (`id`, `spell_id`) VALUES ('2', '79480');
-- Flask
INSERT INTO `spell_group` (`id`, `spell_id`) VALUES ('1', '79471');
INSERT INTO `spell_group` (`id`, `spell_id`) VALUES ('2', '79471');
INSERT INTO `spell_group` (`id`, `spell_id`) VALUES ('1', '79470');
INSERT INTO `spell_group` (`id`, `spell_id`) VALUES ('2', '79470');
INSERT INTO `spell_group` (`id`, `spell_id`) VALUES ('1', '79472');
INSERT INTO `spell_group` (`id`, `spell_id`) VALUES ('2', '79472');
INSERT INTO `spell_group` (`id`, `spell_id`) VALUES ('1', '79469');
INSERT INTO `spell_group` (`id`, `spell_id`) VALUES ('2', '79469');
INSERT INTO `spell_group` (`id`, `spell_id`) VALUES ('1', '94160');
INSERT INTO `spell_group` (`id`, `spell_id`) VALUES ('2', '94160');
INSERT INTO `spell_group` (`id`, `spell_id`) VALUES ('1', '92679');
INSERT INTO `spell_group` (`id`, `spell_id`) VALUES ('2', '92679');
INSERT INTO `spell_group` (`id`, `spell_id`) VALUES ('1', '79637');
INSERT INTO `spell_group` (`id`, `spell_id`) VALUES ('2', '79637');