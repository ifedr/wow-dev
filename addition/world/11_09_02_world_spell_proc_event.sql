DELETE FROM `spell_proc_event` WHERE `entry` = 51525;
DELETE FROM `spell_proc_event` WHERE `entry` = 51526;
DELETE FROM `spell_proc_event` WHERE `entry` = 51527;

INSERT INTO `spell_proc_event` (`entry`, `SchoolMask`, `SpellFamilyName`, `SpellFamilyMask0`, `SpellFamilyMask1`, `SpellFamilyMask2`, `procFlags`, `procEx`, `ppmRate`, `CustomChance`, `Cooldown`) VALUES (51525, 0, 11, 0, 16777232, 131076, 4112, 0, 0, 0, 0);
INSERT INTO `spell_proc_event` (`entry`, `SchoolMask`, `SpellFamilyName`, `SpellFamilyMask0`, `SpellFamilyMask1`, `SpellFamilyMask2`, `procFlags`, `procEx`, `ppmRate`, `CustomChance`, `Cooldown`) VALUES (51526, 0, 11, 0, 16777232, 131076, 4112, 0, 0, 0, 0);
INSERT INTO `spell_proc_event` (`entry`, `SchoolMask`, `SpellFamilyName`, `SpellFamilyMask0`, `SpellFamilyMask1`, `SpellFamilyMask2`, `procFlags`, `procEx`, `ppmRate`, `CustomChance`, `Cooldown`) VALUES (51527, 0, 11, 0, 16777232, 131076, 4112, 0, 0, 0, 0);
