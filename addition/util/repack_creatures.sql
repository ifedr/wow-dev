DROP PROCEDURE IF EXISTS repack_creatures;

DELIMITER $$

CREATE PROCEDURE  repack_creatures()

BEGIN

        DECLARE portable INT DEFAULT 0;
        DECLARE available INT DEFAULT 0;
        SELECT max(guid) FROM `creature` INTO portable;

        portableLoop: WHILE portable > 12000000 DO
                SELECT max(guid) FROM `creature` INTO portable;
                SELECT min(c1.guid + 1) FROM  `creature` c1 LEFT JOIN `creature` c2 ON c1.guid + 1 = c2.guid WHERE c2.guid IS NULL INTO available;

                UPDATE `creature` SET guid = available WHERE guid = portable;


        END WHILE portableLoop;


END $$

DELIMITER ;

CALL repack_creatures();