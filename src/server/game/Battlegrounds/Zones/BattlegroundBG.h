/*
 * Copyright (C) 2005-2011 MaNGOS <http://www.getmangos.com/>
 *
 * Copyright (C) 2008-2011 Trinity <http://www.trinitycore.org/>
 *
 * Copyright (C) 2010-2011 Project SkyFire <http://www.projectskyfire.org/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef __BattlegroundBG_H
#define __BattlegroundBG_H

class Battleground;

enum BG_BG_WorldStates
{
	BG_BG_OP_OCCUPIED_BASES_HORDE       = 1778,
	BG_BG_OP_OCCUPIED_BASES_ALLY        = 1779,
	BG_BG_OP_RESOURCES_ALLY             = 1776,
	BG_BG_OP_RESOURCES_HORDE            = 1777,
	BG_BG_OP_RESOURCES_MAX              = 1780,
	BG_BG_OP_RESOURCES_WARNING          = 1955
};

//// incorrect values from BG_AB_OP_NODESTATES
//const uint32 BG_BG_OP_NODESTATES[5] =    {1767, 1782, 1772, 1792, 1787};
//const uint32 BG_BG_OP_NODEICONS[5]  =    {1842, 1846, 1845, 1844, 1843};
const uint32 BG_BG_OP_NODESTATES[3] =    {1767, 1782, 1772};
const uint32 BG_BG_OP_NODEICONS[3]  =    {1842, 1846, 1845};

enum BG_BG_NodeObjectId
{
//	BG_BG_OBJECTID_NODE_BANNER_0    = 205557,       // Lighthouse banner
	BG_BG_OBJECTID_NODE_BANNER_0    = 180087,       // Stables banner
	BG_BG_OBJECTID_NODE_BANNER_1    = 180088,       // Blacksmith banner
	BG_BG_OBJECTID_NODE_BANNER_2    = 180089,       // Farm banner

//    BG_BG_OBJECTID_NODE_BANNER_0    = 205557,       // Lighthouse banner
//    BG_BG_OBJECTID_NODE_BANNER_1    = 208782,       // Mine banner
//    BG_BG_OBJECTID_NODE_BANNER_2    = 208785,       // Watterworks banner
};

enum BG_BG_ObjectType
{
    BG_BG_OBJECT_BANNER_NEUTRAL          = 0,
    BG_BG_OBJECT_BANNER_CONT_A           = 1,
    BG_BG_OBJECT_BANNER_CONT_H           = 2,
    BG_BG_OBJECT_BANNER_ALLY             = 3,
    BG_BG_OBJECT_BANNER_HORDE            = 4,
    BG_BG_OBJECT_AURA_ALLY               = 5,
    BG_BG_OBJECT_AURA_HORDE              = 6,
    BG_BG_OBJECT_AURA_CONTESTED          = 7,

    BG_BG_OBJECT_GATE_A                  = 24,
    BG_BG_OBJECT_GATE_H                  = 25,
    BG_BG_OBJECT_MAX                     = 26,
};

/* Object id templates from DB */
enum BG_BG_ObjectTypes
{
	//// from AB battleground
	BG_BG_OBJECTID_BANNER_A             = 180058,
	BG_BG_OBJECTID_BANNER_CONT_A        = 180059,
	BG_BG_OBJECTID_BANNER_H             = 180060,
	BG_BG_OBJECTID_BANNER_CONT_H        = 180061,

	BG_BG_OBJECTID_AURA_A               = 180100,
	BG_BG_OBJECTID_AURA_H               = 180101,
	BG_BG_OBJECTID_AURA_C               = 180102,

/*
    BG_BG_OBJECTID_BANNER_A             = 180076,
    BG_BG_OBJECTID_BANNER_CONT_A        = 180419,  // 180098
    BG_BG_OBJECTID_BANNER_H             = 180070,
    BG_BG_OBJECTID_BANNER_CONT_H        = 180420,  // 180099

    BG_BG_OBJECTID_AURA_A               = 180100,
    BG_BG_OBJECTID_AURA_H               = 180101,
    BG_BG_OBJECTID_AURA_C               = 180102,
*/
	BG_BG_OBJECTID_GATE_A               = 180255,
	BG_BG_OBJECTID_GATE_H               = 180256
//    BG_BG_OBJECTID_GATE_A               = 207177,	//205496,
//    BG_BG_OBJECTID_GATE_H               = 207178
};

enum BG_BG_Timers
{
    BG_BG_FLAG_CAPTURING_TIME   = 60000,
};

enum BG_BG_Score
{
    BG_BG_WARNING_NEAR_VICTORY_SCORE    = 1800,
    BG_BG_MAX_TEAM_SCORE                = 2000
};

/* do NOT change the order, else wrong behaviour */
enum BG_BG_BattlegroundNodes
{
    BG_BG_NODE_LIGHTHOUSE       = 0,
    BG_BG_NODE_WATERWORKS       = 1,
    BG_BG_NODE_MINE             = 2,

    BG_BG_DYNAMIC_NODES_COUNT   = 3,                        // dynamic nodes that can be captured

    BG_BG_SPIRIT_ALIANCE        = 3,
    BG_BG_SPIRIT_HORDE          = 4,

    BG_BG_ALL_NODES_COUNT       = 5,                        // all nodes (dynamic and static)
};

enum BG_BG_NodeStatus
{
    BG_BG_NODE_TYPE_NEUTRAL             = 0,
    BG_BG_NODE_TYPE_CONTESTED           = 1,
    BG_BG_NODE_STATUS_ALLY_CONTESTED    = 1,
    BG_BG_NODE_STATUS_HORDE_CONTESTED   = 2,
    BG_BG_NODE_TYPE_OCCUPIED            = 3,
    BG_BG_NODE_STATUS_ALLY_OCCUPIED     = 3,
    BG_BG_NODE_STATUS_HORDE_OCCUPIED    = 4
};

enum BG_BG_Sounds
{
    BG_BG_SOUND_NODE_CLAIMED            = 8192,
    BG_BG_SOUND_NODE_CAPTURED_ALLIANCE  = 8173,
    BG_BG_SOUND_NODE_CAPTURED_HORDE     = 8213,
    BG_BG_SOUND_NODE_ASSAULTED_ALLIANCE = 8212,
    BG_BG_SOUND_NODE_ASSAULTED_HORDE    = 8174,
    BG_BG_SOUND_NEAR_VICTORY            = 8456
};

enum BG_BG_Objectives
{
    BG_OBJECTIVE_ASSAULT_BASE           = 122,
    BG_OBJECTIVE_DEFEND_BASE            = 123
};

#define BG_BG_NotABBGWeekendHonorTicks      330
#define BG_BG_ABBGWeekendHonorTicks         200
#define BG_BG_NotABBGWeekendReputationTicks 200
#define BG_BG_ABBGWeekendReputationTicks    150


// x, y, z, o
const float BG_BG_NodePositions[BG_BG_DYNAMIC_NODES_COUNT][4] = {
	{1057.879f, 1278.227f, 4.57818f, 4.837269f}, //4.837269f},         // Lighthouse
	{980.1053f, 948.8199f, 13.1707f, 2.827435f}, //2.827435f},         // Watterworks
	{1251.0894f, 958.345f, 6.20254f, 2.847070f}, //2.847070f},         // Mine
};

// x, y, z, o, rot0, rot1, rot2, rot3
const float BG_BG_DoorPositions[2][8] = {
    {918.876f, 1336.56f, 27.6195f, 2.77481f, 0.0f, 0.0f, 0.983231f, 0.182367f},
    {1396.15f, 977.014f, 7.43169f, 6.27043f, 0.0f, 0.0f, 0.006378f, -0.99998f}
};

const uint32 BG_BG_TickIntervals[4] = {0, 12000, 5000, 1000};
const uint32 BG_BG_TickPoints[4] = {0, 10, 10, 30};

// WorldSafeLocs ids for 3 nodes, and for ally, and horde starting location
// lighhouse, watter, mine, ally, horde
const uint32 BG_BG_GraveyardIds[BG_BG_ALL_NODES_COUNT] = {1736, 1738, 1735, 1740, 1739};

// x, y, z, o
const float BG_BG_SpiritGuidePos[BG_BG_ALL_NODES_COUNT][4] = {
	{1037.03f, 1341.00f, 12.00f, 4.6f},                   // lighthouse
	{885.34f, 935.75f, 25.67f, 0.52f},                    // watter
	{1252.00f, 833.50f, 27.80f, 1.31f},                   // mine
	{901.60f, 1341.34f, 27.48f, 5.98f},                   // alliance starting base
	{1407.23f, 976.24f, 7.43169f, 2.90f}                  // horde starting base
};

struct BG_BG_BannerTimer
{
    uint32      timer;
    uint8       type;
    uint8       teamIndex;
};

class BattlegroundBGScore : public BattlegroundScore
{
    public:
        BattlegroundBGScore(): BasesAssaulted(0), BasesDefended(0) {};
        virtual ~BattlegroundBGScore() {};
        uint32 BasesAssaulted;
        uint32 BasesDefended;
};

class BattlegroundBG : public Battleground
{
    friend class BattlegroundMgr;

    public:
        BattlegroundBG();
        ~BattlegroundBG();
        void Update(uint32 diff);

        /* inherited from BattlegroundClass */
        virtual void AddPlayer(Player *plr);
        virtual void StartingEventCloseDoors();
        virtual void StartingEventOpenDoors();

        void RemovePlayer(Player *plr, uint64 guid);
        void HandleAreaTrigger(Player *Source, uint32 Trigger);
        bool SetupBattleground();
        void EndBattleground(uint32 winner);

        /* Scorekeeping */
        void UpdatePlayerScore(Player *Source, uint32 type, uint32 value, bool doAddHonor = true);

	virtual void Reset();
	virtual WorldSafeLocsEntry const* GetClosestGraveYard(Player* player);

	virtual void FillInitialWorldStates(WorldPacket& data);

	/* Nodes occupying */
        virtual void EventPlayerClickedOnFlag(Player *source, GameObject* target_obj);

        /* achievement req. */
        bool IsAllNodesConrolledByTeam(uint32 team) const;  // overwrited

    private:
	/* Gameobject spawning/despawning */
        void _CreateBanner(uint8 node, uint8 type, uint8 teamIndex, bool delay);
        void _DelBanner(uint8 node, uint8 type, uint8 teamIndex);
        void _SendNodeUpdate(uint8 node);

        /* Creature spawning/despawning */
        // TODO: working, scripted peons spawning
        void _NodeOccupied(uint8 node,Team team);
        void _NodeDeOccupied(uint8 node);

        int32 _GetNodeNameId(uint8 node);

    private:
	/* Nodes info:
            0: neutral
            1: ally contested
            2: horde contested
            3: ally occupied
            4: horde occupied     */

        uint8               m_Nodes[BG_BG_DYNAMIC_NODES_COUNT];
        uint8               m_prevNodes[BG_BG_DYNAMIC_NODES_COUNT];
        BG_BG_BannerTimer   m_BannerTimers[BG_BG_DYNAMIC_NODES_COUNT];
        uint32              m_NodeTimers[BG_BG_DYNAMIC_NODES_COUNT];

        uint32              m_lastTick[BG_TEAMS_COUNT];
        uint32              m_HonorScoreTics[BG_TEAMS_COUNT];
        uint32              m_ReputationScoreTics[BG_TEAMS_COUNT];

        bool                m_IsInformedNearVictory;
        uint32              m_HonorTics;
        uint32              m_ReputationTics;
};
#endif
