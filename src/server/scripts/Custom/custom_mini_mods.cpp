/*
 * Auto join in RandomBG at login.
 * Set max health and mana after duel, reset cooldowns.
 * Resurrect command for players. INSERT INTO command VALUES ('resurrect', 0, 'Resurrect yourself');
 * ChangeAccount command for GMs. INSERT INTO command VALUES ('changeaccount', 3, '.changeaccount $name $password $new_acc');
 */

#include "ScriptPCH.h"
#include "Chat.h"
#include "Config.h"
#include "AccountMgr.h"

// config
bool enableBGAutoJoin = false;
bool enableAfterDuel  = false;
bool enableCommands   = false;

class custom_mini_mods_ws : public WorldScript
{
    public:
        custom_mini_mods_ws() : WorldScript("custom_mini_mods_ws") {}

    void OnConfigLoad(bool /*reload*/)
    {
        enableBGAutoJoin = sConfig->GetBoolDefault("Custom.BGAutoJoin", false);
        enableAfterDuel  = sConfig->GetBoolDefault("Custom.AfterDuel", false);
        enableCommands   = sConfig->GetBoolDefault("Custom.Commands", false);
    }
};

class custom_mini_mods_ps : public PlayerScript
{
    public:
         custom_mini_mods_ps() : PlayerScript("custom_mini_mods_ps") {}

    void OnLogin(Player* pl)
    {
        if (enableBGAutoJoin)
        {
            // Generating BattlemasterJoinOpcode
            WorldPacket data;
            data << uint8(0);     // joinAsGroup
            data << uint32(0);    // unk
            data << uint32(32);   // random bg
            data << uint32(0);    // unk2
            pl->GetSession()->HandleBattlemasterJoinOpcode(data);
        }
    }

    void OnDuelEnd(Player* pWinner, Player* pLooser, DuelCompleteType type)
    {
        if (enableAfterDuel)
        {
            if (type == DUEL_WON && (pWinner->GetAreaId()==4982 || pWinner->GetAreaId()==12 || pWinner->GetAreaId()==1741 || pWinner->GetAreaId()==2177 || pWinner->GetAreaId()==4564))
            {
                pWinner->RemoveArenaSpellCooldowns();
                pLooser->RemoveArenaSpellCooldowns();
                pWinner->SetHealth(pWinner->GetMaxHealth());
                pLooser->SetHealth(pLooser->GetMaxHealth());
                pWinner->SetPower(POWER_MANA, pWinner->GetMaxPower(POWER_MANA));
                pLooser->SetPower(POWER_MANA, pLooser->GetMaxPower(POWER_MANA));
            }
        }
    }
};

class custom_mini_mods_cs : public CommandScript
{
    public:
        custom_mini_mods_cs() : CommandScript("custom_mini_mods_cs") { }

        static bool HandleResurrectCommand(ChatHandler* handler, const char* /*args*/) 
        {
            if (!enableCommands)
                return false;

            Player* pl = handler->GetSession()->GetPlayer();

            if (!pl->isDead())
                return false;

            pl->ResurrectPlayer(0.5f, true);
            pl->SpawnCorpseBones();
            pl->SaveToDB();
            return true;
        }

        static bool HandleChangeAccountCommand(ChatHandler* handler, const char* args) 
        {
            //if (!enableCommands)
            //    return false;

			// .changeaccount $name /*$password*/ $new_acc
			if (!*args)
			{
				handler->SendSysMessage(LANG_CMD_SYNTAX);
				handler->SetSentErrorMessage(true);
				return false;
			}

			// 1. get player, passwd, account from string
			char* _name    = strtok((char*)args, " ");
			//char* _pass    = strtok(NULL, " ");
			char* _new_acc = strtok(NULL, " ");

            if (!_name || /*!_pass ||*/ !_new_acc)
				return false;

			std::string name = _name;
			//std::string pass = _pass;
			std::string new_acc = _new_acc;

			// 2. checks
			//    is player online - doesnt work
            if (sObjectMgr->GetPlayer(name.c_str()))
			{
				handler->SendSysMessage("Error: player should be offline.");
				handler->SetSentErrorMessage(true);
				return false;
			}
			//    is player exist
            QueryResult result = CharacterDatabase.PQuery("SELECT characters.guid, characters.account FROM characters WHERE characters.name = '%s'", name.c_str());
            if (!result)
            {
				handler->SendSysMessage("Error: player does not exists.");
                handler->SetSentErrorMessage(true);
                return false;
            }
			//    get player guid, acc id
			Field *fields = result->Fetch();
			uint64 guid  = fields[0].GetUInt64();
			uint32 accId = fields[1].GetUInt32();
			//    check password - disable
			//if (!sAccountMgr->CheckPassword(accId, pass))
			//{
			//	handler->SendSysMessage("Error: wrong password.");
			//	handler->SetSentErrorMessage(true);
			//	return false;
			//}
			//    check new acc
			uint32 new_accId = sAccountMgr->GetId(new_acc);
			if (!new_accId)
			{
				handler->SendSysMessage("Error: new account does not exists.");
				handler->SetSentErrorMessage(true);
				return false;
			}
            // 3. ok, let's do it
			CharacterDatabase.PExecute("UPDATE characters SET account = %u WHERE guid = %u", new_accId, guid);
			// 4. profit
            handler->SendSysMessage("Account has been changed.");

            return true;
        }

        ChatCommand* GetCommands() const
        {
            static ChatCommand CustomCommandTable[] =
            {
                { "resurrect",     SEC_PLAYER,        false,   &HandleResurrectCommand,     "", NULL },
				{ "changeaccount", SEC_ADMINISTRATOR, true,    &HandleChangeAccountCommand, "", NULL },
                { NULL,            0,                 false,   NULL,                        "", NULL }
            };
            return CustomCommandTable;
        }
};



void AddSC_custom_mini_mods()
{
    new custom_mini_mods_ws;
    new custom_mini_mods_ps;
    new custom_mini_mods_cs;
}
