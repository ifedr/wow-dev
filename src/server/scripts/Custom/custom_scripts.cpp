/*
 * Gossip: Currency Exchange
 * GO: Chest with currency
 * 
 */

#include "ScriptPCH.h"

class custom_currency_exchange : public CreatureScript
{
    public:
        custom_currency_exchange() : CreatureScript("custom_currency_exchange") {}

        enum Enums
        {
            GOSSIP_ACTION_HONOR_TO_JUSTICE  = GOSSIP_ACTION_INFO_DEF + 1,
            GOSSIP_ACTION_CONQUEST_TO_VALOR = GOSSIP_ACTION_INFO_DEF + 2,

            CURRENCY_HONOR_POINTS    = 392, // pvp low
            CURRENCY_CONQUEST_POINTS = 390, // pvp high
            CURRENCY_JUSTICE_POINTS  = 395, // pve low
            CURRENCY_VALOR_POINTS    = 396, // pve high

            RESULT_SUCCESS             = 0,
            RESULT_WRONG_NUMBER        = 1,
            RESULT_NOT_ENOUGH_CURRENCY = 2,
        };

        uint32 currId[2]; // [0]-from, [1]-to
        int32  currCount;
        
        bool OnGossipHello(Player* pPlayer, Creature* pCreature)
        {
            pPlayer->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_MONEY_BAG, "Honor Points -> Justice Points (1:1)", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_HONOR_TO_JUSTICE, "", 0, true);
            pPlayer->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_MONEY_BAG, "Conquest Points -> Valor Points (1:1)", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_CONQUEST_TO_VALOR, "", 0, true);

            pPlayer->PlayerTalkClass->SendGossipMenu(2, pCreature->GetGUID());
            
            return true;
        }

        bool OnGossipSelect(Player* pPlayer, Creature* pCreature, uint32 /*uiSender*/, uint32 uiAction)
        {
            return true;
        }

        bool OnGossipSelectCode(Player* pPlayer, Creature* pCreature, uint32 uiSender, uint32 uiAction, const char* code)
        {
            pPlayer->PlayerTalkClass->ClearMenus();
            if (uiSender != GOSSIP_SENDER_MAIN)
                return false;

            switch (uiAction)
            {
                case GOSSIP_ACTION_HONOR_TO_JUSTICE:
                    currId[0] = CURRENCY_HONOR_POINTS;
                    currId[1] = CURRENCY_JUSTICE_POINTS;
                    break;
                case GOSSIP_ACTION_CONQUEST_TO_VALOR:
                    currId[0] = CURRENCY_CONQUEST_POINTS;
                    currId[1] = CURRENCY_VALOR_POINTS;
                    break;
                default:
                    return false;
            }

            uint32 result = HandleGossip(pPlayer, code);
            
            switch (result)
            {
                case RESULT_SUCCESS:
                    pPlayer->BroadcastMessage("Success!");
                    break;
                case RESULT_WRONG_NUMBER:
                    pPlayer->BroadcastMessage("Error: bad count. It should be number between 1 and 9999.");
                    break;
                case RESULT_NOT_ENOUGH_CURRENCY:
                    pPlayer->BroadcastMessage("Error: not enough currency.");
                    break;
                default:
                    break;
            }

            pPlayer->CLOSE_GOSSIP_MENU();

            return true;
        }

        uint32 HandleGossip(Player* pPlayer, const char* code)
        {
            if (std::string(code).length() > 4)
                return RESULT_WRONG_NUMBER;

            try
            {
                currCount = (int32)atoi(code);
            }
            catch(...)
            {
                currCount = 0;
                return RESULT_WRONG_NUMBER;
            }

            if (currCount <= 0 || currCount > 9999)
                return RESULT_WRONG_NUMBER;

            currCount *= PLAYER_CURRENCY_PRECISION;

            // Check whether player has this currency
            if (!pPlayer->HasCurrency(currId[0], currCount))
                return RESULT_NOT_ENOUGH_CURRENCY;

            // All ok, let's do it
            pPlayer->ModifyCurrency(currId[0], -currCount, true);
            pPlayer->ModifyCurrency(currId[1], currCount, true);
            
            return RESULT_SUCCESS;
        }
};

class custom_chest_with_currency : public GameObjectScript
{
public:
    custom_chest_with_currency() : GameObjectScript("custom_chest_with_currency") {}

    bool OnGossipHello(Player* player, GameObject* /*go*/)
    {
        player->ModifyCurrency(CURRENCY_TYPE_CONQUEST_POINTS, 300 * PLAYER_CURRENCY_PRECISION, true);
        player->ModifyCurrency(CURRENCY_TYPE_HONOR_POINTS, 300 * PLAYER_CURRENCY_PRECISION, true);

        char message[64]; 
        sprintf(message, "%s wins in Gurubashi event!", player->GetName());
        sWorld->SendServerMessage(SERVER_MSG_STRING, message);
        return false;
    }
};

void AddSC_custom_scripts()
{
    new custom_currency_exchange();
    new custom_chest_with_currency();
}