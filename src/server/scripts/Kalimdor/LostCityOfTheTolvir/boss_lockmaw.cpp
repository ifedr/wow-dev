/* ScriptData
SDName: Boss Lockmaw & Augh
SD%Complete: 100%
SDComment: Augh = SAI
SDCategory: Shadowfang Keep
EndScriptData */

#include "ScriptPCH.h"
#include "lost_city_of_the_tolvir.h"

enum Spells
{
    // Lockmaw
    SPELL_DUST_FLAIL            = 81642,
    SPELL_SCENT_OF_BLOOD        = 81690,
    SPELL_VENOMOUS_RAGE         = 81706,
    SPELL_VISCOUS_POISON        = 81630,
    // Augh
    SPELL_PARALYTIC_BLOW_DART   = 84799,
	SPELL_WHIRLWIND				= 91408,
	SPELL_DRAGON_BREATH			= 90026,
	SPELL_FRENZY				= 91415,
};

enum Events
{
    EVENT_DESPAWN_AUGH_SCENT = 1,
	EVENT_DESPAWN_AUGH_WIRLWIND = 2,
	EVENT_WHIRLWIND = 3,
	EVENT_DRAGON_BREATH = 4,
	EVENT_PARALYTIC_BLOW_DART = 5,
};

const Position SummonLocations[6] =
{
    //Frenzied Crocolisks
    {-11033.29f, -1674.57f, -0.56f, 1.09f},
    {-11029.84f, -1673.09f, -0.37f, 2.33f},
    {-11007.25f, -1666.37f, -0.23f, 2.46f},
    {-11006.83f, -1666.85f, -0.25f, 2.23f},
    {-11031.00f, -1653.59f,  0.86f, 2.42f},
    {-11069.23f, -1667.72f,  0.75f, 0.72f},
};

class boss_lockmaw : public CreatureScript
{
    public:
        boss_lockmaw() : CreatureScript("boss_lockmaw") {}

        CreatureAI* GetAI(Creature* pCreature) const
        {
           return new boss_lockmawAI(pCreature);
        }

        struct boss_lockmawAI : public BossAI
        {
            boss_lockmawAI(Creature* pCreature) : BossAI(pCreature, DATA_LOCKMAW)
            {
				me->ApplySpellImmune(0, IMMUNITY_EFFECT, SPELL_EFFECT_KNOCK_BACK, true);
				me->ApplySpellImmune(0, IMMUNITY_MECHANIC, MECHANIC_GRIP, true);
				pInstance = pCreature->GetInstanceScript();
            }

            InstanceScript* pInstance;
			EventMap events;
            bool check_in;

            uint32 DustTailTimer;
            uint32 ScentOfBloodTimer;
            uint32 ViscousPoisonTimer;
            uint32 WhirlwindTimer;
			uint32 venomous_rage_casted;

            bool Rage;
            std::list<uint64> SummonList;

            void Reset ()
            {
				events.Reset();
				if (pInstance && (pInstance->GetData(DATA_LOCKMAW_EVENT) != DONE &&  !check_in))
					pInstance->SetData(DATA_LOCKMAW_EVENT, NOT_STARTED);

                DustTailTimer = 15000;
                ScentOfBloodTimer = 13000;
                ViscousPoisonTimer = 7000;
                WhirlwindTimer = 35000;
				venomous_rage_casted = 0;

                Rage = false;
                RemoveSummons();
				check_in = false;
            }

            void RemoveSummons()
            {
                if (SummonList.empty())
                    return;

                for (std::list<uint64>::const_iterator itr = SummonList.begin(); itr != SummonList.end(); ++itr)
                {
                    if (Creature* pTemp = Unit::GetCreature(*me, *itr))
                        if (pTemp)
                            pTemp->DisappearAndDie();
                }
                SummonList.clear();
            }

            void JustSummoned(Creature* pSummon)
            {
                switch (pSummon->GetEntry())
                {
                    case CREATURE_AUGH_SCENT:
                    case CREATURE_AUGH_WHIRLWIND:
                        if (Unit* pTarget = SelectTarget(SELECT_TARGET_RANDOM,0))
                            pSummon->AI()->AttackStart(pTarget);
                        SummonList.push_back(pSummon->GetGUID());
                        break;
                }
            }

            void JustDied(Unit* /*Kill*/)
            {
				if (pInstance)
				{
					pInstance->SetData(DATA_LOCKMAW_EVENT, DONE);
					if (IsHeroic())
					{
						int32 income = 7500;
						pInstance->DoUpdateCurrency(CURRENCY_TYPE_JUSTICE_POINTS, income);
					}
				}

                if (IsHeroic())
                    me->SummonCreature(CREATURE_AUGH, SummonLocations[5], TEMPSUMMON_MANUAL_DESPAWN);
            }

            void EnterCombat(Unit* /*Ent*/)
            {
                DoZoneInCombat();

                if (pInstance)
					pInstance->SetData(DATA_LOCKMAW_EVENT, IN_PROGRESS);
            }

            void UpdateAI(const uint32 diff)
            {
                if (!UpdateVictim())
                    return;

				if (me->GetHealthPct() < 30 && venomous_rage_casted == 0)
                {
                    DoCast(me, SPELL_VENOMOUS_RAGE);
                    venomous_rage_casted = 1;
                }

                if (DustTailTimer <= diff)
                {
                    DoCast(me->getVictim(), SPELL_DUST_FLAIL);
                    DustTailTimer = 25000;
                } else DustTailTimer -= diff;

                if (ScentOfBloodTimer <= diff)
                {
                    me->SummonCreature(CREATURE_AUGH_SCENT, SummonLocations[4], TEMPSUMMON_TIMED_DESPAWN_OUT_OF_COMBAT, 15 *IN_MILLISECONDS);
                    ScentOfBloodTimer = 45000;
                } else ScentOfBloodTimer -= diff;

                if (WhirlwindTimer <= diff)
                {
                    me->SummonCreature(CREATURE_AUGH_WHIRLWIND, SummonLocations[4], TEMPSUMMON_TIMED_DESPAWN_OUT_OF_COMBAT, 20 *IN_MILLISECONDS);
                    WhirlwindTimer = 45000;
                } else WhirlwindTimer -= diff;

                if (ViscousPoisonTimer <= diff)
                {
                    if (Unit* pTarget = SelectTarget(SELECT_TARGET_RANDOM, 0, 100, true))
                        DoCast(pTarget, SPELL_VISCOUS_POISON);
                    ViscousPoisonTimer = 12000;
                } else ViscousPoisonTimer -= diff;

                DoMeleeAttackIfReady();
            }
        };
};

class npc_augh_scent : public CreatureScript
{
    public:
        npc_augh_scent() : CreatureScript("npc_augh_scent") { }

        struct npc_augh_scentAI : public BossAI
        {
            npc_augh_scentAI(Creature *pCreature) : BossAI(pCreature, DATA_AUGH_SCENT)
            {
            }

            uint32 ParalyticBlowDartTimer;

            bool Scent;

            void Reset()
            {
                ParalyticBlowDartTimer = 3000;
				events.ScheduleEvent(EVENT_DESPAWN_AUGH_SCENT, 15 *IN_MILLISECONDS);
                Scent = false;
            }

            void UpdateAI(const uint32 diff)
            {
                if (Scent == false)
                {
                    if (Unit* pTarget = SelectTarget(SELECT_TARGET_RANDOM, 0, 100, true))
                    {
                        Talk(0);
                        DoCast(pTarget, SPELL_SCENT_OF_BLOOD);
                        for(uint8 i=0; i<4; i++)
                        {
                            Creature* Crocolisk = me->SummonCreature(CREATURE_FRENZIED_CROCOLISK, SummonLocations[i], TEMPSUMMON_CORPSE_DESPAWN);
                            Crocolisk->AddThreat(pTarget, 0.0f);
                            DoZoneInCombat(Crocolisk);
                        }
                    }
                    Scent = true;
                }

                if (ParalyticBlowDartTimer <= diff)
                {
                    if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, 100, true))
                        DoCast(target, SPELL_PARALYTIC_BLOW_DART, true);
                    ParalyticBlowDartTimer = 15000;
                } else ParalyticBlowDartTimer -= diff;

				events.Update(diff);

            if (me->HasUnitState(UNIT_STAT_CASTING))
                    return;

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                case EVENT_DESPAWN_AUGH_SCENT:
                    me->DespawnOrUnsummon();
                    break;
                }
            }
            }
        };

        CreatureAI* GetAI(Creature* pCreature) const
        {
            return new npc_augh_scentAI(pCreature);
        }
};

class npc_augh_whirlwind : public CreatureScript
{
    public:
        npc_augh_whirlwind() : CreatureScript("npc_augh_whirlwind") { }

        struct npc_augh_whirlwindAI : public BossAI
        {
            npc_augh_whirlwindAI(Creature *pCreature) : BossAI(pCreature, DATA_AUGH_WHIRLWIND)
            {
            }

            bool Targeted;

            void Reset()
            {
                Targeted = false;
				events.ScheduleEvent(EVENT_DESPAWN_AUGH_WIRLWIND, 20 *IN_MILLISECONDS);
            }

            void UpdateAI(const uint32 diff)
            {
                if (Targeted == false)
                {
                    if (Unit* pTarget = SelectTarget(SELECT_TARGET_RANDOM, 0, 100, true))
                    {
                        DoCast(me, 84784);
                        Talk(0);
                        me->GetMotionMaster()->MoveChase(pTarget);
                    }
                    Targeted = true;
                }

				events.Update(diff);

            if (me->HasUnitState(UNIT_STAT_CASTING))
                    return;

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                case EVENT_DESPAWN_AUGH_WIRLWIND:
                    me->DespawnOrUnsummon();
                    break;
                }
            }
            }
        };

        CreatureAI* GetAI(Creature* pCreature) const
        {
            return new npc_augh_whirlwindAI(pCreature);
        }
};

class boss_augh : public CreatureScript
{
    public:
        boss_augh() : CreatureScript("boss_augh") { }

        struct boss_aughAI : public BossAI
        {
            boss_aughAI(Creature *pCreature) : BossAI(pCreature, DATA_AUGH)
            {
				 me->ApplySpellImmune(0, IMMUNITY_EFFECT, SPELL_EFFECT_KNOCK_BACK, true);
				 me->ApplySpellImmune(0, IMMUNITY_MECHANIC, MECHANIC_GRIP, true);
				 pInstance = pCreature->GetInstanceScript();
            }

			InstanceScript *pInstance;
			EventMap events;

			uint32 frenzy;

            void Reset()
            {
                _Reset();
				events.ScheduleEvent(EVENT_PARALYTIC_BLOW_DART, 1 *IN_MILLISECONDS);
				events.ScheduleEvent(EVENT_WHIRLWIND, 2 *IN_MILLISECONDS);
				events.ScheduleEvent(EVENT_DRAGON_BREATH, 15 *IN_MILLISECONDS);
				frenzy = 0;
            }

			void JustDied(Unit* /*killer*/)
            {
				_JustDied();

				Map* map = instance->instance;
				Map::PlayerList const &players = map->GetPlayers();
				if (!players.isEmpty())
                {
                    if (Player* player = players.begin()->getSource())
                    {
						if (IsHeroic())
							player->ModifyCurrency(395, 7500);
                    }
                }
            }

            void UpdateAI(const uint32 diff)
            {
				if (!UpdateVictim())
                    return;

				if (me->GetHealthPct() < 30 && frenzy == 0)
				{
                    DoCast(me, SPELL_FRENZY);
                    frenzy = 1;
				}

				events.Update(diff);

				if (me->HasUnitState(UNIT_STAT_CASTING))
                    return;

				while (uint32 eventId = events.ExecuteEvent())
				{
                    switch (eventId)
                    {
					case EVENT_PARALYTIC_BLOW_DART:
						DoCast(SPELL_PARALYTIC_BLOW_DART);
						events.RescheduleEvent(EVENT_PARALYTIC_BLOW_DART, 15 *IN_MILLISECONDS);
						break;
					case EVENT_WHIRLWIND:
						DoCast(SPELL_WHIRLWIND);
						events.RescheduleEvent(EVENT_WHIRLWIND, 16 *IN_MILLISECONDS);
						break;
					case EVENT_DRAGON_BREATH:
						DoCast(SPELL_DRAGON_BREATH);
						events.RescheduleEvent(EVENT_DRAGON_BREATH, 20 *IN_MILLISECONDS);
						break;
                    }
				}
				DoMeleeAttackIfReady();
            }
        };

        CreatureAI* GetAI(Creature* pCreature) const
        {
            return new boss_aughAI(pCreature);
        }
};

void AddSC_boss_lockmaw()
{
    new boss_lockmaw();
    new npc_augh_scent();
    new npc_augh_whirlwind();
	new boss_augh();
}