/*
* Copyright (C) 2010-2011 Project SkyFire <http://www.projectskyfire.org/>
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "ScriptPCH.h"
#include "lost_city_of_the_tolvir.h"
#include "Group.h"

enum Spells
{
    SPELL_BERSERK               = 47008,
    SPELL_ABSORB_STORMS			= 83151,
	SPELL_DEFLECTING_WIND		= 84589,
	SPELL_STORM_BOLT			= 91853,
	SPELL_STORM_BOLT1			= 73564,
	SPELL_WAILING_WIND			= 83066,
	SPELL_GATHERED_STORM		= 84982,
	SPELL_DEPLETION				= 84550,
	SPELL_CHAIN_LIGHTNING		= 83455,
	SPELL_LIGHTING_CHARGE		= 91871,
	SPELL_LIGHTING_NOVA			= 84544,
	SPELL_THUNDER_CRASH			= 84521,
};

enum Events
{
    EVENT_BERSERK = 1,
    EVENT_ABSORB_STORM = 2,
	EVENT_DEFLECTING_WIND = 3,
	EVENT_STORM_BOLT = 4,
	EVENT_STORM_BOLT1 = 5,
	EVENT_WAILING_WIND = 6,
	EVENT_WAILING_WIND1 = 7,
	EVENT_GATHERED_STORM = 8,
	EVENT_DEPLETION	= 9,
	EVENT_CHAIN_LIGHTNING = 10,
	EVENT_LIGHTING_NOVA	= 11,
	EVENT_LIGHTING_CHARGE = 12,
	EVENT_MIMION_SUMMON = 13,
	EVENT_MIMION_SUMMON1 = 14,
	EVENT_MIMION_SUMMON2 = 15,
	EVENT_MIMION_SUMMON3 = 16,
	EVENT_MIMION_SUMMON4 = 17,
	EVENT_SERVANT_SUMMON = 18,
	EVENT_SERVANT_SUMMON1 = 19,
	EVENT_SERVANT_SUMMON2 = 20,
};

enum Phases
{
    PHASE_ALL           = 0,
    PHASE_NORMAL        = 1,
    PHASE_DEFLECTING_WIND    = 2,
};

enum Yells
{
    SAY_AGGRO                   = -1755005,
    SAY_REPETANCE_1             = -1755006,
    SAY_REPETANCE_2             = -1755007,
    SAY_DEATH                   = -1755008,
    SAY_KILL                    = -1755009,
};

const Position PosSummon[6] =
{
    {-10949.400391f, -1407.969971f, 28.049999f, 0.0f},
	{-10922.328125f, -1422.982300f, 28.090771f, 0.0f},
	{-10979.182617f, -1418.634888f, 28.091806f, 0.0f},
	{-10962.189453f, -1398.535645f, 28.088137f, 0.0f},
	{-10935.017578f, -1397.594604f, 28.088663f, 0.0f},
	{-10947.481445f, -1420.527588f, 28.087818f, 0.0f},
};

class boss_siamat: public CreatureScript
{
    public:
        boss_siamat() : CreatureScript("boss_siamat") { }

    struct boss_siamatAI: public BossAI
    {
        boss_siamatAI(Creature* pCreature) : BossAI(pCreature, DATA_SIAMAT) 
	 {
	     me->ApplySpellImmune(0, IMMUNITY_EFFECT, SPELL_EFFECT_KNOCK_BACK, true);
	     me->ApplySpellImmune(0, IMMUNITY_MECHANIC, MECHANIC_GRIP, true);
		 pInstance = pCreature->GetInstanceScript();
	 }

        InstanceScript *pInstance;
        EventMap events;
        bool check_in;
		
		uint8 Phase;
        bool Phased;
        uint8 SpawnCount;
        uint8 PhaseCount;

        uint32 StormBoltTimer;
        uint32 Phase2EndTimer;
		uint32 wailing_wind_casted;

        void Reset()
        {
			events.Reset();
            if (pInstance && (pInstance->GetData(DATA_SIAMAT_EVENT) != DONE &&  !check_in))
                pInstance->SetData(DATA_SIAMAT_EVENT, NOT_STARTED);

            Phased = false;
			check_in = false;

            Phase = PHASE_NORMAL;

            SpawnCount = 3;
            PhaseCount = 0;

            StormBoltTimer = 5000;
			wailing_wind_casted = 0;

            me->RemoveAurasDueToSpell(SPELL_DEFLECTING_WIND);
            me->RemoveAurasDueToSpell(SPELL_GATHERED_STORM);

            me->GetMotionMaster()->MoveTargetedHome();
			summons.DespawnAll();
        }

		void EnterCombat(Unit* /*Ent*/)
        {
			DoZoneInCombat();

            if (pInstance)
                pInstance->SetData(DATA_SIAMAT_EVENT, IN_PROGRESS);
        }

        void JustDied(Unit* /*killer*/)
        {
            if (pInstance)
			{
				pInstance->SetData(DATA_SIAMAT_EVENT, DONE);
				if (IsHeroic())
				{
					int32 income = 7500;
					pInstance->DoUpdateCurrency(CURRENCY_TYPE_JUSTICE_POINTS, income);
				}
			}
        }

		void JustSummoned(Creature* summoned)
        {
			summons.Summon(summoned);
			switch(summoned->GetEntry())
			{
			case NPC_MINION_OF_SIAMAT:
				if (Unit *pVictim = SelectTarget(SELECT_TARGET_RANDOM))
					summoned->AI()->AttackStart(pVictim);
				break;
			case NPC_SERVANT_OF_SIAMAT:
				if (Unit *pVictim = SelectTarget(SELECT_TARGET_RANDOM))
					summoned->AI()->AttackStart(pVictim);
				break;
			}
        }

        void UpdateAI(const uint32 diff)
        {
            if (!UpdateVictim())
                return;

			if (SpawnCount == 0 && Phase == PHASE_DEFLECTING_WIND)
            {
                me->ApplySpellImmune(0, IMMUNITY_MECHANIC, MECHANIC_INTERRUPT, false);
                SpawnCount = 3;
                SetCombatMovement(true);
                Phase = PHASE_NORMAL;
                Phased = false;
                me->RemoveAurasDueToSpell(SPELL_DEFLECTING_WIND);
            }

            if (me->HealthBelowPct(90) && Phase == PHASE_NORMAL && PhaseCount == 0)
            {
                me->ApplySpellImmune(0, IMMUNITY_MECHANIC, MECHANIC_INTERRUPT, true);
                PhaseCount++;
                SetCombatMovement(false);
                Phase = PHASE_DEFLECTING_WIND;
                DoCast(me, SPELL_DEFLECTING_WIND);
				events.ScheduleEvent(EVENT_MIMION_SUMMON, 1 *IN_MILLISECONDS);
				events.ScheduleEvent(EVENT_SERVANT_SUMMON, 20 *IN_MILLISECONDS);
                Position pos;
                me->GetPosition(&pos);
                Phase2EndTimer = 60000;
            }

			if (me->HealthBelowPct(60) && Phase == PHASE_NORMAL && PhaseCount == 0)
            {
                me->ApplySpellImmune(0, IMMUNITY_MECHANIC, MECHANIC_INTERRUPT, true);
                PhaseCount++;
                SetCombatMovement(false);
                Phase = PHASE_DEFLECTING_WIND;
                DoCast(me, SPELL_DEFLECTING_WIND);
                Position pos;
                me->GetPosition(&pos);
                Phase2EndTimer = 60000;
            }

			if (me->HealthBelowPct(40) && Phase == PHASE_NORMAL && PhaseCount == 0)
            {
                me->ApplySpellImmune(0, IMMUNITY_MECHANIC, MECHANIC_INTERRUPT, true);
                PhaseCount++;
                SetCombatMovement(false);
                Phase = PHASE_DEFLECTING_WIND;
                DoCast(me, SPELL_DEFLECTING_WIND);
                Position pos;
                me->GetPosition(&pos);
                Phase2EndTimer = 60000;
            }

			if (me->GetHealthPct() < 30 && wailing_wind_casted == 0)            
			{                                                                
				DoCast(me, SPELL_GATHERED_STORM);                                
				DoCast(SPELL_WAILING_WIND);                                
				DoCast(SPELL_ABSORB_STORMS);                                
				events.ScheduleEvent(EVENT_WAILING_WIND, 10 *IN_MILLISECONDS);                                
				events.ScheduleEvent(EVENT_ABSORB_STORM, 15 *IN_MILLISECONDS);                
				wailing_wind_casted = 1;            
			}

			if (StormBoltTimer <= diff && Phase == PHASE_NORMAL)
            {
                if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, 100, true))
                    DoCast(target, SPELL_STORM_BOLT);
                StormBoltTimer = 5000;
            } else StormBoltTimer -= diff;

            if (Phase == PHASE_DEFLECTING_WIND)
            {
                if (Phase2EndTimer <= diff)
                {
                    SpawnCount = 3;
                    SetCombatMovement(true);
                    Phase = PHASE_NORMAL;
                    Phased = false;
                    me->RemoveAurasDueToSpell(SPELL_DEFLECTING_WIND);
                } else Phase2EndTimer -= diff;
            }

            events.Update(diff);

            if (me->HasUnitState(UNIT_STAT_CASTING))
                    return;

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                case EVENT_STORM_BOLT:
                    DoCast(SPELL_STORM_BOLT);
                    events.RescheduleEvent(EVENT_STORM_BOLT, 15 *IN_MILLISECONDS);
                    break;
                case EVENT_WAILING_WIND:
					DoCast(SPELL_WAILING_WIND);
                    events.RescheduleEvent(EVENT_WAILING_WIND, 20 *IN_MILLISECONDS);
                    break;
				case EVENT_MIMION_SUMMON:
					DoSummon(NPC_MINION_OF_SIAMAT, PosSummon[urand(0, 2)]);
                    events.ScheduleEvent(EVENT_MIMION_SUMMON, 12 *IN_MILLISECONDS);
                    break;
				case EVENT_MIMION_SUMMON1:
					DoSummon(NPC_MINION_OF_SIAMAT, PosSummon[urand(0, 2)]);
                    events.ScheduleEvent(EVENT_MIMION_SUMMON1, 12 *IN_MILLISECONDS);
                    break;
				case EVENT_MIMION_SUMMON2:
					DoSummon(NPC_MINION_OF_SIAMAT, PosSummon[urand(0, 2)]);
                    events.ScheduleEvent(EVENT_MIMION_SUMMON2, 12 *IN_MILLISECONDS);
                    break;
				case EVENT_MIMION_SUMMON3:
					DoSummon(NPC_MINION_OF_SIAMAT, PosSummon[urand(0, 2)]);
                    events.ScheduleEvent(EVENT_MIMION_SUMMON3, 12 *IN_MILLISECONDS);
                    break;
				case EVENT_MIMION_SUMMON4:
					DoSummon(NPC_MINION_OF_SIAMAT, PosSummon[urand(0, 2)]);
                    break;
				case EVENT_SERVANT_SUMMON:
					DoSummon(NPC_SERVANT_OF_SIAMAT, PosSummon[urand(3, 5)]);
                    events.ScheduleEvent(EVENT_SERVANT_SUMMON, 20 *IN_MILLISECONDS);
                    break;
				case EVENT_SERVANT_SUMMON1:
					DoSummon(NPC_SERVANT_OF_SIAMAT, PosSummon[urand(3, 5)]);
                    events.ScheduleEvent(EVENT_SERVANT_SUMMON1, 20 *IN_MILLISECONDS);
                    break;
				case EVENT_SERVANT_SUMMON2:
					DoSummon(NPC_SERVANT_OF_SIAMAT, PosSummon[urand(3, 5)]);
                    break;
				case EVENT_ABSORB_STORM:
					DoCast(SPELL_ABSORB_STORMS);
                    events.RescheduleEvent(EVENT_ABSORB_STORM, 15 *IN_MILLISECONDS);
                    break;
                case EVENT_BERSERK:
                    DoCast(me, SPELL_BERSERK);
                    break;
                }
            }

            DoMeleeAttackIfReady();
        }
     };

    CreatureAI* GetAI(Creature* pCreature) const
    {
        return new boss_siamatAI(pCreature);
    }
};

class npc_minion_of_siamat: public CreatureScript
{
 public:
    npc_minion_of_siamat() : CreatureScript("npc_minion_of_siamat") { }

    struct npc_minion_of_siamatAI: public BossAI
    {
        npc_minion_of_siamatAI(Creature* pCreature) : BossAI(pCreature, NPC_MINION_OF_SIAMAT) 
	    {
            me->ApplySpellImmune(0, IMMUNITY_EFFECT, SPELL_EFFECT_KNOCK_BACK, true);
            me->ApplySpellImmune(0, IMMUNITY_MECHANIC, MECHANIC_GRIP, true);
			pInstance = pCreature->GetInstanceScript();
	    }

		InstanceScript *pInstance;
		EventMap events;

        void Reset()
        {
            _Reset();
            events.ScheduleEvent(EVENT_DEPLETION, 10 *IN_MILLISECONDS);
			events.ScheduleEvent(EVENT_CHAIN_LIGHTNING, 5 *IN_MILLISECONDS);
        }

        void UpdateAI(const uint32 diff)
        {
            if (!UpdateVictim())
                return;
            
            events.Update(diff);

            if (me->HasUnitState(UNIT_STAT_CASTING))
                    return;

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                case EVENT_DEPLETION:
                    DoCast(SPELL_DEPLETION);
                    events.RescheduleEvent(EVENT_DEPLETION, 10 *IN_MILLISECONDS);
                    break;
                case EVENT_CHAIN_LIGHTNING:
                    DoCast(SPELL_CHAIN_LIGHTNING);
                    events.RescheduleEvent(EVENT_CHAIN_LIGHTNING, 15 *IN_MILLISECONDS);
                    break;;
                }
            }

            DoMeleeAttackIfReady();
        }    
     };

	 CreatureAI* GetAI(Creature* pCreature) const
    {
        return new npc_minion_of_siamatAI(pCreature);
    }
};

class npc_servant_of_siamat: public CreatureScript
{
 public:
    npc_servant_of_siamat() : CreatureScript("npc_servant_of_siamat") { }

    struct npc_servant_of_siamatAI: public BossAI
    {
        npc_servant_of_siamatAI(Creature* pCreature) : BossAI(pCreature, NPC_SERVANT_OF_SIAMAT) 
	    {
            me->ApplySpellImmune(0, IMMUNITY_EFFECT, SPELL_EFFECT_KNOCK_BACK, true);
            me->ApplySpellImmune(0, IMMUNITY_MECHANIC, MECHANIC_GRIP, true);
			pInstance = pCreature->GetInstanceScript();
	    }

		InstanceScript *pInstance;
		EventMap events;

		uint32 frenzy_casted;

        void Reset()
        {
            _Reset();
			events.ScheduleEvent(EVENT_LIGHTING_NOVA, 10 *IN_MILLISECONDS);
			frenzy_casted = 0;
			if (IsHeroic())
			events.ScheduleEvent(EVENT_LIGHTING_CHARGE, 5 *IN_MILLISECONDS);
        }

        void UpdateAI(const uint32 diff)
        {
            if (!UpdateVictim())
                return;

		if (me->GetHealthPct() < 30 && frenzy_casted == 0)
            {
                DoCast(SPELL_THUNDER_CRASH);
                frenzy_casted = 1;
            }
            
            events.Update(diff);

            if (me->HasUnitState(UNIT_STAT_CASTING))
                    return;

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                case EVENT_LIGHTING_CHARGE:
					if (IsHeroic())
                    DoCast(SPELL_LIGHTING_CHARGE);
                    events.RescheduleEvent(EVENT_LIGHTING_CHARGE, 10 *IN_MILLISECONDS);
                    break;
                case EVENT_LIGHTING_NOVA:
                    DoCast(SPELL_LIGHTING_NOVA);
                    events.RescheduleEvent(EVENT_LIGHTING_NOVA, 15 *IN_MILLISECONDS);
                    break;
                }
            }

            DoMeleeAttackIfReady();
        }    
     };

	 CreatureAI* GetAI(Creature* pCreature) const
    {
        return new npc_servant_of_siamatAI(pCreature);
    }
};

void AddSC_boss_siamat()
{
    new boss_siamat();
    new npc_servant_of_siamat();
	new npc_minion_of_siamat();
}
