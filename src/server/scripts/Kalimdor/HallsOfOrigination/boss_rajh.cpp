/* ScriptData
SDName: Boss Rajh
SD%Complete: 80%
SDComment: Add object handling.
SDCategory: Halls of Origination
EndScriptData */

#include"ScriptPCH.h"
#include"halls_of_origination.h"

enum Spells
{
    SPELL_BERSERK               = 47008,
    SPELL_BLESSING_OF_THE_SUN	= 76355,
	SPELL_INFERNO_LEAP			= 87653,
	SPELL_SUMMON_SUN_ORB		= 80352,
	SPELL_SUN_STRIKE			= 73874,
	SPELL_SOLAR_WINDS			= 89130,
};

enum Events
{
    EVENT_BERSERK = 1,
    EVENT_BLESSING_OF_THE_SUN = 2,
	EVENT_INFERNO_LEAP = 3,
	EVENT_SUMMON_SUN_ORB = 4,
	EVENT_SUN_STRIKE = 5,
	EVENT_SUMMON_SOLAR_WINDS = 6,
	EVENT_SOLAR_WINDS = 7,
};

const Position PosSummon[3] =
{
    {-665.961731f, 185.324036f, 343.941498f, 0.0f}, // SEEDLING
	{-675.580322f, 171.748093f, 343.933044f, 0.0f}, // SEEDLING
	{-691.398376f, 167.576935f, 343.928802f, 0.0f}, // SEEDLING
};

class boss_rajh: public CreatureScript
{
    public:
        boss_rajh() : CreatureScript("boss_rajh") { }

    struct boss_rajhAI: public BossAI
    {
        boss_rajhAI(Creature* pCreature) : BossAI(pCreature, DATA_RAJH) 
	 {
	     me->ApplySpellImmune(0, IMMUNITY_EFFECT, SPELL_EFFECT_KNOCK_BACK, true);
	     me->ApplySpellImmune(0, IMMUNITY_MECHANIC, MECHANIC_GRIP, true);
	 }
		
        EventMap events;
        bool check_in;
		
		uint32 blessing_of_the_sun_casted;

        void Reset()
        {
            events.Reset();
            if (pInstance && (pInstance->GetData(DATA_RAJH_EVENT) != DONE &&  !check_in))
                pInstance->SetData(DATA_RAJH_EVENT, NOT_STARTED);

            me->RemoveAurasDueToSpell(SPELL_BERSERK);
            events.ScheduleEvent(EVENT_BERSERK, 300 *IN_MILLISECONDS);
			events.ScheduleEvent(EVENT_SUN_STRIKE, 3 *IN_MILLISECONDS);
			events.ScheduleEvent(EVENT_INFERNO_LEAP, 10 *IN_MILLISECONDS);
			events.ScheduleEvent(EVENT_SUMMON_SUN_ORB, 5 *IN_MILLISECONDS);
			events.ScheduleEvent(EVENT_BLESSING_OF_THE_SUN, 20 *IN_MILLISECONDS);
			events.ScheduleEvent(EVENT_SUMMON_SOLAR_WINDS, 10 *IN_MILLISECONDS);
			blessing_of_the_sun_casted = 0;
			check_in = false;
			summons.DespawnAll();
        }

        void JustDied(Unit* /*killer*/)
        {
			if (pInstance)
			{
				pInstance->SetData(DATA_RAJH_EVENT, DONE);
				if (IsHeroic())
				{
					int32 income = 7500;
					pInstance->DoUpdateCurrency(CURRENCY_TYPE_JUSTICE_POINTS, income);
				}
			}
        }

		void EnterCombat(Unit* /*Ent*/)
        {
			DoZoneInCombat();

            if (pInstance)
                pInstance->SetData(DATA_RAJH_EVENT, IN_PROGRESS);
        }

		void JustSummoned(Creature* summoned)
        {
			summons.Summon(summoned);
			switch(summoned->GetEntry())
			{
			case NPC_SOLAR_WINDS:
				if (Unit *pVictim = SelectTarget(SELECT_TARGET_RANDOM))
					summoned->AI()->AttackStart(pVictim);
				break;
			}
        }

        void UpdateAI(const uint32 diff)
        {
            if (!UpdateVictim())
                return;

			if (me->GetHealthPct() < 75 && blessing_of_the_sun_casted == 0)
            {
                DoCast(SPELL_BLESSING_OF_THE_SUN);
                blessing_of_the_sun_casted = 1;
            }

			if (me->GetHealthPct() < 50 && blessing_of_the_sun_casted == 1)
            {
                DoCast(SPELL_BLESSING_OF_THE_SUN);
                blessing_of_the_sun_casted = 2;
            }

			if (me->GetHealthPct() < 25 && blessing_of_the_sun_casted == 2)
            {
                DoCast(SPELL_BLESSING_OF_THE_SUN);
                blessing_of_the_sun_casted = 3;
            }

            events.Update(diff);

            if (me->HasUnitState(UNIT_STAT_CASTING))
                    return;

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                case EVENT_INFERNO_LEAP:
                    DoCast(SPELL_INFERNO_LEAP);
                    events.RescheduleEvent(EVENT_INFERNO_LEAP, 20 *IN_MILLISECONDS);
                    break;
                case EVENT_SUN_STRIKE:
					DoCast(SPELL_SUN_STRIKE);
                    events.RescheduleEvent(EVENT_SUN_STRIKE, 20 *IN_MILLISECONDS);
                    break;
				case EVENT_SUMMON_SUN_ORB:
					DoCast(SPELL_SUMMON_SUN_ORB);
                    events.RescheduleEvent(EVENT_SUMMON_SUN_ORB, 25 *IN_MILLISECONDS);
                    break;
				case EVENT_SUMMON_SOLAR_WINDS:
					me->SummonCreature(NPC_SOLAR_WINDS, PosSummon[rand() % (1, 2)], TEMPSUMMON_CORPSE_DESPAWN);
                    events.RescheduleEvent(EVENT_SUMMON_SOLAR_WINDS, 25 *IN_MILLISECONDS);
                    break;
                case EVENT_BERSERK:
                    DoCast(me, SPELL_BERSERK);
                    break;
                }
            }

            DoMeleeAttackIfReady();
        }
     };

    CreatureAI* GetAI(Creature* pCreature) const
    {
        return new boss_rajhAI(pCreature);
    }
};

class npc_solar_winds: public CreatureScript
{
 public:
    npc_solar_winds() : CreatureScript("npc_solar_winds") { }

    struct npc_solar_windsAI: public BossAI
    {
        npc_solar_windsAI(Creature* pCreature) : BossAI(pCreature, NPC_SOLAR_WINDS) 
	    {
            me->ApplySpellImmune(0, IMMUNITY_EFFECT, SPELL_EFFECT_KNOCK_BACK, true);
            me->ApplySpellImmune(0, IMMUNITY_MECHANIC, MECHANIC_GRIP, true);
	    }

        EventMap events;

		uint32 void_barrier_casted;

        void Reset()
        {
            _Reset();
            events.ScheduleEvent(EVENT_SOLAR_WINDS, 1 *IN_MILLISECONDS);
			void_barrier_casted = 0;
        }

        void UpdateAI(const uint32 diff)
        {
            if (!UpdateVictim())
                return;
            
            events.Update(diff);

            if (me->HasUnitState(UNIT_STAT_CASTING))
                    return;

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                case EVENT_SOLAR_WINDS:
                    DoCast(SPELL_SOLAR_WINDS);
                    events.RescheduleEvent(EVENT_SOLAR_WINDS, 3 *IN_MILLISECONDS);
                    break;
                }
            }

            DoMeleeAttackIfReady();
        }    
     };

	 CreatureAI* GetAI(Creature* pCreature) const
    {
        return new npc_solar_windsAI(pCreature);
    }
};

void AddSC_boss_rajh()
{
    new boss_rajh();
	new npc_solar_winds();
}
