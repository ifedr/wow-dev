/* ScriptData
SDName: Boss Ammunae
SD%Complete: 80%
SDComment: Add object handling.
SDCategory: Halls of Origination
EndScriptData */

#include"ScriptPCH.h"
#include"halls_of_origination.h"

enum Spells
{
    SPELL_BERSERK               = 47008,
    SPELL_CONSUME_LIFE_ENERGY	= 79768,
	SPELL_RAMPANTH_GROWTH		= 75790,
	SPELL_WITHER				= 76043,
	SPELL_ENERGIZING_GROWTH		= 75624,
	SPELL_FROZEN_WAKE			= 61394,
	SPELL_THORN_SLASH			= 76044,
	SPELL_NOXIOUS_SPORES		= 75702,
	SPELL_SPORE_CLOUD			= 75701,
};

enum Events
{
    EVENT_BERSERK = 1,
    EVENT_CONSUME_LIFE_ENERGY = 2,
	EVENT_RAMPANTH_GROWTH = 3,
	EVENT_WITHER = 4,
	EVENT_ENERGIZING_GROWTH = 5,
	EVENT_FROZEN_WAKE = 6,
	EVENT_THORN_SLASH = 7,
	EVENT_NOXIOUS_SPORES = 8,
	EVENT_SUMMON_SEEDLING = 9,
	EVENT_SUMMON_BLOODPETAL = 10,
	EVENT_SUMMON_SPORE = 11,
};

const Position PosSummon[20] =
{
    {-665.961731f, 185.324036f, 343.941498f, 0.0f}, // SEEDLING
	{-675.580322f, 171.748093f, 343.933044f, 0.0f}, // SEEDLING
	{-691.398376f, 167.576935f, 343.928802f, 0.0f}, // SEEDLING
	{-706.495422f, 172.362137f, 343.931702f, 0.0f}, // SEEDLING
	{-715.961121f, 185.290619f, 343.941895f, 0.0f}, // SEEDLING
	{-716.364746f, 202.002899f, 343.942261f, 0.0f}, // SEEDLING
	{-706.979248f, 214.525131f, 343.932434f, 0.0f}, // SEEDLING
	{-690.749634f, 219.792831f, 343.928009f, 0.0f}, // SEEDLING
	{-675.738098f, 214.678162f, 343.931854f, 0.0f}, // SEEDLING
	{-686.566711f, 207.196198f, 343.936768f, 0.0f}, // SEEDLING
	{-676.212952f, 193.530563f, 343.946564f, 0.0f}, // SEEDLING
	{-686.931885f, 179.539825f, 343.937714f, 0.0f}, // SEEDLING
	{-703.050903f, 184.862762f, 343.941406f, 0.0f}, // SEEDLING
	{-691.899231f, 192.913208f, 343.947083f, 0.0f}, // SEEDLING
	{-662.152527f, 157.370804f, 343.912262f, 0.0f}, // Bloodpatel
	{-668.922668f, 248.597778f, 343.890106f, 0.0f}, // Bloodpatel
	{-734.853821f, 214.841675f, 343.946777f, 0.0f}, // Bloodpatel
	{-685.662354f, 204.139664f, 343.939819f, 0.0f}, // Spore
	{-656.217102f, 193.483322f, 343.948792f, 0.0f}, // Spore
	{-721.997864f, 133.872238f, 343.880554f, 0.0f}, // Spore
};

class boss_ammunae: public CreatureScript
{
    public:
        boss_ammunae() : CreatureScript("boss_ammunae") { }

    struct boss_ammunaeAI: public BossAI
    {
        boss_ammunaeAI(Creature* pCreature) : BossAI(pCreature, DATA_AMMUNAE) 
	 {
	     me->ApplySpellImmune(0, IMMUNITY_EFFECT, SPELL_EFFECT_KNOCK_BACK, true);
	     me->ApplySpellImmune(0, IMMUNITY_MECHANIC, MECHANIC_GRIP, true);
		 pInstance = pCreature->GetInstanceScript();
	 }

		InstanceScript *pInstance;
        EventMap events;
        bool check_in;

        void Reset()
        {
            events.Reset();
            if (pInstance && (pInstance->GetData(DATA_AMMUNAE_EVENT) != DONE &&  !check_in))
                pInstance->SetData(DATA_AMMUNAE_EVENT, NOT_STARTED);

            me->RemoveAurasDueToSpell(SPELL_BERSERK);
            events.ScheduleEvent(EVENT_BERSERK, 300 *IN_MILLISECONDS);
			events.ScheduleEvent(EVENT_CONSUME_LIFE_ENERGY, 10 *IN_MILLISECONDS);
			events.ScheduleEvent(EVENT_RAMPANTH_GROWTH, 11 *IN_MILLISECONDS);
			events.ScheduleEvent(EVENT_WITHER, 15 *IN_MILLISECONDS);
			events.ScheduleEvent(EVENT_SUMMON_SEEDLING, 5 *IN_MILLISECONDS);
			events.ScheduleEvent(EVENT_SUMMON_BLOODPETAL, 20 *IN_MILLISECONDS);
			events.ScheduleEvent(EVENT_SUMMON_SPORE, 25 *IN_MILLISECONDS);
			check_in = false;
			summons.DespawnAll();
        }

		void EnterCombat(Unit* /*Ent*/)
        {
			DoZoneInCombat();

            if (pInstance)
                pInstance->SetData(DATA_AMMUNAE_EVENT, IN_PROGRESS);
        }

        void JustDied(Unit* /*killer*/)
        {
            if (pInstance)
			{
                pInstance->SetData(DATA_AMMUNAE_EVENT, DONE);
				if (IsHeroic())
				{
					int32 income = 7500;
					pInstance->DoUpdateCurrency(CURRENCY_TYPE_JUSTICE_POINTS, income);
				}
            }
        }

		void JustSummoned(Creature* summoned)
        {
			summons.Summon(summoned);
			switch(summoned->GetEntry())
			{
			case NPC_SEEDLING_POD:
				if (Unit *pVictim = SelectTarget(SELECT_TARGET_RANDOM))
					summoned->AI()->AttackStart(pVictim);
				break;
			case NPC_BLOODPETAL_BLOSSOM:
				if (Unit *pVictim = SelectTarget(SELECT_TARGET_RANDOM))
					summoned->AI()->AttackStart(pVictim);
				break;
			case NPC_SPORE:
				if (Unit *pVictim = SelectTarget(SELECT_TARGET_RANDOM))
					summoned->AI()->AttackStart(pVictim);
				break;
			}
        }

        void UpdateAI(const uint32 diff)
        {
            if (!UpdateVictim())
                return;

            events.Update(diff);

            if (me->HasUnitState(UNIT_STAT_CASTING))
                    return;

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                case EVENT_CONSUME_LIFE_ENERGY:
                    DoCast(SPELL_CONSUME_LIFE_ENERGY);
                    events.RescheduleEvent(EVENT_CONSUME_LIFE_ENERGY, 20 *IN_MILLISECONDS);
                    break;
                case EVENT_RAMPANTH_GROWTH:
					DoCast(SPELL_RAMPANTH_GROWTH);
                    events.RescheduleEvent(EVENT_RAMPANTH_GROWTH, 20 *IN_MILLISECONDS);
                    break;
				case EVENT_WITHER:
					DoCast(SPELL_WITHER);
                    events.RescheduleEvent(EVENT_WITHER, 25 *IN_MILLISECONDS);
                    break;
				case EVENT_SUMMON_SEEDLING:
					DoSummon(NPC_SEEDLING_POD, PosSummon[rand() % (1, 14)]);
                    events.RescheduleEvent(EVENT_SUMMON_SEEDLING, 5 *IN_MILLISECONDS);
                    break;
				case EVENT_SUMMON_BLOODPETAL:
					DoSummon(NPC_BLOODPETAL_BLOSSOM, PosSummon[rand() % (15, 17)]);
                    events.RescheduleEvent(EVENT_SUMMON_BLOODPETAL, 20 *IN_MILLISECONDS);
                    break;
				case EVENT_SUMMON_SPORE:
					DoSummon(NPC_SPORE, PosSummon[rand() % (18, 20)]);
                    events.RescheduleEvent(EVENT_SUMMON_SPORE, 25 *IN_MILLISECONDS);
                    break;
                case EVENT_BERSERK:
                    DoCast(me, SPELL_BERSERK);
                    break;
                }
            }

            DoMeleeAttackIfReady();
        }
     };

    CreatureAI* GetAI(Creature* pCreature) const
    {
        return new boss_ammunaeAI(pCreature);
    }
};

class npc_seedling_pod: public CreatureScript
{
 public:
    npc_seedling_pod() : CreatureScript("npc_seedling_pod") { }

    struct npc_seedling_podAI: public BossAI
    {
        npc_seedling_podAI(Creature* pCreature) : BossAI(pCreature, NPC_SEEDLING_POD) 
	    {
            me->ApplySpellImmune(0, IMMUNITY_EFFECT, SPELL_EFFECT_KNOCK_BACK, true);
            me->ApplySpellImmune(0, IMMUNITY_MECHANIC, MECHANIC_GRIP, true);
			pInstance = pCreature->GetInstanceScript();
	    }

		InstanceScript *pInstance;
        EventMap events;

        void Reset()
        {
            _Reset();
            events.ScheduleEvent(EVENT_ENERGIZING_GROWTH, 2 *IN_MILLISECONDS);
			me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_DISABLE_MOVE);
        }

        void UpdateAI(const uint32 diff)
        {
            if (!UpdateVictim())
                return;
            
            events.Update(diff);

            if (me->HasUnitState(UNIT_STAT_CASTING))
                    return;

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                case EVENT_ENERGIZING_GROWTH:
                    DoCast(SPELL_ENERGIZING_GROWTH);
                    events.RescheduleEvent(EVENT_ENERGIZING_GROWTH, 3 *IN_MILLISECONDS);
                    break;;
                }
            }

            DoMeleeAttackIfReady();
        }    
     };

	 CreatureAI* GetAI(Creature* pCreature) const
    {
        return new npc_seedling_podAI(pCreature);
    }
};

class npc_bloodpetal_blossom: public CreatureScript
{
 public:
    npc_bloodpetal_blossom() : CreatureScript("npc_bloodpetal_blossom") { }

    struct npc_bloodpetal_blossomAI: public BossAI
    {
        npc_bloodpetal_blossomAI(Creature* pCreature) : BossAI(pCreature, NPC_BLOODPETAL_BLOSSOM) 
	    {
            me->ApplySpellImmune(0, IMMUNITY_EFFECT, SPELL_EFFECT_KNOCK_BACK, true);
            me->ApplySpellImmune(0, IMMUNITY_MECHANIC, MECHANIC_GRIP, true);
			pInstance = pCreature->GetInstanceScript();
	    }

		InstanceScript *pInstance;
        EventMap events;

        void Reset()
        {
            _Reset();
			events.ScheduleEvent(EVENT_FROZEN_WAKE, 10 *IN_MILLISECONDS);
			events.ScheduleEvent(EVENT_THORN_SLASH, 5 *IN_MILLISECONDS);
        }

        void UpdateAI(const uint32 diff)
        {
            if (!UpdateVictim())
                return;
            
            events.Update(diff);

            if (me->HasUnitState(UNIT_STAT_CASTING))
                    return;

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                case EVENT_FROZEN_WAKE:
                    DoCast(SPELL_FROZEN_WAKE);
                    events.RescheduleEvent(EVENT_FROZEN_WAKE, 15 *IN_MILLISECONDS);
                    break;
                case EVENT_THORN_SLASH:
                    DoCast(SPELL_THORN_SLASH);
                    events.RescheduleEvent(EVENT_THORN_SLASH, 10 *IN_MILLISECONDS);
                    break;
                }
            }

            DoMeleeAttackIfReady();
        }    
     };

	 CreatureAI* GetAI(Creature* pCreature) const
    {
        return new npc_bloodpetal_blossomAI(pCreature);
    }
};

class npc_spore: public CreatureScript
{
 public:
    npc_spore() : CreatureScript("npc_spore") { }

    struct npc_sporeAI: public BossAI
    {
        npc_sporeAI(Creature* pCreature) : BossAI(pCreature, NPC_SPORE) 
	    {
            me->ApplySpellImmune(0, IMMUNITY_EFFECT, SPELL_EFFECT_KNOCK_BACK, true);
            me->ApplySpellImmune(0, IMMUNITY_MECHANIC, MECHANIC_GRIP, true);
			pInstance = pCreature->GetInstanceScript();
	    }

		InstanceScript *pInstance;
        EventMap events;

		uint32 spore_cloud;

        void Reset()
        {
            _Reset();
			events.ScheduleEvent(EVENT_NOXIOUS_SPORES, 2 *IN_MILLISECONDS);
			spore_cloud = 0;
        }

		void JustDied(Unit* /*killer*/)
        {
            _JustDied();

            DoCast(me, SPELL_SPORE_CLOUD);
        }

        void UpdateAI(const uint32 diff)
        {
            if (!UpdateVictim())
                return;
            
            events.Update(diff);

            if (me->HasUnitState(UNIT_STAT_CASTING))
                    return;

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                case EVENT_NOXIOUS_SPORES:
                    DoCast(SPELL_NOXIOUS_SPORES);
                    events.RescheduleEvent(EVENT_NOXIOUS_SPORES, 15 *IN_MILLISECONDS);
                    break;
                }
            }

            DoMeleeAttackIfReady();
        }    
     };

	 CreatureAI* GetAI(Creature* pCreature) const
    {
        return new npc_sporeAI(pCreature);
    }
};

void AddSC_boss_ammunae()
{
    new boss_ammunae();
    new npc_bloodpetal_blossom();
	new npc_seedling_pod();
	new npc_spore();
}