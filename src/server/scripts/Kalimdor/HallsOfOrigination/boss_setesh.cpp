/* ScriptData
SDName: Boss RAJH
SD%Complete: 80%
SDComment: 
SDCategory: Halls of Origination
EndScriptData */

#include"ScriptPCH.h"
#include"halls_of_origination.h"

enum Spells
{
    SPELL_BERSERK               = 47008,
	SPELL_CHAOS_BOLT			= 89867,
	SPELL_SEED_OF_CHAOS			= 89867,
	SPELL_CHARGED_FISTS			= 77238,
	SPELL_VOID_BARRIER			= 76959,
	SPELL_ANTI_MAGIC_PRISON		= 76903,
	SPELL_FROZEN_WAKE			= 61394,
	SPELL_SHADOW_BOLT_VOLLEY	= 76146,
};

enum Events
{
    EVENT_BERSERK = 1,
    EVENT_CHAOS_BOLT = 2,
	EVENT_SEED_OF_CHAOS = 3,
	EVENT_CHARGED_FISTS = 4,
	EVENT_VOID_BARRIER = 5,
	EVENT_ANTI_MAGIC_PRISON = 6,
	EVENT_FROZEN_WAKE = 7,
	EVENT_SHADOW_BOLT_VOLLEY = 8,
	EVENT_CHAOS_PORTAL_SUMMON = 9,
	EVENT_VOID_SENTINE_SUMMON = 10,
	EVENT_VOID_SEEKER_SUMMON = 11,
	EVENT_VOID_WURM_SUMMON = 12,
};

const Position PosSummon[2] =
{
    {-433.608490f, 8.353800f, 343.863953f, 0.0f},          // 2 - Portal Right
    {-577.137817f, 3.316941f, 343.865631f, 0.0f},         // 3 - Portal Left
};

class boss_setesh: public CreatureScript
{
    public:
        boss_setesh() : CreatureScript("boss_setesh") { }

    struct boss_seteshAI: public BossAI
    {
        boss_seteshAI(Creature* pCreature) : BossAI(pCreature, DATA_SETESH) 
	 {
	     me->ApplySpellImmune(0, IMMUNITY_EFFECT, SPELL_EFFECT_KNOCK_BACK, true);
	     me->ApplySpellImmune(0, IMMUNITY_MECHANIC, MECHANIC_GRIP, true);
	 }

        EventMap events;
        bool check_in;
		
		void Reset()
        {
            events.Reset();
            if (pInstance && (pInstance->GetData(DATA_SETESH_EVENT) != DONE &&  !check_in))
                pInstance->SetData(DATA_SETESH_EVENT, NOT_STARTED);

            me->RemoveAurasDueToSpell(SPELL_BERSERK);
            events.ScheduleEvent(EVENT_BERSERK, 300 *IN_MILLISECONDS);
			events.ScheduleEvent(EVENT_CHAOS_BOLT, 5 *IN_MILLISECONDS);
			events.ScheduleEvent(EVENT_SEED_OF_CHAOS, 10 *IN_MILLISECONDS);
			events.ScheduleEvent(EVENT_CHAOS_PORTAL_SUMMON, 10 *IN_MILLISECONDS);
			summons.DespawnAll();
			check_in = false;
        }

	void EnterCombat(Unit* /*Ent*/)
        {

            DoZoneInCombat();

            if (pInstance)
                pInstance->SetData(DATA_SETESH_EVENT, IN_PROGRESS);

        }

		void JustSummoned(Creature* summoned)
        {
			summons.Summon(summoned);
			switch(summoned->GetEntry())
			{
			case NPC_CHAOS_PORTAL:
				if (Unit *pVictim = SelectTarget(SELECT_TARGET_RANDOM))
					summoned->AI()->AttackStart(pVictim);
				break;
			}
        }

        void JustDied(Unit* /*killer*/)
        {
            if (pInstance)
			{
				pInstance->SetData(DATA_SETESH_EVENT, DONE);
				if (IsHeroic())
				{
					int32 income = 7500;
					pInstance->DoUpdateCurrency(CURRENCY_TYPE_JUSTICE_POINTS, income);
				}
			}
        }

        void UpdateAI(const uint32 diff)
        {
            if (!UpdateVictim())
                return;

            events.Update(diff);

            if (me->HasUnitState(UNIT_STAT_CASTING))
                    return;

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                case EVENT_CHAOS_BOLT:
                    DoCast(SPELL_CHAOS_BOLT);
                    events.RescheduleEvent(EVENT_CHAOS_BOLT, 5 *IN_MILLISECONDS);
                    break;
                case EVENT_SEED_OF_CHAOS:
					DoCast(SPELL_SEED_OF_CHAOS);
                    events.RescheduleEvent(EVENT_SEED_OF_CHAOS, 15 *IN_MILLISECONDS);
                    break;
				case EVENT_CHAOS_PORTAL_SUMMON:
					me->SummonCreature(NPC_CHAOS_PORTAL, PosSummon[rand() % (1, 2)], TEMPSUMMON_CORPSE_DESPAWN);
                    events.RescheduleEvent(EVENT_CHAOS_PORTAL_SUMMON, 60 *IN_MILLISECONDS);
                    break;
                case EVENT_BERSERK:
                    DoCast(me, SPELL_BERSERK);
                    break;
                }
            }

            DoMeleeAttackIfReady();
        }
     };

    CreatureAI* GetAI(Creature* pCreature) const
    {
        return new boss_seteshAI(pCreature);
    }
};

class npc_void_sentinele: public CreatureScript
{
 public:
    npc_void_sentinele() : CreatureScript("npc_void_sentinele") { }

    struct npc_void_sentineleAI: public BossAI
    {
        npc_void_sentineleAI(Creature* pCreature) : BossAI(pCreature, NPC_VOID_SENTINEL) 
	    {
            me->ApplySpellImmune(0, IMMUNITY_EFFECT, SPELL_EFFECT_KNOCK_BACK, true);
            me->ApplySpellImmune(0, IMMUNITY_MECHANIC, MECHANIC_GRIP, true);
	    }

        EventMap events;

		uint32 void_barrier_casted;

        void Reset()
        {
            _Reset();
            events.ScheduleEvent(EVENT_CHARGED_FISTS, 1 *IN_MILLISECONDS);
			void_barrier_casted = 0;
        }

        void UpdateAI(const uint32 diff)
        {
            if (!UpdateVictim())
                return;

			if (me->GetHealthPct() < 25 && void_barrier_casted == 0)
            {
                DoCast(SPELL_VOID_BARRIER);
                void_barrier_casted = 1;
            }
            
            events.Update(diff);

            if (me->HasUnitState(UNIT_STAT_CASTING))
                    return;

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                case EVENT_CHARGED_FISTS:
                    DoCast(SPELL_CHARGED_FISTS);
                    events.RescheduleEvent(EVENT_CHARGED_FISTS, 15 *IN_MILLISECONDS);
                    break;
                }
            }

            DoMeleeAttackIfReady();
        }    
     };

	 CreatureAI* GetAI(Creature* pCreature) const
    {
        return new npc_void_sentineleAI(pCreature);
    }
};

class npc_void_seeker: public CreatureScript
{
 public:
    npc_void_seeker() : CreatureScript("npc_void_seeker") { }

    struct npc_void_seekerAI: public BossAI
    {
        npc_void_seekerAI(Creature* pCreature) : BossAI(pCreature, NPC_VOID_SEEKER) 
	    {
            me->ApplySpellImmune(0, IMMUNITY_EFFECT, SPELL_EFFECT_KNOCK_BACK, true);
            me->ApplySpellImmune(0, IMMUNITY_MECHANIC, MECHANIC_GRIP, true);
	    }

        EventMap events;

        void Reset()
        {
            _Reset();
			events.ScheduleEvent(EVENT_ANTI_MAGIC_PRISON, 10 *IN_MILLISECONDS);
			events.ScheduleEvent(EVENT_FROZEN_WAKE, 5 *IN_MILLISECONDS);
			events.ScheduleEvent(EVENT_SHADOW_BOLT_VOLLEY, 8 *IN_MILLISECONDS);
        }

        void UpdateAI(const uint32 diff)
        {
            if (!UpdateVictim())
                return;
            
            events.Update(diff);

            if (me->HasUnitState(UNIT_STAT_CASTING))
                    return;

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                case EVENT_ANTI_MAGIC_PRISON:
                    DoCast(SPELL_ANTI_MAGIC_PRISON);
                    events.RescheduleEvent(EVENT_ANTI_MAGIC_PRISON, 15 *IN_MILLISECONDS);
                    break;
				case EVENT_SHADOW_BOLT_VOLLEY:
                    DoCast(SPELL_SHADOW_BOLT_VOLLEY);
                    events.RescheduleEvent(EVENT_SHADOW_BOLT_VOLLEY, 8 *IN_MILLISECONDS);
                    break;
				case EVENT_FROZEN_WAKE:
                    DoCast(SPELL_FROZEN_WAKE);
                    events.RescheduleEvent(EVENT_FROZEN_WAKE, 10 *IN_MILLISECONDS);
                    break;
                }
            }

            DoMeleeAttackIfReady();
        }    
     };

	 CreatureAI* GetAI(Creature* pCreature) const
    {
        return new npc_void_seekerAI(pCreature);
    }
};

class npc_chaos_portal: public CreatureScript
{
public:
    npc_chaos_portal() : CreatureScript("npc_chaos_portal") { }

    struct npc_chaos_portalAI : public BossAI
    {
        npc_chaos_portalAI(Creature* pCreature) : BossAI(pCreature, NPC_CHAOS_PORTAL)
        {
            me->ApplySpellImmune(0, IMMUNITY_EFFECT, SPELL_EFFECT_KNOCK_BACK, true);
            me->ApplySpellImmune(0, IMMUNITY_MECHANIC, MECHANIC_GRIP, true);
        }

        EventMap events;

        void Reset()
        {
            me->SetReactState(REACT_PASSIVE);
			me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_DISABLE_MOVE);
			events.ScheduleEvent(EVENT_VOID_SENTINE_SUMMON, 1 *IN_MILLISECONDS);
			events.ScheduleEvent(EVENT_VOID_SEEKER_SUMMON, 8 *IN_MILLISECONDS);
			events.ScheduleEvent(EVENT_VOID_WURM_SUMMON, 5 *IN_MILLISECONDS);
			summons.DespawnAll();
        }

		void JustSummoned(Creature* summoned)
        {
            summons.Summon(summoned);
            summoned->SetCorpseDelay(0);
			switch(summoned->GetEntry())
			{
			case NPC_VOID_SENTINEL:
				if (Unit *pVictim = SelectTarget(SELECT_TARGET_RANDOM))
					summoned->AI()->AttackStart(pVictim);
				break;
			case NPC_VOID_WURM:
				if (Unit *pVictim = SelectTarget(SELECT_TARGET_RANDOM))
					summoned->AI()->AttackStart(pVictim);
				break;
			case NPC_VOID_SEEKER:
				if (Unit *pVictim = SelectTarget(SELECT_TARGET_RANDOM))
					summoned->AI()->AttackStart(pVictim);
				break;
			}
        }

		void JustDied(Unit* /*pKiller*/)
        {
            me->ForcedDespawn();
        }

        void UpdateAI(const uint32 diff)
        {
            if (!UpdateVictim())
                return;
            
            events.Update(diff);

            if (me->HasUnitState(UNIT_STAT_CASTING))
                    return;

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                case EVENT_VOID_SENTINE_SUMMON:
                    me->SummonCreature(NPC_VOID_SENTINEL, PosSummon[rand() % (1, 2)], TEMPSUMMON_CORPSE_DESPAWN);
                    events.RescheduleEvent(EVENT_VOID_SENTINE_SUMMON, 15 *IN_MILLISECONDS);
                    break;
				case EVENT_VOID_SEEKER_SUMMON:
                    me->SummonCreature(NPC_VOID_SEEKER, PosSummon[rand() % (1, 2)], TEMPSUMMON_CORPSE_DESPAWN);
                    events.RescheduleEvent(EVENT_VOID_SEEKER_SUMMON, 18 *IN_MILLISECONDS);
                    break;
				case EVENT_VOID_WURM_SUMMON:
                    me->SummonCreature(NPC_VOID_WURM, PosSummon[rand() % (1, 2)], TEMPSUMMON_CORPSE_DESPAWN);
					me->SummonCreature(NPC_VOID_WURM, PosSummon[rand() % (1)], TEMPSUMMON_CORPSE_DESPAWN);
                    events.RescheduleEvent(EVENT_VOID_WURM_SUMMON, 15 *IN_MILLISECONDS);
                    break;
                }
            }
			DoMeleeAttackIfReady();
		}
    };

	CreatureAI* GetAI(Creature* pCreature) const
    {
        return new npc_chaos_portalAI(pCreature);
    }
};

void AddSC_boss_setesh()
{
    new boss_setesh();
	new npc_void_sentinele();
	new npc_void_seeker();
	new npc_chaos_portal();
}
