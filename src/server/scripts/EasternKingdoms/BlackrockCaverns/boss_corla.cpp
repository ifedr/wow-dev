#include"ScriptPCH.h"
#include"WorldPacket.h"
#include"blackrock_caverns.h"
#include"ScriptMgr.h"
#include"ScriptedCreature.h"
#include"SpellScript.h"
#include"SpellAuraEffects.h"

enum Spells
{
    SPELL_DARK_COMMAND         = 75823,
    H_SPELL_DARK_COMMAND       = 93462,
};

enum Events
{
    EVENT_DARK_COMMAND,
};

class boss_corla: public CreatureScript
{
public:
    boss_corla() : CreatureScript("boss_corla") { }

    CreatureAI* GetAI(Creature* pCreature) const
    {
       return new boss_corlaAI(pCreature);
    }
    struct boss_corlaAI: public ScriptedAI
    {
        boss_corlaAI(Creature* pCreature) : ScriptedAI(pCreature)
        {
            pInstance = pCreature->GetInstanceScript();
        }

        InstanceScript *pInstance;
        EventMap events;
        bool check_in;

        void Reset()
        {
            events.Reset();
            if (pInstance && (pInstance->GetData(DATA_CORLA_EVENT) != DONE &&  !check_in))
                pInstance->SetData(DATA_CORLA_EVENT, NOT_STARTED);

            check_in = false;
        }

        void EnterCombat(Unit* /*Ent*/)
        {
            events.ScheduleEvent(EVENT_DARK_COMMAND, urand(5000, 15000));
            if (pInstance)
                pInstance->SetData(DATA_CORLA_EVENT, IN_PROGRESS);
        }

        void JustDied(Unit* /*Kill*/)
        {
            if (pInstance){
                pInstance->SetData(DATA_CORLA_EVENT, DONE);
                if (IsHeroic())
                {
                    int32 income = 7000;
                    pInstance->DoUpdateCurrency(CURRENCY_TYPE_JUSTICE_POINTS, income);
                }
            }
        }

        void UpdateAI(const uint32 uiDiff)
        {
            if (!UpdateVictim())
                return;

            events.Update(uiDiff);

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch(eventId)
                {
                    case EVENT_DARK_COMMAND:
                        if (Unit *pTarget = SelectUnit(SELECT_TARGET_RANDOM, 0))
                            DoCast(pTarget, DUNGEON_MODE(SPELL_DARK_COMMAND, H_SPELL_DARK_COMMAND));
                        events.RescheduleEvent(EVENT_DARK_COMMAND, urand(5000, 15000));
                        break;
                }
            }
            DoMeleeAttackIfReady();
        }
     };
};

void AddSC_boss_corla()
{
    new boss_corla();
}
