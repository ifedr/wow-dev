#include"ScriptPCH.h"
#include"WorldPacket.h"
#include"blackrock_caverns.h"
#include"ScriptMgr.h"
#include"ScriptedCreature.h"
#include"SpellScript.h"
#include"SpellAuraEffects.h"

enum Spells
{
    SPELL_QUICKSILVER_ARMOR    = 75842,
    SPELL_SUPERHEATED          = 75846,
    SPELL_HEAT_WAVE            = 63679,
    SPELL_LAVA_SPOUT           = 76007,
    H_SPELL_LAVA_SPOUT         = 93565,
    SPELL_CLEAVE               = 15284,
};

enum Events
{
    EVENT_CLEAVE,
    EVENT_SUPERHEATED,
};

class boss_karsh: public CreatureScript
{
public:
    boss_karsh() : CreatureScript("boss_karsh") { }

    CreatureAI* GetAI(Creature* pCreature) const
    {
       return new boss_karshAI(pCreature);
    }
    struct boss_karshAI: public ScriptedAI
    {
        boss_karshAI(Creature* pCreature) : ScriptedAI(pCreature)
        {
            pInstance = pCreature->GetInstanceScript();
        }

        InstanceScript *pInstance;
        EventMap events;
        bool check_in;
        bool armor;

        void Reset()
        {
            events.Reset();
            if (pInstance && (pInstance->GetData(DATA_KARSH_STEELBENDER_EVENT) != DONE &&  !check_in))
                pInstance->SetData(DATA_KARSH_STEELBENDER_EVENT, NOT_STARTED);

            check_in = false;
            armor = false;
        }

        void EnterCombat(Unit* /*Ent*/)
        {
            events.RescheduleEvent(EVENT_CLEAVE, urand(5000, 10000));
            if (pInstance)
                pInstance->SetData(DATA_KARSH_STEELBENDER_EVENT, IN_PROGRESS);
            DoCast(me, SPELL_HEAT_WAVE);
            DoCast(me, SPELL_QUICKSILVER_ARMOR);
        }

        void JustDied(Unit* /*Kill*/)
        {
            if (pInstance){
                pInstance->SetData(DATA_KARSH_STEELBENDER_EVENT, DONE);
                if (IsHeroic())
                {
                    int32 income = 7000;
                    pInstance->DoUpdateCurrency(CURRENCY_TYPE_JUSTICE_POINTS, income);
                }
            }
        }

        void MovementInform(uint32 type, uint32 id)
        {
            if (type != POINT_MOTION_TYPE)
                return;

            float x, y;

            me->GetPosition(x,y);
            if(!armor && (((x - 237.8) * (x - 237.8) + (y - 785.2) * (y - 785.2)) <= 5.89 * 5.89))
            {
                events.ScheduleEvent(EVENT_SUPERHEATED, 1000);
                armor = true;
            }
        }

        void UpdateAI(const uint32 uiDiff)
        {
            if (!UpdateVictim())
                return;

            events.Update(uiDiff);

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch(eventId)
                {
                    case EVENT_CLEAVE:
                        DoCast(me->getVictim(), SPELL_CLEAVE);
                        events.RescheduleEvent(EVENT_CLEAVE, urand(5000, 10000));
                        break;
                    case EVENT_SUPERHEATED:
                        DoCast(me, SPELL_SUPERHEATED);
                        armor = false;
                        break;
                }
            }
            DoMeleeAttackIfReady();
        }
     };
};

void AddSC_boss_karsh()
{
    new boss_karsh();
}
