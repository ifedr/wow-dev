#include"ScriptPCH.h"
#include"WorldPacket.h"
#include"blackrock_caverns.h"
#include"ScriptMgr.h"
#include"ScriptedCreature.h"
#include"SpellScript.h"
#include"SpellAuraEffects.h"

enum Spells
{
    SPELL_SKULLCRACKER         = 75543,
    H_SPELL_SKULLCRACKER       = 93453,
    SPELL_CHAINS_OF_WOE        = 75539,
    SPELL_CHAINS_OF_WOE_NPC    = 82192,
    SPELL_QUAKE                = 75272,
    SPELL_WOUNDING_STRIKE      = 75571,
    H_SPELL_WOUNDING_STRIKE    = 93452,
};

enum Events
{
    EVENT_SKULLCRACKER,
    EVENT_QUAKE,
    EVENT_WOUNDING_STRIKE,
    EVENT_CHAINS,
};

class boss_romogg: public CreatureScript
{
public:
    boss_romogg() : CreatureScript("boss_romogg") { }

    CreatureAI* GetAI(Creature* pCreature) const
    {
       return new boss_romoggAI(pCreature);
    }
    struct boss_romoggAI: public ScriptedAI
    {
        boss_romoggAI(Creature* pCreature) : ScriptedAI(pCreature)
        {
            pInstance = pCreature->GetInstanceScript();
        }

        InstanceScript *pInstance;
        EventMap events;
        bool sc_flag_1;
        bool sc_flag_2;
        bool check_in;
        float curr_health;

        void Reset()
        {
            events.Reset();
            if (pInstance && (pInstance->GetData(DATA_ROMOGG_BONECRUSHER_EVENT) != DONE &&  !check_in))
                pInstance->SetData(DATA_ROMOGG_BONECRUSHER_EVENT, NOT_STARTED);

            check_in = false;
            sc_flag_1 = false;
            sc_flag_2 = false;
        }

        void EnterCombat(Unit* /*Ent*/)
        {
            events.ScheduleEvent(EVENT_WOUNDING_STRIKE, urand(8000, 12000));
            events.ScheduleEvent(EVENT_QUAKE, 27000);
            if (pInstance)
                pInstance->SetData(DATA_ROMOGG_BONECRUSHER_EVENT, IN_PROGRESS);
        }

        void JustDied(Unit* /*Kill*/)
        {
            if (pInstance){
                pInstance->SetData(DATA_ROMOGG_BONECRUSHER_EVENT, DONE);
                if (IsHeroic())
                {
                    int32 income = 7000;
                    pInstance->DoUpdateCurrency(CURRENCY_TYPE_JUSTICE_POINTS, income);
                }
            }
        }

        void UpdateAI(const uint32 uiDiff)
        {
            if (!UpdateVictim())
                return;

            events.Update(uiDiff);

            curr_health = me->GetHealthPct();
            if (curr_health < 66.0 && !sc_flag_1)
            {
                events.ScheduleEvent(EVENT_SKULLCRACKER, 1000);
                sc_flag_1 = true;
            } else if (curr_health < 33.0 && !sc_flag_2)
            {
                events.ScheduleEvent(EVENT_SKULLCRACKER, 1000);
                sc_flag_2 = true;
            }

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch(eventId)
                {
                    case EVENT_WOUNDING_STRIKE:
                        DoCast(me->getVictim(), DUNGEON_MODE(SPELL_WOUNDING_STRIKE, H_SPELL_WOUNDING_STRIKE));
                        events.RescheduleEvent(EVENT_WOUNDING_STRIKE, urand(8000, 12000));
                        break;
                    case EVENT_QUAKE:
                        DoCast(me->getVictim(), SPELL_QUAKE);
                        events.RescheduleEvent(EVENT_QUAKE, 27000);
                        break;
                    case EVENT_SKULLCRACKER:
                        DoCast(me->getVictim(), SPELL_CHAINS_OF_WOE);
                        DoTeleportAll(me->GetPositionX(),me->GetPositionY(),me->GetPositionZ(),me->GetOrientation());
                        DoCast(me->getVictim(), DUNGEON_MODE(SPELL_SKULLCRACKER, H_SPELL_SKULLCRACKER));
                        break;
                }
            }
            DoMeleeAttackIfReady();
        }
     };
};

class npc_chains: public CreatureScript
{
public:
    npc_chains() : CreatureScript("npc_chains") { }

    CreatureAI* GetAI(Creature* pCreature) const
    {
       return new npc_chainsAI(pCreature);
    }
    struct npc_chainsAI: public ScriptedAI
    {
        npc_chainsAI(Creature* pCreature) : ScriptedAI(pCreature)
        {
            pInstance = pCreature->GetInstanceScript();
        }

        InstanceScript *pInstance;
        EventMap events;

        void Reset()
        {
            events.Reset();
        }

        void EnterCombat(Unit* /*Ent*/)
        {
            events.RescheduleEvent(EVENT_CHAINS, 1000);
            DoCast(me->getVictim(), SPELL_CHAINS_OF_WOE_NPC);
        }


        void UpdateAI(const uint32 uiDiff)
        {
            if (!UpdateVictim())
                return;

            events.Update(uiDiff);

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch(eventId)
                {
                    case EVENT_CHAINS:
                        DoCast(me->getVictim(), SPELL_CHAINS_OF_WOE_NPC);
                        events.ScheduleEvent(EVENT_CHAINS, 1000);
                        break;
                }
            }
            DoMeleeAttackIfReady();
        }
     };
};

void AddSC_boss_romogg()
{
    new boss_romogg();
	new npc_chains();
}
