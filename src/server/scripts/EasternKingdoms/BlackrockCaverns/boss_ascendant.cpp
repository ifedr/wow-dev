#include"ScriptPCH.h"
#include"WorldPacket.h"
#include"blackrock_caverns.h"
#include"ScriptMgr.h"
#include"ScriptedCreature.h"
#include"SpellScript.h"
#include"SpellAuraEffects.h"

static const Position aSpawnLocations[3] =
{
    {331.947174, 543.660278, 66.006714f, 0.0f},
    {331.066010, 567.918030, 66.006599f, 0.0f},
    {337.066010, 550.918030, 66.006599f, 0.0f},
};

enum subCreatures
{
    SHADOW_OF_OBSIDIUS = 40817,
};

enum Spells
{
    SPELL_STONE_BLOW           = 76185,
    SPELL_THUNDERCLAP          = 76186,
    SPELL_TWILIGHT             = 76188,
    H_SPELL_TWILIGHT           = 93613,
    SPELL_VEIL                 = 76190,
};

enum Events
{
    EVENT_STONE_BLOW,
    EVENT_THUNDERCLAP,
    EVENT_TWILIGHT,
    EVENT_SPELL_VEIL,
};

class boss_ascendant: public CreatureScript
{
public:
    boss_ascendant() : CreatureScript("boss_ascendant") { }

    CreatureAI* GetAI(Creature* pCreature) const
    {
       return new boss_ascendantAI(pCreature);
    }
    struct boss_ascendantAI: public ScriptedAI
    {
        boss_ascendantAI(Creature* pCreature) : ScriptedAI(pCreature), Summons(me)
        {
            pInstance = pCreature->GetInstanceScript();
        }

        InstanceScript *pInstance;
        EventMap events;
        SummonList Summons;
        bool check_in;

        void Reset()
        {
            events.Reset();
            Summons.DespawnAll();
            if (pInstance && (pInstance->GetData(DATA_ASCENDANT_LORD_OBSIDIUS_EVENT) != DONE &&  !check_in))
                pInstance->SetData(DATA_ASCENDANT_LORD_OBSIDIUS_EVENT, NOT_STARTED);

            check_in = false;
        }

        void EnterCombat(Unit* /*Ent*/)
        {
            DoZoneInCombat();
            if (pInstance)
                pInstance->SetData(DATA_ASCENDANT_LORD_OBSIDIUS_EVENT, IN_PROGRESS);

            me->SummonCreature(SHADOW_OF_OBSIDIUS, aSpawnLocations[0], TEMPSUMMON_CORPSE_DESPAWN);
            me->SummonCreature(SHADOW_OF_OBSIDIUS, aSpawnLocations[1], TEMPSUMMON_CORPSE_DESPAWN);
            me->SummonCreature(SHADOW_OF_OBSIDIUS, aSpawnLocations[2], TEMPSUMMON_CORPSE_DESPAWN);

            events.RescheduleEvent(EVENT_STONE_BLOW, 10000);
            events.RescheduleEvent(EVENT_TWILIGHT, urand(5000, 10000));
            if(IsHeroic())
                events.RescheduleEvent(EVENT_THUNDERCLAP, urand(10000, 15000));

        }

        void JustSummoned(Creature *pSummoned)
        {
            pSummoned->SetInCombatWithZone();
            if (Unit* pTarget = SelectTarget(SELECT_TARGET_RANDOM,0))
                pSummoned->AI()->AttackStart(pTarget);
            Summons.Summon(pSummoned);
        }

        void SummonedCreatureDespawn(Creature *summon)
        {
            Summons.Despawn(summon);
        }

        void JustDied(Unit* /*Kill*/)
        {
            if (pInstance){
                pInstance->SetData(DATA_ASCENDANT_LORD_OBSIDIUS_EVENT, DONE);
                if (IsHeroic())
                {
                    int32 income = 7000;
                    pInstance->DoUpdateCurrency(CURRENCY_TYPE_JUSTICE_POINTS, income);
                }
            }
            Summons.DespawnAll();
        }

        void UpdateAI(const uint32 uiDiff)
        {
            if (!UpdateVictim())
                return;

            events.Update(uiDiff);

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch(eventId)
                {
                    case EVENT_STONE_BLOW:
                        DoCast(me->getVictim(), SPELL_STONE_BLOW);
                        events.ScheduleEvent(EVENT_STONE_BLOW, 10000);
                        break;
                    case EVENT_THUNDERCLAP:
                        DoCast(me->getVictim(), SPELL_THUNDERCLAP);
                        events.ScheduleEvent(EVENT_THUNDERCLAP, urand(10000, 15000));
                        break;
                    case EVENT_TWILIGHT:
                        if (Unit *pTarget = SelectUnit(SELECT_TARGET_RANDOM, 0))
                            DoCast(pTarget, DUNGEON_MODE(SPELL_TWILIGHT, H_SPELL_TWILIGHT));
                        events.ScheduleEvent(EVENT_TWILIGHT, urand(5000, 10000));
                        break;
                }
            }
            DoMeleeAttackIfReady();
        }
     };
};

class npc_shadow: public CreatureScript
{
public:
    npc_shadow() : CreatureScript("npc_shadow") { }

    CreatureAI* GetAI(Creature* pCreature) const
    {
       return new npc_shadowAI(pCreature);
    }
    struct npc_shadowAI: public ScriptedAI
    {
        npc_shadowAI(Creature* pCreature) : ScriptedAI(pCreature)
        {
            pInstance = pCreature->GetInstanceScript();
        }

        InstanceScript *pInstance;
        EventMap events;

        void Reset()
        {
            me->ApplySpellImmune(0, IMMUNITY_EFFECT, SPELL_EFFECT_KNOCK_BACK, true);//imune to knock aways like blast wave
            events.Reset();
        }

        void DamageTaken(Unit * /*done_by*/, uint32 &damage)
        {
            damage = 0;
        }

        void EnterCombat(Unit* /*Ent*/)
        {
            DoCast(me->getVictim(), SPELL_VEIL);
            events.RescheduleEvent(EVENT_SPELL_VEIL, 10000);
        }

        void UpdateAI(const uint32 uiDiff)
        {
            if (!UpdateVictim())
                return;

            events.Update(uiDiff);
            while (uint32 eventId = events.ExecuteEvent())
            {
                switch(eventId)
                {
                    case EVENT_SPELL_VEIL:
                        DoCast(me->getVictim(), SPELL_VEIL);
                        events.ScheduleEvent(EVENT_SPELL_VEIL, 10000);
                        break;
                }
            }
            DoMeleeAttackIfReady();
        }
     };
};

void AddSC_boss_ascendant()
{
    new boss_ascendant();
    new npc_shadow();
}
