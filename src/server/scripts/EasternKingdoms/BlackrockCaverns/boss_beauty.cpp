#include"ScriptPCH.h"
#include"WorldPacket.h"
#include"blackrock_caverns.h"
#include"ScriptMgr.h"
#include"ScriptedCreature.h"
#include"SpellScript.h"
#include"SpellAuraEffects.h"

enum Spells
{
    SPELL_MAGMA_SPIT           = 76031,
    SPELL_CHARGE               = 76030,
    H_SPELL_CHARGE             = 93580,
    SPELL_FLAMEBREAK           = 76032,
    H_SPELL_FLAMEBREAK         = 93583,
    SPELL_ROAR                 = 76028,
    H_SPELL_ROAR               = 93586,
};

enum Events
{
    EVENT_MAGMA_SPIT,
    EVENT_CHARGE_CUSTOM,
    EVENT_FLAMEBREAK,
    EVENT_ROAR,
};

class boss_beauty: public CreatureScript
{
public:
    boss_beauty() : CreatureScript("boss_beauty") { }

    CreatureAI* GetAI(Creature* pCreature) const
    {
       return new boss_beautyAI(pCreature);
    }
    struct boss_beautyAI: public ScriptedAI
    {
        boss_beautyAI(Creature* pCreature) : ScriptedAI(pCreature)
        {
            pInstance = pCreature->GetInstanceScript();
        }

        InstanceScript *pInstance;
        EventMap events;
        bool check_in;

        void Reset()
        {
            events.Reset();
            if (pInstance && (pInstance->GetData(DATA_BEAUTY_EVENT) != DONE &&  !check_in))
                pInstance->SetData(DATA_BEAUTY_EVENT, NOT_STARTED);

            check_in = false;
        }

        void EnterCombat(Unit* /*Ent*/)
        {
            DoZoneInCombat();
            events.RescheduleEvent(EVENT_CHARGE_CUSTOM, 20000);
            events.RescheduleEvent(EVENT_MAGMA_SPIT, urand(10000, 15000));
            events.RescheduleEvent(EVENT_FLAMEBREAK, urand(10000, 30000));
            events.RescheduleEvent(EVENT_ROAR, urand(15000, 20000));
            if (pInstance)
                pInstance->SetData(DATA_BEAUTY_EVENT, IN_PROGRESS);
        }

        void JustDied(Unit* /*Kill*/)
        {
            if (pInstance){
                pInstance->SetData(DATA_BEAUTY_EVENT, DONE);
                if (IsHeroic())
                {
                    int32 income = 7000;
                    pInstance->DoUpdateCurrency(CURRENCY_TYPE_JUSTICE_POINTS, income);
                }
            }
        }

        void UpdateAI(const uint32 uiDiff)
        {
            if (!UpdateVictim())
                return;

            events.Update(uiDiff);

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch(eventId)
                {
                    case EVENT_MAGMA_SPIT:
                        if (Unit *pTarget = SelectUnit(SELECT_TARGET_RANDOM, 0))
                            DoCast(pTarget, SPELL_MAGMA_SPIT);
                        events.ScheduleEvent(EVENT_MAGMA_SPIT, urand(10000, 15000));
                        break;
                    case EVENT_CHARGE_CUSTOM:
                        if (Unit *pTarget = SelectUnit(SELECT_TARGET_RANDOM, 5))
                            DoCast(pTarget, DUNGEON_MODE(SPELL_CHARGE, H_SPELL_CHARGE));
                        events.ScheduleEvent(EVENT_CHARGE_CUSTOM, 20000);
                        break;
                    case EVENT_FLAMEBREAK:
                        DoCast(me->getVictim(), DUNGEON_MODE(SPELL_FLAMEBREAK, H_SPELL_FLAMEBREAK));
                        events.ScheduleEvent(EVENT_FLAMEBREAK, urand(10000, 30000));
                        break;
                    case EVENT_ROAR:
                        DoCast(me->getVictim(), DUNGEON_MODE(SPELL_ROAR, H_SPELL_ROAR));
                        events.ScheduleEvent(EVENT_ROAR, urand(15000, 20000));
                        break;
                }
            }
            DoMeleeAttackIfReady();
        }
     };
};

void AddSC_boss_beauty()
{
    new boss_beauty();
}
