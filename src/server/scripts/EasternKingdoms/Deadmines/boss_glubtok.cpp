#include"ScriptPCH.h"
#include"WorldPacket.h"
#include"deadmines.h"
#include"ScriptMgr.h"
#include"ScriptedCreature.h"
#include"SpellScript.h"
#include"SpellAuraEffects.h"

enum Spells
{
    SPELL_FIRE_BLOSSOM         = 88129,
    H_SPELL_FIRE_BLOSSOM	   = 91286,
    SPELL_FROST_BLOSSOM        = 88169,
    H_SPELL_FROST_BLOSSOM	   = 91287,
	SPELL_FISTS_OF_FLAME	   = 87859,
	SPELL_FISTS_OF_FROST	   = 87861,
	SPELL_FIRE_WALL			   = 43113,
};

enum Events
{
    EVENT_FIRE_WALL,
    EVENT_FIRE_BLOSSOM,
    EVENT_FROST_BLOSSOM,
    EVENT_FLAME_FISTS,
    EVENT_FROST_FISTS,
};

class boss_glubtok: public CreatureScript
{
public:
    boss_glubtok() : CreatureScript("boss_glubtok") { }

    CreatureAI* GetAI(Creature* pCreature) const
    {
       return new boss_glubtokAI(pCreature);
    }
    struct boss_glubtokAI: public ScriptedAI
    {
        boss_glubtokAI(Creature* pCreature) : ScriptedAI(pCreature)
        {
            pInstance = pCreature->GetInstanceScript();
        }

        InstanceScript *pInstance;
        EventMap events;
        bool check_in;
		bool phase;

        void Reset()
        {
            events.Reset();
            if (pInstance && (pInstance->GetData(DATA_GLUBTOK_EVENT) != DONE &&  !check_in))
                pInstance->SetData(DATA_GLUBTOK_EVENT, NOT_STARTED);

			phase = false;
            check_in = false;
        }

        void EnterCombat(Unit* /*Ent*/)
        {
			DoZoneInCombat();
			if (IsHeroic())
			{
				events.ScheduleEvent(EVENT_FIRE_WALL, 2000);
				events.ScheduleEvent(EVENT_FIRE_BLOSSOM, 3000);
			}
            if (pInstance)
                pInstance->SetData(DATA_GLUBTOK_EVENT, IN_PROGRESS);

        }

        void JustDied(Unit* /*Kill*/)
        {
            if (pInstance)
			{
                pInstance->SetData(DATA_GLUBTOK_EVENT, DONE);
				if (IsHeroic())
				{
					int32 income = 7000;
					pInstance->DoUpdateCurrency(CURRENCY_TYPE_JUSTICE_POINTS, income);
				}
			}
        }

        void UpdateAI(const uint32 uiDiff)
        {
            if (!UpdateVictim())
                return;

            events.Update(uiDiff);

			if (!phase && HealthBelowPct(50) && HealthAbovePct(20))
			{
				DoStartNoMovement(me->getVictim());
                events.ScheduleEvent(EVENT_FIRE_WALL, 2000);
                events.ScheduleEvent(EVENT_FIRE_BLOSSOM, 3000);
				phase = true;
			}
			else if (phase && HealthBelowPct(20))
			{
				DoStartMovement(me->getVictim());
				events.ScheduleEvent(EVENT_FLAME_FISTS, 15000);
				phase = false;
			}

            while (uint32 eventId = events.ExecuteEvent()) {
                switch (eventId) {
                    case EVENT_FLAME_FISTS:
                        if (!phase) {
                            DoCast(me, SPELL_FISTS_OF_FLAME);
                            events.RescheduleEvent(EVENT_FROST_FISTS,
                                    urand(15000, 20000));
                        }
                        break;
                    case EVENT_FROST_FISTS:
                        if (!phase) {
                            DoCast(me, SPELL_FISTS_OF_FROST);
                            events.RescheduleEvent(EVENT_FLAME_FISTS,
                                    urand(15000, 20000));
                        }
                        break;
                    case EVENT_FIRE_BLOSSOM:
                        if (phase) {
                            if (Unit *pTarget = SelectUnit(SELECT_TARGET_RANDOM, 0))
                                DoCast(
                                        pTarget,
                                        DUNGEON_MODE(SPELL_FIRE_BLOSSOM,
                                                H_SPELL_FIRE_BLOSSOM));
                            events.RescheduleEvent(EVENT_FROST_BLOSSOM, 3000);
                        }
                        break;
                    case EVENT_FROST_BLOSSOM:
                        if (phase) {
                            if (Unit *pTarget = SelectUnit(SELECT_TARGET_RANDOM, 0))
                                DoCast(
                                        pTarget,
                                        DUNGEON_MODE(SPELL_FROST_BLOSSOM,
                                                H_SPELL_FROST_BLOSSOM));
                            events.RescheduleEvent(EVENT_FIRE_BLOSSOM, 3000);
                        }
                        break;
                    case EVENT_FIRE_WALL:
                        if (phase) {
                            DoCast(me, SPELL_FIRE_WALL);
                            events.RescheduleEvent(EVENT_FIRE_WALL, 2000);
                        }
                        break;
                }
            }
            DoMeleeAttackIfReady();           
        }
     };
};

void AddSC_boss_glubtok()
{
    new boss_glubtok();
}
