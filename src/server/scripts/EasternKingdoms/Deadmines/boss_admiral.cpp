#include"ScriptPCH.h"
#include"WorldPacket.h"
#include"deadmines.h"
#include"ScriptMgr.h"
#include"ScriptedCreature.h"
#include"SpellScript.h"
#include"SpellAuraEffects.h"

enum Spells
{
    SPELL_SWIPE			       = 88839,
    H_SPELL_SWIPE			   = 91859,
	SPELL_THROAT			   = 91863,
	SPELL_BLOOD				   = 88736,
};

enum Events
{
    EVENT_SWIPE,
    EVENT_THROAT,
};

class boss_admiral: public CreatureScript
{
public:
    boss_admiral() : CreatureScript("boss_admiral") { }

    CreatureAI* GetAI(Creature* pCreature) const
    {
       return new boss_admiralAI(pCreature);
    }
    struct boss_admiralAI: public ScriptedAI
    {
        boss_admiralAI(Creature* pCreature) : ScriptedAI(pCreature)
        {
            pInstance = pCreature->GetInstanceScript();
        }

        InstanceScript *pInstance;
        EventMap events;
        bool check_in;

        void Reset()
        {
            events.Reset();
            if (pInstance && (pInstance->GetData(DATA_ADMIRAL_EVENT) != DONE &&  !check_in))
                pInstance->SetData(DATA_ADMIRAL_EVENT, NOT_STARTED);

            check_in = false;
        }

        void EnterCombat(Unit* /*Ent*/)
        {
			DoZoneInCombat();
        	events.ScheduleEvent(EVENT_SWIPE, urand(15000, 20000));
			events.ScheduleEvent(EVENT_THROAT, urand(25000, 30000));
            if (pInstance)
                pInstance->SetData(DATA_ADMIRAL_EVENT, IN_PROGRESS);

        }

        void JustDied(Unit* /*Kill*/)
        {
            if (pInstance)
			{
                pInstance->SetData(DATA_ADMIRAL_EVENT, DONE);
				if (IsHeroic())
				{
					int32 income = 7000;
					pInstance->DoUpdateCurrency(CURRENCY_TYPE_JUSTICE_POINTS, income);
				}
            }
        }

        void UpdateAI(const uint32 uiDiff)
        {
            if (!UpdateVictim())
                return;

            events.Update(uiDiff);

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch(eventId)
                {
                    case EVENT_THROAT:
							if (Unit *pTarget = SelectUnit(SELECT_TARGET_RANDOM, 4))
								DoCast(pTarget, SPELL_THROAT);
							events.RescheduleEvent(EVENT_THROAT, urand(25000, 30000));
                        break;
					case EVENT_SWIPE:
							DoCast(me->getVictim(), SPELL_SWIPE);
							events.RescheduleEvent(EVENT_SWIPE, urand(15000, 20000));
                        break;
                }
            }
            DoMeleeAttackIfReady();           
        }
     };
};

void AddSC_boss_admiral()
{
    new boss_admiral();
}
