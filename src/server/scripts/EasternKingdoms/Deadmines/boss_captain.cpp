#include"ScriptPCH.h"
#include"WorldPacket.h"
#include"deadmines.h"
#include"ScriptMgr.h"
#include"ScriptedCreature.h"
#include"SpellScript.h"
#include"SpellAuraEffects.h"

enum Spells
{
    SPELL_CAULDRON			       = 89252,
};

enum Events
{
    /*Event*/
};

class boss_captain: public CreatureScript
{
public:
    boss_captain() : CreatureScript("boss_captain") { }

    CreatureAI* GetAI(Creature* pCreature) const
    {
       return new boss_captainAI(pCreature);
    }
    struct boss_captainAI: public ScriptedAI
    {
        boss_captainAI(Creature* pCreature) : ScriptedAI(pCreature)
        {
            pInstance = pCreature->GetInstanceScript();
        }

        InstanceScript *pInstance;
        EventMap events;
        bool check_in;

        void Reset()
        {
            events.Reset();
            if (pInstance && (pInstance->GetData(DATA_CAPTAIN_EVENT) != DONE &&  !check_in))
                pInstance->SetData(DATA_CAPTAIN_EVENT, NOT_STARTED);

            check_in = false;
        }

        void EnterCombat(Unit* /*Ent*/)
        {
			DoZoneInCombat();
            if (pInstance)
                pInstance->SetData(DATA_CAPTAIN_EVENT, IN_PROGRESS);
			DoCast(me, SPELL_CAULDRON);
        }

        void JustDied(Unit* /*Kill*/)
        {
            if (pInstance)
			{
                pInstance->SetData(DATA_CAPTAIN_EVENT, DONE);
				if (IsHeroic())
				{
					int32 income = 7000;
					pInstance->DoUpdateCurrency(CURRENCY_TYPE_JUSTICE_POINTS, income);
				}
            }
        }

        void UpdateAI(const uint32 uiDiff)
        {
            if (!UpdateVictim())
                return;

            events.Update(uiDiff);

            DoMeleeAttackIfReady();           
        }
     };
};

void AddSC_boss_captain()
{
    new boss_captain();
}