#include"ScriptPCH.h"
#include"WorldPacket.h"
#include"deadmines.h"
#include"ScriptMgr.h"
#include"ScriptedCreature.h"
#include"SpellScript.h"
#include"SpellAuraEffects.h"

enum Spells
{
    SPELL_HARVEST		       = 88521,
    H_SPELL_HARVEST			   = 91718,
    SPELL_REAPER		       = 88490,
    H_SPELL_REAPER			   = 91717,
	SPELL_SAFETY			   = 88522,
	H_SPELL_SAFETY			   = 91720,
	SPELL_OVERDRIVE			   = 88481,
};

enum Events
{
    EVENT_OVERDRIVE,
    EVENT_HARVEST,
    EVENT_REAPER,
};

class boss_reaper: public CreatureScript
{
public:
    boss_reaper() : CreatureScript("boss_reaper") { }

    CreatureAI* GetAI(Creature* pCreature) const
    {
       return new boss_reaperAI(pCreature);
    }
    struct boss_reaperAI: public ScriptedAI
    {
        boss_reaperAI(Creature* pCreature) : ScriptedAI(pCreature)
        {
            pInstance = pCreature->GetInstanceScript();
        }

        InstanceScript *pInstance;
        EventMap events;
        bool enrage;
        bool check_in;
		float curr_health;

        void Reset()
        {
            events.Reset();
            if (pInstance && (pInstance->GetData(DATA_REAPER_EVENT) != DONE &&  !check_in))
                pInstance->SetData(DATA_REAPER_EVENT, NOT_STARTED);

            check_in = false;
            enrage = false;
        }

        void EnterCombat(Unit* /*Ent*/)
        {
			DoZoneInCombat();
        	events.ScheduleEvent(EVENT_OVERDRIVE, urand(30000, 60000));
			events.ScheduleEvent(EVENT_HARVEST, urand(15000, 20000));
			events.ScheduleEvent(EVENT_REAPER, urand(7000, 15000));
            if (pInstance)
                pInstance->SetData(DATA_REAPER_EVENT, IN_PROGRESS);
        }

        void JustDied(Unit* /*Kill*/)
        {
            if (pInstance)
			{
                pInstance->SetData(DATA_REAPER_EVENT, DONE);
				if (IsHeroic())
				{
					int32 income = 7000;
					pInstance->DoUpdateCurrency(CURRENCY_TYPE_JUSTICE_POINTS, income);
				}
            }
        }

        void UpdateAI(const uint32 uiDiff)
        {
            if (!UpdateVictim())
                return;

            events.Update(uiDiff);

            if(!enrage && HealthBelowPct(30)){
            	DoCast(me, DUNGEON_MODE(SPELL_SAFETY, H_SPELL_SAFETY));
            	enrage = true;
            }

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch(eventId)
                {
                    case EVENT_OVERDRIVE:
                        DoCast(me->getVictim(), SPELL_OVERDRIVE);
                        events.RescheduleEvent(EVENT_OVERDRIVE, urand(30000, 60000));
                        break;
                    case EVENT_HARVEST:
                        DoCast(me->getVictim(), SPELL_HARVEST);
                        events.RescheduleEvent(EVENT_HARVEST, urand(15000, 20000));
                        break;
                    case EVENT_REAPER:
                        DoCast(me->getVictim(), SPELL_REAPER);
                        events.RescheduleEvent(EVENT_REAPER, urand(7000, 15000));
                        break;
                }
            }
            DoMeleeAttackIfReady();           
        }
     };
};

void AddSC_boss_reaper()
{
    new boss_reaper();
}
