#include"ScriptPCH.h"
#include"deadmines.h"

#define ENCOUNTERS 6

class instance_deadmines: public InstanceMapScript
{
public:
    instance_deadmines() : InstanceMapScript("instance_deadmines", 36) { }
    
    InstanceScript* GetInstanceScript(InstanceMap *map) const
    {
        return new instance_deadmines_InstanceMapScript(map);
    }
    
    struct instance_deadmines_InstanceMapScript: public InstanceScript
    {
        instance_deadmines_InstanceMapScript(InstanceMap *map) : InstanceScript(map) { }

        uint32 uiEncounter[ENCOUNTERS];

        uint64 uiGlubtok;
		uint64 uiHelix;
		uint64 uiReaper;
		uint64 uiAdmiral;
		uint64 uiCaptain;
		uint64 uiVanessa;
 
        void Initialize()
        {
            uiGlubtok = 0;
            uiHelix = 0;
			uiReaper = 0;
			uiAdmiral = 0;
			uiCaptain = 0;
			uiVanessa = 0;
            for(uint8 i=0; i < ENCOUNTERS; ++i)
                uiEncounter[i] = NOT_STARTED;
            
        }

        bool IsEncounterInProgress() const
        {
            for(uint8 i=0; i < ENCOUNTERS; ++i)
            {
                if (uiEncounter[i] == IN_PROGRESS)
                    return true;
            }
            return false;
        }

        void OnCreatureCreate(Creature* pCreature, bool)
        {
            switch(pCreature->GetEntry())
            {
                case BOSS_GLUBTOK:
                    uiGlubtok = pCreature->GetGUID();
                    break;
				case BOSS_HELIX:
                    uiHelix = pCreature->GetGUID();
                    break;
				case BOSS_REAPER:
                    uiReaper = pCreature->GetGUID();
                    break;
				case BOSS_ADMIRAL:
                    uiAdmiral = pCreature->GetGUID();
                    break;
				case BOSS_CAPTAIN:
                    uiCaptain = pCreature->GetGUID();
                    break;
				case BOSS_VANESSA:
                    uiVanessa = pCreature->GetGUID();
                    break;
            }
        }

        uint64 getData64(uint32 identifier)
        {
            switch(identifier)
            {
                case DATA_GLUBTOK:
                    return uiGlubtok;
				case DATA_HELIX:
                    return uiHelix;
				case DATA_REAPER:
                    return uiReaper;
				case DATA_ADMIRAL:
                    return uiAdmiral;
				case DATA_CAPTAIN:
                    return uiCaptain;
				case DATA_VANESSA:
                    return uiVanessa;
            }
        return 0;
        }

        void SetData(uint32 type, uint32 data)
        {
            switch(type)
            {
                case DATA_GLUBTOK:
                    uiEncounter[0] = data;
                    break;
				case DATA_HELIX:
                    uiEncounter[1] = data;
                    break;
				case DATA_REAPER:
                    uiEncounter[2] = data;
                    break;
				case DATA_ADMIRAL:
                    uiEncounter[3] = data;
                    break;
				case DATA_CAPTAIN:
                    uiEncounter[4] = data;
                    break;
				case DATA_VANESSA:
                    uiEncounter[5] = data;
                    break;
            }

            if (data == DONE)
            {
                SaveToDB();
            }
        }

        std::string GetSaveData()
        {
            OUT_SAVE_INST_DATA;

            std::string str_data;
            std::ostringstream saveStream;
            saveStream << "D M" << uiEncounter[0] << " " << uiEncounter[1] << " " << uiEncounter[2] << " " << uiEncounter[3] << " " << uiEncounter[4] << " " << uiEncounter[5];
            str_data = saveStream.str();

            OUT_SAVE_INST_DATA_COMPLETE;
            return str_data;
        }

        void Load(const char* in)
        {
            if (!in)
            {
                OUT_LOAD_INST_DATA_FAIL;
                return;
            }

            OUT_LOAD_INST_DATA(in);

            char dataHead1, dataHead2;
            uint16 data0, data1, data2, data3, data4, data5;

            std::istringstream loadStream(in);
            loadStream >> dataHead1 >> dataHead2 >> data0 >> data1 >> data2 >> data3 >> data4 >> data5;

            if (dataHead1 == 'D' && dataHead2 == 'M')
            {
                uiEncounter[0] = data0;
                uiEncounter[1] = data1;
				uiEncounter[2] = data2;
				uiEncounter[3] = data3;
				uiEncounter[4] = data4;
				uiEncounter[5] = data5;
                       
                for(uint8 i=0; i < ENCOUNTERS; ++i)
                    if (uiEncounter[i] == IN_PROGRESS)
                        uiEncounter[i] = NOT_STARTED;
            }
            else OUT_LOAD_INST_DATA_FAIL;

            OUT_LOAD_INST_DATA_COMPLETE;
        }
    };
};

void AddSC_instance_deadmines()
{
    new instance_deadmines();
}
