#include"ScriptPCH.h"
#include"WorldPacket.h"
#include"shadowfang.h"
#include"ScriptMgr.h"
#include"ScriptedCreature.h"
#include"SpellScript.h"
#include"SpellAuraEffects.h"

enum Spells
{
    SPELL_VEIL				= 23224,
    H_SPELL_VEIL			= 69633,
	SPELL_SUMMON			= 93857,
};

enum Events
{
    EVENT_VEIL,
};

class boss_silverlaine: public CreatureScript
{
public:
    boss_silverlaine() : CreatureScript("boss_silverlaine") { }

    CreatureAI* GetAI(Creature* pCreature) const
    {
       return new boss_silverlaineAI(pCreature);
    }
    struct boss_silverlaineAI: public ScriptedAI
    {
        boss_silverlaineAI(Creature* pCreature) : ScriptedAI(pCreature)
        {
            pInstance = pCreature->GetInstanceScript();
        }

        InstanceScript *pInstance;
        EventMap events;
        bool check_in;
		bool summon1;
		bool summon2;

        void Reset()
        {
            events.Reset();
            if (pInstance && (pInstance->GetData(DATA_SILVERLAINE_EVENT) != DONE &&  !check_in))
                pInstance->SetData(DATA_SILVERLAINE_EVENT, NOT_STARTED);

            check_in = false;
			summon1 = false;
			summon2 = false;
        }

        void EnterCombat(Unit* /*Ent*/)
        {
			DoZoneInCombat();

            if (pInstance)
                pInstance->SetData(DATA_SILVERLAINE_EVENT, IN_PROGRESS);

			events.ScheduleEvent(EVENT_VEIL, 20000);
        }

        void JustDied(Unit* /*Kill*/)
        {
            if (pInstance)
			{
                pInstance->SetData(DATA_SILVERLAINE_EVENT, DONE);
				if (IsHeroic())
				{
					int32 income = 7000;
					pInstance->DoUpdateCurrency(CURRENCY_TYPE_JUSTICE_POINTS, income);
				}
            }
        }

        void UpdateAI(const uint32 uiDiff)
        {
            if (!UpdateVictim())
                return;

            events.Update(uiDiff);

			if (IsHeroic() && HealthBelowPct(75.0) && !summon1)
			{
				DoCast(me->getVictim(), SPELL_SUMMON); 
				summon1 = true;
			}

			if (IsHeroic() && HealthBelowPct(35.0) && !summon2)
			{
				DoCast(me->getVictim(), SPELL_SUMMON); 
				summon2 = true;
			}

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch(eventId)
                {
                    case EVENT_VEIL:
						if (Unit *pTarget = SelectUnit(SELECT_TARGET_RANDOM, 0))
							DoCast(pTarget, DUNGEON_MODE(SPELL_VEIL, H_SPELL_VEIL));
                        events.RescheduleEvent(EVENT_VEIL, 20000);
                        break;
                }
            }
            DoMeleeAttackIfReady();           
        }
     };
};

void AddSC_boss_silverlaine()
{
    new boss_silverlaine();
}
