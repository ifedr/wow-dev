#include"ScriptPCH.h"
#include"WorldPacket.h"
#include"shadowfang.h"
#include"ScriptMgr.h"
#include"ScriptedCreature.h"
#include"SpellScript.h"
#include"SpellAuraEffects.h"

enum Spells
{
    SPELL_ASPHYXIATE		= 93423,
    H_SPELL_ASPHYXIATE		= 93710,
	SPELL_PAIN				= 93581,
    H_SPELL_PAIN			= 93712,
	SPELL_MEND				= 93654,
    H_SPELL_MEND			= 93713,
	SPELL_DARK				= 93757,
	SPELL_WRACKING_PAIN		= 93720,
	SPELL_CALAMITY			= 93812,
};

enum Events
{
    EVENT_ASPHYXIATE,
	EVENT_PAIN,
	EVENT_MEND,
	EVENT_WRACKING_PAIN,
	EVENT_CALAMITY,
};

class boss_ashbury: public CreatureScript
{
public:
    boss_ashbury() : CreatureScript("boss_ashbury") { }

    CreatureAI* GetAI(Creature* pCreature) const
    {
       return new boss_ashburyAI(pCreature);
    }
    struct boss_ashburyAI: public ScriptedAI
    {
        boss_ashburyAI(Creature* pCreature) : ScriptedAI(pCreature)
        {
            pInstance = pCreature->GetInstanceScript();
        }

        InstanceScript *pInstance;
        EventMap events;
        bool check_in;
		float curr_health;
		bool phase;

        void Reset()
        {
            events.Reset();
            if (pInstance && (pInstance->GetData(DATA_ASHBURY_EVENT) != DONE &&  !check_in))
                pInstance->SetData(DATA_ASHBURY_EVENT, NOT_STARTED);

			phase = false;
            check_in = false;
        }

        void EnterCombat(Unit* /*Ent*/)
        {
			DoZoneInCombat();

            if (pInstance)
                pInstance->SetData(DATA_ASHBURY_EVENT, IN_PROGRESS);

			if (!phase)
				events.RescheduleEvent(EVENT_ASPHYXIATE, urand(55000, 65000));
				events.RescheduleEvent(EVENT_PAIN, urand(10000, 15000));
				events.RescheduleEvent(EVENT_MEND, urand(55000, 65000));
				if (IsHeroic())
					events.RescheduleEvent(EVENT_WRACKING_PAIN, urand(25000, 35000));

			if (phase)
				events.RescheduleEvent(EVENT_CALAMITY, 1000);
        }

        void JustDied(Unit* /*Kill*/)
        {
            if (pInstance)
			{
                pInstance->SetData(DATA_ASHBURY_EVENT, DONE);
				if (IsHeroic())
				{
					int32 income = 7000;
					pInstance->DoUpdateCurrency(CURRENCY_TYPE_JUSTICE_POINTS, income);
				}
            }
        }

        void UpdateAI(const uint32 uiDiff)
        {
            if (!UpdateVictim())
                return;

            events.Update(uiDiff);
			curr_health = me->GetHealthPct();
			if (curr_health < 20.0 && !phase)
			{
				DoCast(me, SPELL_DARK); 
				phase = true;
			}

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch(eventId)
                {
                    case EVENT_ASPHYXIATE:
						DoCast(me->getVictim(), DUNGEON_MODE(SPELL_ASPHYXIATE, H_SPELL_ASPHYXIATE));
                        events.ScheduleEvent(EVENT_ASPHYXIATE, 60000);
                        break;
					case EVENT_PAIN:
						if (Unit *pTarget = SelectUnit(SELECT_TARGET_RANDOM, 0))
							DoCast(me->getVictim(), DUNGEON_MODE(SPELL_PAIN, H_SPELL_PAIN));
                        events.ScheduleEvent(EVENT_PAIN, urand(10000, 15000));
                        break;
					case EVENT_MEND:
						DoCast(me, DUNGEON_MODE(SPELL_MEND, H_SPELL_MEND));
                        events.ScheduleEvent(EVENT_MEND, urand(55000, 65000));
                        break;
					case EVENT_WRACKING_PAIN:
						DoCast(me->getVictim(), SPELL_WRACKING_PAIN);
                        events.ScheduleEvent(EVENT_WRACKING_PAIN, urand(25000, 35000));
                        break;
					case EVENT_CALAMITY:
						DoCast(me->getVictim(), SPELL_CALAMITY);
                        events.ScheduleEvent(EVENT_CALAMITY, 1000);
                        break;
                }
            }
            DoMeleeAttackIfReady();           
        }
     };
};

void AddSC_boss_ashbury()
{
    new boss_ashbury();
}
