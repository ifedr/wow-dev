#include"ScriptPCH.h"
#include"WorldPacket.h"
#include"shadowfang.h"
#include"ScriptMgr.h"
#include"ScriptedCreature.h"
#include"SpellScript.h"
#include"SpellAuraEffects.h"

enum Spells
{
    SPELL_DESECRATION			= 55741,
    H_SPELL_DESECRATION			= 68766,
	SPELL_MALEFIC_STRIKE		= 93685,
	SPELL_SHIELD				= 93693, //self only
	H_SPELL_SHIELD				= 93736,

	//Wailing Guardsman
	SPELL_UNHOLY				= 93844,
	SPELL_SCREAM				= 7074,
	SPELL_MORTAL_STRICE			= 91801,

	//Tormented Officer
	SPELL_FORSAKEN				= 7054,
	SPELL_SHIELD_WALL			= 15062,

};

static const Position aSpawnLocations[2] =
{
    {-249.54, 2263.47, 101.0, 4.9},
    {-250.41, 2237, 101.0, 1.82},
};

enum subCreatures
{
	TORMENTED_OFFICER = 50615,
	WAILING_GUARDSMAN = 50613,
};
     
enum Events
{
    EVENT_SUMMON,
	EVENT_MALEFIC_STRIKE,
	EVENT_SHIELD,
	EVENT_DESECRATION,

	//Wailing Guardsman
	EVENT_SCREAM,
	EVENT_MORTAL_STRICE,

	//Tormented Officer
	EVENT_FORSAKEN,

};
class boss_springvale: public CreatureScript
{
public:
    boss_springvale() : CreatureScript("boss_springvale") { }

    CreatureAI* GetAI(Creature* pCreature) const
    {
        return new boss_springvaleAI(pCreature);
    }

    struct boss_springvaleAI: public ScriptedAI
    {
        boss_springvaleAI(Creature* pCreature) : ScriptedAI(pCreature), Summons(me)
        {
            pInstance = pCreature->GetInstanceScript();
        }

        InstanceScript *pInstance;
        EventMap events;
        bool check_in;
		SummonList Summons;

		void JustSummoned(Creature *pSummoned)
        {
            pSummoned->SetInCombatWithZone();
            if (Unit* pTarget = SelectTarget(SELECT_TARGET_RANDOM,0))
                pSummoned->AI()->AttackStart(pTarget);
            Summons.Summon(pSummoned);
        }

        void SummonedCreatureDespawn(Creature *summon)
        {
            Summons.Despawn(summon);
        }

        void Reset()
        {
            events.Reset();
			Summons.DespawnAll();
			me->SummonCreature(TORMENTED_OFFICER, aSpawnLocations[0], TEMPSUMMON_CORPSE_DESPAWN);
			me->SummonCreature(WAILING_GUARDSMAN, aSpawnLocations[1], TEMPSUMMON_CORPSE_DESPAWN);
            if (pInstance && (pInstance->GetData(DATA_SPRINGVALE_EVENT) != DONE &&  !check_in))
                pInstance->SetData(DATA_SPRINGVALE_EVENT, NOT_STARTED);

            check_in = false;
        }

        void EnterCombat(Unit* /*Ent*/)
        {
			DoZoneInCombat();
			if(IsHeroic())
        		events.ScheduleEvent(EVENT_SUMMON, 60000);
			events.ScheduleEvent(EVENT_MALEFIC_STRIKE, 20000);
			events.ScheduleEvent(EVENT_SHIELD, urand(15000,30000));
			events.ScheduleEvent(EVENT_DESECRATION, urand(30000, 40000));
            if (pInstance)
                pInstance->SetData(DATA_SPRINGVALE_EVENT, IN_PROGRESS);

        }

        void JustDied(Unit* /*Kill*/)
        {
            if (pInstance)
			{
                pInstance->SetData(DATA_SPRINGVALE_EVENT, DONE);
				if (IsHeroic())
				{
					int32 income = 7000;
					pInstance->DoUpdateCurrency(CURRENCY_TYPE_JUSTICE_POINTS, income);
				}
            }
			Summons.DespawnAll();
        }

        void UpdateAI(const uint32 uiDiff)
        {
            if (!UpdateVictim())
                return;

            events.Update(uiDiff);

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch(eventId)
                {
                    case EVENT_SUMMON:
						me->SummonCreature(TORMENTED_OFFICER, aSpawnLocations[0], TEMPSUMMON_CORPSE_DESPAWN);
						me->SummonCreature(WAILING_GUARDSMAN, aSpawnLocations[0], TEMPSUMMON_CORPSE_DESPAWN);
						me->SummonCreature(TORMENTED_OFFICER, aSpawnLocations[1], TEMPSUMMON_CORPSE_DESPAWN);
						me->SummonCreature(WAILING_GUARDSMAN, aSpawnLocations[1], TEMPSUMMON_CORPSE_DESPAWN);
                        events.RescheduleEvent(EVENT_SUMMON, 60000);
                        break;
					case EVENT_MALEFIC_STRIKE:
						DoCast(me->getVictim(), SPELL_MALEFIC_STRIKE);
                        events.RescheduleEvent(EVENT_MALEFIC_STRIKE, 20000);
                        break;
					case EVENT_SHIELD:
						DoCast(me, DUNGEON_MODE(SPELL_SHIELD, H_SPELL_SHIELD));
                        events.RescheduleEvent(EVENT_SHIELD, urand(15000,30000));
                        break;
					case EVENT_DESECRATION:
						if (Unit *pTarget = SelectUnit(SELECT_TARGET_RANDOM, 0))
							DoCast(pTarget, DUNGEON_MODE(SPELL_DESECRATION, H_SPELL_DESECRATION));
                        events.RescheduleEvent(EVENT_DESECRATION, urand(30000, 40000));
                        break;
                }
            }
            DoMeleeAttackIfReady();           
        }
     };
};

class npc_wailing_guardsman: public CreatureScript
{
public:
    npc_wailing_guardsman() : CreatureScript("npc_wailing_guardsman") { }

    CreatureAI* GetAI(Creature* pCreature) const
    {
       return new npc_wailing_guardsmanAI(pCreature);
    }
    struct npc_wailing_guardsmanAI: public ScriptedAI
    {
        npc_wailing_guardsmanAI(Creature* pCreature) : ScriptedAI(pCreature)
        {
            pInstance = pCreature->GetInstanceScript();
        }

        InstanceScript *pInstance;
        EventMap events;

        void Reset()
        {
            events.Reset();
        }

        void EnterCombat(Unit* /*Ent*/)
        {
			DoZoneInCombat();
			DoCast(me->getVictim(), SPELL_UNHOLY);
			events.RescheduleEvent(EVENT_MORTAL_STRICE, 15000);
			events.RescheduleEvent(EVENT_SCREAM, 20000);
        }

        void UpdateAI(const uint32 uiDiff)
        {
            if (!UpdateVictim())
                return;

            events.Update(uiDiff);
            while (uint32 eventId = events.ExecuteEvent())
            {
                switch(eventId)
                {
                    case EVENT_SCREAM:
                    	DoCast(me->getVictim(), SPELL_SCREAM);
                        events.ScheduleEvent(EVENT_SCREAM, 20000);
                        break;
					case EVENT_MORTAL_STRICE:
                    	DoCast(me->getVictim(), SPELL_MORTAL_STRICE);
                        events.ScheduleEvent(EVENT_MORTAL_STRICE, 15000);
                        break;
                }
            }
            DoMeleeAttackIfReady();       
        }
     };
};

class npc_tormented_officer: public CreatureScript
{
public:
    npc_tormented_officer() : CreatureScript("npc_tormented_officer") { }

    CreatureAI* GetAI(Creature* pCreature) const
    {
       return new npc_tormented_officerAI(pCreature);
    }
    struct npc_tormented_officerAI: public ScriptedAI
    {
        npc_tormented_officerAI(Creature* pCreature) : ScriptedAI(pCreature)
        {
            pInstance = pCreature->GetInstanceScript();
        }

        InstanceScript *pInstance;
        EventMap events;
		float curr_health;
		bool shield;

        void Reset()
        {
            events.Reset();
			shield = false;
        }

        void EnterCombat(Unit* /*Ent*/)
        {
			DoZoneInCombat();
			DoCast(me->getVictim(), SPELL_UNHOLY);
			events.RescheduleEvent(EVENT_FORSAKEN, 10000);
        }

        void UpdateAI(const uint32 uiDiff)
        {
            if (!UpdateVictim())
                return;

			curr_health = me->GetHealthPct();
			if (curr_health < 20.0 && !shield)
			{
				DoCast(me, SPELL_SHIELD_WALL); 
				shield = true;
			}
            events.Update(uiDiff);
            while (uint32 eventId = events.ExecuteEvent())
            {
                switch(eventId)
                {
                    case EVENT_FORSAKEN:
						if (Unit *pTarget = SelectUnit(SELECT_TARGET_RANDOM, 0))
                    		DoCast(pTarget, SPELL_FORSAKEN);
                        events.ScheduleEvent(EVENT_FORSAKEN, 10000);
                        break;
                }
            }
            DoMeleeAttackIfReady();       
        }
     };
};

void AddSC_boss_springvale()
{
    new boss_springvale();
	new npc_tormented_officer();
	new npc_wailing_guardsman();
}
