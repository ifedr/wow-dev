#include"ScriptPCH.h"
#include"WorldPacket.h"
#include"shadowfang.h"
#include"ScriptMgr.h"
#include"ScriptedCreature.h"
#include"SpellScript.h"
#include"SpellAuraEffects.h"

enum Spells
{
    SPELL_ICE_SHARDS			= 93527,
	SPELL_TOXIC_1				= 93689,
	SPELL_TOXIC_2				= 93617,
	SPELL_POISONOUS				= 93697,
};
     
enum Events
{
    EVENT_ICE_SHARDS,
	EVENT_TOXIC_1,
	EVENT_TOXIC_2,
	EVENT_POISONOUS,
};

class boss_walden: public CreatureScript
{
public:
    boss_walden() : CreatureScript("boss_walden") { }

    CreatureAI* GetAI(Creature* pCreature) const
    {
       return new boss_waldenAI(pCreature);
    }
    struct boss_waldenAI: public ScriptedAI
    {
        boss_waldenAI(Creature* pCreature) : ScriptedAI(pCreature)
        {
            pInstance = pCreature->GetInstanceScript();
        }

        InstanceScript *pInstance;
        EventMap events;
        bool check_in;

        void Reset()
        {
            events.Reset();
            if (pInstance && (pInstance->GetData(DATA_WALDEN_EVENT) != DONE &&  !check_in))
                pInstance->SetData(DATA_WALDEN_EVENT, NOT_STARTED);

            check_in = false;
        }

        void EnterCombat(Unit* /*Ent*/)
        {
			DoZoneInCombat();
			if(IsHeroic())
			{
        		events.ScheduleEvent(EVENT_TOXIC_1, urand(45000,60000));
				events.ScheduleEvent(EVENT_TOXIC_2, urand(90000,120000));
			}
			events.ScheduleEvent(EVENT_ICE_SHARDS, 90000);
			events.ScheduleEvent(EVENT_POISONOUS, urand(10000,30000));
            if (pInstance)
                pInstance->SetData(DATA_WALDEN_EVENT, IN_PROGRESS);

        }

        void JustDied(Unit* /*Kill*/)
        {
            if (pInstance)
			{
                pInstance->SetData(DATA_WALDEN_EVENT, DONE);
				if (IsHeroic())
				{
					int32 income = 7000;
					pInstance->DoUpdateCurrency(CURRENCY_TYPE_JUSTICE_POINTS, income);
				}
            }
        }

        void UpdateAI(const uint32 uiDiff)
        {
            if (!UpdateVictim())
                return;

            events.Update(uiDiff);

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch(eventId)
                {
					case EVENT_ICE_SHARDS:
						DoCast(me->getVictim(), SPELL_ICE_SHARDS);
                        events.RescheduleEvent(EVENT_ICE_SHARDS, 90000);
                        break;
					case EVENT_TOXIC_1:
						DoCast(me->getVictim(), SPELL_TOXIC_1);
                        events.RescheduleEvent(EVENT_TOXIC_1, urand(90000,120000));
                        break;
					case EVENT_TOXIC_2:
						DoCast(me->getVictim(), SPELL_TOXIC_2);
                        events.RescheduleEvent(EVENT_TOXIC_2, urand(90000,120000));
                        break;
					case EVENT_POISONOUS:
						if (Unit *pTarget = SelectUnit(SELECT_TARGET_RANDOM, 0))
							DoCast(pTarget, SPELL_POISONOUS);
                        events.RescheduleEvent(EVENT_POISONOUS, urand(10000, 30000));
                        break;
                }
            }
            DoMeleeAttackIfReady();           
        }
     };
};

void AddSC_boss_walden()
{
    new boss_walden();
}
