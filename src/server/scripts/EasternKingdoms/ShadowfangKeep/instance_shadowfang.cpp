#include"ScriptPCH.h"
#include"shadowfang.h"

#define ENCOUNTERS 5

class instance_shadowfang: public InstanceMapScript
{
public:
    instance_shadowfang() : InstanceMapScript("instance_shadowfang", 33) { }
    
    InstanceScript* GetInstanceScript(InstanceMap *map) const
    {
        return new instance_shadowfang_InstanceMapScript(map);
    }
    
    struct instance_shadowfang_InstanceMapScript: public InstanceScript
    {
        instance_shadowfang_InstanceMapScript(InstanceMap *map) : InstanceScript(map) { }

        uint32 uiEncounter[ENCOUNTERS];

        uint64 uiAshbury;
		uint64 uiSilverlaine;
		uint64 uiSpringvale;
		uint64 uiWalden;
		uint64 uiGodfrey;
 
        void Initialize()
        {
            uiAshbury = 0;
            uiSilverlaine = 0;
			uiSpringvale = 0;
			uiWalden = 0;
			uiGodfrey = 0;
            for(uint8 i=0; i < ENCOUNTERS; ++i)
                uiEncounter[i] = NOT_STARTED;
            
        }

        bool IsEncounterInProgress() const
        {
            for(uint8 i=0; i < ENCOUNTERS; ++i)
            {
                if (uiEncounter[i] == IN_PROGRESS)
                    return true;
            }
            return false;
        }

        void OnCreatureCreate(Creature* pCreature, bool)
        {
            switch(pCreature->GetEntry())
            {
                case BOSS_ASHBURY:
                    uiAshbury = pCreature->GetGUID();
                    break;
				case BOSS_SILVERLAINE:
                    uiSilverlaine = pCreature->GetGUID();
                    break;
				case BOSS_SPRINGVALE:
                    uiSpringvale = pCreature->GetGUID();
                    break;
				case BOSS_WALDEN:
                    uiWalden = pCreature->GetGUID();
                    break;
				case BOSS_GODFREY:
                    uiGodfrey = pCreature->GetGUID();
                    break;
            }
        }

        uint64 getData64(uint32 identifier)
        {
            switch(identifier)
            {
                case DATA_ASHBURY:
                    return uiAshbury;
				case DATA_SILVERLAINE:
                    return uiSilverlaine;
				case DATA_SPRINGVALE:
                    return uiSpringvale;
				case DATA_WALDEN:
                    return uiWalden;
				case DATA_GODFREY:
                    return uiGodfrey;
            }
        return 0;
        }

        void SetData(uint32 type, uint32 data)
        {
            switch(type)
            {
                case DATA_ASHBURY:
                    uiEncounter[0] = data;
                    break;
				case DATA_SILVERLAINE:
                    uiEncounter[1] = data;
                    break;
				case DATA_SPRINGVALE:
                    uiEncounter[2] = data;
                    break;
				case DATA_WALDEN:
                    uiEncounter[3] = data;
                    break;
				case DATA_GODFREY:
                    uiEncounter[4] = data;
                    break;
            }

            if (data == DONE)
            {
                SaveToDB();
            }
        }

        std::string GetSaveData()
        {
            OUT_SAVE_INST_DATA;

            std::string str_data;
            std::ostringstream saveStream;
            saveStream << "S F" << uiEncounter[0] << " " << uiEncounter[1] << " " << uiEncounter[2] << " " << uiEncounter[3] << " " << uiEncounter[4];
            str_data = saveStream.str();

            OUT_SAVE_INST_DATA_COMPLETE;
            return str_data;
        }

        void Load(const char* in)
        {
            if (!in)
            {
                OUT_LOAD_INST_DATA_FAIL;
                return;
            }

            OUT_LOAD_INST_DATA(in);

            char dataHead1, dataHead2;
            uint16 data0, data1, data2, data3, data4;

            std::istringstream loadStream(in);
            loadStream >> dataHead1 >> dataHead2 >> data0 >> data1 >> data2 >> data3 >> data4;

            if (dataHead1 == 'S' && dataHead2 == 'F')
            {
                uiEncounter[0] = data0;
                uiEncounter[1] = data1;
				uiEncounter[2] = data2;
				uiEncounter[3] = data3;
				uiEncounter[4] = data4;
                       
                for(uint8 i=0; i < ENCOUNTERS; ++i)
                    if (uiEncounter[i] == IN_PROGRESS)
                        uiEncounter[i] = NOT_STARTED;
            }
            else OUT_LOAD_INST_DATA_FAIL;

            OUT_LOAD_INST_DATA_COMPLETE;
        }
    };
};

void AddSC_instance_shadowfang_keep()
{
    new instance_shadowfang();
}
