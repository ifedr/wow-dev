#include"ScriptPCH.h"
#include"WorldPacket.h"
#include"shadowfang.h"
#include"ScriptMgr.h"
#include"ScriptedCreature.h"
#include"SpellScript.h"
#include"SpellAuraEffects.h"

enum Spells
{
    SPELL_SUMMON			= 93707,
	SPELL_PISTOL			= 93520,
	SPELL_WOUND				= 93675, 
	H_SPELL_WOUND			= 93771, 
	SPELL_BULLETS			= 93629,
	H_SPELL_BULLETS			= 93761,
};
     
enum Events
{
    EVENT_SUMMON,
	EVENT_PISTOL,
	EVENT_WOUND,
	EVENT_BULLETS,
};

class boss_godfrey: public CreatureScript
{
public:
	boss_godfrey() : CreatureScript("boss_godfrey") { }

    CreatureAI* GetAI(Creature* pCreature) const
    {
       return new boss_godfreyAI(pCreature);
    }
    struct boss_godfreyAI: public ScriptedAI
    {
        boss_godfreyAI(Creature* pCreature) : ScriptedAI(pCreature)
        {
            pInstance = pCreature->GetInstanceScript();
        }

        InstanceScript *pInstance;
        EventMap events;
        bool check_in;

        void Reset()
        {
            events.Reset();
            if (pInstance && (pInstance->GetData(DATA_GODFREY_EVENT) != DONE &&  !check_in))
                pInstance->SetData(DATA_GODFREY_EVENT, NOT_STARTED);

            check_in = false;
        }

        void EnterCombat(Unit* /*Ent*/)
        {
			DoZoneInCombat();
			if(IsHeroic())
			{
        		events.ScheduleEvent(EVENT_SUMMON, 30000);
				events.ScheduleEvent(EVENT_PISTOL, urand(45000,60000));
			}
			events.ScheduleEvent(EVENT_WOUND, urand(5000,6000));
			events.ScheduleEvent(EVENT_BULLETS, urand(10000,30000));
            if (pInstance)
                pInstance->SetData(DATA_GODFREY_EVENT, IN_PROGRESS);

        }

        void JustDied(Unit* /*Kill*/)
        {
            if (pInstance)
			{
                pInstance->SetData(DATA_GODFREY_EVENT, DONE);
				if (IsHeroic())
				{
					int32 income = 7000;
					pInstance->DoUpdateCurrency(CURRENCY_TYPE_JUSTICE_POINTS, income);
				}
            }
        }

        void UpdateAI(const uint32 uiDiff)
        {
            if (!UpdateVictim())
                return;

            events.Update(uiDiff);

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch(eventId)
                {
					case EVENT_SUMMON:
						DoCast(me->getVictim(), SPELL_SUMMON);
                        events.RescheduleEvent(EVENT_SUMMON, 30000);
                        break;
					case EVENT_PISTOL:
						DoCast(me->getVictim(), SPELL_PISTOL);
                        events.RescheduleEvent(EVENT_PISTOL, urand(45000,60000));
                        break;
					case EVENT_WOUND:
						DoCast(me->getVictim(), DUNGEON_MODE(SPELL_WOUND, H_SPELL_WOUND));
                        events.RescheduleEvent(EVENT_WOUND, urand(5000,6000));
                        break;
					case EVENT_BULLETS:
						if (Unit *pTarget = SelectUnit(SELECT_TARGET_RANDOM, 0))
							DoCast(pTarget, DUNGEON_MODE(SPELL_BULLETS, H_SPELL_BULLETS));
                        events.RescheduleEvent(EVENT_BULLETS, urand(10000, 30000));
                        break;
                }
            }
            DoMeleeAttackIfReady();           
        }
     };
};

void AddSC_boss_godfrey()
{
    new boss_godfrey();
}
