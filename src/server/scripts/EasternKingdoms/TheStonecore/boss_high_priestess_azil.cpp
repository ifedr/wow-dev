/*
* Copyright (C) 2010-2011 Project SkyFire <http://www.projectskyfire.org/>
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "ScriptPCH.h"
#include "the_stonecore.h"
#include "Group.h"

enum Spells
{
    SPELL_BERSERK              = 47008,
    SPELL_CURSE_OF_BLOOD       = 79345,
    SPELL_ENERGY_SHIELD        = 79050,
    SPELL_FORCE_GRIP           = 79351,
    SPELL_SEISMIC_SHARD        = 79002,
    SPELL_GRAVITY_WELL         = 79340,
};

enum Events
{
    EVENT_BERSERK = 1,
    EVENT_CURSE_OF_BLOOD,
    EVENT_ENERGY_SHIELD,
    EVENT_FORCE_GRIP,
    EVENT_SEISMIC_SHARD,
    EVENT_GRAVITY_WELL,
};

class boss_high_priestess_azil: public CreatureScript
{
    public:
        boss_high_priestess_azil() : CreatureScript("boss_high_priestess_azil") { }

    struct boss_high_priestess_azilAI: public BossAI
    {
        boss_high_priestess_azilAI(Creature* pCreature) : BossAI(pCreature, DATA_HIGH_PRIESTESS_AZIL) 
	 {
	     me->ApplySpellImmune(0, IMMUNITY_EFFECT, SPELL_EFFECT_KNOCK_BACK, true);
	     me->ApplySpellImmune(0, IMMUNITY_MECHANIC, MECHANIC_GRIP, true);
	 }

        EventMap events;
        bool check_in;

        uint32 energy_shield_casted;

        void Reset()
        {
            events.Reset();
            if (pInstance && (pInstance->GetData(DATA_HIGH_PRIESTESS_AZIL_EVENT) != DONE &&  !check_in))
                pInstance->SetData(DATA_HIGH_PRIESTESS_AZIL_EVENT, NOT_STARTED);

            me->RemoveAurasDueToSpell(SPELL_BERSERK);
            events.ScheduleEvent(EVENT_BERSERK, 300 *IN_MILLISECONDS);
            events.ScheduleEvent(EVENT_CURSE_OF_BLOOD, 5 *IN_MILLISECONDS);
	     events.ScheduleEvent(EVENT_FORCE_GRIP, 14 *IN_MILLISECONDS);
            energy_shield_casted = 0;
			check_in = false;
        }

		void EnterCombat(Unit* /*Ent*/)
        {
			DoZoneInCombat();

            if (pInstance)
                pInstance->SetData(DATA_HIGH_PRIESTESS_AZIL_EVENT, IN_PROGRESS);
        }

        void JustDied(Unit* /*killer*/)
        {
            if (pInstance)
			{
                pInstance->SetData(DATA_HIGH_PRIESTESS_AZIL_EVENT, DONE);
				if (IsHeroic())
				{
					int32 income = 7500;
					pInstance->DoUpdateCurrency(CURRENCY_TYPE_JUSTICE_POINTS, income);
				}
            }
        }

        void UpdateAI(const uint32 diff)
        {
            if (!UpdateVictim())
                return;

	     if (me->GetHealthPct() < 33 && energy_shield_casted == 0)
            {
                DoCast(SPELL_ENERGY_SHIELD);
                events.DelayEvents(3 *IN_MILLISECONDS);
                energy_shield_casted = 1;
            }
            
            events.Update(diff);

            if (me->HasUnitState(UNIT_STAT_CASTING))
                    return;

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                case EVENT_CURSE_OF_BLOOD:
                    DoCast(SPELL_CURSE_OF_BLOOD);
                    events.RescheduleEvent(EVENT_CURSE_OF_BLOOD, 15 *IN_MILLISECONDS);
                    break;
		  case EVENT_FORCE_GRIP:
		      DoCast(SPELL_FORCE_GRIP);
                    events.RescheduleEvent(EVENT_FORCE_GRIP, 20 *IN_MILLISECONDS);
                    break;
                case EVENT_BERSERK:
                    DoCast(me, SPELL_BERSERK);
                    break;
                }
            }

            DoMeleeAttackIfReady();
        }
     };

    CreatureAI* GetAI(Creature* pCreature) const
    {
        return new boss_high_priestess_azilAI(pCreature);
    }
};

void AddSC_boss_high_priestess_azil()
{
    new boss_high_priestess_azil();
}
