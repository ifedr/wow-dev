/*
* Copyright (C) 2010-2011 Project SkyFire <http://www.projectskyfire.org/>
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "ScriptPCH.h"
#include "the_stonecore.h"
#include "Group.h"

enum Spells
{
    SPELL_BERSERK              = 47008,
    SPELL_CRYSTAL_STORM        = 92301,
    SPELL_SAND_BLAST           = 80807,
};

enum Events
{
    EVENT_BERSERK = 1,
    EVENT_SAND_BLAST,
};

class boss_slabhide: public CreatureScript
{
    public:
        boss_slabhide() : CreatureScript("boss_slabhide") { }

    struct boss_slabhideAI: public BossAI
    {
        boss_slabhideAI(Creature* pCreature) : BossAI(pCreature, DATA_SLABHIDE) 
	 {
	     me->ApplySpellImmune(0, IMMUNITY_EFFECT, SPELL_EFFECT_KNOCK_BACK, true);
	     me->ApplySpellImmune(0, IMMUNITY_MECHANIC, MECHANIC_GRIP, true);
	 }

        EventMap events;
        bool check_in;

        uint32 crystal_storm_casted;

        void Reset()
        {
            events.Reset();
            if (pInstance && (pInstance->GetData(DATA_SLABHIDE_EVENT) != DONE &&  !check_in))
                pInstance->SetData(DATA_SLABHIDE_EVENT, NOT_STARTED);

            me->RemoveAurasDueToSpell(SPELL_BERSERK);
            events.ScheduleEvent(EVENT_BERSERK, 300 *IN_MILLISECONDS);
            events.ScheduleEvent(EVENT_SAND_BLAST, 8 *IN_MILLISECONDS);
            crystal_storm_casted = 0;
			check_in = false;
        }

		void EnterCombat(Unit* /*Ent*/)
        {
			DoZoneInCombat();

            if (pInstance)
                pInstance->SetData(DATA_SLABHIDE_EVENT, IN_PROGRESS);
        }

        void JustDied(Unit* /*killer*/)
        {
            if (pInstance)
			{
                pInstance->SetData(DATA_SLABHIDE_EVENT, DONE);
				if (IsHeroic())
				{
					int32 income = 7500;
					pInstance->DoUpdateCurrency(CURRENCY_TYPE_JUSTICE_POINTS, income);
				}
            }
        }

        void UpdateAI(const uint32 diff)
        {
            if (!UpdateVictim())
                return;

	     if (me->GetHealthPct() < 66 && crystal_storm_casted == 0)
            {
                DoCast(SPELL_CRYSTAL_STORM);
                events.DelayEvents(3 *IN_MILLISECONDS);
                crystal_storm_casted = 1;
            }
            if (me->GetHealthPct() < 33 && crystal_storm_casted == 1)
            {
                DoCast(SPELL_CRYSTAL_STORM);
                events.DelayEvents(3 *IN_MILLISECONDS);
                crystal_storm_casted = 2;
            }
            
            events.Update(diff);

            if (me->HasUnitState(UNIT_STAT_CASTING))
                    return;

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                case EVENT_SAND_BLAST:
                    if (Unit *target = SelectTarget(SELECT_TARGET_RANDOM, 0))
                    DoCast(target, SPELL_SAND_BLAST);
                    events.RescheduleEvent(EVENT_SAND_BLAST, 15 *IN_MILLISECONDS);
                    break;
                case EVENT_BERSERK:
                    DoCast(me, SPELL_BERSERK);
                    break;
                }
            }

            DoMeleeAttackIfReady();
        }
     };

    CreatureAI* GetAI(Creature* pCreature) const
    {
        return new boss_slabhideAI(pCreature);
    }
};

void AddSC_boss_slabhide()
{
    new boss_slabhide();
}
