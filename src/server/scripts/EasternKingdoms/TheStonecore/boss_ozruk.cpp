/*
* Copyright (C) 2010-2011 Project SkyFire <http://www.projectskyfire.org/>
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "ScriptPCH.h"
#include "the_stonecore.h"
#include "Group.h"

enum Spells
{
    SPELL_BERSERK                           = 47008,
    SPELL_ELEMENTIUM_BULWARK                = 78939,
    SPELL_ELEMENTIUM_SPIKE_SHIELD           = 78859,
    SPELL_ELEMENTIUM_SPIKE_SHIELD1          = 78835,
    SPELL_ENRAGE                            = 80467,
    SPELL_FROST_FEVER			  = 55095,
    SPELL_GROUND_SLAM			  = 78903,
    SPELL_PARALYZE				  = 92426,
    SPELL_SHATTER				  = 78807,
    SPELL_SLOW				  = 31589,
};

enum Events
{
    EVENT_BERSERK = 1,
    EVENT_GROUND_SLAM,
    EVENT_FROST_FEVER,
    EVENT_PARALYZE,
    EVENT_SHATTER,
    EVENT_SLOW,
    EVENT_ELEMENTIUM_BULWARK,
    EVENT_ELEMENTIUM_SPIKE_SHIELD,
    EVENT_ELEMENTIUM_SPIKE_SHIELD1,
};

class boss_ozruk: public CreatureScript
{
    public:
        boss_ozruk() : CreatureScript("boss_ozruk") { }

    struct boss_ozrukAI: public BossAI
    {
        boss_ozrukAI(Creature* pCreature) : BossAI(pCreature, DATA_OZRUK) 
	 {
	     me->ApplySpellImmune(0, IMMUNITY_EFFECT, SPELL_EFFECT_KNOCK_BACK, true);
	     me->ApplySpellImmune(0, IMMUNITY_MECHANIC, MECHANIC_GRIP, true);
	 }

        EventMap events;
        bool check_in;
		
		uint32 enrage_casted;

        void Reset()
        {
            events.Reset();
            if (pInstance && (pInstance->GetData(DATA_OZRUK_EVENT) != DONE &&  !check_in))
                pInstance->SetData(DATA_OZRUK_EVENT, NOT_STARTED);

            me->RemoveAurasDueToSpell(SPELL_BERSERK);
            events.ScheduleEvent(EVENT_BERSERK, 300 *IN_MILLISECONDS);
            events.ScheduleEvent(EVENT_GROUND_SLAM, 20 *IN_MILLISECONDS);
	     events.ScheduleEvent(EVENT_PARALYZE, 30 *IN_MILLISECONDS);
	     events.ScheduleEvent(EVENT_SLOW, 15 *IN_MILLISECONDS);
	     events.ScheduleEvent(EVENT_SHATTER, 20 *IN_MILLISECONDS);
	     events.ScheduleEvent(EVENT_ELEMENTIUM_BULWARK, 5 *IN_MILLISECONDS);
	     events.ScheduleEvent(EVENT_ELEMENTIUM_SPIKE_SHIELD, 15 *IN_MILLISECONDS);
	     events.ScheduleEvent(EVENT_ELEMENTIUM_SPIKE_SHIELD1, 23 *IN_MILLISECONDS);
		 check_in = false;
            enrage_casted = 0;
        }

		void EnterCombat(Unit* /*Ent*/)
        {
			DoZoneInCombat();

            if (pInstance)
                pInstance->SetData(DATA_OZRUK_EVENT, IN_PROGRESS);
        }

        void JustDied(Unit* /*killer*/)
        {
            if (pInstance)
			{
                pInstance->SetData(DATA_OZRUK_EVENT, DONE);
				if (IsHeroic())
				{
					int32 income = 7500;
					pInstance->DoUpdateCurrency(CURRENCY_TYPE_JUSTICE_POINTS, income);
				}
            }
        }

        void UpdateAI(const uint32 diff)
        {
            if (!UpdateVictim())
                return;

	     if (me->GetHealthPct() < 20 && enrage_casted == 0)
            {
                DoCast(SPELL_ENRAGE);
                enrage_casted = 1;
            }
            
            events.Update(diff);

            if (me->HasUnitState(UNIT_STAT_CASTING))
                    return;

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                case EVENT_ELEMENTIUM_BULWARK:
                    DoCast(SPELL_ELEMENTIUM_BULWARK);
                    events.RescheduleEvent(EVENT_ELEMENTIUM_BULWARK, 20 *IN_MILLISECONDS);
                    break;
		  case EVENT_ELEMENTIUM_SPIKE_SHIELD:
		      DoCast(SPELL_ELEMENTIUM_SPIKE_SHIELD);
                    events.RescheduleEvent(EVENT_ELEMENTIUM_SPIKE_SHIELD, 30 *IN_MILLISECONDS);
                    break;
		  case EVENT_ELEMENTIUM_SPIKE_SHIELD1:
		      DoCast(SPELL_ELEMENTIUM_SPIKE_SHIELD1);
                    events.RescheduleEvent(EVENT_ELEMENTIUM_SPIKE_SHIELD1, 40 *IN_MILLISECONDS);
                    break;
		  case EVENT_SLOW:
		      DoCast(SPELL_SLOW);
                    events.RescheduleEvent(EVENT_SLOW, 25 *IN_MILLISECONDS);
                    break;
		  case EVENT_PARALYZE:
		      DoCast(SPELL_PARALYZE);
                    events.RescheduleEvent(EVENT_PARALYZE, 46 *IN_MILLISECONDS);
                    break;
		  case EVENT_GROUND_SLAM:
		      DoCast(SPELL_GROUND_SLAM);
                    events.RescheduleEvent(EVENT_GROUND_SLAM, 25 *IN_MILLISECONDS);
                    break;
		  case EVENT_SHATTER:
		      DoCast(SPELL_SHATTER);
                    events.RescheduleEvent(EVENT_SHATTER, 60 *IN_MILLISECONDS);
                    break;
		  case EVENT_FROST_FEVER:
		      DoCast(SPELL_FROST_FEVER);
                    events.RescheduleEvent(EVENT_FROST_FEVER, 20 *IN_MILLISECONDS);
                    break;
                case EVENT_BERSERK:
                    DoCast(me, SPELL_BERSERK);
                    break;
                }
            }

            DoMeleeAttackIfReady();
        }
     };

    CreatureAI* GetAI(Creature* pCreature) const
    {
        return new boss_ozrukAI(pCreature);
    }
};

void AddSC_boss_ozruk()
{
    new boss_ozruk();
}
