#ifndef DEF_BASTION_OF_TWILIGHT_H
#define DEF_BASTION_OF_TWILIGHT_H

#define ICCSCRIPTNAME "instance_bastion_of_twilight"

enum SharedSpells
{
    SPELL_BERSERK = 26662,
};

enum Data
{
    DATA_HALFUS_WYRMBREAKER_EVENT,
};

enum Data64
{
    DATA_HALFUS_WYRMBREAKER = 0,
    DATA_STORM_RIDER = 1,
    DATA_TIME_WARDEN = 2,
    DATA_SLATE_DRAGON = 3,
    DATA_NETHER_SCION = 4,
    DATA_EMERALD_WHELP = 5,
    DATA_PROTO_BEHEMOTH = 6,
    DATA_SINESTRA = 7,
};

#define MAX_ENCOUNTER 5

enum Creatures
{
    //Halfus Wyrmbreaker
    NPC_HALFUS_WYRMBREAKER = 44600,
    NPC_PROTO_BEHEMOTH = 44687,
    NPC_STORM_RIDER = 44650,
    NPC_TIME_WARDEN = 44797,
    NPC_SLATE_DRAGON = 44652,
    NPC_NETHER_SCION = 44645,
    NPC_EMERALD_WHELP = 44641,
};

const Position SinestraMinionPositions [] =
{
    {-984.649963f, -777.938354f, 438.592987f, 0.876615f},  // 0 : Calen
    {-996.034119f, -674.310852f, 440.059662f, 4.635530f},  // 1 : Pulsing Twilight Egg - Left
    {-903.791565f, -769.350952f, 440.954987f, 3.305641f},  // 2 : Pulsing Twilight Egg - Right
};

#define POSITION_CALEN                      SinestraMinionPositions[0]
#define POSITION_PULSING_TWILIGHT_EGG_LEFT  SinestraMinionPositions[1]
#define POSITION_PULSING_TWILIGHT_EGG_RIGHT SinestraMinionPositions[2]

#endif
