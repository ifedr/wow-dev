/*
* Copyright (C) 2010-2011 Project SkyFire <http://www.projectskyfire.org/>
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include"ScriptPCH.h"
#include"WorldPacket.h"
#include"baradin_hold.h"
#include"ScriptMgr.h"
#include"ScriptedCreature.h"
#include"SpellScript.h"
#include"SpellAuraEffects.h"

enum Spells
{
    SPELL_BERSERK              = 47008,
    SPELL_CONSUMING_DARKNESS   = 88954,
    H_SPELL_CONSUMING_DARKNESS = 95173,
    SPELL_FEL_FIRESTORM        = 88972,
    SPELL_METEOR_SLASH         = 88942,
    H_SPELL_METEOR_SLASH       = 95172,
};

enum Events
{
    EVENT_BERSERK,
    EVENT_CONSUMING_DARKNESS,
    EVENT_FEL_FIRESTORM,
    EVENT_METEOR_SLASH,
};

class boss_argaloth: public CreatureScript
{
public:
    boss_argaloth() : CreatureScript("boss_argaloth") { }

    CreatureAI* GetAI(Creature* pCreature) const
    {
       return new boss_argalothAI(pCreature);
    }
    struct boss_argalothAI: public ScriptedAI
    {
        boss_argalothAI(Creature* pCreature) : ScriptedAI(pCreature)
        {
            pInstance = pCreature->GetInstanceScript();
        }

        InstanceScript *pInstance;
        EventMap events;
        bool fs_flag_1;
        bool fs_flag_2;
        bool check_in;
        float curr_health;

        void Reset()
        {
            events.Reset();
            if (pInstance && (pInstance->GetData(DATA_ARGALOTH_EVENT) != DONE &&  !check_in))
                pInstance->SetData(DATA_ARGALOTH_EVENT, NOT_STARTED);

            check_in = false;
            fs_flag_1 = false;
            fs_flag_2 = false;
        }

        void EnterCombat(Unit* /*Ent*/)
        {
            DoZoneInCombat();
            events.RescheduleEvent(EVENT_CONSUMING_DARKNESS, urand(12000, 14000));
            events.RescheduleEvent(EVENT_METEOR_SLASH, 21000);
            events.RescheduleEvent(EVENT_BERSERK, 300000);
            if (pInstance)
                pInstance->SetData(DATA_ARGALOTH_EVENT, IN_PROGRESS);
        }

        void JustDied(Unit* /*Kill*/)
        {
            if (pInstance){
                pInstance->SetData(DATA_ARGALOTH_EVENT, DONE);
                int32 income = Is25ManRaid() ? 10500 : 7500;
                pInstance->DoUpdateCurrency(CURRENCY_TYPE_VALOR_POINTS, income);
            }
        }

        void UpdateAI(const uint32 uiDiff)
        {
            if (!UpdateVictim())
                return;

            curr_health = me->GetHealthPct();
            if (curr_health < 66.0 && (fs_flag_1 == false))
            {
                DoCast(me->getVictim(), SPELL_FEL_FIRESTORM);
                fs_flag_1 = true;
            } else if (curr_health < 33.0 && (fs_flag_2 == false))
            {
                DoCast(me->getVictim(), SPELL_FEL_FIRESTORM);
                fs_flag_2 = true;
            }
            events.Update(uiDiff);

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch(eventId)
                {
                    case EVENT_CONSUMING_DARKNESS:
                        if (Unit *pTarget = SelectUnit(SELECT_TARGET_RANDOM, 0))
                            DoCast(pTarget, RAID_MODE(SPELL_CONSUMING_DARKNESS, H_SPELL_CONSUMING_DARKNESS));
                        events.ScheduleEvent(EVENT_CONSUMING_DARKNESS, urand(12000, 14000));
                        break;
                    case EVENT_METEOR_SLASH:
                        DoCast(me->getVictim(), RAID_MODE(SPELL_METEOR_SLASH, H_SPELL_METEOR_SLASH));
                        events.ScheduleEvent(EVENT_METEOR_SLASH, 21000);
                        break;
                    case EVENT_BERSERK:
                        DoCast(me, SPELL_BERSERK);
                        break;
                }
            }
            DoMeleeAttackIfReady();
            
        }
     };
};

void AddSC_boss_argaloth()
{
    new boss_argaloth();
}
