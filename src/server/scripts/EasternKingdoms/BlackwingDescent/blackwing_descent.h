/*
* Copyright (C) 2010-2011 Project SkyFire <http://www.projectskyfire.org/>
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_BLACKWING_DESCENT_H
#define DEF_BLACKWING_DESCENT_H

enum CreatureIds
{
    // Implemented
    BOSS_MAGMAW               = 41570,
    BOSS_MALORIAK             = 41378,
    BOSS_ATRAMEDES            = 41442,
    BOSS_CHIMAERON            = 43296,
    // Not implemented
    BOSS_NEFARIAN             = 41376,
    BOSS_ARCANOTRON           = 42166,
    BOSS_ELECTRON             = 42179,
    BOSS_MAGMATRON            = 42178,
    BOSS_TOXITRON             = 42180,

    // Misc
    NPC_BILE_O_TRON_800      = 44418,
    NPC_ONYXIA               = 41270,
    NPC_LORD_VICTOR_NEFARIAN = 41379,
};

enum Data
{
    MAGMAW,
    MALORIAK,
    ATRAMEDES,
    CHIMAERON,
};

#endif
