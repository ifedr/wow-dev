#include"ScriptPCH.h"
#include"blackwing_descent.h"

enum Spells 
{
    SPELL_BERSERK = 26662,
    SPELL_MS = 78068,
    SPELL_LS = 77689,
    SPELL_PoF = 77970,
    SPELL_MC = 88287,
    SPELL_MT = 78403,
};

static const Position aSpawnLocations[1] =
{
    {-325.247, -34.859, 211.344f, 0.0f},
};

enum subCreatures
{
    BLAZING_BONE_CONSTRUCT = 49416,
};

enum Events
{
    EVENT_BERSERK,
    EVENT_MS,
    EVENT_LS,
    EVENT_PoF,
    EVENT_MC,
    EVENT_MT,
    EVENT_SUMMON,
};

class boss_magmaw: public CreatureScript
{
public:
    boss_magmaw() : CreatureScript("boss_magmaw") { }

    struct boss_magmawAI: public BossAI
    {
        boss_magmawAI(Creature* pCreature) : BossAI(pCreature, MAGMAW){ }


        void Reset()
        {
            _Reset();
            me->RemoveAurasDueToSpell(SPELL_BERSERK);
        }

        void EnterCombat(Unit* /*Ent*/)
        {
            _EnterCombat();
            events.ScheduleEvent(EVENT_BERSERK, 600 * IN_MILLISECONDS);
            events.ScheduleEvent(EVENT_MS, 10 * IN_MILLISECONDS);
            events.ScheduleEvent(SPELL_LS, 20 * IN_MILLISECONDS);
            events.ScheduleEvent(EVENT_PoF, 30 * IN_MILLISECONDS);
            events.ScheduleEvent(EVENT_MC, 30 * IN_MILLISECONDS);
            events.ScheduleEvent(EVENT_MT, 1 * IN_MILLISECONDS);
            if (IsHeroic())
            {
                events.ScheduleEvent(EVENT_SUMMON, 120 * IN_MILLISECONDS);
            }
        }

        void JustDied(Unit* /*Kill*/)
        {
            _JustDied();
            if (instance)
            {
                int32 income = Is25ManRaid() ? 105 : 75;
                instance->DoUpdateCurrency(CURRENCY_TYPE_VALOR_POINTS, income * PLAYER_CURRENCY_PRECISION);
            }
        }
        

        void UpdateAI(const uint32 uiDiff) 
        {
            if (!UpdateVictim())
                return;

            events.Update(uiDiff);

            if (me->HasUnitState(UNIT_STAT_CASTING))
                return;

            while (uint32 eventId = events.ExecuteEvent()) 
            {
                switch (eventId) 
                {
                    case EVENT_BERSERK:
                        DoCast(me, SPELL_BERSERK);
                        break;
                    case EVENT_MS:
                        DoCast(SPELL_MS);
                        events.ScheduleEvent(EVENT_MS, 10 * IN_MILLISECONDS);
                        break;
                    case EVENT_LS:
                        DoCast(SPELL_LS);
                        events.ScheduleEvent(EVENT_LS, 35 * IN_MILLISECONDS);
                        break;
                    case EVENT_PoF:
                        DoCast(SPELL_PoF);
                        events.ScheduleEvent(EVENT_PoF, 60 * IN_MILLISECONDS);
                        break;
                    case EVENT_MC:
                        DoCast(SPELL_MC);
                        events.ScheduleEvent(EVENT_MC, 60 * IN_MILLISECONDS);
                        break;
                    case EVENT_SUMMON:
                        me->SummonCreature(BLAZING_BONE_CONSTRUCT, aSpawnLocations[1], TEMPSUMMON_CORPSE_DESPAWN);
                        events.ScheduleEvent(EVENT_SUMMON, 60 * IN_MILLISECONDS);
                        break;
                    case EVENT_MT:
                        if(!me->IsWithinMeleeRange(me->getVictim()))
                        {
                            DoCast(me, SPELL_MT);
                        }
                        events.ScheduleEvent(EVENT_MT, 1 * IN_MILLISECONDS);
                        break;
                }
            }
            DoMeleeAttackIfReady();
        }
    };

    CreatureAI* GetAI(Creature* pCreature) const
    {
       return new boss_magmawAI(pCreature);
    }
};

void AddSC_boss_magmaw()
{
    new boss_magmaw();
}
