#include"ScriptPCH.h"
#include"blackwing_descent.h"

enum Spells 
{
    SPELL_BERSERK = 64238,
    SPELL_M = 77612,
    SPELL_SF = 77840,
    SPELL_RFB = 78353,
};

enum Events
{
    EVENT_BERSERK,
    EVENT_M,
    EVENT_SF,
    EVENT_RFB,
};

class boss_atramedes: public CreatureScript
{
public:
    boss_atramedes() : CreatureScript("boss_atramedes") { }

    struct boss_atramedesAI: public BossAI
    {
        boss_atramedesAI(Creature* pCreature) : BossAI(pCreature, ATRAMEDES){ }

        void Reset()
        {
            _Reset();
            me->RemoveAurasDueToSpell(SPELL_BERSERK);
        }

        void EnterCombat(Unit* /*Ent*/)
        {
            // TODO: remove test code. This check must be on upper level.
            if(instance && (instance->GetBossState(ATRAMEDES) == DONE))
            {
                EnterEvadeMode();
            }
            else
            {
                _EnterCombat();
                events.ScheduleEvent(EVENT_BERSERK, 600 * IN_MILLISECONDS);
                events.ScheduleEvent(EVENT_M, 5 * IN_MILLISECONDS);
                events.ScheduleEvent(EVENT_SF, 30 * IN_MILLISECONDS);
                events.ScheduleEvent(EVENT_RFB, 60 * IN_MILLISECONDS);
            }
        }

        void JustDied(Unit* /*Kill*/)
        {
            _JustDied();
            if (instance)
            {
                int32 income = Is25ManRaid() ? 105 : 75;
                instance->DoUpdateCurrency(CURRENCY_TYPE_VALOR_POINTS, income * PLAYER_CURRENCY_PRECISION);
            }
        }
        

        void UpdateAI(const uint32 uiDiff) 
        {
            if (!UpdateVictim())
                return;

            events.Update(uiDiff);

            if (me->HasUnitState(UNIT_STAT_CASTING))
                return;

            while (uint32 eventId = events.ExecuteEvent()) 
            {
                switch (eventId) 
                {
                    case EVENT_BERSERK:
                        DoCast(me, SPELL_BERSERK);
                        break;
                    case EVENT_M:
                        DoCast(SPELL_M);
                        events.ScheduleEvent(EVENT_M, 5 * IN_MILLISECONDS);
                        break;
                    case EVENT_SF:
                        DoCast(SPELL_SF);
                        events.ScheduleEvent(EVENT_SF, 35 * IN_MILLISECONDS);
                        break;
                    case EVENT_RFB:
                        DoCast(SPELL_RFB);
                        events.ScheduleEvent(EVENT_RFB, 60 * IN_MILLISECONDS);
                        break;

                }
            }
            DoMeleeAttackIfReady();
        }
    };

    CreatureAI* GetAI(Creature* pCreature) const
    {
       return new boss_atramedesAI(pCreature);
    }
};

void AddSC_boss_atramedes()
{
    new boss_atramedes();
}
