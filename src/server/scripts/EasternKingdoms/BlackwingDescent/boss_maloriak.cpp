#include"ScriptPCH.h"
#include"blackwing_descent.h"

enum Spells 
{
    SPELL_BERSERK = 64238,
    SPELL_PHASE0_ARCAN = 77896,
    SPELL_PHASE0_REMEDY = 92967,
	//80%
    SPELL_PHASE1_CF = 77786,
    SPELL_PHASE1_SB = 77679,
	//50%
    SPELL_PHASE2_FF = 77699,
    SPELL_PHASE2_BC = 77760,
	//25%
    SPELL_PHASE3_MJ = 78095,
    SPELL_PAHSE3_AZ = 78208,
    SPELL_PHASE3_AN = 78225,

};

static const Position aSpawnLocations[1] =
{
    {-106.157997, -480.57, 73.455f, 0.0f},
};

enum subCreatures
{
    VILE_SWILL = 49811,
};

enum Events
{
    EVENT_BERSERK,
    EVENT_ARCAN,
    EVENT_REMEDY,

    EVENT_PHASE1_CF,
    EVENT_PHASE1_SB,

    EVENT_PHASE2_FF,
    EVENT_PHASE2_BC,

    EVENT_PHASE3_MJ,
    EVENT_PAHSE3_AZ,
    EVENT_PHASE3_AN,

    EVENT_SUMMON,
};

class boss_maloriak: public CreatureScript
{
public:
    boss_maloriak() : CreatureScript("boss_maloriak") { }

    struct boss_maloriakAI: public BossAI
    {
        boss_maloriakAI(Creature* pCreature) : BossAI(pCreature, MALORIAK){ }

        uint32 phase;

        void Reset()
        {
            _Reset();
            me->RemoveAurasDueToSpell(SPELL_BERSERK);
            phase = 0;
        }

        void EnterCombat(Unit* /*Ent*/)
        {
            _EnterCombat();
            events.ScheduleEvent(EVENT_BERSERK, 360 * IN_MILLISECONDS);
            events.ScheduleEvent(EVENT_ARCAN, 20 * IN_MILLISECONDS);
            events.ScheduleEvent(EVENT_REMEDY, 10 * IN_MILLISECONDS);
            if (IsHeroic())
            {
                events.ScheduleEvent(EVENT_SUMMON, 60 * IN_MILLISECONDS);
            }
        }


        void JustDied(Unit* /*Kill*/)
        {
            _JustDied();
            if (instance)
            {
                int32 income = Is25ManRaid() ? 105 : 75;
                instance->DoUpdateCurrency(CURRENCY_TYPE_VALOR_POINTS, income * PLAYER_CURRENCY_PRECISION);
            }
        }
        

        void UpdateAI(const uint32 uiDiff) 
        {
            if (!UpdateVictim())
                return;

            if (phase == 0 && me->HealthBelowPct(80)) 
            {
                events.ScheduleEvent(EVENT_PHASE1_CF, 5 * IN_MILLISECONDS);
                events.ScheduleEvent(EVENT_PHASE1_SB, 10 * IN_MILLISECONDS);
                phase++;
            } 
            else if (phase == 1 && me->HealthBelowPct(50)) 
            {
                events.CancelEvent(EVENT_PHASE1_CF);
                events.CancelEvent(EVENT_PHASE1_SB);
                events.ScheduleEvent(EVENT_PHASE2_FF, 10 * IN_MILLISECONDS);
                events.ScheduleEvent(EVENT_PHASE2_BC, 5 * IN_MILLISECONDS);
                phase++;
            }
            else if (phase == 2 && me->HealthBelowPct(25)) 
            {
                events.CancelEvent(EVENT_PHASE2_FF);
                events.CancelEvent(EVENT_PHASE2_BC);
                events.ScheduleEvent(EVENT_PHASE3_MJ, 5 * IN_MILLISECONDS);
                events.ScheduleEvent(EVENT_PAHSE3_AZ, 10 * IN_MILLISECONDS);
                events.ScheduleEvent(EVENT_PHASE3_AN, 15 * IN_MILLISECONDS);
                phase++;
            }

            events.Update(uiDiff);

            if (me->HasUnitState(UNIT_STAT_CASTING))
                return;

            while (uint32 eventId = events.ExecuteEvent()) 
            {
                switch (eventId) 
                {
                    case EVENT_ARCAN:
                        DoCast(SPELL_PHASE0_ARCAN);
                        events.ScheduleEvent(EVENT_ARCAN, 60 * IN_MILLISECONDS);
                        break;
                    case EVENT_BERSERK:
                        DoCast(me, SPELL_BERSERK);
                        break;
                    case EVENT_REMEDY:
                        DoCast(me, SPELL_PHASE0_REMEDY);
                        events.ScheduleEvent(EVENT_REMEDY, 30 * IN_MILLISECONDS);
                        break;
                    case EVENT_PHASE1_CF:
                        DoCast(SPELL_PHASE1_CF);
                        events.ScheduleEvent(EVENT_PHASE1_CF, 12 * IN_MILLISECONDS);
                        break;
                    case EVENT_PHASE1_SB:
                        DoCast(SPELL_PHASE1_SB);
                        events.ScheduleEvent(EVENT_PHASE1_SB, 40 * IN_MILLISECONDS);
                        break;
                    case EVENT_PHASE2_BC:
                        DoCast(SPELL_PHASE2_BC);
                        events.ScheduleEvent(EVENT_PHASE2_BC, 12 * IN_MILLISECONDS);
                        break;
                    case EVENT_PHASE2_FF:
                        DoCast(SPELL_PHASE2_FF);
                        events.ScheduleEvent(EVENT_PHASE2_FF, 40 * IN_MILLISECONDS);
                        break;
                    case EVENT_PHASE3_MJ:
                        DoCast(SPELL_PHASE3_MJ);
                        events.ScheduleEvent(EVENT_PHASE3_MJ, 40 * IN_MILLISECONDS);
                        break;
                    case EVENT_PAHSE3_AZ:
                        DoCast(SPELL_PAHSE3_AZ);
                        events.ScheduleEvent(EVENT_PAHSE3_AZ, 45 * IN_MILLISECONDS);
                        break;
                    case EVENT_PHASE3_AN:
                        DoCast(SPELL_PHASE3_AN);
                        events.ScheduleEvent(EVENT_PHASE3_AN, 50 * IN_MILLISECONDS);
                        break;
                    case EVENT_SUMMON:
                        me->SummonCreature(VILE_SWILL, aSpawnLocations[1], TEMPSUMMON_CORPSE_DESPAWN);
                        me->SummonCreature(VILE_SWILL, aSpawnLocations[1], TEMPSUMMON_CORPSE_DESPAWN);
                        me->SummonCreature(VILE_SWILL, aSpawnLocations[1], TEMPSUMMON_CORPSE_DESPAWN);
                        me->SummonCreature(VILE_SWILL, aSpawnLocations[1], TEMPSUMMON_CORPSE_DESPAWN);
                        me->SummonCreature(VILE_SWILL, aSpawnLocations[1], TEMPSUMMON_CORPSE_DESPAWN);
                        events.ScheduleEvent(EVENT_SUMMON, 60 * IN_MILLISECONDS);
                        break;
                }
            }
            DoMeleeAttackIfReady();
        }
    };

    CreatureAI* GetAI(Creature* pCreature) const
    {
       return new boss_maloriakAI(pCreature);
    }
};

void AddSC_boss_maloriak()
{
    new boss_maloriak();
}
