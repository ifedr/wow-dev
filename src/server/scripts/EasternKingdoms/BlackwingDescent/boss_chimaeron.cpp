#include"ScriptPCH.h"
#include"blackwing_descent.h"

enum Spells 
{
    SPELL_BERSERK = 26662,
    SPELL_PHASE0_BREAK = 82881,
    SPELL_PHASE0_CS = 82935,
    SPELL_PHASE01_DA = 82882, // Self
	//20%
    SPELL_PHASE1_MORTALITY = 82934, //Self
    SPELL_PAHSE1_MS = 91307,
};

enum Events
{
    EVENT_BERSERK,
    EVENT_PHASE0_BREAK,
    EVENT_PHASE0_CS,
    EVENT_PHASE01_DA,
};

class boss_chimaeron: public CreatureScript
{
public:
    boss_chimaeron() : CreatureScript("boss_chimaeron") { }

    struct boss_chimaeronAI: public BossAI
    {
        boss_chimaeronAI(Creature* pCreature) : BossAI(pCreature, CHIMAERON){ }

        uint32 phase;

        void Reset()
        {
            _Reset();
            me->RemoveAurasDueToSpell(SPELL_BERSERK);
            phase = 0;
        }

        void EnterCombat(Unit* /*Ent*/)
        {
            _EnterCombat();
            events.ScheduleEvent(EVENT_BERSERK, 480 * IN_MILLISECONDS);
            events.ScheduleEvent(EVENT_PHASE0_BREAK, 20 * IN_MILLISECONDS);
            events.ScheduleEvent(EVENT_PHASE0_CS, 30 * IN_MILLISECONDS);
            events.ScheduleEvent(EVENT_PHASE01_DA, 30 * IN_MILLISECONDS);
        }

        void JustDied(Unit* /*Kill*/)
        {
            _JustDied();
            if (instance)
            {
                int32 income = Is25ManRaid() ? 105 : 75;
                instance->DoUpdateCurrency(CURRENCY_TYPE_VALOR_POINTS, income * PLAYER_CURRENCY_PRECISION);
            }
        }
        

        void UpdateAI(const uint32 uiDiff) 
        {
            if (!UpdateVictim())
                return;

            if (phase == 0 && me->HealthBelowPct(20)) 
            {
                events.CancelEvent(EVENT_PHASE01_DA);
                DoCast(me, SPELL_PHASE1_MORTALITY);
                DoCast(me, SPELL_PAHSE1_MS);
                events.ScheduleEvent(EVENT_PHASE01_DA, 5 * IN_MILLISECONDS);
                phase++;
            } 


            events.Update(uiDiff);

            if (me->HasUnitState(UNIT_STAT_CASTING))
                return;

            while (uint32 eventId = events.ExecuteEvent()) 
            {
                switch (eventId) 
                {
                    case EVENT_BERSERK:
                        DoCast(me, SPELL_BERSERK);
                        break;
                    case EVENT_PHASE01_DA:
                        DoCast(me, SPELL_PHASE01_DA);
			            events.ScheduleEvent(EVENT_PHASE01_DA, 5 * IN_MILLISECONDS);
                        break;
                    case EVENT_PHASE0_BREAK:
                        DoCast(me->getVictim(), SPELL_PHASE0_BREAK);
                        events.ScheduleEvent(EVENT_PHASE0_BREAK, 15 * IN_MILLISECONDS);
                        break;
                    case EVENT_PHASE0_CS:
                        DoCast(SPELL_PHASE0_CS);
                        events.ScheduleEvent(EVENT_PHASE0_CS, 60 * IN_MILLISECONDS);
                        break;
                }
            }
            DoMeleeAttackIfReady();
        }
    };

    CreatureAI* GetAI(Creature* pCreature) const
    {
       return new boss_chimaeronAI(pCreature);
    }
};

void AddSC_boss_chimaeron()
{
    new boss_chimaeron();
}
