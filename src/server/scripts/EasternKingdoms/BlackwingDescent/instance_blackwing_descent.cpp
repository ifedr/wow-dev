/*
* Copyright (C) 2010-2011 Project SkyFire <http://www.projectskyfire.org/>
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include"ScriptPCH.h"
#include"blackwing_descent.h"

#define ENCOUNTERS 4

class instance_blackwing_descent: public InstanceMapScript
{
public:
    instance_blackwing_descent() : InstanceMapScript("instance_blackwing_descent", 757) { }
    
    InstanceScript* GetInstanceScript(InstanceMap *map) const
    {
        return new instance_blackwing_descent_InstanceMapScript(map);
    }
    
    struct instance_blackwing_descent_InstanceMapScript: public InstanceScript
    {
        instance_blackwing_descent_InstanceMapScript(InstanceMap *map) : InstanceScript(map) { }

        uint32 uiEncounter[ENCOUNTERS];

        uint64 magmawGUID;
        uint64 maloriakGUID;
        uint64 atramedesGUID;
        uint64 chimaeronGUID;
 
        void Initialize()
        {
            SetBossNumber(ENCOUNTERS);
            magmawGUID = 0;
            maloriakGUID = 0;
            atramedesGUID = 0;
            chimaeronGUID = 0;
            for(uint8 i=0; i < ENCOUNTERS; ++i)
                uiEncounter[i] = NOT_STARTED;
            
        }

        bool IsEncounterInProgress() const
        {
            for(uint8 i=0; i < ENCOUNTERS; ++i)
            {
                if (uiEncounter[i] == IN_PROGRESS)
                    return true;
            }
            return false;
        }

        void OnCreatureCreate(Creature* pCreature, bool)
        {
            switch(pCreature->GetEntry())
            {
                case BOSS_MAGMAW:
                    magmawGUID = pCreature->GetGUID();
                    break;
                case BOSS_MALORIAK:
                    maloriakGUID = pCreature->GetGUID();
                    break;
                case BOSS_ATRAMEDES:
                    atramedesGUID = pCreature->GetGUID();
                    break;
                case BOSS_CHIMAERON:
                    chimaeronGUID = pCreature->GetGUID();
                    break;
            }
        }

        uint64 getData64(uint32 identifier)
        {
            switch(identifier)
            {
                case MAGMAW:
                    return magmawGUID;
                case MALORIAK:
                    return maloriakGUID;
                case ATRAMEDES:
                    return atramedesGUID;
                case CHIMAERON:
                    return chimaeronGUID;
            }
            return 0;
        }

        void SetData(uint32 type, uint32 data)
        {
            switch(type)
            {
                case MAGMAW:
                    uiEncounter[0] = data;
                    break;
                case MALORIAK:
                    uiEncounter[1] = data;
                    break;
                case ATRAMEDES:
                    uiEncounter[2] = data;
                    break;
                case CHIMAERON:
                    uiEncounter[3] = data;
                    break;
            }

            if (data == DONE)
            {
                SaveToDB();
            }
        }

        std::string GetSaveData()
        {
            OUT_SAVE_INST_DATA;

            std::string str_data;
            std::ostringstream saveStream;
            saveStream << "B D" << uiEncounter[0] << " " << uiEncounter[1] << " " << uiEncounter[2] << " " << uiEncounter[3];
            str_data = saveStream.str();

            OUT_SAVE_INST_DATA_COMPLETE;
            return str_data;
        }

        void Load(const char* in)
        {
            if (!in)
            {
                OUT_LOAD_INST_DATA_FAIL;
                return;
            }

            OUT_LOAD_INST_DATA(in);

            char dataHead1, dataHead2;
            uint16 data0, data1, data2, data3;

            std::istringstream loadStream(in);
            loadStream >> dataHead1 >> dataHead2 >> data0 >> data1 >> data2 >> data3;

            if (dataHead1 == 'B' && dataHead2 == 'D')
            {
                uiEncounter[0] = data0;
                uiEncounter[1] = data1;
                uiEncounter[2] = data2;
                uiEncounter[3] = data3;
                       
                for(uint8 i=0; i < ENCOUNTERS; ++i)
                    if (uiEncounter[i] == IN_PROGRESS)
                        uiEncounter[i] = NOT_STARTED;
            }
            else OUT_LOAD_INST_DATA_FAIL;

            OUT_LOAD_INST_DATA_COMPLETE;
        }
    };
};

void AddSC_instance_blackwing_descent()
{
    new instance_blackwing_descent();
}
