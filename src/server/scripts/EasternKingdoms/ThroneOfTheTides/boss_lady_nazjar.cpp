
#include "ScriptPCH.h"
#include "throne_of_the_tides.h"

enum Yells
{
	SAY_RESET					= -1631140,
	SAY_ENTER_COMBAT			= -1631141,
	SAY_JUST_DIED				= -1631142,
	SAY_KILLED_UNIT				= -1631143,
	SAY_PHASE_START			= -1631144,
	SAY_CHANNELING			= -1631145,
	SAY_PHASE_INACTIVE				= -1631146,
	SAY_EVENT_SUMMON_GEYSER		= -1631147,
	SAY_EVENT_FUNGAL_SPORES		= -1631148,
	SAY_EVENT_SHOCK_BLAST		= -1631149,
	SAY_PHASE_MIDDLE					= -1631150,
	SAY_PHASE_END					= -1631151,
};

enum Spells
{
    SPELL_SUMMON_GEYSER			= 75722,
    SPELL_WATERSPROUT			= 75683,
    SPELL_FUNGAL_SPORES			= 76001,
    SPELL_SHOCK_BLAST_NORMAL	= 76008,
    SPELL_SHOCK_BLAST_HEROIC	= 91477,
};

enum Events
{
	EVENT_NONE = 0,
	EVENT_SUMMON_GEYSER,
	EVENT_WATERSPROUT,
	EVENT_FUNGAL_SPORES,
	EVENT_SHOCK_BLAST,
	EVENT_PHASE_1,
};

enum LadyNazjarPhases
{
    PHASE_START = 1,
    PHASE_INACTIVE = 2,
    PHASE_MIDDLE = 3,
    PHASE_END = 4,
};

enum subCreatures
{
	NPC_TEMPEST_WITCH = 44404,
	NPC_HONOR_GUARD = 40633,
};


static const Position MiddleRoomLocation = {191.0f, 802.0f, 807.0f, 0.0f};

static const Position aSpawnLocations[3] =
{
    // Tempest Witch
    {219.0f, 830.0f, 807.638f, 0.0f},
    {219.0f, 776.0f, 807.638f, 0.0f},
    // Honor Guard
    {155.0f, 800.0f, 807.638f, 0.0f}
};


class boss_lady_nazjar : public CreatureScript
{
public:
	boss_lady_nazjar() : CreatureScript("boss_lady_nazjar") { }

    CreatureAI* GetAI(Creature* pCreature) const
    {
        return new boss_lady_nazjarAI(pCreature);
    }

    struct boss_lady_nazjarAI : public ScriptedAI
    {
    	boss_lady_nazjarAI(Creature *c) : ScriptedAI(c), Summons(me)
        {
            pInstance = c->GetInstanceScript();
        }

        InstanceScript* pInstance;
        EventMap events;
        
                   SummonList Summons;

        uint32 m_uiPhase;

        void Reset()
        {
            DoScriptText(SAY_RESET, me);
            m_uiPhase = PHASE_START;
                            events.Reset();

                            Summons.DespawnAll();
                            DespawnCreatures(NPC_TEMPEST_WITCH, 150.0f);
                            DespawnCreatures(NPC_HONOR_GUARD, 150.0f);
 
            if (pInstance)
                pInstance->SetData(DATA_LADY_NAZJAR_EVENT, NOT_STARTED);
        }

        void KilledUnit(Unit* /*Victim*/)
        {
            DoScriptText(SAY_KILLED_UNIT, me);
        }

        void JustDied(Unit* /*Killer*/)
        {
            DoScriptText(SAY_JUST_DIED, me);
            Summons.DespawnAll();
            DespawnCreatures(NPC_TEMPEST_WITCH, 150.0f);
            DespawnCreatures(NPC_HONOR_GUARD, 150.0f);
                            if (pInstance)
                pInstance->SetData(DATA_LADY_NAZJAR_EVENT, DONE);
        }
        
                   void JustSummoned(Creature *pSummoned)
        {
            pSummoned->SetInCombatWithZone();
            if (Unit* pTarget = SelectTarget(SELECT_TARGET_RANDOM,0))
                pSummoned->AI()->AttackStart(pTarget);

            Summons.Summon(pSummoned);
        }

        void SummonedCreatureDespawn(Creature *summon)
        {
            Summons.Despawn(summon);
        }

        void installPhase1Timers()
        {
        	events.RescheduleEvent(EVENT_SUMMON_GEYSER, 15000);
			events.RescheduleEvent(EVENT_SHOCK_BLAST, 20000);
        }

        void enterPhase2()
        {
			me->GetMotionMaster()->MovePoint(0, MiddleRoomLocation);
			DoCast(me, SPELL_WATERSPROUT);
			me->SummonCreature(NPC_TEMPEST_WITCH, aSpawnLocations[0], TEMPSUMMON_CORPSE_DESPAWN);
			me->SummonCreature(NPC_TEMPEST_WITCH, aSpawnLocations[1], TEMPSUMMON_CORPSE_DESPAWN);
			me->SummonCreature(NPC_HONOR_GUARD, aSpawnLocations[2], TEMPSUMMON_CORPSE_DESPAWN);
			events.SetPhase(1);
			events.RescheduleEvent(EVENT_FUNGAL_SPORES, 10000, 0, 1);
			events.RescheduleEvent(EVENT_PHASE_1, 60000, 0, 1);
        }

        void EnterCombat(Unit * /*who*/)
        {
            DoScriptText(SAY_ENTER_COMBAT, me);
            DoZoneInCombat();
            installPhase1Timers();

            if (pInstance)
                pInstance->SetData(DATA_LADY_NAZJAR_EVENT, IN_PROGRESS);
        }

        void UpdateAI(const uint32 diff)
        {
            //Return since we have no target
            if (!UpdateVictim())
                return;

            events.Update(diff);

            if (me->HasUnitState(UNIT_STAT_CASTING))
            {
                 DoScriptText(SAY_CHANNELING, me);
                return;
            }

            // PHASE_INACTIVE invokes only here. PHASE_MIDDLE and PHASE_END will be invoke by Event timer below       
            if ((m_uiPhase == PHASE_START && HealthBelowPct(66)) || (m_uiPhase == PHASE_MIDDLE && HealthBelowPct(33)))
            {
                DoScriptText(SAY_PHASE_INACTIVE, me);
                m_uiPhase = PHASE_INACTIVE;
                enterPhase2();
            }

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch(eventId)
                {
                    case EVENT_PHASE_1:    		            
            if(HealthBelowPct(33))
            {
                 m_uiPhase = PHASE_END;
                 DoScriptText(SAY_PHASE_END, me);
            }
            else if(HealthBelowPct(66))
            {
                 m_uiPhase = PHASE_MIDDLE;
                 DoScriptText(SAY_PHASE_MIDDLE, me);
            }             

            			events.SetPhase(0);
                    	installPhase1Timers();
                        return;
                    case EVENT_SUMMON_GEYSER:
    		            DoScriptText(SAY_EVENT_SUMMON_GEYSER, me);
                        if (Unit *pTarget = SelectUnit(SELECT_TARGET_RANDOM, 0))
                            DoCast(pTarget, SPELL_SUMMON_GEYSER);
                        events.ScheduleEvent(EVENT_SUMMON_GEYSER, 15000);
                        return;
                    case EVENT_SHOCK_BLAST:
    		            DoScriptText(SAY_EVENT_SHOCK_BLAST, me);
                        DoCast(me->getVictim(), DUNGEON_MODE(SPELL_SHOCK_BLAST_NORMAL, SPELL_SHOCK_BLAST_HEROIC));
                        events.ScheduleEvent(EVENT_SHOCK_BLAST, 20000);
                        return;
                    case EVENT_FUNGAL_SPORES:
    		            DoScriptText(SAY_EVENT_FUNGAL_SPORES, me);
                        if (Unit *pTarget = SelectUnit(SELECT_TARGET_RANDOM, 0))
                            DoCast(pTarget, SPELL_FUNGAL_SPORES);
                        events.ScheduleEvent(EVENT_FUNGAL_SPORES, 10000, 0, 1);
                        return;
                }
            }

            DoMeleeAttackIfReady();
        }

        void DespawnCreatures(uint32 entry, float distance)
        {
            std::list<Creature*> m_pCreatures;
            GetCreatureListWithEntryInGrid(m_pCreatures, me, entry, distance);
     
            if (m_pCreatures.empty())
                return;
     
            for(std::list<Creature*>::iterator iter = m_pCreatures.begin(); iter != m_pCreatures.end(); ++iter)
                (*iter)->DespawnOrUnsummon();
        }
    };

};


void AddSC_boss_lady_nazjar()
{
    new boss_lady_nazjar();
}
