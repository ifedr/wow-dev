#include "ScriptPCH.h"
#include "throne_of_the_tides.h"

enum Yells
{
    SAY_RESET = -1631140,
    SAY_ENTER_COMBAT = -1631141,
    SAY_JUST_DIED = -1631142,
    SAY_KILLED_UNIT = -1631143,
    SAY_CHANNELING = -1631145,
};

enum Spells
{
    SPELL_DARK_FISSURE = 76047, //todo: need heroic?
    SPELL_CURSE_OF_FATIGUE = 76094,
    SPELL_ENRAGE = 76100,
    SPELL_SQUEEZE_NORMAL = 76026,
    SPELL_SQUEEZE_HEROIC = 91484,
};

enum Events
{
    EVENT_NONE = 0,
    EVENT_DARK_FISSURE,
    EVENT_CURSE_OF_FATIGUE,
    EVENT_ENRAGE,
    EVENT_SQUEEZE,
};

class boss_commander_ulthok: public CreatureScript
{
public:
    boss_commander_ulthok() :
        CreatureScript("boss_commander_ulthok")
    {
    }

    CreatureAI* GetAI(Creature* pCreature) const
    {
        return new boss_commander_ulthokAI(pCreature);
    }

    struct boss_commander_ulthokAI: public ScriptedAI
    {
        boss_commander_ulthokAI(Creature *c) :
            ScriptedAI(c)
        {
            pInstance = c->GetInstanceScript();
        }

        InstanceScript* pInstance;
        EventMap events;

        void Reset()
        {
            DoScriptText(SAY_RESET, me);
            events.Reset();

            if (pInstance)
                pInstance->SetData(DATA_COMMANDER_ULTHOK_EVENT, NOT_STARTED);
        }

        void KilledUnit(Unit* /*Victim*/)
        {
            DoScriptText(SAY_KILLED_UNIT, me);
        }

        void JustDied(Unit* /*Killer*/)
        {
            DoScriptText(SAY_JUST_DIED, me);
            if (pInstance)
                pInstance->SetData(DATA_COMMANDER_ULTHOK_EVENT, DONE);
        }

        void EnterCombat(Unit * /*who*/)
        {
            DoScriptText(SAY_ENTER_COMBAT, me);
            DoZoneInCombat();
            events.ScheduleEvent(EVENT_DARK_FISSURE, 30000);
            events.ScheduleEvent(EVENT_CURSE_OF_FATIGUE, 40000);
            events.ScheduleEvent(EVENT_ENRAGE, 60000);
            events.ScheduleEvent(EVENT_SQUEEZE, 50000);

            if (pInstance)
                pInstance->SetData(DATA_COMMANDER_ULTHOK_EVENT, IN_PROGRESS);
        }

        void UpdateAI(const uint32 diff)
        {
            //Return since we have no target
            if (!UpdateVictim())
                return;

            events.Update(diff);

            if (me->HasUnitState(UNIT_STAT_CASTING))
            {
                DoScriptText(SAY_CHANNELING, me);
                return;
            }

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                    case EVENT_DARK_FISSURE:
                        DoCast(me, SPELL_DARK_FISSURE);
                        events.ScheduleEvent(EVENT_DARK_FISSURE, 30000);
                        return;
                    case EVENT_CURSE_OF_FATIGUE:
                        if (Unit *pTarget = SelectUnit(SELECT_TARGET_RANDOM, 0))
                            DoCast(pTarget, SPELL_CURSE_OF_FATIGUE);
                        events.ScheduleEvent(EVENT_CURSE_OF_FATIGUE, 40000);
                        return;
                    case EVENT_ENRAGE:
                        DoCast(me, SPELL_ENRAGE);
                        events.ScheduleEvent(EVENT_ENRAGE, 60000);
                        return;
                    case EVENT_SQUEEZE:
                        if (Unit *pTarget = SelectUnit(SELECT_TARGET_RANDOM, 0))
                            DoCast(pTarget, DUNGEON_MODE(SPELL_SQUEEZE_NORMAL, SPELL_SQUEEZE_HEROIC));
                        events.ScheduleEvent(EVENT_SQUEEZE, 50000);
                        return;
                }
            }

            DoMeleeAttackIfReady();
        }
    };
};

void AddSC_boss_commander_ulthok()
{
    new boss_commander_ulthok();
}
