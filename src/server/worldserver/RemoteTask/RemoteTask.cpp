#include "RemoteTask.h"

const std::string RemoteTask::STATUS_NEW = "NEW";
const std::string RemoteTask::STATUS_FAILED = "FAILED";
const std::string RemoteTask::STATUS_SUCCESS = "SUCCESS";

volatile uint64 RemoteTask::lastFetchedId = 0;

void RemoteTask::commandFinished(void* remoteTaskArg, bool success)
{
    RemoteTask *remoteTask = (RemoteTask*) remoteTaskArg;
    remoteTask->setCommandSuccess(success);

    PreparedStatement* stmt = WorldDatabase.GetPreparedStatement(WORLD_UPDATE_REMOTE_TASK);

    stmt->setString(0, remoteTask->hasCommandSucceeded() ? STATUS_SUCCESS : STATUS_FAILED);
    stmt->setString(1, remoteTask->getPrintBuffer());
    stmt->setUInt64(2, remoteTask->getId());

    WorldDatabase.Execute(stmt);

    delete remoteTask;
}

void RemoteTask::enqueueCliCommands()
{
    uint64 lastId = lastFetchedId;

    PreparedStatement* stmt = WorldDatabase.GetPreparedStatement(WORLD_LOAD_REMOTE_TASK);
    stmt->setUInt64(0, lastId);
    PreparedQueryResult result = WorldDatabase.Query(stmt);

    if (!result)
    {
        // no results
        return;
    }

    std::list<RemoteTask*> tasks;

    // fetch list from DB
    do
    {
        Field* fields = result->Fetch();

        uint64 id = fields[0].GetUInt64();
        std::string command = fields[1].GetString();

        RemoteTask *remoteTask = new RemoteTask(id, command);
        tasks.push_back(remoteTask);

    } while (result->NextRow());

    // add remote tasks to queue (or reject)
    for (std::list<RemoteTask*>::const_iterator iter = tasks.begin(); iter != tasks.end(); ++iter)
    {
        RemoteTask *remoteTask = *iter;
        lastId = remoteTask->getId();

        const char* const command = remoteTask->getCommand().c_str();

        if (!command || !*command)
        {
            remoteTask->appendToPrintBuffer("Command mustn't be empty");
            commandFinished(remoteTask, false);
            continue;
        }

        // CliCommandHolder will be deleted from world, accessing after queueing is NOT save
        CliCommandHolder* cmd = new CliCommandHolder(remoteTask, command, print, commandFinished);
        sWorld->QueueCliCommand(cmd);
    }

    lastFetchedId = lastId;
    tasks.clear();
}
