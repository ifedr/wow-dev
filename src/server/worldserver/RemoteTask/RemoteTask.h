#ifndef _REMOTE_TASK_H
#define _REMOTE_TASK_H

#include "Common.h"
#include "Log.h"
#include "World.h"

class RemoteTask
{
public:
    static const std::string STATUS_NEW;
    static const std::string STATUS_FAILED;
    static const std::string STATUS_SUCCESS;

    RemoteTask(uint64 _id, std::string& _command) :
        id(_id), command(_command), success(false)
    {
    }

    virtual ~RemoteTask()
    {
    }

    static void enqueueCliCommands();

    static void print(void* callbackArg, const char* msg)
    {
        ((RemoteTask*) callbackArg)->appendToPrintBuffer(msg);
    }

    static void commandFinished(void* callbackArg, bool success);

private:
    static volatile uint64 lastFetchedId;

    const uint64 id;
    const std::string command;

    bool success;
    std::string printBuffer;

    uint64 getId() const
    {
        return id;
    }

    const std::string& getCommand() const
    {
        return command;
    }

    void appendToPrintBuffer(const char* msg)
    {
        printBuffer += msg;
    }

    const std::string& getPrintBuffer() const
    {
        return printBuffer;
    }

    void setCommandSuccess(bool val)
    {
        success = val;
    }

    bool hasCommandSucceeded() const
    {
        return success;
    }
};

#endif // _REMOTE_TASK_H
